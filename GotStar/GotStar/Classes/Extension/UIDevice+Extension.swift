//
//  UIDevice+Extension.swift
//  GotoGarageSale
//
//  Created by Admin on 18/01/19.
//  Copyright © 2019 GotoConcept. All rights reserved.
//

import Foundation

extension UIDevice {
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        return identifier
    }
}
