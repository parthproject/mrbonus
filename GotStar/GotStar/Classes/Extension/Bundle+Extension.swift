//
//  Bundle+Extension.swift
//  GotStar
//
//  Created by baps on 19/01/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

extension Bundle {
    static func appName() -> String {
        guard let dictionary = Bundle.main.infoDictionary else {
            return ""
        }
        if let version : String = dictionary["CFBundleName"] as? String {
            return version
        } else {
            return ""
        }
    }
}
