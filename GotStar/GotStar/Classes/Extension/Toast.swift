//
//  Toast.swift
//  GotoLiquorStoreiOS
//
//  Created by Admin on 13/07/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation


//https://stackoverflow.com/questions/31540375/how-to-toast-message-in-swift
class Toast
{
    class private func showAlert2(backgroundColor:UIColor, textColor:UIColor, message:String)
    {
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        if appDelegate.window!.viewWithTag(101010) != nil {
           return
        }
        let label = UILabel(frame: CGRect.zero)
        label.tag = 101010
        label.textAlignment = NSTextAlignment.center
        label.text = message
        label.font = UIFont(name: "", size: 15)
        label.adjustsFontSizeToFitWidth = true
        
        label.backgroundColor =  backgroundColor //UIColor.whiteColor()
        label.textColor = textColor //TEXT COLOR
        
        label.sizeToFit()
        label.numberOfLines = 4
        label.layer.shadowColor = UIColor.gray.cgColor
        label.layer.shadowOffset = CGSize(width: 4, height: 3)
        label.layer.shadowOpacity = 0.3
        //label.frame = CGRect(x: appDelegate.window!.frame.size.width, y: 64, width: appDelegate.window!.frame.size.width, height: 44)
        label.frame = CGRect(x: 0, y: appDelegate.window!.frame.size.height, width: appDelegate.window!.frame.size.width, height: 44)
        
        label.alpha = 1
        
        appDelegate.window!.addSubview(label)
        
        var basketTopFrame: CGRect = label.frame;
        //basketTopFrame.origin.x = 0;
        
        var bottomSafeArea:CGFloat = 0.0
        if #available(iOS 11.0, *) {
            bottomSafeArea = appDelegate.window!.safeAreaInsets.bottom
        }
        
        basketTopFrame.origin.y = appDelegate.window!.frame.size.height - bottomSafeArea - 44
        
        UIView.animate(withDuration
            :1.5, delay: 0.0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.1, options: UIView.AnimationOptions.curveEaseOut, animations: { () -> Void in
                label.frame = basketTopFrame
        },  completion: {
            (value: Bool) in
            UIView.animate(withDuration:1.5, delay: 1.5, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.1, options: UIView.AnimationOptions.curveEaseIn, animations: { () -> Void in
                label.alpha = 0
            },  completion: {
                (value: Bool) in
                label.removeFromSuperview()
            })
        })
    }
   
   class private func showAlert(backgroundColor:UIColor, textColor:UIColor, message:String)
   {
       let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
       if appDelegate.window!.viewWithTag(101010) != nil {
          return
       }
       let label = UILabel()
       label.tag = 101010
       label.textAlignment = NSTextAlignment.center
       label.font = UIFont(name: "", size: 15)
       //label.adjustsFontSizeToFitWidth = true
       
       label.backgroundColor =  backgroundColor
       label.textColor = textColor
       
       label.numberOfLines = 0
       label.text = message
       
       //label.sizeToFit()
       let maximumLabelSize: CGSize = CGSize(width: appDelegate.window!.frame.size.width, height: 100)
       let expectedLabelSize: CGSize = label.sizeThatFits(maximumLabelSize)
       var newFrame: CGRect = label.frame
       newFrame.size.height = expectedLabelSize.height
       let LABLE_HEIGHT = newFrame.size.height + 30
       
       label.layer.shadowColor = UIColor.gray.cgColor
       label.layer.shadowOffset = CGSize(width: 4, height: 3)
       label.layer.shadowOpacity = 0.3
       //label.frame = CGRect(x: appDelegate.window!.frame.size.width, y: 64, width: appDelegate.window!.frame.size.width, height: 44)
       label.frame = CGRect(x: 0, y: appDelegate.window!.frame.size.height, width: appDelegate.window!.frame.size.width, height: LABLE_HEIGHT)
       
       label.alpha = 1
       
       appDelegate.window!.addSubview(label)
       
       var basketTopFrame: CGRect = label.frame;
       //basketTopFrame.origin.x = 0;
       
       var bottomSafeArea:CGFloat = 0.0
       if #available(iOS 11.0, *) {
           bottomSafeArea = appDelegate.window!.safeAreaInsets.bottom
       }
       
       basketTopFrame.origin.y = appDelegate.window!.frame.size.height - bottomSafeArea - LABLE_HEIGHT
       
       UIView.animate(withDuration
           :1.5, delay: 0.0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.1, options: UIView.AnimationOptions.curveEaseOut, animations: { () -> Void in
               label.frame = basketTopFrame
       },  completion: {
           (value: Bool) in
           UIView.animate(withDuration:1.5, delay: 1.5, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.1, options: UIView.AnimationOptions.curveEaseIn, animations: { () -> Void in
               label.alpha = 0
           },  completion: {
               (value: Bool) in
               label.removeFromSuperview()
           })
       })
   }
    
    class func showPositiveMessage(message:String)
    {
//        showAlert(backgroundColor: .green, textColor: UIColor.white, message: message)
        showAlert(backgroundColor: .PRIMERY_COLOR, textColor: UIColor.black, message: message)
    }
    class func showNegativeMessage(message:String)
    {
        showAlert(backgroundColor: .red, textColor: UIColor.white, message: message)
    }
}
