//
//  Image+Extension.swift
//  GotoGarageSale
//
//  Created by Admin on 25/12/18.
//  Copyright © 2018 GotoConcept. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    var highestQualityJPEGNSData: Data { return self.jpegData(compressionQuality: 1.0)! }
    var highQualityJPEGNSData: Data    { return self.jpegData(compressionQuality: 0.75)! }
    var mediumQualityJPEGNSData: Data  { return self.jpegData(compressionQuality: 0.5)! }
    var lowQualityJPEGNSData: Data     { return self.jpegData(compressionQuality: 0.25)! }
    var lowestQualityJPEGNSData: Data  { return self.jpegData(compressionQuality: 0.0)! }
    
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ jpegQuality: JPEGQuality) -> Data? {
        return jpegData(compressionQuality: jpegQuality.rawValue)
    }
    
    func checkSizeOfImageInMB() -> Double{
        var imageSize = 0.0
        if let data = self.jpegData(compressionQuality: 1.0) {
            // print("size of image in MB: %f ", (Double(data.count) / 1024.0) / 1024.0)
            imageSize = (Double(data.count) / 1024.0) / 1024.0
        }
        
        return imageSize
    }
    
    //https://forums.developer.apple.com/thread/67564
    func changePhotoQuality(maxSize: CGFloat, times: Int) -> Data? {
        var maxQuality: CGFloat = 1.0
        var bestData: Data?
        
//        for _ in 1...times {
//            let thisQuality = (maxQuality ) * 0.8
//            guard let data =  self.jpegData(compressionQuality: thisQuality) else { return nil }
//            let thisSize = (CGFloat(data.count / 1024)) / 1024 // Byte to KB & KB to MB
//            if thisSize > maxSize {
//                maxQuality = thisQuality
//            } else {
//                bestData = data
//                return bestData
//            }
//        }
        
        var thisSize = CGFloat()
        repeat {
            let thisQuality = (maxQuality ) * 0.9
            guard let data =  self.jpegData(compressionQuality: thisQuality) else { return nil }
            thisSize = (CGFloat(data.count / 1024)) / 1024 // Byte to KB & KB to MB
            if thisSize > maxSize {
                maxQuality = thisQuality
            } else {
                bestData = data
                //return bestData
            }
            
        } while thisSize > maxSize
    
        return bestData
    }
}
