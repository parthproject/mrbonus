//
//  DateFormate.swift
//  GotoGarageSale
//
//  Created by Admin on 10/12/18.
//  Copyright © 2018 GotoConcept. All rights reserved.
//

import Foundation

let displayDateFormat = "E, MMM dd"
let displayTimeFormat = "hh:mm aa"
let serverDateFormat = "yyyy-MM-dd"
let serverTimeFormat = "HH:mm:ss"



func getDate(dateString:String) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = serverDateFormat
    guard let date = formatter.date(from: dateString) else {
//        print("Date is : \(dateString)")
        fatalError()
    }
    
    let myString = formatter.string(from: date)
    let yourDate = formatter.date(from: myString)
    formatter.dateFormat = displayDateFormat
    let myStringafd = formatter.string(from: yourDate!)
    
    return myStringafd
}

func getTime(dateString:String) -> String {
    let formatter = DateFormatter()
    formatter.locale = Locale(identifier: "en_US")
    formatter.dateFormat = serverTimeFormat
    guard let date = formatter.date(from: dateString) else {
        fatalError()
    }
    
    let myString = formatter.string(from: date)
    let yourDate = formatter.date(from: myString)
    formatter.dateFormat = displayTimeFormat
    
    let myStringafd = formatter.string(from: yourDate!)
    
    return myStringafd
}

func changeDateFormatFromDisplayToServer(date: Date) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    //let timeZone = NSTimeZone(name: "UTC")
    //formatter.timeZone = timeZone as TimeZone?

//    guard let date = formatter.date(from: dateString) else {
//        fatalError()
//    }
    
    let myString = formatter.string(from: date)
    let yourDate = formatter.date(from: myString)
    formatter.dateFormat = serverDateFormat
    let myStringafd = formatter.string(from: yourDate!)
    
    return myStringafd
}

func changeTimeFormatFromDisplayToServer(dateString:String) -> String {
    let formatter = DateFormatter()
    formatter.locale = Locale(identifier: "en_US")
    formatter.dateFormat = displayTimeFormat
    guard let date = formatter.date(from: dateString) else {
        fatalError()
    }
    
    let myString = formatter.string(from: date)
    let yourDate = formatter.date(from: myString)
    formatter.dateFormat = serverTimeFormat
    let myStringafd = formatter.string(from: yourDate!)
    
    return myStringafd
}

//func convertDateString(dateString : String!, fromFormat sourceFormat : String!, toFormat desFormat : String!) -> String {
//
//    let dateFormatter = DateFormatter()
//    dateFormatter.dateFormat = sourceFormat
//    let date = dateFormatter.date(from: dateString)
//
//    dateFormatter.dateFormat = desFormat
//
//    return dateFormatter.string(from: date!)
//}

func convertDateFormater(_ date: String) -> String
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let date = dateFormatter.date(from: date)
    dateFormatter.dateFormat = serverDateFormat
    let result = dateFormatter.string(from: date!)
    return  result
}

func getCurrentDateInString() -> String {
    let dateFormatter : DateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let date = Date()
    let dateString = dateFormatter.string(from: date)
    return dateString
}

//func changeCurrentDateFormat(format: String) -> String{
//    let formatter = DateFormatter()
//    formatter.dateFormat = format
//
//    let dateComponent = DateComponents()
//    let futureDate = Calendar.current.date(byAdding: dateComponent, to: Date())
//    let result = formatter.string(from: futureDate!)
//    return result
//}

func getStartTimeInString() -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = serverTimeFormat
    let initialStartTime = formatter.date(from: "10:00:00")
    let dateInString = formatter.string(from: initialStartTime!)
    return dateInString
}

func getEndTimeInString() -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = serverTimeFormat
    let initialStartTime = formatter.date(from: "18:00:00")
    let dateInString = formatter.string(from: initialStartTime!)
    return dateInString
}

func daysBetweenDates(startDate: Date, endDate: Date) -> Int
{
    let calendar = NSCalendar.current
    let components = calendar.dateComponents([ .hour, .minute], from: startDate, to: endDate)
    //print("Hour: \(components.hour ?? 0), Minute: \(components.minute ?? 0)")
    return components.hour!
}

func getDate(dateString:String, currentFormate: String, requiredFormate: String) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = currentFormate
    guard let date = formatter.date(from: dateString) else {
        fatalError()
    }
    
    let myString = formatter.string(from: date)
    let yourDate = formatter.date(from: myString)
    formatter.dateFormat = requiredFormate
    let myStringafd = formatter.string(from: yourDate!)
    
    return myStringafd
}

func getDateFromString(dateString: String) -> Date {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    let date = dateFormatter.date(from: dateString)!
    
    return date
}


extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
}
