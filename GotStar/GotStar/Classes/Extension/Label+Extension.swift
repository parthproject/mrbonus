//
//  Label+Extension.swift
//  GotoGarageSale
//
//  Created by Admin on 25/03/19.
//  Copyright © 2019 GotoConcept. All rights reserved.
//

//https://spin.atomicobject.com/2017/08/04/swift-extending-uilabel/
import Foundation
import UIKit

//@IBDesignable
//class EdgeInsetLabel: UILabel {
//    var textInsets = UIEdgeInsets.zero {
//        didSet { invalidateIntrinsicContentSize() }
//    }
//
//    override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
//        let insetRect = UIEdgeInsetsInsetRect(bounds, textInsets)
//        let textRect = super.textRect(forBounds: insetRect, limitedToNumberOfLines: numberOfLines)
//        let invertedInsets = UIEdgeInsets(top: -textInsets.top,
//                                          left: -textInsets.left,
//                                          bottom: -textInsets.bottom,
//                                          right: -textInsets.right)
//        return UIEdgeInsetsInsetRect(textRect, invertedInsets)
//    }
//
//    override func drawText(in rect: CGRect) {
//        super.drawText(in: UIEdgeInsetsInsetRect(rect, textInsets))
//    }
//}
//
//extension EdgeInsetLabel {
//    @IBInspectable
//    var leftTextInset: CGFloat {
//        set { textInsets.left = newValue }
//        get { return textInsets.left }
//    }
//
//    @IBInspectable
//    var rightTextInset: CGFloat {
//        set { textInsets.right = newValue }
//        get { return textInsets.right }
//    }
//
//    @IBInspectable
//    var topTextInset: CGFloat {
//        set { textInsets.top = newValue }
//        get { return textInsets.top }
//    }
//
//    @IBInspectable
//    var bottomTextInset: CGFloat {
//        set { textInsets.bottom = newValue }
//        get { return textInsets.bottom }
//    }
//}

class PaddingLabel: UILabel {
    
    @IBInspectable var topInset: CGFloat = 5.0
    @IBInspectable var bottomInset: CGFloat = 5.0
    @IBInspectable var leftInset: CGFloat = 5.0
    @IBInspectable var rightInset: CGFloat = 5.0
    
    override func drawText(in rect: CGRect) {
        //let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        //super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
        super.drawText(in: rect)
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            var contentSize = super.intrinsicContentSize
            contentSize.height += topInset + bottomInset
            contentSize.width += leftInset + rightInset
            return contentSize
        }
    }
    
}

extension UILabel {
    @IBInspectable
    var rotation: Int {
        get {
            return 0
        } set {
            let radians = CGFloat(CGFloat(Double.pi) * CGFloat(newValue) / CGFloat(180.0))
            self.transform = CGAffineTransform(rotationAngle: radians)
        }
    }
}


/*
The quick brown fox jumps over the lazy dog.

 */
