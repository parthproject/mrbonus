//
//  View+Extension.swift
//  Foody
//
//  Created by Admin on 19/11/18.
//  Copyright © 2018 techgrains. All rights reserved.
//

import Foundation

var associateObjectValue: Int = 0


extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
//            print("Get border images: \(layer.borderWidth)")
            return layer.borderWidth
        }
        set {
            let screenScale = UIScreen.main.scale
            if(screenScale == 1) {
                layer.borderWidth = newValue
            } else if(screenScale == 2) {
                layer.borderWidth = newValue * 0.5
            } else if(screenScale == 3) {
                layer.borderWidth = newValue * 0.33
            }
            
            //print("Set border images: \(layer.borderWidth)")
            //layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    //https://stackoverflow.com/questions/39624675/add-shadow-on-uiview-using-swift-3
    private static var _addShadow:Bool = false
    @IBInspectable var addShadow:Bool {
        get {
            return UIView._addShadow
        }
        set(newValue) {
            if(newValue == true){
                layer.masksToBounds = false
//                layer.shadowColor = UIColor.gray.cgColor
                layer.shadowColor = UIColor(named: "NS_Black")?.cgColor
                layer.shadowOpacity = 0.5//0.5
                //layer.shadowOffset = CGSize(width: -1, height: 1)
                layer.shadowOffset = .zero
                layer.shadowRadius = 2//4
                
                //layer.shadowPath = UIBezierPath(rect: bounds).cgPath
                layer.shouldRasterize = true
                layer.rasterizationScale =  UIScreen.main.scale
            }
        }
    }
    
    private static var _addShadowBottom:Bool = false
    @IBInspectable var addShadowBottom:Bool {
        get {
            return UIView._addShadowBottom
        }
        set(newValue) {
            if(newValue == true){
                layer.masksToBounds = false
                layer.shadowColor = UIColor.black.cgColor
                layer.shadowOpacity = 0.1
                layer.shadowOffset = CGSize(width: 0, height: 4)
                //layer.shadowRadius = 2
                
                layer.shouldRasterize = true
                layer.rasterizationScale =  UIScreen.main.scale
            }
        }
    }
    
//    private static var _orangeShadowBottom:Bool = false
//    @IBInspectable var orangeShadowBottom:Bool {
//        get {
//            return UIView._orangeShadowBottom
//        }
//        set(newValue) {
//            if(newValue == true){
//                layer.masksToBounds = false
//                layer.shadowColor = UIColor.P3.cgColor
//                layer.shadowOpacity = 0.5
//                layer.shadowOffset = CGSize(width: 0, height: 4)
//                layer.shadowRadius = 4
//                
//                layer.shouldRasterize = true
//                layer.rasterizationScale =  UIScreen.main.scale
//            }
//        }
//    }
    
    

        
    //https://www.yudiz.com/facebook-shimmer-animation-swift-4/
//    fileprivate var isAnimate: Bool {
//        get {
//            return objc_getAssociatedObject(self, &associateObjectValue) as? Bool ?? false
//        }
//        set {
//            return objc_setAssociatedObject(self, &associateObjectValue, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
//        }
//    }
//    
//    @IBInspectable var shimmerAnimation: Bool {
//        get {
//            return isAnimate
//        }
//        set {
//            self.isAnimate = newValue
//        }
//    }
//    
//    func subviewsRecursive() -> [UIView] {
//        return subviews + subviews.flatMap { $0.subviewsRecursive() }
//    }
    
    
    // MARK: - Shimmer Animation
//    func startShimmerAnimation() {
//        for animateView in getSubViewsForAnimate() {
//            animateView.clipsToBounds = true
//            let gradientLayer = CAGradientLayer()
//            gradientLayer.colors = [UIColor.clear.cgColor, UIColor.white.withAlphaComponent(0.8).cgColor, UIColor.clear.cgColor]
//            //gradientLayer.colors = [UIColor.gray.withAlphaComponent(0.5).cgColor, UIColor.gray.withAlphaComponent(0.8).cgColor]
//            gradientLayer.startPoint = CGPoint(x: 0.7, y: 1.0)
//            gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.8)
//            //            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
//            //            gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
//            gradientLayer.frame = animateView.bounds
//            animateView.layer.mask = gradientLayer
//            
//            let animation = CABasicAnimation(keyPath: "transform.translation.x")
//            animation.duration = 1.2//1.5
//            animation.fromValue = -animateView.frame.size.width
//            animation.toValue = animateView.frame.size.width
//            animation.repeatCount = .infinity
//            
//            gradientLayer.add(animation, forKey: "")
//        }
//    }
//    
//    func stopShimmerAnimation() {
//        for animateView in getSubViewsForAnimate() {
//            animateView.layer.removeAllAnimations()
//            animateView.layer.mask = nil
//        }
//    }
//    
//    func getSubViewsForAnimate() -> [UIView] {
//        var obj: [UIView] = []
////        for objView in view.subviewsRecursive() {
//        for objView in self.subviewsRecursive() {
//            obj.append(objView)
//        }
//        return obj.filter({ (obj) -> Bool in
//            obj.shimmerAnimation
//        })
//    }
}
