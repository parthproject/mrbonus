//
//  UITableView+Extensions.swift
//  GotoLiquorStoreiOS
//
//  Created by Admin on 20/06/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
import  UIKit

extension UITableView {
    func registerCell<Cell: UITableViewCell> (_ cellClass: Cell.Type) {
        register(cellClass, forCellReuseIdentifier: String(describing: cellClass))
    }
    
    func dequeueReusableCell<Cell: UITableViewCell>(forIndexPath indexPath: IndexPath) -> Cell {
        let identifier = String(describing: Cell.self)
        guard let cell = self.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? Cell else {
            fatalError("Error for cell if: \(identifier) at \(indexPath)")
        }
        return cell
    }
}
