//
//  String+Extension.swift
//  GotoGarageSale
//
//  Created by Admin on 29/12/18.
//  Copyright © 2018 GotoConcept. All rights reserved.
//

import Foundation

extension String {
    func checkRangeOfString() -> String {
        var finalWebUrl = ""
        if self.range(of:"http") == nil {
            finalWebUrl = "https://\(self)"
        } else {
            finalWebUrl = self
        }
        return finalWebUrl
    }
    
//    static func deviceName() -> String {
    func deviceName() -> String {
    
        var systemInfo = utsname()
        uname(&systemInfo)
        
        guard let iOSDeviceModelsPath = Bundle.main.path(forResource: "iOSDeviceModelMapping", ofType: "plist") else { return "" }
        guard let iOSDevices = NSDictionary(contentsOfFile: iOSDeviceModelsPath) else { return "" }
        
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        return iOSDevices.value(forKey: identifier) as! String
    }
    
    func getVersionString() -> String {
        let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
        return appVersion!
    }
    
    func getBuildString() -> String {
        let buildVersion = Bundle.main.infoDictionary!["CFBundleVersion"] as? String
        return buildVersion!
    }
    
    //1.0.1
    //a*10(power 4) + b*10(power 2) + c
    func getVersionNumberFromVersion() -> Int {
        let list = self.components(separatedBy: ".")
        
        var number = 0
        if(list.count == 3) {
            number = (Int(list[0])! * 10000) +  (Int(list[1])! * 100) + Int(list[2])!
        } else if(list.count == 2) {
            number = (Int(list[0])! * 10000) +  (Int(list[1])! * 100)
        }
        return number
    }
    
    //https://stackoverflow.com/questions/37048759/swift-display-html-data-in-a-label-or-textview
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    
    
    func filterAndModifyTextAttributes(searchStringCharacters: String) -> NSMutableAttributedString {
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: self)
        let pattern = searchStringCharacters.lowercased()
        let range: NSRange = NSMakeRange(0, self.count)
        var regex = NSRegularExpression()
        do {
            regex = try NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options())
            regex.enumerateMatches(in: self.lowercased(), options: NSRegularExpression.MatchingOptions(), range: range) {
                (textCheckingResult, matchingFlags, stop) in
                let subRange = textCheckingResult?.range
//                let attributes : [NSAttributedString.Key : Any] = [.font : UIFont.boldSystemFont(ofSize: 17),.foregroundColor: UIColor.red ]
                let attributes : [NSAttributedString.Key : Any] = [.font : UIFont.boldSystemFont(ofSize: 15),.foregroundColor: UIColor.black ]
                attributedString.addAttributes(attributes, range: subRange!)
            }
        }catch{
            print(error.localizedDescription)
        }
        return attributedString
    }

    
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
    
    func strikeThrough() -> NSAttributedString {
        let attributeString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSUnderlineStyle.single.rawValue, range: NSMakeRange(0,attributeString.length))
        return attributeString
    }
}
extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}




/*
  // ************ guard ************
 guard let name = publicName, let photo = publicPhoto, let age = publicAge else {
    // if one or two of the variables contain "nil"
    print("Something is missing")
    return
 }
 
 
 
 A defer block only executes only after current scope (loop, method, etc) exits.
 for i in 1...3 {
    defer { print ("Deferred \(i)") }
    print ("First \(i)")
 }
 // First 1
 // Deferred 1
 // First 2
 // Deferred 2
 // First 3
 // Deferred 3
 
 
 
 // ************ Error handling ************
 enum HeightError: Error {
    case maxHeight
    case minHeight
 }
 func checkHeightError(height: Int) throws {
     if height > 200 {
     throw HeightError.maxHeight
     } else if height < 140 {
     throw HeightError.minHeight
     } else {
     print("Enjoy your ride")
     }
 }
 do {
    try checkHeightError(height: 240)
 } catch HeightError.maxHeight {
    print("Get out of here")
    // Logic
 } catch HeightError.minHeight {
    print("Too short to ride")
 }
 
 
 // ************ Generic ************
 func printElement<T>(array: [T]) {
    for element in array {
    print(element)
 }
 }
 
 
 */
