//
//  ScrollView+Extension.swift
//  GotoLiquorStoreiOS
//
//  Created by Admin on 27/07/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation


extension UIScrollView {
    
    func scrollToBottom() {
        let bottomOffset = CGPoint(x: 0, y: contentSize.height - bounds.size.height + contentInset.bottom)
        setContentOffset(bottomOffset, animated: true)
    }
}
