//
//  CategorylistService.swift
//  GotStar
//
//  Created by Admin on 12/7/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

class CategorylistService: Service {
    static var instance: CategorylistService?
    
    class func getInstance() -> CategorylistService {
        if instance == nil {
            instance = CategorylistService()
        }
        return instance!
    }
    
    override init() {
        super.init()
    }
    
    func serviceParams(_ request: CategorylistRequest) -> [AnyHashable: Any] {
        let dictionary = [AnyHashable: Any]()
        return dictionary
    }
    
    func generateResponseModal(_ json: [AnyHashable: Any]?) -> CategorylistResponse {
        let response = CategorylistResponse()
        
        let status: Int32? = (json!["status"] as? Int32)
        if status ==  STATUS_OK {
        //if self.statusCode == STATUS_OK {
            response.success = true
         //PopularModel
            
            let list:Array<Any>? = (json?["Result"] as? Array<Any>)
            if self.hasValue(list) {
                for counter in 0..<list!.count {
                    let dic = list![counter] as? [AnyHashable: Any]
                    response.categoryList.append(categoryrModelFromDictionary(dic!))
                }
            }
        }
        else {
            response.success = false
            if self.hasValue(json) {
                response.setErrorJson(json!)
            }
        }
        return response
    }
    
    func serviceResponse(forURL requestUrl: String, with request: CategorylistRequest) -> String {
        var response: String = ""
        switch CATEGORY_SERVICE {
        case LIVE_SERVICE:
            let dictionary: [AnyHashable: Any] = self.serviceParams(request)
            response = self.tgService(method: HttpMethod.GET, url: requestUrl, params: dictionary, headers: SessionManager.getInstance().getHeaderDictionary())
            break
        case MOCK_SERVICE:
            self.statusCode = Int(STATUS_OK)
            if(statusCode == STATUS_OK) {
                response = ServiceMockResponse.getFileContent("CategorylistResponse", ofFileType: "json", afterDelay: 0.0)
            } else {
                response = ServiceMockResponse.getFileContent("ErrorResponse", ofFileType: "json", afterDelay: 0.0)
            }
            
            break
        default:
            break
        }
        return response
    }
    
    func categorylistRequest(_ request:CategorylistRequest) -> CategorylistResponse? {
        let serviceResponse: String = self.serviceResponse(forURL: request.url(), with: request)
        _ = self.isSpecificStringAvailable(serviceResponse)
        
        var json = [AnyHashable: Any]()
        if(statusCode == SERVICE_TIME_OUT_CODE) {
            json = self.dictionaryfromJSON(ServiceMockResponse.getFileContent("ServiceTimeOutResponse", ofFileType: "json", afterDelay: 0.0))!
        } else {
            do {
                let data = serviceResponse.data(using: String.Encoding.utf8, allowLossyConversion: false)!
                _ = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
                json = self.dictionaryfromJSON(serviceResponse)!
            } catch let error as NSError {
                json = self.dictionaryfromJSON(ServiceMockResponse.getFileContent("ServiceTimeOutResponse", ofFileType: "json", afterDelay: 0.0))!
                print("Failed to load: \(error.localizedDescription)")
            }
        }
        if(IS_LOG_ENABLE == 1) {
            //print(jsonBodyStringFromDic(params: json))
        }
        let response: CategorylistResponse? = self.generateResponseModal(json)
        return response
    }
}
