//
//  AddWatchlistService.swift
//  GotStar
//
//  Created by baps on 19/01/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class AddWatchlistService: Service {
    static var instance: AddWatchlistService?
    
    class func getInstance() -> AddWatchlistService {
        if instance == nil {
            instance = AddWatchlistService()
        }
        return instance!
    }
    
    override init() {
        super.init()
    }
    
    func serviceParams(_ request: AddWatchlistRequest) -> [AnyHashable: Any] {
        var dictionary = [AnyHashable: Any]()
        
        // Set user_id
        if self.hasValue(request.user_id) {
            dictionary["user_id"] = request.user_id
        }
        // Set tv_id
        if self.hasValue(request.tv_id) {
            dictionary["tv_id"] = request.tv_id
        }
        else {
            dictionary["tv_id"] = ""
        }
        
        // Set a_id
        if self.hasValue(request.a_id) {
            dictionary["a_id"] = request.a_id
        } else {
            dictionary["a_id"] = ""
        }
        
        // Set c_id
        if self.hasValue(request.c_id) {
            dictionary["c_id"] = request.c_id
        } else {
            dictionary["c_id"] = ""
        }
        
        return dictionary
    }
    
    func generateResponseModal(_ json: [AnyHashable: Any]?) -> AddWatchlistResponse {
        let response = AddWatchlistResponse()
        
        let status: Int32? = (json!["status"] as? Int32)
        if status ==  STATUS_OK {
        //if self.statusCode == STATUS_OK {
            response.success = true
         
            let message: String? = (json!["message"] as? String)
            if hasValue(message) {
                response.responseString = message!
            }
            
        }
        else {
            response.success = false
            
            if self.hasValue(json) {
                response.setErrorJson(json!)
            }
        }
        return response
    }
    
    func serviceResponse(forURL requestUrl: String, with request: AddWatchlistRequest) -> String {
        var response: String = ""
        switch ADD_WATCHLIST_SERVICE {
        case LIVE_SERVICE:
            let dictionary: [AnyHashable: Any] = self.serviceParams(request)
            response = self.tgService(method: HttpMethod.GET, url: requestUrl, params: dictionary, headers: SessionManager.getInstance().getHeaderDictionary())
            break
        case MOCK_SERVICE:
            self.statusCode = Int(STATUS_OK)
            if(statusCode == STATUS_OK) {
                response = ServiceMockResponse.getFileContent("AddWatchlistResponse", ofFileType: "json", afterDelay: 0.0)
            } else {
                response = ServiceMockResponse.getFileContent("ErrorResponse", ofFileType: "json", afterDelay: 0.0)
            }
            
            break
        default:
            break
        }
        return response
    }
    
    func addWatchlistRequest(_ request:AddWatchlistRequest) -> AddWatchlistResponse? {
        let serviceResponse: String = self.serviceResponse(forURL: request.url(), with: request)
        _ = self.isSpecificStringAvailable(serviceResponse)
        
        var json = [AnyHashable: Any]()
        if(statusCode == SERVICE_TIME_OUT_CODE) {
            json = self.dictionaryfromJSON(ServiceMockResponse.getFileContent("ServiceTimeOutResponse", ofFileType: "json", afterDelay: 0.0))!
        } else {
            do {
                let data = serviceResponse.data(using: String.Encoding.utf8, allowLossyConversion: false)!
                _ = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
                json = self.dictionaryfromJSON(serviceResponse)!
            } catch let error as NSError {
                json = self.dictionaryfromJSON(ServiceMockResponse.getFileContent("ServiceTimeOutResponse", ofFileType: "json", afterDelay: 0.0))!
                print("Failed to load: \(error.localizedDescription)")
            }
        }
        if(IS_LOG_ENABLE == 1) {
//            print(jsonBodyStringFromDic(params: json))
        }
        let response: AddWatchlistResponse? = self.generateResponseModal(json)
        return response
    }
}
