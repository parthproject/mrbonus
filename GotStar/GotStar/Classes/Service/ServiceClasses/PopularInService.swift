//
//  PopularInService.swift
//  GotStar
//
//  Created by Admin on 12/7/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

class PopularInService: Service {
    static var instance: PopularInService?
    
    class func getInstance() -> PopularInService {
        if instance == nil {
            instance = PopularInService()
        }
        return instance!
    }
    
    override init() {
        super.init()
    }
    
    func serviceParams(_ request: PopularInRequest) -> [AnyHashable: Any] {
        var dictionary = [AnyHashable: Any]()
        
        // Set type
        if self.hasValue(request.type) {
            dictionary["type"] = request.type
        }
        
        // Set cat_id
        if self.hasValue(request.cat_id) {
            dictionary["cat_id"] = request.cat_id
        }
        
        return dictionary
    }
    
    func generateResponseModal(_ json: [AnyHashable: Any]?) -> PopularInResponse {
        let response = PopularInResponse()
        
        let status: Int32? = (json!["status"] as? Int32)
        if status ==  STATUS_OK {
        //if self.statusCode == STATUS_OK {
            response.success = true
         
            if self.hasValue(json) {
                response.setErrorJson(json!)
                
                let list:Array<Any>? = (json?["Result"] as? Array<Any>)
                if self.hasValue(list) {
                    for counter in 0..<list!.count {
                        let dic = list![counter] as? [AnyHashable: Any]
                        response.popularInList.append(bannerModelFromDictionary(dic!))
                    }
                }
            }
        }
        else {
            response.success = false
            
            if self.hasValue(json) {
                response.setErrorJson(json!)
            }
        }
        return response
    }
    
    func serviceResponse(forURL requestUrl: String, with request: PopularInRequest) -> String {
        var response: String = ""
        switch POPULAR_IN_SERVICE {
        case LIVE_SERVICE:
            let dictionary: [AnyHashable: Any] = self.serviceParams(request)
            response = self.tgService(method: HttpMethod.GET, url: requestUrl, params: dictionary, headers: SessionManager.getInstance().getHeaderDictionary())
            break
        case MOCK_SERVICE:
            self.statusCode = Int(STATUS_OK)
            if(statusCode == STATUS_OK) {
                response = ServiceMockResponse.getFileContent("PopularInResponse", ofFileType: "json", afterDelay: 0.0)
            } else {
                response = ServiceMockResponse.getFileContent("ErrorResponse", ofFileType: "json", afterDelay: 0.0)
            }
            
            break
        default:
            break
        }
        return response
    }
    
    func popularInRequest(_ request:PopularInRequest) -> PopularInResponse? {
        let serviceResponse: String = self.serviceResponse(forURL: request.url(), with: request)
        _ = self.isSpecificStringAvailable(serviceResponse)
        
        var json = [AnyHashable: Any]()
        if(statusCode == SERVICE_TIME_OUT_CODE) {
            json = self.dictionaryfromJSON(ServiceMockResponse.getFileContent("ServiceTimeOutResponse", ofFileType: "json", afterDelay: 0.0))!
        } else {
            do {
                let data = serviceResponse.data(using: String.Encoding.utf8, allowLossyConversion: false)!
                _ = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
                json = self.dictionaryfromJSON(serviceResponse)!
            } catch let error as NSError {
                json = self.dictionaryfromJSON(ServiceMockResponse.getFileContent("ServiceTimeOutResponse", ofFileType: "json", afterDelay: 0.0))!
                print("Failed to load: \(error.localizedDescription)")
            }
        }
        if(IS_LOG_ENABLE == 1) {
            //print(jsonBodyStringFromDic(params: json))
        }
        let response: PopularInResponse? = self.generateResponseModal(json)
        return response
    }
}
