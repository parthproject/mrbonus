//
//  GetWatchListService.swift
//  GotStar
//
//  Created by baps on 19/01/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class GetWatchListService: Service {
    static var instance: GetWatchListService?
    
    class func getInstance() -> GetWatchListService {
        if instance == nil {
            instance = GetWatchListService()
        }
        return instance!
    }
    
    override init() {
        super.init()
    }
    
    func serviceParams(_ request: GetWatchListRequest) -> [AnyHashable: Any] {
        var dictionary = [AnyHashable: Any]()
        
        // Set ftvs_id
        if self.hasValue(request.user_id) {
            dictionary["user_id"] = request.user_id
        }
        
        
        return dictionary
    }
    
    func generateResponseModal(_ json: [AnyHashable: Any]?) -> GetWatchListResponse {
        let response = GetWatchListResponse()
        
        let status: Int32? = (json!["status"] as? Int32)
        if status ==  STATUS_OK {
        //if self.statusCode == STATUS_OK {
            response.success = true
         
            if self.hasValue(json) {
                response.setErrorJson(json!)
                
                let list:Array<Any>? = (json?["Result"] as? Array<Any>)
                if self.hasValue(list) {
                    for counter in 0..<list!.count {
                        let dic = list![counter] as? [AnyHashable: Any]
                        response.list.append(bannerModelFromDictionary(dic!))
                    }
                }
            }
        }
        else {
            response.success = false
            
            if self.hasValue(json) {
                response.setErrorJson(json!)
            }
        }
        return response
    }
    
    func serviceResponse(forURL requestUrl: String, with request: GetWatchListRequest) -> String {
        var response: String = ""
        switch GET_WATCHLIST_SERVICE {
        case LIVE_SERVICE:
            let dictionary: [AnyHashable: Any] = self.serviceParams(request)
            response = self.tgService(method: HttpMethod.GET, url: requestUrl, params: dictionary, headers: SessionManager.getInstance().getHeaderDictionary())
            break
        case MOCK_SERVICE:
            self.statusCode = Int(STATUS_OK)
            if(statusCode == STATUS_OK) {
                response = ServiceMockResponse.getFileContent("GetWatchListResponse", ofFileType: "json", afterDelay: 0.0)
            } else {
                response = ServiceMockResponse.getFileContent("ErrorResponse", ofFileType: "json", afterDelay: 0.0)
            }
            
            break
        default:
            break
        }
        return response
    }
    
    func getWatchListRequest(_ request:GetWatchListRequest) -> GetWatchListResponse? {
        let serviceResponse: String = self.serviceResponse(forURL: request.url(), with: request)
        _ = self.isSpecificStringAvailable(serviceResponse)
        
        var json = [AnyHashable: Any]()
        if(statusCode == SERVICE_TIME_OUT_CODE) {
            json = self.dictionaryfromJSON(ServiceMockResponse.getFileContent("ServiceTimeOutResponse", ofFileType: "json", afterDelay: 0.0))!
        } else {
            do {
                let data = serviceResponse.data(using: String.Encoding.utf8, allowLossyConversion: false)!
                _ = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
                json = self.dictionaryfromJSON(serviceResponse)!
            } catch let error as NSError {
                json = self.dictionaryfromJSON(ServiceMockResponse.getFileContent("ServiceTimeOutResponse", ofFileType: "json", afterDelay: 0.0))!
                print("Failed to load: \(error.localizedDescription)")
            }
        }
        if(IS_LOG_ENABLE == 1) {
//            print(jsonBodyStringFromDic(params: json))
        }
        let response: GetWatchListResponse? = self.generateResponseModal(json)
        return response
    }
}
