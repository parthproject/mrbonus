//
//  ChannelListService.swift
//  GotStar
//
//  Created by baps on 22/01/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class ChannelListService: Service {
    static var instance: ChannelListService?
    
    class func getInstance() -> ChannelListService {
        if instance == nil {
            instance = ChannelListService()
        }
        return instance!
    }
    
    override init() {
        super.init()
    }
    
    func serviceParams(_ request: ChannelListRequest) -> [AnyHashable: Any] {
        let dictionary = [AnyHashable: Any]()
        
//        // Set type
//        if self.hasValue(request.type) {
//            dictionary["type"] = request.type
//        }
        
        
        return dictionary
    }
    
    func generateResponseModal(_ json: [AnyHashable: Any]?) -> ChannelListResponse {
        let response = ChannelListResponse()
        
        let status: Int32? = (json!["status"] as? Int32)
        if status ==  STATUS_OK {
        //if self.statusCode == STATUS_OK {
            response.success = true
         
            if self.hasValue(json) {
                response.setErrorJson(json!)
                
                let list:Array<Any>? = (json?["Result"] as? Array<Any>)
                if self.hasValue(list) {
                    for counter in 0..<list!.count {
                        let dic = list![counter] as? [AnyHashable: Any]
                        response.list.append(channelModelFromDictionary(dic!))
                    }
                }
            }
        }
        else {
            response.success = false
            
            if self.hasValue(json) {
                response.setErrorJson(json!)
            }
        }
        return response
    }
    
    func serviceResponse(forURL requestUrl: String, with request: ChannelListRequest) -> String {
        var response: String = ""
        switch CHANNEL_LIST_SERVICE {
        case LIVE_SERVICE:
            let dictionary: [AnyHashable: Any] = self.serviceParams(request)
            response = self.tgService(method: HttpMethod.GET, url: requestUrl, params: dictionary, headers: SessionManager.getInstance().getHeaderDictionary())
            break
        case MOCK_SERVICE:
            self.statusCode = Int(STATUS_OK)
            if(statusCode == STATUS_OK) {
                response = ServiceMockResponse.getFileContent("ChannelListResponse", ofFileType: "json", afterDelay: 0.0)
            } else {
                response = ServiceMockResponse.getFileContent("ErrorResponse", ofFileType: "json", afterDelay: 0.0)
            }
            
            break
        default:
            break
        }
        return response
    }
    
    func channelListRequest(_ request:ChannelListRequest) -> ChannelListResponse? {
        let serviceResponse: String = self.serviceResponse(forURL: request.url(), with: request)
        _ = self.isSpecificStringAvailable(serviceResponse)
        
        var json = [AnyHashable: Any]()
        if(statusCode == SERVICE_TIME_OUT_CODE) {
            json = self.dictionaryfromJSON(ServiceMockResponse.getFileContent("ServiceTimeOutResponse", ofFileType: "json", afterDelay: 0.0))!
        } else {
            do {
                let data = serviceResponse.data(using: String.Encoding.utf8, allowLossyConversion: false)!
                _ = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
                json = self.dictionaryfromJSON(serviceResponse)!
            } catch let error as NSError {
                json = self.dictionaryfromJSON(ServiceMockResponse.getFileContent("ServiceTimeOutResponse", ofFileType: "json", afterDelay: 0.0))!
                print("Failed to load: \(error.localizedDescription)")
            }
        }
        if(IS_LOG_ENABLE == 1) {
//            print(jsonBodyStringFromDic(params: json))
        }
        let response: ChannelListResponse? = self.generateResponseModal(json)
        return response
    }
}
