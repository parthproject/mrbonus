//
//  ProfileFetchService.swift
//  GotoGarageSale
//
//  Created by Admin on 12/5/18.
//  Copyright © 2018 GotoConcept. All rights reserved.
//

import UIKit

class ProfileFetchService: Service {
    static var instance: ProfileFetchService?
    
    class func getInstance() -> ProfileFetchService {
        if instance == nil {
            instance = ProfileFetchService()
        }
        return instance!
    }
    
    override init() {
        super.init()
    }
    
    func serviceParams(_ request: ProfileFetchRequest) -> [AnyHashable: Any] {
        var dictionary = [AnyHashable: Any]()
        
        if self.hasValue(request.user_id) {
            dictionary["user_id"] = request.user_id
        }
        
        return dictionary
    }
    
    func generateResponseModal(_ json: [AnyHashable: Any]?) -> ProfileFetchResponse {
        let response = ProfileFetchResponse()
        
        let status: Int32? = (json!["status"] as? Int32)
        if status ==  STATUS_OK {
            //        if self.statusCode == STATUS_OK {
            response.success = true
            
            let list:Array<Any>? = (json?["Result"] as? Array<Any>)
            if self.hasValue(list) {
//                for counter in 0..<list!.count {
                for counter in 0..<1 {
                    let dic = list![counter] as? [AnyHashable: Any]
//                    response.bannerList.append(bannerModelFromDictionary(dic!))
                    
                    let obj = ProfileModel()
                    
                    // Get email
                    let email: String? = (dic!["email"] as? String)
                    if hasValue(email) {
                        obj.email = email!
                    }
                    
                    let id: String? = (dic!["id"] as? String)
                    if hasValue(id) {
                        obj.id = id!
                    }
                    
                    let mobile_number: String? = (dic!["mobile_number"] as? String)
                    if hasValue(mobile_number) {
                        obj.mobile_number = mobile_number!
                    }
                    
                    let fullname: String? = (dic!["fullname"] as? String)
                    if hasValue(fullname) {
                        obj.fullname = fullname!
                    }
                    
                    response.profileModel = obj
                }
            }
        }
        else {
            response.success = false
            if self.hasValue(json) {
                response.setErrorJson(json!)
            }
        }
        return response
    }
    
    func serviceResponse(forURL requestUrl: String, with request: ProfileFetchRequest) -> String {
        var response: String = ""
        switch PROFILE_SERVICE {
        case LIVE_SERVICE:
            let dictionary: [AnyHashable: Any] = self.serviceParams(request)
            response = self.tgService(method: HttpMethod.GET, url: requestUrl, params: dictionary, headers: SessionManager.getInstance().getHeaderDictionary())
            break
        case MOCK_SERVICE:
            self.statusCode = Int(STATUS_OK)
            if(statusCode == STATUS_OK) {
                response = ServiceMockResponse.getFileContent("ProfileFetchResponse", ofFileType: "json", afterDelay: 0.0)
            } else {
                response = ServiceMockResponse.getFileContent("ErrorResponse", ofFileType: "json", afterDelay: 0.0)
            }
            
            break
        default:
            break
        }
        return response
    }
    
    func profileFetchRequest(_ request:ProfileFetchRequest) -> ProfileFetchResponse? {
        let serviceResponse: String = self.serviceResponse(forURL: request.url(), with: request)
        _ = self.isSpecificStringAvailable(serviceResponse)
        
        var json = [AnyHashable: Any]()
        if(statusCode == SERVICE_TIME_OUT_CODE) {
            json = self.dictionaryfromJSON(ServiceMockResponse.getFileContent("ServiceTimeOutResponse", ofFileType: "json", afterDelay: 0.0))!
        }
        else {
            //json = self.dictionaryfromJSON(serviceResponse)!
            do {
                let data = serviceResponse.data(using: String.Encoding.utf8, allowLossyConversion: false)!
                _ = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
                json = self.dictionaryfromJSON(serviceResponse)!
            } catch let error as NSError {
                json = self.dictionaryfromJSON(ServiceMockResponse.getFileContent("ServiceTimeOutResponse", ofFileType: "json", afterDelay: 0.0))!
                print("Failed to load: \(error.localizedDescription)")
            }
        }
        
        if(IS_LOG_ENABLE == 1) {
            print(jsonBodyStringFromDic(params: json))
        }
        let response: ProfileFetchResponse? = self.generateResponseModal(json)
        return response
    }
}
