//
//  GeneralSettingService.swift
//  E-Books
//
//  Created by Admin on 12/28/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation


class GeneralSettingService: Service {
    static var instance: GeneralSettingService?
    
    class func getInstance() -> GeneralSettingService {
        if instance == nil {
            instance = GeneralSettingService()
        }
        return instance!
    }
    
    override init() {
        super.init()
        // Initialization code here.
    }
    
    func serviceParams(_ request: GeneralSettingRequest) -> [AnyHashable: Any] {
        let dictionary = [AnyHashable: Any]()
        return dictionary
    }
    
    func generateResponseModal(_ json: [AnyHashable: Any]?) -> GeneralSettingResponse {
        let response = GeneralSettingResponse()
        
        let status: Int32? = (json!["status"] as? Int32)
        if status ==  STATUS_OK {
        //if self.statusCode == STATUS_OK {
            response.success = true
            
            let list:Array<Any>? = (json?["Result"] as? Array<Any>)
            if self.hasValue(list) {
                for counter in 0..<list!.count {
                    let dic = list![counter] as? [AnyHashable: Any]
                    response.list.append(generalSettingModelFromDictionary(dic!))
                }
            }
        }
        else {
            response.success = false
            if self.hasValue(json) {
                response.setErrorJson(json!)
            }
        }
        return response
    }
    
    func serviceResponse(forURL requestUrl: String, with request: GeneralSettingRequest) -> String {
        var response: String = ""
        switch GENERAL_SETTING_SERVICE {
        case LIVE_SERVICE:
            let dictionary: [AnyHashable: Any] = self.serviceParams(request)
            if(IS_LOG_ENABLE == 1) {
                print("Genearl Setting Service:", dictionary)
            }
            response = self.tgService(method: HttpMethod.GET, url: requestUrl, params: dictionary, headers: SessionManager.getInstance().getHeaderDictionary())
            break
        case MOCK_SERVICE:
            self.statusCode = Int(STATUS_OK)
            //self.statusCode = Int(STATUS_BAD_REQUEST)
            if(statusCode == STATUS_OK) {
                response = ServiceMockResponse.getFileContent("GeneralSettingResponse", ofFileType: "json", afterDelay: 0.0)
            } else {
                response = ServiceMockResponse.getFileContent("ErrorResponse", ofFileType: "json", afterDelay: 0.0)
            }
            
            break
        default:
            break
        }
        return response
    }
    
    func generalSettingRequest(_ request:GeneralSettingRequest) -> GeneralSettingResponse? {
        let serviceResponse: String = self.serviceResponse(forURL: request.url(), with: request)
        _ = self.isSpecificStringAvailable(serviceResponse)
        
        var json = [AnyHashable: Any]()
        if(statusCode == SERVICE_TIME_OUT_CODE) {
            json = self.dictionaryfromJSON(ServiceMockResponse.getFileContent("ServiceTimeOutResponse", ofFileType: "json", afterDelay: 0.0))!
        } else {
            do {
                let data = serviceResponse.data(using: String.Encoding.utf8, allowLossyConversion: false)!
                _ = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
                json = self.dictionaryfromJSON(serviceResponse)!
            } catch let error as NSError {
                json = self.dictionaryfromJSON(ServiceMockResponse.getFileContent("ServiceTimeOutResponse", ofFileType: "json", afterDelay: 0.0))!
                print("Failed to load: \(error.localizedDescription)")
            }
        }
        
        if(IS_LOG_ENABLE == 1) {
//            print(jsonBodyStringFromDic(params: json))
        }
        
        let response: GeneralSettingResponse? = self.generateResponseModal(json)
        return response
    }
}
