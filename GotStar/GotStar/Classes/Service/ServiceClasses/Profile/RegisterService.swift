//
//  RegisterService.swift
//  GotoGarageSale
//
//  Created by Admin on 11/27/18.
//  Copyright © 2018 GotoConcept. All rights reserved.
//

import UIKit

class RegisterService: Service {
    static var instance: RegisterService?
    
    class func getInstance() -> RegisterService {
        if instance == nil {
            instance = RegisterService()
        }
        return instance!
    }
    
    override init() {
        super.init()
        // Initialization code here.
    }
    
    func serviceParams(_ request: RegisterRequest) -> [AnyHashable: Any] {
        var dictionary = [AnyHashable: Any]()
        // Set Password
        if self.hasValue(request.password) {
            dictionary["password"] = request.password
        }
        
        // Set name
        if self.hasValue(request.name) {
            dictionary["fullname"] = request.name
        }
        
        // Set email
        if self.hasValue(request.email) {
            dictionary["email"] = request.email
        }
        
        // Set phone
        if self.hasValue(request.phone) {
            dictionary["mobile_number"] = request.phone
        }
        
        
        return dictionary
    }
    
    func generateResponseModal(_ json: [AnyHashable: Any]?) -> RegisterResponse {
        let response = RegisterResponse()
        
        let status: Int32? = (json!["status"] as? Int32)
        if status ==  STATUS_OK {
        //if self.statusCode == STATUS_OK {
            response.success = true
            if self.hasValue(json) {
                
                let User_id: String? = (json!["User_id"] as? String)
                if hasValue(User_id) {
                    response.User_id = User_id!
                }
                
            }
        }
        else {
            response.success = false
            if self.hasValue(json) {
                response.setErrorJson(json!)
            }
        }
        return response
    }
    
    func serviceResponse(forURL requestUrl: String, with request: RegisterRequest) -> String {
        var response: String = ""
        switch REGISTATION_SERVICE {
        case LIVE_SERVICE:
            let dictionary: [AnyHashable: Any] = self.serviceParams(request)
            if(IS_LOG_ENABLE == 1) {
                print("Register Service:", dictionary)
            }
            response = self.tgService(method: HttpMethod.POST, url: requestUrl, params: dictionary, headers: SessionManager.getInstance().getHeaderDictionary())
            break
        case MOCK_SERVICE:
            self.statusCode = Int(STATUS_OK)
            if(statusCode == STATUS_OK) {
                response = ServiceMockResponse.getFileContent("RegisterResponse", ofFileType: "json", afterDelay: 0.0)
            } else {
                response = ServiceMockResponse.getFileContent("ErrorResponse", ofFileType: "json", afterDelay: 0.0)
            }
            
            break
        default:
            break
        }
        return response
    }
    
    func registerRequest(_ request:RegisterRequest) -> RegisterResponse? {
        let serviceResponse: String = self.serviceResponse(forURL: request.url(), with: request)
        _ = self.isSpecificStringAvailable(serviceResponse)
        
        var json = [AnyHashable: Any]()
        if(statusCode == SERVICE_TIME_OUT_CODE) {
            json = self.dictionaryfromJSON(ServiceMockResponse.getFileContent("ServiceTimeOutResponse", ofFileType: "json", afterDelay: 0.0))!
        } else {
            //json = self.dictionaryfromJSON(serviceResponse)!
            do {
                let data = serviceResponse.data(using: String.Encoding.utf8, allowLossyConversion: false)!
                _ = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
                json = self.dictionaryfromJSON(serviceResponse)!
            } catch let error as NSError {
                json = self.dictionaryfromJSON(ServiceMockResponse.getFileContent("ServiceTimeOutResponse", ofFileType: "json", afterDelay: 0.0))!
                print("Failed to load: \(error.localizedDescription)")
            }
        }
        
        if(IS_LOG_ENABLE == 1) {
            print(jsonBodyStringFromDic(params: json))
        }
        
        let response: RegisterResponse? = self.generateResponseModal(json)
        return response
    }
}
