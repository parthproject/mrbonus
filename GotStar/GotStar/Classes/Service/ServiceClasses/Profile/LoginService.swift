//
//  LoginService.swift
//  GotoGarageSale
//
//  Created by Admin on 01/12/18.
//  Copyright © 2018 GotoConcept. All rights reserved.
//

import UIKit

class LoginService: Service {
    static var instance: LoginService?
    
    class func getInstance() -> LoginService {
        if instance == nil {
            instance = LoginService()
        }
        return instance!
    }
    
    override init() {
        super.init()
        // Initialization code here.
    }
    
    func serviceParams(_ request: LoginRequest) -> [AnyHashable: Any] {
        var dictionary = [AnyHashable: Any]()
        // Set Password
        if self.hasValue(request.password) {
            dictionary["password"] = request.password
        } else {
            dictionary["password"] = ""
        }
        
        // Set username
        if self.hasValue(request.email) {
            dictionary["email"] = request.email
        } else {
            dictionary["email"] = ""
        }
        return dictionary
    }
    
    func generateResponseModal(_ json: [AnyHashable: Any]?) -> LoginResponse {
        let response = LoginResponse()
        
        let status: Int32? = (json!["status"] as? Int32)
        if status ==  STATUS_OK {
        //if self.statusCode == STATUS_OK {
            response.success = true
            if self.hasValue(json) {
                //response.loginModel = loginResponseModelFromDictionary(json!)
                var obj = LoginResponseModel()
                
                // Get User_id
                let User_id: String? = (json!["User_id"] as? String)
                if hasValue(User_id) {
                    obj.User_id = User_id!
                }
                
                let is_premium: String? = (json!["is_premium"] as? String)
                if hasValue(is_premium) {
                    obj.is_premium = is_premium!
                }
                
                response.loginModel = obj
            }
        }
        else {
            response.success = false
            if self.hasValue(json) {
                response.setErrorJson(json!)
            }
        }
        return response
    }
    
    func serviceResponse(forURL requestUrl: String, with request: LoginRequest) -> String {
        var response: String = ""
        switch LOGIN_SERVICE {
        case LIVE_SERVICE:
            let dictionary: [AnyHashable: Any] = self.serviceParams(request)
            if(IS_LOG_ENABLE == 1) {
                print("Login Service:", dictionary)
            }
            response = self.tgService(method: HttpMethod.GET, url: requestUrl, params: dictionary, headers: SessionManager.getInstance().getHeaderDictionary())
            break
        case MOCK_SERVICE:
            self.statusCode = Int(STATUS_OK)
            //self.statusCode = Int(STATUS_BAD_REQUEST)
            if(statusCode == STATUS_OK) {
                response = ServiceMockResponse.getFileContent("LoginResponse", ofFileType: "json", afterDelay: 0.0)
            } else {
                response = ServiceMockResponse.getFileContent("ErrorResponse", ofFileType: "json", afterDelay: 0.0)
            }
            
            break
        default:
            break
        }
        return response
    }
    
    func loginRequest(_ request:LoginRequest) -> LoginResponse? {
        let serviceResponse: String = self.serviceResponse(forURL: request.url(), with: request)
        _ = self.isSpecificStringAvailable(serviceResponse)
        
        var json = [AnyHashable: Any]()
        if(statusCode == SERVICE_TIME_OUT_CODE) {
            json = self.dictionaryfromJSON(ServiceMockResponse.getFileContent("ServiceTimeOutResponse", ofFileType: "json", afterDelay: 0.0))!
        } else {
            //json = self.dictionaryfromJSON(serviceResponse)!
            do {
                let data = serviceResponse.data(using: String.Encoding.utf8, allowLossyConversion: false)!
                _ = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
                json = self.dictionaryfromJSON(serviceResponse)!
            } catch let error as NSError {
                json = self.dictionaryfromJSON(ServiceMockResponse.getFileContent("ServiceTimeOutResponse", ofFileType: "json", afterDelay: 0.0))!
                print("Failed to load: \(error.localizedDescription)")
            }
        }
        
        if(IS_LOG_ENABLE == 1) {
            //print(jsonBodyStringFromDic(params: json))
        }
        
        let response: LoginResponse? = self.generateResponseModal(json)
        return response
    }
}

