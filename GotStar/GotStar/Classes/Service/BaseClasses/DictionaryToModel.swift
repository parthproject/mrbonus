//
//  DictionaryToModel.swift
//  GotoGarageSale
//
//  Created by Admin on 18/12/18.
//  Copyright © 2018 GotoConcept. All rights reserved.
//

import Foundation
import UIKit

func hasValue(_ object: Any?) -> Bool {
    if object == nil {
        return false
    }
    if object != nil && (object as? NSNull) != NSNull() {
        // Check NSString Class
        if (object is String) || (object is String) {
            let str = object as! String
            //            if str.characters.count > 0{
            //                return true
            //            }
            if str.isEmpty || str.count == 0{
                return false
            } else {
                return true
            }
        }
        // Check UIImage Class
        if (object is UIImage) {
            if (object as AnyObject).cgImage != nil {
                return true
            }
            else {
                return false
            }
        }
        // Check NSArray Class
        if (object is [Any]) {
            if (object as AnyObject).count > 0 {
                return true
            }
            else {
                return false
            }
        }
        // Check NSDictionary Class
        if (object is [AnyHashable: Any]) {
            if (object as AnyObject).count > 0 {
                return true
            }
            else {
                return false
            }
        }
        // Check Bool Class
        if (object is Bool) {
            return true
        }
        return true
    }
    return false
}



func bannerModelFromDictionary(_ json: [AnyHashable: Any]) -> BannerModel {
    var obj = BannerModel()
    
    
    // Get tvv_video
    let tvv_video: String? = (json["tvv_video"] as? String)
    if hasValue(tvv_video) {
        obj.tvv_video = tvv_video!
    }
    
    // Get cat_image
    let cat_image: String? = (json["cat_image"] as? String)
    if hasValue(cat_image) {
        obj.cat_image = cat_image!
    }
    
    // Get tvs_name
    let tvs_name: String? = (json["tvs_name"] as? String)
    if hasValue(tvs_name) {
        obj.tvs_name = tvs_name!
    }
    
    // Get tvs_image
    let tvs_image: String? = (json["tvs_image"] as? String)
    if hasValue(tvs_image) {
        obj.tvs_image = tvs_image!
    }
    
    // Get tvv_video_type
    let tvv_video_type: String? = (json["tvv_video_type"] as? String)
    if hasValue(tvv_video_type) {
        obj.tvv_video_type = tvv_video_type!
    }
    
    // Get tvv_video_url
    let tvv_video_url: String? = (json["tvv_video_url"] as? String)
    if hasValue(tvv_video_url) {
        obj.tvv_video_url = tvv_video_url!
    }
    
    // Get v_type
    let v_type: String? = (json["v_type"] as? String)
    if hasValue(v_type) {
        obj.v_type = v_type!
    }
    
    // Get tvs_id
    let tvs_id: String? = (json["tvs_id"] as? String)
    if hasValue(tvs_id) {
        obj.tvs_id = tvs_id!
    }
    
    // Get tvv_web_img
    let tvv_web_img: String? = (json["tvv_web_img"] as? String)
    if hasValue(tvv_web_img) {
        obj.tvv_web_img = tvv_web_img!
    }
    
    // Get tvv_download
    let tvv_download: String? = (json["tvv_download"] as? String)
    if hasValue(tvv_download) {
        obj.tvv_download = tvv_download!
    }
    
    // Get type
    let type: String? = (json["type"] as? String)
    if hasValue(type) {
        obj.type = type!
    }
    
    // Get c_status
    let c_status: String? = (json["c_status"] as? String)
    if hasValue(c_status) {
        obj.c_status = c_status!
    }
    
    // Get cat_name
    let cat_name: String? = (json["cat_name"] as? String)
    if hasValue(cat_name) {
        obj.cat_name = cat_name!
    }
    
    // Get tvv_description
    let tvv_description: String? = (json["tvv_description"] as? String)
    if hasValue(tvv_description) {
        obj.tvv_description = tvv_description!
    }
    
    // Get tvv_id
    let tvv_id: String? = (json["tvv_id"] as? String)
    if hasValue(tvv_id) {
        obj.tvv_id = tvv_id!
    }
    
    // Get tvv_view
    let tvv_view: String? = (json["tvv_view"] as? String)
    if hasValue(tvv_view) {
        obj.tvv_view = tvv_view!
    }
    
    // Get tvv_date
    let tvv_date: String? = (json["tvv_date"] as? String)
    if hasValue(tvv_date) {
        obj.tvv_date = tvv_date!
    }
    
    // Get fc_id
    let fc_id: String? = (json["fc_id"] as? String)
    if hasValue(fc_id) {
        obj.fc_id = fc_id!
    }
    
    // Get tvs_date
    let tvs_date: String? = (json["tvs_date"] as? String)
    if hasValue(tvs_date) {
        obj.tvs_date = tvs_date!
    }
    
    // Get tvs_view
    let tvs_view: String? = (json["tvs_view"] as? String)
    if hasValue(tvs_view) {
        obj.tvs_view = tvs_view!
    }
    
    // Get c_id
    let c_id: String? = (json["c_id"] as? String)
    if hasValue(c_id) {
        obj.c_id = c_id!
    }
    
    // Get c_date
    let c_date: String? = (json["c_date"] as? String)
    if hasValue(c_date) {
        obj.c_date = c_date!
    }
    
    // Get tvv_thumbnail
    let tvv_thumbnail: String? = (json["tvv_thumbnail"] as? String)
    if hasValue(tvv_thumbnail) {
        obj.tvv_thumbnail = tvv_thumbnail!
    }
    
    // Get tvv_name
    let tvv_name: String? = (json["tvv_name"] as? String)
    if hasValue(tvv_name) {
        obj.tvv_name = tvv_name!
    }
    
    // Get ftvs_id
    let ftvs_id: String? = (json["ftvs_id"] as? String)
    if hasValue(ftvs_id) {
        obj.ftvs_id = ftvs_id!
    }
    
    // Get is_premium
    let is_premium: String? = (json["is_premium"] as? String)
    if hasValue(is_premium) {
        obj.is_premium = is_premium!
    }
    
    //get w_id
    let w_id: String? = (json["w_id"] as? String)
    if hasValue(w_id) {
        obj.w_id = w_id!
    }
    
    return obj
}

func categoryrModelFromDictionary(_ json: [AnyHashable: Any]) -> CategoryrModel {
    var obj = CategoryrModel()
    
    
    // Get cat_image
    let cat_image: String? = (json["cat_image"] as? String)
    if hasValue(cat_image) {
        obj.cat_image = cat_image!
    }
    
    // Get cat_name
    let cat_name: String? = (json["cat_name"] as? String)
    if hasValue(cat_name) {
        obj.cat_name = cat_name!
    }
    
    // Get c_date
    let c_date: String? = (json["c_date"] as? String)
    if hasValue(c_date) {
        obj.c_date = c_date!
    }
    
    // Get c_status
    let c_status: String? = (json["c_status"] as? String)
    if hasValue(c_status) {
        obj.c_status = c_status!
    }
    
    // Get c_id
    let c_id: String? = (json["c_id"] as? String)
    if hasValue(c_id) {
        obj.c_id = c_id!
    }
    
    return obj
}

func generalSettingModelFromDictionary(_ json: [AnyHashable: Any]) -> GeneralSettingModel {
    var obj = GeneralSettingModel()
    
    // Get id
    let id: String? = (json["id"] as? String)
    if hasValue(id) {
        obj.id = id!
    }
    
    // Get key
    let key: String? = (json["key"] as? String)
    if hasValue(key) {
        obj.key = key!
    }
    
    // Get value
    let value: String? = (json["value"] as? String)
    if hasValue(value) {
        obj.value = value!
    }
    
    return obj
}

func channelModelFromDictionary(_ json: [AnyHashable: Any]) -> ChannelModel {
    var obj = ChannelModel()
    
    // Get is_premium
    let is_premium: String? = (json["is_premium"] as? String)
    if hasValue(is_premium) {
        obj.is_premium = is_premium!
    }
    
    // Get status
    let status: String? = (json["status"] as? String)
    if hasValue(status) {
        obj.status = status!
    }
    
    // Get id
    let id: String? = (json["id"] as? String)
    if hasValue(id) {
        obj.id = id!
    }
    
    // Get channel_name
    let channel_name: String? = (json["channel_name"] as? String)
    if hasValue(channel_name) {
        obj.channel_name = channel_name!
    }
    
    // Get channel_desc
    let channel_desc: String? = (json["channel_desc"] as? String)
    if hasValue(channel_desc) {
        obj.channel_desc = channel_desc!
    }
    
    // Get channel_view
    let channel_view: String? = (json["channel_view"] as? String)
    if hasValue(channel_view) {
        obj.channel_view = channel_view!
    }
    
    // Get channel_url
    let channel_url: String? = (json["channel_url"] as? String)
    if hasValue(channel_url) {
        obj.channel_url = channel_url!
    }
    
    // Get channel_image
    let channel_image: String? = (json["channel_image"] as? String)
    if hasValue(channel_image) {
        obj.channel_image = channel_image!
    }
    
    return obj
}



