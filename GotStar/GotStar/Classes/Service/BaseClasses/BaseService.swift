//
//  BaseService.swift
//  GotoLiquorStoreOwneriOS
//
//  Created by Admin on 25/12/18.
//  Copyright © 2018 Kuldip Patel. All rights reserved.
//

import Foundation
enum HttpMethod: String {
    case POST = "POST"
    case GET = "GET"
    case PUT = "PUT"
    case DELETE = "DELETE"
}
public var BUNDLEID = "BundleId"
public var APPLICATION_JSON = "application/json"
public var CONTENT_LENGTH = "Content-Length"
public var ACCEPT = "Accept"
public var CONTENT_TYPE = "Content-Type"



class Service: NSObject {
    
    var statusCode :Int = 0
    var TIME_OUT_INTERVAL = 60
    var task = URLSessionDataTask()
    
    //TGUtil object
    override init() {
        super.init()
    }
    
    func isSpecificStringAvailable(_ string: String) -> Void{
        if(string.contains("<!DOCTYPE html>")) {
            statusCode = Int(SERVICE_TIME_OUT_CODE)
        }
    }
    
    /**
     * Generic Service Call
     * @param method String
     * @param url String
     * @param params [AnyHashable: Any]
     * @return String
     */
    func tgService(method: HttpMethod, url: String, params: [AnyHashable: Any]?, headers: [AnyHashable: Any]?) -> String {
        var responseString = ""
        if(method == HttpMethod.POST) {
//            responseString = serviceResponseByPost(url, params, headers)
            responseString = serviceResponseByPost(requestUrl: url, params: params, headers: headers, isJsonBodyRequest: false)
        } else if(method == HttpMethod.GET){
            responseString = serviceResponseByGet(url, params, headers)
        } else if(method == HttpMethod.DELETE){
            responseString = serviceResponseByDelete(url, params, headers)
        }
        else {
            responseString = serviceResponseByPut(url, params, headers)
        }
        return responseString
    }
    
    // MARK: Get with params - Dictionary
    /**
     * Service Response By Get Dictionary
     * @param requestUrl String
     * @param dictionary [AnyHashable: Any]
     * @return String
     */
    func serviceResponseByGet(_ requestUrl: String,_ params: [AnyHashable: Any]?,_ headers: [AnyHashable: Any]?) -> String {
        var url: String = requestUrl
        if params != nil {
            let httpString = paramString(params: params!)
            //let httpString = self.JSONStingFromArray(params!)
            print("Request: \(requestUrl)", httpString)
            
            if (httpString == "") == false {
                url = "\(requestUrl)?\(httpString)"
            }
        }
        
        var urlRequest: URLRequest = createRequest(url) as URLRequest
        urlRequest.httpMethod = HttpMethod.GET.rawValue
        if headers != nil {
            for (key,value) in headers! {
                urlRequest.setValue(value as? String, forHTTPHeaderField: key as! String)
            }
        }
        let response: String = createResponse(urlRequest)
        if self.hasValue(response) {
            return addKeyParameterIntoJsonString(jsonString: response)
        }
        else {
            return response
        }
    }
    
    func serviceResponseByPut(_ requestUrl: String,_ params: [AnyHashable: Any]?,_ headers: [AnyHashable: Any]?) -> String {
        var httpString:String = ""
        if params != nil {
            //httpString = paramString(params: params!)
            httpString = self.JSONStingFromArray(params!)
            print("Request: \(requestUrl)", httpString)
        }
        
        var urlRequest: URLRequest = createRequest(requestUrl) as URLRequest
        urlRequest.httpMethod = HttpMethod.PUT.rawValue
        if params != nil {
            urlRequest.httpBody = httpString.data(using: String.Encoding.utf8)
        }
        
        if headers != nil {
            for (key,value) in headers! {
                urlRequest.setValue(value as? String, forHTTPHeaderField: key as! String)
            }
        }
        
        let response: String = createResponse(urlRequest)
        if self.hasValue(response) {
            return addKeyParameterIntoJsonString(jsonString: response)
        }
        else {
            return response
        }
    }
    
    func serviceResponseByDelete(_ requestUrl: String,_ params: [AnyHashable: Any]?,_ headers: [AnyHashable: Any]?) -> String {
        var httpString:String = ""
        if params != nil {
            //httpString = paramString(params: params!)
            httpString = self.JSONStingFromArray(params!)
            print("Request: \(requestUrl)", httpString)
        }
        
        var urlRequest: URLRequest = createRequest(requestUrl) as URLRequest
        urlRequest.httpMethod = HttpMethod.DELETE.rawValue
        if params != nil {
            urlRequest.httpBody = httpString.data(using: String.Encoding.utf8)
        }
        
        if headers != nil {
            for (key,value) in headers! {
                urlRequest.setValue(value as? String, forHTTPHeaderField: key as! String)
            }
        }
        
        let response: String = createResponse(urlRequest)
        if self.hasValue(response) {
            return addKeyParameterIntoJsonString(jsonString: response)
        }
        else {
            return response
        }
    }
    
    // MARK: Post Dictionary Method
    /**
     * Service Response By Post Dictionary
     * @param requestUrl String
     * @param dictionary [AnyHashable: Any]
     * @return String
     */
    func serviceResponseByPost(_ requestUrl: String,_ params: [AnyHashable: Any]?,_ headers: [AnyHashable: Any]?) -> String {
        var httpString:String = ""
        if params != nil {
            //httpString = paramString(params: params!)
            httpString = self.JSONStingFromArray(params!)
            print("Request: \(requestUrl)", httpString)
        }
        
        var urlRequest: URLRequest = createRequest(requestUrl) as URLRequest
        urlRequest.httpMethod = HttpMethod.POST.rawValue
        if params != nil {
            urlRequest.httpBody = httpString.data(using: String.Encoding.utf8)
        }
        
        if headers != nil {
            for (key,value) in headers! {
                urlRequest.setValue(value as? String, forHTTPHeaderField: key as! String)
            }
        }
        return createResponse(urlRequest)
    }
    
    
    // MARK: POST  JSON Method
    /**
     * Service Response By Post JSON
     * @param requestUrl String
     * @param jsonParameter String
     * @return String
     */
    func serviceResponseByPostJSON(_ requestUrl: String,_ jsonString: String) -> String {
        let jsonParameter = addCommonRequestParametersIntoJSONBody(jsonString)
        let requestData = jsonParameter.data(using: String.Encoding.utf8, allowLossyConversion: false)!
        
        var urlRequest: URLRequest? = createRequest(requestUrl) as URLRequest
        urlRequest?.httpMethod = HttpMethod.POST.rawValue
        urlRequest?.setValue("\(UInt(requestData.count))", forHTTPHeaderField: CONTENT_LENGTH)
        urlRequest?.setValue(APPLICATION_JSON, forHTTPHeaderField: ACCEPT)
        urlRequest?.setValue(APPLICATION_JSON, forHTTPHeaderField: CONTENT_TYPE)
        urlRequest?.httpBody = requestData
        let response: String = createResponse(urlRequest!)
        return response
    }
    
    // MARK: Post Dictionary Method
    /**
     * Service Response By Post Dictionary
     * @param requestUrl String
     * @param dictionary [AnyHashable: Any]
     * @return String
     */
    func serviceResponseByPost( requestUrl: String, params: [AnyHashable: Any]?, headers: [AnyHashable: Any]?, isJsonBodyRequest: Bool) -> String {
        
        var urlRequest: URLRequest = createRequest(requestUrl) as URLRequest
        urlRequest.httpMethod = HttpMethod.POST.rawValue
        
        if params != nil {
            var httpString:String = ""
            if(isJsonBodyRequest) {
                httpString = jsonBodyStringFromDic(params: params!)
                 print("Request: \(requestUrl)", httpString)
                urlRequest.setValue(APPLICATION_JSON, forHTTPHeaderField: CONTENT_TYPE)
            } else {
                httpString = keyValueStringFromDic(params:params!)
            }
            urlRequest.httpBody = httpString.data(using: String.Encoding.utf8)
        }
        
        if headers != nil {
            for (key,value) in headers! {
                urlRequest.setValue(value as? String, forHTTPHeaderField: key as! String)
            }
        }
        
        let response: String = createResponse(urlRequest)
        if self.hasValue(response) {
            return addKeyParameterIntoJsonString(jsonString: response)
        }
        
        return response
    }
    
    /**
     * Get keyValueString
     * @param params [AnyHashable: Any]
     * @return String
     */
    func keyValueStringFromDic(params: [AnyHashable: Any]) -> String {
        var httpString = String()
        let keyArray: [Any] = params.flatMap(){ $0.0 as? String }
        for key in keyArray {
            let value: Any = params[String(describing: key)]!
            httpString = httpString + ("\(key)=\(value)&")
        }
        if (httpString.count) > 0 {
            httpString = httpString.substring(to: httpString.index(httpString.startIndex, offsetBy: (httpString.count ) - 1))
        }
        return httpString
    }
    
    /**
     * Get paramString
     * @param params [AnyHashable: Any]
     * @return String
     */
    func paramString(params: [AnyHashable: Any]) -> String {
        var httpString = String()
        let keyArray: [Any] = params.flatMap(){ $0.0 as? String }
        for key in keyArray {
            let value: Any = params[String(describing: key)]!
            httpString = httpString + ("\(key)=\(value)&")
        }
        if (httpString.count) > 0 {
            httpString = httpString.substring(to: httpString.index(httpString.startIndex, offsetBy: (httpString.count ) - 1))
        }
        return httpString
    }
    
    /**
     * Create URLRequest
     *
     * @param requestUrl String
     */
    func createRequest(_ requestUrl: String) -> NSMutableURLRequest {
        let url = URL(string: requestUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)
        let urlRequest = NSMutableURLRequest(url: url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: TimeInterval(TIME_OUT_INTERVAL))
        return urlRequest
    }
    
    // MARK: Supporting Method ------------------------------
    func mid<T: Comparable>(array: [T]) -> T? {
        guard !array.isEmpty else { return nil }
        return array.sorted()[(array.count - 1) / 2]
    }
    
    /**
     * Create Response
     * @param urlRequest URLRequest
     * @return String
     */
    func createResponse(_ urlRequest: URLRequest) -> String {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        var resultJson = ""
        let semaphore = DispatchSemaphore(value: 0)
        task = session.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
            
            let res = response as? HTTPURLResponse
            self.statusCode = res?.statusCode ?? Int(SERVICE_TIME_OUT_CODE)
            //self.statusCode =  Int(SERVICE_TIME_OUT_CODE)
            
            if let data = data {
                resultJson = String(data: data, encoding: String.Encoding.utf8)!
            }
            semaphore.signal()
        })
        task.resume()
        _ = semaphore.wait(timeout: DispatchTime.distantFuture)
        return resultJson
    }
    
    /**
     * Convert String to Dictionary
     * @param body String
     * @return Dictionary
     */
    func convertJsonToDic(body: String) -> Dictionary<String, Any>? {
        var dictonary:Dictionary<String, Any>?
        if let data = body.data(using: String.Encoding.utf8) {
            do {
                dictonary = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
            } catch let error as NSError {
                print(error)
            }
        }
        return dictonary
    }
    
    /**
     * Add key parameter in JsonString if not present
     * @param jsonString String
     * @return String
     */
    func addKeyParameterIntoJsonString(jsonString: String) -> String {
        var body = jsonString
        let firstChar = body[body.index(body.startIndex, offsetBy: 0)]
        let lastChar = body[body.index(before: body.endIndex)]
        
        if(firstChar == "[" && lastChar == "]") {
            body = body.replacingCharacters(in: body.startIndex..<body.startIndex, with: "{\"list\":")
            body = body + "}"
        }
        
        /*if(jsonString == "Deposit action not found") {
         body = "{\"message\":" + "\"Deposit action not found\"" + "}"
         }*/
        
        return body
    }
    
    /**
     * Convert Dictionary to JSON
     * @param dictionary Dictionary
     * @return String - formatted string
     */
    func jsonRepresentation(_ dictionary: Dictionary<String, Any>) -> String {
        var jsonString = String()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: [])
            if  let dictFromJSON = decoded as? [String:String] {
                jsonString = String(describing: dictFromJSON)
            }
        } catch {
            print(error.localizedDescription)
        }
        return jsonString
    }
    
    /**
     * Add RequestParameters Into JSONBody
     * @param body String
     * @return String - formatted string
     */
    func addCommonRequestParametersIntoJSONBody(_ body: String) -> String {
        var body = body
        if (!self.stringHasValue(body)) {
            body = "{}"
        }
        var bodyParameters = dictionaryfromJSON(body)
        let appBundle = Bundle.main
        let bundleID: String? = appBundle.bundleIdentifier
        bodyParameters?[BUNDLEID] = bundleID
        let jsonBody: String? = jsonRepresentation(bodyParameters!)
        return jsonBody!
    }
    
    /**
     * Get jsonString
     * @param params [AnyHashable: Any]
     * @return String
     */
    func jsonBodyStringFromDic(params: [AnyHashable: Any]) -> String {
        if let json = try? JSONSerialization.data(withJSONObject: params, options: []) {
            if let bodyString = String(data: json, encoding: String.Encoding.utf8) {
                return bodyString
            }
        }
        return ""
    }
    
    /**
     * Add JSONStingFromArray Into String
     * @param object Any
     * @return String - formatted string
     */
    public func JSONStingFromArray(_ object: Any) -> String {
        var jsonString = String()
        do {
            //Convert to Data
            let jsonData = try JSONSerialization.data(withJSONObject: object, options: JSONSerialization.WritingOptions.prettyPrinted)
            //Convert back to string. Usually only do this for debugging
            if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                jsonString = JSONString
            }
        } catch {
            print(error.localizedDescription)
        }
        
        return jsonString
    }
    
    /**
     * Convert JSON to Dictionary
     * @param body String
     * @return Dictionary
     */
    public func dictionaryfromJSON(_ body: String) -> Dictionary<String, Any>? {
        var body = body.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if (!self.stringHasValue(body)) {
            body = "{}"
        }
        
        var dictonary:Dictionary<String, Any>?
        dictonary = convertJsonToDic(body: body)
        
        if(dictonary == nil) {
            body = addKeyParameterIntoJsonString(jsonString: body)
            dictonary = convertJsonToDic(body: body)
        }
        return dictonary
    }
    
    func contentType(forImageData data: Data) -> (String, String) {
        var c = [UInt32](repeating: 0, count: 1)
        (data as NSData).getBytes(&c, length: 1)
        switch (c[0]) {
        case 0xff:
            return ("image/jpeg","jpeg")
        case 0x89:
            return ("image/png","png")
        case 0x47:
            return ("image/gif","gif")
        case 0x49, 0x4d:
            return ("image/tiff","tiff")
        default:
            return ("video/mp4","mp4")
        }
    }
    
    // MARK: - Value Check Methods
    func stringHasValue(_ string: String) -> Bool {
        if string.isEmpty {
            return false
        } else {
            return true
        }
    }
    func arrayHasValue(_ array: [Any]) -> Bool {
        if array.isEmpty {
            return false
        } else {
            return true
        }
    }
    func dictionaryHasValue(_ dictionary: [AnyHashable: Any]) -> Bool {
        if dictionary.isEmpty {
            return false
        } else {
            return true
        }
    }
    func dictionaryHasKey(_ dictionary: [AnyHashable: Any]) -> Bool {
        if dictionary.isEmpty {
            return false
        } else {
            return true
        }
    }
    
    func hasValue(_ object: Any?) -> Bool {
        if object == nil {
            return false
        }
        if object != nil && (object as? NSNull) != NSNull() {
            // Check NSString Class
            if (object is String) || (object is String) {
                let str = object as! String
                if str.count > 0{
                    return true
                }
                if str.isEmpty {
                    return false
                }
            }
            // Check UIImage Class
            if (object is UIImage) {
                if (object as AnyObject).cgImage != nil {
                    return true
                }
                else {
                    return false
                }
            }
            // Check NSArray Class
            if (object is [Any]) {
                if (object as AnyObject).count > 0 {
                    return true
                }
                else {
                    return false
                }
            }
            // Check NSDictionary Class
            if (object is [AnyHashable: Any]) {
                if (object as AnyObject).count > 0 {
                    return true
                }
                else {
                    return false
                }
            }
            // Check Bool Class
            if (object is Bool) {
                return true
            }
            return true
        }
        return false
    }
    
    func imageFromURL(_ url:String  ) -> String {
        let urlString:String? = url.replacingOccurrences(of: " ", with: "%20")
        let path:String = self.getCachePath()
        let imageUrl:String? = urlString
        var data:Data? = CacheManager.getInstance().callSynchronousCachedRequest(imageUrl ?? "")
        let suffix:String? = URL(fileURLWithPath: imageUrl!).lastPathComponent.components(separatedBy: ".").last?.lowercased()
        
        if ((("jpeg" == suffix) || ("png" == suffix) || ("jpg" == suffix)) && hasValue(data)){
            let image:UIImage? = UIImage(data: data ?? Data())
            if image != nil {
                data = UIImage(data: data!)!.pngData()
            }
        }
        else {
            return url
        }
        let lastPath : String = URL(fileURLWithPath: imageUrl ?? "").lastPathComponent
        let cacheFilePath: URL = URL(fileURLWithPath: path).appendingPathComponent(lastPath)
        do {
            try data?.write(to:cacheFilePath, options: .atomic)
        } catch {}
        return url
    }
    
    
    func getCachePath() -> String {
        let cacheDirectory: String = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0]
        return "\(cacheDirectory)\("/GoSurvey/ImageData")"
        //        var cacheDirectory: String = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0]
        //        return "\(cacheDirectory)\("/GoSurvey/ImageData")"
    }
    
    func downloadVideoAudio(_ urlString : String?) -> String?
    {
        //let urlString = "http://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_1mb.mp4"
        //let downloadUrlString = urlString
        print("downloadVideo");
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let fileName = urlString! as NSString;
        let filePath="\(documentsPath)/\(fileName.lastPathComponent)";
        let fileExists = FileManager().fileExists(atPath: filePath)
        if(fileExists){
            //print("Already videoSaved");
        }else{
            let url = NSURL(string: urlString!)
            let urlData = NSData(contentsOf: url! as URL)
            if(hasValue(urlData))
            {
                //download
                DispatchQueue.main.async(execute: { () -> Void in
                    //print(filePath)
                    urlData?.write(toFile: filePath, atomically: true)
                    //print("videoSaved")
                })
            }else {
                print("Url is not valid")
            }
        }
        return urlString
    }
    
    // MARK: Multipart Method
    /**
     * Service serviceResponseByMultipart
     * @param method HttpMethod
     * @param requestUrl [AnyHashable: Any]
     * @param params [AnyHashable: Any]
     * @param multipartParams [AnyHashable: Any]
     * @param headers [AnyHashable: Any]
     * @param mimeType String > image/jpg
     * @return String
     */
    //https://newfivefour.com/swift-form-data-multipart-upload-URLRequest.html
    
    public func tgRequestMultipart(method: HttpMethod, requestUrl: String, params: [AnyHashable: Any]?, multipartParams: [AnyHashable: Any]?, headers: [AnyHashable: Any]?) -> String {
        
        let cgiUrl = URL(string: requestUrl)
        var urlRequest = URLRequest(url: cgiUrl!)
        
        if headers != nil {
            for (key,value) in headers! {
                urlRequest.setValue(value as? String, forHTTPHeaderField: key as! String)
            }
        }
        
        let boundary = "Boundary-\(UUID().uuidString)"
        urlRequest.httpMethod = method.rawValue
        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        urlRequest.httpBody = createBody(parameters: params!,
                                         boundary: boundary,
                                         multipartParams: multipartParams)
        return createResponse(urlRequest as URLRequest)
    }
    
    func createBody(parameters: [AnyHashable: Any],
                    boundary: String,
                    multipartParams: [AnyHashable: Any]?) -> Data {
        let body = NSMutableData()
        let boundaryPrefix = "--\(boundary)\r\n"
        
        for (key, value) in parameters {
            if ((key as! String) == "storeId") {
                body.appendString(boundaryPrefix)
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
        }
        
        body.appendString(boundaryPrefix)
        //        body.appendString("Content-Disposition: form-data; name=\"file\"; filename=\"\(parameters["saleItemId"] as! Int32)).jpeg\"\r\n")
        body.appendString("Content-Disposition: form-data; name=\"file\"; filename=\"image_name.jpeg\"\r\n")
        
        body.appendString("Content-Type: image/jpeg\r\n\r\n")
        if hasValue(parameters["file"]) {
            body.append((parameters["file"] as? Data)!)
        }
        body.appendString("\r\n")
        body.appendString("--".appending(boundary.appending("--")))
        return body as Data
    }
}

extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}
