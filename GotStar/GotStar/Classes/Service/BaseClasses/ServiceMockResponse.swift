//
//  ServiceMockResponse.swift
//  GotoLiquorStoreOwneriOS
//
//  Created by Admin on 20/01/19.
//  Copyright © 2019 Kuldip Patel. All rights reserved.
//

import Foundation

let MOCK_RESPONSE_WAIT_ENABLED = true
let DEFAULT_WAIT_SECONDS = 1.0

var totalWaitSeconds: Float = 0.0
var waitUntilFinish: Bool = false

class ServiceMockResponse: NSObject {
    override init() {
        super.init()
        // Custom Initialization Code
    }
    // MARK: - Utility Methods
    ///*
    func params(_ url: String) -> [AnyHashable: Any] {
        let params = [AnyHashable: Any]()
        return params
    }
    
    @objc func autoLoadFinish() {
        waitUntilFinish = false
    }
    
    class public func getFileContent(_ fileName: String, ofFileType fileType: String, afterDelay seconds: Float) -> String {
        let filePath: String? = Bundle.main.path(forResource: fileName, ofType: fileType)
        var encoding: String.Encoding = String.Encoding.utf8
        //let fileContent = try? String(contentsOfFile: filePath!, usedEncoding: &encoding)
        let fileContent = try? String(contentsOfFile: filePath!, usedEncoding: &encoding)
        //waitBeforeGivingResponse(forSeconds: seconds)
        
        if MOCK_RESPONSE_WAIT_ENABLED && seconds > 0.0 {
            totalWaitSeconds = seconds
            waitUntilFinish = true
            // Schedule Auto Load Finish Timer
            self.perform(#selector(self.autoLoadFinish), with: nil, afterDelay: TimeInterval(totalWaitSeconds))//Hide
            while waitUntilFinish {
                // Wait until Asynchronus Response is received
                //RunLoop.current.run(mode: RunLoopMode.defaultRunLoopMode, before: Date.distantFuture)
                RunLoop.current.run(mode: RunLoop.Mode.default, before: Date.distantFuture)
            }
        }
        return fileContent!
    }
    
    func serviceResponse(_ url: String) -> String {
        //   NSLog(@"Override this method for particular mock service response.");
        return ""
    }
}
