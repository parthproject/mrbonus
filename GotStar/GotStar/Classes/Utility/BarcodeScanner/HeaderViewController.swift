import UIKit

/// Delegate to handle touch event of the close button.
protocol HeaderViewControllerDelegate: class {
    func headerViewControllerDidTapCloseButton(_ controller: HeaderViewController)
    func headerViewControllerDidTapBarCodeButton(_ controller: HeaderViewController, code:String)
}

/// View controller with title label and close button.
/// It will be added as a child view controller if `BarcodeScannerController` is being presented.
public final class HeaderViewController: UIViewController {
  weak var delegate: HeaderViewControllerDelegate?

  // MARK: - UI properties

  /// Header view with title label and close button.
  public private(set) lazy var navigationBar: UINavigationBar = self.makeNavigationBar()
  /// Title view of the navigation bar.
  public private(set) lazy var titleLabel: UILabel = self.makeTitleLabel()
  /// Left bar button item of the navigation bar.
  public private(set) lazy var closeButton: UIButton = self.makeCloseButton()
    
    /// Right bar button item of the navigation bar.
    public private(set) lazy var barCodeButton: UIButton = self.makeBarCodeButton()
    
  // MARK: - View lifecycle

  public override func viewDidLoad() {
    super.viewDidLoad()

    navigationBar.delegate = self
    closeButton.addTarget(self, action: #selector(handleCloseButtonTap), for: .touchUpInside)
    barCodeButton.addTarget(self, action: #selector(handleBarCodeButtonTap), for: .touchUpInside)

    view.addSubview(navigationBar)
    setupConstraints()
  }

  // MARK: - Actions

  @objc private func handleCloseButtonTap() {
    delegate?.headerViewControllerDidTapCloseButton(self)
  }
    
    @objc private func handleBarCodeButtonTap() {
        let alertController = UIAlertController(title: "Enter UPC Code", message: "", preferredStyle: UIAlertController.Style.alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter UPC Code"
            textField.keyboardType = .asciiCapableNumberPad
        }
        let saveAction = UIAlertAction(title: "Search", style: UIAlertAction.Style.default, handler: { alert -> Void in
            let textCode = alertController.textFields![0] as UITextField
            if textCode.text?.count ?? 0 > 0 {
                self.delegate?.headerViewControllerDidTapBarCodeButton(self,code: textCode.text ?? "")
            }else {
                self.showAlertView(title: ALERT, message: "Please entered valid UPC code")
            }
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in })
        
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        
        self.present(alertController, animated: true, completion: nil)
        //delegate?.headerViewControllerDidTapCloseButton(self)
    }
    
    //MARK : AlertMessage
    private func showAlertView(title: String, message: String) {
        //DispatchQueue.main.async {
            var stringMessage = message
        if (title.count == 0 && message.count == 0) || (title.count > 0 && message.count == 0) {
            stringMessage = "Unknown error occurred"
        }
            let alert = UIAlertController(title: title, message: stringMessage, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        //}
    }

  // MARK: - Layout

  private func setupConstraints() {
    NSLayoutConstraint.activate(
      navigationBar.leadingAnchor.constraint(equalTo: view.leadingAnchor),
      navigationBar.trailingAnchor.constraint(equalTo: view.trailingAnchor)
    )

    if #available(iOS 11, *) {
      navigationBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
    } else {
      navigationBar.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor).isActive = true
    }
  }
}


// MARK: - Subviews factory

private extension HeaderViewController {
  func makeNavigationBar() -> UINavigationBar {
    let navigationBar = UINavigationBar()
    navigationBar.isTranslucent = false
    navigationBar.backgroundColor = .white
    navigationBar.items = [makeNavigationItem()]
    return navigationBar
  }

  func makeNavigationItem() -> UINavigationItem {
    let navigationItem = UINavigationItem()
    closeButton.sizeToFit()
    navigationItem.leftBarButtonItem = UIBarButtonItem(customView: closeButton)
    barCodeButton.sizeToFit()
    navigationItem.rightBarButtonItem = UIBarButtonItem(customView: barCodeButton)
    titleLabel.sizeToFit()
    navigationItem.titleView = titleLabel
    return navigationItem
  }

  func makeTitleLabel() -> UILabel {
    let label = UILabel()
    label.text = localizedString("Scan Barcode")
    label.font = UIFont.boldSystemFont(ofSize: 17)
    label.textColor = .black
    label.numberOfLines = 1
    label.textAlignment = .center
    return label
  }

  func makeCloseButton() -> UIButton {
    let button = UIButton(type: .system)
    button.setTitle(localizedString("Close"), for: UIControl.State())
    button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
    //button.tintColor = .black
    return button
  }
    
    func makeBarCodeButton() -> UIButton {
        let button = UIButton(type: .system)
        button.setTitle(localizedString("UPC Code"), for: UIControl.State())
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        //button.tintColor = .black
        return button
    }
}

// MARK: - UINavigationBarDelegate

extension HeaderViewController: UINavigationBarDelegate {
  public func position(for bar: UIBarPositioning) -> UIBarPosition {
    return .topAttached
  }
}
