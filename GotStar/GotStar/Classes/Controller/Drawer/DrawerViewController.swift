//
//  DrawerViewController.swift
//  GoSurveyiOS
//
//  Created by Admin on 21/06/17.
//  Copyright © 2017 Admin. All rights reserved.
//

//https://github.com/topics/drawer?l=swift

import Foundation
import UIKit

class DrawerViewController: BaseViewController {
    var centerNavigationController: UINavigationController?
    var tabBarController2: UITabBarController?
    
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var infoViewHeightConstraint: NSLayoutConstraint!
    let INFO_VIEW_HEIGHT = 70
    let SCROLL_CONTENT_VIEW_HEIGHT = 527//474//633
    let MY_VIEW_HEIGHT = 222
    let SIGN_OUT_HEIGHT = 63
    
    //@IBOutlet weak var loginView: UIView!
    //@IBOutlet weak var loginViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var drawerContentView: UIView!
    @IBOutlet weak var drawerContentViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var drawerContentViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet var scrollView: UIScrollView?
    @IBOutlet weak var scrollViewTopConstraint: NSLayoutConstraint!
    @IBOutlet var scrollContentView: UIView?
    @IBOutlet weak var scrollContentViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var userNameLbl: UILabel!

    @IBOutlet weak var loginBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        navigationController?.navigationBar.barStyle = .black
        //        navigationController?.navigationBar.barTintColor = UIColor.black
        //        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.systemYellow]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.layoutIfNeeded()
        
        if(session.isUserLogin) {

            userNameLbl.text = session.profileModel.fullname
            loginBtn.setTitle("LogOut", for: .normal)
        } else {

            userNameLbl.text = "Enjoy with your Favorite Apps"
            loginBtn.setTitle("Login", for: .normal)
        }
        //versionLbl.text = "v\(String().getVersionString())"
        setSafeArea()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        print(self.drawerContentView.frame)
    }
    
    
    func setSafeArea() {
        /*
         if #available(iOS 11.0, *) {
         print(UIApplication.shared.statusBarFrame.height)
         
         let window = UIApplication.shared.keyWindow
         let topPadding = window?.safeAreaInsets.top
         let bottomPadding = window?.safeAreaInsets.bottom
         
         infoViewHeightConstraint.constant = CGFloat(INFO_VIEW_HEIGHT) + topPadding!
         //drawerContentViewTopConstraint.constant = 0
         
         self.view.layoutIfNeeded()
         //drawerContentViewBottomConstraint.constant = -bottomPadding!
         self.view.layoutIfNeeded()
         
         scrollViewTopConstraint.constant = infoViewHeightConstraint.constant
         
         self.view.layoutIfNeeded()
         }
         */
    }
   
   
   
       func loadData2() {
           
          
           
           let strURL = session.generalSettingDic["contact"]
           let url2 = URL(string: strURL!)
           let vc = WebViewController(nibName: "WebViewController", bundle: nil)
                         vc.urls = url2
                         //        vc.isTVTab = true
                                 centerNavigationController?.pushViewController(vc, animated: true)
                             
           
          

           
       }
    
    
    @IBAction func drawerClicked(_ sender: Any) {
        
        if((sender as AnyObject).tag == 1) {
            //let vc = HomeViewController(nibName: "HomeViewController", bundle: nil)
            //vc.isFromDrawerView = true
            //centerNavigationController?.pushViewController(vc, animated: true)
            //            centerNavigationController?.navigationController?.tabBarController?.selectedIndex = 0
            tabBarController2?.selectedIndex = 0
        }
        else  if((sender as AnyObject).tag == 2) {
             let vc = ChannelsViewController(nibName: "ChannelsViewController", bundle: nil)
             centerNavigationController?.pushViewController(vc, animated: true)
        }
        else  if((sender as AnyObject).tag == 3) {
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.initAndShowUnity()
         

        }
        else  if((sender as AnyObject).tag == 4) {
            if(session.isUserLogin) {
                let vc = WishListViewController(nibName: "WishListViewController", bundle: nil)
                vc.drawerTitle = "WatchList"
                centerNavigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = LoginViewController(nibName: "LoginViewController", bundle: nil)
                centerNavigationController?.pushViewController(vc, animated: true)
            }
        }
        else  if((sender as AnyObject).tag == 5) {
            tabBarController2?.selectedIndex = 4
        }
        else  if((sender as AnyObject).tag == 6) {
            loadData2()
        }
        else  if((sender as AnyObject).tag == 7) {
            if(session.isUserLogin) {
                let vc = ProfileViewController(nibName: "ProfileViewController", bundle: nil)
                centerNavigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = LoginViewController(nibName: "LoginViewController", bundle: nil)
                centerNavigationController?.pushViewController(vc, animated: true)
            }
        }
            
            else  if((sender as AnyObject).tag == 8) {
                
            }
            
        else if((sender as AnyObject).tag == 9) {
            let vc = SettingViewController(nibName: "SettingViewController", bundle: nil)
            centerNavigationController?.pushViewController(vc, animated: true)
        }
        else if((sender as AnyObject).tag == 10) {
            if(session.isUserLogin) {
                let alert = UIAlertController(title: "Mr.Bonus", message: "Are you sure you want to Logout?", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: { (alert: UIAlertAction!) -> Void in
                    
                }))
                
                alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { (alert: UIAlertAction!) -> Void in
                    self.session.logout()
                }))
                //self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                present(alert, animated: true, completion: nil)
            } else {
                let vc = LoginViewController(nibName: "LoginViewController", bundle: nil)
                centerNavigationController?.pushViewController(vc, animated: true)
            }
        }
        self.mm_drawerController()?.closeDrawer(animated: true) { _ in }
        
    }
    
//    override var prefersStatusBarHidden: Bool {
//        return false
//    }
    
}
