//
//  WishListViewController.swift
//  GotStar
//
//  Created by Admin on 12/8/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class WishListViewController: BaseViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var wishlist = [BannerModel]()
    
    //Passing
    var drawerTitle = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.register(UINib(nibName:"PopularSpiritsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PopularSpiritsCollectionViewCell")
        
        
        self.title = "WishList"
        watchlistService()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        removeBackButtonTitle()
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Show the Navigation Bar
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
}


extension WishListViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return wishlist.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (self.view.frame.width / 2) - 15
        return CGSize(width: width, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PopularSpiritsCollectionViewCell", for: indexPath) as! PopularSpiritsCollectionViewCell
        cell.backgroundColor = .orange
        
        let obj = wishlist[indexPath.row]
        
        BaseViewController.setImageFromUri(imgView: cell.imageView, imageUrl: obj.tvv_thumbnail, placeHolderImage: "ic_place_holder")
//        cell.nameLbl.text = obj.tvs_name.count > 0 ? obj.tvs_name : obj.tvv_name
        cell.nameLbl.text =  obj.tvv_name

//        cell.imageView.image = UIImage(named: "movie_5")
//        cell.nameLbl.text = "Sararat Episode - 1"
        cell.editButton.tag = indexPath.row
        cell.editButton.isHidden = false
        cell.editButton.tintColor = .PRIMERY_COLOR
        cell.editButton.tag = indexPath.row
        cell.editButton.addTarget(self, action: #selector(editbtnClicked), for: .touchUpInside)
        
        return cell
        
    }
    
    @objc func editbtnClicked(_ sender: UIButton) {
        let obj = wishlist[sender.tag]
        
        let alert = UIAlertController(title: "", message: obj.tvv_name, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Remove from wishlist", style: .destructive , handler:{ (UIAlertAction)in
            self.removeWatchService(w_id: obj.w_id, index: sender.tag)
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        //        let obj = list[indexPath.row]
        //        let vc = DetailViewController(nibName: "DetailViewController", bundle: nil)
        //        vc.model = obj
        //        self.navigationController!.pushViewController(vc, animated: true)
    }
    
}

//MARK: Service Call methods
extension WishListViewController {
    
    func watchlistService() {
        if(session.isInternetAvailable()) {
            // Animate Activity Indicator
            self.setProgressIndicatorWithTitle("Loading...", andDetail: "")
            self.showProgressIndicator(true)
            
            DispatchQueue.global(qos: .userInteractive).async {
                
                let (isSuccess, list) = SessionManager.getInstance().getWatchlist(user_id: self.session.profileModel.id)
                
                DispatchQueue.main.async {
                    self.hideProgressIndicator(true)
                    if(isSuccess && list?.count ?? 0 > 0) {
                        self.wishlist = list!
                        self.collectionView.reloadData()
                    } else {
                        Toast.showNegativeMessage(message: "No records found!")
                    }
                }
            }
        } else {
            
            Toast.showNegativeMessage(message: Constants.OPPS_NO_INTERNET_MESSAGE)
        }
    }
    
    func removeWatchService(w_id: String, index: Int) {
        if(session.isInternetAvailable()) {
            // Animate Activity Indicator
            self.setProgressIndicatorWithTitle("Loading...", andDetail: "")
            self.showProgressIndicator(true)
            
            DispatchQueue.global(qos: .userInteractive).async {
                
                let (isSuccess, message) = SessionManager.getInstance().removeWatchlist(w_id: w_id)
                
                DispatchQueue.main.async {
                    self.hideProgressIndicator(true)
                    if(isSuccess) {
                        Toast.showPositiveMessage(message: message!)
                        self.wishlist.remove(at: index)
                        self.collectionView.reloadData()
                    } else {
                        Toast.showNegativeMessage(message: "No records found!")
                    }
                }
            }
        } else {
            
            Toast.showNegativeMessage(message: Constants.OPPS_NO_INTERNET_MESSAGE)
        }
    }
}
