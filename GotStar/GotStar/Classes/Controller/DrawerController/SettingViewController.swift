//
//  SettingViewController.swift
//  GotStar
//
//  Created by Admin on 03/12/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import StoreKit

class SettingViewController: BaseViewController {
    
    @IBOutlet weak var clearBtn: UIButton!
    @IBOutlet weak var pushNotificationSwiftBtn: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "Change Settings"
        setTheme()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        removeBackButtonTitle()
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Show the Navigation Bar
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    func setTheme() {
        clearBtn.tintColor = .PRIMERY_COLOR
        pushNotificationSwiftBtn.onTintColor = .PRIMERY_COLOR
        pushNotificationSwiftBtn.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
    }
    
    @IBAction func clearCasheBtnClicked(_ sender: Any) {
        Toast.showPositiveMessage(message: "Locally cashed data")
    }
    
    @IBAction func notificationBtnClicked(_ sender: UISwitch) {
        
        if(sender.isOn) {
            pushNotificationSwiftBtn.isOn = true
        } else {
            pushNotificationSwiftBtn.isOn = false
        }
    }
    
    @IBAction func privacyPolicyClicked(_ sender: Any) {
        let vc = PrivacyPolicyViewController(nibName: "PrivacyPolicyViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func aboutUsClicked(_ sender: Any) {
        let vc = AboutUsViewController(nibName: "AboutUsViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func shareAppBtnClicked(_ sender: Any) {
        self.callActivityViewController(info: ["App URL"])
    }
    
    @IBAction func rateAppBtn(_ sender: Any) {
        rateApp()
    }
    
    func rateApp() {
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()
            
        }
    }
    
}
