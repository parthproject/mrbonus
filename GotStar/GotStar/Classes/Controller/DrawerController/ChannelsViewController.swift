//
//  ChannelsViewController.swift
//  GotStar
//
//  Created by Admin on 12/8/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class ChannelsViewController: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var channelList = [ChannelModel]()
    
    //Passing
//    var drawerTitle = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.register(UINib(nibName:"PopularSpiritsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PopularSpiritsCollectionViewCell")
        
        self.title = "Channels"
        
        channelListService()
    }


     override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           removeBackButtonTitle()
           self.navigationController?.setNavigationBarHidden(false, animated: animated)
       }
       
       override func viewWillDisappear(_ animated: Bool) {
           super.viewWillDisappear(animated)
           // Show the Navigation Bar
           self.navigationController?.setNavigationBarHidden(true, animated: animated)
       }

}


extension ChannelsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return channelList.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (self.view.frame.width / 2) - 25
        return CGSize(width: width, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PopularSpiritsCollectionViewCell", for: indexPath) as! PopularSpiritsCollectionViewCell
        cell.backgroundColor = .orange
        
        let obj = channelList[indexPath.row]
        BaseViewController.setImageFromUri(imgView: cell.imageView, imageUrl: obj.channel_image, placeHolderImage: "ic_place_holder")
        cell.nameLbl.text = obj.channel_name
        return cell
    }
    
    @objc func editbtnClicked(_ sender: UIButton) {
        let alert = UIAlertController(title: "", message: "Sararat Episode - 1", preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
            print("User click Delete button")
        }))

        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        let obj = channelList[indexPath.row]
        let vc = ChannelDetailViewController(nibName: "ChannelDetailViewController", bundle: nil)
        vc.model = obj
        self.navigationController!.pushViewController(vc, animated: true)
    }
    
    
    
}


//MARK: Service Call methods
extension ChannelsViewController {
    
    func channelListService() {
        if(session.isInternetAvailable()) {
            // Animate Activity Indicator
            self.setProgressIndicatorWithTitle("Loading...", andDetail: "")
            self.showProgressIndicator(true)
            
            DispatchQueue.global(qos: .userInteractive).async {
                
                let (isSuccess, list) = SessionManager.getInstance().channelList()
                
                DispatchQueue.main.async {
                    self.hideProgressIndicator(true)
                    if(isSuccess && list?.count ?? 0 > 0) {
                        self.channelList = list!
                        self.collectionView.reloadData()
                    } else {
                        Toast.showNegativeMessage(message: "No records found!")
                    }
                }
            }
        } else {
            
            Toast.showNegativeMessage(message: Constants.OPPS_NO_INTERNET_MESSAGE)
        }
    }
}
