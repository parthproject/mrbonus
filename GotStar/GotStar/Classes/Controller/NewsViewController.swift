//
//  NewsViewController.swift
//  GotStar
//
//  Created by Admin on 12/7/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import GoogleMobileAds


class NewsViewController: BaseViewController {

    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet weak var scrollContentView: UIView!
    
    @IBOutlet weak var newsTableView: UITableView!
    var newsList = [BannerModel]()
    
    //For banner
       @IBOutlet weak var bannerView: GADBannerView!
       @IBOutlet weak var bannerViewHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpForAds()
        newsTableView.tableFooterView = UIView()
        setNavigationBar()
        
        //addScrollView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.contentView.layoutIfNeeded()
        if(!session.isNewsTabActive) {
            getAllNewsListService()//Service
            setActiveTab()
            

            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
                   imageView.contentMode = .scaleAspectFit

                   let image = UIImage(named: "ic_navi.png")
                   imageView.image = image

                   navigationItem.titleView = imageView
                   
        }
    }
    
    func setActiveTab() {
        session.isTVTabActive = false
        session.iSMovieTabActive = false
        session.isSportsTabActive = false
        session.isNewsTabActive = true
        session.isPremiumTabActive = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = " "
    }


    func addScrollView() {
        // _ = contentView.subviews.map { $0.removeFromSuperview() }
        
        //Added Sales Scroll View
        scrollView.frame = CGRect(x: 0.0, y: 0, width: contentView.frame.width, height: contentView.frame.height)
        contentView.addSubview(scrollView)
        //scrollContentViewHeightConstraint.constant = 400
        self.view.layoutIfNeeded()
    }

}



//MARK: - TableView delegates
extension NewsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsList.count
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        
//        return 180
//        
//        // Swift 4.2 onwards
//        //return UITableView.automaticDimension
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("NewsTableViewCell", owner: self, options: nil)?.first as! NewsTableViewCell
        cell.selectionStyle = .none
        
        let obj = newsList[indexPath.row]
        
        var imgUrl = ""
        if(obj.tvv_thumbnail.contains("https://")) {
            imgUrl = obj.tvv_thumbnail
        } else if(obj.tvs_image.contains("https://")) {
            imgUrl = obj.tvs_image
        }
        
        BaseViewController.setImageFromUri(imgView: cell.imgView, imageUrl: imgUrl, placeHolderImage: "ic_place_holder")
        cell.nameLbl.text = obj.tvv_name
        cell.descriptionLbl.text = obj.tvv_description
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = newsList[indexPath.row]
        let vc = DetailViewController(nibName: "DetailViewController", bundle: nil)
        vc.model = obj
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        tableView.deselectRow(at: indexPath, animated: false)
//
//
//    }
    
}
//MARK: Service Call methods
extension NewsViewController {
    
    func getAllNewsListService() {
        
        if(session.isInternetAvailable()) {
            // Animate Activity Indicator
            self.setProgressIndicatorWithTitle("Loading...", andDetail: "")
            self.showProgressIndicator(true)
            
            DispatchQueue.global(qos: .userInteractive).async {
                let (isSuccess, list) = SessionManager.getInstance().topPickForYouList(type: BannerType.NEWS.rawValue)
//                let (isSuccess, list) = SessionManager.getInstance().getAllNewsList(type: BannerType.NEWS.rawValue)
                DispatchQueue.main.async {
                    
                    if(isSuccess) {
                        self.view.layoutIfNeeded()
                        let popularView = PopularView(frame: CGRect(x: 0.0, y: 0, width: (self.scrollContentView.frame.width), height: 175.0))
                        popularView.layoutIfNeeded()
                        
                        popularView.loadData(title: "Popular Poker", list: list!, nav: self.navigationController!)
//                        self.scrollContentView.addSubview(popularView)
                        self.contentView.addSubview(popularView)
                        
                        self.topPickForYouService()
                    } else {
                        print("False")
                        self.hideProgressIndicator(true)
                    }
                }
            }
        } else {
            Toast.showNegativeMessage(message: Constants.OPPS_NO_INTERNET_MESSAGE)
        }
    }
    
    func topPickForYouService() {
        
        if(session.isInternetAvailable()) {
            // Animate Activity Indicator
            self.setProgressIndicatorWithTitle("Loading...", andDetail: "")
            self.showProgressIndicator(true)
            
            DispatchQueue.global(qos: .userInteractive).async {
                let (isSuccess, list) = SessionManager.getInstance().getAllNewsList(type: BannerType.NEWS.rawValue)
//                let (isSuccess, list) = SessionManager.getInstance().topPickForYouList(type: BannerType.NEWS.rawValue)
                DispatchQueue.main.async {
                    self.hideProgressIndicator(true)

                    
                    if(isSuccess && list?.count ?? 0 > 0) {
                        self.newsList = list!
                    } else {
                        print("False")
                        self.newsList.removeAll()
                    }
                    self.newsTableView.reloadData()
                }
            }
        } else {
            Toast.showNegativeMessage(message: Constants.OPPS_NO_INTERNET_MESSAGE)
        }
    }
}

extension NewsViewController: GADBannerViewDelegate {
        func setUpForAds() {
        let banner_ad_value = self.session.generalSettingDic["banner_ad"]
        if(banner_ad_value == "yes") {
            self.bannerViewHeightConstraint.constant = 50
            
            bannerView.adUnitID = self.session.generalSettingDic["banner_adid"]
            bannerView.rootViewController = self
            bannerView.load(GADRequest())
            bannerView.delegate = self
        } else {
            self.bannerViewHeightConstraint.constant = 0
        }
        self.view.layoutIfNeeded()
    }
    
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
      print("adViewDidReceiveAd")
    }

    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
        didFailToReceiveAdWithError error: Error) {
      print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }

    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
      print("adViewWillPresentScreen")
    }

    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
      print("adViewWillDismissScreen")
    }

    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
      print("adViewDidDismissScreen")
    }

    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
      print("adViewWillLeaveApplication")
    }

}


