//
//  PrivacyPolicyViewController.swift
//  E-Books
//
//  Created by baps on 15/01/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class PrivacyPolicyViewController: BaseViewController {

    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Privacy Policy"
        
        let text = self.session.generalSettingDic["privacy_policy"]
        textView.text = text
        
        DispatchQueue.main.async {
          self.textView.scrollRangeToVisible(NSRange(location: 0, length: 0))
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.tabBarController?.tabBar.isHidden = true
        
//            textView.setContentOffset(.zero, animated: true)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
}
