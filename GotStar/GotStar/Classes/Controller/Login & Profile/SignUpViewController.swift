//
//  SignUpViewController.swift
//  GotStar
//
//  Created by Admin on 12/8/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class SignUpViewController: BaseViewController {
    
    @IBOutlet weak var appLogoImgView: UIImageView!
    @IBOutlet weak var fullNameTextField: ACFloatingTextfield!
    @IBOutlet weak var emailTextField: ACFloatingTextfield!
    @IBOutlet weak var passwordTextField: ACFloatingTextfield!
    @IBOutlet weak var phoneTextField: ACFloatingTextfield!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTheme()
        if let appLogo = session.generalSettingDic["app_logo"] {
            let logoUrl = BASE_URL + IMAGE_URL + appLogo
            BaseViewController.setImageFromUri(imgView: appLogoImgView, imageUrl: logoUrl, placeHolderImage: "ic_got_star")
        }
    }
    
    func setTheme() {
        signUpBtn.backgroundColor = .PRIMERY_COLOR
        
        loginBtn.setTitleColor(.PRIMERY_COLOR, for: .normal)
    }
    
    
    @IBAction func skipBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func registerClicked(_ sender: Any) {
        endEditing()
        
        if(session.isInternetAvailable()) {
            if(fullNameTextField.text?.count == 0) {
                showAlertView(title: Constants.ALERT, message: Constants.FULL_NAME_ERROR_MESSAGE)
            } else if((fullNameTextField.text?.count)! < 3 || (fullNameTextField.text?.count)! > 64) {
                showAlertView(title: Constants.ALERT, message: Constants.FULL_NAME_LIMIT_ERROR_MESSAGE)
            }
            else if(emailTextField.text?.count == 0) {
                showAlertView(title: Constants.ALERT, message: Constants.EMAIL_EMPTY_MESSAGE)
            }
            else if(!isValidEmail(email: emailTextField.text!)) {
                showAlertView(title: Constants.ALERT, message: Constants.EMAIL_ERROR_MESSAGE)
            }
            else if(passwordTextField.text?.count == 0) {
                showAlertView(title: Constants.ALERT, message: Constants.PASSWORD_EMPTY_MESSAGE)
            }
            else if((passwordTextField.text?.count)! < 8 || (passwordTextField.text?.count)! > 64) {
                showAlertView(title: Constants.ALERT, message: Constants.PASSWORD_LENGHT_ERROR_MESSAGE)
            }
                //            else if(!isValidPassword(password: passwordTextField.text!)) {
                //                showAlertView(title: Constants.ALERT, message: PASSWORD_FORMATE_ERROR_MESSAGE)
                //            }
            else if((phoneTextField.text?.count)! == 0) {
                showAlertView(title: Constants.ALERT, message: Constants.PHONE_NUMBER_ERROR_MESSAGE)
            }
            else if((phoneTextField.text?.count)! < 10 || (phoneTextField.text?.count)! > 20) {
                showAlertView(title: Constants.ALERT, message: Constants.PHONE_NUMBER_VALIDATION_2)
            }
            else {
                registerService()
            }
        }
        else {
            Toast.showNegativeMessage(message: Constants.OPPS_NO_INTERNET_MESSAGE)
        }
    }
}

extension SignUpViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //endEditing()
        //return false
        
        let nextTag = textField.tag + 1
        
        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            registerClicked(self)
            textField.resignFirstResponder()
        }
        
        return true
    }
}


//MARK: Service Call methods
extension SignUpViewController {
    
    func registerService() {
        if(session.isInternetAvailable()) {
            // Animate Activity Indicator
            self.setProgressIndicatorWithTitle("Loading...", andDetail: "")
            self.showProgressIndicator(true)
            
            let name = fullNameTextField.text!
            let email = emailTextField.text!
            let password = passwordTextField.text!
            let phone = phoneTextField.text!
            
            DispatchQueue.global(qos: .userInteractive).async {
                let (isSuccess, userId) = SessionManager.getInstance().register(name: name, email: email, password: password, phone: phone)
                DispatchQueue.main.async {
                    self.hideProgressIndicator(true)
                    if(isSuccess) {
                        self.profileService(User_id: userId!)
                        
                    } else {
                        print("False")
                    }
                }
            }
        } else {
            Toast.showNegativeMessage(message: Constants.OPPS_NO_INTERNET_MESSAGE)
        }
    }
    
    func profileService(User_id: String) {

        if(session.isInternetAvailable()) {
            // Animate Activity Indicator
//            self.setProgressIndicatorWithTitle("Loading...", andDetail: "")
//            self.showProgressIndicator(true)
        
            DispatchQueue.global(qos: .userInteractive).async {
                let isSuccess = SessionManager.getInstance().profileFetch(user_id: User_id)
                DispatchQueue.main.async {
                    self.hideProgressIndicator(true)
                    if(isSuccess) {
                       var navigationArray = self.navigationController?.viewControllers //To get all UIViewController stack as Array
                        navigationArray!.removeLast(2)
                        self.navigationController?.viewControllers = navigationArray!
                    }
                }
            }
        } else {
            Toast.showNegativeMessage(message: Constants.OPPS_NO_INTERNET_MESSAGE)
        }
    }
}
