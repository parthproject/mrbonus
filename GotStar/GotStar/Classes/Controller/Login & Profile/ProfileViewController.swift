//
//  ProfileViewController.swift
//  GotStar
//
//  Created by Admin on 12/8/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController {

    @IBOutlet weak var nameLbl: UILabel!
    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    
    
       override func viewDidLoad() {
         super.viewDidLoad()
         // Do any additional setup after loading the view.
         self.title = "Profile"
        
        nameLbl.text = session.profileModel.fullname
        nameField.text = session.profileModel.fullname
        emailField.text = session.profileModel.email
        phoneField.text = session.profileModel.mobile_number
     }
    
    func updateLayer() {
       

       // Other updates.
    }
     
     override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
         removeBackButtonTitle()
         self.navigationController?.setNavigationBarHidden(false, animated: animated)
     }
     
     override func viewWillDisappear(_ animated: Bool) {
         super.viewWillDisappear(animated)
         // Show the Navigation Bar
         self.navigationController?.setNavigationBarHidden(true, animated: animated)
     }

}
