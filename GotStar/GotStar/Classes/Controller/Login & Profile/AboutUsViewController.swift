//
//  AboutUsViewController.swift
//  E-Books
//
//  Created by baps on 15/01/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class AboutUsViewController: BaseViewController {
    
    
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet weak var scrollContentView: UIView!
    @IBOutlet weak var scrollViewContentHeightConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var appNameView: UIView!
    @IBOutlet weak var companyView: UIView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var websiteView: UIView!
    @IBOutlet weak var contactView: UIView!
    @IBOutlet weak var aboutView: UIView!
    
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblWebsite: UILabel!
    @IBOutlet weak var lblContact: UILabel!
    @IBOutlet weak var lblAboutUs: UILabel!
    
    
    @IBOutlet weak var appLogoImgView: UIImageView!
    @IBOutlet weak var appNameLbl: UILabel!
    
    @IBOutlet weak var companyLogoImgView: UIImageView!
    @IBOutlet weak var companyNameLbl: UILabel!
    
    @IBOutlet weak var emailImgView: UIImageView!
    @IBOutlet weak var emailLbl: UILabel!
    
    @IBOutlet weak var websiteImgView: UIImageView!
    @IBOutlet weak var websiteLbl: UILabel!
    
    @IBOutlet weak var contactImgView: UIImageView!
    @IBOutlet weak var contactLbl: UILabel!
    
    @IBOutlet weak var aboutUsLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "About Us"
        
        addScrollView()
        loadData()
        setTheme()
        contactLbl?.text = "+49 211 179 33 545"

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func setTheme() {
        appNameView.backgroundColor = .SECONDARY_COLOR
        companyView.backgroundColor = appNameView.backgroundColor
        emailView.backgroundColor = appNameView.backgroundColor
        websiteView.backgroundColor = appNameView.backgroundColor
        contactView.backgroundColor = appNameView.backgroundColor
        aboutView.backgroundColor = appNameView.backgroundColor
        
        appNameLbl.textColor = .PRIMERY_COLOR
        lblCompany.textColor = appNameLbl.textColor
        lblEmail.textColor = appNameLbl.textColor
        lblWebsite.textColor = appNameLbl.textColor
        lblContact.textColor = appNameLbl.textColor
        lblAboutUs.textColor = appNameLbl.textColor
        
        appLogoImgView.tintColor = .PRIMERY_COLOR
        companyLogoImgView.tintColor = appLogoImgView.tintColor
        emailImgView.tintColor = appLogoImgView.tintColor
        websiteImgView.tintColor = appLogoImgView.tintColor
        contactImgView?.tintColor = appLogoImgView.tintColor
    }
    
    func addScrollView() {
        self.view.layoutIfNeeded()
        //Added Sales Scroll View
        scrollView.frame = CGRect(x: 0.0, y: 0, width: mainContentView.frame.width, height: mainContentView.frame.height)
        mainContentView.addSubview(scrollView)
        self.view.layoutIfNeeded()
        
//        scrollViewContentHeightConstraint.constant = 1000
    }
    
    func loadData() {
        if let appLogo = session.generalSettingDic["app_logo"] {
            let logoUrl = BASE_URL + IMAGE_URL + appLogo
            BaseViewController.setImageFromUri(imgView: appLogoImgView, imageUrl: logoUrl, placeHolderImage: "ic_app_logo")
        }
        appNameLbl.text = session.generalSettingDic["app_name"]
        
        companyNameLbl.text = session.generalSettingDic["Author"]
        
        emailLbl.text = session.generalSettingDic["email"]
        
        websiteLbl.text = session.generalSettingDic["website"]
        
        
        aboutUsLbl.text = session.generalSettingDic["app_desripation"]
        
        self.view.layoutIfNeeded()
        scrollViewContentHeightConstraint.constant = aboutView.frame.origin.y + aboutView.frame.height + 20

        
    }
    
}
