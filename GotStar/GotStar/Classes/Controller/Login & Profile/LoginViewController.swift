//
//  LoginViewController.swift
//  GotStar
//
//  Created by Admin on 12/8/19.
//  Copyright © 2019 Admin. All rights reserved.
//

//checkmark.rectangle
//square

import UIKit
//import GoogleMobileAds

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var appLogoImgView: UIImageView!
    @IBOutlet weak var emailTextField: ACFloatingTextfield!
    @IBOutlet weak var passwordTextField: ACFloatingTextfield!
    @IBOutlet weak var rememberMeBtn: UIButton!
    var isRememberMeSelected = false
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    
//    var interstitial: GADInterstitial!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTheme()
        // Do any additional setup after loading the view.
        if let appLogo = session.generalSettingDic["app_logo"] {
            let logoUrl = BASE_URL + IMAGE_URL + appLogo
            BaseViewController.setImageFromUri(imgView: appLogoImgView, imageUrl: logoUrl, placeHolderImage: "ic_got_star")
        }
        
        
        rememberMeBtn.setImage(UIImage(named: "ic_square"), for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.tabBarController?.tabBar.isHidden = true
        
//        if let interstital_ad = session.generalSettingDic["interstital_ad"] {
//            if(interstital_ad == "yes") {
//                interstitial = createAndLoadInterstitial()
//            }
//        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func setTheme() {
        loginBtn.backgroundColor = .PRIMERY_COLOR
        //        rememberMeBtn.tintColor = .PRIMERY_COLOR

        signUpBtn.setTitleColor(.PRIMERY_COLOR, for: .normal)
    }
    
    @IBAction func skipBtnClicked(_ sender: Any) {
//        if let interstital_ad = session.generalSettingDic["interstital_ad"] {
//            if(interstital_ad == "yes") {
//                if(interstitial == nil) {
//                    interstitial = createAndLoadInterstitial()
//                }
//
//                if interstitial.isReady {
//                    interstitial.present(fromRootViewController: self)
//                } else {
//                    print("Ad wasn't ready")
//                    self.navigationController?.popViewController(animated: true)
//                }
//            } else {
//                self.navigationController?.popViewController(animated: true)
//            }
//        }
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func signUpBtnClicked(_ sender: Any) {
        let vc = SignUpViewController(nibName: "SignUpViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func rememberBtnClicked(_ sender: UIButton) {
        if(isRememberMeSelected) {
//            if #available(iOS 13.0, *) {
//                rememberMeBtn.setImage(UIImage(systemName: "square"), for: .normal)
//            } else {
                rememberMeBtn.setImage(UIImage(named: "ic_square"), for: .normal)
//            }
        } else {
//            if #available(iOS 13.0, *) {
//                rememberMeBtn.setImage(UIImage(systemName: "checkmark.rectangle"), for: .normal)
//            } else {
                rememberMeBtn.setImage(UIImage(named: "ic_square_checked"), for: .normal)
//            }
        }
        isRememberMeSelected = !isRememberMeSelected
    }
    
    
    @IBAction func loginClicked(_ sender: Any) {
        endEditing()
        
        if(session.isInternetAvailable()) {
            if(emailTextField.text?.count == 0) {
                showAlertView(title: Constants.ALERT, message: Constants.EMAIL_EMPTY_MESSAGE)
            }
            else if(!isValidEmail(email: emailTextField.text!)) {
                showAlertView(title: Constants.ALERT, message: Constants.EMAIL_ERROR_MESSAGE)
            } else if(passwordTextField.text?.count == 0) {
                showAlertView(title: Constants.ALERT, message: Constants.PASSWORD_EMPTY_MESSAGE)
            }
            else {
                loginService()
            }
        }
        else {
            Toast.showNegativeMessage(message: Constants.OPPS_NO_INTERNET_MESSAGE)
        }
    }
    
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //return false
        let nextTag = textField.tag + 1
        
        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            loginClicked(self)
            textField.resignFirstResponder()
        }
        
        return true
    }
}


//MARK: Service Call methods
extension LoginViewController {
    
    func loginService() {
        if(session.isInternetAvailable()) {
            // Animate Activity Indicator
            self.setProgressIndicatorWithTitle("Loading...", andDetail: "")
            self.showProgressIndicator(true)
            
            let email = emailTextField.text!
            let password = passwordTextField.text!
            
            DispatchQueue.global(qos: .userInteractive).async {
                let (isSuccess, model) = SessionManager.getInstance().login(email: email, password: password)
                DispatchQueue.main.async {
                    self.hideProgressIndicator(true)
                    if(isSuccess) {
                        self.profileService(User_id: model!.User_id)
                        
                    } else {
                        print("False")
                    }
                }
            }
        } else {
            Toast.showNegativeMessage(message: Constants.OPPS_NO_INTERNET_MESSAGE)
        }
    }
    
    func profileService(User_id: String) {
        
        if(session.isInternetAvailable()) {
            // Animate Activity Indicator
            //            self.setProgressIndicatorWithTitle("Loading...", andDetail: "")
            //            self.showProgressIndicator(true)
            
            DispatchQueue.global(qos: .userInteractive).async {
                let isSuccess = SessionManager.getInstance().profileFetch(user_id: User_id)
                DispatchQueue.main.async {
                    self.hideProgressIndicator(true)
                    if(isSuccess) {
//                        self.skipBtnClicked(self)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        } else {
            Toast.showNegativeMessage(message: Constants.OPPS_NO_INTERNET_MESSAGE)
        }
    }
}

//extension LoginViewController: GADInterstitialDelegate {
//
//    func createAndLoadInterstitial() -> GADInterstitial {
//        let interstital_adid = session.generalSettingDic["interstital_adid"]
//        let interstitial = GADInterstitial(adUnitID: interstital_adid ?? "")
//        interstitial.delegate = self
//        interstitial.load(GADRequest())
//        return interstitial
//    }
//
//
//    /// Tells the delegate an ad request succeeded.
//    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
//        print("interstitialDidReceiveAd")
//    }
//
//    /// Tells the delegate an ad request failed.
//    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
//        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
//    }
//
//    /// Tells the delegate that an interstitial will be presented.
//    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
//        print("interstitialWillPresentScreen")
//    }
//
//    /// Tells the delegate the interstitial is to be animated off the screen.
//    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
//        print("interstitialWillDismissScreen")
//    }
//
//    /// Tells the delegate the interstitial had been animated off the screen.
//    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
//        print("interstitialDidDismissScreen")
//        self.navigationController?.popViewController(animated: true)
//
//    }
//
//    /// Tells the delegate that a user click will open another app
//    /// (such as the App Store), backgrounding the current app.
//    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
//        print("interstitialWillLeaveApplication")
//    }
//}
