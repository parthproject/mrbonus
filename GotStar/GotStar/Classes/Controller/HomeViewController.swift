//
//  HomeViewController.swift
//  GotStar
//
//  Created by Admin on 03/12/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import GoogleMobileAds


class HomeViewController: BaseViewController {
    
    var typeName = String()
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet weak var scrollContentView: UIView!
    @IBOutlet weak var topScrollView: UIScrollView!
    var bannerList = [BannerModel]()
    var popularShowList = [BannerModel]()
    var topScrollTimer = Timer()
    @IBOutlet weak var bannerHeightConstraint: NSLayoutConstraint!
    
    var popularInNameList = [String]()
    var popularInItemList = [[BannerModel]]()
    var serviceCallPopularList = [String]()
    
    //For banner
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var bannerViewHeightConstraint: NSLayoutConstraint!
    
    //Passing
    var isFromDrawerView = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        if(isFromDrawerView) {
//            typeName = BannerType.TV.rawValue
//            removeBackButtonTitle()
//        } else {
        
        typeName = getTypeNameBasedOnTab()
            setNavigationBar()
//        }
        
        addScrollView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        if(isFromDrawerView) {
//            self.title = "GotStar App"
//           self.navigationController?.setNavigationBarHidden(false, animated: animated)
//        }
        
        self.tabBarController?.tabBar.isHidden = false
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
        imageView.contentMode = .scaleAspectFit

        let image = UIImage(named: "ic_navi.png")
        imageView.image = image

        navigationItem.titleView = imageView
        
        self.navigationController?.navigationBar.isTranslucent = true
        

        performSelector(inBackground: #selector(generalSettingBG), with: nil)
        
        if(!session.isTVTabActive) {
            bannerListService()//Service
            setActiveTab()
        }
    }
  
    
    
    func setActiveTab() {
        session.isTVTabActive = true
        session.iSMovieTabActive = false
        session.isSportsTabActive = false
        session.isNewsTabActive = false
        session.isPremiumTabActive = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        if(isFromDrawerView) {
//            self.navigationController?.setNavigationBarHidden(true, animated: animated)
//        }
        scrollView.setContentOffset(.zero, animated: false)
        topScrollView.setContentOffset(.zero, animated: false)
        self.navigationItem.title = " "
    }
    
    func addScrollView() {
        // _ = contentView.subviews.map { $0.removeFromSuperview() }
        
        //Added Sales Scroll View
        scrollView.frame = CGRect(x: 0.0, y: 0, width: contentView.frame.width, height: contentView.frame.height)
        contentView.addSubview(scrollView)
        //scrollContentViewHeightConstraint.constant = 400
        self.view.layoutIfNeeded()
    }
    
    @objc func generalSettingBG() {
        DispatchQueue.global(qos: .userInteractive).async {
            let isSuccess = SessionManager.getInstance().generalSetting()
            DispatchQueue.main.async {
                if(isSuccess) {
                    let banner_ad_value = self.session.generalSettingDic["banner_ad"]
                    if(banner_ad_value == "yes") {
                        self.bannerViewHeightConstraint.constant = 50
                        self.setUpForAds()
                    } else {
                        self.bannerViewHeightConstraint.constant = 0
                    }
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
}

//MARK: - Top Scroll View
extension HomeViewController {
    func addBannerView() {
        _ = topScrollView.subviews.map { $0.removeFromSuperview() }
        self.topScrollTimer.invalidate()
        
        for index in 0..<bannerList.count {
            let obj = bannerList[index]
            
            let bannerView = HomeBannerView(frame: CGRect(x: CGFloat(index) * topScrollView.frame.width, y: 0, width: (topScrollView.frame.width), height: topScrollView.frame.height))
            bannerView.layoutIfNeeded()
            
            BaseViewController.setImageFromUri(imgView: bannerView.imageView, imageUrl: obj.tvv_thumbnail, placeHolderImage: "ic_place_holder")
            bannerView.nameLbl.text = obj.tvv_name
            bannerView.typeLbl.text = obj.cat_name
            bannerView.countLbl.text = "\(index + 1)/\(bannerList.count)"
            //            bannerView.imageView.image = UIImage(named: "movie_\(index+1)")
            
            
            bannerView.clickBtn.tag = index
            bannerView.clickBtn.addTarget(self, action: #selector(self.bannerClicked(_:)), for: .touchUpInside)
            self.topScrollView.addSubview(bannerView)
        }
        
        //        banner1PageControl.currentPage = 0
        self.topScrollView.contentOffset = CGPoint(x: 0, y: 0)
        
        self.topScrollView.contentSize = CGSize(width: self.topScrollView.frame.size.width * CGFloat(bannerList.count),height: self.topScrollView.frame.size.height)
        
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { // Change `2.0` to the desired number of seconds.
            self.topScrollTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.moveToNextPage), userInfo: nil, repeats: true)
        }
    }
    
    //The target function
    @objc func bannerClicked(_ sender: UIButton) {
        let obj = bannerList[sender.tag]
        print(obj.tvv_name)
        
        let vc = DetailViewController(nibName: "DetailViewController", bundle: nil)
        vc.model = obj
//        vc.isTVTab = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func moveToNextPage (){
        let pageWidth:CGFloat = self.topScrollView.frame.width
        let maxWidth:CGFloat = pageWidth * CGFloat(bannerList.count)
        let contentOffset:CGFloat = self.topScrollView.contentOffset.x
        
        var slideToX = contentOffset + pageWidth
        
        if  contentOffset + pageWidth == maxWidth
        {
            slideToX = 0
        }
        self.topScrollView.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.topScrollView.frame.height), animated: true)
        //        self.banner1PageControl.currentPage = Int(slideToX / pageWidth)
    }
}

//MARK: Service Call methods
extension HomeViewController {
    
    //getBannerlist
    func bannerListService() {
//        _ = scrollContentView.subviews.map { $0.removeFromSuperview() }
        
        let subViews = self.scrollContentView.subviews
        for subview in subViews{
            if subview.tag != 111 {
                subview.removeFromSuperview()
            }
        }
        
        if(session.isInternetAvailable()) {
            // Animate Activity Indicator
            self.setProgressIndicatorWithTitle("Loading...", andDetail: "")
            self.showProgressIndicator(true)
            
            DispatchQueue.global(qos: .userInteractive).async {
                let (isSuccess, bannerList) = SessionManager.getInstance().getBannerlist(type: self.typeName)
                DispatchQueue.main.async {
                    //self.hideProgressIndicator(true)
                    if(isSuccess) {
                        self.bannerList = bannerList!
                        self.addBannerView()
                        
                        self.popularShoeListService()
                    } else {
//                        print("False")
                        self.hideProgressIndicator(true)
                    }
                }
            }
        } else {
            
            Toast.showNegativeMessage(message: Constants.OPPS_NO_INTERNET_MESSAGE)
        }
    }
    
    //getPopular Show
    func popularShoeListService() {
        if(session.isInternetAvailable()) {
            // Animate Activity Indicator
//            self.setProgressIndicatorWithTitle("Loading...", andDetail: "")
//            self.showProgressIndicator(true)
            
            DispatchQueue.global(qos: .userInteractive).async {
                let (isSuccess, popularShowList) = SessionManager.getInstance().getPopularShow(type: self.typeName)
                DispatchQueue.main.async {
                    //self.hideProgressIndicator(true)
                    if(isSuccess && popularShowList?.count ?? 0 > 0) {
                        self.popularShowList = popularShowList!
                        self.addPopularShow()
                        
                        self.categotyListService()
                    } else {
                        self.popularShowList.removeAll()
//                        print("False")
                    }
                }
            }
        } else {
            
            Toast.showNegativeMessage(message: Constants.OPPS_NO_INTERNET_MESSAGE)
        }
    }
    
    func addPopularShow() {
        let bannerHeight:Int = Int(self.topScrollView.frame.size.height)
        let popularViewHeight:CGFloat = 165.0
        
        let popularView = PopularView(frame: CGRect(x: 0.0, y: CGFloat(bannerHeight), width: (self.scrollContentView.frame.width), height: popularViewHeight))
        popularView.layoutIfNeeded()
        
        popularView.loadData(title: "Popular Slots", list: popularShowList, nav: self.navigationController!)
        self.scrollContentView.addSubview(popularView)
    }
    
    func categotyListService() {
        if(session.isInternetAvailable()) {
            // Animate Activity Indicator
            //            self.setProgressIndicatorWithTitle("Loading...", andDetail: "")
            //            self.showProgressIndicator(true)
            
            DispatchQueue.global(qos: .userInteractive).async {
                let (isSuccess, categoryList) = SessionManager.getInstance().getCategorylist()
                DispatchQueue.main.async {
                    //self.hideProgressIndicator(true)
                    if(isSuccess) {
                        self.serviceCallPopularList.removeAll()
                        self.popularInNameList.removeAll()
                        self.popularInItemList.removeAll()
                        
                        _ = Timer.scheduledTimer(withTimeInterval: 7, repeats: false, block: { (timer) in
                            if(self.serviceCallPopularList.count != categoryList?.count) {
                                self.hideProgressIndicator(true)
                                
                                self.loadPopuparList()
                            } else {
//                                print("..................End..................")
                            }
                        })
                        
                        for index in 0..<categoryList!.count {
                            let obj = categoryList![index]
                            self.popularInService(cat_name: obj.cat_name, cat_id: obj.c_id, categoryCount: categoryList!.count)
//                            sleep(UInt32(0.5))
                        }
                    } else {
//                        print("False")
                        self.hideProgressIndicator(true)
                    }
                }
            }
        } else {
            Toast.showNegativeMessage(message: Constants.OPPS_NO_INTERNET_MESSAGE)
        }
    }
    
    //
    func popularInService(cat_name: String, cat_id: String, categoryCount: Int) {
        if(session.isInternetAvailable()) {
            // Animate Activity Indicator
//            self.setProgressIndicatorWithTitle("Loading...", andDetail: "")
//            self.showProgressIndicator(true)
            
            DispatchQueue.global(qos: .userInteractive).async {
                let (isSuccess, popularInList) = SessionManager.getInstance().getPopularIn(type: self.typeName, cat_id: cat_id)
                DispatchQueue.main.async {
                    self.serviceCallPopularList.append("called")
//                    print("\(self.serviceCallPopularList.count)............")
                    
                    if(isSuccess) {
                        if(popularInList!.count > 0) {
                            self.popularInNameList.append(cat_name)
                            self.popularInItemList.append(popularInList!)
                        }
                    } else {
                        print("False")
                    }
                    if(categoryCount == self.serviceCallPopularList.count) {
                        self.hideProgressIndicator(true)
                        
                        self.loadPopuparList()
                        
                    }
                }
            }
        } else {
            Toast.showNegativeMessage(message: Constants.OPPS_NO_INTERNET_MESSAGE)
        }
    }
    
    func loadPopuparList() {
        let bannerHeight:Int = Int(topScrollView.frame.size.height) + Int(popularShowList.count > 0 ? 165 : 0)
        let popularViewHeight:CGFloat = 165.0
        
        for index in 0..<self.popularInItemList.count {

            if popularShowList.count > 0
            {
                if index%2 == 0
                {
                    
                    let popularView = PopularViewFull(frame: CGRect(x: 0.0, y: CGFloat(bannerHeight + (index * Int(popularViewHeight)) + (Int(floor(Double(Float(index+1) / 2.0))) * 150)), width: (self.scrollContentView.frame.width), height: popularViewHeight+150))
                    popularView.layoutIfNeeded()
                    
                    popularView.loadData(title: "Popular in \(self.popularInNameList[index])", list: self.popularInItemList[index], nav: self.navigationController!)
                    self.scrollContentView.addSubview(popularView)

                }
                else
                {
                    let popularView = PopularView(frame: CGRect(x: 0.0, y: CGFloat(bannerHeight + (index * Int(popularViewHeight)) + (Int(floor(Double(Float(index+1) / 2.0))) * 150)), width: (self.scrollContentView.frame.width), height: popularViewHeight))
                    popularView.layoutIfNeeded()
                    
                    popularView.loadData(title: "Popular in \(self.popularInNameList[index])", list: self.popularInItemList[index], nav: self.navigationController!)
                    self.scrollContentView.addSubview(popularView)
                }
            }
            else
            {
                if index%2 != 0
                {
                    let popularView = PopularViewFull(frame: CGRect(x: 0.0, y: CGFloat(bannerHeight + (index * Int(popularViewHeight)) + ((index / 2) * 150)), width: (self.scrollContentView.frame.width), height: popularViewHeight+150))
                    popularView.layoutIfNeeded()
                    popularView.loadData(title: "Popular in \(self.popularInNameList[index])", list: self.popularInItemList[index], nav: self.navigationController!)
                    self.scrollContentView.addSubview(popularView)

                }
                else
                {
                    let popularView = PopularView(frame: CGRect(x: 0.0, y: CGFloat(bannerHeight + (index * Int(popularViewHeight)) + ((index / 2) * 150)), width: (self.scrollContentView.frame.width), height: popularViewHeight))
                    popularView.layoutIfNeeded()
                    
                    popularView.loadData(title: "Popular in \(self.popularInNameList[index])", list: self.popularInItemList[index], nav: self.navigationController!)
                    self.scrollContentView.addSubview(popularView)
                }
            }
        }
        
        var conSize = 0
        if popularShowList.count > 0
        {
            conSize = (Int(floor(Double(Float(popularInItemList.count+1) / 2.0))) * 150)
        }
        else
        {
            conSize = (Int(floor(Double(Float(popularInItemList.count) / 2.0))) * 150)
        }
        
        self.scrollView.contentSize = CGSize(width: self.contentView.frame.width, height: CGFloat(bannerHeight + (self.popularInItemList.count * Int(popularViewHeight)) + 20 + conSize))
        self.scrollContentView.frame = CGRect(x: 0, y: 0, width: self.scrollContentView.frame.size.width, height: CGFloat(bannerHeight + (self.popularInItemList.count * Int(popularViewHeight)) + 20 + conSize))

        self.view.layoutIfNeeded()
    }
}

extension HomeViewController: GADBannerViewDelegate {
    func setUpForAds() {
       bannerView.adUnitID = self.session.generalSettingDic["banner_adid"]
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
    }
    
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
      print("adViewDidReceiveAd")
    }

    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: Error)
    {
      print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }

    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
      print("adViewWillPresentScreen")
    }

    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
      print("adViewWillDismissScreen")
    }

    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
      print("adViewDidDismissScreen")
    }

    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
      print("adViewWillLeaveApplication")
    }

}
