//
//  ViewAllViewController.swift
//  GotStar
//
//  Created by Admin on 12/7/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class ViewAllViewController: BaseViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    //Passing
    var navigationTitle = String()
    var list = [BannerModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.collectionView.register(UINib(nibName:"PopularSpiritsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PopularSpiritsCollectionViewCell")

        self.title = navigationTitle
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        self.navigationItem.title = navigationTitle
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = " "
    }
}

extension ViewAllViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return list.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (self.view.frame.width / 2) - 25
        return CGSize(width: width, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PopularSpiritsCollectionViewCell", for: indexPath) as! PopularSpiritsCollectionViewCell
        cell.backgroundColor = .orange
        
        let obj = list[indexPath.row]
        
        var imgUrl = ""
        if(obj.tvv_thumbnail.contains("https://")) {
            imgUrl = obj.tvv_thumbnail
        } else if(obj.tvs_image.contains("https://")) {
            imgUrl = obj.tvs_image
        }
        
        BaseViewController.setImageFromUri(imgView: cell.imageView, imageUrl: imgUrl, placeHolderImage: "ic_place_holder")
        cell.nameLbl.text = obj.tvs_name
        //cell.nameLbl.text = "\(indexPath.row)"
//        cell.imageView.image = UIImage(named: "movie_\(indexPath.row+1)")
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
//        let vc = ViewAllViewController(nibName: "ViewAllViewController", bundle: nil)
//        vc.navigationTitle = titleName
//        (superview?.next as? UIViewController)?.navigationController?.pushViewController(vc, animated: false)
        
        let obj = list[indexPath.row]
        let vc = DetailViewController(nibName: "DetailViewController", bundle: nil)
        vc.model = obj
        self.navigationController!.pushViewController(vc, animated: true)
    }
    
    
    
}
