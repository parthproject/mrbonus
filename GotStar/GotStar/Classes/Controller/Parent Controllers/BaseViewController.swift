//
//  BaseViewController.swift
//  GotoLiquorStoreOwneriOS
//
//  Created by Kuldip Patel on 19/11/18.
//  Copyright © 2018 Kuldip Patel. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore

let IS_IPHONE_4 = (fabs(Double(UIScreen.main.bounds.size.height) - Double(480)) < .ulpOfOne)
let IS_IPHONE_5 = (fabs(Double(UIScreen.main.bounds.size.height) - Double(568)) < .ulpOfOne)
let IS_IPHONE_6 = (fabs(Double(UIScreen.main.bounds.size.height) - Double(667)) < .ulpOfOne)
let IS_IPHONE_6_PLUS = (fabs(Double(UIScreen.main.bounds.size.height) - Double(736)) < .ulpOfOne)

class BaseViewController: UIViewController,MBProgressHUDDelegate {
    
    
    var viewNotificationEnabled: Bool = false
    var session = SessionManager.getInstance()
    var progressIndicator: MBProgressHUD?
    
    func loadVariables() {}
    
    func loadNavigationBar() {}
    
    func loadDisplayContents() {}
    
    func startNotifications() {
        viewNotificationEnabled = true
    }
    
    func resignNotifications() {
        viewNotificationEnabled = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadVariables()
        self.loadNavigationBar()
        self.loadDisplayContents()
        
//        navigationController?.navigationBar.barStyle = .black
//        navigationController?.navigationBar.barTintColor = UIColor.black
//        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.systemYellow]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Enable Notification
        if viewNotificationEnabled == false {
            self.startNotifications()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Disable Notification
        if viewNotificationEnabled == true {
            self.resignNotifications()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func endEditing() {
        self.view.endEditing(true)
    }
    
    func setNavigationBarTitle(_ title: String) {
    }
    // MARK: - Tab Selection Methods
    
    func currentTabSelected(_ tabController: UITabBarController) {
        if viewNotificationEnabled == false {
            self.startNotifications()
        }
    }
    
    func currentTabDeselected(_ tabController: UITabBarController) {
        if viewNotificationEnabled == true {
            self.resignNotifications()
        }
    }
    
    func willAppear(in navController: UINavigationController) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
        print(".....MEMORY WARNING.....")
    }
    
    func showAlertView(title: String, message: String) {
        var stringMessage = message
        if (title.count == 0 && message.count == 0) || (title.count > 0 && message.count == 0) {
            stringMessage = "Unknown error occurred"
        }
        let alert = UIAlertController(title: title, message: stringMessage, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        //self.window?.rootViewController?.present(alert, animated: true, completion: nil)
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Progress Bar Display Methods
    func setProgressIndicatorWithTitle(_ title: String, andDetail detail: String?) {
        // Hide Any Ongoing Progress Indicator
        if progressIndicator != nil {
            progressIndicator?.hide(animated: true)
            progressIndicator = nil
        }
        // Show Indicator
        progressIndicator = MBProgressHUD.showAdded(to: session.getAppScreen(), animated: true)
        //session.getAppScreen().addSubview(progressIndicator!)
        //progressIndicator?.backgroundColor = UIColor.white
        progressIndicator?.delegate = self
        progressIndicator?.label.text = title
        progressIndicator?.detailsLabel.text = detail
        progressIndicator?.mode = .indeterminate
    }
    
    @objc func showProgressIndicator(_ animated: NSNumber) {
        // Display Indicator
        if progressIndicator != nil {
            progressIndicator?.show(animated: true)
        }
    }
    
    func showProgressIndicatorWithTitle(title: String, andDetail detail: String?, animated: Bool) {
        // Set Indicator Details
        setProgressIndicatorWithTitle(title, andDetail: detail!)
        // Display Indicator
        showProgressIndicator(animated as NSNumber)
    }
    
    @objc func hideProgressIndicator(_ animated: NSNumber) {
        // Hide Indicator
        if progressIndicator != nil {
            progressIndicator?.hide(animated: true)
            progressIndicator = nil
        }
    }
    
    // MARK: MBProgressHUD Delegate Methods
    func hudWasHidden(_ hud: MBProgressHUD) {
        // Remove HUD from screen when the HUD was hidden
        if progressIndicator != nil {
            progressIndicator?.removeFromSuperview()
            progressIndicator = nil
        }
    }
    
    // MARK: - Get date string
    func convertFullDateStringToDateString(withTimeZone string: String) -> String {
        if !hasValue(string){
            return ""
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.S"
        //2015-03-01T00:00:00
        var date: Date? = formatter.date(from: string)
        if date == nil {
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
            date = formatter.date(from: string)
        }
        if date == nil {
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            date = formatter.date(from: string)
        }
        //                formatter.dateFormat = "MMM dd, yyyy"
        formatter.dateFormat = "MMM d, yyyy"
        let dateString = formatter.string(from: date!)
        return dateString
    }
    
    // MARK: - Get date string
    func convertFullDateStringToDateDetailsString(withTimeZone string: String) -> String {
        if !hasValue(string){
            return ""
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.S"
        //2015-03-01T00:00:00
        var date: Date? = formatter.date(from: string)
        if date == nil {
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
            date = formatter.date(from: string)
        }
        if date == nil {
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            date = formatter.date(from: string)
        }
        //                formatter.dateFormat = "MMM dd, yyyy 'at' HH:mm a"
        formatter.dateFormat = "MMM d, yyyy 'at' h:mm a"
        let dateString = formatter.string(from: date!)
        return dateString
    }
    
    // MARK: - Get date string
    func convertFullDateTimeStringToDateString(withTimeZone string: String) -> String {
        if !hasValue(string){
            return ""
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.S"
        //2015-03-01T00:00:00
        var date: Date? = formatter.date(from: string)
        if date == nil {
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
            date = formatter.date(from: string)
        }
        if date == nil {
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            date = formatter.date(from: string)
        }
        //                formatter.dateFormat = "MMM dd, yyyy h:mm a"
        formatter.dateFormat = "MMM d, yyyy h:mm a"
        let dateString = formatter.string(from: date!)
        return dateString
    }
    
    //MARK: - Get time string from dateString
    func getTimeString(withTimeZone string: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.S"
        //2015-03-01T00:00:00
        let date: Date? = formatter.date(from: string)
        formatter.dateFormat = "HH:mm"
        let timeString = formatter.string(from: date!)
        return timeString
    }
    
    //MARK: - Check valid email
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func hasValue(_ object: Any?) -> Bool {
        if object == nil {
            return false
        }
        if object != nil && (object as? NSNull) != NSNull() {
            // Check NSString Class
            if (object is String) || (object is String) {
                let str = object as! String
                if str.count > 0{
                    return true
                }
                if str.isEmpty {
                    return false
                }
            }
            // Check UIImage Class
            if (object is UIImage) {
                if (object as AnyObject).cgImage != nil {
                    return true
                }
                else {
                    return false
                }
            }
            // Check NSArray Class
            if (object is [Any]) {
                if (object as AnyObject).count > 0 {
                    return true
                }
                else {
                    return false
                }
            }
            // Check NSDictionary Class
            if (object is [AnyHashable: Any]) {
                if (object as AnyObject).count > 0 {
                    return true
                }
                else {
                    return false
                }
            }
            // Check Bool Class
            if (object is Bool) {
                return true
            }
            return true
        }
        return false
    }
    
    //MARK: Method of valid email
    func isValidEmail(email:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    func callToBackViewController(withCompletionStatus viewController: AnyClass) -> Bool {
        let viewControllers: [Any] = (navigationController?.viewControllers) ?? [Any]()
        for viewControllerId in 0..<viewControllers.count {
            let obj: Any = viewControllers[viewControllerId]
            if ((obj as AnyObject).isKind(of:viewController.self)) {
                navigationController?.popToViewController(obj as! UIViewController, animated: true)
                return true
            }
        }
        return false
    }
    
    func borderLine(view: AnyObject) {
        if #available(iOS 13.0, *) {
            view.layer.cornerRadius = CGFloat(CORNER_RADIUS)
            view.layer.borderColor = UIColor.lightGray.cgColor
            view.layer.borderWidth = 0.5
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    // Mark: - HexaValue To UIColor
    func colorFromHexString (hexString:String?) -> UIColor {
        if hasValue(hexString) {
            var cString:String = hexString!.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
            
            if (cString.hasPrefix("#")) {
                cString.remove(at: cString.startIndex)
            }
            if ((cString.count) != 6) {
                return UIColor.gray
            }
            var rgbValue:UInt32 = 0
            Scanner(string: cString).scanHexInt32(&rgbValue)
            
            return UIColor(
                red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                alpha: CGFloat(1.0)
            )
        }
        return UIColor.clear
    }
    
//    func pushLoginViewController() {
//        DispatchQueue.main.async {
//            self.tabBarController?.selectedIndex = 0
//            let viewController = LoginViewController(nibName: "LoginViewController", bundle: nil)
//            viewController.hidesBottomBarWhenPushed = true
//            self.navigationController?.pushViewController(viewController, animated: false)
//            self.navigationController?.navigationBar.isHidden = true
//        }
//    }
    
    func removeBackButtonTitle() {
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -1000, vertical: 0), for:UIBarMetrics.default)
    }
}

//MARK: - DRAWET SET UP
extension BaseViewController {
    @objc func drawerClicked() {
        self.mm_drawerController()?.toggle(MMDrawerSide(rawValue: 1)!, animated: true) { _ in }
    }
    
    // MARK: - MMDrawerController reference method
    func mm_drawerController() -> MMDrawerController? {
        var parentViewController: UIViewController? = self.parent
        while parentViewController != nil {
            if (parentViewController?.isKind(of: MMDrawerController.self))! {
                return parentViewController as? MMDrawerController
            }
            parentViewController = parentViewController?.parent
        }
        return nil
    }
}

//MARK: - NAVIGATION BAR
extension BaseViewController {
    func setNavigationBar() {
//        let navItem = UINavigationItem(title: "SomeTitle")
//        let doneItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: nil, action: #selector(drawerClicked))
//        navItem.leftBarButtonItem = doneItem
////        navBar.setItems([navItem], animated: false)
//        self.navigationController?.navigationBar.setItems([navItem], animated: false)
        //ic_menu
        
        
//        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(drawerClicked))
        
        let buttonIcon = UIImage(named: "ic_menu")
        let leftBarButton = UIBarButtonItem(title: "Edit", style: UIBarButtonItem.Style.done, target: self, action: #selector(drawerClicked))
        leftBarButton.image = buttonIcon
        self.navigationItem.leftBarButtonItem = leftBarButton
        UINavigationBar.appearance().barTintColor = UIColor.clear

    }
    
    func getTypeNameBasedOnTab() -> String {
        var type = ""
        let selectedIndex = tabBarController!.selectedIndex
        if(selectedIndex == 0) {
            type = BannerType.TV.rawValue
        } else if(selectedIndex == 1) {
            type = BannerType.MOVIE.rawValue
        } else if(selectedIndex == 2) {
            type = BannerType.SPORTS.rawValue
        }
        
        return type
    }
}

extension BaseViewController {
    class func setImageFromUri(imgView: UIImageView, imageUrl: String, placeHolderImage: String) {
        imgView.sd_setIndicatorStyle(.gray)
        //imgView.sd_addActivityIndicator()
        imgView.sd_setShowActivityIndicatorView(false)
        imgView.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: placeHolderImage))
    }
}

extension BaseViewController {
    func callActivityViewController(info: [String]) -> Void {
        let vc = UIActivityViewController(activityItems: info, applicationActivities: [])
        
        if #available(iOS 11.0, *) {
            vc.excludedActivityTypes = [
                .saveToCameraRoll,
                .postToVimeo,
                .postToTencentWeibo,
                .airDrop,
                .openInIBooks,
                .markupAsPDF,
                .print,
                .copyToPasteboard,
                .assignToContact,
                .addToReadingList,
                .postToFlickr
            ]
        } else {
            vc.excludedActivityTypes = [
                .saveToCameraRoll,
                .postToVimeo,
                .postToTencentWeibo,
                .airDrop,
                .print,
                .copyToPasteboard,
                .assignToContact,
                .addToReadingList,
                .postToFlickr,
                .openInIBooks
            ]
        }
        
        self.present(vc, animated: true, completion: nil)
        if let popOver = vc.popoverPresentationController {
            popOver.sourceView = self.view
        }
    }
}
