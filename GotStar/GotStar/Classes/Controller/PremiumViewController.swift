//
//  PremiumViewController.swift
//  GotStar
//
//  Created by Admin on 12/7/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import GoogleMobileAds


class PremiumViewController: BaseViewController {
    
    var typeName = String()
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet weak var scrollContentView: UIView!
    
    var popularInNameList = [String]()
    var popularInItemList = [[BannerModel]?]()
    var serviceCallPopularList = [String]()
    
    
    //For banner
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var bannerViewHeightConstraint: NSLayoutConstraint!
    var isFromDrawerView = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpForAds()
        typeName = BannerType.TV.rawValue
        setNavigationBar()
        
        addScrollView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
        imageView.contentMode = .scaleAspectFit
        
        let image = UIImage(named: "ic_navi.png")
        imageView.image = image
        
        navigationItem.titleView = imageView
        popularInItemList.removeAll()
        popularInNameList.removeAll()
        serviceCallPopularList.removeAll()
        if(!session.isPremiumTabActive) {
            premiumVideoListService()//Service
            setActiveTab()
        }
    }
    
    func setActiveTab() {
        session.isTVTabActive = false
        session.iSMovieTabActive = false
        session.isSportsTabActive = false
        session.isNewsTabActive = false
        session.isPremiumTabActive = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //        if(isFromDrawerView) {
        //            self.navigationController?.setNavigationBarHidden(true, animated: animated)
        //        }
        scrollView.setContentOffset(.zero, animated: false)
        self.navigationItem.title = " "
    }
    
    func addScrollView() {
        // _ = contentView.subviews.map { $0.removeFromSuperview() }
        
        //Added Sales Scroll View
        scrollView.frame = CGRect(x: 0.0, y: 0, width: contentView.frame.width, height: contentView.frame.height)
        contentView.addSubview(scrollView)
        //scrollContentViewHeightConstraint.constant = 400
        self.view.layoutIfNeeded()
    }
    
}



//MARK: Service Call methods
extension PremiumViewController {
    
    func premiumVideoListService() {
        
        self.setProgressIndicatorWithTitle("Loading...", andDetail: "")
        self.showProgressIndicator(true)
        let premiumVideoCount = 4
        popularInItemList.append(nil)
        popularInItemList.append(nil)
        popularInItemList.append(nil)
        popularInItemList.append(nil)
        
        _ = Timer.scheduledTimer(withTimeInterval: 5, repeats: false, block: { (timer) in
            if(self.serviceCallPopularList.count != premiumVideoCount) {
                self.hideProgressIndicator(true)
                
                self.self.loadPopuparList()
            } else {
                //                                print("..................End..................")
            }
        })
        
        for index in 0..<premiumVideoCount {
            
            
            var typeString = ""
            if(index == 0) {
                typeString = BannerType.TV.rawValue
                
            } else if(index == 1) {
                typeString = BannerType.MOVIE.rawValue
            }  else if(index == 2) {
                typeString = BannerType.SPORTS.rawValue
            } else if(index == 3) {
                typeString = BannerType.NEWS.rawValue
            }
            
            premiumVideoByType(type: typeString, typeCount: premiumVideoCount)
        }
    }
    
    //
    func premiumVideoByType(type: String, typeCount: Int) {
        if(session.isInternetAvailable()) {
            // Animate Activity Indicator
            
            
            DispatchQueue.global(qos: .userInteractive).async {
                let (isSuccess, list) = SessionManager.getInstance().premiumVideoList(type: type)
                DispatchQueue.main.async {
                    self.serviceCallPopularList.append("called")
                    
                    if(isSuccess) {
                        if(list!.count > 0) {
                            self.popularInNameList.append("Top Bonus Today")
                            if(BannerType.TV.rawValue == type)
                            {
                                self.popularInItemList[0] = list!
                            }
                            else if(BannerType.MOVIE.rawValue == type)
                            {
                                self.popularInItemList[1] = list!
                            }
                            else if(BannerType.SPORTS.rawValue == type)
                            {
                                self.popularInItemList[2] = list!
                            }
                            else if(BannerType.NEWS.rawValue == type)
                            {
                                self.popularInItemList[3] = list!
                            }
                        }
                    } else {
                        print("False")
                    }
                    if(typeCount == self.serviceCallPopularList.count) {
                        self.hideProgressIndicator(true)
                        
                        self.loadPopuparList()
                        
                    }
                }
            }
        } else {
            Toast.showNegativeMessage(message: Constants.OPPS_NO_INTERNET_MESSAGE)
        }
    }
    
    func loadPopuparList()
    {
        let bannerHeight:Int = Int(0)
        let popularViewHeight:CGFloat = 165.0
        if self.popularInItemList.indices.contains(0)
        {
            for index in 0..<self.popularInItemList.count
            {
                if (self.popularInItemList[index] != nil && index < self.popularInNameList.count)
                {
                    if index % 2 == 1
                    {
                        let popularView = PopularViewFull(frame: CGRect(x: 0.0, y: CGFloat(bannerHeight + (index * Int(popularViewHeight)) + (Int(floor(Double(Float(index) / 2.0))) * 150)), width: (self.scrollContentView.frame.width), height: 315))
                        popularView.layoutIfNeeded()
                        popularView.loadData(title: self.popularInNameList[index], list: self.popularInItemList[index]!, nav: self.navigationController!)
                        self.scrollContentView.addSubview(popularView)
                    }
                    else
                    {
                        let popularView = PopularView(frame: CGRect(x: 0.0, y: CGFloat(bannerHeight + (index * Int(popularViewHeight)) + (Int(floor(Double(Float(index) / 2.0))) * 150)), width: (self.scrollContentView.frame.width), height: popularViewHeight))
                        popularView.layoutIfNeeded()
                        popularView.loadData(title: self.popularInNameList[index], list: self.popularInItemList[index]!, nav: self.navigationController!)
                        self.scrollContentView.addSubview(popularView)
                    }
                }
            }
            let conSize = (Int(floor(Double(Float(popularInItemList.count) / 2.0))) * 150)
            
            self.scrollView.contentSize = CGSize(width: self.contentView.frame.width, height: CGFloat(bannerHeight + (self.popularInItemList.count * Int(popularViewHeight)) + 20 + conSize))
            self.scrollContentView.frame = CGRect(x: 0, y: 0, width: self.scrollContentView.frame.size.width, height: CGFloat(bannerHeight + (self.popularInItemList.count * Int(popularViewHeight)) + 20 + conSize))
            
            self.view.layoutIfNeeded()
        }
    }
}

extension PremiumViewController: GADBannerViewDelegate {
    func setUpForAds() {
        let banner_ad_value = self.session.generalSettingDic["banner_ad"]
        if(banner_ad_value == "yes") {
            self.bannerViewHeightConstraint.constant = 50
            
            bannerView.adUnitID = self.session.generalSettingDic["banner_adid"]
            bannerView.rootViewController = self
            bannerView.load(GADRequest())
            bannerView.delegate = self
        } else {
            self.bannerViewHeightConstraint.constant = 0
        }
        self.view.layoutIfNeeded()
    }
    
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: Error) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
}
