//
//  ChannelDetailViewController.swift
//  GotStar
//
//  Created by Admin on 12/7/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class ChannelDetailViewController: BaseViewController {
    
    @IBOutlet weak var playBtn: UIButton!

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    
    @IBOutlet weak var watchlistView: UIView!
    @IBOutlet weak var shareView: UIView!
    
    @IBOutlet weak var listTitleLbl: UILabel!
    @IBOutlet weak var populatChannelCollectionView: UICollectionView!
    var popularChannelList = [ChannelModel]()
    
    //Passing
    var model = ChannelModel()
    //    var isTVTab = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTheme()
        self.populatChannelCollectionView.register(UINib(nibName:"PopularSpiritsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PopularSpiritsCollectionViewCell")
        self.title = model.channel_name
        
        
        self.loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.tabBarController?.tabBar.isHidden = true
        popularChannelListService()
    }
    
    func setTheme() {
        watchlistView.backgroundColor = .PRIMERY_COLOR
        shareView.backgroundColor = .PRIMERY_COLOR
        playBtn.tintColor = .PRIMERY_COLOR
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = " "
    }
    
    func loadData() {
        BaseViewController.setImageFromUri(imgView: imageView, imageUrl: model.channel_image, placeHolderImage: "ic_place_holder")
        
        lbl1.text = model.channel_name
        lbl2.text = ""
        lbl3.text = model.channel_desc
        
        listTitleLbl.text = "Popular Channels"
        
    }
    
    @IBAction func playClicked(_ sender: Any) {
        let videoURL = URL(string: model.channel_url)
        let player = AVPlayer(url: videoURL!)
        let dmPlayerViewController = AVPlayerViewController()
        dmPlayerViewController.player = player
        self.present(dmPlayerViewController, animated: true) {
            dmPlayerViewController.player!.play()
        }
    }
    
    @IBAction func watchlistClicked(_ sender: Any) {
        if(session.isUserLogin) {
            addWatchService()
        } else {
            let vc = LoginViewController(nibName: "LoginViewController", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func shareClicked(_ sender: Any) {
        let appName = Bundle.appName()
        self.callActivityViewController(info: [appName, model.channel_name])
    }
    
}

extension ChannelDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return popularChannelList.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (self.view.frame.width / 2) - 25
        return CGSize(width: width, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PopularSpiritsCollectionViewCell", for: indexPath) as! PopularSpiritsCollectionViewCell
        cell.backgroundColor = .orange
        
        let obj = popularChannelList[indexPath.row]
        BaseViewController.setImageFromUri(imgView: cell.imageView, imageUrl: obj.channel_image, placeHolderImage: "ic_place_holder")
        cell.nameLbl.text = obj.channel_name
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
//        let obj = list[indexPath.row]
//        let vc = DetailViewController(nibName: "DetailViewController", bundle: nil)
//        vc.model = obj
//        self.navigationController!.pushViewController(vc, animated: true)
        
        model = popularChannelList[indexPath.row]
        loadData()
    }
    
    
    
}


//MARK: Service Call methods
extension ChannelDetailViewController {
    
    func popularChannelListService() {
        if(session.isInternetAvailable()) {
            // Animate Activity Indicator
            self.setProgressIndicatorWithTitle("Loading...", andDetail: "")
            self.showProgressIndicator(true)
            
            DispatchQueue.global(qos: .userInteractive).async {
                
                let (isSuccess, list) = SessionManager.getInstance().popularChannelList(c_id: self.model.id)
                
                DispatchQueue.main.async {
                    self.hideProgressIndicator(true)
                    if(isSuccess && list?.count ?? 0 > 0) {
                        self.popularChannelList = list!
                        self.populatChannelCollectionView.reloadData()
                    } else {
                        print("False")
                    }
                }
            }
        } else {
            
            Toast.showNegativeMessage(message: Constants.OPPS_NO_INTERNET_MESSAGE)
        }
    }
    
    
    func addWatchService() {
        if(session.isInternetAvailable()) {
            // Animate Activity Indicator
            self.setProgressIndicatorWithTitle("Loading...", andDetail: "")
            self.showProgressIndicator(true)
            
            DispatchQueue.global(qos: .userInteractive).async {
                
                let (isSuccess, responseString) = SessionManager.getInstance().addWatchlist(user_id: self.session.profileModel.id, tv_id: "", a_id: "", c_id: self.model.id)
                
                DispatchQueue.main.async {
                    self.hideProgressIndicator(true)
                    if(isSuccess ) {
                        Toast.showPositiveMessage(message: responseString!)
                    } else {
                        print("False")
                    }
                }
            }
        } else {
            
            Toast.showNegativeMessage(message: Constants.OPPS_NO_INTERNET_MESSAGE)
        }
    }
}
