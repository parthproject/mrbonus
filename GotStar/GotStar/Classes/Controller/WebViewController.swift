//
//  DetailViewController.swift
//  GotStar
//
//  Created by Admin on 12/7/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import WebKit


class WebViewController: BaseViewController, WKNavigationDelegate {
    
    @IBOutlet weak var webview: WKWebView!
    var urls: URL!
    
    
    override func loadView() {
        webview = WKWebView()
        webview.navigationDelegate = self
        
        view = webview
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
        imageView.contentMode = .scaleAspectFit

        let image = UIImage(named: "ic_navi.png")
        imageView.image = image

        navigationItem.titleView = imageView
        
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        let myRequest = URLRequest(url: urls!)
        webview.load(myRequest)
        webview.load(URLRequest(url: urls))
        webview.allowsBackForwardNavigationGestures = true
        
        
        let value = UIInterfaceOrientation.landscapeLeft.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
        }
          
        override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
            return .landscape
        }
          
        override var shouldAutorotate: Bool {
            return true
        }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Show the navigation bar on other view controllers
         let value = UIInterfaceOrientation.portrait.rawValue
                   UIDevice.current.setValue(value, forKey: "orientation")
    }
   
}
