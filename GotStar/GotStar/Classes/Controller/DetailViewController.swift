//
//  DetailViewController.swift
//  GotStar
//
//  Created by Admin on 12/7/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import WebKit
import AVKit
import AVFoundation
import DailymotionPlayerSDK
import VIMVideoPlayer
import YouTubePlayer

class DetailViewController: BaseViewController {
    

    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var webview: WKWebView!
    @IBOutlet weak var youTubePlayerView: YouTubePlayerView!
    @IBOutlet weak var vimVPlayerView: VIMVideoPlayerView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!

    @IBOutlet weak var downloadView: UIView!
    @IBOutlet weak var watchlistView: UIView!
    @IBOutlet weak var shareView: UIView!
    
    @IBOutlet weak var listTitleLbl: UILabel!
    @IBOutlet weak var recentTableView: UITableView!
    var recentList = [BannerModel]()
     var videoUrl = ""
    // The player container. See setupPlayerViewController()
    var isPlayerFullscreen = false
    @IBOutlet private var dailymotionView: UIView!
    @IBOutlet weak var dailymotionViewHeightConstaint: NSLayoutConstraint!
    private lazy var dmPlayerViewController: DMPlayerViewController = {
        // If you have an OAuth token, you can pass it to the player to hook up
        // a user's view history.
        let parameters: [String: Any] = [
            "fullscreen-action": "trigger_event", // Trigger an event when the users toggles full screen mode in the player
            "sharing-action": "trigger_event" // Trigger an event to share the video to e.g. show a UIActivityViewController
        ]
        let controller = DMPlayerViewController(parameters: parameters)
        controller.delegate = self
        return controller
    }()
    
    //Passing
    var model = BannerModel()
    //    var isTVTab = Bool()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTheme()
        recentTableView.tableFooterView = UIView()
        videoPlayerView()
                
        self.loadData()
        detailService()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if(model.tvs_name.count > 0) {
            self.title = model.tvs_name
        } else {
            self.title = model.tvv_name
        }
        
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    
    
    func setTheme() {
        downloadView.backgroundColor = .PRIMERY_COLOR
        watchlistView.backgroundColor = .PRIMERY_COLOR
        shareView.backgroundColor = .PRIMERY_COLOR
        playBtn.tintColor = .PRIMERY_COLOR
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = " "
    }
    
    func loadData() {
        //        BaseViewController.setImageFromUri(imgView: imageView, imageUrl: model.tvs_image, placeHolderImage: "ic_place_holder")
        
        dailymotionView.isHidden = true
        imageView.isHidden = false
        imageView.alpha = 0.6;
        playBtn.isHidden = false
        vimVPlayerView.isHidden = true
        
        var imgUrl = ""
        if(model.tvv_thumbnail.contains("https://")) {
            imgUrl = model.tvv_thumbnail
        } else if(model.tvs_image.contains("https://")) {
            imgUrl = model.tvs_image
        }
        
        if(self.tabBarController?.selectedIndex == 0) {
            BaseViewController.setImageFromUri(imgView: imageView, imageUrl: imgUrl, placeHolderImage: "ic_place_holder")
            lbl2.text = model.tvs_name + " - 0 Episodes"
        } else {
            BaseViewController.setImageFromUri(imgView: imageView, imageUrl: imgUrl, placeHolderImage: "ic_place_holder")
            lbl2.text = ""
        }
        lbl1.text = model.tvv_name
        lbl3.text = model.tvv_description
        
        if(model.v_type == "tv") {
            listTitleLbl.text = "Recent Episodes"
        } else {
            listTitleLbl.text = "Related Games"
        }
    }
    
    @IBAction func playVideo(_ sender: UIButton) {

        if(model.is_premium == "0") {

            dailymotionView.isHidden = true
            imageView.isHidden = true
            playBtn.isHidden = true
            vimVPlayerView.isHidden = true
            
           
            if (model.tvv_video_type == "Youtube Video") {
                //https://github.com/gilesvangruisen/Swift-YouTube-Player
                imageView.isHidden = true
                playBtn.isHidden = true
                //            videoUrl = "https://www.youtube.com/watch?v=WkmoDpbCk34"
                videoUrl = model.tvv_video_url
                
                let myVideoURL = NSURL(string: videoUrl)
                youTubePlayerView.loadVideoURL(myVideoURL! as URL)
            } else if (model.tvv_video_type == "Server Video" || model.tvv_video_type == "External Video"){
                //videoUrl = "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4"
                imageView.isHidden = false
                playBtn.isHidden = false
                
                if(model.tvv_video.count > 0) {
                    videoUrl = BASE_URL + BASE_URL_VIDEO + model.tvv_video
                } else {
                    videoUrl = model.tvv_video_url
                }
                
                let videoURL = URL(string: videoUrl)
                
                //The target function
                        
                        
                let vc = WebViewController(nibName: "WebViewController", bundle: nil)
                vc.urls = videoURL
                //        vc.isTVTab = true
                        self.navigationController?.pushViewController(vc, animated: true)
                    
                
                
                
            }
        else if (model.tvv_video_type == "Daily Motion") {
                        //https://github.com/dailymotion/dailymotion-swift-player-sdk-ios
                        //https://www.dailymotion.com/video/x3dzu8m
                        dailymotionView.isHidden = false
                        
                        var videoID = ""
                        if(model.tvv_video_url.contains("watch")) {
                            if let range = model.tvv_video_url.range(of: "v=") {
                                let id = model.tvv_video_url[range.upperBound...]
                                print(id)
                                videoID = String(id)
                            }
                        } else if(model.tvv_video_url.contains("dailymotion.com")) {
                            
                            if let range = model.tvv_video_url.range(of: "/video/") {
                                let id = model.tvv_video_url[range.upperBound...]
                                print(id)
                                videoID = String(id)
                            }
                        } else {
                            Toast.showNegativeMessage(message: "dailymotion Video link is not Valid")
                            return
                        }
                        
                        //            dmPlayerViewController.load(videoId: "x3dzu8m")
                        dmPlayerViewController.load(videoId: videoID)
                    } else if (model.tvv_video_type == "Vimeo Video") { // Not working
                        //https://github.com/vimeo/VIMVideoPlayer
                        vimVPlayerView.isHidden = false
                        self.vimVPlayerView.player.setURL(NSURL(fileURLWithPath: "https://vimeo.com/384747507") as URL)
                        //            self.vimVPlayerView.player.setURL(NSURL(fileURLWithPath: model.tvv_video_url) as URL)
                        self.vimVPlayerView.player.play()
                    }
                } else         if(model.is_premium == "1" && session.isUserLogin) {

            videoUrl = model.tvv_video_url
            let videoURL2 = URL(string: videoUrl)
                     let vc = WebViewController(nibName: "WebViewController", bundle: nil)
                                   vc.urls = videoURL2
                                   //        vc.isTVTab = true
                                           self.navigationController?.pushViewController(vc, animated: true)
                
            }
            
             else {
                let vc = LoginViewController(nibName: "LoginViewController", bundle: nil)
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
            }
            
    
    @IBAction func bonusClicked(_ sender: Any) {
        
              
        loadData2()
            
    }
    
    
    func loadData2() {
        
       
        
        let strURL = session.generalSettingDic["contact"]
        let url2 = URL(string: strURL!)
        let vc = WebViewController(nibName: "WebViewController", bundle: nil)
                      vc.urls = url2
                      //        vc.isTVTab = true
                              self.navigationController?.pushViewController(vc, animated: true)
                          
        
       

        
    }
    
    @IBAction func watchlistClicked(_ sender: Any) {
        if(session.isUserLogin) {
            watchlistService()
        } else {
            let vc = LoginViewController(nibName: "LoginViewController", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func shareClicked(_ sender: Any) {
        let appName = Bundle.appName()
        self.callActivityViewController(info: [appName, model.tvv_name])
    }
    
}

//MARK: - TableView delegates
extension DetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recentList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 90
        
        // Swift 4.2 onwards
        //return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("RecentTableViewCell", owner: self, options: nil)?.first as! RecentTableViewCell
        cell.selectionStyle = .none
        
        let obj = recentList[indexPath.row]
        BaseViewController.setImageFromUri(imgView: cell.imgView, imageUrl: obj.tvv_thumbnail, placeHolderImage: "ic_place_holder")
        cell.nameLbl.text = obj.tvv_name
        cell.timeLbl.text = obj.tvv_date
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        self.model = recentList[indexPath.row]
        dmPlayerViewController.pause()
        loadData()
    }
    
}

//MARK: Service Call methods
extension DetailViewController {
    
    func detailService() {
        if(session.isInternetAvailable()) {
            // Animate Activity Indicator
            self.setProgressIndicatorWithTitle("Loading...", andDetail: "")
            self.showProgressIndicator(true)
            
            let selectedTabIndex = self.tabBarController?.selectedIndex
            
            DispatchQueue.global(qos: .userInteractive).async {
                
                var success = false
                var list:[BannerModel]?
                
                if(selectedTabIndex == 0) {
                    let (isSuccess, serviceList) = SessionManager.getInstance().getTVDetail(isTV: true, ftvs_id: self.model.tvs_id, type: nil, fc_id: nil)
                    success = isSuccess
                    
                    list = serviceList!
                } else {
                    let (isSuccess, serviceList) = SessionManager.getInstance().getTVDetail(isTV: false, ftvs_id: nil, type: self.model.v_type, fc_id: self.model.fc_id)
                    success = isSuccess
                    list = serviceList ?? nil
                    
                    //                    self.model = serviceList![0]
                    
                }
                
                DispatchQueue.main.async {
                    self.hideProgressIndicator(true)
                    if(success && list?.count ?? 0 > 0) {
                        if(selectedTabIndex == 0) {
                            self.model = list![0]
                            self.loadData()
                        } else {
                            if let list = list {
                                for index in 0..<list.count {
                                    let obj = list[index]
                                    if(obj.tvv_id == self.model.tvv_id) {
                                        self.model = list[index]
                                        break
                                    }
                                }
                            }
                        }
                        
                        self.recentList = list!.reversed()
                        self.recentTableView.reloadData()
                        
                    } else {
                        print("False")
                    }
                }
            }
        } else {
            Toast.showNegativeMessage(message: Constants.OPPS_NO_INTERNET_MESSAGE)
        }
    }
    
    
    
    func watchlistService() {
        if(session.isInternetAvailable()) {
            // Animate Activity Indicator
            self.setProgressIndicatorWithTitle("Loading...", andDetail: "")
            self.showProgressIndicator(true)
            
            DispatchQueue.global(qos: .userInteractive).async {
                
                let (isSuccess, responseString) = SessionManager.getInstance().addWatchlist(user_id: self.session.profileModel.id, tv_id: self.model.tvs_id, a_id: "", c_id: "")
                
                DispatchQueue.main.async {
                    self.hideProgressIndicator(true)
                    if(isSuccess ) {
                        Toast.showPositiveMessage(message: responseString!)
                    } else {
                        print("False")
                    }
                }
            }
        } else {
            
            Toast.showNegativeMessage(message: Constants.OPPS_NO_INTERNET_MESSAGE)
        }
    }
}

//MARK: - DM VIDEO Player supporting methods
extension DetailViewController: DMPlayerViewControllerDelegate {
    
    fileprivate func videoPlayerView() {
        dailymotionView.isHidden = true
        vimVPlayerView.isHidden = true
        vimPlayerInit()
        setupPlayerViewController()
    }
    
    // Add the player to your view. e.g. add a container on your storyboard
    // and add the player's view as subview to that
    private func setupPlayerViewController() {
        addChild(dmPlayerViewController)
        
        let view = dmPlayerViewController.view!
        dailymotionView.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        // Make the player's view fit our container view
        NSLayoutConstraint.activate([
            view.leadingAnchor.constraint(equalTo: dailymotionView.leadingAnchor),
            view.trailingAnchor.constraint(equalTo: dailymotionView.trailingAnchor),
            view.topAnchor.constraint(equalTo: dailymotionView.topAnchor),
            view.bottomAnchor.constraint(equalTo: dailymotionView.bottomAnchor)
        ])
    }
    
    private func toggleFullScreen() {
        // Keep track of the orientation via an isPlayerFullscreen bool
        isPlayerFullscreen = !isPlayerFullscreen
        updateDeviceOrientation()
        updatePlayerSize()
    }
    
    private func updateDeviceOrientation() {
        let orientation: UIDeviceOrientation = isPlayerFullscreen ? .landscapeLeft : .portrait
        UIDevice.current.setValue(orientation.rawValue, forKey: #keyPath(UIDevice.orientation))
    }
    
    private func updatePlayerSize() {
        if isPlayerFullscreen {
            dailymotionViewHeightConstaint.constant = self.view.frame.height
        } else {
            // Keep track of the initial player's height, e.g. via a didSet handler in the constraint outlet
            dailymotionViewHeightConstaint.constant = imageView.frame.height
        }
    }
    
    // The delegate has 4 mandatory functions
    
    func player(_ player: DMPlayerViewController, didReceiveEvent event: PlayerEvent) {
        // Sends player events of either .namedEvent(name: String, data: [String: String]?) or .timeEvent(name: String, time: Double)
        switch event {
        case .namedEvent(let name, _) where name == "fullscreen_toggle_requested":
            //        toggleFullScreen()
            print("fullscreen")
        default:
            break
        }
    }
    
    func player(_ player: DMPlayerViewController, openUrl url: URL) {
        // Sent when a user taps on an ad that can display more information
    }
    
    func playerDidInitialize(_ player: DMPlayerViewController) {
        // Sent when the player has finished initializing
    }
    
    func player(_ player: DMPlayerViewController, didFailToInitializeWithError error: Error) {
        // Sent when the player failed to initialized
    }
    
}

extension DetailViewController: VIMVideoPlayerViewDelegate, VIMVideoPlayerDelegate {
    func vimPlayerInit() {
        // Configure the player as needed
        self.vimVPlayerView.player.isLooping = true
        self.vimVPlayerView.player.disableAirplay()
        self.vimVPlayerView.setVideoFillMode(AVLayerVideoGravity.resizeAspectFill.rawValue)
        
        self.vimVPlayerView.delegate = self
    }
}
