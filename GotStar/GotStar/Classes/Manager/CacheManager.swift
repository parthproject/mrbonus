//
//  CacheManager.swift
//  GotoLiquorStoreOwneriOS
//
//  Created by Kuldip Patel on 19/11/18.
//  Copyright © 2018 Kuldip Patel. All rights reserved.
//

import Foundation
let SIXTY_MB: Int = 62914560
let EIGHT_MB: Int = 8388608

class CacheManager: NSObject {
    
    static var instance: CacheManager? = nil
    // App Session Pointer
    class func getInstance() -> CacheManager {
        let lockQueue = DispatchQueue(label: "self")
        lockQueue.sync {
            if instance == nil {
                instance = CacheManager.init()
                let cache = URLCache.shared
                cache.diskCapacity = SIXTY_MB
                cache.memoryCapacity = EIGHT_MB
            }
        }
        return instance!
    }
    
    // Initialization Code
    override init() {
        super.init()
    }
    
    func callSynchronousCachedRequest(_ urlString: String?) -> Data? {
        if !hasValue(urlString) {
            return nil
        }
        let url = URL(string: urlString!)
        let request = URLRequest(url: url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 1000)
        let data = getCachedData(for: request)
        if hasValue(data){
            return data
        }
        else {
            return try? NSURLConnection.sendSynchronousRequest(request, returning: nil)
        }
    }
    
    func getCachedData(for request: URLRequest) -> Data? {
        let cache = URLCache.shared
        let cachedResponse: CachedURLResponse? = cache.cachedResponse(for: request)
        if cachedResponse != nil {
            return (cachedResponse?.data)!
        }
        return nil
    }
    
    func clearCacheData() {
        let cache = URLCache.shared
        cache.removeAllCachedResponses()
    }
    
    func printCacheStats() {
        let cache = URLCache.shared
        let freeCacheDisk: Int = cache.diskCapacity - cache.currentDiskUsage
        let freeMemory: Int = cache.memoryCapacity - cache.currentMemoryUsage
        print("Available Disk: \(freeCacheDisk)")
        print("Available Memory: \(freeMemory)")
    }
    
    
    func hasValue(_ object: Any?) -> Bool {
        if object == nil {
            return false
        }
        if object != nil && (object as? NSNull) != NSNull() {
            // Check NSString Class
            if (object is String) || (object is String) {
                let str = object as! String
                if str.count > 0{
                    return true
                }
                if str.isEmpty {
                    return false
                }
            }
            // Check UIImage Class
            if (object is UIImage) {
                if (object as AnyObject).cgImage != nil {
                    return true
                }
                else {
                    return false
                }
            }
            // Check NSArray Class
            if (object is [Any]) {
                if (object as AnyObject).count > 0 {
                    return true
                }
                else {
                    return false
                }
            }
            // Check NSDictionary Class
            if (object is [AnyHashable: Any]) {
                if (object as AnyObject).count > 0 {
                    return true
                }
                else {
                    return false
                }
            }
            // Check Bool Class
            if (object is Bool) {
                return true
            }
            return true
        }
        return false
    }
}

extension URLSession {
    func sendSynchronousRequest(_ request:URLRequest,
                                timeout:TimeInterval = -1)  -> (Data?,URLResponse?,NSError?)?
    {
        let sem = DispatchSemaphore(value: 0)
        var result:(Data?,URLResponse?,NSError?)
        let task = self.dataTask(with: request, completionHandler: { (theData, theResponse, theError) in
            result = (theData,theResponse,theError as NSError?)
            sem.signal()
        })
        task.resume()
        let t = timeout == -1 ? DispatchTime.distantFuture : DispatchTime.now() + Double(Int64(NSEC_PER_SEC) * Int64(timeout)) / Double(NSEC_PER_SEC)
        let noTimeout = sem.wait(timeout: t)
        if noTimeout == .timedOut {
            return nil
        }
        return result
    }
    
    /// Synchronously launches a URL request, returning JSON or nil on timeout
    func sendSynchronousRequest(_ request:URLRequest,
                                timeout:TimeInterval = -1)  -> [String:Any]?
    {
        guard
            let result:(Data?,URLResponse?,NSError?) = sendSynchronousRequest(request,timeout:timeout),
            let data = result.0,
            let jsonObject = try? JSONSerialization.jsonObject(with: data, options: []),
            let jsonDict = jsonObject as? [String:Any]
            else { return nil }
        
        return jsonDict
    }
}
