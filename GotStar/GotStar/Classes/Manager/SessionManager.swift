//
//  SessionManager.swift
//  GotoLiquorStoreOwneriOS
//
//  Created by Kuldip Patel on 19/11/18.
//  Copyright © 2018 Kuldip Patel. All rights reserved.
//

import Foundation
import CoreLocation
import SystemConfiguration
//import Vision


class SessionManager: NSObject, CLLocationManagerDelegate {
    // MARK: - Initialization Methods
    static var sessionInstance: SessionManager? = nil
    var appInstantId:String?
    var serviceErrorDescription:String = ""
    var serviceErrorMessage:String = ""
    var orientation:String = ""
    
    // MARK: User Location Info
    var locationManager: CLLocationManager?
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var seenError : Bool = false
    var locationFixAchieved : Bool = false
    
    var isFilterCall : Bool = false
    var isStoreDetailsCall : Bool = true
    var isProfileStoreCall : Bool = true
    var isGlobalProduct : Bool = false
    var isProductWithoutPrice : Bool = false
    var sessionEmail: String = ""
    
    //New Variables
    var profileModel = ProfileModel()
    var isUserLogin = false
    var generalSettingDic: [String: String] = [:]
    
    var isTVTabActive = false
    var iSMovieTabActive = false
    var isSportsTabActive = false
    var isNewsTabActive = false
    var isPremiumTabActive = false

    
    // App Session Pointer
    class func getInstance() -> SessionManager {
        let lockQueue = DispatchQueue(label: "self")
        lockQueue.sync {
            if sessionInstance == nil {
                sessionInstance = SessionManager.init()
                print("SessionManager loaded...")
            }
        }
        return sessionInstance!
    }
    
    override init() {
        super.init()
        
        // Initialization code here
        loadSessionDefaults()
        
        getProfileModel()
    }
    
    func loadSessionDefaults() {
        // Update User Location
        if CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != .denied {
            updateUserLocation()
        }
    }
    
    // MARK: - User Location Update Helper Methods
    func updateUserLocation() {
        seenError = false
        locationFixAchieved = false
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        locationManager?.requestWhenInUseAuthorization()
        locationManager?.distanceFilter = kCLDistanceFilterNone
        locationManager?.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        latitude = (location?.coordinate.latitude)!
        longitude = (location?.coordinate.longitude)!
        //perform(#selector(self.stopUpdatingLocation), with: nil, afterDelay: 10.0)
    }
    
    
    @objc func stopUpdatingLocation() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNEW_LOCATION_FETCHED), object: nil)
        locationManager?.stopUpdatingLocation()
        locationManager?.delegate = nil
        //locationManager = nil
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didFailWithError error: Error){
        if (latitude == 0 && longitude == 0) || (latitude == 0.0 && longitude == 0.0) {
            latitude = 0.0
            longitude = 0.0
        }
        locationManager?.stopUpdatingLocation()
        locationManager?.delegate = nil
        //locationManager = nil
        print("Unable to get location: \(error.localizedDescription)")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNEW_LOCATION_FAILED), object: nil)
    }
    
    // MARK: - Application Instance Methods
    // Get Application Delegate
    func appDelegate() -> AppDelegate? {
        return (UIApplication.shared.delegate as? AppDelegate)
    }
    
    // Get Whole Screen
    func getAppScreen() -> UIWindow {
        return (appDelegate()?.window!)!
    }
    
    //MARK: Get AppInstanceId
    private func getAppInstantId() -> String {
        appInstantId = UIDevice.current.identifierForVendor?.uuidString
        if let uuID = appInstantId {
            return uuID
        } else {
            appInstantId = ""
            return ""
        }
    }
    
    /**
     * Check Internet rechability
     *
     * @return Bool
     */
    public func isInternetAvailable() -> Bool {
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
    }
    
    //MARK : AlertMessage
    func showAlertView(title: String, message: String) {
        DispatchQueue.main.async {
            var stringMessage = message
            if (title.count == 0 && message.count == 0) || (title.count > 0 && message.count == 0) {
                stringMessage = "Unknown error occurred"
            }
            let alert = UIAlertController(title: title, message: stringMessage, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    func showAlertViewWithAction(title: String, message: String) {
        DispatchQueue.main.async {
            var stringMessage = message
            if (title.count == 0 && message.count == 0) || (title.count > 0 && message.count == 0) {
                stringMessage = "Unknown error occurred"
            }
            let alert = UIAlertController(title: title, message: stringMessage, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (action: UIAlertAction) -> Void in
                //UIControl().sendAction(#selector(NSXPCConnection.suspend), to: UIApplication.shared, for: nil)
            }))
            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    //    //MARK: - Header Dictionary
    //MARK: - Header Dictionary
    func getHeaderDictionary() -> [AnyHashable: Any] {
        let headerDic = [AnyHashable: Any]()
        //    headerDic["Content-Type"] = "application/json"
        //    headerDic["StoreId"] = "\(storeId)"
        //    headerDic["AppSource"] = "iOS"
        //    if(loginModel.accessToken.count > 0) {
        //        headerDic["Authorization"] = "Bearer \(loginModel.accessToken)"
        //    } else {
        //        headerDic["Authorization"] = "Bearer \(guestUsetToken)"
        //    }
        return headerDic
    }
    
    private func showServiceErrorMessage(response : Response) {
        if(response.errorCode == SERVICE_TIME_OUT_CODE) {
            showAlertViewWithAction(title: response.error, message: response.message)
        }
        else if(response.detailError.count == 0) {
            showAlertView(title: Constants.ALERT, message: response.message)
        } else {
            showAlertView(title: response.message, message: response.detailError)
        }
    }
    
    //MARK: -  ----------------- Service Call --------------------
    
    func login(email:String, password: String) -> (Bool, LoginResponseModel?){
        let request = LoginRequest()
        request.password = password
        request.email = email
        
       let response: LoginResponse = LoginService.getInstance().loginRequest(request)!
       if response.success {
        return (true, response.loginModel)
       } else {
           showServiceErrorMessage(response: response)
           return (false, nil)
       }
    }
    
    func register(name: String, email:String, password: String, phone: String) -> (Bool, String?){
        let request = RegisterRequest()
        request.name = name
        request.password = password
        request.email = email
        request.phone = phone
        
       let response: RegisterResponse = RegisterService.getInstance().registerRequest(request)!
       if response.success {
        return (true, response.User_id)
       } else {
           showServiceErrorMessage(response: response)
           return (false, nil)
       }
    }
    
    func profileFetch(user_id: String) -> Bool {
        let request = ProfileFetchRequest()
        request.user_id = user_id
        
        let response: ProfileFetchResponse = ProfileFetchService.getInstance().profileFetchRequest(request)!
        if response.success {
            profileModel = response.profileModel
            saveProfileModel()
            return true
        } else {
           showServiceErrorMessage(response: response)
            return false
        }
    }
    
    func generalSetting() -> Bool {
        let request = GeneralSettingRequest()
        
        let response: GeneralSettingResponse = GeneralSettingService.getInstance().generalSettingRequest(request)!
        if response.success {
            generalSettingDic.removeAll()
            for index in 0..<response.list.count {
                let obj = response.list[index]
                generalSettingDic[obj.key] = obj.value
            }
            return true
        } else {
            showServiceErrorMessage(response: response)
            return false
        }
    }
    
    func saveProfileModel() {
        isUserLogin = true
        let userModelData = NSKeyedArchiver.archivedData(withRootObject: profileModel)
        UserDefaults.standard.set(userModelData, forKey: "profileModel")
    }
    
    func getProfileModel() {
        if let loadedData = UserDefaults.standard.data(forKey: "profileModel") {
            if let model = NSKeyedUnarchiver.unarchiveObject(with: loadedData) as? ProfileModel {
                profileModel = model
                isUserLogin = true
            }
        }
        
    }
    
    func logout() {
        isUserLogin = false
        UserDefaults.standard.removeObject(forKey: "profileModel")
    }
    
    func getBannerlist(type: String) -> (Bool, [BannerModel]?) {
       let request = BannerListRequest()
        request.type = type
       let response: BannerListResponse = BannerListService.getInstance().bannerListRequest(request)!
       if response.success {
        return (true, response.bannerList)
       } else {
           showServiceErrorMessage(response: response)
           return (false, nil)
       }
    }
    
    func getCategorylist() -> (Bool, [CategoryrModel]?) {
       let request = CategorylistRequest()
       let response: CategorylistResponse = CategorylistService.getInstance().categorylistRequest(request)!
       if response.success {
        return (true, response.categoryList)
       } else {
           showServiceErrorMessage(response: response)
           return (false, nil)
       }
    }
    
    func getPopularIn(type: String, cat_id: String) -> (Bool, [BannerModel]?) {
       let request = PopularInRequest()
        request.type = type
        request.cat_id = cat_id
        
       let response: PopularInResponse = PopularInService.getInstance().popularInRequest(request)!
       if response.success {
            return (true, response.popularInList)
       } else {
           //showServiceErrorMessage(response: response)
           return (false, nil)
       }
    }
    
    func getPopularShow(type: String) -> (Bool, [BannerModel]?) {
       let request = PopularShowRequest()
        request.type = type
        
       let response: PopularShowResponse = PopularShowService.getInstance().popularShowRequest(request)!
       if response.success {
            return (true, response.popularShowList)
       } else {
           //showServiceErrorMessage(response: response)
           return (false, nil)
       }
    }
    
    func getTVDetail(isTV: Bool, ftvs_id: String?, type: String?, fc_id: String?) -> (Bool, [BannerModel]?) {
       let request = TVDetailRequest()
        request.isTV = isTV
        request.ftvs_id = ftvs_id
        request.type = type
        request.fc_id = fc_id
        
       let response: TVDetailResponse = TVDetailService.getInstance().tvDetailRequest(request)!
       if response.success {
            return (true, response.list)
       } else {
           //showServiceErrorMessage(response: response)
           return (false, nil)
       }
    }
    
    func adddownload(user_id: String, tv_id: String, a_id: String) -> (Bool, String?) {
       let request = AddDownloadRequest()
        request.user_id = user_id
        request.tv_id = tv_id
        request.a_id = a_id
        
       let response: AddDownloadResponse = AddDownloadService.getInstance().addDownloadRequest(request)!
       if response.success {
            return (true, response.responseString)
       } else {
           //showServiceErrorMessage(response: response)
           return (false, nil)
       }
    }
    
    func addWatchlist(user_id: String, tv_id: String, a_id: String, c_id: String) -> (Bool, String?) {
       let request = AddWatchlistRequest()
        request.user_id = user_id
        request.tv_id = tv_id
        request.a_id = a_id
        request.c_id = c_id
        
       let response: AddWatchlistResponse = AddWatchlistService.getInstance().addWatchlistRequest(request)!
       if response.success {
            return (true, response.responseString)
       } else {
           //showServiceErrorMessage(response: response)
           return (false, nil)
       }
    }
    
    func getWatchlist(user_id: String) -> (Bool, [BannerModel]?) {
       let request = GetWatchListRequest()
        request.user_id = user_id

       let response: GetWatchListResponse = GetWatchListService.getInstance().getWatchListRequest(request)!
       if response.success {
            return (true, response.list)
       } else {
           //showServiceErrorMessage(response: response)
           return (false, nil)
       }
    }
    
    func removeWatchlist(w_id: String) -> (Bool, String?) {
       let request = RemoveWishListRequest()
        request.w_id = w_id
        
       let response: RemoveWishListResponse = RemoveWishListService.getInstance().removeWishListRequest(request)!
       if response.success {
            return (true, response.responseString)
       } else {
           //showServiceErrorMessage(response: response)
           return (false, nil)
       }
    }
    
    func premiumVideoList(type: String) -> (Bool, [BannerModel]?) {
       let request = PremiumVideoRequest()
        request.type = type
        
       let response: PremiumVideoResponse = PremiumVideoService.getInstance().premiumVideoRequest(request)!
       if response.success {
            return (true, response.list)
       } else {
           //showServiceErrorMessage(response: response)
           return (false, nil)
       }
    }
    
    func topPickForYouList(type: String) -> (Bool, [BannerModel]?) {
       let request = TopPickForYouRequest()
        request.type = type
        
       let response: TopPickForYouResponse = TopPickForYouService.getInstance().topPickForYouRequest(request)!
       if response.success {
            return (true, response.list)
       } else {
           //showServiceErrorMessage(response: response)
           return (false, nil)
       }
    }
    
    func getAllNewsList(type: String) -> (Bool, [BannerModel]?) {
       let request = GetAllNewsRequest()
        request.type = type
        
       let response: GetAllNewsResponse = GetAllNewsService.getInstance().getAllNewsRequest(request)!
       if response.success {
            return (true, response.list)
       } else {
           //showServiceErrorMessage(response: response)
           return (false, nil)
       }
    }
    
    func channelList() -> (Bool, [ChannelModel]?) {
       let request = ChannelListRequest()
        
       let response: ChannelListResponse = ChannelListService.getInstance().channelListRequest(request)!
       if response.success {
            return (true, response.list)
       } else {
           //showServiceErrorMessage(response: response)
           return (false, nil)
       }
    }
    
    func channelDetail() -> (Bool, [ChannelModel]?) {
       let request = ChannelDetailRequest()
        
       let response: ChannelDetailResponse = ChannelDetailService.getInstance().channelDetailRequest(request)!
       if response.success {
            return (true, response.list)
       } else {
           //showServiceErrorMessage(response: response)
           return (false, nil)
       }
    }
    
    func popularChannelList(c_id: String) -> (Bool, [ChannelModel]?) {
       let request = PopularChannelRequest()
        request.c_id = c_id
        
       let response: PopularChannelResponse = PopularChannelService.getInstance().popularChannelRequest(request)!
       if response.success {
            return (true, response.list)
       } else {
           //showServiceErrorMessage(response: response)
           return (false, nil)
       }
    }
}
