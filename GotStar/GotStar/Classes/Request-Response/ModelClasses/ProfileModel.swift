//
//  ProfileModel.swift
//  GotStar
//
//  Created by Admin on 12/8/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

class ProfileModel: NSObject, NSCoding, NSCopying {
    var email = String()
    var id = String()
    var mobile_number = String()
    var fullname = String()
    
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = ProfileModel()
        copy.email = email
        copy.id = id
        copy.mobile_number = mobile_number
        copy.fullname = fullname
        return copy
    }
    
    // MARK: - NSCoding Protocols
    func encode(with encoder: NSCoder) {
        encoder.encode(email, forKey: "email")
        encoder.encode(id, forKey: "id")
        encoder.encode(mobile_number, forKey: "mobile_number")
        encoder.encode(fullname, forKey: "fullname")
    }
    
    required init(coder decoder: NSCoder) {
        super.init()
        email = (decoder.decodeObject(forKey: "email") as? String) ?? ""
        id = (decoder.decodeObject(forKey: "id") as? String) ?? ""
        mobile_number = (decoder.decodeObject(forKey: "mobile_number") as? String) ?? ""
        fullname = (decoder.decodeObject(forKey: "fullname") as? String) ?? ""
    }
    
    override init() {}
}

