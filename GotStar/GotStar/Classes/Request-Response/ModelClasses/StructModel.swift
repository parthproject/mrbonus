//
//  StructModel.swift
//  GotStar
//
//  Created by Admin on 12/7/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

struct LoginResponseModel {
    var User_id = String()
    var is_premium = String()
}

struct BannerModel {
    var tvv_video = String()
    var cat_image = String()
    var tvs_name = String()
    var tvs_image = String()
    var tvv_video_type = String()
    var tvv_video_url = String()
    var v_type = String()
    var tvs_id = String()
    var tvv_web_img = String()
    var tvv_download = String()
    var type = String()
    var c_status = String()
    var cat_name = String()
    var tvv_description = String()
    var tvv_id = String()
    var tvv_view = String()
    var tvv_date = String()
    var fc_id = String()
    var tvs_date = String()
    var tvs_view = String()
    var c_id = String()
    var c_date = String()
    var tvv_thumbnail = String()
    var tvv_name = String()
    var ftvs_id = String()
    var is_premium = String()
    var w_id = String()
}


struct CategoryrModel {
    var cat_image = String()
    var cat_name = String()
    var c_date = String()
    var c_status = String()
    var c_id = String()
}

struct GeneralSettingModel {
    var id = String()
    var key = String()
    var value = String()
}

struct ChannelModel {
    var is_premium = String()
    var status = String()
    var id = String()
    var channel_name = String()
    var channel_desc = String()
    var channel_view = String()
    var channel_url = String()
    var channel_image = String()
}
