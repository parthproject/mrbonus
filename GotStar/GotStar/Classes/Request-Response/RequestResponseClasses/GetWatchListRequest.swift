//
//  GetWatchListRequest.swift
//  GotStar
//
//  Created by baps on 19/01/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class GetWatchListRequest: Request {
    
    var user_id:String?
    
    override init() {
        super.init()
        serviceMethodName = GET_WATCHLIST_SERVICE_NAME
    }
    
    override func url() -> String {
        
        // Get Server URL
        var urlString: String = "\(serverName)"
        // Append Service Method Name
        urlString = urlString + "/\(serviceMethodName)"
        return urlString
    }
}
