//
//  TopPickForYouRequest.swift
//  GotStar
//
//  Created by baps on 21/01/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class TopPickForYouRequest: Request {
    
    var type:String?
    
    override init() {
        super.init()
        serviceMethodName = TOP_PICK_FOR_YOU_SERVICE_NAME
    }
    
    override func url() -> String {
        
        // Get Server URL
        var urlString: String = "\(serverName)"
        // Append Service Method Name
        urlString = urlString + "/\(serviceMethodName)"
        return urlString
    }
}
