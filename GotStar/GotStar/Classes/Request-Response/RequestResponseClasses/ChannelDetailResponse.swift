//
//  ChannelDetailResponse.swift
//  GotStar
//
//  Created by baps on 22/01/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class ChannelDetailResponse: Response {
    var list = [ChannelModel]()
}
