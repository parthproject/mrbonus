//
//  TVDetailRequest.swift
//  GotStar
//
//  Created by baps on 18/01/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class TVDetailRequest: Request {
    var isTV = false
    //For TV
    var ftvs_id:String?
    
    //For other
    var type: String?
    var fc_id: String?
    
    override init() {
        super.init()
        serviceMethodName = TV_DETAIL_SERVICE_NAME
    }
    
    override func url() -> String {
        if(!isTV) {
            serviceMethodName = MOVIE_DETAIL_SERVICE_NAME
        }
        
        // Get Server URL
        var urlString: String = "\(serverName)"
        // Append Service Method Name
        urlString = urlString + "/\(serviceMethodName)"
        return urlString
    }
}
