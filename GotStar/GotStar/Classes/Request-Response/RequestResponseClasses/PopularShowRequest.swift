//
//  PopularShowRequest.swift
//  GotStar
//
//  Created by baps on 18/01/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class PopularShowRequest: Request {
    var type:String?
    
    override init() {
        super.init()
        serviceMethodName = POPULAR_SHOW_SERVICE_NAME
    }
    
    override func url() -> String {
        // Get Server URL
        var urlString: String = "\(serverName)"
        // Append Service Method Name
        urlString = urlString + "/\(serviceMethodName)"
        return urlString
    }
}
