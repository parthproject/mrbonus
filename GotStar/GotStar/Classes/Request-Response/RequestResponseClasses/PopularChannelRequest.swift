//
//  PopularChannelRequest.swift
//  GotStar
//
//  Created by baps on 22/01/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class PopularChannelRequest: Request {
    
    var c_id:String?
    
    override init() {
        super.init()
        serviceMethodName = POPULAR_CHANNEL_SERVICE_NAME
    }
    
    override func url() -> String {
        
        // Get Server URL
        var urlString: String = "\(serverName)"
        // Append Service Method Name
        urlString = urlString + "/\(serviceMethodName)"
        return urlString
    }
}
