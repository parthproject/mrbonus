//
//  LoginRequest.swift
//  GotoGarageSale
//
//  Created by Admin on 01/12/18.
//  Copyright © 2018 GotoConcept. All rights reserved.
//

import Foundation

class LoginRequest: Request {
    var email:String = ""
    var password:String = ""
    
    override init() {
        super.init()
        serviceMethodName = LOGIN_SERVICE_NAME
    }
    
    override func url() -> String {
        // Get Server URL
        var urlString: String = "\(serverName)"
        // Append Service Method Name
        urlString = urlString + "/\(serviceMethodName)"
        return urlString
    }
}
