//
//  ProfileFetchResponse.swift
//  GotoGarageSale
//
//  Created by Admin on 12/5/18.
//  Copyright © 2018 GotoConcept. All rights reserved.
//

import UIKit

class ProfileFetchResponse: Response {
    var profileModel = ProfileModel()
}
