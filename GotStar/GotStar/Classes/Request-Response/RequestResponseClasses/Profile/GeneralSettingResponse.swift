//
//  GeneralSettingResponse.swift
//  E-Books
//
//  Created by Admin on 12/28/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

class GeneralSettingResponse: Response {
    var list = [GeneralSettingModel]()
}
