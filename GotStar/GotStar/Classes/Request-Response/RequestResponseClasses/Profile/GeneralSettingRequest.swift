//
//  GeneralSettingRequest.swift
//  E-Books
//
//  Created by Admin on 12/28/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

class GeneralSettingRequest: Request {
    
    override init() {
        super.init()
        serviceMethodName = GENERAL_SETTING_SERVICE_NAME
    }
    
    override func url() -> String {
        // Get Server URL
        var urlString: String = "\(serverName)"
        // Append Service Method Name
        urlString = urlString + "/\(serviceMethodName)"
        return urlString
    }
}
