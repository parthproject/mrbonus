//
//  ProfileFetchRequest.swift
//  GotoGarageSale
//
//  Created by Admin on 12/5/18.
//  Copyright © 2018 GotoConcept. All rights reserved.
//

import UIKit

class ProfileFetchRequest: Request {
    var user_id:String = ""
    
    override init() {
        super.init()
        serviceMethodName = PROFILE_SERVICE_NAME
    }
    
    override func url() -> String {
        // Get Server URL
        var urlString: String = "\(serverName)"
        // Append Service Method Name
        urlString = urlString + "/\(serviceMethodName)"
        return urlString
    }
}
