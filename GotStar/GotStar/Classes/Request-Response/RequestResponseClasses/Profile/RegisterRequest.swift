//
//  RegisterRequest.swift
//  GotoGarageSale
//
//  Created by Admin on 11/27/18.
//  Copyright © 2018 GotoConcept. All rights reserved.
//

import UIKit

class RegisterRequest: Request {
    var password:String = ""
    var name:String = ""
    var email:String = ""
    var phone:String = ""
    
    override init() {
        super.init()
        serviceMethodName = REGISTATION_SERVICE_NAME
    }
    
    override func url() -> String {
        // Get Server URL
        var urlString: String = "\(serverName)"
        // Append Service Method Name
        urlString = urlString + "/\(serviceMethodName)"
        return urlString
    }
}
