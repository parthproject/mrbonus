//
//  LoginResponse.swift
//  GotoGarageSale
//
//  Created by Admin on 01/12/18.
//  Copyright © 2018 GotoConcept. All rights reserved.
//

import Foundation

class LoginResponse: Response {
    var loginModel = LoginResponseModel()
}
