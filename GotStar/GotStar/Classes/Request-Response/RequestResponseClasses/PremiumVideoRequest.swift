//
//  PremiumVideoRequest.swift
//  GotStar
//
//  Created by baps on 20/01/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class PremiumVideoRequest: Request {
    
    var type:String?
    
    override init() {
        super.init()
        serviceMethodName = PREMIUM_VIDEO_SERVICE_NAME
    }
    
    override func url() -> String {
        
        // Get Server URL
        var urlString: String = "\(serverName)"
        // Append Service Method Name
        urlString = urlString + "/\(serviceMethodName)"
        return urlString
    }
}
