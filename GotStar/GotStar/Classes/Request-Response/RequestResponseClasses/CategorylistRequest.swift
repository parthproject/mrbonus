//
//  CategorylistRequest.swift
//  GotStar
//
//  Created by Admin on 12/7/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

class CategorylistRequest: Request {
    
    
    override init() {
        super.init()
        serviceMethodName = CATEGORY_SERVICE_NAME
    }
    
    override func url() -> String {
        // Get Server URL
        var urlString: String = "\(serverName)"
        // Append Service Method Name
        urlString = urlString + "/\(serviceMethodName)"
        return urlString
    }
}
