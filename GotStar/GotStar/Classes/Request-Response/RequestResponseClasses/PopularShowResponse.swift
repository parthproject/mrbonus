//
//  PopularShowResponse.swift
//  GotStar
//
//  Created by baps on 18/01/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class PopularShowResponse: Response {
    var popularShowList = [BannerModel]()
}
