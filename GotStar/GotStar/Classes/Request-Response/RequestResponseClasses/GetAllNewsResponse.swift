//
//  GetAllNewsResponse.swift
//  GotStar
//
//  Created by baps on 21/01/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class GetAllNewsResponse: Response {
    var list = [BannerModel]()
}
