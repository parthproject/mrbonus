//
//  BannerListResponse.swift
//  GotStar
//
//  Created by Admin on 12/7/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

class BannerListResponse: Response {
    var bannerList = [BannerModel]()
}
