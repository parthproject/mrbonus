//
//  Response.swift
//  GotoLiquorStoreOwneriOS
//
//  Created by Admin on 25/12/18.
//  Copyright © 2018 Kuldip Patel. All rights reserved.
//

import Foundation

class Response: NSObject {
    var error:String = ""
    var errorCode: Int = 0
    var status:Int = 0
    var message:String = ""
    var timeStamp:String = ""
    var detailError:String = ""
    var success:Bool = false
    var json:[AnyHashable:Any] = [AnyHashable:Any]()
    
    override init() {
        super.init()
    }
    
    func setErrorJson(_ json:[AnyHashable:Any]) -> Void {
        
        self.json = json
        
        let timeStamp: Int? = (json["timeStamp"] as? Int)
        if self.hasValue(timeStamp) {
            self.timeStamp = "\(timeStamp ?? 0)"
        }
        
        let status: Int? = (json["status"] as? Int)
        if self.hasValue(status) {
            self.status = status!
        }
        
        let errorCode: Int? = (json["code"] as? Int)
        if self.hasValue(errorCode) {
            self.errorCode = errorCode!
        }
        
        let message: String? = (json["message"] as? String)
        if self.hasValue(message) {
            self.message = message!
        }
        
        if let status_code = status {
            if status_code < 500 {
                let details: [String]? = (json["details"] as? [String])
                if self.hasValue(details) {
                    self.detailError = details!.joined(separator: ",")
                }
            }
        }
        
        
        let error: String? = (json["error"] as? String)
        if self.hasValue(error) {
            self.error = error!
        }
    }
    
    func hasValue(_ object: Any?) -> Bool {
        if object == nil {
            return false
        }
        if object != nil && (object as? NSNull) != NSNull() {
            // Check NSString Class
            if (object is String) || (object is String) {
                let str = object as! String
                if str.count > 0{
                    return true
                }
                if str.isEmpty {
                    return false
                }
            }
            // Check UIImage Class
            if (object is UIImage) {
                if (object as AnyObject).cgImage != nil {
                    return true
                }
                else {
                    return false
                }
            }
            // Check NSArray Class
            if (object is [Any]) {
                if (object as AnyObject).count > 0 {
                    return true
                }
                else {
                    return false
                }
            }
            // Check NSDictionary Class
            if (object is [AnyHashable: Any]) {
                if (object as AnyObject).count > 0 {
                    return true
                }
                else {
                    return false
                }
            }
            // Check Bool Class
            if (object is Bool) {
                return true
            }
            return true
        }
        return false
    }
}
