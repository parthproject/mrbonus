//
//  HomeBannerView.swift
//  GotoLiquorStoreiOS
//
//  Created by Admin on 29/04/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class HomeBannerView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var clickBtn: UIButton!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var countLbl: UILabel!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commitInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commitInit()
    }
    
    private func commitInit() {
        //We're goint to do stuff here
        Bundle.main.loadNibNamed("HomeBannerView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        setTheme()
    }
    
    func setTheme() {
        typeLbl.textColor = .PRIMERY_COLOR
        countLbl.textColor = .PRIMERY_COLOR
    }

}
