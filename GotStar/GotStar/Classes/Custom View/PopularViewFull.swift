//
//  PopularViewFull.swift
//  MrBonus
//
//  Created by Bhavin Bera on 21/03/21.
//  Copyright © 2021 Admin. All rights reserved.
//

import UIKit

class PopularViewFull: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewAllBtn: UIButton!
    
    var titleName = String()
    var itemList = [BannerModel]()
    var navController = UINavigationController()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commitInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commitInit()
    }
    
    private func commitInit() {
        //We're goint to do stuff here
        Bundle.main.loadNibNamed("PopularViewFull", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        self.collectionView.register(UINib(nibName:"PopularSpiritsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PopularSpiritsCollectionViewCell")

        setTheme()
    }
    
    func setTheme() {
        viewAllBtn.setTitleColor(.PRIMERY_COLOR, for: .normal)
        viewAllBtn.tintColor = .PRIMERY_COLOR
    }
    
    func loadData(title: String, list: [BannerModel], nav: UINavigationController) {
        titleName = title
        navController = nav
        
//        let tabIndex = navController.tabBarController?.selectedIndex
//        titleLbl.text = (tabIndex == 4 ? "Premiun " : "Popular in ") + title
        titleLbl.text = title

        itemList = list
        collectionView.reloadData()
    }
    
    @IBAction func viewAllBtnClicked(_ sender: Any) {
        let vc = ViewAllViewController(nibName: "ViewAllViewController", bundle: nil)
        vc.navigationTitle = titleLbl.text!
        vc.list = itemList
        navController.pushViewController(vc, animated: true)
    }
}

extension PopularViewFull: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemList.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.contentView.frame.size.width-40, height: 260)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PopularSpiritsCollectionViewCell", for: indexPath) as! PopularSpiritsCollectionViewCell
        
        let obj = itemList[indexPath.row]
        
        var imgUrl = ""
//        if(obj.tvv_thumbnail.count > 0) {
//            imgUrl = obj.tvv_thumbnail
//        } else if(obj.tvs_image.count > 0) {
//            imgUrl = obj.tvs_image
//        }
        
        if(obj.tvv_thumbnail.contains("https://")) {
            imgUrl = obj.tvv_thumbnail
        } else if(obj.tvs_image.contains("https://")) {
            imgUrl = obj.tvs_image
        }
        
        BaseViewController.setImageFromUri(imgView: cell.imageView, imageUrl: imgUrl, placeHolderImage: "ic_place_holder")
        cell.nameLbl.text = obj.tvs_name.count > 0 ? obj.tvs_name : obj.tvv_name
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)

        let obj = itemList[indexPath.row]
        let vc = DetailViewController(nibName: "DetailViewController", bundle: nil)
        vc.model = obj
        navController.pushViewController(vc, animated: true)
    }
    
}
