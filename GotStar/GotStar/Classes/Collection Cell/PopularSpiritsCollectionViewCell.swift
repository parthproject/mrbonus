//
//  PopularSpiritsCollectionViewCell.swift
//  GotoLiquorStoreiOS
//
//  Created by Admin on 29/04/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class PopularSpiritsCollectionViewCell: UICollectionViewCell {

    @IBOutlet var imageView: UIImageView!
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var sizeLbl: UILabel!
    @IBOutlet var priceLbl: UILabel!
    @IBOutlet weak var editButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        //F7MG1
//        nameLbl.font = UIFont.systemFont(ofSize: FontSize.F7.rawValue, weight: .medium)
//        nameLbl.textColor = UIColor.G1
//        
//        //F8LG1
//        sizeLbl.font = UIFont.systemFont(ofSize: FontSize.F8.rawValue, weight: .light)
//        sizeLbl.textColor = UIColor.G1
//        
//        //F8SBP4
//        priceLbl.font = UIFont.systemFont(ofSize: FontSize.F8.rawValue, weight: .semibold)
//        priceLbl.textColor = UIColor.P4
    }

}
