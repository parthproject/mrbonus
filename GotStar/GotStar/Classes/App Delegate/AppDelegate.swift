//
//  AppDelegate.swift
//  GotStar
//
//  Created by Admin on 03/12/19.
//  Copyright © 2019 Admin. All rights reserved.
//

//https://github.com/teamSolutionAnalysts/ios-tabbar-with-side-menu

import UIKit
import GoogleMobileAds
import OneSignal
import UnityFramework

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UINavigationControllerDelegate, UITabBarControllerDelegate {
    var unityFramework:UnityFramework?

    var window: UIWindow?
    //    var navigationController: UINavigationController?
    //    var rootViewController: UIViewController?
    var tabBarController: UITabBarController?
    var drawerController: MMDrawerController?
    var application: UIApplication?
    
    func unityFrameworkLoad() -> UnityFramework? {
        let bundlePath = Bundle.main.bundlePath.appending("/Frameworks/UnityFramework.framework")
        if let unityBundle = Bundle.init(path: bundlePath){
            if let frameworkInstance = unityBundle.principalClass?.getInstance(){
                return frameworkInstance
            }
        }
        return nil
    }
    func initAndShowUnity() -> Void {
        if let framework = self.unityFrameworkLoad(){
            self.unityFramework = framework
            self.unityFramework?.setDataBundleId("com.unity3d.framework")
            self.unityFramework?.runEmbedded(withArgc: CommandLine.argc, argv: CommandLine.unsafeArgv, appLaunchOpts: [:])
            self.unityFramework?.showUnityWindow()
        }
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //Add mob
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        
        //one signal
        // Insert OneSignal initialization code from above here
        //START OneSignal initialization code
//        NSDictionary * settings = @{kOSSettingsKeyAutoPrompt : @false};
//
//        // Replace 'YOUR_APP_ID' with your OneSignal App ID.
//        OneSignal.initWithLaunchOptions(launchOptions,
//        appId: ONESIGNAL_APP_ID,
//        handleNotificationAction: nil,
//        settings: settings)
//
//        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
//
//        // Recommend moving the below line to prompt for push after informing the user about
//        //   how your app will use them.
//        OneSignal.promptForPushNotifications(userResponse: { accepted in
//        print("User accepted notifications: \(accepted)")
//        })
//        //END OneSignal initializataion code
        
        OneSignal.setLogLevel(.LL_VERBOSE, visualLevel: .LL_NONE)

          // OneSignal initialization
          OneSignal.initWithLaunchOptions(launchOptions)
          OneSignal.setAppId(ONESIGNAL_APP_ID)

          // promptForPushNotifications will show the native iOS notification permission prompt.
          // We recommend removing the following code and instead using an In-App Message to prompt for notification permission (See step 8)
          OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
          })

        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Done"
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
//        IQKeyboardManager.shared.disabledToolbarClasses = [ForgotPasswordViewController.self]
        
        
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        self.tabBarController = UITabBarController()
        self.tabBarController?.delegate = self
        UITabBar.appearance().tintColor = UIColor.PRIMERY_COLOR
        self.tabBarController?.tabBar.backgroundColor = UIColor(hexString: "#C5B358")
        

//        To change Navigation Bar Background Color
        UINavigationBar.appearance().barTintColor = UIColor.clear
//        To change Back button title & icon color
        UINavigationBar.appearance().tintColor = UIColor.PRIMERY_COLOR
//        To change Navigation Bar Title Color
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.PRIMERY_COLOR]
        
        //-------------- Tab bar setup -------------------
        let tvVC = HomeViewController()
        tvVC.title = "Mr.Bonus Slot Games"
        let movieVC = MovieViewController()
        movieVC.title = "Mr.Bonus Table Games"
        let sportsVC = SportViewController()
        sportsVC.title = "Mr.Bonus Sport Games"
        let newsVC = NewsViewController()
        newsVC.title = "Mr.Bonus Poker Games"
        let premiumVC = PremiumViewController()
        premiumVC.title = "Mr.Bonus Premium"
        
        tvVC.tabBarItem = UITabBarItem(title: "Slot Games", image: UIImage(named: "ic_tab_tv"), selectedImage:UIImage(named: "ic_tab_tv"))
        movieVC.tabBarItem = UITabBarItem(title: "Table Games", image: UIImage(named: "ic_tab_movie"), selectedImage:  UIImage(named: "ic_tab_movie"))
        sportsVC.tabBarItem = UITabBarItem(title: "Sports", image: UIImage(named: "ic_tab_sports"), selectedImage:  UIImage(named: "ic_tab_sports"))
        newsVC.tabBarItem = UITabBarItem(title: "Poker", image: UIImage(named: "ic_tab_news"), selectedImage:  UIImage(named: "ic_tab_news"))
        premiumVC.tabBarItem = UITabBarItem(title: "Premium", image: UIImage(named: "ic_tab_premium"), selectedImage:  UIImage(named: "ic_tab_premium"))
        
        let controllers = [tvVC, movieVC, sportsVC, newsVC, premiumVC]
        self.tabBarController?.viewControllers = controllers.map { UINavigationController(rootViewController: $0)}
        defaultDrawerViewForTabApp()
        //-------------- Tab bar setup >>> End -------------------
        
        
        
        
        //Comment below line if drawer is not required
        //        let viewController = HomeViewController(nibName: "HomeViewController", bundle: nil)
        //        rootViewController = viewController
        //        self.navigationController = UINavigationController(rootViewController: rootViewController!)
        //        defaultDrawerViewForNavigationApp()
        //        window?.makeKeyAndVisible()
        //        navigationController?.setNavigationBarHidden(true, animated: false)
        
        return true
    }
    
    
    
    func defaultDrawerViewForTabApp() {
        let leftDrawer = DrawerViewController()
        
        let navigationViewController: UINavigationController = UINavigationController(rootViewController: self.tabBarController!)
        leftDrawer.centerNavigationController = navigationViewController
        leftDrawer.tabBarController2 = self.tabBarController
        navigationViewController.isNavigationBarHidden = true
        let drawerController = MMDrawerController(center: navigationViewController, leftDrawerViewController: leftDrawer, rightDrawerViewController: nil)
        drawerController?.shouldStretchDrawer = false
        //        drawerController?.showsStatusBarBackgroundView = true
        //        drawerController?.statusBarViewBackgroundColor = .white
        
        //For shadow
        drawerController?.showsShadow = true
        drawerController?.shadowRadius = 3
        
        //        drawerController?.openDrawerGestureModeMask = MMOpenDrawerGestureMode.init(rawValue: 0)
        drawerController?.openDrawerGestureModeMask = MMOpenDrawerGestureMode.custom//Stop gusture open drawer
        //            drawerController?.openDrawerGestureModeMask = MMOpenDrawerGestureMode.all//Start gusture open drawer
        
        drawerController?.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.all
        
        drawerController?.maximumLeftDrawerWidth = UIScreen.main.bounds.size.width - 120
        if IS_IPHONE_5 || IS_IPHONE_4 {
            drawerController?.maximumLeftDrawerWidth = UIScreen.main.bounds.size.width - 80
        }
        drawerController?.maximumRightDrawerWidth = UIScreen.main.bounds.size.width / 2
        //        drawerController?.navigationController!.setNavigationBarHidden(true, animated: false)
        window?.rootViewController = drawerController
        window?.makeKeyAndVisible()
    }
    
    //    func defaultDrawerViewForNavigationApp() {
    //            let leftDrawer = DrawerViewController()
    //            leftDrawer.centerNavigationController = self.navigationController//This is for navigation app
    //            let drawerController = MMDrawerController(center: navigationController, leftDrawerViewController: leftDrawer, rightDrawerViewController: nil)
    //            drawerController?.shouldStretchDrawer = false
    //    //        drawerController?.showsStatusBarBackgroundView = true
    //    //        drawerController?.statusBarViewBackgroundColor = .white
    //
    //            //For shadow
    //            drawerController?.showsShadow = true
    //            drawerController?.shadowRadius = 3
    //
    //            //        drawerController?.openDrawerGestureModeMask = MMOpenDrawerGestureMode.init(rawValue: 0)
    //            drawerController?.openDrawerGestureModeMask = MMOpenDrawerGestureMode.custom//Stop gusture open drawer
    //            //drawerController?.openDrawerGestureModeMask = MMOpenDrawerGestureMode.all//Start gusture open drawer
    //
    //            drawerController?.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.all
    //
    //            drawerController?.maximumLeftDrawerWidth = UIScreen.main.bounds.size.width - 120
    //            if IS_IPHONE_5 || IS_IPHONE_4 {
    //                drawerController?.maximumLeftDrawerWidth = UIScreen.main.bounds.size.width - 80
    //            }
    //            drawerController?.maximumRightDrawerWidth = UIScreen.main.bounds.size.width / 2
    //            window?.rootViewController = drawerController
    //        }
    
    // MARK: UISceneSession Lifecycle
    //
    //    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
    //        // Called when a new scene session is being created.
    //        // Use this method to select a configuration to create the new scene with.
    //        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    //    }
    //
    //    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    //        // Called when the user discards a scene session.
    //        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    //        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    //    }
    
    
}

