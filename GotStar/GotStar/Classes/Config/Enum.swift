//
//  Enum.swift
//  GotStar
//
//  Created by Admin on 12/7/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation


enum BannerType: String {
    case TV  = "tv"
    case MOVIE  = "movie"
    case SPORTS  = "sport"
    case NEWS  = "news"
    case PREMIUM  = "premium"
}
