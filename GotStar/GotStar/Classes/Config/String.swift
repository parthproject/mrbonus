//
//  String.swift
//  GotoGarageSale
//
//  Created by Admin on 17/01/19.
//  Copyright © 2019 GotoConcept. All rights reserved.
//

import Foundation

struct Constants {

   static let ALERT                                 = "Alert"
   static let SUCCESS                               = "Success!"
   static let EMAIL_EMPTY_MESSAGE                   = "Please enter email"
   static let EMAIL_ERROR_MESSAGE                   = "Please enter valid email"
   static let PASSWORD_EMPTY_MESSAGE                = "Please enter password"
   static let PASSWORD_ERROR_MESSAGE                = "Please enter valid password"
   static let PASSWORD_FORMATE_ERROR_MESSAGE        = "Password is invalid. Must have at least one small letter, one capital letter, one number and one special character. No space allowed"
   static let PASSWORD_LENGHT_ERROR_MESSAGE         = "Password must be between 8 to 64 characters"
   static let FULL_NAME_ERROR_MESSAGE               = "Please enter name"
   static let FULL_NAME_LIMIT_ERROR_MESSAGE         = "Name must be between 3 to 64 characters"
   static let PHONE_NUMBER_ERROR_MESSAGE            = "Please enter phone number"
   static let PHONE_NUMBER_VALIDATION               = "Phone number must be 10 digits"
    static let PHONE_NUMBER_VALIDATION_2               = "Please enter valid number"
   static let CHECK_MAIL_MESSAGE                    = "Password reset email sent. Check your email for instructions that will tell you how to reset your password. Once reset, you can use your new password on Sign In screen."
   static let INTERNET_ERROR_TITLE                  = "Sorry"
   static let INTERNET_ERROR_MESSAGE                = "Internet is not reachable"
   static let CURRENT_PASSWORD_ERROR_MESSAGE        = "Please enter old password"
   static let NEW_PASSWORD_ERROR_MESSAGE            = "Please enter new password"
   static let CONFIRM_NEW_PASSWORD_ERROR_MESSAGE    = "Please enter confirm password"
   static let SAME_PASSWORD_ERROR_MESSAGE           = "New password and confirm password should be same"
   static let LOCATION_TEXT                         = "Current Location"
   
   static let THANKS_SIGN_UP_MESSAGE                = "Thanks for signing up. Please check email for verification link."
   static let VERIFICATION_LINK_MESSAGE             = "Verification link has been sent. Kindly check your email."
   static let THANKS_VERIFICATION_MESSAGE           = "Thank you for verifying your email"
   static let ORDER_CANCELED_MESSAGE                = "Your order has been canceled successfully"
   static let PRODUCT_ADDED_TO_CART_MESSAGE         = "Product added to your shopping cart"
   static let DETAIL_UPDATED_MESSAGE                = "Details updated successfully"
   static let OPPS_NO_INTERNET_MESSAGE              = "Oops! No Internet"
   static let AGE_LIMIT_MESSAGE                     = "To shop on this app, you must be at least 21 years or older."
   static let SPACE_OF_PROMOTION                    = "   "
   static let ENTER_PROMOCODE_MESSAGE               = "Please enter promotion code"
   static let SELECT_AT_LEAST_ONE_STORE             = "Please select at least one store"
   
   static let SUBSCRIPTION                          = "Subscription"
   static let SUBSCRIPTION_LIST_EMPTY_MESSAGE       = "No subscription available"
   static let SUBSCRIPTION_TITLE                    = "Please select cities in order to get notification of the new sales happening around."


}
