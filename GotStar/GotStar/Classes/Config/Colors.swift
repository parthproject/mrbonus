//
//  Colors.swift
//  GotoLiquorStoreiOS
//
//  Created by Admin on 03/05/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation


extension UIColor {
    
    static let PRIMERY_COLOR = UIColor.init(hexString: "#df0209")
    static let SECONDARY_COLOR = UIColor.init(hexString: "#7d7d7d")
    static let DARK_TEXT_COLOR = UIColor.black
    static let LIGHT_TEXT_COLOR = UIColor.white
    
//    static let PRIMERY_COLOR = UIColor.red
//    static let SECONDARY_COLOR = UIColor.init(hexString: "#7d7d7d")
//    static let DARK_TEXT_COLOR = UIColor.white
//    static let LIGHT_TEXT_COLOR = UIColor.black

}
