
#define BASE_URL                                        @""
#define IMAGE_URL                                       @"assets/images/app/"
#define BASE_URL_VIDEO                                  @"assets/images/serial/"
#define API_URL                                         @"https://mrbonus.tv/server2/index.php/api"

#define ONESIGNAL_APP_ID                                 @""

#define IS_LOG_ENABLE                                   0

#define RESPONSE_TYPE                                   @"json"

// Not consider location flag if 0.
#define IS_CHECK_LOCATION_FLAG_ENABLE                   0

// Service Control Panel
#define MOCK_SERVICE                                    0
#define LIVE_SERVICE                                    1
#define SERVICE_MODE                                    LIVE_SERVICE

#define CATEGORY_SERVICE                                SERVICE_MODE
#define BANNER_LIST_SERVICE                             SERVICE_MODE
#define POPULAR_IN_SERVICE                              SERVICE_MODE
#define LOGIN_SERVICE                                   SERVICE_MODE
#define REGISTATION_SERVICE                             SERVICE_MODE
#define PROFILE_SERVICE                                 SERVICE_MODE
#define GENERAL_SETTING_SERVICE                         SERVICE_MODE
#define POPULAR_SHOW_SERVICE                            SERVICE_MODE
#define TV_DETAIL_SERVICE                               SERVICE_MODE
#define ADD_DOWNLOAD_SERVICE                            SERVICE_MODE
#define ADD_WATCHLIST_SERVICE                           SERVICE_MODE
#define GET_WATCHLIST_SERVICE                           SERVICE_MODE
#define REMOVE_WATCHLIST_SERVICE                        SERVICE_MODE
#define PREMIUM_VIDEO_SERVICE                           SERVICE_MODE
#define TOP_PICK_FOR_YOU_SERVICE                        SERVICE_MODE
#define GET_ALL_NEWS_SERVICE                            SERVICE_MODE
#define CHANNEL_LIST_SERVICE                            SERVICE_MODE
#define CHANNEL_DETAIL_SERVICE                          SERVICE_MODE
#define POPULAR_CHANNEL_SERVICE                         SERVICE_MODE


#define CATEGORY_SERVICE_NAME                           @"categorylist"
#define BANNER_LIST_SERVICE_NAME                        @"bannerlist"
#define POPULAR_IN_SERVICE_NAME                         @"popular_in"
#define LOGIN_SERVICE_NAME                              @"login"
#define REGISTATION_SERVICE_NAME                        @"registration"
#define PROFILE_SERVICE_NAME                            @"profile"
#define GENERAL_SETTING_SERVICE_NAME                    @"general_setting"
#define POPULAR_SHOW_SERVICE_NAME                       @"popularshow"
#define TV_DETAIL_SERVICE_NAME                          @"tv_video_by_serial_id"
#define MOVIE_DETAIL_SERVICE_NAME                       @"video_by_id"
#define ADD_DOWNLOAD_SERVICE_NAME                       @"add_download"
#define ADD_WATCHLIST_SERVICE_NAME                      @"add_watchlist"
#define GET_WATCHLIST_SERVICE_NAME                      @"getwatchlist"
#define REMOVE_WATCHLIST_SERVICE_NAME                   @"remove_watchlist"
#define PREMIUM_VIDEO_SERVICE_NAME                      @"premium_video"
#define GET_ALL_NEWS_SERVICE_NAME                       @"get_all_news"
#define TOP_PICK_FOR_YOU_SERVICE_NAME                   @"top_pick_for_you"
#define CHANNEL_LIST_SERVICE_NAME                       @"Channellist"
#define CHANNEL_DETAIL_SERVICE_NAME                     @""
#define POPULAR_CHANNEL_SERVICE_NAME                    @"PopularChannellist"


//--- Location Update Notification ---
#define kNEW_LOCATION_FETCHED                           @"NewLocationFetched"
#define kNEW_LOCATION_FAILED                            @"NewLocationFailed"


#define ALERT                                           @"Alert"
#define EMAIL_ERROR_MESSAGE                             @"Please enter email"
#define USERNAME_ERROR_MESSAGE                          @"Please enter email/username"
#define PASSWORD_ERROR_MESSAGE                          @"Please enter password"
#define PASSWORD_LENGHT_ERROR_MESSAGE                   @"Please enter atleast 8 character in password"
#define FULL_NAME_ERROR_MESSAGE                         @"Please enter name"
#define PHONE_NUMBER_ERROR_MESSAGE                      @"Please enter phone number"
#define CHECK_MAIL_MESSAGE                              @"Please check your mail to reset password"
#define INTERNET_ERROR_TITLE                            @"Sorry"
#define INTERNET_ERROR_MESSAGE                          @"Internet is not reachable"
#define CURRENT_PASSWORD_ERROR_MESSAGE                  @"Please enter current password"
#define NEW_PASSWORD_ERROR_MESSAGE                      @"Please enter new password"
#define CONFIRM_NEW_PASSWORD_ERROR_MESSAGE              @"Please enter confirm new password"
#define SAME_PASSWORD_ERROR_MESSAGE                     @"New password and confirm new password does not match"
#define LOCATION_TEXT                                   @"Current Location"
#define TAKE_NEW_PHOTO_MESSAGE                          @"Take new photo"
#define SELECT_FROM_LIBRARY_MESSAGE                     @"Select from library"

#define NO_PRODUCT_MESSAGE                              @"No products available in the store. Please add products to your store."
#define SPACE_OF_PROMOTION                              @"   "
#define REASON_IS_REQUIRED                              @"Reason is required"

#define STATUS_OK                                       200
#define STATUS_BAD_REQUEST                              400
#define STATUS_UNAUTHORIZED                             401
#define STATUS_NOT_FOUND                                404
#define STATUS_INTERNAL_SERVER_ERROR                    500

#define CODE_MISSING_TOKEN                              101
#define CODE_INVALID_TOKEN                              102
#define CODE_EXPIRED_TOKEN                              103
#define SERVICE_TIME_OUT_CODE                           -1


#define CORNER_RADIUS                                    10


#define ORDER_LOAD_COUNT                                 500
#define PRODUCT_LOAD_COUNT                               50
#define PRODUCT_ROW_COUNT                               16
