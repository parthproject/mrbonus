﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String UnityEngine.Purchasing.MiniJson::JsonEncode(System.Object)
extern void MiniJson_JsonEncode_m4891593121DB9BF4CCA15F236D9BFC6AFD643482 ();
// 0x00000002 System.Object UnityEngine.Purchasing.MiniJson::JsonDecode(System.String)
extern void MiniJson_JsonDecode_m8F4B396DE9D11409AC70C11E36F8AC2863A83EF0 ();
// 0x00000003 System.Void UnityEngine.Purchasing.INativeStore::RetrieveProducts(System.String)
// 0x00000004 System.Void UnityEngine.Purchasing.INativeStore::Purchase(System.String,System.String)
// 0x00000005 System.Void UnityEngine.Purchasing.INativeStore::FinishTransaction(System.String,System.String)
// 0x00000006 System.Void UnityEngine.Purchasing.UnityPurchasingCallback::.ctor(System.Object,System.IntPtr)
extern void UnityPurchasingCallback__ctor_mA8AF178AEE5B2A6418D7887D6B750176836DABDA ();
// 0x00000007 System.Void UnityEngine.Purchasing.UnityPurchasingCallback::Invoke(System.String,System.String,System.String,System.String)
extern void UnityPurchasingCallback_Invoke_mD0D48B0F0A62B2BF5B1AF1A14E0278B5252BECA8 ();
// 0x00000008 System.IAsyncResult UnityEngine.Purchasing.UnityPurchasingCallback::BeginInvoke(System.String,System.String,System.String,System.String,System.AsyncCallback,System.Object)
extern void UnityPurchasingCallback_BeginInvoke_m2A4FD86243CB3AE4B316AA506625BC67D0EF9CA5 ();
// 0x00000009 System.Void UnityEngine.Purchasing.UnityPurchasingCallback::EndInvoke(System.IAsyncResult)
extern void UnityPurchasingCallback_EndInvoke_m493B724411466DEA1A0C62CE7DC29AAED74ACF01 ();
// 0x0000000A System.Object UnityEngine.Purchasing.MiniJSON.Json::Deserialize(System.String)
extern void Json_Deserialize_m4DF883C3C0C6387F6E993B2A75A01E991AFAC055 ();
// 0x0000000B System.String UnityEngine.Purchasing.MiniJSON.Json::Serialize(System.Object)
extern void Json_Serialize_mC053F9574354D75C44065DE0F017DB2D00414164 ();
// 0x0000000C System.Boolean UnityEngine.Purchasing.MiniJSON.Json_Parser::IsWordBreak(System.Char)
extern void Parser_IsWordBreak_mA3011F57A07E400EE014A9714DD7923C51B2FBDE ();
// 0x0000000D System.Void UnityEngine.Purchasing.MiniJSON.Json_Parser::.ctor(System.String)
extern void Parser__ctor_mDAC5A4C7BC893137B18685EAEA2DEE02849888DF ();
// 0x0000000E System.Object UnityEngine.Purchasing.MiniJSON.Json_Parser::Parse(System.String)
extern void Parser_Parse_mBCF197967BCE6611EEEE9ADA8B6EC4F284452135 ();
// 0x0000000F System.Void UnityEngine.Purchasing.MiniJSON.Json_Parser::Dispose()
extern void Parser_Dispose_m626C6851FEECD51D7DB2A083F9D145BE9DB406A7 ();
// 0x00000010 System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MiniJSON.Json_Parser::ParseObject()
extern void Parser_ParseObject_mD93811A97FFEB18F9D29153468CEF67AA04A02AC ();
// 0x00000011 System.Collections.Generic.List`1<System.Object> UnityEngine.Purchasing.MiniJSON.Json_Parser::ParseArray()
extern void Parser_ParseArray_mBC4FDFCE01D7CF03BF1B48418E225A0D61C730A5 ();
// 0x00000012 System.Object UnityEngine.Purchasing.MiniJSON.Json_Parser::ParseValue()
extern void Parser_ParseValue_m1D41CA8038B97690306E9D4FD511201860498713 ();
// 0x00000013 System.Object UnityEngine.Purchasing.MiniJSON.Json_Parser::ParseByToken(UnityEngine.Purchasing.MiniJSON.Json_Parser_TOKEN)
extern void Parser_ParseByToken_m536291874E441B572ACF00D350458E1A1CF15891 ();
// 0x00000014 System.String UnityEngine.Purchasing.MiniJSON.Json_Parser::ParseString()
extern void Parser_ParseString_m824A5AD8628784BDAAA515D43357F7D667AACAC6 ();
// 0x00000015 System.Object UnityEngine.Purchasing.MiniJSON.Json_Parser::ParseNumber()
extern void Parser_ParseNumber_m4C75FF6AE1D23D0391737CFE5E0D2DDBC63222B4 ();
// 0x00000016 System.Void UnityEngine.Purchasing.MiniJSON.Json_Parser::EatWhitespace()
extern void Parser_EatWhitespace_mCD7F80C1294FE9B3BD03319A3C2C625D7CC6A485 ();
// 0x00000017 System.Char UnityEngine.Purchasing.MiniJSON.Json_Parser::get_PeekChar()
extern void Parser_get_PeekChar_m4C59D86249D4BDC34034ED8212D7C1DB2F0FDE4D ();
// 0x00000018 System.Char UnityEngine.Purchasing.MiniJSON.Json_Parser::get_NextChar()
extern void Parser_get_NextChar_m46C018191785411310602B3F022D46C3085DED3C ();
// 0x00000019 System.String UnityEngine.Purchasing.MiniJSON.Json_Parser::get_NextWord()
extern void Parser_get_NextWord_m04E6AC4117C72E8B702F5157306EEBD571EBE588 ();
// 0x0000001A UnityEngine.Purchasing.MiniJSON.Json_Parser_TOKEN UnityEngine.Purchasing.MiniJSON.Json_Parser::get_NextToken()
extern void Parser_get_NextToken_m4B6BCBF18A6C1245108CEC0C40721FD08C6DE5C1 ();
// 0x0000001B System.Void UnityEngine.Purchasing.MiniJSON.Json_Serializer::.ctor()
extern void Serializer__ctor_m77DB4100B561FE67B8F93105462632E985B9A31B ();
// 0x0000001C System.String UnityEngine.Purchasing.MiniJSON.Json_Serializer::Serialize(System.Object)
extern void Serializer_Serialize_m5EE777FFD0A4A796C872F774CA5F8D06A707F695 ();
// 0x0000001D System.Void UnityEngine.Purchasing.MiniJSON.Json_Serializer::SerializeValue(System.Object)
extern void Serializer_SerializeValue_m4EFB29AAAB95494D19A50B3C9DA7D20B6DA59B38 ();
// 0x0000001E System.Void UnityEngine.Purchasing.MiniJSON.Json_Serializer::SerializeObject(System.Collections.IDictionary)
extern void Serializer_SerializeObject_mA598CDF8A872E08815AAA88D87ADE6106497EBEE ();
// 0x0000001F System.Void UnityEngine.Purchasing.MiniJSON.Json_Serializer::SerializeArray(System.Collections.IList)
extern void Serializer_SerializeArray_m6CDBD88758627DD3E21B596A24E9A2A20304D1EB ();
// 0x00000020 System.Void UnityEngine.Purchasing.MiniJSON.Json_Serializer::SerializeString(System.String)
extern void Serializer_SerializeString_mFEBA44A6A5824F1903783BEB1B81FDF72D8BCCE4 ();
// 0x00000021 System.Void UnityEngine.Purchasing.MiniJSON.Json_Serializer::SerializeOther(System.Object)
extern void Serializer_SerializeOther_m61D82C0B9FEF0F39DDAFFEB43B851545EB63633D ();
// 0x00000022 System.String UnityEngine.Purchasing.MiniJSON.MiniJsonExtensions::GetString(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String,System.String)
extern void MiniJsonExtensions_GetString_m0B244C8B27E21C4EE034DF9C5E0E4C6AE89D12D0 ();
// 0x00000023 System.String UnityEngine.Purchasing.MiniJSON.MiniJsonExtensions::toJson(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void MiniJsonExtensions_toJson_m2DB3976D2DC0DF3E9E186DEE7854BAEB1F7551C9 ();
// 0x00000024 System.String UnityEngine.Purchasing.MiniJSON.MiniJsonExtensions::toJson(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void MiniJsonExtensions_toJson_mEB129B8C724E52DE7DFD8313520B59ECE1F82AC3 ();
// 0x00000025 System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MiniJSON.MiniJsonExtensions::HashtableFromJson(System.String)
extern void MiniJsonExtensions_HashtableFromJson_m5E34E1DB2C178E140494CD1AF26718CEDC518B5E ();
static Il2CppMethodPointer s_methodPointers[37] = 
{
	MiniJson_JsonEncode_m4891593121DB9BF4CCA15F236D9BFC6AFD643482,
	MiniJson_JsonDecode_m8F4B396DE9D11409AC70C11E36F8AC2863A83EF0,
	NULL,
	NULL,
	NULL,
	UnityPurchasingCallback__ctor_mA8AF178AEE5B2A6418D7887D6B750176836DABDA,
	UnityPurchasingCallback_Invoke_mD0D48B0F0A62B2BF5B1AF1A14E0278B5252BECA8,
	UnityPurchasingCallback_BeginInvoke_m2A4FD86243CB3AE4B316AA506625BC67D0EF9CA5,
	UnityPurchasingCallback_EndInvoke_m493B724411466DEA1A0C62CE7DC29AAED74ACF01,
	Json_Deserialize_m4DF883C3C0C6387F6E993B2A75A01E991AFAC055,
	Json_Serialize_mC053F9574354D75C44065DE0F017DB2D00414164,
	Parser_IsWordBreak_mA3011F57A07E400EE014A9714DD7923C51B2FBDE,
	Parser__ctor_mDAC5A4C7BC893137B18685EAEA2DEE02849888DF,
	Parser_Parse_mBCF197967BCE6611EEEE9ADA8B6EC4F284452135,
	Parser_Dispose_m626C6851FEECD51D7DB2A083F9D145BE9DB406A7,
	Parser_ParseObject_mD93811A97FFEB18F9D29153468CEF67AA04A02AC,
	Parser_ParseArray_mBC4FDFCE01D7CF03BF1B48418E225A0D61C730A5,
	Parser_ParseValue_m1D41CA8038B97690306E9D4FD511201860498713,
	Parser_ParseByToken_m536291874E441B572ACF00D350458E1A1CF15891,
	Parser_ParseString_m824A5AD8628784BDAAA515D43357F7D667AACAC6,
	Parser_ParseNumber_m4C75FF6AE1D23D0391737CFE5E0D2DDBC63222B4,
	Parser_EatWhitespace_mCD7F80C1294FE9B3BD03319A3C2C625D7CC6A485,
	Parser_get_PeekChar_m4C59D86249D4BDC34034ED8212D7C1DB2F0FDE4D,
	Parser_get_NextChar_m46C018191785411310602B3F022D46C3085DED3C,
	Parser_get_NextWord_m04E6AC4117C72E8B702F5157306EEBD571EBE588,
	Parser_get_NextToken_m4B6BCBF18A6C1245108CEC0C40721FD08C6DE5C1,
	Serializer__ctor_m77DB4100B561FE67B8F93105462632E985B9A31B,
	Serializer_Serialize_m5EE777FFD0A4A796C872F774CA5F8D06A707F695,
	Serializer_SerializeValue_m4EFB29AAAB95494D19A50B3C9DA7D20B6DA59B38,
	Serializer_SerializeObject_mA598CDF8A872E08815AAA88D87ADE6106497EBEE,
	Serializer_SerializeArray_m6CDBD88758627DD3E21B596A24E9A2A20304D1EB,
	Serializer_SerializeString_mFEBA44A6A5824F1903783BEB1B81FDF72D8BCCE4,
	Serializer_SerializeOther_m61D82C0B9FEF0F39DDAFFEB43B851545EB63633D,
	MiniJsonExtensions_GetString_m0B244C8B27E21C4EE034DF9C5E0E4C6AE89D12D0,
	MiniJsonExtensions_toJson_m2DB3976D2DC0DF3E9E186DEE7854BAEB1F7551C9,
	MiniJsonExtensions_toJson_mEB129B8C724E52DE7DFD8313520B59ECE1F82AC3,
	MiniJsonExtensions_HashtableFromJson_m5E34E1DB2C178E140494CD1AF26718CEDC518B5E,
};
static const int32_t s_InvokerIndices[37] = 
{
	0,
	0,
	26,
	27,
	27,
	137,
	436,
	1515,
	26,
	0,
	0,
	48,
	26,
	0,
	23,
	14,
	14,
	14,
	34,
	14,
	14,
	23,
	255,
	255,
	14,
	10,
	23,
	0,
	26,
	26,
	26,
	26,
	26,
	2,
	0,
	0,
	0,
};
extern const Il2CppCodeGenModule g_Purchasing_CommonCodeGenModule;
const Il2CppCodeGenModule g_Purchasing_CommonCodeGenModule = 
{
	"Purchasing.Common.dll",
	37,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
