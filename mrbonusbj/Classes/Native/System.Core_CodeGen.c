﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String SR::GetString(System.String)
extern void SR_GetString_m0D34A4798D653D11FFC8F27A24C741A83A3DA90B ();
// 0x00000002 System.Void System.Security.Cryptography.AesManaged::.ctor()
extern void AesManaged__ctor_mB2BB25E2F795428300A966DF7C4706BDDB65FB64 ();
// 0x00000003 System.Int32 System.Security.Cryptography.AesManaged::get_FeedbackSize()
extern void AesManaged_get_FeedbackSize_mA079406B80A8CDFB6811251C8BCE9EFE3C83A712 ();
// 0x00000004 System.Void System.Security.Cryptography.AesManaged::set_FeedbackSize(System.Int32)
extern void AesManaged_set_FeedbackSize_m881146DEC23D6F9FA3480D2ABDA7CBAFBD71C7C9 ();
// 0x00000005 System.Byte[] System.Security.Cryptography.AesManaged::get_IV()
extern void AesManaged_get_IV_mAAC08AB6D76CE29D3AEFCEF7B46F17B788B00B6E ();
// 0x00000006 System.Void System.Security.Cryptography.AesManaged::set_IV(System.Byte[])
extern void AesManaged_set_IV_m6AF8905A7F0DBD20D7E059360423DB57C7DFA722 ();
// 0x00000007 System.Byte[] System.Security.Cryptography.AesManaged::get_Key()
extern void AesManaged_get_Key_mC3790099349E411DFBC3EB6916E31CCC1F2AC088 ();
// 0x00000008 System.Void System.Security.Cryptography.AesManaged::set_Key(System.Byte[])
extern void AesManaged_set_Key_m654922A858A73BC91747B52F5D8B194B1EA88ADC ();
// 0x00000009 System.Int32 System.Security.Cryptography.AesManaged::get_KeySize()
extern void AesManaged_get_KeySize_m5218EB6C55678DC91BDE12E4F0697B719A2C7DD6 ();
// 0x0000000A System.Void System.Security.Cryptography.AesManaged::set_KeySize(System.Int32)
extern void AesManaged_set_KeySize_m0AF9E2BB96295D70FBADB46F8E32FB54A695C349 ();
// 0x0000000B System.Security.Cryptography.CipherMode System.Security.Cryptography.AesManaged::get_Mode()
extern void AesManaged_get_Mode_m85C722AAA2A9CF3BC012EC908CF5B3B57BAF4BDA ();
// 0x0000000C System.Void System.Security.Cryptography.AesManaged::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesManaged_set_Mode_mE06717F04195261B88A558FBD08AEB847D9320D8 ();
// 0x0000000D System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesManaged::get_Padding()
extern void AesManaged_get_Padding_mBD0B0AA07CF0FBFDFC14458D14F058DE6DA656F0 ();
// 0x0000000E System.Void System.Security.Cryptography.AesManaged::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesManaged_set_Padding_m1BAC3EECEF3E2F49E4641E29169F149EDA8C5B23 ();
// 0x0000000F System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor()
extern void AesManaged_CreateDecryptor_m9E9E7861138397C7A6AAF8C43C81BD4CFCB8E0BD ();
// 0x00000010 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateDecryptor_mEBE041A905F0848F846901916BA23485F85C65F1 ();
// 0x00000011 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor()
extern void AesManaged_CreateEncryptor_m82CC97D7C3C330EB8F5F61B3192D65859CAA94F4 ();
// 0x00000012 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateEncryptor_mA914CA875EF777EDB202343570182CC0D9D89A91 ();
// 0x00000013 System.Void System.Security.Cryptography.AesManaged::Dispose(System.Boolean)
extern void AesManaged_Dispose_m57258CB76A9CCEF03FF4D4C5DE02E9A31056F8ED ();
// 0x00000014 System.Void System.Security.Cryptography.AesManaged::GenerateIV()
extern void AesManaged_GenerateIV_m92735378E3FB47DE1D0241A923CB4E426C702ABC ();
// 0x00000015 System.Void System.Security.Cryptography.AesManaged::GenerateKey()
extern void AesManaged_GenerateKey_m5C790BC376A3FAFF13617855FF6BFA8A57925146 ();
// 0x00000016 System.Void System.Security.Cryptography.AesCryptoServiceProvider::.ctor()
extern void AesCryptoServiceProvider__ctor_m8AA4C1503DBE1849070CFE727ED227BE5043373E ();
// 0x00000017 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateIV()
extern void AesCryptoServiceProvider_GenerateIV_mAE25C1774AEB75702E4737808E56FD2EC8BF54CC ();
// 0x00000018 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateKey()
extern void AesCryptoServiceProvider_GenerateKey_mC65CD8C14E8FD07E9469E74C641A746E52977586 ();
// 0x00000019 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateDecryptor_m3842B2AC283063BE4D9902818C8F68CFB4100139 ();
// 0x0000001A System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateEncryptor_mACCCC00AED5CBBF5E9437BCA907DD67C6D123672 ();
// 0x0000001B System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_IV()
extern void AesCryptoServiceProvider_get_IV_m30FBD13B702C384941FB85AD975BB3C0668F426F ();
// 0x0000001C System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_IV(System.Byte[])
extern void AesCryptoServiceProvider_set_IV_m195F582AD29E4B449AFC54036AAECE0E05385C9C ();
// 0x0000001D System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_Key()
extern void AesCryptoServiceProvider_get_Key_m9ABC98DF0CDE8952B677538C387C66A88196786A ();
// 0x0000001E System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Key(System.Byte[])
extern void AesCryptoServiceProvider_set_Key_m4B9CE2F92E3B1BC209BFAECEACB7A976BBCDC700 ();
// 0x0000001F System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_KeySize()
extern void AesCryptoServiceProvider_get_KeySize_m10BDECEC12722803F3DE5F15CD76C5BDF588D1FA ();
// 0x00000020 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_KeySize(System.Int32)
extern void AesCryptoServiceProvider_set_KeySize_mA26268F7CEDA7D0A2447FC2022327E0C49C89B9B ();
// 0x00000021 System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_FeedbackSize()
extern void AesCryptoServiceProvider_get_FeedbackSize_mB93FFC9FCB2C09EABFB13913E245A2D75491659F ();
// 0x00000022 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_FeedbackSize(System.Int32)
extern void AesCryptoServiceProvider_set_FeedbackSize_m409990EF50B03E207F0BAAE9BE19C23A77EA9C27 ();
// 0x00000023 System.Security.Cryptography.CipherMode System.Security.Cryptography.AesCryptoServiceProvider::get_Mode()
extern void AesCryptoServiceProvider_get_Mode_m5C09588E49787D597CF8C0CD0C74DB63BE0ACE5F ();
// 0x00000024 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesCryptoServiceProvider_set_Mode_mC7EE07E709C918D0745E5A207A66D89F08EA57EA ();
// 0x00000025 System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesCryptoServiceProvider::get_Padding()
extern void AesCryptoServiceProvider_get_Padding_mA56E045AE5CCF569C4A21C949DD4A4332E63F438 ();
// 0x00000026 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesCryptoServiceProvider_set_Padding_m94A4D3BE55325036611C5015E02CB622CFCDAF22 ();
// 0x00000027 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor()
extern void AesCryptoServiceProvider_CreateDecryptor_mD858924207EA664C6E32D42408FB5C8040DD4D44 ();
// 0x00000028 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor()
extern void AesCryptoServiceProvider_CreateEncryptor_m964DD0E94A26806AB34A7A79D4E4D1539425A2EA ();
// 0x00000029 System.Void System.Security.Cryptography.AesCryptoServiceProvider::Dispose(System.Boolean)
extern void AesCryptoServiceProvider_Dispose_mCFA420F8643911F86A112F50905FCB34C4A3045F ();
// 0x0000002A System.Void System.Security.Cryptography.AesTransform::.ctor(System.Security.Cryptography.Aes,System.Boolean,System.Byte[],System.Byte[])
extern void AesTransform__ctor_m1BC6B0F208747D4E35A58075D74DEBD5F72DB7DD ();
// 0x0000002B System.Void System.Security.Cryptography.AesTransform::ECB(System.Byte[],System.Byte[])
extern void AesTransform_ECB_mAFE52E4D1958026C3343F85CC950A8E24FDFBBDA ();
// 0x0000002C System.UInt32 System.Security.Cryptography.AesTransform::SubByte(System.UInt32)
extern void AesTransform_SubByte_mEDB43A2A4E83017475094E5616E7DBC56F945A24 ();
// 0x0000002D System.Void System.Security.Cryptography.AesTransform::Encrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Encrypt128_m09C945A0345FD32E8DB3F4AF4B4E184CADD754DA ();
// 0x0000002E System.Void System.Security.Cryptography.AesTransform::Decrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Decrypt128_m1AE10B230A47A294B5B10EFD9C8243B02DBEA463 ();
// 0x0000002F System.Void System.Security.Cryptography.AesTransform::.cctor()
extern void AesTransform__cctor_mDEA197C50BA055FF76B7ECFEB5C1FD7900CE4325 ();
// 0x00000030 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x00000031 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000032 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 ();
// 0x00000033 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000034 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000035 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`3<TSource,System.Int32,TResult>)
// 0x00000036 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`3<TSource,System.Int32,TResult>)
// 0x00000037 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000038 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000039 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Take(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000003A System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::TakeIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000003B TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003C System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003D System.Collections.Generic.Dictionary`2<TKey,TSource> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000003E System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x0000003F TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000040 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000041 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000042 TSource System.Linq.Enumerable::Aggregate(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`3<TSource,TSource,TSource>)
// 0x00000043 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000044 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000045 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000046 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000047 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000048 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000049 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000004A System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000004B System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x0000004C System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000004D System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x0000004E System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000004F System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000050 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x00000051 System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x00000052 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000053 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000054 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000055 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000056 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000057 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000058 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000059 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000005A System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x0000005B System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x0000005C System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000005D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000005E System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000005F System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x00000060 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x00000061 System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x00000062 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000063 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000064 System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000065 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x00000066 System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x00000067 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000068 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000069 System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000006A System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x0000006B System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x0000006C System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000006D System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000006E System.Void System.Linq.Enumerable_<SelectIterator>d__5`2::.ctor(System.Int32)
// 0x0000006F System.Void System.Linq.Enumerable_<SelectIterator>d__5`2::System.IDisposable.Dispose()
// 0x00000070 System.Boolean System.Linq.Enumerable_<SelectIterator>d__5`2::MoveNext()
// 0x00000071 System.Void System.Linq.Enumerable_<SelectIterator>d__5`2::<>m__Finally1()
// 0x00000072 TResult System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000073 System.Void System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.IEnumerator.Reset()
// 0x00000074 System.Object System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.IEnumerator.get_Current()
// 0x00000075 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000076 System.Collections.IEnumerator System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000077 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000078 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000079 System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x0000007A TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x0000007B System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::.ctor(System.Int32)
// 0x0000007C System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::System.IDisposable.Dispose()
// 0x0000007D System.Boolean System.Linq.Enumerable_<TakeIterator>d__25`1::MoveNext()
// 0x0000007E System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::<>m__Finally1()
// 0x0000007F TSource System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000080 System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerator.Reset()
// 0x00000081 System.Object System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerator.get_Current()
// 0x00000082 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000083 System.Collections.IEnumerator System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000084 System.Func`2<TElement,TElement> System.Linq.IdentityFunction`1::get_Instance()
// 0x00000085 System.Void System.Linq.IdentityFunction`1_<>c::.cctor()
// 0x00000086 System.Void System.Linq.IdentityFunction`1_<>c::.ctor()
// 0x00000087 TElement System.Linq.IdentityFunction`1_<>c::<get_Instance>b__1_0(TElement)
// 0x00000088 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x00000089 TElement[] System.Linq.Buffer`1::ToArray()
// 0x0000008A System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x0000008B System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000008C System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000008D System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000008E System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000008F System.Void System.Collections.Generic.HashSet`1::CopyFrom(System.Collections.Generic.HashSet`1<T>)
// 0x00000090 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000091 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x00000092 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x00000093 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x00000094 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000095 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x00000096 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000097 System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000098 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000099 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000009A System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000009B System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x0000009C System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x0000009D System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000009E System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x0000009F System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x000000A0 System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::get_Comparer()
// 0x000000A1 System.Void System.Collections.Generic.HashSet`1::TrimExcess()
// 0x000000A2 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x000000A3 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x000000A4 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x000000A5 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x000000A6 System.Void System.Collections.Generic.HashSet`1::AddValue(System.Int32,System.Int32,T)
// 0x000000A7 System.Boolean System.Collections.Generic.HashSet`1::AreEqualityComparersEqual(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x000000A8 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x000000A9 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x000000AA System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x000000AB System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x000000AC T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x000000AD System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x000000AE System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[174] = 
{
	SR_GetString_m0D34A4798D653D11FFC8F27A24C741A83A3DA90B,
	AesManaged__ctor_mB2BB25E2F795428300A966DF7C4706BDDB65FB64,
	AesManaged_get_FeedbackSize_mA079406B80A8CDFB6811251C8BCE9EFE3C83A712,
	AesManaged_set_FeedbackSize_m881146DEC23D6F9FA3480D2ABDA7CBAFBD71C7C9,
	AesManaged_get_IV_mAAC08AB6D76CE29D3AEFCEF7B46F17B788B00B6E,
	AesManaged_set_IV_m6AF8905A7F0DBD20D7E059360423DB57C7DFA722,
	AesManaged_get_Key_mC3790099349E411DFBC3EB6916E31CCC1F2AC088,
	AesManaged_set_Key_m654922A858A73BC91747B52F5D8B194B1EA88ADC,
	AesManaged_get_KeySize_m5218EB6C55678DC91BDE12E4F0697B719A2C7DD6,
	AesManaged_set_KeySize_m0AF9E2BB96295D70FBADB46F8E32FB54A695C349,
	AesManaged_get_Mode_m85C722AAA2A9CF3BC012EC908CF5B3B57BAF4BDA,
	AesManaged_set_Mode_mE06717F04195261B88A558FBD08AEB847D9320D8,
	AesManaged_get_Padding_mBD0B0AA07CF0FBFDFC14458D14F058DE6DA656F0,
	AesManaged_set_Padding_m1BAC3EECEF3E2F49E4641E29169F149EDA8C5B23,
	AesManaged_CreateDecryptor_m9E9E7861138397C7A6AAF8C43C81BD4CFCB8E0BD,
	AesManaged_CreateDecryptor_mEBE041A905F0848F846901916BA23485F85C65F1,
	AesManaged_CreateEncryptor_m82CC97D7C3C330EB8F5F61B3192D65859CAA94F4,
	AesManaged_CreateEncryptor_mA914CA875EF777EDB202343570182CC0D9D89A91,
	AesManaged_Dispose_m57258CB76A9CCEF03FF4D4C5DE02E9A31056F8ED,
	AesManaged_GenerateIV_m92735378E3FB47DE1D0241A923CB4E426C702ABC,
	AesManaged_GenerateKey_m5C790BC376A3FAFF13617855FF6BFA8A57925146,
	AesCryptoServiceProvider__ctor_m8AA4C1503DBE1849070CFE727ED227BE5043373E,
	AesCryptoServiceProvider_GenerateIV_mAE25C1774AEB75702E4737808E56FD2EC8BF54CC,
	AesCryptoServiceProvider_GenerateKey_mC65CD8C14E8FD07E9469E74C641A746E52977586,
	AesCryptoServiceProvider_CreateDecryptor_m3842B2AC283063BE4D9902818C8F68CFB4100139,
	AesCryptoServiceProvider_CreateEncryptor_mACCCC00AED5CBBF5E9437BCA907DD67C6D123672,
	AesCryptoServiceProvider_get_IV_m30FBD13B702C384941FB85AD975BB3C0668F426F,
	AesCryptoServiceProvider_set_IV_m195F582AD29E4B449AFC54036AAECE0E05385C9C,
	AesCryptoServiceProvider_get_Key_m9ABC98DF0CDE8952B677538C387C66A88196786A,
	AesCryptoServiceProvider_set_Key_m4B9CE2F92E3B1BC209BFAECEACB7A976BBCDC700,
	AesCryptoServiceProvider_get_KeySize_m10BDECEC12722803F3DE5F15CD76C5BDF588D1FA,
	AesCryptoServiceProvider_set_KeySize_mA26268F7CEDA7D0A2447FC2022327E0C49C89B9B,
	AesCryptoServiceProvider_get_FeedbackSize_mB93FFC9FCB2C09EABFB13913E245A2D75491659F,
	AesCryptoServiceProvider_set_FeedbackSize_m409990EF50B03E207F0BAAE9BE19C23A77EA9C27,
	AesCryptoServiceProvider_get_Mode_m5C09588E49787D597CF8C0CD0C74DB63BE0ACE5F,
	AesCryptoServiceProvider_set_Mode_mC7EE07E709C918D0745E5A207A66D89F08EA57EA,
	AesCryptoServiceProvider_get_Padding_mA56E045AE5CCF569C4A21C949DD4A4332E63F438,
	AesCryptoServiceProvider_set_Padding_m94A4D3BE55325036611C5015E02CB622CFCDAF22,
	AesCryptoServiceProvider_CreateDecryptor_mD858924207EA664C6E32D42408FB5C8040DD4D44,
	AesCryptoServiceProvider_CreateEncryptor_m964DD0E94A26806AB34A7A79D4E4D1539425A2EA,
	AesCryptoServiceProvider_Dispose_mCFA420F8643911F86A112F50905FCB34C4A3045F,
	AesTransform__ctor_m1BC6B0F208747D4E35A58075D74DEBD5F72DB7DD,
	AesTransform_ECB_mAFE52E4D1958026C3343F85CC950A8E24FDFBBDA,
	AesTransform_SubByte_mEDB43A2A4E83017475094E5616E7DBC56F945A24,
	AesTransform_Encrypt128_m09C945A0345FD32E8DB3F4AF4B4E184CADD754DA,
	AesTransform_Decrypt128_m1AE10B230A47A294B5B10EFD9C8243B02DBEA463,
	AesTransform__cctor_mDEA197C50BA055FF76B7ECFEB5C1FD7900CE4325,
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[174] = 
{
	0,
	23,
	10,
	32,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	107,
	14,
	107,
	31,
	23,
	23,
	23,
	23,
	23,
	107,
	107,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	14,
	31,
	912,
	27,
	37,
	215,
	215,
	3,
	0,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[38] = 
{
	{ 0x02000008, { 61, 4 } },
	{ 0x02000009, { 65, 9 } },
	{ 0x0200000A, { 76, 7 } },
	{ 0x0200000B, { 85, 10 } },
	{ 0x0200000C, { 97, 11 } },
	{ 0x0200000D, { 111, 9 } },
	{ 0x0200000E, { 123, 12 } },
	{ 0x0200000F, { 138, 9 } },
	{ 0x02000010, { 147, 1 } },
	{ 0x02000011, { 148, 2 } },
	{ 0x02000012, { 150, 8 } },
	{ 0x02000013, { 158, 4 } },
	{ 0x02000014, { 162, 3 } },
	{ 0x02000015, { 165, 4 } },
	{ 0x02000016, { 169, 34 } },
	{ 0x02000018, { 203, 2 } },
	{ 0x06000033, { 0, 10 } },
	{ 0x06000034, { 10, 10 } },
	{ 0x06000035, { 20, 1 } },
	{ 0x06000036, { 21, 2 } },
	{ 0x06000037, { 23, 5 } },
	{ 0x06000038, { 28, 5 } },
	{ 0x06000039, { 33, 1 } },
	{ 0x0600003A, { 34, 2 } },
	{ 0x0600003B, { 36, 3 } },
	{ 0x0600003C, { 39, 2 } },
	{ 0x0600003D, { 41, 3 } },
	{ 0x0600003E, { 44, 7 } },
	{ 0x0600003F, { 51, 3 } },
	{ 0x06000040, { 54, 1 } },
	{ 0x06000041, { 55, 3 } },
	{ 0x06000042, { 58, 3 } },
	{ 0x06000052, { 74, 2 } },
	{ 0x06000057, { 83, 2 } },
	{ 0x0600005C, { 95, 2 } },
	{ 0x06000062, { 108, 3 } },
	{ 0x06000067, { 120, 3 } },
	{ 0x0600006C, { 135, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[205] = 
{
	{ (Il2CppRGCTXDataType)2, 16467 },
	{ (Il2CppRGCTXDataType)3, 10799 },
	{ (Il2CppRGCTXDataType)2, 16468 },
	{ (Il2CppRGCTXDataType)2, 16469 },
	{ (Il2CppRGCTXDataType)3, 10800 },
	{ (Il2CppRGCTXDataType)2, 16470 },
	{ (Il2CppRGCTXDataType)2, 16471 },
	{ (Il2CppRGCTXDataType)3, 10801 },
	{ (Il2CppRGCTXDataType)2, 16472 },
	{ (Il2CppRGCTXDataType)3, 10802 },
	{ (Il2CppRGCTXDataType)2, 16473 },
	{ (Il2CppRGCTXDataType)3, 10803 },
	{ (Il2CppRGCTXDataType)2, 16474 },
	{ (Il2CppRGCTXDataType)2, 16475 },
	{ (Il2CppRGCTXDataType)3, 10804 },
	{ (Il2CppRGCTXDataType)2, 16476 },
	{ (Il2CppRGCTXDataType)2, 16477 },
	{ (Il2CppRGCTXDataType)3, 10805 },
	{ (Il2CppRGCTXDataType)2, 16478 },
	{ (Il2CppRGCTXDataType)3, 10806 },
	{ (Il2CppRGCTXDataType)3, 10807 },
	{ (Il2CppRGCTXDataType)2, 16479 },
	{ (Il2CppRGCTXDataType)3, 10808 },
	{ (Il2CppRGCTXDataType)2, 16480 },
	{ (Il2CppRGCTXDataType)3, 10809 },
	{ (Il2CppRGCTXDataType)3, 10810 },
	{ (Il2CppRGCTXDataType)2, 12679 },
	{ (Il2CppRGCTXDataType)3, 10811 },
	{ (Il2CppRGCTXDataType)2, 16481 },
	{ (Il2CppRGCTXDataType)3, 10812 },
	{ (Il2CppRGCTXDataType)3, 10813 },
	{ (Il2CppRGCTXDataType)2, 12686 },
	{ (Il2CppRGCTXDataType)3, 10814 },
	{ (Il2CppRGCTXDataType)3, 10815 },
	{ (Il2CppRGCTXDataType)2, 16482 },
	{ (Il2CppRGCTXDataType)3, 10816 },
	{ (Il2CppRGCTXDataType)2, 16483 },
	{ (Il2CppRGCTXDataType)3, 10817 },
	{ (Il2CppRGCTXDataType)3, 10818 },
	{ (Il2CppRGCTXDataType)2, 12696 },
	{ (Il2CppRGCTXDataType)3, 10819 },
	{ (Il2CppRGCTXDataType)3, 10820 },
	{ (Il2CppRGCTXDataType)2, 16484 },
	{ (Il2CppRGCTXDataType)3, 10821 },
	{ (Il2CppRGCTXDataType)2, 12709 },
	{ (Il2CppRGCTXDataType)3, 10822 },
	{ (Il2CppRGCTXDataType)2, 12702 },
	{ (Il2CppRGCTXDataType)2, 16485 },
	{ (Il2CppRGCTXDataType)3, 10823 },
	{ (Il2CppRGCTXDataType)3, 10824 },
	{ (Il2CppRGCTXDataType)3, 10825 },
	{ (Il2CppRGCTXDataType)2, 12710 },
	{ (Il2CppRGCTXDataType)2, 16486 },
	{ (Il2CppRGCTXDataType)3, 10826 },
	{ (Il2CppRGCTXDataType)2, 12713 },
	{ (Il2CppRGCTXDataType)2, 12715 },
	{ (Il2CppRGCTXDataType)2, 16487 },
	{ (Il2CppRGCTXDataType)3, 10827 },
	{ (Il2CppRGCTXDataType)2, 12718 },
	{ (Il2CppRGCTXDataType)2, 16488 },
	{ (Il2CppRGCTXDataType)3, 10828 },
	{ (Il2CppRGCTXDataType)3, 10829 },
	{ (Il2CppRGCTXDataType)3, 10830 },
	{ (Il2CppRGCTXDataType)2, 12723 },
	{ (Il2CppRGCTXDataType)3, 10831 },
	{ (Il2CppRGCTXDataType)3, 10832 },
	{ (Il2CppRGCTXDataType)2, 12735 },
	{ (Il2CppRGCTXDataType)2, 16489 },
	{ (Il2CppRGCTXDataType)3, 10833 },
	{ (Il2CppRGCTXDataType)3, 10834 },
	{ (Il2CppRGCTXDataType)2, 12737 },
	{ (Il2CppRGCTXDataType)2, 16366 },
	{ (Il2CppRGCTXDataType)3, 10835 },
	{ (Il2CppRGCTXDataType)3, 10836 },
	{ (Il2CppRGCTXDataType)2, 16490 },
	{ (Il2CppRGCTXDataType)3, 10837 },
	{ (Il2CppRGCTXDataType)3, 10838 },
	{ (Il2CppRGCTXDataType)2, 12747 },
	{ (Il2CppRGCTXDataType)2, 16491 },
	{ (Il2CppRGCTXDataType)3, 10839 },
	{ (Il2CppRGCTXDataType)3, 10840 },
	{ (Il2CppRGCTXDataType)3, 10363 },
	{ (Il2CppRGCTXDataType)3, 10841 },
	{ (Il2CppRGCTXDataType)2, 16492 },
	{ (Il2CppRGCTXDataType)3, 10842 },
	{ (Il2CppRGCTXDataType)3, 10843 },
	{ (Il2CppRGCTXDataType)2, 12759 },
	{ (Il2CppRGCTXDataType)2, 16493 },
	{ (Il2CppRGCTXDataType)3, 10844 },
	{ (Il2CppRGCTXDataType)3, 10845 },
	{ (Il2CppRGCTXDataType)3, 10846 },
	{ (Il2CppRGCTXDataType)3, 10847 },
	{ (Il2CppRGCTXDataType)3, 10848 },
	{ (Il2CppRGCTXDataType)3, 10369 },
	{ (Il2CppRGCTXDataType)3, 10849 },
	{ (Il2CppRGCTXDataType)2, 16494 },
	{ (Il2CppRGCTXDataType)3, 10850 },
	{ (Il2CppRGCTXDataType)3, 10851 },
	{ (Il2CppRGCTXDataType)2, 12772 },
	{ (Il2CppRGCTXDataType)2, 16495 },
	{ (Il2CppRGCTXDataType)3, 10852 },
	{ (Il2CppRGCTXDataType)3, 10853 },
	{ (Il2CppRGCTXDataType)2, 12774 },
	{ (Il2CppRGCTXDataType)2, 16496 },
	{ (Il2CppRGCTXDataType)3, 10854 },
	{ (Il2CppRGCTXDataType)3, 10855 },
	{ (Il2CppRGCTXDataType)2, 16497 },
	{ (Il2CppRGCTXDataType)3, 10856 },
	{ (Il2CppRGCTXDataType)3, 10857 },
	{ (Il2CppRGCTXDataType)2, 16498 },
	{ (Il2CppRGCTXDataType)3, 10858 },
	{ (Il2CppRGCTXDataType)3, 10859 },
	{ (Il2CppRGCTXDataType)2, 12789 },
	{ (Il2CppRGCTXDataType)2, 16499 },
	{ (Il2CppRGCTXDataType)3, 10860 },
	{ (Il2CppRGCTXDataType)3, 10861 },
	{ (Il2CppRGCTXDataType)3, 10862 },
	{ (Il2CppRGCTXDataType)3, 10380 },
	{ (Il2CppRGCTXDataType)2, 16500 },
	{ (Il2CppRGCTXDataType)3, 10863 },
	{ (Il2CppRGCTXDataType)3, 10864 },
	{ (Il2CppRGCTXDataType)2, 16501 },
	{ (Il2CppRGCTXDataType)3, 10865 },
	{ (Il2CppRGCTXDataType)3, 10866 },
	{ (Il2CppRGCTXDataType)2, 12805 },
	{ (Il2CppRGCTXDataType)2, 16502 },
	{ (Il2CppRGCTXDataType)3, 10867 },
	{ (Il2CppRGCTXDataType)3, 10868 },
	{ (Il2CppRGCTXDataType)3, 10869 },
	{ (Il2CppRGCTXDataType)3, 10870 },
	{ (Il2CppRGCTXDataType)3, 10871 },
	{ (Il2CppRGCTXDataType)3, 10872 },
	{ (Il2CppRGCTXDataType)3, 10386 },
	{ (Il2CppRGCTXDataType)2, 16503 },
	{ (Il2CppRGCTXDataType)3, 10873 },
	{ (Il2CppRGCTXDataType)3, 10874 },
	{ (Il2CppRGCTXDataType)2, 16504 },
	{ (Il2CppRGCTXDataType)3, 10875 },
	{ (Il2CppRGCTXDataType)3, 10876 },
	{ (Il2CppRGCTXDataType)2, 16505 },
	{ (Il2CppRGCTXDataType)2, 16506 },
	{ (Il2CppRGCTXDataType)3, 10877 },
	{ (Il2CppRGCTXDataType)3, 10878 },
	{ (Il2CppRGCTXDataType)2, 12822 },
	{ (Il2CppRGCTXDataType)2, 16507 },
	{ (Il2CppRGCTXDataType)3, 10879 },
	{ (Il2CppRGCTXDataType)3, 10880 },
	{ (Il2CppRGCTXDataType)3, 10881 },
	{ (Il2CppRGCTXDataType)3, 10882 },
	{ (Il2CppRGCTXDataType)3, 10883 },
	{ (Il2CppRGCTXDataType)3, 10884 },
	{ (Il2CppRGCTXDataType)2, 12852 },
	{ (Il2CppRGCTXDataType)2, 12847 },
	{ (Il2CppRGCTXDataType)3, 10885 },
	{ (Il2CppRGCTXDataType)2, 12846 },
	{ (Il2CppRGCTXDataType)2, 16508 },
	{ (Il2CppRGCTXDataType)3, 10886 },
	{ (Il2CppRGCTXDataType)3, 10887 },
	{ (Il2CppRGCTXDataType)2, 16509 },
	{ (Il2CppRGCTXDataType)3, 10888 },
	{ (Il2CppRGCTXDataType)2, 12856 },
	{ (Il2CppRGCTXDataType)3, 10889 },
	{ (Il2CppRGCTXDataType)2, 16510 },
	{ (Il2CppRGCTXDataType)3, 10890 },
	{ (Il2CppRGCTXDataType)2, 16510 },
	{ (Il2CppRGCTXDataType)2, 16511 },
	{ (Il2CppRGCTXDataType)2, 12867 },
	{ (Il2CppRGCTXDataType)2, 12865 },
	{ (Il2CppRGCTXDataType)2, 16512 },
	{ (Il2CppRGCTXDataType)3, 10891 },
	{ (Il2CppRGCTXDataType)2, 16513 },
	{ (Il2CppRGCTXDataType)3, 10892 },
	{ (Il2CppRGCTXDataType)3, 10893 },
	{ (Il2CppRGCTXDataType)2, 12874 },
	{ (Il2CppRGCTXDataType)3, 10894 },
	{ (Il2CppRGCTXDataType)2, 12874 },
	{ (Il2CppRGCTXDataType)3, 10895 },
	{ (Il2CppRGCTXDataType)2, 12891 },
	{ (Il2CppRGCTXDataType)3, 10896 },
	{ (Il2CppRGCTXDataType)3, 10897 },
	{ (Il2CppRGCTXDataType)3, 10898 },
	{ (Il2CppRGCTXDataType)2, 16514 },
	{ (Il2CppRGCTXDataType)3, 10899 },
	{ (Il2CppRGCTXDataType)3, 10900 },
	{ (Il2CppRGCTXDataType)3, 10901 },
	{ (Il2CppRGCTXDataType)2, 12871 },
	{ (Il2CppRGCTXDataType)3, 10902 },
	{ (Il2CppRGCTXDataType)3, 10903 },
	{ (Il2CppRGCTXDataType)2, 12876 },
	{ (Il2CppRGCTXDataType)3, 10904 },
	{ (Il2CppRGCTXDataType)1, 16515 },
	{ (Il2CppRGCTXDataType)2, 12875 },
	{ (Il2CppRGCTXDataType)3, 10905 },
	{ (Il2CppRGCTXDataType)1, 12875 },
	{ (Il2CppRGCTXDataType)1, 12871 },
	{ (Il2CppRGCTXDataType)2, 16514 },
	{ (Il2CppRGCTXDataType)2, 12875 },
	{ (Il2CppRGCTXDataType)2, 12873 },
	{ (Il2CppRGCTXDataType)2, 12877 },
	{ (Il2CppRGCTXDataType)3, 10906 },
	{ (Il2CppRGCTXDataType)3, 10907 },
	{ (Il2CppRGCTXDataType)3, 10908 },
	{ (Il2CppRGCTXDataType)2, 12872 },
	{ (Il2CppRGCTXDataType)3, 10909 },
	{ (Il2CppRGCTXDataType)2, 12887 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	174,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	38,
	s_rgctxIndices,
	205,
	s_rgctxValues,
	NULL,
};
