﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;

struct EVENT_FILTER_DESCRIPTOR_t24FD3DB96806FFE8C96FFDB38B1B8331EA0D72BB ;
struct Guid_t ;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif

extern "C" void DEFAULT_CALL ReversePInvokeWrapper_EventProvider_EtwEnableCallBack_m32987ABF4E909DC5476F09C034714951CB4A8048(Guid_t * ___sourceId0, int32_t ___controlCode1, uint8_t ___setLevel2, int64_t ___anyKeyword3, int64_t ___allKeyword4, EVENT_FILTER_DESCRIPTOR_t24FD3DB96806FFE8C96FFDB38B1B8331EA0D72BB * ___filterData5, void* ___callbackContext6);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_OSSpecificSynchronizationContext_InvocationEntry_m056BCE43FF155AAE872FF7E565F8F72A50D26147(intptr_t ___arg0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_AppleStoreImpl_MessageCallback_m1AFEBE0209301D9DE3F08BF561DC43984AEC3251(char* ___subject0, char* ___payload1, char* ___receipt2, char* ___transactionId3);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_FacebookStoreImpl_MessageCallback_m95B738D784D0C0AC054DE6F7DBC86B4E12517105(char* ___subject0, char* ___payload1, char* ___receipt2, char* ___transactionId3);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_TizenStoreImpl_MessageCallback_m468657DE860B56D8404F5502CB68ABA6C7B13748(char* ___subject0, char* ___payload1, char* ___receipt2, char* ___transactionId3);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_AdLoaderClient_AdLoaderDidReceiveNativeCustomTemplateAdCallback_m96CB8031ECC1E20CDB3AA013D0E1CA5C82FBA9F0(intptr_t ___adLoader0, intptr_t ___nativeCustomTemplateAd1, char* ___templateID2);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_AdLoaderClient_AdLoaderDidFailToReceiveAdWithErrorCallback_mC2A70F1FD62DF7B697FF79C19CDB49D89A3ED296(intptr_t ___adLoader0, char* ___error1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_BannerClient_AdViewDidReceiveAdCallback_m5F0611A8BD59EBB7E6A4CFD4AF1C20132C7D5345(intptr_t ___bannerClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_BannerClient_AdViewDidFailToReceiveAdWithErrorCallback_m863303F5E9FF5A436CE67C416A3E708E9E2F6206(intptr_t ___bannerClient0, char* ___error1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_BannerClient_AdViewWillPresentScreenCallback_mCCCDADC61157A1E8B91B12B866A326AE24024816(intptr_t ___bannerClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_BannerClient_AdViewDidDismissScreenCallback_m15101864FE77339582C8DF5FC5129E33CFC9674B(intptr_t ___bannerClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_BannerClient_AdViewWillLeaveApplicationCallback_mD2C41507939D77A4986009D02E1962A5A200AFE8(intptr_t ___bannerClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_CustomNativeTemplateClient_NativeCustomTemplateDidReceiveClickCallback_m93BA0F7BF0763D9ECEECB912C673A1BAED9D989F(intptr_t ___nativeCustomAd0, char* ___assetName1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_InterstitialClient_InterstitialDidReceiveAdCallback_mF485EB5E41F80ED5C5B6FF0376CD4B8BB96EED2B(intptr_t ___interstitialClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_InterstitialClient_InterstitialDidFailToReceiveAdWithErrorCallback_m9BD349E80A9B92676F0BF98C52C36A690DF107CA(intptr_t ___interstitialClient0, char* ___error1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_InterstitialClient_InterstitialWillPresentScreenCallback_mB5835C4991F852B2E0768EAE7F9E9478EB2FD2C5(intptr_t ___interstitialClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_InterstitialClient_InterstitialDidDismissScreenCallback_mAD21EEBDD6D829719AF1FB8F438CC04C63EF431C(intptr_t ___interstitialClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_InterstitialClient_InterstitialWillLeaveApplicationCallback_m41A6D1FEF7A23CB20F37D767455B69D71FE83D42(intptr_t ___interstitialClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_NativeExpressAdClient_NativeExpressAdViewDidReceiveAdCallback_m7E5BCA95DAE8C27B53B4DF27571E1D47DC1BC924(intptr_t ___nativeExpressClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_NativeExpressAdClient_NativeExpressAdViewDidFailToReceiveAdWithErrorCallback_m02B2DBEB39F61BC6B4188F167105425E5139DF1B(intptr_t ___nativeExpressClient0, char* ___error1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_NativeExpressAdClient_NativeExpressAdViewWillPresentScreenCallback_m374AF054824A2809817D4D34D05128633FD80BDB(intptr_t ___nativeExpressClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_NativeExpressAdClient_NativeExpressAdViewDidDismissScreenCallback_mB912EF80149AE81DA955BB54023A8919BEE81671(intptr_t ___nativeExpressClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_NativeExpressAdClient_NativeExpressAdViewWillLeaveApplicationCallback_m39E270E3EBE1071BA14C4D71464294BAA4C70D28(intptr_t ___nativeExpressClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidReceiveAdCallback_mC3AC0F651C19E19CC6247CB1AF7E694C3A8968E2(intptr_t ___rewardBasedVideoAdClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_m92F8D48CFD9859C3DF734B3B531377978AF137BD(intptr_t ___rewardBasedVideoAdClient0, char* ___error1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidOpenCallback_m139F9D3FE045E6AD73E3C653FD5BC0D1B4CF1901(intptr_t ___rewardBasedVideoAdClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidStartCallback_mF45B63FD0D47CFC8566ED5B3137D933247CBAE87(intptr_t ___rewardBasedVideoAdClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidCloseCallback_m734CE280D2BBC57576B3A57E32C521E83443B202(intptr_t ___rewardBasedVideoAdClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidRewardUserCallback_mC1CA5205857E2C48D6CB3F2BE820505DF05D7070(intptr_t ___rewardBasedVideoAdClient0, char* ___rewardType1, double ___rewardAmount2);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdWillLeaveApplicationCallback_mFF7E45939A22B52E3C318C1115C13E42827B349C(intptr_t ___rewardBasedVideoAdClient0);


extern const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[];
const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[30] = 
{
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_EventProvider_EtwEnableCallBack_m32987ABF4E909DC5476F09C034714951CB4A8048),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_OSSpecificSynchronizationContext_InvocationEntry_m056BCE43FF155AAE872FF7E565F8F72A50D26147),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_AppleStoreImpl_MessageCallback_m1AFEBE0209301D9DE3F08BF561DC43984AEC3251),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_FacebookStoreImpl_MessageCallback_m95B738D784D0C0AC054DE6F7DBC86B4E12517105),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_TizenStoreImpl_MessageCallback_m468657DE860B56D8404F5502CB68ABA6C7B13748),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_AdLoaderClient_AdLoaderDidReceiveNativeCustomTemplateAdCallback_m96CB8031ECC1E20CDB3AA013D0E1CA5C82FBA9F0),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_AdLoaderClient_AdLoaderDidFailToReceiveAdWithErrorCallback_mC2A70F1FD62DF7B697FF79C19CDB49D89A3ED296),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_BannerClient_AdViewDidReceiveAdCallback_m5F0611A8BD59EBB7E6A4CFD4AF1C20132C7D5345),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_BannerClient_AdViewDidFailToReceiveAdWithErrorCallback_m863303F5E9FF5A436CE67C416A3E708E9E2F6206),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_BannerClient_AdViewWillPresentScreenCallback_mCCCDADC61157A1E8B91B12B866A326AE24024816),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_BannerClient_AdViewDidDismissScreenCallback_m15101864FE77339582C8DF5FC5129E33CFC9674B),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_BannerClient_AdViewWillLeaveApplicationCallback_mD2C41507939D77A4986009D02E1962A5A200AFE8),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_CustomNativeTemplateClient_NativeCustomTemplateDidReceiveClickCallback_m93BA0F7BF0763D9ECEECB912C673A1BAED9D989F),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_InterstitialClient_InterstitialDidReceiveAdCallback_mF485EB5E41F80ED5C5B6FF0376CD4B8BB96EED2B),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_InterstitialClient_InterstitialDidFailToReceiveAdWithErrorCallback_m9BD349E80A9B92676F0BF98C52C36A690DF107CA),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_InterstitialClient_InterstitialWillPresentScreenCallback_mB5835C4991F852B2E0768EAE7F9E9478EB2FD2C5),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_InterstitialClient_InterstitialDidDismissScreenCallback_mAD21EEBDD6D829719AF1FB8F438CC04C63EF431C),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_InterstitialClient_InterstitialWillLeaveApplicationCallback_m41A6D1FEF7A23CB20F37D767455B69D71FE83D42),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_NativeExpressAdClient_NativeExpressAdViewDidReceiveAdCallback_m7E5BCA95DAE8C27B53B4DF27571E1D47DC1BC924),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_NativeExpressAdClient_NativeExpressAdViewDidFailToReceiveAdWithErrorCallback_m02B2DBEB39F61BC6B4188F167105425E5139DF1B),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_NativeExpressAdClient_NativeExpressAdViewWillPresentScreenCallback_m374AF054824A2809817D4D34D05128633FD80BDB),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_NativeExpressAdClient_NativeExpressAdViewDidDismissScreenCallback_mB912EF80149AE81DA955BB54023A8919BEE81671),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_NativeExpressAdClient_NativeExpressAdViewWillLeaveApplicationCallback_m39E270E3EBE1071BA14C4D71464294BAA4C70D28),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidReceiveAdCallback_mC3AC0F651C19E19CC6247CB1AF7E694C3A8968E2),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_m92F8D48CFD9859C3DF734B3B531377978AF137BD),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidOpenCallback_m139F9D3FE045E6AD73E3C653FD5BC0D1B4CF1901),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidStartCallback_mF45B63FD0D47CFC8566ED5B3137D933247CBAE87),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidCloseCallback_m734CE280D2BBC57576B3A57E32C521E83443B202),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidRewardUserCallback_mC1CA5205857E2C48D6CB3F2BE820505DF05D7070),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdWillLeaveApplicationCallback_mFF7E45939A22B52E3C318C1115C13E42827B349C),
};
