﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct GenericVirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct GenericInterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct GenericInterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// GoogleMobileAds.Api.AdRequest
struct AdRequest_t98763832B122F67AC2952360B6381F4F5848306F;
// GoogleMobileAds.Api.AdRequest/Builder
struct Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9;
// GoogleMobileAds.Api.Mediation.MediationExtras
struct MediationExtras_t53F0F7F90139A2D039272C9764DC880D4B3AB301;
// GoogleMobileAds.Api.Mediation.MediationExtras[]
struct MediationExtrasU5BU5D_tFB81D6FC5C84CCBF15F7317FB1BFAEAC22FCFF16;
// GoogleMobileAds.Api.Reward
struct Reward_t5656463A5A19508F4B9AF6BAB14D343F5A9EC260;
// GoogleMobileAds.Api.RewardBasedVideoAd
struct RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491;
// GoogleMobileAds.Common.IRewardBasedVideoAdClient
struct IRewardBasedVideoAdClient_tE13DAC556DD16DB599ADC16C58B10889C179AB01;
// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdWillLeaveApplicationCallback
struct GADURewardBasedVideoAdWillLeaveApplicationCallback_t0056DE714C24D44E29732E1C20F113CB30974404;
// GoogleMobileAds.iOS.Utils
struct Utils_t556E7CC3B1D0D46BFFE21762D6F9E776521277D4;
// Menu
struct Menu_t7D066A010AF240D53C2DDA2B05DF31FD0B5F3129;
// MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t0B801029DE3A1B9D601C52FDBED1EC104703FB02;
// Purchaser
struct Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037;
// Purchaser/<>c
struct U3CU3Ec_t4CD03EBC853C316BC6B34DD2FD63D9BDEE3F6BC2;
// System.Action`1<System.Boolean>
struct Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Attribute
struct Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.String>[]
struct EntryU5BU5D_t034347107F1D23C91DE1D712EA637D904789415C;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.String>
struct KeyCollection_tC73654392B284B89334464107B696C9BD89776D9;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.String>
struct ValueCollection_tA3B972EF56F7C97E35054155C33556C55FAAFD43;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Purchasing.Product>
struct Dictionary_2_tCAF0D54E59FAC220E2EF9D3ECAFF045862EC66C0;
// System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.String>
struct Dictionary_2_t49835E8A3E77B2EEF19B998068B89F2CFEBD24E3;
// System.Collections.Generic.HashSet`1/Slot<System.String>[]
struct SlotU5BU5D_t7D654D5D811EA1E18FB487C5E2E5081DEC5F9975;
// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t725419BA457D845928B505ACE877FF46BC71E897;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61;
// System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Product>
struct HashSet_1_t361D3549FC7078E1C2F7C4C344909910E39A0D92;
// System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>
struct HashSet_1_tE69581B2FBB736F3DE03D470AE10601F835DF863;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t1F07EAC22CC1D4F279164B144240E4718BD7E7A9;
// System.Collections.Generic.List`1<GoogleMobileAds.Api.Mediation.MediationExtras>
struct List_1_t8F38EE057190BE9897CDE829F7EB4282DF6F3EBA;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.PayoutDefinition>
struct List_1_t3BB189095B85DE5B71713AA7779513A84EB7EB9A;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>
struct EventHandler_1_t8C0A860DD2008426741B7F51D392BD377092A45C;
// System.EventHandler`1<GoogleMobileAds.Api.Reward>
struct EventHandler_1_tE84E7D22A41CECD013ED02984B817FEBA6923CC8;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1A2ED46832F9B37E4743C62DC7F242BAD7E46479;
// System.EventHandler`1<System.Object>
struct EventHandler_1_t10245A26B14DDE8DDFD5B263BDE0641F17DCFDC3;
// System.Func`2<UnityEngine.Purchasing.Product,System.String>
struct Func_2_tE3DDEF72300908E3958302E8350C5EECED6BD11E;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.Binder
struct Binder_t4D5CB06963501D32847C057B57157D6DC49CA759;
// System.Reflection.MemberFilter
struct MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// Uniject.IUtil
struct IUtil_t2165B16C13F1C6C5AFFD2F8039367AFDB972D697;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520;
// UnityEngine.ILogger
struct ILogger_t572B66532D8EB6E76240476A788384A26D70866F;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Purchasing.CloudCatalogImpl
struct CloudCatalogImpl_t4E9FD02BFB4CF8B40D79A7CDE96B860D38CC0FB3;
// UnityEngine.Purchasing.ConfigurationBuilder
struct ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE;
// UnityEngine.Purchasing.Extension.IPurchasingBinder
struct IPurchasingBinder_t29DFEB92F0789E5E0A5F52148451C2A5F9BEA425;
// UnityEngine.Purchasing.Extension.IPurchasingModule
struct IPurchasingModule_tBA2577ED1E963CA0ABA1DFA359A020982DE0B1BF;
// UnityEngine.Purchasing.Extension.IPurchasingModule[]
struct IPurchasingModuleU5BU5D_tF106F307EE194D8B47140E160F5BC38EDFFF9384;
// UnityEngine.Purchasing.IAsyncWebUtil
struct IAsyncWebUtil_t652AE97438C7488173CD558F586ED1BC547EDD60;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_t4AEE9698E20BA2DCA32EC4729DCAA60D09DA95CA;
// UnityEngine.Purchasing.INativeStoreProvider
struct INativeStoreProvider_t87C597D591C6E87EEE53807B67C071E0DC834BC0;
// UnityEngine.Purchasing.IStoreController
struct IStoreController_t6A30FF7F81804FCC0082AB9DF2CE86BCFB91DF67;
// UnityEngine.Purchasing.IStoreListener
struct IStoreListener_tBB9A0373D14167C7600B988F37B85E14E8D60A66;
// UnityEngine.Purchasing.Product
struct Product_t830A133A97BEA12C3CD696853098A295D99A6DE4;
// UnityEngine.Purchasing.ProductCollection
struct ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07;
// UnityEngine.Purchasing.ProductDefinition
struct ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10;
// UnityEngine.Purchasing.ProductMetadata
struct ProductMetadata_t1CAA912963F8B24B34F53CCF29A6E0BD89B33B48;
// UnityEngine.Purchasing.Product[]
struct ProductU5BU5D_tCC1E9F34B8B3A88563CA989013308FB058864237;
// UnityEngine.Purchasing.PurchaseEventArgs
struct PurchaseEventArgs_tF6E04BFD5492F5F57309FFBB2415EB26E5B88C04;
// UnityEngine.Purchasing.PurchasingFactory
struct PurchasingFactory_tAFBB2D3F6FD1740FCC4B64AE02E4BFFF8A2AB765;
// UnityEngine.Purchasing.StandardPurchasingModule
struct StandardPurchasingModule_tAB117B14746822C4AF812B66148122909DD53847;
// UnityEngine.Purchasing.StandardPurchasingModule/StoreInstance
struct StoreInstance_tA65323B0763035D2948B6E42398D527E0365978C;
// UnityEngine.Purchasing.WinRTStore
struct WinRTStore_t85FB198173C694935BF2F8E2EF2069AFB7E3E68F;

IL2CPP_EXTERN_C RuntimeClass* Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EventHandler_1_tE84E7D22A41CECD013ED02984B817FEBA6923CC8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IAppleExtensions_tB86ACF573E6F6E038B547C551A0459A693DDE17C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IStoreController_t6A30FF7F81804FCC0082AB9DF2CE86BCFB91DF67_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InitializationFailureReason_t5A6284D67FA09D301793E67D8B7003299F4D3062_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IntPtr_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PurchaseFailureReason_t42EC65C1103B27F82198DC42C8D96C1A14ED7432_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RuntimePlatform_tD5F5737C1BBBCBB115EB104DF2B7876387E80132_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StandardPurchasingModule_tAB117B14746822C4AF812B66148122909DD53847_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t4CD03EBC853C316BC6B34DD2FD63D9BDEE3F6BC2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral113ACB59CC385DBB3293CAFA0C8B52A644B633DF;
IL2CPP_EXTERN_C String_t* _stringLiteral18CFCD24381BFC95C94A162DFB57B289E5A36585;
IL2CPP_EXTERN_C String_t* _stringLiteral33366CF6F9F373698DE577F581C8F88B40982A02;
IL2CPP_EXTERN_C String_t* _stringLiteral361EE03281C3856B6F0042183EFFB1584B30D7AC;
IL2CPP_EXTERN_C String_t* _stringLiteral46F11B6B621D61F57230E331CE1A1C24927FAC27;
IL2CPP_EXTERN_C String_t* _stringLiteral4FA3A1827DAD202EA37346A8E2899BD01A256858;
IL2CPP_EXTERN_C String_t* _stringLiteral5387EB5DAB5EC6B269E296E7EE0C7033A094E744;
IL2CPP_EXTERN_C String_t* _stringLiteral541A0FB97764D244630A052F6CA777D4285581F9;
IL2CPP_EXTERN_C String_t* _stringLiteral6672E4B2C5B31990F57E7C47B6E501B6DA4AE7B5;
IL2CPP_EXTERN_C String_t* _stringLiteral694BCD90F42D64B37F587CB128C57C1C0669E065;
IL2CPP_EXTERN_C String_t* _stringLiteral6E0822F145777280292381332960A5EDEB0560B4;
IL2CPP_EXTERN_C String_t* _stringLiteral728E0F853BE209BCB8D5467D302BF96125A98E5D;
IL2CPP_EXTERN_C String_t* _stringLiteral735D638870FD9A0CFDCEE74317CFC691B237F1CC;
IL2CPP_EXTERN_C String_t* _stringLiteral79134CE1905549F57C45F0675918958B49A8627C;
IL2CPP_EXTERN_C String_t* _stringLiteral8050389710BA17EF56B61EFEC3D606A234949412;
IL2CPP_EXTERN_C String_t* _stringLiteral9339764CAB44BA34A23F1B0C57220F7617877495;
IL2CPP_EXTERN_C String_t* _stringLiteralA8CD2D6BCE16F5AA74C61C9FD527B4146958B48D;
IL2CPP_EXTERN_C String_t* _stringLiteralAC6876A1732FDFAF2A6BFDBB9A20AC4C8FADAF39;
IL2CPP_EXTERN_C String_t* _stringLiteralB76F0072492A26196E1938F3067FDB84969270DF;
IL2CPP_EXTERN_C String_t* _stringLiteralB9C36642D40977DF4D4E7B56EEAB4CC19FC3F3EF;
IL2CPP_EXTERN_C String_t* _stringLiteralC95259DE1FD719814DAEF8F1DC4BD64F9D885FF0;
IL2CPP_EXTERN_C String_t* _stringLiteralCDA051C901386F0E24914B0EEB92EF4E380C159D;
IL2CPP_EXTERN_C String_t* _stringLiteralE3E82846C32567811615378F30240185871E08E5;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_mFEB000BCBDAB6DE953B2B50CD113641DCF601890_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Array_Empty_TisIPurchasingModule_tBA2577ED1E963CA0ABA1DFA359A020982DE0B1BF_m6A5F4EE32F39C664A1A279F15D55E86D8DA4D93E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_GetEnumerator_m3378B4792B81EF81397CB9D9A761BD7149BD27F5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m16C0E963A012498CD27422B463DB327BA4C7A321_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_mB4D942857769BA4A52C25194705752C32D86DCED_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_mB81C8E39092695921473DBCA98385B2C53AFA7CF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_mD9B1DB257A9F9A3CEA69542101B953689A4AD978_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m129741E497FB617DC9845CFEE4CB27B84C86301A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m6E6A22A8620F5A5582BB67E367BE5086D7D895A6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mCCD47EE234D4C2A16ED96329CC6BC10D2C101702_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mF9F196771804D9FAE1887C1EE679DB80CF90182D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m894E7226842A0AB920967095678A311EFF7C5737_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_mB8FC184CAAB4A8E56B08699B9C9894803A9EA58A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_mBEC9B470213860581893E0F197CAAE657B8B6C69_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_mF1A272E02A81B502A1CDF431D0AC41BE3D53E764_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EventHandler_1__ctor_m1450748EF9416B797EDE630BEA6C50F96956282F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* HashSet_1_GetEnumerator_m2EC6A826B51D71BE1AB94EEFB697BC6412B1CDF8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* IExtensionProvider_GetExtension_TisIAppleExtensions_tB86ACF573E6F6E038B547C551A0459A693DDE17C_mA70A998DA58769AC90A2E909C671E14F966E7E4F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* KeyValuePair_2_get_Key_m434E29A1251E81B5A2124466105823011C462BF2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* KeyValuePair_2_get_Value_mEAF4B15DEEAC6EB29683A5C6569F0F50B4DEBDA2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m739CC8C648FEC19900D51E4F3BCE6B15D3C6325A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_mDFFBEE5A0B86EF1F068C4ED0ABC0F39B7CA7677E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Menu_HandleRewardBasedVideoRewarded_m0B9E46D6532F5EEAAD4E0F3737CE65436DA63572_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1_GetValueOrDefault_m9EDDA3FC2B886F793AC331A3E3617416D931833E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1_GetValueOrDefault_mA2BC7FE28E1E54F6DED8F2DA5ACA72A56083E43D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1_GetValueOrDefault_mD65884636E94AF88C2745255BBE130A1B64BA294_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1_get_HasValue_m275A31438FCDAEEE039E95D887684E04FD6ECE2B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1_get_HasValue_mB231DEE33107C7E966680F0404FFDD7939B24625_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1_get_HasValue_mE966D0285B7519BF51C1B117A064B4F40858077A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CRestorePurchasesU3Eb__11_0_m997253913D06FEB1A817C89E993EC7970359E20E_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t GADURewardBasedVideoAdWillLeaveApplicationCallback_BeginInvoke_mED9BCF0C91B338AD4B9A6E39629994AFEAAE6F32_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Menu_HandleRewardBasedVideoRewarded_m0B9E46D6532F5EEAAD4E0F3737CE65436DA63572_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Menu_PlayBtn_m6E1DEECCF0637E2764399EDAB455C1CC65D2438E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Menu_RateBtn_m88827BCC56691B4728908B482D4D7FAB1F59AB54_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Menu_RequestRewardVideo_mC6FC3DBAC40902860533D8BF5C78FB5BBD476944_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Menu_Start_m26D2BE4EA9DEE5AF2FE548CF8EDDAFA0FE7379E9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Purchaser_BuyConsumable_mFD6E311F8C75EF0A2589C9282FC91463BE256FDD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Purchaser_BuyProductID_mBCDAC5023D5830F87D3E0116AE9DC5447065385E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Purchaser_InitializePurchasing_mA433D882053BB54E75A01A2919EA1989C9470F9F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Purchaser_IsInitialized_m272B7D0B6DB8CDEB87223E1224F013BB3533C890_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Purchaser_OnInitializeFailed_mD26D07D1E337C7DFCA40D1D65DA7F4F85BF449DE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Purchaser_OnInitialized_m2C5D997E02074CA968ACF89D2EA8849616E2AE8A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Purchaser_OnPurchaseFailed_m4A93D014B730F86486878D3A2EEE69C949EB07F9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Purchaser_ProcessPurchase_m9A0B2A4762E2AF7D006F42F231BB676CDDCC6238_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Purchaser_RestorePurchases_m24A1E9C7782A272240B01EB09EBC8828ED550846_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Purchaser_Start_mCD122FAB158727577ED229C323774D10737D1835_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Purchaser__cctor_mADAAF4DC1AEEC46CD5D7A737ED6896BF8BC7C6E8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t RewardBasedVideoAd_get_Instance_mD22F18358E8AAE1EF7D0EF41D89938E27F110413AssemblyU2DCSharp1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec_U3CRestorePurchasesU3Eb__11_0_m997253913D06FEB1A817C89E993EC7970359E20E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__cctor_mBACD32049FC5669777FA1E6521D96C92CD685940_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Utils_BuildAdRequest_m79F99FD27AEBB11A401DF217D6C5F537B8CC9FA2_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520;
struct IPurchasingModuleU5BU5D_tF106F307EE194D8B47140E160F5BC38EDFFF9384;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// Constants
struct  Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C  : public RuntimeObject
{
public:

public:
};

struct Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_StaticFields
{
public:
	// System.String Constants::interstitialID
	String_t* ___interstitialID_0;
	// System.String Constants::rewardedVideoID
	String_t* ___rewardedVideoID_1;
	// System.Int32 Constants::gamePlayed
	int32_t ___gamePlayed_2;
	// System.Int32 Constants::videoAdRewardMoney
	int32_t ___videoAdRewardMoney_3;
	// System.Int32 Constants::startMoney
	int32_t ___startMoney_4;
	// System.String Constants::purchaseMoney1
	String_t* ___purchaseMoney1_5;
	// System.String Constants::purchaseMoney2
	String_t* ___purchaseMoney2_6;
	// System.String Constants::purchaseMoney3
	String_t* ___purchaseMoney3_7;
	// System.String Constants::purchaseMoney4
	String_t* ___purchaseMoney4_8;

public:
	inline static int32_t get_offset_of_interstitialID_0() { return static_cast<int32_t>(offsetof(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_StaticFields, ___interstitialID_0)); }
	inline String_t* get_interstitialID_0() const { return ___interstitialID_0; }
	inline String_t** get_address_of_interstitialID_0() { return &___interstitialID_0; }
	inline void set_interstitialID_0(String_t* value)
	{
		___interstitialID_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___interstitialID_0), (void*)value);
	}

	inline static int32_t get_offset_of_rewardedVideoID_1() { return static_cast<int32_t>(offsetof(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_StaticFields, ___rewardedVideoID_1)); }
	inline String_t* get_rewardedVideoID_1() const { return ___rewardedVideoID_1; }
	inline String_t** get_address_of_rewardedVideoID_1() { return &___rewardedVideoID_1; }
	inline void set_rewardedVideoID_1(String_t* value)
	{
		___rewardedVideoID_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rewardedVideoID_1), (void*)value);
	}

	inline static int32_t get_offset_of_gamePlayed_2() { return static_cast<int32_t>(offsetof(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_StaticFields, ___gamePlayed_2)); }
	inline int32_t get_gamePlayed_2() const { return ___gamePlayed_2; }
	inline int32_t* get_address_of_gamePlayed_2() { return &___gamePlayed_2; }
	inline void set_gamePlayed_2(int32_t value)
	{
		___gamePlayed_2 = value;
	}

	inline static int32_t get_offset_of_videoAdRewardMoney_3() { return static_cast<int32_t>(offsetof(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_StaticFields, ___videoAdRewardMoney_3)); }
	inline int32_t get_videoAdRewardMoney_3() const { return ___videoAdRewardMoney_3; }
	inline int32_t* get_address_of_videoAdRewardMoney_3() { return &___videoAdRewardMoney_3; }
	inline void set_videoAdRewardMoney_3(int32_t value)
	{
		___videoAdRewardMoney_3 = value;
	}

	inline static int32_t get_offset_of_startMoney_4() { return static_cast<int32_t>(offsetof(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_StaticFields, ___startMoney_4)); }
	inline int32_t get_startMoney_4() const { return ___startMoney_4; }
	inline int32_t* get_address_of_startMoney_4() { return &___startMoney_4; }
	inline void set_startMoney_4(int32_t value)
	{
		___startMoney_4 = value;
	}

	inline static int32_t get_offset_of_purchaseMoney1_5() { return static_cast<int32_t>(offsetof(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_StaticFields, ___purchaseMoney1_5)); }
	inline String_t* get_purchaseMoney1_5() const { return ___purchaseMoney1_5; }
	inline String_t** get_address_of_purchaseMoney1_5() { return &___purchaseMoney1_5; }
	inline void set_purchaseMoney1_5(String_t* value)
	{
		___purchaseMoney1_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___purchaseMoney1_5), (void*)value);
	}

	inline static int32_t get_offset_of_purchaseMoney2_6() { return static_cast<int32_t>(offsetof(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_StaticFields, ___purchaseMoney2_6)); }
	inline String_t* get_purchaseMoney2_6() const { return ___purchaseMoney2_6; }
	inline String_t** get_address_of_purchaseMoney2_6() { return &___purchaseMoney2_6; }
	inline void set_purchaseMoney2_6(String_t* value)
	{
		___purchaseMoney2_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___purchaseMoney2_6), (void*)value);
	}

	inline static int32_t get_offset_of_purchaseMoney3_7() { return static_cast<int32_t>(offsetof(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_StaticFields, ___purchaseMoney3_7)); }
	inline String_t* get_purchaseMoney3_7() const { return ___purchaseMoney3_7; }
	inline String_t** get_address_of_purchaseMoney3_7() { return &___purchaseMoney3_7; }
	inline void set_purchaseMoney3_7(String_t* value)
	{
		___purchaseMoney3_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___purchaseMoney3_7), (void*)value);
	}

	inline static int32_t get_offset_of_purchaseMoney4_8() { return static_cast<int32_t>(offsetof(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_StaticFields, ___purchaseMoney4_8)); }
	inline String_t* get_purchaseMoney4_8() const { return ___purchaseMoney4_8; }
	inline String_t** get_address_of_purchaseMoney4_8() { return &___purchaseMoney4_8; }
	inline void set_purchaseMoney4_8(String_t* value)
	{
		___purchaseMoney4_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___purchaseMoney4_8), (void*)value);
	}
};


// GoogleMobileAds.Api.Mediation.MediationExtras
struct  MediationExtras_t53F0F7F90139A2D039272C9764DC880D4B3AB301  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> GoogleMobileAds.Api.Mediation.MediationExtras::<Extras>k__BackingField
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___U3CExtrasU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CExtrasU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MediationExtras_t53F0F7F90139A2D039272C9764DC880D4B3AB301, ___U3CExtrasU3Ek__BackingField_0)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_U3CExtrasU3Ek__BackingField_0() const { return ___U3CExtrasU3Ek__BackingField_0; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_U3CExtrasU3Ek__BackingField_0() { return &___U3CExtrasU3Ek__BackingField_0; }
	inline void set_U3CExtrasU3Ek__BackingField_0(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___U3CExtrasU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CExtrasU3Ek__BackingField_0), (void*)value);
	}
};


// GoogleMobileAds.Api.RewardBasedVideoAd
struct  RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491  : public RuntimeObject
{
public:
	// GoogleMobileAds.Common.IRewardBasedVideoAdClient GoogleMobileAds.Api.RewardBasedVideoAd::client
	RuntimeObject* ___client_0;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdLoaded
	EventHandler_1_t1A2ED46832F9B37E4743C62DC7F242BAD7E46479 * ___OnAdLoaded_2;
	// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdFailedToLoad
	EventHandler_1_t8C0A860DD2008426741B7F51D392BD377092A45C * ___OnAdFailedToLoad_3;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdOpening
	EventHandler_1_t1A2ED46832F9B37E4743C62DC7F242BAD7E46479 * ___OnAdOpening_4;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdStarted
	EventHandler_1_t1A2ED46832F9B37E4743C62DC7F242BAD7E46479 * ___OnAdStarted_5;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdClosed
	EventHandler_1_t1A2ED46832F9B37E4743C62DC7F242BAD7E46479 * ___OnAdClosed_6;
	// System.EventHandler`1<GoogleMobileAds.Api.Reward> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdRewarded
	EventHandler_1_tE84E7D22A41CECD013ED02984B817FEBA6923CC8 * ___OnAdRewarded_7;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdLeavingApplication
	EventHandler_1_t1A2ED46832F9B37E4743C62DC7F242BAD7E46479 * ___OnAdLeavingApplication_8;

public:
	inline static int32_t get_offset_of_client_0() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491, ___client_0)); }
	inline RuntimeObject* get_client_0() const { return ___client_0; }
	inline RuntimeObject** get_address_of_client_0() { return &___client_0; }
	inline void set_client_0(RuntimeObject* value)
	{
		___client_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___client_0), (void*)value);
	}

	inline static int32_t get_offset_of_OnAdLoaded_2() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491, ___OnAdLoaded_2)); }
	inline EventHandler_1_t1A2ED46832F9B37E4743C62DC7F242BAD7E46479 * get_OnAdLoaded_2() const { return ___OnAdLoaded_2; }
	inline EventHandler_1_t1A2ED46832F9B37E4743C62DC7F242BAD7E46479 ** get_address_of_OnAdLoaded_2() { return &___OnAdLoaded_2; }
	inline void set_OnAdLoaded_2(EventHandler_1_t1A2ED46832F9B37E4743C62DC7F242BAD7E46479 * value)
	{
		___OnAdLoaded_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAdLoaded_2), (void*)value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_3() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491, ___OnAdFailedToLoad_3)); }
	inline EventHandler_1_t8C0A860DD2008426741B7F51D392BD377092A45C * get_OnAdFailedToLoad_3() const { return ___OnAdFailedToLoad_3; }
	inline EventHandler_1_t8C0A860DD2008426741B7F51D392BD377092A45C ** get_address_of_OnAdFailedToLoad_3() { return &___OnAdFailedToLoad_3; }
	inline void set_OnAdFailedToLoad_3(EventHandler_1_t8C0A860DD2008426741B7F51D392BD377092A45C * value)
	{
		___OnAdFailedToLoad_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAdFailedToLoad_3), (void*)value);
	}

	inline static int32_t get_offset_of_OnAdOpening_4() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491, ___OnAdOpening_4)); }
	inline EventHandler_1_t1A2ED46832F9B37E4743C62DC7F242BAD7E46479 * get_OnAdOpening_4() const { return ___OnAdOpening_4; }
	inline EventHandler_1_t1A2ED46832F9B37E4743C62DC7F242BAD7E46479 ** get_address_of_OnAdOpening_4() { return &___OnAdOpening_4; }
	inline void set_OnAdOpening_4(EventHandler_1_t1A2ED46832F9B37E4743C62DC7F242BAD7E46479 * value)
	{
		___OnAdOpening_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAdOpening_4), (void*)value);
	}

	inline static int32_t get_offset_of_OnAdStarted_5() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491, ___OnAdStarted_5)); }
	inline EventHandler_1_t1A2ED46832F9B37E4743C62DC7F242BAD7E46479 * get_OnAdStarted_5() const { return ___OnAdStarted_5; }
	inline EventHandler_1_t1A2ED46832F9B37E4743C62DC7F242BAD7E46479 ** get_address_of_OnAdStarted_5() { return &___OnAdStarted_5; }
	inline void set_OnAdStarted_5(EventHandler_1_t1A2ED46832F9B37E4743C62DC7F242BAD7E46479 * value)
	{
		___OnAdStarted_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAdStarted_5), (void*)value);
	}

	inline static int32_t get_offset_of_OnAdClosed_6() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491, ___OnAdClosed_6)); }
	inline EventHandler_1_t1A2ED46832F9B37E4743C62DC7F242BAD7E46479 * get_OnAdClosed_6() const { return ___OnAdClosed_6; }
	inline EventHandler_1_t1A2ED46832F9B37E4743C62DC7F242BAD7E46479 ** get_address_of_OnAdClosed_6() { return &___OnAdClosed_6; }
	inline void set_OnAdClosed_6(EventHandler_1_t1A2ED46832F9B37E4743C62DC7F242BAD7E46479 * value)
	{
		___OnAdClosed_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAdClosed_6), (void*)value);
	}

	inline static int32_t get_offset_of_OnAdRewarded_7() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491, ___OnAdRewarded_7)); }
	inline EventHandler_1_tE84E7D22A41CECD013ED02984B817FEBA6923CC8 * get_OnAdRewarded_7() const { return ___OnAdRewarded_7; }
	inline EventHandler_1_tE84E7D22A41CECD013ED02984B817FEBA6923CC8 ** get_address_of_OnAdRewarded_7() { return &___OnAdRewarded_7; }
	inline void set_OnAdRewarded_7(EventHandler_1_tE84E7D22A41CECD013ED02984B817FEBA6923CC8 * value)
	{
		___OnAdRewarded_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAdRewarded_7), (void*)value);
	}

	inline static int32_t get_offset_of_OnAdLeavingApplication_8() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491, ___OnAdLeavingApplication_8)); }
	inline EventHandler_1_t1A2ED46832F9B37E4743C62DC7F242BAD7E46479 * get_OnAdLeavingApplication_8() const { return ___OnAdLeavingApplication_8; }
	inline EventHandler_1_t1A2ED46832F9B37E4743C62DC7F242BAD7E46479 ** get_address_of_OnAdLeavingApplication_8() { return &___OnAdLeavingApplication_8; }
	inline void set_OnAdLeavingApplication_8(EventHandler_1_t1A2ED46832F9B37E4743C62DC7F242BAD7E46479 * value)
	{
		___OnAdLeavingApplication_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAdLeavingApplication_8), (void*)value);
	}
};

struct RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491_StaticFields
{
public:
	// GoogleMobileAds.Api.RewardBasedVideoAd GoogleMobileAds.Api.RewardBasedVideoAd::instance
	RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491 * ___instance_1;

public:
	inline static int32_t get_offset_of_instance_1() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491_StaticFields, ___instance_1)); }
	inline RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491 * get_instance_1() const { return ___instance_1; }
	inline RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491 ** get_address_of_instance_1() { return &___instance_1; }
	inline void set_instance_1(RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491 * value)
	{
		___instance_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_1), (void*)value);
	}
};


// GoogleMobileAds.iOS.Utils
struct  Utils_t556E7CC3B1D0D46BFFE21762D6F9E776521277D4  : public RuntimeObject
{
public:

public:
};


// Purchaser_<>c
struct  U3CU3Ec_t4CD03EBC853C316BC6B34DD2FD63D9BDEE3F6BC2  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t4CD03EBC853C316BC6B34DD2FD63D9BDEE3F6BC2_StaticFields
{
public:
	// Purchaser_<>c Purchaser_<>c::<>9
	U3CU3Ec_t4CD03EBC853C316BC6B34DD2FD63D9BDEE3F6BC2 * ___U3CU3E9_0;
	// System.Action`1<System.Boolean> Purchaser_<>c::<>9__11_0
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___U3CU3E9__11_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4CD03EBC853C316BC6B34DD2FD63D9BDEE3F6BC2_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t4CD03EBC853C316BC6B34DD2FD63D9BDEE3F6BC2 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t4CD03EBC853C316BC6B34DD2FD63D9BDEE3F6BC2 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t4CD03EBC853C316BC6B34DD2FD63D9BDEE3F6BC2 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__11_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4CD03EBC853C316BC6B34DD2FD63D9BDEE3F6BC2_StaticFields, ___U3CU3E9__11_0_1)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_U3CU3E9__11_0_1() const { return ___U3CU3E9__11_0_1; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_U3CU3E9__11_0_1() { return &___U3CU3E9__11_0_1; }
	inline void set_U3CU3E9__11_0_1(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___U3CU3E9__11_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__11_0_1), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};


// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct  Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___buckets_0;
	// System.Collections.Generic.Dictionary`2_Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t034347107F1D23C91DE1D712EA637D904789415C* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2_KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_tC73654392B284B89334464107B696C9BD89776D9 * ___keys_7;
	// System.Collections.Generic.Dictionary`2_ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_tA3B972EF56F7C97E35054155C33556C55FAAFD43 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC, ___buckets_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC, ___entries_1)); }
	inline EntryU5BU5D_t034347107F1D23C91DE1D712EA637D904789415C* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t034347107F1D23C91DE1D712EA637D904789415C** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t034347107F1D23C91DE1D712EA637D904789415C* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC, ___keys_7)); }
	inline KeyCollection_tC73654392B284B89334464107B696C9BD89776D9 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_tC73654392B284B89334464107B696C9BD89776D9 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_tC73654392B284B89334464107B696C9BD89776D9 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC, ___values_8)); }
	inline ValueCollection_tA3B972EF56F7C97E35054155C33556C55FAAFD43 * get_values_8() const { return ___values_8; }
	inline ValueCollection_tA3B972EF56F7C97E35054155C33556C55FAAFD43 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_tA3B972EF56F7C97E35054155C33556C55FAAFD43 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Collections.Generic.HashSet`1<System.String>
struct  HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.HashSet`1::_buckets
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____buckets_7;
	// System.Collections.Generic.HashSet`1_Slot<T>[] System.Collections.Generic.HashSet`1::_slots
	SlotU5BU5D_t7D654D5D811EA1E18FB487C5E2E5081DEC5F9975* ____slots_8;
	// System.Int32 System.Collections.Generic.HashSet`1::_count
	int32_t ____count_9;
	// System.Int32 System.Collections.Generic.HashSet`1::_lastIndex
	int32_t ____lastIndex_10;
	// System.Int32 System.Collections.Generic.HashSet`1::_freeList
	int32_t ____freeList_11;
	// System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::_comparer
	RuntimeObject* ____comparer_12;
	// System.Int32 System.Collections.Generic.HashSet`1::_version
	int32_t ____version_13;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.HashSet`1::_siInfo
	SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * ____siInfo_14;

public:
	inline static int32_t get_offset_of__buckets_7() { return static_cast<int32_t>(offsetof(HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61, ____buckets_7)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__buckets_7() const { return ____buckets_7; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__buckets_7() { return &____buckets_7; }
	inline void set__buckets_7(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____buckets_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____buckets_7), (void*)value);
	}

	inline static int32_t get_offset_of__slots_8() { return static_cast<int32_t>(offsetof(HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61, ____slots_8)); }
	inline SlotU5BU5D_t7D654D5D811EA1E18FB487C5E2E5081DEC5F9975* get__slots_8() const { return ____slots_8; }
	inline SlotU5BU5D_t7D654D5D811EA1E18FB487C5E2E5081DEC5F9975** get_address_of__slots_8() { return &____slots_8; }
	inline void set__slots_8(SlotU5BU5D_t7D654D5D811EA1E18FB487C5E2E5081DEC5F9975* value)
	{
		____slots_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____slots_8), (void*)value);
	}

	inline static int32_t get_offset_of__count_9() { return static_cast<int32_t>(offsetof(HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61, ____count_9)); }
	inline int32_t get__count_9() const { return ____count_9; }
	inline int32_t* get_address_of__count_9() { return &____count_9; }
	inline void set__count_9(int32_t value)
	{
		____count_9 = value;
	}

	inline static int32_t get_offset_of__lastIndex_10() { return static_cast<int32_t>(offsetof(HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61, ____lastIndex_10)); }
	inline int32_t get__lastIndex_10() const { return ____lastIndex_10; }
	inline int32_t* get_address_of__lastIndex_10() { return &____lastIndex_10; }
	inline void set__lastIndex_10(int32_t value)
	{
		____lastIndex_10 = value;
	}

	inline static int32_t get_offset_of__freeList_11() { return static_cast<int32_t>(offsetof(HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61, ____freeList_11)); }
	inline int32_t get__freeList_11() const { return ____freeList_11; }
	inline int32_t* get_address_of__freeList_11() { return &____freeList_11; }
	inline void set__freeList_11(int32_t value)
	{
		____freeList_11 = value;
	}

	inline static int32_t get_offset_of__comparer_12() { return static_cast<int32_t>(offsetof(HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61, ____comparer_12)); }
	inline RuntimeObject* get__comparer_12() const { return ____comparer_12; }
	inline RuntimeObject** get_address_of__comparer_12() { return &____comparer_12; }
	inline void set__comparer_12(RuntimeObject* value)
	{
		____comparer_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____comparer_12), (void*)value);
	}

	inline static int32_t get_offset_of__version_13() { return static_cast<int32_t>(offsetof(HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61, ____version_13)); }
	inline int32_t get__version_13() const { return ____version_13; }
	inline int32_t* get_address_of__version_13() { return &____version_13; }
	inline void set__version_13(int32_t value)
	{
		____version_13 = value;
	}

	inline static int32_t get_offset_of__siInfo_14() { return static_cast<int32_t>(offsetof(HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61, ____siInfo_14)); }
	inline SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * get__siInfo_14() const { return ____siInfo_14; }
	inline SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 ** get_address_of__siInfo_14() { return &____siInfo_14; }
	inline void set__siInfo_14(SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * value)
	{
		____siInfo_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____siInfo_14), (void*)value);
	}
};


// System.Collections.Generic.List`1<GoogleMobileAds.Api.Mediation.MediationExtras>
struct  List_1_t8F38EE057190BE9897CDE829F7EB4282DF6F3EBA  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	MediationExtrasU5BU5D_tFB81D6FC5C84CCBF15F7317FB1BFAEAC22FCFF16* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t8F38EE057190BE9897CDE829F7EB4282DF6F3EBA, ____items_1)); }
	inline MediationExtrasU5BU5D_tFB81D6FC5C84CCBF15F7317FB1BFAEAC22FCFF16* get__items_1() const { return ____items_1; }
	inline MediationExtrasU5BU5D_tFB81D6FC5C84CCBF15F7317FB1BFAEAC22FCFF16** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(MediationExtrasU5BU5D_tFB81D6FC5C84CCBF15F7317FB1BFAEAC22FCFF16* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t8F38EE057190BE9897CDE829F7EB4282DF6F3EBA, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t8F38EE057190BE9897CDE829F7EB4282DF6F3EBA, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t8F38EE057190BE9897CDE829F7EB4282DF6F3EBA, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t8F38EE057190BE9897CDE829F7EB4282DF6F3EBA_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	MediationExtrasU5BU5D_tFB81D6FC5C84CCBF15F7317FB1BFAEAC22FCFF16* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t8F38EE057190BE9897CDE829F7EB4282DF6F3EBA_StaticFields, ____emptyArray_5)); }
	inline MediationExtrasU5BU5D_tFB81D6FC5C84CCBF15F7317FB1BFAEAC22FCFF16* get__emptyArray_5() const { return ____emptyArray_5; }
	inline MediationExtrasU5BU5D_tFB81D6FC5C84CCBF15F7317FB1BFAEAC22FCFF16** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(MediationExtrasU5BU5D_tFB81D6FC5C84CCBF15F7317FB1BFAEAC22FCFF16* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.String>
struct  List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____items_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__items_1() const { return ____items_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_StaticFields, ____emptyArray_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__emptyArray_5() const { return ____emptyArray_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.EmptyArray`1<System.Object>
struct  EmptyArray_1_tCF137C88A5824F413EFB5A2F31664D8207E61D26  : public RuntimeObject
{
public:

public:
};

struct EmptyArray_1_tCF137C88A5824F413EFB5A2F31664D8207E61D26_StaticFields
{
public:
	// T[] System.EmptyArray`1::Value
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___Value_0;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(EmptyArray_1_tCF137C88A5824F413EFB5A2F31664D8207E61D26_StaticFields, ___Value_0)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_Value_0() const { return ___Value_0; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Value_0), (void*)value);
	}
};


// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_0), (void*)value);
	}
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// UnityEngine.Purchasing.ConfigurationBuilder
struct  ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.PurchasingFactory UnityEngine.Purchasing.ConfigurationBuilder::m_Factory
	PurchasingFactory_tAFBB2D3F6FD1740FCC4B64AE02E4BFFF8A2AB765 * ___m_Factory_0;
	// System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition> UnityEngine.Purchasing.ConfigurationBuilder::m_Products
	HashSet_1_tE69581B2FBB736F3DE03D470AE10601F835DF863 * ___m_Products_1;
	// System.Boolean UnityEngine.Purchasing.ConfigurationBuilder::<useCatalogProvider>k__BackingField
	bool ___U3CuseCatalogProviderU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_m_Factory_0() { return static_cast<int32_t>(offsetof(ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE, ___m_Factory_0)); }
	inline PurchasingFactory_tAFBB2D3F6FD1740FCC4B64AE02E4BFFF8A2AB765 * get_m_Factory_0() const { return ___m_Factory_0; }
	inline PurchasingFactory_tAFBB2D3F6FD1740FCC4B64AE02E4BFFF8A2AB765 ** get_address_of_m_Factory_0() { return &___m_Factory_0; }
	inline void set_m_Factory_0(PurchasingFactory_tAFBB2D3F6FD1740FCC4B64AE02E4BFFF8A2AB765 * value)
	{
		___m_Factory_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Factory_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Products_1() { return static_cast<int32_t>(offsetof(ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE, ___m_Products_1)); }
	inline HashSet_1_tE69581B2FBB736F3DE03D470AE10601F835DF863 * get_m_Products_1() const { return ___m_Products_1; }
	inline HashSet_1_tE69581B2FBB736F3DE03D470AE10601F835DF863 ** get_address_of_m_Products_1() { return &___m_Products_1; }
	inline void set_m_Products_1(HashSet_1_tE69581B2FBB736F3DE03D470AE10601F835DF863 * value)
	{
		___m_Products_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Products_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseCatalogProviderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE, ___U3CuseCatalogProviderU3Ek__BackingField_2)); }
	inline bool get_U3CuseCatalogProviderU3Ek__BackingField_2() const { return ___U3CuseCatalogProviderU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CuseCatalogProviderU3Ek__BackingField_2() { return &___U3CuseCatalogProviderU3Ek__BackingField_2; }
	inline void set_U3CuseCatalogProviderU3Ek__BackingField_2(bool value)
	{
		___U3CuseCatalogProviderU3Ek__BackingField_2 = value;
	}
};


// UnityEngine.Purchasing.Extension.AbstractPurchasingModule
struct  AbstractPurchasingModule_t73F8B7B7D6CA305D6CFAFE4692A6F0F7B42531D9  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.Extension.IPurchasingBinder UnityEngine.Purchasing.Extension.AbstractPurchasingModule::m_Binder
	RuntimeObject* ___m_Binder_0;

public:
	inline static int32_t get_offset_of_m_Binder_0() { return static_cast<int32_t>(offsetof(AbstractPurchasingModule_t73F8B7B7D6CA305D6CFAFE4692A6F0F7B42531D9, ___m_Binder_0)); }
	inline RuntimeObject* get_m_Binder_0() const { return ___m_Binder_0; }
	inline RuntimeObject** get_address_of_m_Binder_0() { return &___m_Binder_0; }
	inline void set_m_Binder_0(RuntimeObject* value)
	{
		___m_Binder_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Binder_0), (void*)value);
	}
};


// UnityEngine.Purchasing.Product
struct  Product_t830A133A97BEA12C3CD696853098A295D99A6DE4  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.ProductDefinition UnityEngine.Purchasing.Product::<definition>k__BackingField
	ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * ___U3CdefinitionU3Ek__BackingField_0;
	// UnityEngine.Purchasing.ProductMetadata UnityEngine.Purchasing.Product::<metadata>k__BackingField
	ProductMetadata_t1CAA912963F8B24B34F53CCF29A6E0BD89B33B48 * ___U3CmetadataU3Ek__BackingField_1;
	// System.Boolean UnityEngine.Purchasing.Product::<availableToPurchase>k__BackingField
	bool ___U3CavailableToPurchaseU3Ek__BackingField_2;
	// System.String UnityEngine.Purchasing.Product::<transactionID>k__BackingField
	String_t* ___U3CtransactionIDU3Ek__BackingField_3;
	// System.String UnityEngine.Purchasing.Product::<receipt>k__BackingField
	String_t* ___U3CreceiptU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CdefinitionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Product_t830A133A97BEA12C3CD696853098A295D99A6DE4, ___U3CdefinitionU3Ek__BackingField_0)); }
	inline ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * get_U3CdefinitionU3Ek__BackingField_0() const { return ___U3CdefinitionU3Ek__BackingField_0; }
	inline ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 ** get_address_of_U3CdefinitionU3Ek__BackingField_0() { return &___U3CdefinitionU3Ek__BackingField_0; }
	inline void set_U3CdefinitionU3Ek__BackingField_0(ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * value)
	{
		___U3CdefinitionU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdefinitionU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CmetadataU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Product_t830A133A97BEA12C3CD696853098A295D99A6DE4, ___U3CmetadataU3Ek__BackingField_1)); }
	inline ProductMetadata_t1CAA912963F8B24B34F53CCF29A6E0BD89B33B48 * get_U3CmetadataU3Ek__BackingField_1() const { return ___U3CmetadataU3Ek__BackingField_1; }
	inline ProductMetadata_t1CAA912963F8B24B34F53CCF29A6E0BD89B33B48 ** get_address_of_U3CmetadataU3Ek__BackingField_1() { return &___U3CmetadataU3Ek__BackingField_1; }
	inline void set_U3CmetadataU3Ek__BackingField_1(ProductMetadata_t1CAA912963F8B24B34F53CCF29A6E0BD89B33B48 * value)
	{
		___U3CmetadataU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmetadataU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CavailableToPurchaseU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Product_t830A133A97BEA12C3CD696853098A295D99A6DE4, ___U3CavailableToPurchaseU3Ek__BackingField_2)); }
	inline bool get_U3CavailableToPurchaseU3Ek__BackingField_2() const { return ___U3CavailableToPurchaseU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CavailableToPurchaseU3Ek__BackingField_2() { return &___U3CavailableToPurchaseU3Ek__BackingField_2; }
	inline void set_U3CavailableToPurchaseU3Ek__BackingField_2(bool value)
	{
		___U3CavailableToPurchaseU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CtransactionIDU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Product_t830A133A97BEA12C3CD696853098A295D99A6DE4, ___U3CtransactionIDU3Ek__BackingField_3)); }
	inline String_t* get_U3CtransactionIDU3Ek__BackingField_3() const { return ___U3CtransactionIDU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CtransactionIDU3Ek__BackingField_3() { return &___U3CtransactionIDU3Ek__BackingField_3; }
	inline void set_U3CtransactionIDU3Ek__BackingField_3(String_t* value)
	{
		___U3CtransactionIDU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtransactionIDU3Ek__BackingField_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CreceiptU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Product_t830A133A97BEA12C3CD696853098A295D99A6DE4, ___U3CreceiptU3Ek__BackingField_4)); }
	inline String_t* get_U3CreceiptU3Ek__BackingField_4() const { return ___U3CreceiptU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CreceiptU3Ek__BackingField_4() { return &___U3CreceiptU3Ek__BackingField_4; }
	inline void set_U3CreceiptU3Ek__BackingField_4(String_t* value)
	{
		___U3CreceiptU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CreceiptU3Ek__BackingField_4), (void*)value);
	}
};


// UnityEngine.Purchasing.ProductCollection
struct  ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Purchasing.Product> UnityEngine.Purchasing.ProductCollection::m_IdToProduct
	Dictionary_2_tCAF0D54E59FAC220E2EF9D3ECAFF045862EC66C0 * ___m_IdToProduct_0;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Purchasing.Product> UnityEngine.Purchasing.ProductCollection::m_StoreSpecificIdToProduct
	Dictionary_2_tCAF0D54E59FAC220E2EF9D3ECAFF045862EC66C0 * ___m_StoreSpecificIdToProduct_1;
	// UnityEngine.Purchasing.Product[] UnityEngine.Purchasing.ProductCollection::m_Products
	ProductU5BU5D_tCC1E9F34B8B3A88563CA989013308FB058864237* ___m_Products_2;
	// System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Product> UnityEngine.Purchasing.ProductCollection::m_ProductSet
	HashSet_1_t361D3549FC7078E1C2F7C4C344909910E39A0D92 * ___m_ProductSet_3;

public:
	inline static int32_t get_offset_of_m_IdToProduct_0() { return static_cast<int32_t>(offsetof(ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07, ___m_IdToProduct_0)); }
	inline Dictionary_2_tCAF0D54E59FAC220E2EF9D3ECAFF045862EC66C0 * get_m_IdToProduct_0() const { return ___m_IdToProduct_0; }
	inline Dictionary_2_tCAF0D54E59FAC220E2EF9D3ECAFF045862EC66C0 ** get_address_of_m_IdToProduct_0() { return &___m_IdToProduct_0; }
	inline void set_m_IdToProduct_0(Dictionary_2_tCAF0D54E59FAC220E2EF9D3ECAFF045862EC66C0 * value)
	{
		___m_IdToProduct_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_IdToProduct_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StoreSpecificIdToProduct_1() { return static_cast<int32_t>(offsetof(ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07, ___m_StoreSpecificIdToProduct_1)); }
	inline Dictionary_2_tCAF0D54E59FAC220E2EF9D3ECAFF045862EC66C0 * get_m_StoreSpecificIdToProduct_1() const { return ___m_StoreSpecificIdToProduct_1; }
	inline Dictionary_2_tCAF0D54E59FAC220E2EF9D3ECAFF045862EC66C0 ** get_address_of_m_StoreSpecificIdToProduct_1() { return &___m_StoreSpecificIdToProduct_1; }
	inline void set_m_StoreSpecificIdToProduct_1(Dictionary_2_tCAF0D54E59FAC220E2EF9D3ECAFF045862EC66C0 * value)
	{
		___m_StoreSpecificIdToProduct_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StoreSpecificIdToProduct_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Products_2() { return static_cast<int32_t>(offsetof(ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07, ___m_Products_2)); }
	inline ProductU5BU5D_tCC1E9F34B8B3A88563CA989013308FB058864237* get_m_Products_2() const { return ___m_Products_2; }
	inline ProductU5BU5D_tCC1E9F34B8B3A88563CA989013308FB058864237** get_address_of_m_Products_2() { return &___m_Products_2; }
	inline void set_m_Products_2(ProductU5BU5D_tCC1E9F34B8B3A88563CA989013308FB058864237* value)
	{
		___m_Products_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Products_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_ProductSet_3() { return static_cast<int32_t>(offsetof(ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07, ___m_ProductSet_3)); }
	inline HashSet_1_t361D3549FC7078E1C2F7C4C344909910E39A0D92 * get_m_ProductSet_3() const { return ___m_ProductSet_3; }
	inline HashSet_1_t361D3549FC7078E1C2F7C4C344909910E39A0D92 ** get_address_of_m_ProductSet_3() { return &___m_ProductSet_3; }
	inline void set_m_ProductSet_3(HashSet_1_t361D3549FC7078E1C2F7C4C344909910E39A0D92 * value)
	{
		___m_ProductSet_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ProductSet_3), (void*)value);
	}
};

struct ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07_StaticFields
{
public:
	// System.Func`2<UnityEngine.Purchasing.Product,System.String> UnityEngine.Purchasing.ProductCollection::<>f__amU24cache0
	Func_2_tE3DDEF72300908E3958302E8350C5EECED6BD11E * ___U3CU3Ef__amU24cache0_4;
	// System.Func`2<UnityEngine.Purchasing.Product,System.String> UnityEngine.Purchasing.ProductCollection::<>f__amU24cache1
	Func_2_tE3DDEF72300908E3958302E8350C5EECED6BD11E * ___U3CU3Ef__amU24cache1_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Func_2_tE3DDEF72300908E3958302E8350C5EECED6BD11E * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Func_2_tE3DDEF72300908E3958302E8350C5EECED6BD11E ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Func_2_tE3DDEF72300908E3958302E8350C5EECED6BD11E * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3Ef__amU24cache0_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline Func_2_tE3DDEF72300908E3958302E8350C5EECED6BD11E * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline Func_2_tE3DDEF72300908E3958302E8350C5EECED6BD11E ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(Func_2_tE3DDEF72300908E3958302E8350C5EECED6BD11E * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3Ef__amU24cache1_5), (void*)value);
	}
};


// UnityEngine.Purchasing.PurchaseEventArgs
struct  PurchaseEventArgs_tF6E04BFD5492F5F57309FFBB2415EB26E5B88C04  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.Product UnityEngine.Purchasing.PurchaseEventArgs::<purchasedProduct>k__BackingField
	Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * ___U3CpurchasedProductU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CpurchasedProductU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PurchaseEventArgs_tF6E04BFD5492F5F57309FFBB2415EB26E5B88C04, ___U3CpurchasedProductU3Ek__BackingField_0)); }
	inline Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * get_U3CpurchasedProductU3Ek__BackingField_0() const { return ___U3CpurchasedProductU3Ek__BackingField_0; }
	inline Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 ** get_address_of_U3CpurchasedProductU3Ek__BackingField_0() { return &___U3CpurchasedProductU3Ek__BackingField_0; }
	inline void set_U3CpurchasedProductU3Ek__BackingField_0(Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * value)
	{
		___U3CpurchasedProductU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpurchasedProductU3Ek__BackingField_0), (void*)value);
	}
};


// GoogleMobileAds.Api.Reward
struct  Reward_t5656463A5A19508F4B9AF6BAB14D343F5A9EC260  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.String GoogleMobileAds.Api.Reward::<Type>k__BackingField
	String_t* ___U3CTypeU3Ek__BackingField_1;
	// System.Double GoogleMobileAds.Api.Reward::<Amount>k__BackingField
	double ___U3CAmountU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Reward_t5656463A5A19508F4B9AF6BAB14D343F5A9EC260, ___U3CTypeU3Ek__BackingField_1)); }
	inline String_t* get_U3CTypeU3Ek__BackingField_1() const { return ___U3CTypeU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CTypeU3Ek__BackingField_1() { return &___U3CTypeU3Ek__BackingField_1; }
	inline void set_U3CTypeU3Ek__BackingField_1(String_t* value)
	{
		___U3CTypeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CTypeU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CAmountU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Reward_t5656463A5A19508F4B9AF6BAB14D343F5A9EC260, ___U3CAmountU3Ek__BackingField_2)); }
	inline double get_U3CAmountU3Ek__BackingField_2() const { return ___U3CAmountU3Ek__BackingField_2; }
	inline double* get_address_of_U3CAmountU3Ek__BackingField_2() { return &___U3CAmountU3Ek__BackingField_2; }
	inline void set_U3CAmountU3Ek__BackingField_2(double value)
	{
		___U3CAmountU3Ek__BackingField_2 = value;
	}
};


// MonoPInvokeCallbackAttribute
struct  MonoPInvokeCallbackAttribute_t0B801029DE3A1B9D601C52FDBED1EC104703FB02  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Collections.Generic.HashSet`1_Enumerator<System.Object>
struct  Enumerator_tC01102D13E6FA5B4F99A7406FE3F96BED7C2250A 
{
public:
	// System.Collections.Generic.HashSet`1<T> System.Collections.Generic.HashSet`1_Enumerator::_set
	HashSet_1_t725419BA457D845928B505ACE877FF46BC71E897 * ____set_0;
	// System.Int32 System.Collections.Generic.HashSet`1_Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.HashSet`1_Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.HashSet`1_Enumerator::_current
	RuntimeObject * ____current_3;

public:
	inline static int32_t get_offset_of__set_0() { return static_cast<int32_t>(offsetof(Enumerator_tC01102D13E6FA5B4F99A7406FE3F96BED7C2250A, ____set_0)); }
	inline HashSet_1_t725419BA457D845928B505ACE877FF46BC71E897 * get__set_0() const { return ____set_0; }
	inline HashSet_1_t725419BA457D845928B505ACE877FF46BC71E897 ** get_address_of__set_0() { return &____set_0; }
	inline void set__set_0(HashSet_1_t725419BA457D845928B505ACE877FF46BC71E897 * value)
	{
		____set_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____set_0), (void*)value);
	}

	inline static int32_t get_offset_of__index_1() { return static_cast<int32_t>(offsetof(Enumerator_tC01102D13E6FA5B4F99A7406FE3F96BED7C2250A, ____index_1)); }
	inline int32_t get__index_1() const { return ____index_1; }
	inline int32_t* get_address_of__index_1() { return &____index_1; }
	inline void set__index_1(int32_t value)
	{
		____index_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Enumerator_tC01102D13E6FA5B4F99A7406FE3F96BED7C2250A, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}

	inline static int32_t get_offset_of__current_3() { return static_cast<int32_t>(offsetof(Enumerator_tC01102D13E6FA5B4F99A7406FE3F96BED7C2250A, ____current_3)); }
	inline RuntimeObject * get__current_3() const { return ____current_3; }
	inline RuntimeObject ** get_address_of__current_3() { return &____current_3; }
	inline void set__current_3(RuntimeObject * value)
	{
		____current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____current_3), (void*)value);
	}
};


// System.Collections.Generic.HashSet`1_Enumerator<System.String>
struct  Enumerator_tF6B5A6D64F2347BF9A12A794D4A8C252CA1431A7 
{
public:
	// System.Collections.Generic.HashSet`1<T> System.Collections.Generic.HashSet`1_Enumerator::_set
	HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61 * ____set_0;
	// System.Int32 System.Collections.Generic.HashSet`1_Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.HashSet`1_Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.HashSet`1_Enumerator::_current
	String_t* ____current_3;

public:
	inline static int32_t get_offset_of__set_0() { return static_cast<int32_t>(offsetof(Enumerator_tF6B5A6D64F2347BF9A12A794D4A8C252CA1431A7, ____set_0)); }
	inline HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61 * get__set_0() const { return ____set_0; }
	inline HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61 ** get_address_of__set_0() { return &____set_0; }
	inline void set__set_0(HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61 * value)
	{
		____set_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____set_0), (void*)value);
	}

	inline static int32_t get_offset_of__index_1() { return static_cast<int32_t>(offsetof(Enumerator_tF6B5A6D64F2347BF9A12A794D4A8C252CA1431A7, ____index_1)); }
	inline int32_t get__index_1() const { return ____index_1; }
	inline int32_t* get_address_of__index_1() { return &____index_1; }
	inline void set__index_1(int32_t value)
	{
		____index_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Enumerator_tF6B5A6D64F2347BF9A12A794D4A8C252CA1431A7, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}

	inline static int32_t get_offset_of__current_3() { return static_cast<int32_t>(offsetof(Enumerator_tF6B5A6D64F2347BF9A12A794D4A8C252CA1431A7, ____current_3)); }
	inline String_t* get__current_3() const { return ____current_3; }
	inline String_t** get_address_of__current_3() { return &____current_3; }
	inline void set__current_3(String_t* value)
	{
		____current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____current_3), (void*)value);
	}
};


// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
struct  KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___key_0), (void*)value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___value_1), (void*)value);
	}
};


// System.Collections.Generic.KeyValuePair`2<System.String,System.String>
struct  KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	String_t* ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___key_0), (void*)value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8, ___value_1)); }
	inline String_t* get_value_1() const { return ___value_1; }
	inline String_t** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(String_t* value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___value_1), (void*)value);
	}
};


// System.Collections.Generic.List`1_Enumerator<GoogleMobileAds.Api.Mediation.MediationExtras>
struct  Enumerator_t1B7A508A639F9A6260571CC57EAB3F3C4B96A49C 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t8F38EE057190BE9897CDE829F7EB4282DF6F3EBA * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	MediationExtras_t53F0F7F90139A2D039272C9764DC880D4B3AB301 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t1B7A508A639F9A6260571CC57EAB3F3C4B96A49C, ___list_0)); }
	inline List_1_t8F38EE057190BE9897CDE829F7EB4282DF6F3EBA * get_list_0() const { return ___list_0; }
	inline List_1_t8F38EE057190BE9897CDE829F7EB4282DF6F3EBA ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t8F38EE057190BE9897CDE829F7EB4282DF6F3EBA * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t1B7A508A639F9A6260571CC57EAB3F3C4B96A49C, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t1B7A508A639F9A6260571CC57EAB3F3C4B96A49C, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1B7A508A639F9A6260571CC57EAB3F3C4B96A49C, ___current_3)); }
	inline MediationExtras_t53F0F7F90139A2D039272C9764DC880D4B3AB301 * get_current_3() const { return ___current_3; }
	inline MediationExtras_t53F0F7F90139A2D039272C9764DC880D4B3AB301 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(MediationExtras_t53F0F7F90139A2D039272C9764DC880D4B3AB301 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1_Enumerator<System.Object>
struct  Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___list_0)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_list_0() const { return ___list_0; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1_Enumerator<System.String>
struct  Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	String_t* ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813, ___list_0)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_list_0() const { return ___list_0; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813, ___current_3)); }
	inline String_t* get_current_3() const { return ___current_3; }
	inline String_t** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(String_t* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DaysToMonth365_29), (void*)value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DaysToMonth366_30), (void*)value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Nullable`1<System.Boolean>
struct  Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// GoogleMobileAds.Api.Gender
struct  Gender_tB13F4EC090657F9C9725E7EAEFCA6056E5C72660 
{
public:
	// System.Int32 GoogleMobileAds.Api.Gender::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Gender_tB13F4EC090657F9C9725E7EAEFCA6056E5C72660, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Collections.Generic.Dictionary`2_Enumerator<System.Object,System.Object>
struct  Enumerator_tED23DFBF3911229086C71CCE7A54D56F5FFB34CB 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2_Enumerator::dictionary
	Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2_Enumerator::version
	int32_t ___version_1;
	// System.Int32 System.Collections.Generic.Dictionary`2_Enumerator::index
	int32_t ___index_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2_Enumerator::current
	KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE  ___current_3;
	// System.Int32 System.Collections.Generic.Dictionary`2_Enumerator::getEnumeratorRetType
	int32_t ___getEnumeratorRetType_4;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_tED23DFBF3911229086C71CCE7A54D56F5FFB34CB, ___dictionary_0)); }
	inline Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dictionary_0), (void*)value);
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(Enumerator_tED23DFBF3911229086C71CCE7A54D56F5FFB34CB, ___version_1)); }
	inline int32_t get_version_1() const { return ___version_1; }
	inline int32_t* get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(int32_t value)
	{
		___version_1 = value;
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(Enumerator_tED23DFBF3911229086C71CCE7A54D56F5FFB34CB, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tED23DFBF3911229086C71CCE7A54D56F5FFB34CB, ___current_3)); }
	inline KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___key_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___value_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_getEnumeratorRetType_4() { return static_cast<int32_t>(offsetof(Enumerator_tED23DFBF3911229086C71CCE7A54D56F5FFB34CB, ___getEnumeratorRetType_4)); }
	inline int32_t get_getEnumeratorRetType_4() const { return ___getEnumeratorRetType_4; }
	inline int32_t* get_address_of_getEnumeratorRetType_4() { return &___getEnumeratorRetType_4; }
	inline void set_getEnumeratorRetType_4(int32_t value)
	{
		___getEnumeratorRetType_4 = value;
	}
};


// System.Collections.Generic.Dictionary`2_Enumerator<System.String,System.String>
struct  Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2_Enumerator::dictionary
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2_Enumerator::version
	int32_t ___version_1;
	// System.Int32 System.Collections.Generic.Dictionary`2_Enumerator::index
	int32_t ___index_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2_Enumerator::current
	KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8  ___current_3;
	// System.Int32 System.Collections.Generic.Dictionary`2_Enumerator::getEnumeratorRetType
	int32_t ___getEnumeratorRetType_4;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3, ___dictionary_0)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dictionary_0), (void*)value);
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3, ___version_1)); }
	inline int32_t get_version_1() const { return ___version_1; }
	inline int32_t* get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(int32_t value)
	{
		___version_1 = value;
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3, ___current_3)); }
	inline KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___key_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___value_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_getEnumeratorRetType_4() { return static_cast<int32_t>(offsetof(Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3, ___getEnumeratorRetType_4)); }
	inline int32_t get_getEnumeratorRetType_4() const { return ___getEnumeratorRetType_4; }
	inline int32_t* get_address_of_getEnumeratorRetType_4() { return &___getEnumeratorRetType_4; }
	inline void set_getEnumeratorRetType_4(int32_t value)
	{
		___getEnumeratorRetType_4 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Int32Enum
struct  Int32Enum_t6312CE4586C17FE2E2E513D2E7655B574F10FDCD 
{
public:
	// System.Int32 System.Int32Enum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Int32Enum_t6312CE4586C17FE2E2E513D2E7655B574F10FDCD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Nullable`1<System.DateTime>
struct  Nullable_1_t9B468FE267D697A84B680BFB00D8C20BAB1A26E7 
{
public:
	// T System.Nullable`1::value
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t9B468FE267D697A84B680BFB00D8C20BAB1A26E7, ___value_0)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_value_0() const { return ___value_0; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t9B468FE267D697A84B680BFB00D8C20BAB1A26E7, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.StringComparison
struct  StringComparison_t02BAA95468CE9E91115C604577611FDF58FEDCF0 
{
public:
	// System.Int32 System.StringComparison::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StringComparison_t02BAA95468CE9E91115C604577611FDF58FEDCF0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.KeyCode
struct  KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.NetworkReachability
struct  NetworkReachability_t087B53A52A7317136038FE00D4AF827A6B8DB56E 
{
public:
	// System.Int32 UnityEngine.NetworkReachability::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NetworkReachability_t087B53A52A7317136038FE00D4AF827A6B8DB56E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.Purchasing.AppStore
struct  AppStore_t8E387D55444E4FD84E578A54EE70EBB5CF9EB334 
{
public:
	// System.Int32 UnityEngine.Purchasing.AppStore::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AppStore_t8E387D55444E4FD84E578A54EE70EBB5CF9EB334, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Purchasing.FakeStoreUIMode
struct  FakeStoreUIMode_t7A838E0321ECEBD14964C91109ABF566CFC646E4 
{
public:
	// System.Int32 UnityEngine.Purchasing.FakeStoreUIMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FakeStoreUIMode_t7A838E0321ECEBD14964C91109ABF566CFC646E4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Purchasing.InitializationFailureReason
struct  InitializationFailureReason_t5A6284D67FA09D301793E67D8B7003299F4D3062 
{
public:
	// System.Int32 UnityEngine.Purchasing.InitializationFailureReason::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InitializationFailureReason_t5A6284D67FA09D301793E67D8B7003299F4D3062, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Purchasing.ProductType
struct  ProductType_tC52C3BA25156195ACF6AE97650D056434BD51075 
{
public:
	// System.Int32 UnityEngine.Purchasing.ProductType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ProductType_tC52C3BA25156195ACF6AE97650D056434BD51075, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Purchasing.PurchaseFailureReason
struct  PurchaseFailureReason_t42EC65C1103B27F82198DC42C8D96C1A14ED7432 
{
public:
	// System.Int32 UnityEngine.Purchasing.PurchaseFailureReason::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PurchaseFailureReason_t42EC65C1103B27F82198DC42C8D96C1A14ED7432, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Purchasing.PurchaseProcessingResult
struct  PurchaseProcessingResult_tD6C3A87FC6F83D8D9133EDA9D1938FC51DFC35E3 
{
public:
	// System.Int32 UnityEngine.Purchasing.PurchaseProcessingResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PurchaseProcessingResult_tD6C3A87FC6F83D8D9133EDA9D1938FC51DFC35E3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.RuntimePlatform
struct  RuntimePlatform_tD5F5737C1BBBCBB115EB104DF2B7876387E80132 
{
public:
	// System.Int32 UnityEngine.RuntimePlatform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RuntimePlatform_tD5F5737C1BBBCBB115EB104DF2B7876387E80132, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.Nullable`1<GoogleMobileAds.Api.Gender>
struct  Nullable_1_t65AAF2600D9569366D224E2EB7B3F65128A96D1B 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t65AAF2600D9569366D224E2EB7B3F65128A96D1B, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t65AAF2600D9569366D224E2EB7B3F65128A96D1B, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Nullable`1<System.Int32Enum>
struct  Nullable_1_tBCA4780CE8E9555A53CF0BA48AF742DA998C0833 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tBCA4780CE8E9555A53CF0BA48AF742DA998C0833, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tBCA4780CE8E9555A53CF0BA48AF742DA998C0833, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Purchasing.ProductDefinition
struct  ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.ProductDefinition::<id>k__BackingField
	String_t* ___U3CidU3Ek__BackingField_0;
	// System.String UnityEngine.Purchasing.ProductDefinition::<storeSpecificId>k__BackingField
	String_t* ___U3CstoreSpecificIdU3Ek__BackingField_1;
	// UnityEngine.Purchasing.ProductType UnityEngine.Purchasing.ProductDefinition::<type>k__BackingField
	int32_t ___U3CtypeU3Ek__BackingField_2;
	// System.Boolean UnityEngine.Purchasing.ProductDefinition::<enabled>k__BackingField
	bool ___U3CenabledU3Ek__BackingField_3;
	// System.Collections.Generic.List`1<UnityEngine.Purchasing.PayoutDefinition> UnityEngine.Purchasing.ProductDefinition::m_Payouts
	List_1_t3BB189095B85DE5B71713AA7779513A84EB7EB9A * ___m_Payouts_4;

public:
	inline static int32_t get_offset_of_U3CidU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10, ___U3CidU3Ek__BackingField_0)); }
	inline String_t* get_U3CidU3Ek__BackingField_0() const { return ___U3CidU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CidU3Ek__BackingField_0() { return &___U3CidU3Ek__BackingField_0; }
	inline void set_U3CidU3Ek__BackingField_0(String_t* value)
	{
		___U3CidU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CidU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CstoreSpecificIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10, ___U3CstoreSpecificIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CstoreSpecificIdU3Ek__BackingField_1() const { return ___U3CstoreSpecificIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CstoreSpecificIdU3Ek__BackingField_1() { return &___U3CstoreSpecificIdU3Ek__BackingField_1; }
	inline void set_U3CstoreSpecificIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CstoreSpecificIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CstoreSpecificIdU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10, ___U3CtypeU3Ek__BackingField_2)); }
	inline int32_t get_U3CtypeU3Ek__BackingField_2() const { return ___U3CtypeU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CtypeU3Ek__BackingField_2() { return &___U3CtypeU3Ek__BackingField_2; }
	inline void set_U3CtypeU3Ek__BackingField_2(int32_t value)
	{
		___U3CtypeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CenabledU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10, ___U3CenabledU3Ek__BackingField_3)); }
	inline bool get_U3CenabledU3Ek__BackingField_3() const { return ___U3CenabledU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CenabledU3Ek__BackingField_3() { return &___U3CenabledU3Ek__BackingField_3; }
	inline void set_U3CenabledU3Ek__BackingField_3(bool value)
	{
		___U3CenabledU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_m_Payouts_4() { return static_cast<int32_t>(offsetof(ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10, ___m_Payouts_4)); }
	inline List_1_t3BB189095B85DE5B71713AA7779513A84EB7EB9A * get_m_Payouts_4() const { return ___m_Payouts_4; }
	inline List_1_t3BB189095B85DE5B71713AA7779513A84EB7EB9A ** get_address_of_m_Payouts_4() { return &___m_Payouts_4; }
	inline void set_m_Payouts_4(List_1_t3BB189095B85DE5B71713AA7779513A84EB7EB9A * value)
	{
		___m_Payouts_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Payouts_4), (void*)value);
	}
};


// UnityEngine.Purchasing.StandardPurchasingModule
struct  StandardPurchasingModule_tAB117B14746822C4AF812B66148122909DD53847  : public AbstractPurchasingModule_t73F8B7B7D6CA305D6CFAFE4692A6F0F7B42531D9
{
public:
	// UnityEngine.Purchasing.AppStore UnityEngine.Purchasing.StandardPurchasingModule::m_AppStorePlatform
	int32_t ___m_AppStorePlatform_1;
	// UnityEngine.Purchasing.INativeStoreProvider UnityEngine.Purchasing.StandardPurchasingModule::m_NativeStoreProvider
	RuntimeObject* ___m_NativeStoreProvider_2;
	// UnityEngine.RuntimePlatform UnityEngine.Purchasing.StandardPurchasingModule::m_RuntimePlatform
	int32_t ___m_RuntimePlatform_3;
	// System.Boolean UnityEngine.Purchasing.StandardPurchasingModule::m_UseCloudCatalog
	bool ___m_UseCloudCatalog_4;
	// Uniject.IUtil UnityEngine.Purchasing.StandardPurchasingModule::<util>k__BackingField
	RuntimeObject* ___U3CutilU3Ek__BackingField_6;
	// UnityEngine.ILogger UnityEngine.Purchasing.StandardPurchasingModule::<logger>k__BackingField
	RuntimeObject* ___U3CloggerU3Ek__BackingField_7;
	// UnityEngine.Purchasing.IAsyncWebUtil UnityEngine.Purchasing.StandardPurchasingModule::<webUtil>k__BackingField
	RuntimeObject* ___U3CwebUtilU3Ek__BackingField_8;
	// UnityEngine.Purchasing.StandardPurchasingModule_StoreInstance UnityEngine.Purchasing.StandardPurchasingModule::<storeInstance>k__BackingField
	StoreInstance_tA65323B0763035D2948B6E42398D527E0365978C * ___U3CstoreInstanceU3Ek__BackingField_9;
	// UnityEngine.Purchasing.CloudCatalogImpl UnityEngine.Purchasing.StandardPurchasingModule::m_CloudCatalog
	CloudCatalogImpl_t4E9FD02BFB4CF8B40D79A7CDE96B860D38CC0FB3 * ___m_CloudCatalog_11;
	// UnityEngine.Purchasing.FakeStoreUIMode UnityEngine.Purchasing.StandardPurchasingModule::<useFakeStoreUIMode>k__BackingField
	int32_t ___U3CuseFakeStoreUIModeU3Ek__BackingField_12;
	// System.Boolean UnityEngine.Purchasing.StandardPurchasingModule::<useFakeStoreAlways>k__BackingField
	bool ___U3CuseFakeStoreAlwaysU3Ek__BackingField_13;
	// UnityEngine.Purchasing.WinRTStore UnityEngine.Purchasing.StandardPurchasingModule::windowsStore
	WinRTStore_t85FB198173C694935BF2F8E2EF2069AFB7E3E68F * ___windowsStore_14;

public:
	inline static int32_t get_offset_of_m_AppStorePlatform_1() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_tAB117B14746822C4AF812B66148122909DD53847, ___m_AppStorePlatform_1)); }
	inline int32_t get_m_AppStorePlatform_1() const { return ___m_AppStorePlatform_1; }
	inline int32_t* get_address_of_m_AppStorePlatform_1() { return &___m_AppStorePlatform_1; }
	inline void set_m_AppStorePlatform_1(int32_t value)
	{
		___m_AppStorePlatform_1 = value;
	}

	inline static int32_t get_offset_of_m_NativeStoreProvider_2() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_tAB117B14746822C4AF812B66148122909DD53847, ___m_NativeStoreProvider_2)); }
	inline RuntimeObject* get_m_NativeStoreProvider_2() const { return ___m_NativeStoreProvider_2; }
	inline RuntimeObject** get_address_of_m_NativeStoreProvider_2() { return &___m_NativeStoreProvider_2; }
	inline void set_m_NativeStoreProvider_2(RuntimeObject* value)
	{
		___m_NativeStoreProvider_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_NativeStoreProvider_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_RuntimePlatform_3() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_tAB117B14746822C4AF812B66148122909DD53847, ___m_RuntimePlatform_3)); }
	inline int32_t get_m_RuntimePlatform_3() const { return ___m_RuntimePlatform_3; }
	inline int32_t* get_address_of_m_RuntimePlatform_3() { return &___m_RuntimePlatform_3; }
	inline void set_m_RuntimePlatform_3(int32_t value)
	{
		___m_RuntimePlatform_3 = value;
	}

	inline static int32_t get_offset_of_m_UseCloudCatalog_4() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_tAB117B14746822C4AF812B66148122909DD53847, ___m_UseCloudCatalog_4)); }
	inline bool get_m_UseCloudCatalog_4() const { return ___m_UseCloudCatalog_4; }
	inline bool* get_address_of_m_UseCloudCatalog_4() { return &___m_UseCloudCatalog_4; }
	inline void set_m_UseCloudCatalog_4(bool value)
	{
		___m_UseCloudCatalog_4 = value;
	}

	inline static int32_t get_offset_of_U3CutilU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_tAB117B14746822C4AF812B66148122909DD53847, ___U3CutilU3Ek__BackingField_6)); }
	inline RuntimeObject* get_U3CutilU3Ek__BackingField_6() const { return ___U3CutilU3Ek__BackingField_6; }
	inline RuntimeObject** get_address_of_U3CutilU3Ek__BackingField_6() { return &___U3CutilU3Ek__BackingField_6; }
	inline void set_U3CutilU3Ek__BackingField_6(RuntimeObject* value)
	{
		___U3CutilU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CutilU3Ek__BackingField_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CloggerU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_tAB117B14746822C4AF812B66148122909DD53847, ___U3CloggerU3Ek__BackingField_7)); }
	inline RuntimeObject* get_U3CloggerU3Ek__BackingField_7() const { return ___U3CloggerU3Ek__BackingField_7; }
	inline RuntimeObject** get_address_of_U3CloggerU3Ek__BackingField_7() { return &___U3CloggerU3Ek__BackingField_7; }
	inline void set_U3CloggerU3Ek__BackingField_7(RuntimeObject* value)
	{
		___U3CloggerU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CloggerU3Ek__BackingField_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CwebUtilU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_tAB117B14746822C4AF812B66148122909DD53847, ___U3CwebUtilU3Ek__BackingField_8)); }
	inline RuntimeObject* get_U3CwebUtilU3Ek__BackingField_8() const { return ___U3CwebUtilU3Ek__BackingField_8; }
	inline RuntimeObject** get_address_of_U3CwebUtilU3Ek__BackingField_8() { return &___U3CwebUtilU3Ek__BackingField_8; }
	inline void set_U3CwebUtilU3Ek__BackingField_8(RuntimeObject* value)
	{
		___U3CwebUtilU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CwebUtilU3Ek__BackingField_8), (void*)value);
	}

	inline static int32_t get_offset_of_U3CstoreInstanceU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_tAB117B14746822C4AF812B66148122909DD53847, ___U3CstoreInstanceU3Ek__BackingField_9)); }
	inline StoreInstance_tA65323B0763035D2948B6E42398D527E0365978C * get_U3CstoreInstanceU3Ek__BackingField_9() const { return ___U3CstoreInstanceU3Ek__BackingField_9; }
	inline StoreInstance_tA65323B0763035D2948B6E42398D527E0365978C ** get_address_of_U3CstoreInstanceU3Ek__BackingField_9() { return &___U3CstoreInstanceU3Ek__BackingField_9; }
	inline void set_U3CstoreInstanceU3Ek__BackingField_9(StoreInstance_tA65323B0763035D2948B6E42398D527E0365978C * value)
	{
		___U3CstoreInstanceU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CstoreInstanceU3Ek__BackingField_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_CloudCatalog_11() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_tAB117B14746822C4AF812B66148122909DD53847, ___m_CloudCatalog_11)); }
	inline CloudCatalogImpl_t4E9FD02BFB4CF8B40D79A7CDE96B860D38CC0FB3 * get_m_CloudCatalog_11() const { return ___m_CloudCatalog_11; }
	inline CloudCatalogImpl_t4E9FD02BFB4CF8B40D79A7CDE96B860D38CC0FB3 ** get_address_of_m_CloudCatalog_11() { return &___m_CloudCatalog_11; }
	inline void set_m_CloudCatalog_11(CloudCatalogImpl_t4E9FD02BFB4CF8B40D79A7CDE96B860D38CC0FB3 * value)
	{
		___m_CloudCatalog_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CloudCatalog_11), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseFakeStoreUIModeU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_tAB117B14746822C4AF812B66148122909DD53847, ___U3CuseFakeStoreUIModeU3Ek__BackingField_12)); }
	inline int32_t get_U3CuseFakeStoreUIModeU3Ek__BackingField_12() const { return ___U3CuseFakeStoreUIModeU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CuseFakeStoreUIModeU3Ek__BackingField_12() { return &___U3CuseFakeStoreUIModeU3Ek__BackingField_12; }
	inline void set_U3CuseFakeStoreUIModeU3Ek__BackingField_12(int32_t value)
	{
		___U3CuseFakeStoreUIModeU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CuseFakeStoreAlwaysU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_tAB117B14746822C4AF812B66148122909DD53847, ___U3CuseFakeStoreAlwaysU3Ek__BackingField_13)); }
	inline bool get_U3CuseFakeStoreAlwaysU3Ek__BackingField_13() const { return ___U3CuseFakeStoreAlwaysU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CuseFakeStoreAlwaysU3Ek__BackingField_13() { return &___U3CuseFakeStoreAlwaysU3Ek__BackingField_13; }
	inline void set_U3CuseFakeStoreAlwaysU3Ek__BackingField_13(bool value)
	{
		___U3CuseFakeStoreAlwaysU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_windowsStore_14() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_tAB117B14746822C4AF812B66148122909DD53847, ___windowsStore_14)); }
	inline WinRTStore_t85FB198173C694935BF2F8E2EF2069AFB7E3E68F * get_windowsStore_14() const { return ___windowsStore_14; }
	inline WinRTStore_t85FB198173C694935BF2F8E2EF2069AFB7E3E68F ** get_address_of_windowsStore_14() { return &___windowsStore_14; }
	inline void set_windowsStore_14(WinRTStore_t85FB198173C694935BF2F8E2EF2069AFB7E3E68F * value)
	{
		___windowsStore_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___windowsStore_14), (void*)value);
	}
};

struct StandardPurchasingModule_tAB117B14746822C4AF812B66148122909DD53847_StaticFields
{
public:
	// UnityEngine.Purchasing.StandardPurchasingModule UnityEngine.Purchasing.StandardPurchasingModule::ModuleInstance
	StandardPurchasingModule_tAB117B14746822C4AF812B66148122909DD53847 * ___ModuleInstance_5;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.String> UnityEngine.Purchasing.StandardPurchasingModule::AndroidStoreNameMap
	Dictionary_2_t49835E8A3E77B2EEF19B998068B89F2CFEBD24E3 * ___AndroidStoreNameMap_10;

public:
	inline static int32_t get_offset_of_ModuleInstance_5() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_tAB117B14746822C4AF812B66148122909DD53847_StaticFields, ___ModuleInstance_5)); }
	inline StandardPurchasingModule_tAB117B14746822C4AF812B66148122909DD53847 * get_ModuleInstance_5() const { return ___ModuleInstance_5; }
	inline StandardPurchasingModule_tAB117B14746822C4AF812B66148122909DD53847 ** get_address_of_ModuleInstance_5() { return &___ModuleInstance_5; }
	inline void set_ModuleInstance_5(StandardPurchasingModule_tAB117B14746822C4AF812B66148122909DD53847 * value)
	{
		___ModuleInstance_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ModuleInstance_5), (void*)value);
	}

	inline static int32_t get_offset_of_AndroidStoreNameMap_10() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_tAB117B14746822C4AF812B66148122909DD53847_StaticFields, ___AndroidStoreNameMap_10)); }
	inline Dictionary_2_t49835E8A3E77B2EEF19B998068B89F2CFEBD24E3 * get_AndroidStoreNameMap_10() const { return ___AndroidStoreNameMap_10; }
	inline Dictionary_2_t49835E8A3E77B2EEF19B998068B89F2CFEBD24E3 ** get_address_of_AndroidStoreNameMap_10() { return &___AndroidStoreNameMap_10; }
	inline void set_AndroidStoreNameMap_10(Dictionary_2_t49835E8A3E77B2EEF19B998068B89F2CFEBD24E3 * value)
	{
		___AndroidStoreNameMap_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AndroidStoreNameMap_10), (void*)value);
	}
};


// GoogleMobileAds.Api.AdRequest
struct  AdRequest_t98763832B122F67AC2952360B6381F4F5848306F  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> GoogleMobileAds.Api.AdRequest::<TestDevices>k__BackingField
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___U3CTestDevicesU3Ek__BackingField_2;
	// System.Collections.Generic.HashSet`1<System.String> GoogleMobileAds.Api.AdRequest::<Keywords>k__BackingField
	HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61 * ___U3CKeywordsU3Ek__BackingField_3;
	// System.Nullable`1<System.DateTime> GoogleMobileAds.Api.AdRequest::<Birthday>k__BackingField
	Nullable_1_t9B468FE267D697A84B680BFB00D8C20BAB1A26E7  ___U3CBirthdayU3Ek__BackingField_4;
	// System.Nullable`1<GoogleMobileAds.Api.Gender> GoogleMobileAds.Api.AdRequest::<Gender>k__BackingField
	Nullable_1_t65AAF2600D9569366D224E2EB7B3F65128A96D1B  ___U3CGenderU3Ek__BackingField_5;
	// System.Nullable`1<System.Boolean> GoogleMobileAds.Api.AdRequest::<TagForChildDirectedTreatment>k__BackingField
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___U3CTagForChildDirectedTreatmentU3Ek__BackingField_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> GoogleMobileAds.Api.AdRequest::<Extras>k__BackingField
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___U3CExtrasU3Ek__BackingField_7;
	// System.Collections.Generic.List`1<GoogleMobileAds.Api.Mediation.MediationExtras> GoogleMobileAds.Api.AdRequest::<MediationExtras>k__BackingField
	List_1_t8F38EE057190BE9897CDE829F7EB4282DF6F3EBA * ___U3CMediationExtrasU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CTestDevicesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdRequest_t98763832B122F67AC2952360B6381F4F5848306F, ___U3CTestDevicesU3Ek__BackingField_2)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_U3CTestDevicesU3Ek__BackingField_2() const { return ___U3CTestDevicesU3Ek__BackingField_2; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_U3CTestDevicesU3Ek__BackingField_2() { return &___U3CTestDevicesU3Ek__BackingField_2; }
	inline void set_U3CTestDevicesU3Ek__BackingField_2(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___U3CTestDevicesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CTestDevicesU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CKeywordsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdRequest_t98763832B122F67AC2952360B6381F4F5848306F, ___U3CKeywordsU3Ek__BackingField_3)); }
	inline HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61 * get_U3CKeywordsU3Ek__BackingField_3() const { return ___U3CKeywordsU3Ek__BackingField_3; }
	inline HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61 ** get_address_of_U3CKeywordsU3Ek__BackingField_3() { return &___U3CKeywordsU3Ek__BackingField_3; }
	inline void set_U3CKeywordsU3Ek__BackingField_3(HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61 * value)
	{
		___U3CKeywordsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CKeywordsU3Ek__BackingField_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CBirthdayU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AdRequest_t98763832B122F67AC2952360B6381F4F5848306F, ___U3CBirthdayU3Ek__BackingField_4)); }
	inline Nullable_1_t9B468FE267D697A84B680BFB00D8C20BAB1A26E7  get_U3CBirthdayU3Ek__BackingField_4() const { return ___U3CBirthdayU3Ek__BackingField_4; }
	inline Nullable_1_t9B468FE267D697A84B680BFB00D8C20BAB1A26E7 * get_address_of_U3CBirthdayU3Ek__BackingField_4() { return &___U3CBirthdayU3Ek__BackingField_4; }
	inline void set_U3CBirthdayU3Ek__BackingField_4(Nullable_1_t9B468FE267D697A84B680BFB00D8C20BAB1A26E7  value)
	{
		___U3CBirthdayU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CGenderU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AdRequest_t98763832B122F67AC2952360B6381F4F5848306F, ___U3CGenderU3Ek__BackingField_5)); }
	inline Nullable_1_t65AAF2600D9569366D224E2EB7B3F65128A96D1B  get_U3CGenderU3Ek__BackingField_5() const { return ___U3CGenderU3Ek__BackingField_5; }
	inline Nullable_1_t65AAF2600D9569366D224E2EB7B3F65128A96D1B * get_address_of_U3CGenderU3Ek__BackingField_5() { return &___U3CGenderU3Ek__BackingField_5; }
	inline void set_U3CGenderU3Ek__BackingField_5(Nullable_1_t65AAF2600D9569366D224E2EB7B3F65128A96D1B  value)
	{
		___U3CGenderU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CTagForChildDirectedTreatmentU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AdRequest_t98763832B122F67AC2952360B6381F4F5848306F, ___U3CTagForChildDirectedTreatmentU3Ek__BackingField_6)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_U3CTagForChildDirectedTreatmentU3Ek__BackingField_6() const { return ___U3CTagForChildDirectedTreatmentU3Ek__BackingField_6; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_U3CTagForChildDirectedTreatmentU3Ek__BackingField_6() { return &___U3CTagForChildDirectedTreatmentU3Ek__BackingField_6; }
	inline void set_U3CTagForChildDirectedTreatmentU3Ek__BackingField_6(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___U3CTagForChildDirectedTreatmentU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CExtrasU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AdRequest_t98763832B122F67AC2952360B6381F4F5848306F, ___U3CExtrasU3Ek__BackingField_7)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_U3CExtrasU3Ek__BackingField_7() const { return ___U3CExtrasU3Ek__BackingField_7; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_U3CExtrasU3Ek__BackingField_7() { return &___U3CExtrasU3Ek__BackingField_7; }
	inline void set_U3CExtrasU3Ek__BackingField_7(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___U3CExtrasU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CExtrasU3Ek__BackingField_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CMediationExtrasU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AdRequest_t98763832B122F67AC2952360B6381F4F5848306F, ___U3CMediationExtrasU3Ek__BackingField_8)); }
	inline List_1_t8F38EE057190BE9897CDE829F7EB4282DF6F3EBA * get_U3CMediationExtrasU3Ek__BackingField_8() const { return ___U3CMediationExtrasU3Ek__BackingField_8; }
	inline List_1_t8F38EE057190BE9897CDE829F7EB4282DF6F3EBA ** get_address_of_U3CMediationExtrasU3Ek__BackingField_8() { return &___U3CMediationExtrasU3Ek__BackingField_8; }
	inline void set_U3CMediationExtrasU3Ek__BackingField_8(List_1_t8F38EE057190BE9897CDE829F7EB4282DF6F3EBA * value)
	{
		___U3CMediationExtrasU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CMediationExtrasU3Ek__BackingField_8), (void*)value);
	}
};


// GoogleMobileAds.Api.AdRequest_Builder
struct  Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> GoogleMobileAds.Api.AdRequest_Builder::<TestDevices>k__BackingField
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___U3CTestDevicesU3Ek__BackingField_0;
	// System.Collections.Generic.HashSet`1<System.String> GoogleMobileAds.Api.AdRequest_Builder::<Keywords>k__BackingField
	HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61 * ___U3CKeywordsU3Ek__BackingField_1;
	// System.Nullable`1<System.DateTime> GoogleMobileAds.Api.AdRequest_Builder::<Birthday>k__BackingField
	Nullable_1_t9B468FE267D697A84B680BFB00D8C20BAB1A26E7  ___U3CBirthdayU3Ek__BackingField_2;
	// System.Nullable`1<GoogleMobileAds.Api.Gender> GoogleMobileAds.Api.AdRequest_Builder::<Gender>k__BackingField
	Nullable_1_t65AAF2600D9569366D224E2EB7B3F65128A96D1B  ___U3CGenderU3Ek__BackingField_3;
	// System.Nullable`1<System.Boolean> GoogleMobileAds.Api.AdRequest_Builder::<ChildDirectedTreatmentTag>k__BackingField
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___U3CChildDirectedTreatmentTagU3Ek__BackingField_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> GoogleMobileAds.Api.AdRequest_Builder::<Extras>k__BackingField
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___U3CExtrasU3Ek__BackingField_5;
	// System.Collections.Generic.List`1<GoogleMobileAds.Api.Mediation.MediationExtras> GoogleMobileAds.Api.AdRequest_Builder::<MediationExtras>k__BackingField
	List_1_t8F38EE057190BE9897CDE829F7EB4282DF6F3EBA * ___U3CMediationExtrasU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CTestDevicesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9, ___U3CTestDevicesU3Ek__BackingField_0)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_U3CTestDevicesU3Ek__BackingField_0() const { return ___U3CTestDevicesU3Ek__BackingField_0; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_U3CTestDevicesU3Ek__BackingField_0() { return &___U3CTestDevicesU3Ek__BackingField_0; }
	inline void set_U3CTestDevicesU3Ek__BackingField_0(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___U3CTestDevicesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CTestDevicesU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CKeywordsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9, ___U3CKeywordsU3Ek__BackingField_1)); }
	inline HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61 * get_U3CKeywordsU3Ek__BackingField_1() const { return ___U3CKeywordsU3Ek__BackingField_1; }
	inline HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61 ** get_address_of_U3CKeywordsU3Ek__BackingField_1() { return &___U3CKeywordsU3Ek__BackingField_1; }
	inline void set_U3CKeywordsU3Ek__BackingField_1(HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61 * value)
	{
		___U3CKeywordsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CKeywordsU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CBirthdayU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9, ___U3CBirthdayU3Ek__BackingField_2)); }
	inline Nullable_1_t9B468FE267D697A84B680BFB00D8C20BAB1A26E7  get_U3CBirthdayU3Ek__BackingField_2() const { return ___U3CBirthdayU3Ek__BackingField_2; }
	inline Nullable_1_t9B468FE267D697A84B680BFB00D8C20BAB1A26E7 * get_address_of_U3CBirthdayU3Ek__BackingField_2() { return &___U3CBirthdayU3Ek__BackingField_2; }
	inline void set_U3CBirthdayU3Ek__BackingField_2(Nullable_1_t9B468FE267D697A84B680BFB00D8C20BAB1A26E7  value)
	{
		___U3CBirthdayU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CGenderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9, ___U3CGenderU3Ek__BackingField_3)); }
	inline Nullable_1_t65AAF2600D9569366D224E2EB7B3F65128A96D1B  get_U3CGenderU3Ek__BackingField_3() const { return ___U3CGenderU3Ek__BackingField_3; }
	inline Nullable_1_t65AAF2600D9569366D224E2EB7B3F65128A96D1B * get_address_of_U3CGenderU3Ek__BackingField_3() { return &___U3CGenderU3Ek__BackingField_3; }
	inline void set_U3CGenderU3Ek__BackingField_3(Nullable_1_t65AAF2600D9569366D224E2EB7B3F65128A96D1B  value)
	{
		___U3CGenderU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CChildDirectedTreatmentTagU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9, ___U3CChildDirectedTreatmentTagU3Ek__BackingField_4)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_U3CChildDirectedTreatmentTagU3Ek__BackingField_4() const { return ___U3CChildDirectedTreatmentTagU3Ek__BackingField_4; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_U3CChildDirectedTreatmentTagU3Ek__BackingField_4() { return &___U3CChildDirectedTreatmentTagU3Ek__BackingField_4; }
	inline void set_U3CChildDirectedTreatmentTagU3Ek__BackingField_4(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___U3CChildDirectedTreatmentTagU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CExtrasU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9, ___U3CExtrasU3Ek__BackingField_5)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_U3CExtrasU3Ek__BackingField_5() const { return ___U3CExtrasU3Ek__BackingField_5; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_U3CExtrasU3Ek__BackingField_5() { return &___U3CExtrasU3Ek__BackingField_5; }
	inline void set_U3CExtrasU3Ek__BackingField_5(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___U3CExtrasU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CExtrasU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CMediationExtrasU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9, ___U3CMediationExtrasU3Ek__BackingField_6)); }
	inline List_1_t8F38EE057190BE9897CDE829F7EB4282DF6F3EBA * get_U3CMediationExtrasU3Ek__BackingField_6() const { return ___U3CMediationExtrasU3Ek__BackingField_6; }
	inline List_1_t8F38EE057190BE9897CDE829F7EB4282DF6F3EBA ** get_address_of_U3CMediationExtrasU3Ek__BackingField_6() { return &___U3CMediationExtrasU3Ek__BackingField_6; }
	inline void set_U3CMediationExtrasU3Ek__BackingField_6(List_1_t8F38EE057190BE9897CDE829F7EB4282DF6F3EBA * value)
	{
		___U3CMediationExtrasU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CMediationExtrasU3Ek__BackingField_6), (void*)value);
	}
};


// GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdWillLeaveApplicationCallback
struct  GADURewardBasedVideoAdWillLeaveApplicationCallback_t0056DE714C24D44E29732E1C20F113CB30974404  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`1<System.Boolean>
struct  Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD  : public MulticastDelegate_t
{
public:

public:
};


// System.AsyncCallback
struct  AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4  : public MulticastDelegate_t
{
public:

public:
};


// System.EventHandler`1<GoogleMobileAds.Api.Reward>
struct  EventHandler_1_tE84E7D22A41CECD013ED02984B817FEBA6923CC8  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// Menu
struct  Menu_t7D066A010AF240D53C2DDA2B05DF31FD0B5F3129  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject[] Menu::screens
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___screens_4;
	// GoogleMobileAds.Api.RewardBasedVideoAd Menu::rewardedAd
	RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491 * ___rewardedAd_5;
	// System.Boolean Menu::setReward
	bool ___setReward_6;

public:
	inline static int32_t get_offset_of_screens_4() { return static_cast<int32_t>(offsetof(Menu_t7D066A010AF240D53C2DDA2B05DF31FD0B5F3129, ___screens_4)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_screens_4() const { return ___screens_4; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_screens_4() { return &___screens_4; }
	inline void set_screens_4(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___screens_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___screens_4), (void*)value);
	}

	inline static int32_t get_offset_of_rewardedAd_5() { return static_cast<int32_t>(offsetof(Menu_t7D066A010AF240D53C2DDA2B05DF31FD0B5F3129, ___rewardedAd_5)); }
	inline RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491 * get_rewardedAd_5() const { return ___rewardedAd_5; }
	inline RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491 ** get_address_of_rewardedAd_5() { return &___rewardedAd_5; }
	inline void set_rewardedAd_5(RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491 * value)
	{
		___rewardedAd_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rewardedAd_5), (void*)value);
	}

	inline static int32_t get_offset_of_setReward_6() { return static_cast<int32_t>(offsetof(Menu_t7D066A010AF240D53C2DDA2B05DF31FD0B5F3129, ___setReward_6)); }
	inline bool get_setReward_6() const { return ___setReward_6; }
	inline bool* get_address_of_setReward_6() { return &___setReward_6; }
	inline void set_setReward_6(bool value)
	{
		___setReward_6 = value;
	}
};


// Purchaser
struct  Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_StaticFields
{
public:
	// UnityEngine.Purchasing.IStoreController Purchaser::m_StoreController
	RuntimeObject* ___m_StoreController_4;
	// UnityEngine.Purchasing.IExtensionProvider Purchaser::m_StoreExtensionProvider
	RuntimeObject* ___m_StoreExtensionProvider_5;
	// System.String Purchaser::kProductIDConsumable1
	String_t* ___kProductIDConsumable1_6;
	// System.String Purchaser::kProductIDConsumable2
	String_t* ___kProductIDConsumable2_7;
	// System.String Purchaser::kProductIDConsumable3
	String_t* ___kProductIDConsumable3_8;
	// System.String Purchaser::kProductIDConsumable4
	String_t* ___kProductIDConsumable4_9;

public:
	inline static int32_t get_offset_of_m_StoreController_4() { return static_cast<int32_t>(offsetof(Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_StaticFields, ___m_StoreController_4)); }
	inline RuntimeObject* get_m_StoreController_4() const { return ___m_StoreController_4; }
	inline RuntimeObject** get_address_of_m_StoreController_4() { return &___m_StoreController_4; }
	inline void set_m_StoreController_4(RuntimeObject* value)
	{
		___m_StoreController_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StoreController_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_StoreExtensionProvider_5() { return static_cast<int32_t>(offsetof(Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_StaticFields, ___m_StoreExtensionProvider_5)); }
	inline RuntimeObject* get_m_StoreExtensionProvider_5() const { return ___m_StoreExtensionProvider_5; }
	inline RuntimeObject** get_address_of_m_StoreExtensionProvider_5() { return &___m_StoreExtensionProvider_5; }
	inline void set_m_StoreExtensionProvider_5(RuntimeObject* value)
	{
		___m_StoreExtensionProvider_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StoreExtensionProvider_5), (void*)value);
	}

	inline static int32_t get_offset_of_kProductIDConsumable1_6() { return static_cast<int32_t>(offsetof(Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_StaticFields, ___kProductIDConsumable1_6)); }
	inline String_t* get_kProductIDConsumable1_6() const { return ___kProductIDConsumable1_6; }
	inline String_t** get_address_of_kProductIDConsumable1_6() { return &___kProductIDConsumable1_6; }
	inline void set_kProductIDConsumable1_6(String_t* value)
	{
		___kProductIDConsumable1_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___kProductIDConsumable1_6), (void*)value);
	}

	inline static int32_t get_offset_of_kProductIDConsumable2_7() { return static_cast<int32_t>(offsetof(Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_StaticFields, ___kProductIDConsumable2_7)); }
	inline String_t* get_kProductIDConsumable2_7() const { return ___kProductIDConsumable2_7; }
	inline String_t** get_address_of_kProductIDConsumable2_7() { return &___kProductIDConsumable2_7; }
	inline void set_kProductIDConsumable2_7(String_t* value)
	{
		___kProductIDConsumable2_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___kProductIDConsumable2_7), (void*)value);
	}

	inline static int32_t get_offset_of_kProductIDConsumable3_8() { return static_cast<int32_t>(offsetof(Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_StaticFields, ___kProductIDConsumable3_8)); }
	inline String_t* get_kProductIDConsumable3_8() const { return ___kProductIDConsumable3_8; }
	inline String_t** get_address_of_kProductIDConsumable3_8() { return &___kProductIDConsumable3_8; }
	inline void set_kProductIDConsumable3_8(String_t* value)
	{
		___kProductIDConsumable3_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___kProductIDConsumable3_8), (void*)value);
	}

	inline static int32_t get_offset_of_kProductIDConsumable4_9() { return static_cast<int32_t>(offsetof(Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_StaticFields, ___kProductIDConsumable4_9)); }
	inline String_t* get_kProductIDConsumable4_9() const { return ___kProductIDConsumable4_9; }
	inline String_t** get_address_of_kProductIDConsumable4_9() { return &___kProductIDConsumable4_9; }
	inline void set_kProductIDConsumable4_9(String_t* value)
	{
		___kProductIDConsumable4_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___kProductIDConsumable4_9), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * m_Items[1];

public:
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Purchasing.Extension.IPurchasingModule[]
struct IPurchasingModuleU5BU5D_tF106F307EE194D8B47140E160F5BC38EDFFF9384  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Collections.Generic.HashSet`1/Enumerator<!0> System.Collections.Generic.HashSet`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tC01102D13E6FA5B4F99A7406FE3F96BED7C2250A  HashSet_1_GetEnumerator_m263AF8F8486FF5B6747A4B4C20B314EC6BB388BF_gshared (HashSet_1_t725419BA457D845928B505ACE877FF46BC71E897 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.HashSet`1/Enumerator<System.Object>::get_Current()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m4C430D4730AABE78C2EDBC5324F1E82FEC21CDED_gshared_inline (Enumerator_tC01102D13E6FA5B4F99A7406FE3F96BED7C2250A * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mA66925E71356820C9239CA8E620442745C88E07F_gshared (Enumerator_tC01102D13E6FA5B4F99A7406FE3F96BED7C2250A * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mD1758E7826FDA8D185FC2C218F9D32B9ADA4FE0D_gshared (Enumerator_tC01102D13E6FA5B4F99A7406FE3F96BED7C2250A * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD  List_1_GetEnumerator_m52CC760E475D226A2B75048D70C4E22692F9F68D_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared_inline (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m38B1099DDAD7EEDE2F4CDAB11C095AC784AC2E34_gshared (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_m94D0DAE031619503CDA6E53C5C3CC78AF3139472_gshared (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);
// System.Boolean System.Nullable`1<System.DateTime>::get_HasValue()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_mB231DEE33107C7E966680F0404FFDD7939B24625_gshared_inline (Nullable_1_t9B468FE267D697A84B680BFB00D8C20BAB1A26E7 * __this, const RuntimeMethod* method);
// !0 System.Nullable`1<System.DateTime>::GetValueOrDefault()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  Nullable_1_GetValueOrDefault_mA2BC7FE28E1E54F6DED8F2DA5ACA72A56083E43D_gshared_inline (Nullable_1_t9B468FE267D697A84B680BFB00D8C20BAB1A26E7 * __this, const RuntimeMethod* method);
// System.Boolean System.Nullable`1<System.Int32Enum>::get_HasValue()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_m58742F2DBAEDAA6626C115F9E99209C8F4D8F195_gshared_inline (Nullable_1_tBCA4780CE8E9555A53CF0BA48AF742DA998C0833 * __this, const RuntimeMethod* method);
// !0 System.Nullable`1<System.Int32Enum>::GetValueOrDefault()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t Nullable_1_GetValueOrDefault_m19DE1B4F1BFB431B2FE3475CECA352580499168D_gshared_inline (Nullable_1_tBCA4780CE8E9555A53CF0BA48AF742DA998C0833 * __this, const RuntimeMethod* method);
// System.Boolean System.Nullable`1<System.Boolean>::get_HasValue()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_m275A31438FCDAEEE039E95D887684E04FD6ECE2B_gshared_inline (Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * __this, const RuntimeMethod* method);
// !0 System.Nullable`1<System.Boolean>::GetValueOrDefault()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR bool Nullable_1_GetValueOrDefault_mD65884636E94AF88C2745255BBE130A1B64BA294_gshared_inline (Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * __this, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tED23DFBF3911229086C71CCE7A54D56F5FFB34CB  Dictionary_2_GetEnumerator_mF1CF1D13F3E70C6D20D96D9AC88E44454E4C0053_gshared (Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA * __this, const RuntimeMethod* method);
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_Current()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE  Enumerator_get_Current_m5B32A9FC8294CB723DCD1171744B32E1775B6318_gshared_inline (Enumerator_tED23DFBF3911229086C71CCE7A54D56F5FFB34CB * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * KeyValuePair_2_get_Key_m9D4E9BCBAB1BE560871A0889C851FC22A09975F4_gshared_inline (KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE * __this, const RuntimeMethod* method);
// !1 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * KeyValuePair_2_get_Value_m8C7B882C4D425535288FAAD08EAF11D289A43AEC_gshared_inline (KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m9B9FB07EC2C1D82E921C9316A4E0901C933BBF6C_gshared (Enumerator_tED23DFBF3911229086C71CCE7A54D56F5FFB34CB * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mE363888280B72ED50538416C060EF9FC94B3BB00_gshared (Enumerator_tED23DFBF3911229086C71CCE7A54D56F5FFB34CB * __this, const RuntimeMethod* method);
// System.Void System.EventHandler`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventHandler_1__ctor_m3D6B301FCEB48533990CFB2386DAAB091D357BAC_gshared (EventHandler_1_t10245A26B14DDE8DDFD5B263BDE0641F17DCFDC3 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// !!0[] System.Array::Empty<System.Object>()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_gshared_inline (const RuntimeMethod* method);
// System.Void System.Action`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_mFEB000BCBDAB6DE953B2B50CD113641DCF601890_gshared (Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);

// System.IntPtr GoogleMobileAds.iOS.Externs::GADUCreateRequest()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t Externs_GADUCreateRequest_m73B645271C266986E9711F38EF4E0CA6FC511D9C (const RuntimeMethod* method);
// System.Collections.Generic.HashSet`1<System.String> GoogleMobileAds.Api.AdRequest::get_Keywords()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61 * AdRequest_get_Keywords_mA1D66061D1F93CF256DDD5AAE938C629104F11AA_inline (AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * __this, const RuntimeMethod* method);
// System.Collections.Generic.HashSet`1/Enumerator<!0> System.Collections.Generic.HashSet`1<System.String>::GetEnumerator()
inline Enumerator_tF6B5A6D64F2347BF9A12A794D4A8C252CA1431A7  HashSet_1_GetEnumerator_m2EC6A826B51D71BE1AB94EEFB697BC6412B1CDF8 (HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tF6B5A6D64F2347BF9A12A794D4A8C252CA1431A7  (*) (HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61 *, const RuntimeMethod*))HashSet_1_GetEnumerator_m263AF8F8486FF5B6747A4B4C20B314EC6BB388BF_gshared)(__this, method);
}
// !0 System.Collections.Generic.HashSet`1/Enumerator<System.String>::get_Current()
inline String_t* Enumerator_get_Current_mF1A272E02A81B502A1CDF431D0AC41BE3D53E764_inline (Enumerator_tF6B5A6D64F2347BF9A12A794D4A8C252CA1431A7 * __this, const RuntimeMethod* method)
{
	return ((  String_t* (*) (Enumerator_tF6B5A6D64F2347BF9A12A794D4A8C252CA1431A7 *, const RuntimeMethod*))Enumerator_get_Current_m4C430D4730AABE78C2EDBC5324F1E82FEC21CDED_gshared_inline)(__this, method);
}
// System.Void GoogleMobileAds.iOS.Externs::GADUAddKeyword(System.IntPtr,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Externs_GADUAddKeyword_mCF88D72AA181DCB3C3DA3FB9BD096E64818FB492 (intptr_t ___request0, String_t* ___keyword1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<System.String>::MoveNext()
inline bool Enumerator_MoveNext_mCCD47EE234D4C2A16ED96329CC6BC10D2C101702 (Enumerator_tF6B5A6D64F2347BF9A12A794D4A8C252CA1431A7 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tF6B5A6D64F2347BF9A12A794D4A8C252CA1431A7 *, const RuntimeMethod*))Enumerator_MoveNext_mA66925E71356820C9239CA8E620442745C88E07F_gshared)(__this, method);
}
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.String>::Dispose()
inline void Enumerator_Dispose_mB81C8E39092695921473DBCA98385B2C53AFA7CF (Enumerator_tF6B5A6D64F2347BF9A12A794D4A8C252CA1431A7 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tF6B5A6D64F2347BF9A12A794D4A8C252CA1431A7 *, const RuntimeMethod*))Enumerator_Dispose_mD1758E7826FDA8D185FC2C218F9D32B9ADA4FE0D_gshared)(__this, method);
}
// System.Collections.Generic.List`1<System.String> GoogleMobileAds.Api.AdRequest::get_TestDevices()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * AdRequest_get_TestDevices_m02EC537E8CEEC7E695A6055B8CB1E1889F164617_inline (AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.String>::GetEnumerator()
inline Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813  List_1_GetEnumerator_mDFFBEE5A0B86EF1F068C4ED0ABC0F39B7CA7677E (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813  (*) (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *, const RuntimeMethod*))List_1_GetEnumerator_m52CC760E475D226A2B75048D70C4E22692F9F68D_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<System.String>::get_Current()
inline String_t* Enumerator_get_Current_m894E7226842A0AB920967095678A311EFF7C5737_inline (Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813 * __this, const RuntimeMethod* method)
{
	return ((  String_t* (*) (Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813 *, const RuntimeMethod*))Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared_inline)(__this, method);
}
// System.Void GoogleMobileAds.iOS.Externs::GADUAddTestDevice(System.IntPtr,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Externs_GADUAddTestDevice_m76BAC633F8636E5D4F8BF2A1D6A9383B7BCC3866 (intptr_t ___request0, String_t* ___deviceId1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.String>::MoveNext()
inline bool Enumerator_MoveNext_m129741E497FB617DC9845CFEE4CB27B84C86301A (Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813 *, const RuntimeMethod*))Enumerator_MoveNext_m38B1099DDAD7EEDE2F4CDAB11C095AC784AC2E34_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.String>::Dispose()
inline void Enumerator_Dispose_mD9B1DB257A9F9A3CEA69542101B953689A4AD978 (Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813 *, const RuntimeMethod*))Enumerator_Dispose_m94D0DAE031619503CDA6E53C5C3CC78AF3139472_gshared)(__this, method);
}
// System.Nullable`1<System.DateTime> GoogleMobileAds.Api.AdRequest::get_Birthday()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Nullable_1_t9B468FE267D697A84B680BFB00D8C20BAB1A26E7  AdRequest_get_Birthday_mF6551ECBB0E8EB04D73536381A5685D0FF55715A_inline (AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * __this, const RuntimeMethod* method);
// System.Boolean System.Nullable`1<System.DateTime>::get_HasValue()
inline bool Nullable_1_get_HasValue_mB231DEE33107C7E966680F0404FFDD7939B24625_inline (Nullable_1_t9B468FE267D697A84B680BFB00D8C20BAB1A26E7 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Nullable_1_t9B468FE267D697A84B680BFB00D8C20BAB1A26E7 *, const RuntimeMethod*))Nullable_1_get_HasValue_mB231DEE33107C7E966680F0404FFDD7939B24625_gshared_inline)(__this, method);
}
// !0 System.Nullable`1<System.DateTime>::GetValueOrDefault()
inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  Nullable_1_GetValueOrDefault_mA2BC7FE28E1E54F6DED8F2DA5ACA72A56083E43D_inline (Nullable_1_t9B468FE267D697A84B680BFB00D8C20BAB1A26E7 * __this, const RuntimeMethod* method)
{
	return ((  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  (*) (Nullable_1_t9B468FE267D697A84B680BFB00D8C20BAB1A26E7 *, const RuntimeMethod*))Nullable_1_GetValueOrDefault_mA2BC7FE28E1E54F6DED8F2DA5ACA72A56083E43D_gshared_inline)(__this, method);
}
// System.Int32 System.DateTime::get_Year()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DateTime_get_Year_m019BED6042282D03E51CE82F590D2A9FE5EA859E (DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * __this, const RuntimeMethod* method);
// System.Int32 System.DateTime::get_Month()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DateTime_get_Month_m9E31D84567E6D221F0E686EC6894A7AD07A5E43C (DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * __this, const RuntimeMethod* method);
// System.Int32 System.DateTime::get_Day()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DateTime_get_Day_m3C888FF1DA5019583A4326FAB232F81D32B1478D (DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * __this, const RuntimeMethod* method);
// System.Void GoogleMobileAds.iOS.Externs::GADUSetBirthday(System.IntPtr,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Externs_GADUSetBirthday_mF7965515078BEAD86EBCC07FFA7004755A1F4D4F (intptr_t ___request0, int32_t ___year1, int32_t ___month2, int32_t ___day3, const RuntimeMethod* method);
// System.Nullable`1<GoogleMobileAds.Api.Gender> GoogleMobileAds.Api.AdRequest::get_Gender()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Nullable_1_t65AAF2600D9569366D224E2EB7B3F65128A96D1B  AdRequest_get_Gender_m40E1D2BE6A214044F95918E202D11E5551118889_inline (AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * __this, const RuntimeMethod* method);
// System.Boolean System.Nullable`1<GoogleMobileAds.Api.Gender>::get_HasValue()
inline bool Nullable_1_get_HasValue_mE966D0285B7519BF51C1B117A064B4F40858077A_inline (Nullable_1_t65AAF2600D9569366D224E2EB7B3F65128A96D1B * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Nullable_1_t65AAF2600D9569366D224E2EB7B3F65128A96D1B *, const RuntimeMethod*))Nullable_1_get_HasValue_m58742F2DBAEDAA6626C115F9E99209C8F4D8F195_gshared_inline)(__this, method);
}
// !0 System.Nullable`1<GoogleMobileAds.Api.Gender>::GetValueOrDefault()
inline int32_t Nullable_1_GetValueOrDefault_m9EDDA3FC2B886F793AC331A3E3617416D931833E_inline (Nullable_1_t65AAF2600D9569366D224E2EB7B3F65128A96D1B * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Nullable_1_t65AAF2600D9569366D224E2EB7B3F65128A96D1B *, const RuntimeMethod*))Nullable_1_GetValueOrDefault_m19DE1B4F1BFB431B2FE3475CECA352580499168D_gshared_inline)(__this, method);
}
// System.Void GoogleMobileAds.iOS.Externs::GADUSetGender(System.IntPtr,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Externs_GADUSetGender_m6DA6BDFDB359466F6E2FBBC7FCF77693DA9241C2 (intptr_t ___request0, int32_t ___genderCode1, const RuntimeMethod* method);
// System.Nullable`1<System.Boolean> GoogleMobileAds.Api.AdRequest::get_TagForChildDirectedTreatment()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  AdRequest_get_TagForChildDirectedTreatment_mAF138AD39FC86FA60D310B685A6F3F1CDE91ABAA_inline (AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * __this, const RuntimeMethod* method);
// System.Boolean System.Nullable`1<System.Boolean>::get_HasValue()
inline bool Nullable_1_get_HasValue_m275A31438FCDAEEE039E95D887684E04FD6ECE2B_inline (Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 *, const RuntimeMethod*))Nullable_1_get_HasValue_m275A31438FCDAEEE039E95D887684E04FD6ECE2B_gshared_inline)(__this, method);
}
// !0 System.Nullable`1<System.Boolean>::GetValueOrDefault()
inline bool Nullable_1_GetValueOrDefault_mD65884636E94AF88C2745255BBE130A1B64BA294_inline (Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 *, const RuntimeMethod*))Nullable_1_GetValueOrDefault_mD65884636E94AF88C2745255BBE130A1B64BA294_gshared_inline)(__this, method);
}
// System.Void GoogleMobileAds.iOS.Externs::GADUTagForChildDirectedTreatment(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Externs_GADUTagForChildDirectedTreatment_m79D9D3D2E5D37AD810F239728B87D2264F994AF3 (intptr_t ___request0, bool ___childDirectedTreatment1, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2<System.String,System.String> GoogleMobileAds.Api.AdRequest::get_Extras()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * AdRequest_get_Extras_m814DAAAC04DFD17FC1DD6446718BEF964934B83B_inline (AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * __this, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.String,System.String>::GetEnumerator()
inline Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3  Dictionary_2_GetEnumerator_m3378B4792B81EF81397CB9D9A761BD7149BD27F5 (Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3  (*) (Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC *, const RuntimeMethod*))Dictionary_2_GetEnumerator_mF1CF1D13F3E70C6D20D96D9AC88E44454E4C0053_gshared)(__this, method);
}
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::get_Current()
inline KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8  Enumerator_get_Current_mBEC9B470213860581893E0F197CAAE657B8B6C69_inline (Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3 * __this, const RuntimeMethod* method)
{
	return ((  KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8  (*) (Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3 *, const RuntimeMethod*))Enumerator_get_Current_m5B32A9FC8294CB723DCD1171744B32E1775B6318_gshared_inline)(__this, method);
}
// !0 System.Collections.Generic.KeyValuePair`2<System.String,System.String>::get_Key()
inline String_t* KeyValuePair_2_get_Key_m434E29A1251E81B5A2124466105823011C462BF2_inline (KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8 * __this, const RuntimeMethod* method)
{
	return ((  String_t* (*) (KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m9D4E9BCBAB1BE560871A0889C851FC22A09975F4_gshared_inline)(__this, method);
}
// !1 System.Collections.Generic.KeyValuePair`2<System.String,System.String>::get_Value()
inline String_t* KeyValuePair_2_get_Value_mEAF4B15DEEAC6EB29683A5C6569F0F50B4DEBDA2_inline (KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8 * __this, const RuntimeMethod* method)
{
	return ((  String_t* (*) (KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m8C7B882C4D425535288FAAD08EAF11D289A43AEC_gshared_inline)(__this, method);
}
// System.Void GoogleMobileAds.iOS.Externs::GADUSetExtra(System.IntPtr,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Externs_GADUSetExtra_m220E34583749470A8335664E94766D25674D1726 (intptr_t ___request0, String_t* ___key1, String_t* ___value2, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::MoveNext()
inline bool Enumerator_MoveNext_m6E6A22A8620F5A5582BB67E367BE5086D7D895A6 (Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3 *, const RuntimeMethod*))Enumerator_MoveNext_m9B9FB07EC2C1D82E921C9316A4E0901C933BBF6C_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::Dispose()
inline void Enumerator_Dispose_m16C0E963A012498CD27422B463DB327BA4C7A321 (Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3 *, const RuntimeMethod*))Enumerator_Dispose_mE363888280B72ED50538416C060EF9FC94B3BB00_gshared)(__this, method);
}
// System.Collections.Generic.List`1<GoogleMobileAds.Api.Mediation.MediationExtras> GoogleMobileAds.Api.AdRequest::get_MediationExtras()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR List_1_t8F38EE057190BE9897CDE829F7EB4282DF6F3EBA * AdRequest_get_MediationExtras_mEEC772BDDD5CA17C993C45BD09F342E0CCFB1842_inline (AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<GoogleMobileAds.Api.Mediation.MediationExtras>::GetEnumerator()
inline Enumerator_t1B7A508A639F9A6260571CC57EAB3F3C4B96A49C  List_1_GetEnumerator_m739CC8C648FEC19900D51E4F3BCE6B15D3C6325A (List_1_t8F38EE057190BE9897CDE829F7EB4282DF6F3EBA * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t1B7A508A639F9A6260571CC57EAB3F3C4B96A49C  (*) (List_1_t8F38EE057190BE9897CDE829F7EB4282DF6F3EBA *, const RuntimeMethod*))List_1_GetEnumerator_m52CC760E475D226A2B75048D70C4E22692F9F68D_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<GoogleMobileAds.Api.Mediation.MediationExtras>::get_Current()
inline MediationExtras_t53F0F7F90139A2D039272C9764DC880D4B3AB301 * Enumerator_get_Current_mB8FC184CAAB4A8E56B08699B9C9894803A9EA58A_inline (Enumerator_t1B7A508A639F9A6260571CC57EAB3F3C4B96A49C * __this, const RuntimeMethod* method)
{
	return ((  MediationExtras_t53F0F7F90139A2D039272C9764DC880D4B3AB301 * (*) (Enumerator_t1B7A508A639F9A6260571CC57EAB3F3C4B96A49C *, const RuntimeMethod*))Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared_inline)(__this, method);
}
// System.IntPtr GoogleMobileAds.iOS.Externs::GADUCreateMutableDictionary()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t Externs_GADUCreateMutableDictionary_m71EE6107C3600A67D5A7C4D73C018CBAE659C23C (const RuntimeMethod* method);
// System.Boolean System.IntPtr::op_Inequality(System.IntPtr,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61 (intptr_t ___value10, intptr_t ___value21, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2<System.String,System.String> GoogleMobileAds.Api.Mediation.MediationExtras::get_Extras()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * MediationExtras_get_Extras_m845B48080AE808551366F9B6E363E0429EAE7869_inline (MediationExtras_t53F0F7F90139A2D039272C9764DC880D4B3AB301 * __this, const RuntimeMethod* method);
// System.Void GoogleMobileAds.iOS.Externs::GADUMutableDictionarySetValue(System.IntPtr,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Externs_GADUMutableDictionarySetValue_m1806F4834F6CCA36CD93928D50F81B097D90F60D (intptr_t ___mutableDictionaryPtr0, String_t* ___key1, String_t* ___value2, const RuntimeMethod* method);
// System.Void GoogleMobileAds.iOS.Externs::GADUSetMediationExtras(System.IntPtr,System.IntPtr,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Externs_GADUSetMediationExtras_m89618AB332B3A9FE4FB423511B821186B35F1D2E (intptr_t ___request0, intptr_t ___mutableDictionaryPtr1, String_t* ___adNetworkExtrasClassName2, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<GoogleMobileAds.Api.Mediation.MediationExtras>::MoveNext()
inline bool Enumerator_MoveNext_mF9F196771804D9FAE1887C1EE679DB80CF90182D (Enumerator_t1B7A508A639F9A6260571CC57EAB3F3C4B96A49C * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t1B7A508A639F9A6260571CC57EAB3F3C4B96A49C *, const RuntimeMethod*))Enumerator_MoveNext_m38B1099DDAD7EEDE2F4CDAB11C095AC784AC2E34_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<GoogleMobileAds.Api.Mediation.MediationExtras>::Dispose()
inline void Enumerator_Dispose_mB4D942857769BA4A52C25194705752C32D86DCED (Enumerator_t1B7A508A639F9A6260571CC57EAB3F3C4B96A49C * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t1B7A508A639F9A6260571CC57EAB3F3C4B96A49C *, const RuntimeMethod*))Enumerator_Dispose_m94D0DAE031619503CDA6E53C5C3CC78AF3139472_gshared)(__this, method);
}
// System.Void GoogleMobileAds.iOS.Externs::GADUSetRequestAgent(System.IntPtr,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Externs_GADUSetRequestAgent_m27CF00F87EA0EBA6A9C3C8709E5FAD8810198647 (intptr_t ___request0, String_t* ___requestAgent1, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlayerPrefs_GetInt_m318D2B42E0FCAF179BF86D6C2353B38A58089BAD (String_t* ___key0, const RuntimeMethod* method);
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPrefs_SetInt_mBF4101DF829B4738CCC293E1C2D173AEE45EFE62 (String_t* ___key0, int32_t ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.PlayerPrefs::Save()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPrefs_Save_m6CC1FE22D4B10AC819F55802D725BE17EA2AD37B (const RuntimeMethod* method);
// System.Void Menu::OpenScreen(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menu_OpenScreen_mCF89ADA1C1BA0D861053344895E94772A530EFE6 (Menu_t7D066A010AF240D53C2DDA2B05DF31FD0B5F3129 * __this, int32_t ___id0, const RuntimeMethod* method);
// UnityEngine.NetworkReachability UnityEngine.Application::get_internetReachability()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Application_get_internetReachability_m8BB82882CF9E286370E4D64176CF2B9333E83F0A (const RuntimeMethod* method);
// System.Void Menu::RequestRewardVideo()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menu_RequestRewardVideo_mC6FC3DBAC40902860533D8BF5C78FB5BBD476944 (Menu_t7D066A010AF240D53C2DDA2B05DF31FD0B5F3129 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetKeyDown_mEA57896808B6F484B12CD0AEEB83390A3CFCDBDC (int32_t ___key0, const RuntimeMethod* method);
// System.Void UnityEngine.Application::Quit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Application_Quit_mA005EB22CB989AC3794334754F15E1C0D2FF1C95 (const RuntimeMethod* method);
// GoogleMobileAds.Api.RewardBasedVideoAd GoogleMobileAds.Api.RewardBasedVideoAd::get_Instance()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491 * RewardBasedVideoAd_get_Instance_mD22F18358E8AAE1EF7D0EF41D89938E27F110413_inline (const RuntimeMethod* method);
// System.Void GoogleMobileAds.Api.AdRequest/Builder::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Builder__ctor_m0E264AB53F2163A32997830032FAFE5B49BD0897 (Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9 * __this, const RuntimeMethod* method);
// GoogleMobileAds.Api.AdRequest/Builder GoogleMobileAds.Api.AdRequest/Builder::AddKeyword(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9 * Builder_AddKeyword_m9FB2118C9FF37FF5537CDBF091A1DC4E9F87B039 (Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9 * __this, String_t* ___keyword0, const RuntimeMethod* method);
// GoogleMobileAds.Api.AdRequest/Builder GoogleMobileAds.Api.AdRequest/Builder::TagForChildDirectedTreatment(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9 * Builder_TagForChildDirectedTreatment_m673ED0659E607F8E82672E7C106DA36CD064F732 (Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9 * __this, bool ___tagForChildDirectedTreatment0, const RuntimeMethod* method);
// GoogleMobileAds.Api.AdRequest GoogleMobileAds.Api.AdRequest/Builder::Build()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * Builder_Build_mD244B61E5DBF9D8D8E882A33977EAC5A6187BFC7 (Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9 * __this, const RuntimeMethod* method);
// System.Void System.EventHandler`1<GoogleMobileAds.Api.Reward>::.ctor(System.Object,System.IntPtr)
inline void EventHandler_1__ctor_m1450748EF9416B797EDE630BEA6C50F96956282F (EventHandler_1_tE84E7D22A41CECD013ED02984B817FEBA6923CC8 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (EventHandler_1_tE84E7D22A41CECD013ED02984B817FEBA6923CC8 *, RuntimeObject *, intptr_t, const RuntimeMethod*))EventHandler_1__ctor_m3D6B301FCEB48533990CFB2386DAAB091D357BAC_gshared)(__this, ___object0, ___method1, method);
}
// System.Void GoogleMobileAds.Api.RewardBasedVideoAd::add_OnAdRewarded(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardBasedVideoAd_add_OnAdRewarded_mA6F7313150EB1E9237EF673700535A0E5DB4E200 (RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491 * __this, EventHandler_1_tE84E7D22A41CECD013ED02984B817FEBA6923CC8 * ___value0, const RuntimeMethod* method);
// System.Void GoogleMobileAds.Api.RewardBasedVideoAd::LoadAd(GoogleMobileAds.Api.AdRequest,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardBasedVideoAd_LoadAd_mDC5DA9B2E11911512038CBE1455D1066834CB1CE (RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491 * __this, AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * ___request0, String_t* ___adUnitId1, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_LoadScene_mFC850AC783E5EA05D6154976385DFECC251CDFB9 (String_t* ___sceneName0, const RuntimeMethod* method);
// System.String UnityEngine.Application::get_identifier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Application_get_identifier_m02D55D452A7710BB0890E529D9E8F891AF6C2AE8 (const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Void UnityEngine.Application::OpenURL(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Application_OpenURL_m2888DA5BDF68B1BC23E983469157783F390D7BC8 (String_t* ___url0, const RuntimeMethod* method);
// System.Boolean GoogleMobileAds.Api.RewardBasedVideoAd::IsLoaded()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool RewardBasedVideoAd_IsLoaded_m52AE856E045477F4340E5A04F3497E51C71E3744 (RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491 * __this, const RuntimeMethod* method);
// System.Void GoogleMobileAds.Api.RewardBasedVideoAd::Show()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RewardBasedVideoAd_Show_m6690A11AF00C03DFD435BD1D95D66F9FB67178F1 (RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// System.Void System.Attribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0 (Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74 * __this, const RuntimeMethod* method);
// System.Void Purchaser::InitializePurchasing()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Purchaser_InitializePurchasing_mA433D882053BB54E75A01A2919EA1989C9470F9F (Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037 * __this, const RuntimeMethod* method);
// System.Boolean Purchaser::IsInitialized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Purchaser_IsInitialized_m272B7D0B6DB8CDEB87223E1224F013BB3533C890 (Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037 * __this, const RuntimeMethod* method);
// UnityEngine.Purchasing.StandardPurchasingModule UnityEngine.Purchasing.StandardPurchasingModule::Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StandardPurchasingModule_tAB117B14746822C4AF812B66148122909DD53847 * StandardPurchasingModule_Instance_mB18020DD1E1F75B5841D60DC58F9BB3D96F79CE5 (const RuntimeMethod* method);
// !!0[] System.Array::Empty<UnityEngine.Purchasing.Extension.IPurchasingModule>()
inline IPurchasingModuleU5BU5D_tF106F307EE194D8B47140E160F5BC38EDFFF9384* Array_Empty_TisIPurchasingModule_tBA2577ED1E963CA0ABA1DFA359A020982DE0B1BF_m6A5F4EE32F39C664A1A279F15D55E86D8DA4D93E_inline (const RuntimeMethod* method)
{
	return ((  IPurchasingModuleU5BU5D_tF106F307EE194D8B47140E160F5BC38EDFFF9384* (*) (const RuntimeMethod*))Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_gshared_inline)(method);
}
// UnityEngine.Purchasing.ConfigurationBuilder UnityEngine.Purchasing.ConfigurationBuilder::Instance(UnityEngine.Purchasing.Extension.IPurchasingModule,UnityEngine.Purchasing.Extension.IPurchasingModule[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE * ConfigurationBuilder_Instance_m8F4EA19F3C0FF462CCF219D0A019BEADA05E7DA2 (RuntimeObject* ___first0, IPurchasingModuleU5BU5D_tF106F307EE194D8B47140E160F5BC38EDFFF9384* ___rest1, const RuntimeMethod* method);
// UnityEngine.Purchasing.ConfigurationBuilder UnityEngine.Purchasing.ConfigurationBuilder::AddProduct(System.String,UnityEngine.Purchasing.ProductType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE * ConfigurationBuilder_AddProduct_m3E95B8F467E2A9C5594E55B04FE33A72D0C66DC0 (ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE * __this, String_t* ___id0, int32_t ___type1, const RuntimeMethod* method);
// System.Void UnityEngine.Purchasing.UnityPurchasing::Initialize(UnityEngine.Purchasing.IStoreListener,UnityEngine.Purchasing.ConfigurationBuilder)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityPurchasing_Initialize_m1BEFCE608B4D495EEB6CF346E51AB8CFB84CBFDC (RuntimeObject* ___listener0, ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE * ___builder1, const RuntimeMethod* method);
// System.Void Purchaser::BuyProductID(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Purchaser_BuyProductID_mBCDAC5023D5830F87D3E0116AE9DC5447065385E (Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037 * __this, String_t* ___productId0, const RuntimeMethod* method);
// UnityEngine.Purchasing.Product UnityEngine.Purchasing.ProductCollection::WithID(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * ProductCollection_WithID_m7B5AB4C1B658271EC30742DD07D53E9043A816A4 (ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07 * __this, String_t* ___id0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Purchasing.Product::get_availableToPurchase()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool Product_get_availableToPurchase_mA4F6F87AA8B83915F1B5E122A10F0CBAFBAE196C_inline (Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * __this, const RuntimeMethod* method);
// UnityEngine.Purchasing.ProductDefinition UnityEngine.Purchasing.Product::get_definition()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * Product_get_definition_mFA1889844E23D37A700C2EA6659EE3BBF1E17C58_inline (Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * __this, const RuntimeMethod* method);
// System.String UnityEngine.Purchasing.ProductDefinition::get_id()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* ProductDefinition_get_id_mD5C2A9C1016245C99EB67B74DFC32ECDC8512468_inline (ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * __this, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_m0ACDD8B34764E4040AED0B3EEB753567E4576BFA (String_t* ___format0, RuntimeObject * ___arg01, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708 (RuntimeObject * ___message0, const RuntimeMethod* method);
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672 (const RuntimeMethod* method);
// System.Void System.Action`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_mFEB000BCBDAB6DE953B2B50CD113641DCF601890 (Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_mFEB000BCBDAB6DE953B2B50CD113641DCF601890_gshared)(__this, ___object0, ___method1, method);
}
// System.String System.String::Concat(System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495 (RuntimeObject * ___arg00, RuntimeObject * ___arg11, const RuntimeMethod* method);
// UnityEngine.Purchasing.Product UnityEngine.Purchasing.PurchaseEventArgs::get_purchasedProduct()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * PurchaseEventArgs_get_purchasedProduct_mB26BCEF9CD45B84DBF55A5C6FC50AF611B6AD6A2_inline (PurchaseEventArgs_tF6E04BFD5492F5F57309FFBB2415EB26E5B88C04 * __this, const RuntimeMethod* method);
// System.Boolean System.String::Equals(System.String,System.String,System.StringComparison)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_Equals_m1A3302D7214F75FB06302101934BF3EE9282AA43 (String_t* ___a0, String_t* ___b1, int32_t ___comparisonType2, const RuntimeMethod* method);
// System.String UnityEngine.Purchasing.ProductDefinition::get_storeSpecificId()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* ProductDefinition_get_storeSpecificId_m544E2354C4C9049DE5C24D07A347669A05B241D0_inline (ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * __this, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_m19325298DBC61AAC016C16F7B3CF97A8A3DEA34A (String_t* ___format0, RuntimeObject * ___arg01, RuntimeObject * ___arg12, const RuntimeMethod* method);
// System.Void Purchaser/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mFFFF6E872083F954C08D0CB94E2B71E42C0B465D (U3CU3Ec_t4CD03EBC853C316BC6B34DD2FD63D9BDEE3F6BC2 * __this, const RuntimeMethod* method);
// System.String System.Boolean::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Boolean_ToString_m62D1EFD5F6D5F6B6AF0D14A07BF5741C94413301 (bool* __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mF4626905368D6558695A823466A1AF65EADB9923 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_GADURewardBasedVideoAdWillLeaveApplicationCallback_t0056DE714C24D44E29732E1C20F113CB30974404 (GADURewardBasedVideoAdWillLeaveApplicationCallback_t0056DE714C24D44E29732E1C20F113CB30974404 * __this, intptr_t ___rewardBasedVideoAdClient0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)(intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___rewardBasedVideoAdClient0);

}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdWillLeaveApplicationCallback::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GADURewardBasedVideoAdWillLeaveApplicationCallback__ctor_m8BD895E3953FF6819459E215D6C87C6CEF0642F1 (GADURewardBasedVideoAdWillLeaveApplicationCallback_t0056DE714C24D44E29732E1C20F113CB30974404 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdWillLeaveApplicationCallback::Invoke(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GADURewardBasedVideoAdWillLeaveApplicationCallback_Invoke_mEEB31E100B363252E5946E753A1526DBE27D3115 (GADURewardBasedVideoAdWillLeaveApplicationCallback_t0056DE714C24D44E29732E1C20F113CB30974404 * __this, intptr_t ___rewardBasedVideoAdClient0, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef void (*FunctionPointerType) (intptr_t, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___rewardBasedVideoAdClient0, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, intptr_t, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___rewardBasedVideoAdClient0, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (intptr_t, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___rewardBasedVideoAdClient0, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< intptr_t >::Invoke(targetMethod, targetThis, ___rewardBasedVideoAdClient0);
					else
						GenericVirtActionInvoker1< intptr_t >::Invoke(targetMethod, targetThis, ___rewardBasedVideoAdClient0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< intptr_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___rewardBasedVideoAdClient0);
					else
						VirtActionInvoker1< intptr_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___rewardBasedVideoAdClient0);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef void (*FunctionPointerType) (RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(&___rewardBasedVideoAdClient0) - 1), targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, intptr_t, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___rewardBasedVideoAdClient0, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdWillLeaveApplicationCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GADURewardBasedVideoAdWillLeaveApplicationCallback_BeginInvoke_mED9BCF0C91B338AD4B9A6E39629994AFEAAE6F32 (GADURewardBasedVideoAdWillLeaveApplicationCallback_t0056DE714C24D44E29732E1C20F113CB30974404 * __this, intptr_t ___rewardBasedVideoAdClient0, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GADURewardBasedVideoAdWillLeaveApplicationCallback_BeginInvoke_mED9BCF0C91B338AD4B9A6E39629994AFEAAE6F32_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___rewardBasedVideoAdClient0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdWillLeaveApplicationCallback::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GADURewardBasedVideoAdWillLeaveApplicationCallback_EndInvoke_m3022871D902B75E579C671E2161BDD27035154B5 (GADURewardBasedVideoAdWillLeaveApplicationCallback_t0056DE714C24D44E29732E1C20F113CB30974404 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.IntPtr GoogleMobileAds.iOS.Utils::BuildAdRequest(GoogleMobileAds.Api.AdRequest)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t Utils_BuildAdRequest_m79F99FD27AEBB11A401DF217D6C5F537B8CC9FA2 (AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * ___request0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utils_BuildAdRequest_m79F99FD27AEBB11A401DF217D6C5F537B8CC9FA2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t V_0;
	memset((&V_0), 0, sizeof(V_0));
	Enumerator_tF6B5A6D64F2347BF9A12A794D4A8C252CA1431A7  V_1;
	memset((&V_1), 0, sizeof(V_1));
	String_t* V_2 = NULL;
	Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813  V_3;
	memset((&V_3), 0, sizeof(V_3));
	String_t* V_4 = NULL;
	Nullable_1_t9B468FE267D697A84B680BFB00D8C20BAB1A26E7  V_5;
	memset((&V_5), 0, sizeof(V_5));
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  V_6;
	memset((&V_6), 0, sizeof(V_6));
	Nullable_1_t65AAF2600D9569366D224E2EB7B3F65128A96D1B  V_7;
	memset((&V_7), 0, sizeof(V_7));
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  V_8;
	memset((&V_8), 0, sizeof(V_8));
	Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3  V_9;
	memset((&V_9), 0, sizeof(V_9));
	KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8  V_10;
	memset((&V_10), 0, sizeof(V_10));
	Enumerator_t1B7A508A639F9A6260571CC57EAB3F3C4B96A49C  V_11;
	memset((&V_11), 0, sizeof(V_11));
	MediationExtras_t53F0F7F90139A2D039272C9764DC880D4B3AB301 * V_12 = NULL;
	intptr_t V_13;
	memset((&V_13), 0, sizeof(V_13));
	KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8  V_14;
	memset((&V_14), 0, sizeof(V_14));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 5);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// IntPtr requestPtr = Externs.GADUCreateRequest();
		intptr_t L_0 = Externs_GADUCreateRequest_m73B645271C266986E9711F38EF4E0CA6FC511D9C(/*hidden argument*/NULL);
		V_0 = (intptr_t)L_0;
		// foreach (string keyword in request.Keywords)
		AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * L_1 = ___request0;
		NullCheck(L_1);
		HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61 * L_2 = AdRequest_get_Keywords_mA1D66061D1F93CF256DDD5AAE938C629104F11AA_inline(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Enumerator_tF6B5A6D64F2347BF9A12A794D4A8C252CA1431A7  L_3 = HashSet_1_GetEnumerator_m2EC6A826B51D71BE1AB94EEFB697BC6412B1CDF8(L_2, /*hidden argument*/HashSet_1_GetEnumerator_m2EC6A826B51D71BE1AB94EEFB697BC6412B1CDF8_RuntimeMethod_var);
		V_1 = L_3;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0023;
		}

IL_0014:
		{
			// foreach (string keyword in request.Keywords)
			String_t* L_4 = Enumerator_get_Current_mF1A272E02A81B502A1CDF431D0AC41BE3D53E764_inline((Enumerator_tF6B5A6D64F2347BF9A12A794D4A8C252CA1431A7 *)(&V_1), /*hidden argument*/Enumerator_get_Current_mF1A272E02A81B502A1CDF431D0AC41BE3D53E764_RuntimeMethod_var);
			V_2 = L_4;
			// Externs.GADUAddKeyword(requestPtr, keyword);
			intptr_t L_5 = V_0;
			String_t* L_6 = V_2;
			Externs_GADUAddKeyword_mCF88D72AA181DCB3C3DA3FB9BD096E64818FB492((intptr_t)L_5, L_6, /*hidden argument*/NULL);
		}

IL_0023:
		{
			// foreach (string keyword in request.Keywords)
			bool L_7 = Enumerator_MoveNext_mCCD47EE234D4C2A16ED96329CC6BC10D2C101702((Enumerator_tF6B5A6D64F2347BF9A12A794D4A8C252CA1431A7 *)(&V_1), /*hidden argument*/Enumerator_MoveNext_mCCD47EE234D4C2A16ED96329CC6BC10D2C101702_RuntimeMethod_var);
			if (L_7)
			{
				goto IL_0014;
			}
		}

IL_002c:
		{
			IL2CPP_LEAVE(0x3C, FINALLY_002e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_002e;
	}

FINALLY_002e:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_mB81C8E39092695921473DBCA98385B2C53AFA7CF((Enumerator_tF6B5A6D64F2347BF9A12A794D4A8C252CA1431A7 *)(&V_1), /*hidden argument*/Enumerator_Dispose_mB81C8E39092695921473DBCA98385B2C53AFA7CF_RuntimeMethod_var);
		IL2CPP_END_FINALLY(46)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(46)
	{
		IL2CPP_JUMP_TBL(0x3C, IL_003c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_003c:
	{
		// foreach (string deviceId in request.TestDevices)
		AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * L_8 = ___request0;
		NullCheck(L_8);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_9 = AdRequest_get_TestDevices_m02EC537E8CEEC7E695A6055B8CB1E1889F164617_inline(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813  L_10 = List_1_GetEnumerator_mDFFBEE5A0B86EF1F068C4ED0ABC0F39B7CA7677E(L_9, /*hidden argument*/List_1_GetEnumerator_mDFFBEE5A0B86EF1F068C4ED0ABC0F39B7CA7677E_RuntimeMethod_var);
		V_3 = L_10;
	}

IL_0048:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005b;
		}

IL_004a:
		{
			// foreach (string deviceId in request.TestDevices)
			String_t* L_11 = Enumerator_get_Current_m894E7226842A0AB920967095678A311EFF7C5737_inline((Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813 *)(&V_3), /*hidden argument*/Enumerator_get_Current_m894E7226842A0AB920967095678A311EFF7C5737_RuntimeMethod_var);
			V_4 = L_11;
			// Externs.GADUAddTestDevice(requestPtr, deviceId);
			intptr_t L_12 = V_0;
			String_t* L_13 = V_4;
			Externs_GADUAddTestDevice_m76BAC633F8636E5D4F8BF2A1D6A9383B7BCC3866((intptr_t)L_12, L_13, /*hidden argument*/NULL);
		}

IL_005b:
		{
			// foreach (string deviceId in request.TestDevices)
			bool L_14 = Enumerator_MoveNext_m129741E497FB617DC9845CFEE4CB27B84C86301A((Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813 *)(&V_3), /*hidden argument*/Enumerator_MoveNext_m129741E497FB617DC9845CFEE4CB27B84C86301A_RuntimeMethod_var);
			if (L_14)
			{
				goto IL_004a;
			}
		}

IL_0064:
		{
			IL2CPP_LEAVE(0x74, FINALLY_0066);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0066;
	}

FINALLY_0066:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_mD9B1DB257A9F9A3CEA69542101B953689A4AD978((Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813 *)(&V_3), /*hidden argument*/Enumerator_Dispose_mD9B1DB257A9F9A3CEA69542101B953689A4AD978_RuntimeMethod_var);
		IL2CPP_END_FINALLY(102)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(102)
	{
		IL2CPP_JUMP_TBL(0x74, IL_0074)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0074:
	{
		// if (request.Birthday.HasValue)
		AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * L_15 = ___request0;
		NullCheck(L_15);
		Nullable_1_t9B468FE267D697A84B680BFB00D8C20BAB1A26E7  L_16 = AdRequest_get_Birthday_mF6551ECBB0E8EB04D73536381A5685D0FF55715A_inline(L_15, /*hidden argument*/NULL);
		V_5 = L_16;
		bool L_17 = Nullable_1_get_HasValue_mB231DEE33107C7E966680F0404FFDD7939B24625_inline((Nullable_1_t9B468FE267D697A84B680BFB00D8C20BAB1A26E7 *)(&V_5), /*hidden argument*/Nullable_1_get_HasValue_mB231DEE33107C7E966680F0404FFDD7939B24625_RuntimeMethod_var);
		if (!L_17)
		{
			goto IL_00b1;
		}
	}
	{
		// DateTime birthday = request.Birthday.GetValueOrDefault();
		AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * L_18 = ___request0;
		NullCheck(L_18);
		Nullable_1_t9B468FE267D697A84B680BFB00D8C20BAB1A26E7  L_19 = AdRequest_get_Birthday_mF6551ECBB0E8EB04D73536381A5685D0FF55715A_inline(L_18, /*hidden argument*/NULL);
		V_5 = L_19;
		DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  L_20 = Nullable_1_GetValueOrDefault_mA2BC7FE28E1E54F6DED8F2DA5ACA72A56083E43D_inline((Nullable_1_t9B468FE267D697A84B680BFB00D8C20BAB1A26E7 *)(&V_5), /*hidden argument*/Nullable_1_GetValueOrDefault_mA2BC7FE28E1E54F6DED8F2DA5ACA72A56083E43D_RuntimeMethod_var);
		V_6 = L_20;
		// Externs.GADUSetBirthday(requestPtr, birthday.Year, birthday.Month, birthday.Day);
		intptr_t L_21 = V_0;
		int32_t L_22 = DateTime_get_Year_m019BED6042282D03E51CE82F590D2A9FE5EA859E((DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 *)(&V_6), /*hidden argument*/NULL);
		int32_t L_23 = DateTime_get_Month_m9E31D84567E6D221F0E686EC6894A7AD07A5E43C((DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 *)(&V_6), /*hidden argument*/NULL);
		int32_t L_24 = DateTime_get_Day_m3C888FF1DA5019583A4326FAB232F81D32B1478D((DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 *)(&V_6), /*hidden argument*/NULL);
		Externs_GADUSetBirthday_mF7965515078BEAD86EBCC07FFA7004755A1F4D4F((intptr_t)L_21, L_22, L_23, L_24, /*hidden argument*/NULL);
	}

IL_00b1:
	{
		// if (request.Gender.HasValue)
		AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * L_25 = ___request0;
		NullCheck(L_25);
		Nullable_1_t65AAF2600D9569366D224E2EB7B3F65128A96D1B  L_26 = AdRequest_get_Gender_m40E1D2BE6A214044F95918E202D11E5551118889_inline(L_25, /*hidden argument*/NULL);
		V_7 = L_26;
		bool L_27 = Nullable_1_get_HasValue_mE966D0285B7519BF51C1B117A064B4F40858077A_inline((Nullable_1_t65AAF2600D9569366D224E2EB7B3F65128A96D1B *)(&V_7), /*hidden argument*/Nullable_1_get_HasValue_mE966D0285B7519BF51C1B117A064B4F40858077A_RuntimeMethod_var);
		if (!L_27)
		{
			goto IL_00d7;
		}
	}
	{
		// Externs.GADUSetGender(requestPtr, (int)request.Gender.GetValueOrDefault());
		intptr_t L_28 = V_0;
		AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * L_29 = ___request0;
		NullCheck(L_29);
		Nullable_1_t65AAF2600D9569366D224E2EB7B3F65128A96D1B  L_30 = AdRequest_get_Gender_m40E1D2BE6A214044F95918E202D11E5551118889_inline(L_29, /*hidden argument*/NULL);
		V_7 = L_30;
		int32_t L_31 = Nullable_1_GetValueOrDefault_m9EDDA3FC2B886F793AC331A3E3617416D931833E_inline((Nullable_1_t65AAF2600D9569366D224E2EB7B3F65128A96D1B *)(&V_7), /*hidden argument*/Nullable_1_GetValueOrDefault_m9EDDA3FC2B886F793AC331A3E3617416D931833E_RuntimeMethod_var);
		Externs_GADUSetGender_m6DA6BDFDB359466F6E2FBBC7FCF77693DA9241C2((intptr_t)L_28, L_31, /*hidden argument*/NULL);
	}

IL_00d7:
	{
		// if (request.TagForChildDirectedTreatment.HasValue)
		AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * L_32 = ___request0;
		NullCheck(L_32);
		Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  L_33 = AdRequest_get_TagForChildDirectedTreatment_mAF138AD39FC86FA60D310B685A6F3F1CDE91ABAA_inline(L_32, /*hidden argument*/NULL);
		V_8 = L_33;
		bool L_34 = Nullable_1_get_HasValue_m275A31438FCDAEEE039E95D887684E04FD6ECE2B_inline((Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 *)(&V_8), /*hidden argument*/Nullable_1_get_HasValue_m275A31438FCDAEEE039E95D887684E04FD6ECE2B_RuntimeMethod_var);
		if (!L_34)
		{
			goto IL_00fd;
		}
	}
	{
		// Externs.GADUTagForChildDirectedTreatment(
		//         requestPtr,
		//         request.TagForChildDirectedTreatment.GetValueOrDefault());
		intptr_t L_35 = V_0;
		AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * L_36 = ___request0;
		NullCheck(L_36);
		Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  L_37 = AdRequest_get_TagForChildDirectedTreatment_mAF138AD39FC86FA60D310B685A6F3F1CDE91ABAA_inline(L_36, /*hidden argument*/NULL);
		V_8 = L_37;
		bool L_38 = Nullable_1_GetValueOrDefault_mD65884636E94AF88C2745255BBE130A1B64BA294_inline((Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 *)(&V_8), /*hidden argument*/Nullable_1_GetValueOrDefault_mD65884636E94AF88C2745255BBE130A1B64BA294_RuntimeMethod_var);
		Externs_GADUTagForChildDirectedTreatment_m79D9D3D2E5D37AD810F239728B87D2264F994AF3((intptr_t)L_35, L_38, /*hidden argument*/NULL);
	}

IL_00fd:
	{
		// foreach (KeyValuePair<string, string> entry in request.Extras)
		AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * L_39 = ___request0;
		NullCheck(L_39);
		Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * L_40 = AdRequest_get_Extras_m814DAAAC04DFD17FC1DD6446718BEF964934B83B_inline(L_39, /*hidden argument*/NULL);
		NullCheck(L_40);
		Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3  L_41 = Dictionary_2_GetEnumerator_m3378B4792B81EF81397CB9D9A761BD7149BD27F5(L_40, /*hidden argument*/Dictionary_2_GetEnumerator_m3378B4792B81EF81397CB9D9A761BD7149BD27F5_RuntimeMethod_var);
		V_9 = L_41;
	}

IL_010a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0129;
		}

IL_010c:
		{
			// foreach (KeyValuePair<string, string> entry in request.Extras)
			KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8  L_42 = Enumerator_get_Current_mBEC9B470213860581893E0F197CAAE657B8B6C69_inline((Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3 *)(&V_9), /*hidden argument*/Enumerator_get_Current_mBEC9B470213860581893E0F197CAAE657B8B6C69_RuntimeMethod_var);
			V_10 = L_42;
			// Externs.GADUSetExtra(requestPtr, entry.Key, entry.Value);
			intptr_t L_43 = V_0;
			String_t* L_44 = KeyValuePair_2_get_Key_m434E29A1251E81B5A2124466105823011C462BF2_inline((KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8 *)(&V_10), /*hidden argument*/KeyValuePair_2_get_Key_m434E29A1251E81B5A2124466105823011C462BF2_RuntimeMethod_var);
			String_t* L_45 = KeyValuePair_2_get_Value_mEAF4B15DEEAC6EB29683A5C6569F0F50B4DEBDA2_inline((KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8 *)(&V_10), /*hidden argument*/KeyValuePair_2_get_Value_mEAF4B15DEEAC6EB29683A5C6569F0F50B4DEBDA2_RuntimeMethod_var);
			Externs_GADUSetExtra_m220E34583749470A8335664E94766D25674D1726((intptr_t)L_43, L_44, L_45, /*hidden argument*/NULL);
		}

IL_0129:
		{
			// foreach (KeyValuePair<string, string> entry in request.Extras)
			bool L_46 = Enumerator_MoveNext_m6E6A22A8620F5A5582BB67E367BE5086D7D895A6((Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3 *)(&V_9), /*hidden argument*/Enumerator_MoveNext_m6E6A22A8620F5A5582BB67E367BE5086D7D895A6_RuntimeMethod_var);
			if (L_46)
			{
				goto IL_010c;
			}
		}

IL_0132:
		{
			IL2CPP_LEAVE(0x142, FINALLY_0134);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0134;
	}

FINALLY_0134:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m16C0E963A012498CD27422B463DB327BA4C7A321((Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3 *)(&V_9), /*hidden argument*/Enumerator_Dispose_m16C0E963A012498CD27422B463DB327BA4C7A321_RuntimeMethod_var);
		IL2CPP_END_FINALLY(308)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(308)
	{
		IL2CPP_JUMP_TBL(0x142, IL_0142)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0142:
	{
		// foreach (MediationExtras mediationExtra in request.MediationExtras)
		AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * L_47 = ___request0;
		NullCheck(L_47);
		List_1_t8F38EE057190BE9897CDE829F7EB4282DF6F3EBA * L_48 = AdRequest_get_MediationExtras_mEEC772BDDD5CA17C993C45BD09F342E0CCFB1842_inline(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		Enumerator_t1B7A508A639F9A6260571CC57EAB3F3C4B96A49C  L_49 = List_1_GetEnumerator_m739CC8C648FEC19900D51E4F3BCE6B15D3C6325A(L_48, /*hidden argument*/List_1_GetEnumerator_m739CC8C648FEC19900D51E4F3BCE6B15D3C6325A_RuntimeMethod_var);
		V_11 = L_49;
	}

IL_014f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01c5;
		}

IL_0151:
		{
			// foreach (MediationExtras mediationExtra in request.MediationExtras)
			MediationExtras_t53F0F7F90139A2D039272C9764DC880D4B3AB301 * L_50 = Enumerator_get_Current_mB8FC184CAAB4A8E56B08699B9C9894803A9EA58A_inline((Enumerator_t1B7A508A639F9A6260571CC57EAB3F3C4B96A49C *)(&V_11), /*hidden argument*/Enumerator_get_Current_mB8FC184CAAB4A8E56B08699B9C9894803A9EA58A_RuntimeMethod_var);
			V_12 = L_50;
			// IntPtr mutableDictionaryPtr = Externs.GADUCreateMutableDictionary();
			intptr_t L_51 = Externs_GADUCreateMutableDictionary_m71EE6107C3600A67D5A7C4D73C018CBAE659C23C(/*hidden argument*/NULL);
			V_13 = (intptr_t)L_51;
			// if (mutableDictionaryPtr != IntPtr.Zero)
			intptr_t L_52 = V_13;
			bool L_53 = IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61((intptr_t)L_52, (intptr_t)(0), /*hidden argument*/NULL);
			if (!L_53)
			{
				goto IL_01c5;
			}
		}

IL_016f:
		{
			// foreach (KeyValuePair<string, string> entry in mediationExtra.Extras)
			MediationExtras_t53F0F7F90139A2D039272C9764DC880D4B3AB301 * L_54 = V_12;
			NullCheck(L_54);
			Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * L_55 = MediationExtras_get_Extras_m845B48080AE808551366F9B6E363E0429EAE7869_inline(L_54, /*hidden argument*/NULL);
			NullCheck(L_55);
			Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3  L_56 = Dictionary_2_GetEnumerator_m3378B4792B81EF81397CB9D9A761BD7149BD27F5(L_55, /*hidden argument*/Dictionary_2_GetEnumerator_m3378B4792B81EF81397CB9D9A761BD7149BD27F5_RuntimeMethod_var);
			V_9 = L_56;
		}

IL_017d:
		try
		{ // begin try (depth: 2)
			{
				goto IL_019d;
			}

IL_017f:
			{
				// foreach (KeyValuePair<string, string> entry in mediationExtra.Extras)
				KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8  L_57 = Enumerator_get_Current_mBEC9B470213860581893E0F197CAAE657B8B6C69_inline((Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3 *)(&V_9), /*hidden argument*/Enumerator_get_Current_mBEC9B470213860581893E0F197CAAE657B8B6C69_RuntimeMethod_var);
				V_14 = L_57;
				// Externs.GADUMutableDictionarySetValue(
				//         mutableDictionaryPtr,
				//         entry.Key,
				//         entry.Value);
				intptr_t L_58 = V_13;
				String_t* L_59 = KeyValuePair_2_get_Key_m434E29A1251E81B5A2124466105823011C462BF2_inline((KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8 *)(&V_14), /*hidden argument*/KeyValuePair_2_get_Key_m434E29A1251E81B5A2124466105823011C462BF2_RuntimeMethod_var);
				String_t* L_60 = KeyValuePair_2_get_Value_mEAF4B15DEEAC6EB29683A5C6569F0F50B4DEBDA2_inline((KeyValuePair_2_t1A58906CCD7ED79792916B56DB716477495C85D8 *)(&V_14), /*hidden argument*/KeyValuePair_2_get_Value_mEAF4B15DEEAC6EB29683A5C6569F0F50B4DEBDA2_RuntimeMethod_var);
				Externs_GADUMutableDictionarySetValue_m1806F4834F6CCA36CD93928D50F81B097D90F60D((intptr_t)L_58, L_59, L_60, /*hidden argument*/NULL);
			}

IL_019d:
			{
				// foreach (KeyValuePair<string, string> entry in mediationExtra.Extras)
				bool L_61 = Enumerator_MoveNext_m6E6A22A8620F5A5582BB67E367BE5086D7D895A6((Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3 *)(&V_9), /*hidden argument*/Enumerator_MoveNext_m6E6A22A8620F5A5582BB67E367BE5086D7D895A6_RuntimeMethod_var);
				if (L_61)
				{
					goto IL_017f;
				}
			}

IL_01a6:
			{
				IL2CPP_LEAVE(0x1B6, FINALLY_01a8);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_01a8;
		}

FINALLY_01a8:
		{ // begin finally (depth: 2)
			Enumerator_Dispose_m16C0E963A012498CD27422B463DB327BA4C7A321((Enumerator_tEE17C0B6306B38B4D74140569F93EA8C3BDD05A3 *)(&V_9), /*hidden argument*/Enumerator_Dispose_m16C0E963A012498CD27422B463DB327BA4C7A321_RuntimeMethod_var);
			IL2CPP_END_FINALLY(424)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(424)
		{
			IL2CPP_JUMP_TBL(0x1B6, IL_01b6)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_01b6:
		{
			// Externs.GADUSetMediationExtras(
			//         requestPtr,
			//         mutableDictionaryPtr,
			//         mediationExtra.IOSMediationExtraBuilderClassName);
			intptr_t L_62 = V_0;
			intptr_t L_63 = V_13;
			MediationExtras_t53F0F7F90139A2D039272C9764DC880D4B3AB301 * L_64 = V_12;
			NullCheck(L_64);
			String_t* L_65 = VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String GoogleMobileAds.Api.Mediation.MediationExtras::get_IOSMediationExtraBuilderClassName() */, L_64);
			Externs_GADUSetMediationExtras_m89618AB332B3A9FE4FB423511B821186B35F1D2E((intptr_t)L_62, (intptr_t)L_63, L_65, /*hidden argument*/NULL);
		}

IL_01c5:
		{
			// foreach (MediationExtras mediationExtra in request.MediationExtras)
			bool L_66 = Enumerator_MoveNext_mF9F196771804D9FAE1887C1EE679DB80CF90182D((Enumerator_t1B7A508A639F9A6260571CC57EAB3F3C4B96A49C *)(&V_11), /*hidden argument*/Enumerator_MoveNext_mF9F196771804D9FAE1887C1EE679DB80CF90182D_RuntimeMethod_var);
			if (L_66)
			{
				goto IL_0151;
			}
		}

IL_01ce:
		{
			IL2CPP_LEAVE(0x1DE, FINALLY_01d0);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_01d0;
	}

FINALLY_01d0:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_mB4D942857769BA4A52C25194705752C32D86DCED((Enumerator_t1B7A508A639F9A6260571CC57EAB3F3C4B96A49C *)(&V_11), /*hidden argument*/Enumerator_Dispose_mB4D942857769BA4A52C25194705752C32D86DCED_RuntimeMethod_var);
		IL2CPP_END_FINALLY(464)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(464)
	{
		IL2CPP_JUMP_TBL(0x1DE, IL_01de)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_01de:
	{
		// Externs.GADUSetRequestAgent(requestPtr, "unity-" + AdRequest.Version);
		intptr_t L_67 = V_0;
		Externs_GADUSetRequestAgent_m27CF00F87EA0EBA6A9C3C8709E5FAD8810198647((intptr_t)L_67, _stringLiteral46F11B6B621D61F57230E331CE1A1C24927FAC27, /*hidden argument*/NULL);
		// return requestPtr;
		intptr_t L_68 = V_0;
		return (intptr_t)L_68;
	}
}
// System.Void GoogleMobileAds.iOS.Utils::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Utils__ctor_mFAEDAC0248C372B9CD40E50183B3662EB5B4A7AC (Utils_t556E7CC3B1D0D46BFFE21762D6F9E776521277D4 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Menu::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menu_Start_m26D2BE4EA9DEE5AF2FE548CF8EDDAFA0FE7379E9 (Menu_t7D066A010AF240D53C2DDA2B05DF31FD0B5F3129 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Menu_Start_m26D2BE4EA9DEE5AF2FE548CF8EDDAFA0FE7379E9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// setReward = false;
		__this->set_setReward_6((bool)0);
		// if (PlayerPrefs.GetInt("firstTime") == 0) {
		int32_t L_0 = PlayerPrefs_GetInt_m318D2B42E0FCAF179BF86D6C2353B38A58089BAD(_stringLiteral541A0FB97764D244630A052F6CA777D4285581F9, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0032;
		}
	}
	{
		// PlayerPrefs.SetInt("money", Constants.startMoney);
		IL2CPP_RUNTIME_CLASS_INIT(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_il2cpp_TypeInfo_var);
		int32_t L_1 = ((Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_StaticFields*)il2cpp_codegen_static_fields_for(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_il2cpp_TypeInfo_var))->get_startMoney_4();
		PlayerPrefs_SetInt_mBF4101DF829B4738CCC293E1C2D173AEE45EFE62(_stringLiteralC95259DE1FD719814DAEF8F1DC4BD64F9D885FF0, L_1, /*hidden argument*/NULL);
		// PlayerPrefs.SetInt("firstTime", 1);
		PlayerPrefs_SetInt_mBF4101DF829B4738CCC293E1C2D173AEE45EFE62(_stringLiteral541A0FB97764D244630A052F6CA777D4285581F9, 1, /*hidden argument*/NULL);
		// PlayerPrefs.Save();
		PlayerPrefs_Save_m6CC1FE22D4B10AC819F55802D725BE17EA2AD37B(/*hidden argument*/NULL);
	}

IL_0032:
	{
		// OpenScreen(0);
		Menu_OpenScreen_mCF89ADA1C1BA0D861053344895E94772A530EFE6(__this, 0, /*hidden argument*/NULL);
		// if (Application.internetReachability != NetworkReachability.NotReachable) {
		int32_t L_2 = Application_get_internetReachability_m8BB82882CF9E286370E4D64176CF2B9333E83F0A(/*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0046;
		}
	}
	{
		// RequestRewardVideo();
		Menu_RequestRewardVideo_mC6FC3DBAC40902860533D8BF5C78FB5BBD476944(__this, /*hidden argument*/NULL);
	}

IL_0046:
	{
		// }
		return;
	}
}
// System.Void Menu::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menu_Update_m3870FCE8E278FA9F95178BDD18B258C3F4D49BAA (Menu_t7D066A010AF240D53C2DDA2B05DF31FD0B5F3129 * __this, const RuntimeMethod* method)
{
	{
		// if (Input.GetKeyDown(KeyCode.Escape)) {
		bool L_0 = Input_GetKeyDown_mEA57896808B6F484B12CD0AEEB83390A3CFCDBDC(((int32_t)27), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		// Application.Quit();
		Application_Quit_mA005EB22CB989AC3794334754F15E1C0D2FF1C95(/*hidden argument*/NULL);
	}

IL_000e:
	{
		// }
		return;
	}
}
// System.Void Menu::RequestRewardVideo()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menu_RequestRewardVideo_mC6FC3DBAC40902860533D8BF5C78FB5BBD476944 (Menu_t7D066A010AF240D53C2DDA2B05DF31FD0B5F3129 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Menu_RequestRewardVideo_mC6FC3DBAC40902860533D8BF5C78FB5BBD476944_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * V_0 = NULL;
	{
		// rewardedAd = RewardBasedVideoAd.Instance;
		IL2CPP_RUNTIME_CLASS_INIT(RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491_il2cpp_TypeInfo_var);
		RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491 * L_0 = RewardBasedVideoAd_get_Instance_mD22F18358E8AAE1EF7D0EF41D89938E27F110413_inline(/*hidden argument*/NULL);
		__this->set_rewardedAd_5(L_0);
		// AdRequest request = new AdRequest.Builder().AddKeyword("game").TagForChildDirectedTreatment(false).Build();
		Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9 * L_1 = (Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9 *)il2cpp_codegen_object_new(Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9_il2cpp_TypeInfo_var);
		Builder__ctor_m0E264AB53F2163A32997830032FAFE5B49BD0897(L_1, /*hidden argument*/NULL);
		NullCheck(L_1);
		Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9 * L_2 = Builder_AddKeyword_m9FB2118C9FF37FF5537CDBF091A1DC4E9F87B039(L_1, _stringLiteralCDA051C901386F0E24914B0EEB92EF4E380C159D, /*hidden argument*/NULL);
		NullCheck(L_2);
		Builder_t2384CCFEB139CFC834887DBD4754905ABFE8F2E9 * L_3 = Builder_TagForChildDirectedTreatment_m673ED0659E607F8E82672E7C106DA36CD064F732(L_2, (bool)0, /*hidden argument*/NULL);
		NullCheck(L_3);
		AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * L_4 = Builder_Build_mD244B61E5DBF9D8D8E882A33977EAC5A6187BFC7(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		// if (!setReward) {
		bool L_5 = __this->get_setReward_6();
		if (L_5)
		{
			goto IL_004c;
		}
	}
	{
		// rewardedAd.OnAdRewarded += HandleRewardBasedVideoRewarded;
		RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491 * L_6 = __this->get_rewardedAd_5();
		EventHandler_1_tE84E7D22A41CECD013ED02984B817FEBA6923CC8 * L_7 = (EventHandler_1_tE84E7D22A41CECD013ED02984B817FEBA6923CC8 *)il2cpp_codegen_object_new(EventHandler_1_tE84E7D22A41CECD013ED02984B817FEBA6923CC8_il2cpp_TypeInfo_var);
		EventHandler_1__ctor_m1450748EF9416B797EDE630BEA6C50F96956282F(L_7, __this, (intptr_t)((intptr_t)Menu_HandleRewardBasedVideoRewarded_m0B9E46D6532F5EEAAD4E0F3737CE65436DA63572_RuntimeMethod_var), /*hidden argument*/EventHandler_1__ctor_m1450748EF9416B797EDE630BEA6C50F96956282F_RuntimeMethod_var);
		NullCheck(L_6);
		RewardBasedVideoAd_add_OnAdRewarded_mA6F7313150EB1E9237EF673700535A0E5DB4E200(L_6, L_7, /*hidden argument*/NULL);
		// setReward = true;
		__this->set_setReward_6((bool)1);
	}

IL_004c:
	{
		// rewardedAd.LoadAd(request, Constants.rewardedVideoID);
		RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491 * L_8 = __this->get_rewardedAd_5();
		AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_il2cpp_TypeInfo_var);
		String_t* L_10 = ((Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_StaticFields*)il2cpp_codegen_static_fields_for(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_il2cpp_TypeInfo_var))->get_rewardedVideoID_1();
		NullCheck(L_8);
		RewardBasedVideoAd_LoadAd_mDC5DA9B2E11911512038CBE1455D1066834CB1CE(L_8, L_9, L_10, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Menu::HandleRewardBasedVideoRewarded(System.Object,GoogleMobileAds.Api.Reward)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menu_HandleRewardBasedVideoRewarded_m0B9E46D6532F5EEAAD4E0F3737CE65436DA63572 (Menu_t7D066A010AF240D53C2DDA2B05DF31FD0B5F3129 * __this, RuntimeObject * ___sender0, Reward_t5656463A5A19508F4B9AF6BAB14D343F5A9EC260 * ___args1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Menu_HandleRewardBasedVideoRewarded_m0B9E46D6532F5EEAAD4E0F3737CE65436DA63572_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// PlayerPrefs.SetInt("money", PlayerPrefs.GetInt("money") + Constants.videoAdRewardMoney);
		int32_t L_0 = PlayerPrefs_GetInt_m318D2B42E0FCAF179BF86D6C2353B38A58089BAD(_stringLiteralC95259DE1FD719814DAEF8F1DC4BD64F9D885FF0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_il2cpp_TypeInfo_var);
		int32_t L_1 = ((Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_StaticFields*)il2cpp_codegen_static_fields_for(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_il2cpp_TypeInfo_var))->get_videoAdRewardMoney_3();
		PlayerPrefs_SetInt_mBF4101DF829B4738CCC293E1C2D173AEE45EFE62(_stringLiteralC95259DE1FD719814DAEF8F1DC4BD64F9D885FF0, ((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)L_1)), /*hidden argument*/NULL);
		// PlayerPrefs.Save();
		PlayerPrefs_Save_m6CC1FE22D4B10AC819F55802D725BE17EA2AD37B(/*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Menu::OpenScreen(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menu_OpenScreen_mCF89ADA1C1BA0D861053344895E94772A530EFE6 (Menu_t7D066A010AF240D53C2DDA2B05DF31FD0B5F3129 * __this, int32_t ___id0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// for (int i = 0; i < screens.Length; i++) {
		V_0 = 0;
		goto IL_002a;
	}

IL_0004:
	{
		// if (i == id) {
		int32_t L_0 = V_0;
		int32_t L_1 = ___id0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0018;
		}
	}
	{
		// screens[i].SetActive(true);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_2 = __this->get_screens_4();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_5);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_5, (bool)1, /*hidden argument*/NULL);
		// } else {
		goto IL_0026;
	}

IL_0018:
	{
		// screens[i].SetActive(false);
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_6 = __this->get_screens_4();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_9);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_9, (bool)0, /*hidden argument*/NULL);
	}

IL_0026:
	{
		// for (int i = 0; i < screens.Length; i++) {
		int32_t L_10 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_002a:
	{
		// for (int i = 0; i < screens.Length; i++) {
		int32_t L_11 = V_0;
		GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* L_12 = __this->get_screens_4();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_12)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		// }
		return;
	}
}
// System.Void Menu::PlayBtn()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menu_PlayBtn_m6E1DEECCF0637E2764399EDAB455C1CC65D2438E (Menu_t7D066A010AF240D53C2DDA2B05DF31FD0B5F3129 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Menu_PlayBtn_m6E1DEECCF0637E2764399EDAB455C1CC65D2438E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("Game");
		SceneManager_LoadScene_mFC850AC783E5EA05D6154976385DFECC251CDFB9(_stringLiteralE3E82846C32567811615378F30240185871E08E5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Menu::RateBtn()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menu_RateBtn_m88827BCC56691B4728908B482D4D7FAB1F59AB54 (Menu_t7D066A010AF240D53C2DDA2B05DF31FD0B5F3129 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Menu_RateBtn_m88827BCC56691B4728908B482D4D7FAB1F59AB54_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Application.OpenURL("https://itunes.apple.com/app/id" + Application.identifier);
		String_t* L_0 = Application_get_identifier_m02D55D452A7710BB0890E529D9E8F891AF6C2AE8(/*hidden argument*/NULL);
		String_t* L_1 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral6E0822F145777280292381332960A5EDEB0560B4, L_0, /*hidden argument*/NULL);
		Application_OpenURL_m2888DA5BDF68B1BC23E983469157783F390D7BC8(L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Menu::ShopBtn()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menu_ShopBtn_m85B3EA4AD485BF9FF002BD93FE770CBB5E74D217 (Menu_t7D066A010AF240D53C2DDA2B05DF31FD0B5F3129 * __this, const RuntimeMethod* method)
{
	{
		// OpenScreen(1);
		Menu_OpenScreen_mCF89ADA1C1BA0D861053344895E94772A530EFE6(__this, 1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Menu::MainMenuBtn()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menu_MainMenuBtn_mC83A16665D27D193705E500150FE617D73C58019 (Menu_t7D066A010AF240D53C2DDA2B05DF31FD0B5F3129 * __this, const RuntimeMethod* method)
{
	{
		// OpenScreen(0);
		Menu_OpenScreen_mCF89ADA1C1BA0D861053344895E94772A530EFE6(__this, 0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Menu::ShowVideoAd()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menu_ShowVideoAd_m3E4B2520FA144D70A553743FFB0318C83CE5B7B1 (Menu_t7D066A010AF240D53C2DDA2B05DF31FD0B5F3129 * __this, const RuntimeMethod* method)
{
	{
		// if (Application.internetReachability != NetworkReachability.NotReachable) {
		int32_t L_0 = Application_get_internetReachability_m8BB82882CF9E286370E4D64176CF2B9333E83F0A(/*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		// if (rewardedAd != null) {
		RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491 * L_1 = __this->get_rewardedAd_5();
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		// if (rewardedAd.IsLoaded()) {
		RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491 * L_2 = __this->get_rewardedAd_5();
		NullCheck(L_2);
		bool L_3 = RewardBasedVideoAd_IsLoaded_m52AE856E045477F4340E5A04F3497E51C71E3744(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		// rewardedAd.Show();
		RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491 * L_4 = __this->get_rewardedAd_5();
		NullCheck(L_4);
		RewardBasedVideoAd_Show_m6690A11AF00C03DFD435BD1D95D66F9FB67178F1(L_4, /*hidden argument*/NULL);
		// } else {
		return;
	}

IL_0028:
	{
		// RequestRewardVideo();
		Menu_RequestRewardVideo_mC6FC3DBAC40902860533D8BF5C78FB5BBD476944(__this, /*hidden argument*/NULL);
		// } else {
		return;
	}

IL_002f:
	{
		// RequestRewardVideo();
		Menu_RequestRewardVideo_mC6FC3DBAC40902860533D8BF5C78FB5BBD476944(__this, /*hidden argument*/NULL);
	}

IL_0035:
	{
		// }
		return;
	}
}
// System.Void Menu::ExitBtn()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menu_ExitBtn_mFCDBE922EFCF6352970EFED1E6455B25ADDB61CE (Menu_t7D066A010AF240D53C2DDA2B05DF31FD0B5F3129 * __this, const RuntimeMethod* method)
{
	{
		// Application.Quit();
		Application_Quit_mA005EB22CB989AC3794334754F15E1C0D2FF1C95(/*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Menu::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menu__ctor_mD372D109F6554E1F9A25291964C852C9F6BFC463 (Menu_t7D066A010AF240D53C2DDA2B05DF31FD0B5F3129 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MonoPInvokeCallbackAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoPInvokeCallbackAttribute__ctor_mB9D2688DB30B918CCD5961A8DB873F32F2205D7E (MonoPInvokeCallbackAttribute_t0B801029DE3A1B9D601C52FDBED1EC104703FB02 * __this, Type_t * ___type0, const RuntimeMethod* method)
{
	{
		// public MonoPInvokeCallbackAttribute(Type type) {}
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		// public MonoPInvokeCallbackAttribute(Type type) {}
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Purchaser::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Purchaser_Start_mCD122FAB158727577ED229C323774D10737D1835 (Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Purchaser_Start_mCD122FAB158727577ED229C323774D10737D1835_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (m_StoreController == null) {
		IL2CPP_RUNTIME_CLASS_INIT(Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = ((Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_StaticFields*)il2cpp_codegen_static_fields_for(Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_il2cpp_TypeInfo_var))->get_m_StoreController_4();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		// InitializePurchasing();
		Purchaser_InitializePurchasing_mA433D882053BB54E75A01A2919EA1989C9470F9F(__this, /*hidden argument*/NULL);
	}

IL_000d:
	{
		// }
		return;
	}
}
// System.Void Purchaser::InitializePurchasing()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Purchaser_InitializePurchasing_mA433D882053BB54E75A01A2919EA1989C9470F9F (Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Purchaser_InitializePurchasing_mA433D882053BB54E75A01A2919EA1989C9470F9F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE * V_0 = NULL;
	{
		// if (IsInitialized()) {
		bool L_0 = Purchaser_IsInitialized_m272B7D0B6DB8CDEB87223E1224F013BB3533C890(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
		IL2CPP_RUNTIME_CLASS_INIT(StandardPurchasingModule_tAB117B14746822C4AF812B66148122909DD53847_il2cpp_TypeInfo_var);
		StandardPurchasingModule_tAB117B14746822C4AF812B66148122909DD53847 * L_1 = StandardPurchasingModule_Instance_mB18020DD1E1F75B5841D60DC58F9BB3D96F79CE5(/*hidden argument*/NULL);
		IPurchasingModuleU5BU5D_tF106F307EE194D8B47140E160F5BC38EDFFF9384* L_2 = Array_Empty_TisIPurchasingModule_tBA2577ED1E963CA0ABA1DFA359A020982DE0B1BF_m6A5F4EE32F39C664A1A279F15D55E86D8DA4D93E_inline(/*hidden argument*/Array_Empty_TisIPurchasingModule_tBA2577ED1E963CA0ABA1DFA359A020982DE0B1BF_m6A5F4EE32F39C664A1A279F15D55E86D8DA4D93E_RuntimeMethod_var);
		ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE * L_3 = ConfigurationBuilder_Instance_m8F4EA19F3C0FF462CCF219D0A019BEADA05E7DA2(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		// builder.AddProduct(Constants.purchaseMoney1, ProductType.Consumable);
		ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_il2cpp_TypeInfo_var);
		String_t* L_5 = ((Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_StaticFields*)il2cpp_codegen_static_fields_for(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_il2cpp_TypeInfo_var))->get_purchaseMoney1_5();
		NullCheck(L_4);
		ConfigurationBuilder_AddProduct_m3E95B8F467E2A9C5594E55B04FE33A72D0C66DC0(L_4, L_5, 0, /*hidden argument*/NULL);
		// builder.AddProduct(Constants.purchaseMoney2, ProductType.Consumable);
		ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE * L_6 = V_0;
		String_t* L_7 = ((Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_StaticFields*)il2cpp_codegen_static_fields_for(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_il2cpp_TypeInfo_var))->get_purchaseMoney2_6();
		NullCheck(L_6);
		ConfigurationBuilder_AddProduct_m3E95B8F467E2A9C5594E55B04FE33A72D0C66DC0(L_6, L_7, 0, /*hidden argument*/NULL);
		// builder.AddProduct(Constants.purchaseMoney3, ProductType.Consumable);
		ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE * L_8 = V_0;
		String_t* L_9 = ((Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_StaticFields*)il2cpp_codegen_static_fields_for(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_il2cpp_TypeInfo_var))->get_purchaseMoney3_7();
		NullCheck(L_8);
		ConfigurationBuilder_AddProduct_m3E95B8F467E2A9C5594E55B04FE33A72D0C66DC0(L_8, L_9, 0, /*hidden argument*/NULL);
		// builder.AddProduct(Constants.purchaseMoney4, ProductType.Consumable);
		ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE * L_10 = V_0;
		String_t* L_11 = ((Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_StaticFields*)il2cpp_codegen_static_fields_for(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_il2cpp_TypeInfo_var))->get_purchaseMoney4_8();
		NullCheck(L_10);
		ConfigurationBuilder_AddProduct_m3E95B8F467E2A9C5594E55B04FE33A72D0C66DC0(L_10, L_11, 0, /*hidden argument*/NULL);
		// UnityPurchasing.Initialize(this, builder);
		ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE * L_12 = V_0;
		UnityPurchasing_Initialize_m1BEFCE608B4D495EEB6CF346E51AB8CFB84CBFDC(__this, L_12, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Boolean Purchaser::IsInitialized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Purchaser_IsInitialized_m272B7D0B6DB8CDEB87223E1224F013BB3533C890 (Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Purchaser_IsInitialized_m272B7D0B6DB8CDEB87223E1224F013BB3533C890_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return m_StoreController != null && m_StoreExtensionProvider != null;
		IL2CPP_RUNTIME_CLASS_INIT(Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = ((Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_StaticFields*)il2cpp_codegen_static_fields_for(Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_il2cpp_TypeInfo_var))->get_m_StoreController_4();
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_il2cpp_TypeInfo_var);
		RuntimeObject* L_1 = ((Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_StaticFields*)il2cpp_codegen_static_fields_for(Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_il2cpp_TypeInfo_var))->get_m_StoreExtensionProvider_5();
		return (bool)((!(((RuntimeObject*)(RuntimeObject*)L_1) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
	}

IL_0010:
	{
		return (bool)0;
	}
}
// System.Void Purchaser::BuyConsumable(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Purchaser_BuyConsumable_mFD6E311F8C75EF0A2589C9282FC91463BE256FDD (Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037 * __this, int32_t ___id0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Purchaser_BuyConsumable_mFD6E311F8C75EF0A2589C9282FC91463BE256FDD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (id == 0) {
		int32_t L_0 = ___id0;
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		// BuyProductID(Constants.purchaseMoney1);
		IL2CPP_RUNTIME_CLASS_INIT(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_il2cpp_TypeInfo_var);
		String_t* L_1 = ((Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_StaticFields*)il2cpp_codegen_static_fields_for(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_il2cpp_TypeInfo_var))->get_purchaseMoney1_5();
		Purchaser_BuyProductID_mBCDAC5023D5830F87D3E0116AE9DC5447065385E(__this, L_1, /*hidden argument*/NULL);
		// } else if (id == 1) {
		return;
	}

IL_000f:
	{
		// } else if (id == 1) {
		int32_t L_2 = ___id0;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_001f;
		}
	}
	{
		// BuyProductID(Constants.purchaseMoney2);
		IL2CPP_RUNTIME_CLASS_INIT(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_il2cpp_TypeInfo_var);
		String_t* L_3 = ((Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_StaticFields*)il2cpp_codegen_static_fields_for(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_il2cpp_TypeInfo_var))->get_purchaseMoney2_6();
		Purchaser_BuyProductID_mBCDAC5023D5830F87D3E0116AE9DC5447065385E(__this, L_3, /*hidden argument*/NULL);
		// } else if (id == 2) {
		return;
	}

IL_001f:
	{
		// } else if (id == 2) {
		int32_t L_4 = ___id0;
		if ((!(((uint32_t)L_4) == ((uint32_t)2))))
		{
			goto IL_002f;
		}
	}
	{
		// BuyProductID(Constants.purchaseMoney3);
		IL2CPP_RUNTIME_CLASS_INIT(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_il2cpp_TypeInfo_var);
		String_t* L_5 = ((Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_StaticFields*)il2cpp_codegen_static_fields_for(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_il2cpp_TypeInfo_var))->get_purchaseMoney3_7();
		Purchaser_BuyProductID_mBCDAC5023D5830F87D3E0116AE9DC5447065385E(__this, L_5, /*hidden argument*/NULL);
		// } else if (id == 3) {
		return;
	}

IL_002f:
	{
		// } else if (id == 3) {
		int32_t L_6 = ___id0;
		if ((!(((uint32_t)L_6) == ((uint32_t)3))))
		{
			goto IL_003e;
		}
	}
	{
		// BuyProductID(Constants.purchaseMoney4);
		IL2CPP_RUNTIME_CLASS_INIT(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_il2cpp_TypeInfo_var);
		String_t* L_7 = ((Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_StaticFields*)il2cpp_codegen_static_fields_for(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_il2cpp_TypeInfo_var))->get_purchaseMoney4_8();
		Purchaser_BuyProductID_mBCDAC5023D5830F87D3E0116AE9DC5447065385E(__this, L_7, /*hidden argument*/NULL);
	}

IL_003e:
	{
		// }
		return;
	}
}
// System.Void Purchaser::BuyProductID(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Purchaser_BuyProductID_mBCDAC5023D5830F87D3E0116AE9DC5447065385E (Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037 * __this, String_t* ___productId0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Purchaser_BuyProductID_mBCDAC5023D5830F87D3E0116AE9DC5447065385E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * V_0 = NULL;
	{
		// if (IsInitialized()) {
		bool L_0 = Purchaser_IsInitialized_m272B7D0B6DB8CDEB87223E1224F013BB3533C890(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0055;
		}
	}
	{
		// Product product = m_StoreController.products.WithID(productId);
		IL2CPP_RUNTIME_CLASS_INIT(Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_il2cpp_TypeInfo_var);
		RuntimeObject* L_1 = ((Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_StaticFields*)il2cpp_codegen_static_fields_for(Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_il2cpp_TypeInfo_var))->get_m_StoreController_4();
		NullCheck(L_1);
		ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07 * L_2 = InterfaceFuncInvoker0< ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t6A30FF7F81804FCC0082AB9DF2CE86BCFB91DF67_il2cpp_TypeInfo_var, L_1);
		String_t* L_3 = ___productId0;
		NullCheck(L_2);
		Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * L_4 = ProductCollection_WithID_m7B5AB4C1B658271EC30742DD07D53E9043A816A4(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		// if (product != null && product.availableToPurchase) {
		Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * L_5 = V_0;
		if (!L_5)
		{
			goto IL_004a;
		}
	}
	{
		Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = Product_get_availableToPurchase_mA4F6F87AA8B83915F1B5E122A10F0CBAFBAE196C_inline(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004a;
		}
	}
	{
		// Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
		Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * L_8 = V_0;
		NullCheck(L_8);
		ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * L_9 = Product_get_definition_mFA1889844E23D37A700C2EA6659EE3BBF1E17C58_inline(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_10 = ProductDefinition_get_id_mD5C2A9C1016245C99EB67B74DFC32ECDC8512468_inline(L_9, /*hidden argument*/NULL);
		String_t* L_11 = String_Format_m0ACDD8B34764E4040AED0B3EEB753567E4576BFA(_stringLiteral9339764CAB44BA34A23F1B0C57220F7617877495, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_11, /*hidden argument*/NULL);
		// m_StoreController.InitiatePurchase(product);
		IL2CPP_RUNTIME_CLASS_INIT(Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_il2cpp_TypeInfo_var);
		RuntimeObject* L_12 = ((Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_StaticFields*)il2cpp_codegen_static_fields_for(Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_il2cpp_TypeInfo_var))->get_m_StoreController_4();
		Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * L_13 = V_0;
		NullCheck(L_12);
		InterfaceActionInvoker1< Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * >::Invoke(2 /* System.Void UnityEngine.Purchasing.IStoreController::InitiatePurchase(UnityEngine.Purchasing.Product) */, IStoreController_t6A30FF7F81804FCC0082AB9DF2CE86BCFB91DF67_il2cpp_TypeInfo_var, L_12, L_13);
		// } else {
		return;
	}

IL_004a:
	{
		// Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(_stringLiteral113ACB59CC385DBB3293CAFA0C8B52A644B633DF, /*hidden argument*/NULL);
		// } else {
		return;
	}

IL_0055:
	{
		// Debug.Log("BuyProductID FAIL. Not initialized.");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(_stringLiteral8050389710BA17EF56B61EFEC3D606A234949412, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Purchaser::RestorePurchases()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Purchaser_RestorePurchases_m24A1E9C7782A272240B01EB09EBC8828ED550846 (Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Purchaser_RestorePurchases_m24A1E9C7782A272240B01EB09EBC8828ED550846_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * G_B6_0 = NULL;
	RuntimeObject* G_B6_1 = NULL;
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * G_B5_0 = NULL;
	RuntimeObject* G_B5_1 = NULL;
	{
		// if (!IsInitialized()) {
		bool L_0 = Purchaser_IsInitialized_m272B7D0B6DB8CDEB87223E1224F013BB3533C890(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		// Debug.Log("RestorePurchases FAIL. Not initialized.");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(_stringLiteral735D638870FD9A0CFDCEE74317CFC691B237F1CC, /*hidden argument*/NULL);
		// return;
		return;
	}

IL_0013:
	{
		// if (Application.platform == RuntimePlatform.IPhonePlayer ||
		//     Application.platform == RuntimePlatform.OSXPlayer) {
		int32_t L_1 = Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672(/*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)8)))
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_2 = Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_005c;
		}
	}

IL_0023:
	{
		// Debug.Log("RestorePurchases started ...");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(_stringLiteral6672E4B2C5B31990F57E7C47B6E501B6DA4AE7B5, /*hidden argument*/NULL);
		// var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
		IL2CPP_RUNTIME_CLASS_INIT(Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_il2cpp_TypeInfo_var);
		RuntimeObject* L_3 = ((Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_StaticFields*)il2cpp_codegen_static_fields_for(Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_il2cpp_TypeInfo_var))->get_m_StoreExtensionProvider_5();
		NullCheck(L_3);
		RuntimeObject* L_4 = GenericInterfaceFuncInvoker0< RuntimeObject* >::Invoke(IExtensionProvider_GetExtension_TisIAppleExtensions_tB86ACF573E6F6E038B547C551A0459A693DDE17C_mA70A998DA58769AC90A2E909C671E14F966E7E4F_RuntimeMethod_var, L_3);
		// apple.RestoreTransactions((result) => {
		//     Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
		// });
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t4CD03EBC853C316BC6B34DD2FD63D9BDEE3F6BC2_il2cpp_TypeInfo_var);
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_5 = ((U3CU3Ec_t4CD03EBC853C316BC6B34DD2FD63D9BDEE3F6BC2_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t4CD03EBC853C316BC6B34DD2FD63D9BDEE3F6BC2_il2cpp_TypeInfo_var))->get_U3CU3E9__11_0_1();
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_6 = L_5;
		G_B5_0 = L_6;
		G_B5_1 = L_4;
		if (L_6)
		{
			G_B6_0 = L_6;
			G_B6_1 = L_4;
			goto IL_0056;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t4CD03EBC853C316BC6B34DD2FD63D9BDEE3F6BC2_il2cpp_TypeInfo_var);
		U3CU3Ec_t4CD03EBC853C316BC6B34DD2FD63D9BDEE3F6BC2 * L_7 = ((U3CU3Ec_t4CD03EBC853C316BC6B34DD2FD63D9BDEE3F6BC2_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t4CD03EBC853C316BC6B34DD2FD63D9BDEE3F6BC2_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_8 = (Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD *)il2cpp_codegen_object_new(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD_il2cpp_TypeInfo_var);
		Action_1__ctor_mFEB000BCBDAB6DE953B2B50CD113641DCF601890(L_8, L_7, (intptr_t)((intptr_t)U3CU3Ec_U3CRestorePurchasesU3Eb__11_0_m997253913D06FEB1A817C89E993EC7970359E20E_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mFEB000BCBDAB6DE953B2B50CD113641DCF601890_RuntimeMethod_var);
		Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * L_9 = L_8;
		((U3CU3Ec_t4CD03EBC853C316BC6B34DD2FD63D9BDEE3F6BC2_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t4CD03EBC853C316BC6B34DD2FD63D9BDEE3F6BC2_il2cpp_TypeInfo_var))->set_U3CU3E9__11_0_1(L_9);
		G_B6_0 = L_9;
		G_B6_1 = G_B5_1;
	}

IL_0056:
	{
		NullCheck(G_B6_1);
		InterfaceActionInvoker1< Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * >::Invoke(0 /* System.Void UnityEngine.Purchasing.IAppleExtensions::RestoreTransactions(System.Action`1<System.Boolean>) */, IAppleExtensions_tB86ACF573E6F6E038B547C551A0459A693DDE17C_il2cpp_TypeInfo_var, G_B6_1, G_B6_0);
		// } else {
		return;
	}

IL_005c:
	{
		// Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
		int32_t L_10 = Application_get_platform_m6AFFFF3B077F4D5CA1F71CF14ABA86A83FC71672(/*hidden argument*/NULL);
		int32_t L_11 = L_10;
		RuntimeObject * L_12 = Box(RuntimePlatform_tD5F5737C1BBBCBB115EB104DF2B7876387E80132_il2cpp_TypeInfo_var, &L_11);
		String_t* L_13 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteral694BCD90F42D64B37F587CB128C57C1C0669E065, L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_13, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Purchaser::OnInitialized(UnityEngine.Purchasing.IStoreController,UnityEngine.Purchasing.IExtensionProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Purchaser_OnInitialized_m2C5D997E02074CA968ACF89D2EA8849616E2AE8A (Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037 * __this, RuntimeObject* ___controller0, RuntimeObject* ___extensions1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Purchaser_OnInitialized_m2C5D997E02074CA968ACF89D2EA8849616E2AE8A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log("OnInitialized: PASS");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(_stringLiteral728E0F853BE209BCB8D5467D302BF96125A98E5D, /*hidden argument*/NULL);
		// m_StoreController = controller;
		RuntimeObject* L_0 = ___controller0;
		IL2CPP_RUNTIME_CLASS_INIT(Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_il2cpp_TypeInfo_var);
		((Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_StaticFields*)il2cpp_codegen_static_fields_for(Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_il2cpp_TypeInfo_var))->set_m_StoreController_4(L_0);
		// m_StoreExtensionProvider = extensions;
		RuntimeObject* L_1 = ___extensions1;
		((Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_StaticFields*)il2cpp_codegen_static_fields_for(Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_il2cpp_TypeInfo_var))->set_m_StoreExtensionProvider_5(L_1);
		// }
		return;
	}
}
// System.Void Purchaser::OnInitializeFailed(UnityEngine.Purchasing.InitializationFailureReason)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Purchaser_OnInitializeFailed_mD26D07D1E337C7DFCA40D1D65DA7F4F85BF449DE (Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037 * __this, int32_t ___error0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Purchaser_OnInitializeFailed_mD26D07D1E337C7DFCA40D1D65DA7F4F85BF449DE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
		int32_t L_0 = ___error0;
		int32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(InitializationFailureReason_t5A6284D67FA09D301793E67D8B7003299F4D3062_il2cpp_TypeInfo_var, &L_1);
		String_t* L_3 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteralAC6876A1732FDFAF2A6BFDBB9A20AC4C8FADAF39, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// UnityEngine.Purchasing.PurchaseProcessingResult Purchaser::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Purchaser_ProcessPurchase_m9A0B2A4762E2AF7D006F42F231BB676CDDCC6238 (Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037 * __this, PurchaseEventArgs_tF6E04BFD5492F5F57309FFBB2415EB26E5B88C04 * ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Purchaser_ProcessPurchase_m9A0B2A4762E2AF7D006F42F231BB676CDDCC6238_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (string.Equals(args.purchasedProduct.definition.id, Constants.purchaseMoney1, System.StringComparison.Ordinal)) {
		PurchaseEventArgs_tF6E04BFD5492F5F57309FFBB2415EB26E5B88C04 * L_0 = ___args0;
		NullCheck(L_0);
		Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * L_1 = PurchaseEventArgs_get_purchasedProduct_mB26BCEF9CD45B84DBF55A5C6FC50AF611B6AD6A2_inline(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * L_2 = Product_get_definition_mFA1889844E23D37A700C2EA6659EE3BBF1E17C58_inline(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = ProductDefinition_get_id_mD5C2A9C1016245C99EB67B74DFC32ECDC8512468_inline(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_il2cpp_TypeInfo_var);
		String_t* L_4 = ((Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_StaticFields*)il2cpp_codegen_static_fields_for(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_il2cpp_TypeInfo_var))->get_purchaseMoney1_5();
		bool L_5 = String_Equals_m1A3302D7214F75FB06302101934BF3EE9282AA43(L_3, L_4, 4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0060;
		}
	}
	{
		// Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
		PurchaseEventArgs_tF6E04BFD5492F5F57309FFBB2415EB26E5B88C04 * L_6 = ___args0;
		NullCheck(L_6);
		Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * L_7 = PurchaseEventArgs_get_purchasedProduct_mB26BCEF9CD45B84DBF55A5C6FC50AF611B6AD6A2_inline(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * L_8 = Product_get_definition_mFA1889844E23D37A700C2EA6659EE3BBF1E17C58_inline(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		String_t* L_9 = ProductDefinition_get_id_mD5C2A9C1016245C99EB67B74DFC32ECDC8512468_inline(L_8, /*hidden argument*/NULL);
		String_t* L_10 = String_Format_m0ACDD8B34764E4040AED0B3EEB753567E4576BFA(_stringLiteral79134CE1905549F57C45F0675918958B49A8627C, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_10, /*hidden argument*/NULL);
		// PlayerPrefs.SetInt("money", PlayerPrefs.GetInt("money") + 1000);
		int32_t L_11 = PlayerPrefs_GetInt_m318D2B42E0FCAF179BF86D6C2353B38A58089BAD(_stringLiteralC95259DE1FD719814DAEF8F1DC4BD64F9D885FF0, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_mBF4101DF829B4738CCC293E1C2D173AEE45EFE62(_stringLiteralC95259DE1FD719814DAEF8F1DC4BD64F9D885FF0, ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)((int32_t)1000))), /*hidden argument*/NULL);
		// PlayerPrefs.Save();
		PlayerPrefs_Save_m6CC1FE22D4B10AC819F55802D725BE17EA2AD37B(/*hidden argument*/NULL);
		// } else if (string.Equals(args.purchasedProduct.definition.id, Constants.purchaseMoney2, System.StringComparison.Ordinal)) {
		goto IL_0199;
	}

IL_0060:
	{
		// } else if (string.Equals(args.purchasedProduct.definition.id, Constants.purchaseMoney2, System.StringComparison.Ordinal)) {
		PurchaseEventArgs_tF6E04BFD5492F5F57309FFBB2415EB26E5B88C04 * L_12 = ___args0;
		NullCheck(L_12);
		Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * L_13 = PurchaseEventArgs_get_purchasedProduct_mB26BCEF9CD45B84DBF55A5C6FC50AF611B6AD6A2_inline(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * L_14 = Product_get_definition_mFA1889844E23D37A700C2EA6659EE3BBF1E17C58_inline(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		String_t* L_15 = ProductDefinition_get_id_mD5C2A9C1016245C99EB67B74DFC32ECDC8512468_inline(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_il2cpp_TypeInfo_var);
		String_t* L_16 = ((Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_StaticFields*)il2cpp_codegen_static_fields_for(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_il2cpp_TypeInfo_var))->get_purchaseMoney2_6();
		bool L_17 = String_Equals_m1A3302D7214F75FB06302101934BF3EE9282AA43(L_15, L_16, 4, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00c0;
		}
	}
	{
		// Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
		PurchaseEventArgs_tF6E04BFD5492F5F57309FFBB2415EB26E5B88C04 * L_18 = ___args0;
		NullCheck(L_18);
		Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * L_19 = PurchaseEventArgs_get_purchasedProduct_mB26BCEF9CD45B84DBF55A5C6FC50AF611B6AD6A2_inline(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * L_20 = Product_get_definition_mFA1889844E23D37A700C2EA6659EE3BBF1E17C58_inline(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		String_t* L_21 = ProductDefinition_get_id_mD5C2A9C1016245C99EB67B74DFC32ECDC8512468_inline(L_20, /*hidden argument*/NULL);
		String_t* L_22 = String_Format_m0ACDD8B34764E4040AED0B3EEB753567E4576BFA(_stringLiteral79134CE1905549F57C45F0675918958B49A8627C, L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_22, /*hidden argument*/NULL);
		// PlayerPrefs.SetInt("money", PlayerPrefs.GetInt("money") + 5000);
		int32_t L_23 = PlayerPrefs_GetInt_m318D2B42E0FCAF179BF86D6C2353B38A58089BAD(_stringLiteralC95259DE1FD719814DAEF8F1DC4BD64F9D885FF0, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_mBF4101DF829B4738CCC293E1C2D173AEE45EFE62(_stringLiteralC95259DE1FD719814DAEF8F1DC4BD64F9D885FF0, ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)((int32_t)5000))), /*hidden argument*/NULL);
		// PlayerPrefs.Save();
		PlayerPrefs_Save_m6CC1FE22D4B10AC819F55802D725BE17EA2AD37B(/*hidden argument*/NULL);
		// } else if (string.Equals(args.purchasedProduct.definition.id, Constants.purchaseMoney3, System.StringComparison.Ordinal)) {
		goto IL_0199;
	}

IL_00c0:
	{
		// } else if (string.Equals(args.purchasedProduct.definition.id, Constants.purchaseMoney3, System.StringComparison.Ordinal)) {
		PurchaseEventArgs_tF6E04BFD5492F5F57309FFBB2415EB26E5B88C04 * L_24 = ___args0;
		NullCheck(L_24);
		Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * L_25 = PurchaseEventArgs_get_purchasedProduct_mB26BCEF9CD45B84DBF55A5C6FC50AF611B6AD6A2_inline(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * L_26 = Product_get_definition_mFA1889844E23D37A700C2EA6659EE3BBF1E17C58_inline(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		String_t* L_27 = ProductDefinition_get_id_mD5C2A9C1016245C99EB67B74DFC32ECDC8512468_inline(L_26, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_il2cpp_TypeInfo_var);
		String_t* L_28 = ((Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_StaticFields*)il2cpp_codegen_static_fields_for(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_il2cpp_TypeInfo_var))->get_purchaseMoney3_7();
		bool L_29 = String_Equals_m1A3302D7214F75FB06302101934BF3EE9282AA43(L_27, L_28, 4, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_011d;
		}
	}
	{
		// Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
		PurchaseEventArgs_tF6E04BFD5492F5F57309FFBB2415EB26E5B88C04 * L_30 = ___args0;
		NullCheck(L_30);
		Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * L_31 = PurchaseEventArgs_get_purchasedProduct_mB26BCEF9CD45B84DBF55A5C6FC50AF611B6AD6A2_inline(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * L_32 = Product_get_definition_mFA1889844E23D37A700C2EA6659EE3BBF1E17C58_inline(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		String_t* L_33 = ProductDefinition_get_id_mD5C2A9C1016245C99EB67B74DFC32ECDC8512468_inline(L_32, /*hidden argument*/NULL);
		String_t* L_34 = String_Format_m0ACDD8B34764E4040AED0B3EEB753567E4576BFA(_stringLiteral79134CE1905549F57C45F0675918958B49A8627C, L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_34, /*hidden argument*/NULL);
		// PlayerPrefs.SetInt("money", PlayerPrefs.GetInt("money") + 10000);
		int32_t L_35 = PlayerPrefs_GetInt_m318D2B42E0FCAF179BF86D6C2353B38A58089BAD(_stringLiteralC95259DE1FD719814DAEF8F1DC4BD64F9D885FF0, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_mBF4101DF829B4738CCC293E1C2D173AEE45EFE62(_stringLiteralC95259DE1FD719814DAEF8F1DC4BD64F9D885FF0, ((int32_t)il2cpp_codegen_add((int32_t)L_35, (int32_t)((int32_t)10000))), /*hidden argument*/NULL);
		// PlayerPrefs.Save();
		PlayerPrefs_Save_m6CC1FE22D4B10AC819F55802D725BE17EA2AD37B(/*hidden argument*/NULL);
		// } else if (string.Equals(args.purchasedProduct.definition.id, Constants.purchaseMoney4, System.StringComparison.Ordinal)) {
		goto IL_0199;
	}

IL_011d:
	{
		// } else if (string.Equals(args.purchasedProduct.definition.id, Constants.purchaseMoney4, System.StringComparison.Ordinal)) {
		PurchaseEventArgs_tF6E04BFD5492F5F57309FFBB2415EB26E5B88C04 * L_36 = ___args0;
		NullCheck(L_36);
		Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * L_37 = PurchaseEventArgs_get_purchasedProduct_mB26BCEF9CD45B84DBF55A5C6FC50AF611B6AD6A2_inline(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * L_38 = Product_get_definition_mFA1889844E23D37A700C2EA6659EE3BBF1E17C58_inline(L_37, /*hidden argument*/NULL);
		NullCheck(L_38);
		String_t* L_39 = ProductDefinition_get_id_mD5C2A9C1016245C99EB67B74DFC32ECDC8512468_inline(L_38, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_il2cpp_TypeInfo_var);
		String_t* L_40 = ((Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_StaticFields*)il2cpp_codegen_static_fields_for(Constants_tB3B5C46616B006B04865C5DB3576677AD7404C3C_il2cpp_TypeInfo_var))->get_purchaseMoney4_8();
		bool L_41 = String_Equals_m1A3302D7214F75FB06302101934BF3EE9282AA43(L_39, L_40, 4, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_017a;
		}
	}
	{
		// Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
		PurchaseEventArgs_tF6E04BFD5492F5F57309FFBB2415EB26E5B88C04 * L_42 = ___args0;
		NullCheck(L_42);
		Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * L_43 = PurchaseEventArgs_get_purchasedProduct_mB26BCEF9CD45B84DBF55A5C6FC50AF611B6AD6A2_inline(L_42, /*hidden argument*/NULL);
		NullCheck(L_43);
		ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * L_44 = Product_get_definition_mFA1889844E23D37A700C2EA6659EE3BBF1E17C58_inline(L_43, /*hidden argument*/NULL);
		NullCheck(L_44);
		String_t* L_45 = ProductDefinition_get_id_mD5C2A9C1016245C99EB67B74DFC32ECDC8512468_inline(L_44, /*hidden argument*/NULL);
		String_t* L_46 = String_Format_m0ACDD8B34764E4040AED0B3EEB753567E4576BFA(_stringLiteral79134CE1905549F57C45F0675918958B49A8627C, L_45, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_46, /*hidden argument*/NULL);
		// PlayerPrefs.SetInt("money", PlayerPrefs.GetInt("money") + 25000);
		int32_t L_47 = PlayerPrefs_GetInt_m318D2B42E0FCAF179BF86D6C2353B38A58089BAD(_stringLiteralC95259DE1FD719814DAEF8F1DC4BD64F9D885FF0, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_mBF4101DF829B4738CCC293E1C2D173AEE45EFE62(_stringLiteralC95259DE1FD719814DAEF8F1DC4BD64F9D885FF0, ((int32_t)il2cpp_codegen_add((int32_t)L_47, (int32_t)((int32_t)25000))), /*hidden argument*/NULL);
		// PlayerPrefs.Save();
		PlayerPrefs_Save_m6CC1FE22D4B10AC819F55802D725BE17EA2AD37B(/*hidden argument*/NULL);
		// } else {
		goto IL_0199;
	}

IL_017a:
	{
		// Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
		PurchaseEventArgs_tF6E04BFD5492F5F57309FFBB2415EB26E5B88C04 * L_48 = ___args0;
		NullCheck(L_48);
		Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * L_49 = PurchaseEventArgs_get_purchasedProduct_mB26BCEF9CD45B84DBF55A5C6FC50AF611B6AD6A2_inline(L_48, /*hidden argument*/NULL);
		NullCheck(L_49);
		ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * L_50 = Product_get_definition_mFA1889844E23D37A700C2EA6659EE3BBF1E17C58_inline(L_49, /*hidden argument*/NULL);
		NullCheck(L_50);
		String_t* L_51 = ProductDefinition_get_id_mD5C2A9C1016245C99EB67B74DFC32ECDC8512468_inline(L_50, /*hidden argument*/NULL);
		String_t* L_52 = String_Format_m0ACDD8B34764E4040AED0B3EEB753567E4576BFA(_stringLiteral18CFCD24381BFC95C94A162DFB57B289E5A36585, L_51, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_52, /*hidden argument*/NULL);
	}

IL_0199:
	{
		// return PurchaseProcessingResult.Complete;
		return (int32_t)(0);
	}
}
// System.Void Purchaser::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Purchaser_OnPurchaseFailed_m4A93D014B730F86486878D3A2EEE69C949EB07F9 (Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037 * __this, Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * ___product0, int32_t ___failureReason1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Purchaser_OnPurchaseFailed_m4A93D014B730F86486878D3A2EEE69C949EB07F9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
		Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * L_0 = ___product0;
		NullCheck(L_0);
		ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * L_1 = Product_get_definition_mFA1889844E23D37A700C2EA6659EE3BBF1E17C58_inline(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = ProductDefinition_get_storeSpecificId_m544E2354C4C9049DE5C24D07A347669A05B241D0_inline(L_1, /*hidden argument*/NULL);
		int32_t L_3 = ___failureReason1;
		int32_t L_4 = L_3;
		RuntimeObject * L_5 = Box(PurchaseFailureReason_t42EC65C1103B27F82198DC42C8D96C1A14ED7432_il2cpp_TypeInfo_var, &L_4);
		String_t* L_6 = String_Format_m19325298DBC61AAC016C16F7B3CF97A8A3DEA34A(_stringLiteral5387EB5DAB5EC6B269E296E7EE0C7033A094E744, L_2, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_6, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Purchaser::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Purchaser__ctor_m526162775A909925C95DBABA1943472F883DFDDA (Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Purchaser::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Purchaser__cctor_mADAAF4DC1AEEC46CD5D7A737ED6896BF8BC7C6E8 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Purchaser__cctor_mADAAF4DC1AEEC46CD5D7A737ED6896BF8BC7C6E8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static string kProductIDConsumable1 = "m1000";
		((Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_StaticFields*)il2cpp_codegen_static_fields_for(Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_il2cpp_TypeInfo_var))->set_kProductIDConsumable1_6(_stringLiteral361EE03281C3856B6F0042183EFFB1584B30D7AC);
		// public static string kProductIDConsumable2 = "m5000";
		((Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_StaticFields*)il2cpp_codegen_static_fields_for(Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_il2cpp_TypeInfo_var))->set_kProductIDConsumable2_7(_stringLiteralA8CD2D6BCE16F5AA74C61C9FD527B4146958B48D);
		// public static string kProductIDConsumable3 = "m10000";
		((Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_StaticFields*)il2cpp_codegen_static_fields_for(Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_il2cpp_TypeInfo_var))->set_kProductIDConsumable3_8(_stringLiteralB76F0072492A26196E1938F3067FDB84969270DF);
		// public static string kProductIDConsumable4 = "m25000";
		((Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_StaticFields*)il2cpp_codegen_static_fields_for(Purchaser_t429CA330F23309AFAC4AF48EB96C9DD2BD3AF037_il2cpp_TypeInfo_var))->set_kProductIDConsumable4_9(_stringLiteral4FA3A1827DAD202EA37346A8E2899BD01A256858);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Purchaser_<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_mBACD32049FC5669777FA1E6521D96C92CD685940 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__cctor_mBACD32049FC5669777FA1E6521D96C92CD685940_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t4CD03EBC853C316BC6B34DD2FD63D9BDEE3F6BC2 * L_0 = (U3CU3Ec_t4CD03EBC853C316BC6B34DD2FD63D9BDEE3F6BC2 *)il2cpp_codegen_object_new(U3CU3Ec_t4CD03EBC853C316BC6B34DD2FD63D9BDEE3F6BC2_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_mFFFF6E872083F954C08D0CB94E2B71E42C0B465D(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t4CD03EBC853C316BC6B34DD2FD63D9BDEE3F6BC2_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t4CD03EBC853C316BC6B34DD2FD63D9BDEE3F6BC2_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Purchaser_<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mFFFF6E872083F954C08D0CB94E2B71E42C0B465D (U3CU3Ec_t4CD03EBC853C316BC6B34DD2FD63D9BDEE3F6BC2 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Purchaser_<>c::<RestorePurchases>b__11_0(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CRestorePurchasesU3Eb__11_0_m997253913D06FEB1A817C89E993EC7970359E20E (U3CU3Ec_t4CD03EBC853C316BC6B34DD2FD63D9BDEE3F6BC2 * __this, bool ___result0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CRestorePurchasesU3Eb__11_0_m997253913D06FEB1A817C89E993EC7970359E20E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
		String_t* L_0 = Boolean_ToString_m62D1EFD5F6D5F6B6AF0D14A07BF5741C94413301((bool*)(&___result0), /*hidden argument*/NULL);
		String_t* L_1 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(_stringLiteral33366CF6F9F373698DE577F581C8F88B40982A02, L_0, _stringLiteralB9C36642D40977DF4D4E7B56EEAB4CC19FC3F3EF, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_1, /*hidden argument*/NULL);
		// });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61 * AdRequest_get_Keywords_mA1D66061D1F93CF256DDD5AAE938C629104F11AA_inline (AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * __this, const RuntimeMethod* method)
{
	{
		// public HashSet<string> Keywords { get; private set; }
		HashSet_1_t7ED30EEB5279B4F7B38D343D0AF490E2296ACF61 * L_0 = __this->get_U3CKeywordsU3Ek__BackingField_3();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * AdRequest_get_TestDevices_m02EC537E8CEEC7E695A6055B8CB1E1889F164617_inline (AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * __this, const RuntimeMethod* method)
{
	{
		// public List<string> TestDevices { get; private set; }
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_0 = __this->get_U3CTestDevicesU3Ek__BackingField_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Nullable_1_t9B468FE267D697A84B680BFB00D8C20BAB1A26E7  AdRequest_get_Birthday_mF6551ECBB0E8EB04D73536381A5685D0FF55715A_inline (AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * __this, const RuntimeMethod* method)
{
	{
		// public DateTime? Birthday { get; private set; }
		Nullable_1_t9B468FE267D697A84B680BFB00D8C20BAB1A26E7  L_0 = __this->get_U3CBirthdayU3Ek__BackingField_4();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Nullable_1_t65AAF2600D9569366D224E2EB7B3F65128A96D1B  AdRequest_get_Gender_m40E1D2BE6A214044F95918E202D11E5551118889_inline (AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * __this, const RuntimeMethod* method)
{
	{
		// public Gender? Gender { get; private set; }
		Nullable_1_t65AAF2600D9569366D224E2EB7B3F65128A96D1B  L_0 = __this->get_U3CGenderU3Ek__BackingField_5();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  AdRequest_get_TagForChildDirectedTreatment_mAF138AD39FC86FA60D310B685A6F3F1CDE91ABAA_inline (AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * __this, const RuntimeMethod* method)
{
	{
		// public bool? TagForChildDirectedTreatment { get; private set; }
		Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  L_0 = __this->get_U3CTagForChildDirectedTreatmentU3Ek__BackingField_6();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * AdRequest_get_Extras_m814DAAAC04DFD17FC1DD6446718BEF964934B83B_inline (AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * __this, const RuntimeMethod* method)
{
	{
		// public Dictionary<string, string> Extras { get; private set; }
		Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * L_0 = __this->get_U3CExtrasU3Ek__BackingField_7();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR List_1_t8F38EE057190BE9897CDE829F7EB4282DF6F3EBA * AdRequest_get_MediationExtras_mEEC772BDDD5CA17C993C45BD09F342E0CCFB1842_inline (AdRequest_t98763832B122F67AC2952360B6381F4F5848306F * __this, const RuntimeMethod* method)
{
	{
		// public List<MediationExtras> MediationExtras { get; private set; }
		List_1_t8F38EE057190BE9897CDE829F7EB4282DF6F3EBA * L_0 = __this->get_U3CMediationExtrasU3Ek__BackingField_8();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * MediationExtras_get_Extras_m845B48080AE808551366F9B6E363E0429EAE7869_inline (MediationExtras_t53F0F7F90139A2D039272C9764DC880D4B3AB301 * __this, const RuntimeMethod* method)
{
	{
		// public Dictionary<string, string> Extras { get; protected set; }
		Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * L_0 = __this->get_U3CExtrasU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491 * RewardBasedVideoAd_get_Instance_mD22F18358E8AAE1EF7D0EF41D89938E27F110413_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RewardBasedVideoAd_get_Instance_mD22F18358E8AAE1EF7D0EF41D89938E27F110413AssemblyU2DCSharp1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return instance;
		IL2CPP_RUNTIME_CLASS_INIT(RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491_il2cpp_TypeInfo_var);
		RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491 * L_0 = ((RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491_StaticFields*)il2cpp_codegen_static_fields_for(RewardBasedVideoAd_t9795550B217AAB71B32AEDA94163C6A929353491_il2cpp_TypeInfo_var))->get_instance_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool Product_get_availableToPurchase_mA4F6F87AA8B83915F1B5E122A10F0CBAFBAE196C_inline (Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CavailableToPurchaseU3Ek__BackingField_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * Product_get_definition_mFA1889844E23D37A700C2EA6659EE3BBF1E17C58_inline (Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * __this, const RuntimeMethod* method)
{
	{
		ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * L_0 = __this->get_U3CdefinitionU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* ProductDefinition_get_id_mD5C2A9C1016245C99EB67B74DFC32ECDC8512468_inline (ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CidU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * PurchaseEventArgs_get_purchasedProduct_mB26BCEF9CD45B84DBF55A5C6FC50AF611B6AD6A2_inline (PurchaseEventArgs_tF6E04BFD5492F5F57309FFBB2415EB26E5B88C04 * __this, const RuntimeMethod* method)
{
	{
		Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * L_0 = __this->get_U3CpurchasedProductU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* ProductDefinition_get_storeSpecificId_m544E2354C4C9049DE5C24D07A347669A05B241D0_inline (ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CstoreSpecificIdU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m4C430D4730AABE78C2EDBC5324F1E82FEC21CDED_gshared_inline (Enumerator_tC01102D13E6FA5B4F99A7406FE3F96BED7C2250A * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get__current_3();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared_inline (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_mB231DEE33107C7E966680F0404FFDD7939B24625_gshared_inline (Nullable_1_t9B468FE267D697A84B680BFB00D8C20BAB1A26E7 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  Nullable_1_GetValueOrDefault_mA2BC7FE28E1E54F6DED8F2DA5ACA72A56083E43D_gshared_inline (Nullable_1_t9B468FE267D697A84B680BFB00D8C20BAB1A26E7 * __this, const RuntimeMethod* method)
{
	{
		DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  L_0 = (DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 )__this->get_value_0();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_m58742F2DBAEDAA6626C115F9E99209C8F4D8F195_gshared_inline (Nullable_1_tBCA4780CE8E9555A53CF0BA48AF742DA998C0833 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t Nullable_1_GetValueOrDefault_m19DE1B4F1BFB431B2FE3475CECA352580499168D_gshared_inline (Nullable_1_tBCA4780CE8E9555A53CF0BA48AF742DA998C0833 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_0();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_m275A31438FCDAEEE039E95D887684E04FD6ECE2B_gshared_inline (Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR bool Nullable_1_GetValueOrDefault_mD65884636E94AF88C2745255BBE130A1B64BA294_gshared_inline (Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_value_0();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE  Enumerator_get_Current_m5B32A9FC8294CB723DCD1171744B32E1775B6318_gshared_inline (Enumerator_tED23DFBF3911229086C71CCE7A54D56F5FFB34CB * __this, const RuntimeMethod* method)
{
	{
		KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE  L_0 = (KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE )__this->get_current_3();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * KeyValuePair_2_get_Key_m9D4E9BCBAB1BE560871A0889C851FC22A09975F4_gshared_inline (KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_key_0();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * KeyValuePair_2_get_Value_m8C7B882C4D425535288FAAD08EAF11D289A43AEC_gshared_inline (KeyValuePair_2_t23481547E419E16E3B96A303578C1EB685C99EEE * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_value_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* Array_Empty_TisRuntimeObject_m9CF99326FAC8A01A4A25C90AA97F0799BA35ECAB_gshared_inline (const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = ((EmptyArray_1_tCF137C88A5824F413EFB5A2F31664D8207E61D26_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->rgctx_data, 0)))->get_Value_0();
		return L_0;
	}
}
