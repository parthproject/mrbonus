﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"



extern const RuntimeMethod* AppleStoreImpl_MessageCallback_m1AFEBE0209301D9DE3F08BF561DC43984AEC3251_RuntimeMethod_var;
extern const RuntimeMethod* FacebookStoreImpl_MessageCallback_m95B738D784D0C0AC054DE6F7DBC86B4E12517105_RuntimeMethod_var;
extern const RuntimeMethod* TizenStoreImpl_MessageCallback_m468657DE860B56D8404F5502CB68ABA6C7B13748_RuntimeMethod_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 UnityEngine.RuntimePlatform Uniject.IUtil::get_platform()
// 0x00000002 System.String Uniject.IUtil::get_persistentDataPath()
// 0x00000003 System.String Uniject.IUtil::get_cloudProjectId()
// 0x00000004 System.String Uniject.IUtil::get_unityVersion()
// 0x00000005 System.String Uniject.IUtil::get_userId()
// 0x00000006 System.String Uniject.IUtil::get_gameVersion()
// 0x00000007 System.UInt64 Uniject.IUtil::get_sessionId()
// 0x00000008 System.String Uniject.IUtil::get_operatingSystem()
// 0x00000009 System.Int32 Uniject.IUtil::get_screenWidth()
// 0x0000000A System.Int32 Uniject.IUtil::get_screenHeight()
// 0x0000000B System.Single Uniject.IUtil::get_screenDpi()
// 0x0000000C System.String Uniject.IUtil::get_screenOrientation()
// 0x0000000D System.Object Uniject.IUtil::InitiateCoroutine(System.Collections.IEnumerator)
// 0x0000000E System.Void Uniject.IUtil::InitiateCoroutine(System.Collections.IEnumerator,System.Int32)
// 0x0000000F System.Void Uniject.IUtil::RunOnMainThread(System.Action)
// 0x00000010 System.Void Uniject.IUtil::AddPauseListener(System.Action`1<System.Boolean>)
// 0x00000011 System.Boolean Uniject.IUtil::IsClassOrSubclass(System.Type,System.Type)
// 0x00000012 UnityEngine.AndroidJavaObject UnityEngine.Purchasing.AndroidJavaStore::GetStore()
extern void AndroidJavaStore_GetStore_mA304200D2D3C146F0AD3A80323B62AB953AB05A8 ();
// 0x00000013 System.Void UnityEngine.Purchasing.AndroidJavaStore::.ctor(UnityEngine.AndroidJavaObject)
extern void AndroidJavaStore__ctor_m01449C4CE7FC18DA7C3EB3BA04B5B04D10D4EFE2 ();
// 0x00000014 System.Void UnityEngine.Purchasing.AndroidJavaStore::RetrieveProducts(System.String)
extern void AndroidJavaStore_RetrieveProducts_m8B8B7BC7B4E4BFB66A005A5D7049DFFCA86E40B3 ();
// 0x00000015 System.Void UnityEngine.Purchasing.AndroidJavaStore::Purchase(System.String,System.String)
extern void AndroidJavaStore_Purchase_mC6AE78FB27F3914D88610A77DFE2842918E8805F ();
// 0x00000016 System.Void UnityEngine.Purchasing.AndroidJavaStore::FinishTransaction(System.String,System.String)
extern void AndroidJavaStore_FinishTransaction_mC6D40C96823DADBD1D99426E70D9CB4EC8D8CC00 ();
// 0x00000017 System.Void UnityEngine.Purchasing.JavaBridge::.ctor(UnityEngine.Purchasing.IUnityCallback)
extern void JavaBridge__ctor_m8B5EB23E084860EEDBF4BF2E4C6E3DA38A2B0C0C ();
// 0x00000018 System.String UnityEngine.Purchasing.SerializationExtensions::TryGetString(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String)
extern void SerializationExtensions_TryGetString_m8666BE2887720A908F5078AE72AB4ABD5E701297 ();
// 0x00000019 System.String UnityEngine.Purchasing.JSONSerializer::SerializeProductDef(UnityEngine.Purchasing.ProductDefinition)
extern void JSONSerializer_SerializeProductDef_m77844CA2AE14EF74419CB47CA7A4B846D7B7EE61 ();
// 0x0000001A System.String UnityEngine.Purchasing.JSONSerializer::SerializeProductDefs(System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.ProductDefinition>)
extern void JSONSerializer_SerializeProductDefs_m935D15BBAE29C788FBD6F5B8848AD9A0EBE1146F ();
// 0x0000001B System.Collections.Generic.List`1<UnityEngine.Purchasing.Extension.ProductDescription> UnityEngine.Purchasing.JSONSerializer::DeserializeProductDescriptions(System.String)
extern void JSONSerializer_DeserializeProductDescriptions_mDBCFC86BCF56FC829E2866E5D276FD3A20FB3BE2 ();
// 0x0000001C UnityEngine.Purchasing.Extension.PurchaseFailureDescription UnityEngine.Purchasing.JSONSerializer::DeserializeFailureReason(System.String)
extern void JSONSerializer_DeserializeFailureReason_m3210F29425C97F28602B5D0F85E0532727969787 ();
// 0x0000001D UnityEngine.Purchasing.ProductMetadata UnityEngine.Purchasing.JSONSerializer::DeserializeMetadata(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void JSONSerializer_DeserializeMetadata_m1C99F239DED10235E61B29B19D05BC77BD0F7962 ();
// 0x0000001E System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.JSONSerializer::EncodeProductDef(UnityEngine.Purchasing.ProductDefinition)
extern void JSONSerializer_EncodeProductDef_m04E0C3CC8268B25EBDE5971EC04B5ABC31D21F49 ();
// 0x0000001F System.Void UnityEngine.Purchasing.ScriptingUnityCallback::.ctor(UnityEngine.Purchasing.IUnityCallback,Uniject.IUtil)
extern void ScriptingUnityCallback__ctor_m48619600DFDBC3CECBCD8D2F16D837ED08137520 ();
// 0x00000020 System.Void UnityEngine.Purchasing.AmazonAppStoreStoreExtensions::.ctor(UnityEngine.AndroidJavaObject)
extern void AmazonAppStoreStoreExtensions__ctor_mB8D766EF75B8A54AD23B084A7DE79D450E4A17C0 ();
// 0x00000021 System.Void UnityEngine.Purchasing.FakeAmazonExtensions::.ctor()
extern void FakeAmazonExtensions__ctor_m64E336084FA7DBFB957ACD40E1800208AEB5BE17 ();
// 0x00000022 System.Void UnityEngine.Purchasing.FakeMoolahConfiguration::set_appKey(System.String)
extern void FakeMoolahConfiguration_set_appKey_m31E4DE57A1B03FB60CBBB599B7327AADD2570B8C ();
// 0x00000023 System.Void UnityEngine.Purchasing.FakeMoolahConfiguration::set_hashKey(System.String)
extern void FakeMoolahConfiguration_set_hashKey_m2DA764F4DE68CDFA0FC5D0190C5B8DBA162A28CD ();
// 0x00000024 System.Void UnityEngine.Purchasing.FakeMoolahConfiguration::SetMode(UnityEngine.Purchasing.CloudMoolahMode)
extern void FakeMoolahConfiguration_SetMode_m9B36461A21CA242CC3EA775AB1ACBA66CE5A9015 ();
// 0x00000025 System.Void UnityEngine.Purchasing.FakeMoolahConfiguration::.ctor()
extern void FakeMoolahConfiguration__ctor_mA5AF3EDC0DF43F8476C64D6BE55057E8C815334A ();
// 0x00000026 System.Void UnityEngine.Purchasing.FakeMoolahExtensions::RestoreTransactionID(System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState>)
extern void FakeMoolahExtensions_RestoreTransactionID_m3615E412049BC50246AA573177BDF27677BA9B97 ();
// 0x00000027 System.Void UnityEngine.Purchasing.FakeMoolahExtensions::.ctor()
extern void FakeMoolahExtensions__ctor_mE2B8A5320FF78AD5859FFA3682976AB911F1D4C4 ();
// 0x00000028 System.Void UnityEngine.Purchasing.IMoolahConfiguration::set_appKey(System.String)
// 0x00000029 System.Void UnityEngine.Purchasing.IMoolahConfiguration::set_hashKey(System.String)
// 0x0000002A System.Void UnityEngine.Purchasing.IMoolahConfiguration::SetMode(UnityEngine.Purchasing.CloudMoolahMode)
// 0x0000002B System.Void UnityEngine.Purchasing.IMoolahExtension::RestoreTransactionID(System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState>)
// 0x0000002C System.Void UnityEngine.Purchasing.MoolahStoreImpl::Initialize(UnityEngine.Purchasing.Extension.IStoreCallback)
extern void MoolahStoreImpl_Initialize_mB3657B44EEF0337D234C5E9E758FB9521DCAC62A ();
// 0x0000002D System.Void UnityEngine.Purchasing.MoolahStoreImpl::RetrieveProducts(System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>)
extern void MoolahStoreImpl_RetrieveProducts_mD752C59D83FB377D46B027E49A3BC58142042E62 ();
// 0x0000002E System.Int32 UnityEngine.Purchasing.MoolahStoreImpl::GetProductTypeIndex(UnityEngine.Purchasing.ProductType)
extern void MoolahStoreImpl_GetProductTypeIndex_m7A0D8AA38F30627E090D6FC4136DDFFD5315EBBF ();
// 0x0000002F System.Void UnityEngine.Purchasing.MoolahStoreImpl::VaildateProductProcess(System.Boolean,System.String)
extern void MoolahStoreImpl_VaildateProductProcess_m55BC13006F68920045766C9CEBAE64459B68C4D3 ();
// 0x00000030 System.String UnityEngine.Purchasing.MoolahStoreImpl::GetCurrentString(System.Object)
extern void MoolahStoreImpl_GetCurrentString_m757FE58555DABE1A2ECE45CA5ECBB8751FE8D1D3 ();
// 0x00000031 System.Collections.IEnumerator UnityEngine.Purchasing.MoolahStoreImpl::VaildateProduct(System.String,System.String,System.Action`2<System.Boolean,System.String>)
extern void MoolahStoreImpl_VaildateProduct_m7D5EF5CC862A8A9B325A7603CA6074DAF65406CB ();
// 0x00000032 System.Void UnityEngine.Purchasing.MoolahStoreImpl::RetrieveProductsSucceeded(System.Collections.Generic.List`1<UnityEngine.Purchasing.Extension.ProductDescription>)
extern void MoolahStoreImpl_RetrieveProductsSucceeded_mAB8A7E7F4C609B60EA062277D31ED9455555C8B0 ();
// 0x00000033 System.Void UnityEngine.Purchasing.MoolahStoreImpl::RetrieveProductsFailed(UnityEngine.Purchasing.InitializationFailureReason)
extern void MoolahStoreImpl_RetrieveProductsFailed_m633FBD30A53905575209DD41A4259BE2D32F2964 ();
// 0x00000034 System.Void UnityEngine.Purchasing.MoolahStoreImpl::ClosePayWebView(System.String)
extern void MoolahStoreImpl_ClosePayWebView_mE6D8F087ED6EC82785F65C899A64A369DAE607C2 ();
// 0x00000035 System.Void UnityEngine.Purchasing.MoolahStoreImpl::PurchaseRusult(System.String)
extern void MoolahStoreImpl_PurchaseRusult_m39F0431BFD7C8C4CCD76F94FC2552B0DFA0B1C3E ();
// 0x00000036 System.Void UnityEngine.Purchasing.MoolahStoreImpl::Purchase(UnityEngine.Purchasing.ProductDefinition,System.String)
extern void MoolahStoreImpl_Purchase_mFC4820701C6AC6E8120A21EC4E4F7C3F8042F700 ();
// 0x00000037 System.String UnityEngine.Purchasing.MoolahStoreImpl::DeviceUniqueIdentifier()
extern void MoolahStoreImpl_DeviceUniqueIdentifier_m8ECD985898FFE720E8660548B5C34D911EB3AF99 ();
// 0x00000038 System.Void UnityEngine.Purchasing.MoolahStoreImpl::RequestAuthCode(System.String,System.String,System.Action`3<System.String,System.String,System.String>,System.Action`2<System.String,System.String>)
extern void MoolahStoreImpl_RequestAuthCode_m8772B6C0E0B1F528318B460C0F915A13846D4BEA ();
// 0x00000039 System.Collections.IEnumerator UnityEngine.Purchasing.MoolahStoreImpl::RequestAuthCode(UnityEngine.WWWForm,System.String,System.String,System.Action`3<System.String,System.String,System.String>,System.Action`2<System.String,System.String>)
extern void MoolahStoreImpl_RequestAuthCode_mFF90DD999B570306498950A93B99429EBE62F58F ();
// 0x0000003A System.Collections.IEnumerator UnityEngine.Purchasing.MoolahStoreImpl::StartPurchasePolling(System.String,System.String,System.Action`3<System.String,System.String,System.String>,System.Action`3<System.String,UnityEngine.Purchasing.PurchaseFailureReason,System.String>)
extern void MoolahStoreImpl_StartPurchasePolling_m74D1B15C798D1D319C0DA5EE254D274B218513A4 ();
// 0x0000003B System.Void UnityEngine.Purchasing.MoolahStoreImpl::PurchaseSucceed(System.String,System.String,System.String)
extern void MoolahStoreImpl_PurchaseSucceed_m98845A1DBF5D7BA1DF22F49E6BE2A3D88F84F3A8 ();
// 0x0000003C System.Void UnityEngine.Purchasing.MoolahStoreImpl::PurchaseFailed(System.String,UnityEngine.Purchasing.PurchaseFailureReason,System.String)
extern void MoolahStoreImpl_PurchaseFailed_mD7C3E88D4962B6163D3DDB2DA21E010F7DC0F710 ();
// 0x0000003D System.Void UnityEngine.Purchasing.MoolahStoreImpl::FinishTransaction(UnityEngine.Purchasing.ProductDefinition,System.String)
extern void MoolahStoreImpl_FinishTransaction_m28D8C1743E97E7DCF2DF7BD01B1238664FA74110 ();
// 0x0000003E System.String UnityEngine.Purchasing.MoolahStoreImpl::GetStringMD5(System.String)
extern void MoolahStoreImpl_GetStringMD5_m12CD817C2B9062B7F2F62116337ECCDEFB00B2D0 ();
// 0x0000003F System.String UnityEngine.Purchasing.MoolahStoreImpl::get_appKey()
extern void MoolahStoreImpl_get_appKey_m0A0B85066F0388406D22AF439FC84831AEDBC423 ();
// 0x00000040 System.Void UnityEngine.Purchasing.MoolahStoreImpl::set_appKey(System.String)
extern void MoolahStoreImpl_set_appKey_m10D03FF35EAB509842AACAE62575AFEE18449778 ();
// 0x00000041 System.String UnityEngine.Purchasing.MoolahStoreImpl::get_hashKey()
extern void MoolahStoreImpl_get_hashKey_m32F417BB0A5FE8EC8649DF5164645CB10A55FE81 ();
// 0x00000042 System.Void UnityEngine.Purchasing.MoolahStoreImpl::set_hashKey(System.String)
extern void MoolahStoreImpl_set_hashKey_mF9CA09D3FDB1373131B3C2E6977F0133D07AB2E3 ();
// 0x00000043 System.String UnityEngine.Purchasing.MoolahStoreImpl::get_notificationURL()
extern void MoolahStoreImpl_get_notificationURL_mFEF77B53CB8EE3C9AB6A5498C10921C95F0D89D5 ();
// 0x00000044 System.Void UnityEngine.Purchasing.MoolahStoreImpl::set_notificationURL(System.String)
extern void MoolahStoreImpl_set_notificationURL_m5586331F66E173CB3CE4531FE8A6EEB0F42A2101 ();
// 0x00000045 System.Void UnityEngine.Purchasing.MoolahStoreImpl::SetMode(UnityEngine.Purchasing.CloudMoolahMode)
extern void MoolahStoreImpl_SetMode_m2F1A8F5A4EC30985BB0BBBB39EC0C6C5FC4C977B ();
// 0x00000046 UnityEngine.Purchasing.CloudMoolahMode UnityEngine.Purchasing.MoolahStoreImpl::GetMode()
extern void MoolahStoreImpl_GetMode_m64ED3042B62C57EEC2C854A97D95FE33BB301994 ();
// 0x00000047 System.Void UnityEngine.Purchasing.MoolahStoreImpl::RestoreTransactionID(System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState>)
extern void MoolahStoreImpl_RestoreTransactionID_m912F75EB80A1274AEF5039785958F658F2BDD2A8 ();
// 0x00000048 System.Collections.IEnumerator UnityEngine.Purchasing.MoolahStoreImpl::RestoreTransactionIDProcess(System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState>)
extern void MoolahStoreImpl_RestoreTransactionIDProcess_m7F5CBA3F7A482DEEF40C3920DBD62432FBC5A5F9 ();
// 0x00000049 System.Void UnityEngine.Purchasing.MoolahStoreImpl::ValidateReceipt(System.String,System.String,System.Action`3<System.String,UnityEngine.Purchasing.ValidateReceiptState,System.String>)
extern void MoolahStoreImpl_ValidateReceipt_m894D5C05C6E0F9ABAB35CAAB0659558BCD07E4E2 ();
// 0x0000004A System.Collections.IEnumerator UnityEngine.Purchasing.MoolahStoreImpl::ValidateReceiptProcess(System.String,System.String,System.Action`3<System.String,UnityEngine.Purchasing.ValidateReceiptState,System.String>)
extern void MoolahStoreImpl_ValidateReceiptProcess_m9BFD9A51B23A7C08A1B9379F18018278B1DFF652 ();
// 0x0000004B System.Void UnityEngine.Purchasing.MoolahStoreImpl::.ctor()
extern void MoolahStoreImpl__ctor_m45B9BF757BB724568D36823E1043C50B84AE0975 ();
// 0x0000004C System.Void UnityEngine.Purchasing.MoolahStoreImpl::.cctor()
extern void MoolahStoreImpl__cctor_m302299AD2F4E724E1539A480D4229B5AE695F912 ();
// 0x0000004D System.Void UnityEngine.Purchasing.MoolahStoreImpl::<RetrieveProducts>b__9_0(System.Boolean,System.String)
extern void MoolahStoreImpl_U3CRetrieveProductsU3Eb__9_0_mC0C23CDD67DB82B29E76A285049E70FA5CDFE6B1 ();
// 0x0000004E System.Void UnityEngine.Purchasing.MoolahStoreImpl_<VaildateProduct>d__13::.ctor(System.Int32)
extern void U3CVaildateProductU3Ed__13__ctor_mCD67B17C14B37F361EB318504D89451ED1BB11D3 ();
// 0x0000004F System.Void UnityEngine.Purchasing.MoolahStoreImpl_<VaildateProduct>d__13::System.IDisposable.Dispose()
extern void U3CVaildateProductU3Ed__13_System_IDisposable_Dispose_mDAD4D5966A79608342076309E6C214ACD1A77EDF ();
// 0x00000050 System.Boolean UnityEngine.Purchasing.MoolahStoreImpl_<VaildateProduct>d__13::MoveNext()
extern void U3CVaildateProductU3Ed__13_MoveNext_m2C00B98CF827E68126B160B9E099932CC1677CCA ();
// 0x00000051 System.Object UnityEngine.Purchasing.MoolahStoreImpl_<VaildateProduct>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CVaildateProductU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC051E7E5F7240EE282DB4114EC9F70FB39F52214 ();
// 0x00000052 System.Void UnityEngine.Purchasing.MoolahStoreImpl_<VaildateProduct>d__13::System.Collections.IEnumerator.Reset()
extern void U3CVaildateProductU3Ed__13_System_Collections_IEnumerator_Reset_mB0C69F193EB534C54C40222D449DB177EB9D610D ();
// 0x00000053 System.Object UnityEngine.Purchasing.MoolahStoreImpl_<VaildateProduct>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CVaildateProductU3Ed__13_System_Collections_IEnumerator_get_Current_m9466624D6F2DD779AC41B9900B8AB3F478D56215 ();
// 0x00000054 System.Void UnityEngine.Purchasing.MoolahStoreImpl_<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_m023DB3766C8DD0B737650705E935AE59875E18EF ();
// 0x00000055 System.Void UnityEngine.Purchasing.MoolahStoreImpl_<>c__DisplayClass18_0::<Purchase>b__0(System.String,System.String,System.String)
extern void U3CU3Ec__DisplayClass18_0_U3CPurchaseU3Eb__0_m3EEF558B55F6449BB2915E92E5A2E6C460C8ABC0 ();
// 0x00000056 System.Void UnityEngine.Purchasing.MoolahStoreImpl_<>c__DisplayClass18_0::<Purchase>b__1(System.String,UnityEngine.Purchasing.PurchaseFailureReason,System.String)
extern void U3CU3Ec__DisplayClass18_0_U3CPurchaseU3Eb__1_mB7EE8E5DFAEEDA1BF25975316F71C1FF91345B26 ();
// 0x00000057 System.Void UnityEngine.Purchasing.MoolahStoreImpl_<>c__DisplayClass18_0::<Purchase>b__2(System.String,System.String,System.String)
extern void U3CU3Ec__DisplayClass18_0_U3CPurchaseU3Eb__2_m16A1A6C667F2E5C60C691924BBC7A31F62BE3E60 ();
// 0x00000058 System.Void UnityEngine.Purchasing.MoolahStoreImpl_<>c__DisplayClass18_0::<Purchase>b__3(System.String,System.String)
extern void U3CU3Ec__DisplayClass18_0_U3CPurchaseU3Eb__3_m1CD71BCFD3805ADCD05CBA0AF0B4736C170F8087 ();
// 0x00000059 System.Void UnityEngine.Purchasing.MoolahStoreImpl_<RequestAuthCode>d__22::.ctor(System.Int32)
extern void U3CRequestAuthCodeU3Ed__22__ctor_mF17FC3E7F5BE183BA45278454C960414AFEC7F50 ();
// 0x0000005A System.Void UnityEngine.Purchasing.MoolahStoreImpl_<RequestAuthCode>d__22::System.IDisposable.Dispose()
extern void U3CRequestAuthCodeU3Ed__22_System_IDisposable_Dispose_mE812CAD04F33D94A151D8A3D4B35EB1182B0EA64 ();
// 0x0000005B System.Boolean UnityEngine.Purchasing.MoolahStoreImpl_<RequestAuthCode>d__22::MoveNext()
extern void U3CRequestAuthCodeU3Ed__22_MoveNext_m997E4D8DF6C6A994ED009A028B95E78FF65B9531 ();
// 0x0000005C System.Object UnityEngine.Purchasing.MoolahStoreImpl_<RequestAuthCode>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRequestAuthCodeU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDD8A8B0FFA0DA2DEB718608581008AA6EEC910D7 ();
// 0x0000005D System.Void UnityEngine.Purchasing.MoolahStoreImpl_<RequestAuthCode>d__22::System.Collections.IEnumerator.Reset()
extern void U3CRequestAuthCodeU3Ed__22_System_Collections_IEnumerator_Reset_mC4B5C97B30538D89F2AAC1781288D0DF9B84AD76 ();
// 0x0000005E System.Object UnityEngine.Purchasing.MoolahStoreImpl_<RequestAuthCode>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CRequestAuthCodeU3Ed__22_System_Collections_IEnumerator_get_Current_m967BEDFD426C15484A6254361D84FEAED019BA03 ();
// 0x0000005F System.Void UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::.ctor(System.Int32)
extern void U3CStartPurchasePollingU3Ed__23__ctor_mC77E33A83A4BE9BE20E5939B0F48FC6DA5AF898C ();
// 0x00000060 System.Void UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::System.IDisposable.Dispose()
extern void U3CStartPurchasePollingU3Ed__23_System_IDisposable_Dispose_m39D82E1608C7EB76CA760A4AAF5093D9A03A1313 ();
// 0x00000061 System.Boolean UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::MoveNext()
extern void U3CStartPurchasePollingU3Ed__23_MoveNext_m66E26C8CB39A1BA33A573F5F87C8A632AD3EAC02 ();
// 0x00000062 System.Object UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartPurchasePollingU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m679EF7F98417AFEE4F09BD5C29F643C91CA3528C ();
// 0x00000063 System.Void UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::System.Collections.IEnumerator.Reset()
extern void U3CStartPurchasePollingU3Ed__23_System_Collections_IEnumerator_Reset_mA313BB35752264939B088A1607FA33A32541F2E6 ();
// 0x00000064 System.Object UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CStartPurchasePollingU3Ed__23_System_Collections_IEnumerator_get_Current_mE8C680CC04C71B1AE055F3DC2AB9DF18EE86922C ();
// 0x00000065 System.Void UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::.ctor(System.Int32)
extern void U3CRestoreTransactionIDProcessU3Ed__45__ctor_m99E1DAEC51C83B09140F3B39711CECEB7CDFF344 ();
// 0x00000066 System.Void UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::System.IDisposable.Dispose()
extern void U3CRestoreTransactionIDProcessU3Ed__45_System_IDisposable_Dispose_mE71F3427A082632FACC7D1EDB6C8A628AC30AF30 ();
// 0x00000067 System.Boolean UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::MoveNext()
extern void U3CRestoreTransactionIDProcessU3Ed__45_MoveNext_m83DC83F9924CA05EF8FC9D3139C972C3C1FD88C1 ();
// 0x00000068 System.Object UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRestoreTransactionIDProcessU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC1FF1A67A12475C96368350787E66EDA4638BAF2 ();
// 0x00000069 System.Void UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::System.Collections.IEnumerator.Reset()
extern void U3CRestoreTransactionIDProcessU3Ed__45_System_Collections_IEnumerator_Reset_m5C05EC468494E743F8323F918609947B417FEFF2 ();
// 0x0000006A System.Object UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::System.Collections.IEnumerator.get_Current()
extern void U3CRestoreTransactionIDProcessU3Ed__45_System_Collections_IEnumerator_get_Current_m71DE8DFF43C88E9F760ADF8DAC4295663BEFD235 ();
// 0x0000006B System.Void UnityEngine.Purchasing.MoolahStoreImpl_<ValidateReceiptProcess>d__47::.ctor(System.Int32)
extern void U3CValidateReceiptProcessU3Ed__47__ctor_m1FDA2398BEB6F5716543617290D6BBF6528593B4 ();
// 0x0000006C System.Void UnityEngine.Purchasing.MoolahStoreImpl_<ValidateReceiptProcess>d__47::System.IDisposable.Dispose()
extern void U3CValidateReceiptProcessU3Ed__47_System_IDisposable_Dispose_m7629CC7EFA06692F88CE4D990CFCF359C335BCAC ();
// 0x0000006D System.Boolean UnityEngine.Purchasing.MoolahStoreImpl_<ValidateReceiptProcess>d__47::MoveNext()
extern void U3CValidateReceiptProcessU3Ed__47_MoveNext_m1BBD29387E31B4A1DDD374A8096AC2CEDD77F758 ();
// 0x0000006E System.Object UnityEngine.Purchasing.MoolahStoreImpl_<ValidateReceiptProcess>d__47::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CValidateReceiptProcessU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEC2629C05909AF60C40B1F4F532256B190E2A38A ();
// 0x0000006F System.Void UnityEngine.Purchasing.MoolahStoreImpl_<ValidateReceiptProcess>d__47::System.Collections.IEnumerator.Reset()
extern void U3CValidateReceiptProcessU3Ed__47_System_Collections_IEnumerator_Reset_mAC5AA05F1DAF6EB74E5EB819CC1A05314E4FF1A3 ();
// 0x00000070 System.Object UnityEngine.Purchasing.MoolahStoreImpl_<ValidateReceiptProcess>d__47::System.Collections.IEnumerator.get_Current()
extern void U3CValidateReceiptProcessU3Ed__47_System_Collections_IEnumerator_get_Current_mDD606F8B415C305DCFE5D6421572CC2938BE0397 ();
// 0x00000071 System.Void UnityEngine.Purchasing.PayMethod::showPayWebView(System.String,System.String,System.String,System.String,System.String)
extern void PayMethod_showPayWebView_mE7EAB314D203A45626FF2B3B9290D2B651B5038F ();
// 0x00000072 System.String UnityEngine.Purchasing.PayMethod::getDeviceID()
extern void PayMethod_getDeviceID_m80B0E609E743D2B84EE9246BFA8F4DBBEC0F2E4E ();
// 0x00000073 System.Void UnityEngine.Purchasing.FakeGooglePlayStoreExtensions::RestoreTransactions(System.Action`1<System.Boolean>)
extern void FakeGooglePlayStoreExtensions_RestoreTransactions_m64163A0B7745A598DEF82ABE87A9AA149D80896C ();
// 0x00000074 System.Void UnityEngine.Purchasing.FakeGooglePlayStoreExtensions::SetLogLevel(System.Int32)
extern void FakeGooglePlayStoreExtensions_SetLogLevel_m2E9A4AA9C6B73AA2AC98BECC9B09CEF068D405A7 ();
// 0x00000075 System.Void UnityEngine.Purchasing.FakeGooglePlayStoreExtensions::.ctor()
extern void FakeGooglePlayStoreExtensions__ctor_mD85768C4E64FAE9E24423FB1C85C002732069334 ();
// 0x00000076 System.Void UnityEngine.Purchasing.GooglePlayAndroidJavaStore::.ctor(UnityEngine.AndroidJavaObject,Uniject.IUtil)
extern void GooglePlayAndroidJavaStore__ctor_mDF006F20A4C1F42F81AAB14C091216C9F16923B0 ();
// 0x00000077 System.Void UnityEngine.Purchasing.GooglePlayAndroidJavaStore::Purchase(System.String,System.String)
extern void GooglePlayAndroidJavaStore_Purchase_m4966D459FDA000D08BBE44373B94BA84E9E5DC4E ();
// 0x00000078 System.Void UnityEngine.Purchasing.FakeGooglePlayConfiguration::.ctor()
extern void FakeGooglePlayConfiguration__ctor_mDD816501908B8979EC958F0F535784C1994F2DD4 ();
// 0x00000079 System.Void UnityEngine.Purchasing.GooglePlayStoreCallback::.ctor(System.Action`1<System.Boolean>)
extern void GooglePlayStoreCallback__ctor_m5788B5A80293BE28D30EF5B6AEC2B88C3CA3A69A ();
// 0x0000007A System.Void UnityEngine.Purchasing.GooglePlayStoreExtensions::.ctor()
extern void GooglePlayStoreExtensions__ctor_m8AABC42BC8DB59F0C13DEAD523B4A637C5D1A790 ();
// 0x0000007B System.Void UnityEngine.Purchasing.GooglePlayStoreExtensions::SetAndroidJavaObject(UnityEngine.AndroidJavaObject)
extern void GooglePlayStoreExtensions_SetAndroidJavaObject_m8CD7F00EA62C94EDFB47165AD1B25B42E69BEDDA ();
// 0x0000007C System.Void UnityEngine.Purchasing.GooglePlayStoreExtensions::RestoreTransactions(System.Action`1<System.Boolean>)
extern void GooglePlayStoreExtensions_RestoreTransactions_m3307BC78807B8C8AF0D3DE214740AC5205541D13 ();
// 0x0000007D System.Void UnityEngine.Purchasing.GooglePlayStoreExtensions::SetLogLevel(System.Int32)
extern void GooglePlayStoreExtensions_SetLogLevel_m7E8DF982F055391DF5C714761D316986C33A6F84 ();
// 0x0000007E System.Void UnityEngine.Purchasing.IGooglePlayStoreExtensions::RestoreTransactions(System.Action`1<System.Boolean>)
// 0x0000007F System.Void UnityEngine.Purchasing.IGooglePlayStoreExtensions::SetLogLevel(System.Int32)
// 0x00000080 System.Void UnityEngine.Purchasing.FakeSamsungAppsExtensions::SetMode(UnityEngine.Purchasing.SamsungAppsMode)
extern void FakeSamsungAppsExtensions_SetMode_mE54A81C756C55FAB27C679E8AB529F91D3A8C2B3 ();
// 0x00000081 System.Void UnityEngine.Purchasing.FakeSamsungAppsExtensions::RestoreTransactions(System.Action`1<System.Boolean>)
extern void FakeSamsungAppsExtensions_RestoreTransactions_m013DA5B1A804925A5C3FA7D0BBBE669D4621F04A ();
// 0x00000082 System.Void UnityEngine.Purchasing.FakeSamsungAppsExtensions::.ctor()
extern void FakeSamsungAppsExtensions__ctor_mAA23B8D906AD755F40DCDE05789FE8FEB6B55469 ();
// 0x00000083 System.Void UnityEngine.Purchasing.ISamsungAppsConfiguration::SetMode(UnityEngine.Purchasing.SamsungAppsMode)
// 0x00000084 System.Void UnityEngine.Purchasing.ISamsungAppsExtensions::RestoreTransactions(System.Action`1<System.Boolean>)
// 0x00000085 System.Void UnityEngine.Purchasing.SamsungAppsStoreExtensions::.ctor()
extern void SamsungAppsStoreExtensions__ctor_mF4879E66662B5AEB866D6CBD24E8CAC881C5EB82 ();
// 0x00000086 System.Void UnityEngine.Purchasing.SamsungAppsStoreExtensions::SetAndroidJavaObject(UnityEngine.AndroidJavaObject)
extern void SamsungAppsStoreExtensions_SetAndroidJavaObject_mE18B1644468157D40A94595975CFAA229523B235 ();
// 0x00000087 System.Void UnityEngine.Purchasing.SamsungAppsStoreExtensions::SetMode(UnityEngine.Purchasing.SamsungAppsMode)
extern void SamsungAppsStoreExtensions_SetMode_mE61F4639399357C86C99B9FCFAC57557330FDC08 ();
// 0x00000088 System.Void UnityEngine.Purchasing.SamsungAppsStoreExtensions::RestoreTransactions(System.Action`1<System.Boolean>)
extern void SamsungAppsStoreExtensions_RestoreTransactions_mFBB6B371266FD700DF722AF82648F2922B1E31D5 ();
// 0x00000089 System.Void UnityEngine.Purchasing.FakeUnityChannelConfiguration::.ctor()
extern void FakeUnityChannelConfiguration__ctor_mC7F0C4338B88A73167B2ACB881458FE70F8A5421 ();
// 0x0000008A System.Void UnityEngine.Purchasing.FakeUnityChannelExtensions::.ctor()
extern void FakeUnityChannelExtensions__ctor_m90374E16EC5817B712997005119E062C4AFBE22C ();
// 0x0000008B System.Void UnityEngine.Purchasing.FakeUDPExtension::.ctor()
extern void FakeUDPExtension__ctor_mB867DF605829D19AFCE77C2398502A4285839B56 ();
// 0x0000008C System.Void UnityEngine.Purchasing.INativeUDPStore::Initialize(System.Action`2<System.Boolean,System.String>)
// 0x0000008D System.Void UnityEngine.Purchasing.INativeUDPStore::Purchase(System.String,System.Action`2<System.Boolean,System.String>,System.String)
// 0x0000008E System.Void UnityEngine.Purchasing.INativeUDPStore::RetrieveProducts(System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>,System.Action`2<System.Boolean,System.String>)
// 0x0000008F System.Void UnityEngine.Purchasing.INativeUDPStore::FinishTransaction(UnityEngine.Purchasing.ProductDefinition,System.String)
// 0x00000090 System.String UnityEngine.Purchasing.UDP::get_Name()
extern void UDP_get_Name_m0710343BC35C86CEEF3D91B960E05C28DCC91044 ();
// 0x00000091 System.Void UnityEngine.Purchasing.UDPBindings::Initialize(System.Action`2<System.Boolean,System.String>)
extern void UDPBindings_Initialize_mB60F0A25A84D2567599C1001C56A8A31E7E60984 ();
// 0x00000092 System.Void UnityEngine.Purchasing.UDPBindings::Purchase(System.String,System.Action`2<System.Boolean,System.String>,System.String)
extern void UDPBindings_Purchase_m2E4C6A35CBD414BD7222CD74F29579B51846902C ();
// 0x00000093 System.Void UnityEngine.Purchasing.UDPBindings::RetrieveProducts(System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>,System.Action`2<System.Boolean,System.String>)
extern void UDPBindings_RetrieveProducts_m613FBAAC1B3C013FCA3E21A22AB1B1A64BDAC343 ();
// 0x00000094 System.Void UnityEngine.Purchasing.UDPBindings::FinishTransaction(UnityEngine.Purchasing.ProductDefinition,System.String)
extern void UDPBindings_FinishTransaction_mE36DE513656EB297079859B929AAE59808DD1D7B ();
// 0x00000095 UnityEngine.UDP.PurchaseInfo UnityEngine.Purchasing.UDPBindings::FindPurchaseInfo(System.String)
extern void UDPBindings_FindPurchaseInfo_mEE367AC13A7C28A3686217B4C11F3EB4ED674531 ();
// 0x00000096 System.Void UnityEngine.Purchasing.UDPBindings::OnInitialized(UnityEngine.UDP.UserInfo)
extern void UDPBindings_OnInitialized_m4147796694FB5D707C5A9F60CC331EFB334BAA11 ();
// 0x00000097 System.Void UnityEngine.Purchasing.UDPBindings::RetrieveProducts(System.String)
extern void UDPBindings_RetrieveProducts_m8168EA5CF3372DC4F6B6A0BCEED07E774B0DB729 ();
// 0x00000098 System.Void UnityEngine.Purchasing.UDPBindings::Purchase(System.String,System.String)
extern void UDPBindings_Purchase_mA7F5841DA552BAC83C622AEB50300C9315D91757 ();
// 0x00000099 System.Void UnityEngine.Purchasing.UDPBindings::FinishTransaction(System.String,System.String)
extern void UDPBindings_FinishTransaction_m00C71A282870BA753348E854919F2F7D747D98CC ();
// 0x0000009A System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.Purchasing.UDPBindings::StringPropertyToDictionary(System.Object)
extern void UDPBindings_StringPropertyToDictionary_mA7A8CAAA37DD25F7A843EC31075E24E646A590B1 ();
// 0x0000009B System.Void UnityEngine.Purchasing.UDPBindings::.ctor()
extern void UDPBindings__ctor_m2EF551717D945B4C0EA1AF95153CC9F3EBEA6761 ();
// 0x0000009C System.Void UnityEngine.Purchasing.UDPImpl::SetNativeStore(UnityEngine.Purchasing.INativeUDPStore)
extern void UDPImpl_SetNativeStore_m874352E8BC12DE27B4BDD1985848726D87CB36CD ();
// 0x0000009D System.Void UnityEngine.Purchasing.UDPImpl::Initialize(UnityEngine.Purchasing.Extension.IStoreCallback)
extern void UDPImpl_Initialize_m75E1116B65CF6D09D3C59D2FF0964628AEAB58CD ();
// 0x0000009E System.Void UnityEngine.Purchasing.UDPImpl::RetrieveProducts(System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>)
extern void UDPImpl_RetrieveProducts_m43FECD5E35F51FFDAB48FF49FF8A3CDC2ECF3E8C ();
// 0x0000009F System.Void UnityEngine.Purchasing.UDPImpl::Purchase(UnityEngine.Purchasing.ProductDefinition,System.String)
extern void UDPImpl_Purchase_mDE36070774F3E79DAC26004D461475260EAF413C ();
// 0x000000A0 System.Void UnityEngine.Purchasing.UDPImpl::FinishTransaction(UnityEngine.Purchasing.ProductDefinition,System.String)
extern void UDPImpl_FinishTransaction_m20B3718BD437F18D4DC9F9519DDEEF8511508D81 ();
// 0x000000A1 System.Void UnityEngine.Purchasing.UDPImpl::DictionaryToStringProperty(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Object)
extern void UDPImpl_DictionaryToStringProperty_mE68DF0CFA71560EE63A3E05926B6CE1922956E8F ();
// 0x000000A2 System.Void UnityEngine.Purchasing.UDPImpl::.ctor()
extern void UDPImpl__ctor_m6FC64FF86C754128D7CC1C7CEDCFE5F5D340DEE4 ();
// 0x000000A3 System.Void UnityEngine.Purchasing.UDPImpl_<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m7A1D41C3297EAE6EF09B7405D3FDF788CFE5CB0E ();
// 0x000000A4 System.Void UnityEngine.Purchasing.UDPImpl_<>c__DisplayClass7_0::<RetrieveProducts>b__0(System.Boolean,System.String)
extern void U3CU3Ec__DisplayClass7_0_U3CRetrieveProductsU3Eb__0_mA42C74DC47D02E5F876AF74B994DA283C8AAF5A2 ();
// 0x000000A5 System.Void UnityEngine.Purchasing.UDPImpl_<>c__DisplayClass7_0::<RetrieveProducts>b__1(System.Boolean,System.String)
extern void U3CU3Ec__DisplayClass7_0_U3CRetrieveProductsU3Eb__1_mAD3B48FD8B66AD992F5B85169AC7DD69CBB36039 ();
// 0x000000A6 System.Void UnityEngine.Purchasing.UDPImpl_<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m2FC07C2D4615C32BF3547CEA84409F924EA5BBA7 ();
// 0x000000A7 System.Void UnityEngine.Purchasing.UDPImpl_<>c__DisplayClass8_0::<Purchase>b__0(System.Boolean,System.String)
extern void U3CU3Ec__DisplayClass8_0_U3CPurchaseU3Eb__0_mC0D3F6E272E13661A3C896AACC1EDE52A6610D8F ();
// 0x000000A8 System.Void UnityEngine.Purchasing.AppleStoreImpl::.ctor(Uniject.IUtil)
extern void AppleStoreImpl__ctor_mAEC6782374073F02B686B1B9D4C65A90F413B0E7 ();
// 0x000000A9 System.Void UnityEngine.Purchasing.AppleStoreImpl::SetNativeStore(UnityEngine.Purchasing.INativeAppleStore)
extern void AppleStoreImpl_SetNativeStore_m047D2A24FCC7F538C285C13BAA38A899A337790A ();
// 0x000000AA System.Void UnityEngine.Purchasing.AppleStoreImpl::OnProductsRetrieved(System.String)
extern void AppleStoreImpl_OnProductsRetrieved_m3C3B057EFE60FF6DFD4C7668D728103534F59BB1 ();
// 0x000000AB System.Void UnityEngine.Purchasing.AppleStoreImpl::RestoreTransactions(System.Action`1<System.Boolean>)
extern void AppleStoreImpl_RestoreTransactions_m7E1AA7E2D67A69AC55E12BC9C672F49C699DBF40 ();
// 0x000000AC System.Void UnityEngine.Purchasing.AppleStoreImpl::RegisterPurchaseDeferredListener(System.Action`1<UnityEngine.Purchasing.Product>)
extern void AppleStoreImpl_RegisterPurchaseDeferredListener_m4E9DB86DC8E9298BC1F0EB01671D8864E6AD487B ();
// 0x000000AD System.Void UnityEngine.Purchasing.AppleStoreImpl::OnPurchaseDeferred(System.String)
extern void AppleStoreImpl_OnPurchaseDeferred_m21B0C908109F269236AD8DF339A3394F38E93685 ();
// 0x000000AE System.Void UnityEngine.Purchasing.AppleStoreImpl::OnPromotionalPurchaseAttempted(System.String)
extern void AppleStoreImpl_OnPromotionalPurchaseAttempted_m8CA1AF8F64C7FA5AD53DD5A5E54640199D76853D ();
// 0x000000AF System.Void UnityEngine.Purchasing.AppleStoreImpl::OnTransactionsRestoredSuccess()
extern void AppleStoreImpl_OnTransactionsRestoredSuccess_mBF3B092A79453DA3C88079AD8AA9B5078FD8A750 ();
// 0x000000B0 System.Void UnityEngine.Purchasing.AppleStoreImpl::OnTransactionsRestoredFail(System.String)
extern void AppleStoreImpl_OnTransactionsRestoredFail_m04D83F71F4C285611530C74D31FA04F9B49532F5 ();
// 0x000000B1 System.Void UnityEngine.Purchasing.AppleStoreImpl::OnAppReceiptRetrieved(System.String)
extern void AppleStoreImpl_OnAppReceiptRetrieved_m125538DA8CE1289976AA7A4C8D373147169A1B6D ();
// 0x000000B2 System.Void UnityEngine.Purchasing.AppleStoreImpl::OnAppReceiptRefreshedFailed()
extern void AppleStoreImpl_OnAppReceiptRefreshedFailed_m2929E90AA825CCE6AD61130AAC8DE0A55D95E70D ();
// 0x000000B3 System.Void UnityEngine.Purchasing.AppleStoreImpl::MessageCallback(System.String,System.String,System.String,System.String)
extern void AppleStoreImpl_MessageCallback_m1AFEBE0209301D9DE3F08BF561DC43984AEC3251 ();
// 0x000000B4 System.Void UnityEngine.Purchasing.AppleStoreImpl::ProcessMessage(System.String,System.String,System.String,System.String)
extern void AppleStoreImpl_ProcessMessage_m97E280642E0C95EB7D9AE6C57BD06409CE1D4BFB ();
// 0x000000B5 System.Void UnityEngine.Purchasing.AppleStoreImpl::OnPurchaseSucceeded(System.String,System.String,System.String)
extern void AppleStoreImpl_OnPurchaseSucceeded_m3857A395DAC7B2B05E1D6EDAE8BF24073D47C8B1 ();
// 0x000000B6 UnityEngine.Purchasing.Security.AppleReceipt UnityEngine.Purchasing.AppleStoreImpl::getAppleReceiptFromBase64String(System.String)
extern void AppleStoreImpl_getAppleReceiptFromBase64String_m69D715DB6897A3F7817765FAE04AB263A79022C3 ();
// 0x000000B7 System.Boolean UnityEngine.Purchasing.AppleStoreImpl::isValidPurchaseState(UnityEngine.Purchasing.Security.AppleReceipt,System.String)
extern void AppleStoreImpl_isValidPurchaseState_m801182B6892956E69E290B2559FA8C1B1865D117 ();
// 0x000000B8 System.Void UnityEngine.Purchasing.AppleStoreImpl_<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_mEF76E555DEC2D18A456AB98F14FA14E084053EC6 ();
// 0x000000B9 System.Boolean UnityEngine.Purchasing.AppleStoreImpl_<>c__DisplayClass23_0::<OnProductsRetrieved>b__0(UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt)
extern void U3CU3Ec__DisplayClass23_0_U3COnProductsRetrievedU3Eb__0_m8C494BC9AB278C6B70007DD7A1966E5A2E9ECF45 ();
// 0x000000BA System.Void UnityEngine.Purchasing.AppleStoreImpl_<>c::.cctor()
extern void U3CU3Ec__cctor_m436F76A4CC0BC7119D77378DD18E7D0EDB313236 ();
// 0x000000BB System.Void UnityEngine.Purchasing.AppleStoreImpl_<>c::.ctor()
extern void U3CU3Ec__ctor_m2A70E8334CFF3794283B6BB4691C29C22F374D33 ();
// 0x000000BC System.Int32 UnityEngine.Purchasing.AppleStoreImpl_<>c::<OnProductsRetrieved>b__23_1(UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt,UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt)
extern void U3CU3Ec_U3COnProductsRetrievedU3Eb__23_1_mA73EFEBE1374DA7A9D01A4EC96CBD1EB8DA18314 ();
// 0x000000BD System.Int32 UnityEngine.Purchasing.AppleStoreImpl_<>c::<isValidPurchaseState>b__40_1(UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt,UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt)
extern void U3CU3Ec_U3CisValidPurchaseStateU3Eb__40_1_mE24747560E0DB04029406086B073A49EDEE6596C ();
// 0x000000BE System.Void UnityEngine.Purchasing.AppleStoreImpl_<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_m303A2CB65F66B65E2F4B472D7C1C487457FB69F6 ();
// 0x000000BF System.Void UnityEngine.Purchasing.AppleStoreImpl_<>c__DisplayClass36_0::<MessageCallback>b__0()
extern void U3CU3Ec__DisplayClass36_0_U3CMessageCallbackU3Eb__0_m94A4FD7E1B7F9A3EF1835B4B1185955FB380C9F4 ();
// 0x000000C0 System.Void UnityEngine.Purchasing.AppleStoreImpl_<>c__DisplayClass40_0::.ctor()
extern void U3CU3Ec__DisplayClass40_0__ctor_m25071E24F4758ABF066B8803D41CF36CD70B5D22 ();
// 0x000000C1 System.Boolean UnityEngine.Purchasing.AppleStoreImpl_<>c__DisplayClass40_0::<isValidPurchaseState>b__0(UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt)
extern void U3CU3Ec__DisplayClass40_0_U3CisValidPurchaseStateU3Eb__0_m042A5EBCC9EFA6D00EC5E82A297A76F44F233F76 ();
// 0x000000C2 System.Void UnityEngine.Purchasing.FakeAppleConfiguation::.ctor()
extern void FakeAppleConfiguation__ctor_m98AA1830AC7F3C5934818B2C4F315534F327DCE6 ();
// 0x000000C3 System.Void UnityEngine.Purchasing.FakeAppleExtensions::RestoreTransactions(System.Action`1<System.Boolean>)
extern void FakeAppleExtensions_RestoreTransactions_m0042645492D2E541A3A7A45BEBFC52507897A518 ();
// 0x000000C4 System.Void UnityEngine.Purchasing.FakeAppleExtensions::RegisterPurchaseDeferredListener(System.Action`1<UnityEngine.Purchasing.Product>)
extern void FakeAppleExtensions_RegisterPurchaseDeferredListener_m71DB6504006F5E41FA3B3FE48210D1ADBFE895C9 ();
// 0x000000C5 System.Void UnityEngine.Purchasing.FakeAppleExtensions::.ctor()
extern void FakeAppleExtensions__ctor_m0CAA9CE6D322BAFEEF692394C29C1F960E57B0E4 ();
// 0x000000C6 System.Void UnityEngine.Purchasing.IAppleExtensions::RestoreTransactions(System.Action`1<System.Boolean>)
// 0x000000C7 System.Void UnityEngine.Purchasing.IAppleExtensions::RegisterPurchaseDeferredListener(System.Action`1<UnityEngine.Purchasing.Product>)
// 0x000000C8 UnityEngine.Purchasing.INativeStore UnityEngine.Purchasing.INativeStoreProvider::GetAndroidStore(UnityEngine.Purchasing.IUnityCallback,UnityEngine.Purchasing.AppStore,UnityEngine.Purchasing.Extension.IPurchasingBinder,Uniject.IUtil)
// 0x000000C9 UnityEngine.Purchasing.INativeAppleStore UnityEngine.Purchasing.INativeStoreProvider::GetStorekit(UnityEngine.Purchasing.IUnityCallback)
// 0x000000CA UnityEngine.Purchasing.INativeTizenStore UnityEngine.Purchasing.INativeStoreProvider::GetTizenStore(UnityEngine.Purchasing.IUnityCallback,UnityEngine.Purchasing.Extension.IPurchasingBinder)
// 0x000000CB UnityEngine.Purchasing.INativeFacebookStore UnityEngine.Purchasing.INativeStoreProvider::GetFacebookStore()
// 0x000000CC System.Void UnityEngine.Purchasing.IStoreInternal::SetModule(UnityEngine.Purchasing.StandardPurchasingModule)
// 0x000000CD System.Void UnityEngine.Purchasing.JSONStore::.ctor()
extern void JSONStore__ctor_mC690C31EF9D9B8966C720B2BC2190FF79E60A57A ();
// 0x000000CE System.Void UnityEngine.Purchasing.JSONStore::SetNativeStore(UnityEngine.Purchasing.INativeStore)
extern void JSONStore_SetNativeStore_m6B53D93734F455C72FCA8AA0A04188229D691473 ();
// 0x000000CF System.Void UnityEngine.Purchasing.JSONStore::UnityEngine.Purchasing.IStoreInternal.SetModule(UnityEngine.Purchasing.StandardPurchasingModule)
extern void JSONStore_UnityEngine_Purchasing_IStoreInternal_SetModule_m53A4CEBC95BC512088AD2F2E47597EB174B7646D ();
// 0x000000D0 System.Void UnityEngine.Purchasing.JSONStore::Initialize(UnityEngine.Purchasing.Extension.IStoreCallback)
extern void JSONStore_Initialize_m5658965AF3AC26BD073D286C6351CB2D5ECA84D4 ();
// 0x000000D1 System.Void UnityEngine.Purchasing.JSONStore::RetrieveProducts(System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>)
extern void JSONStore_RetrieveProducts_mA76F64EE917B1DF7FC23909921174DE9733CA441 ();
// 0x000000D2 System.Void UnityEngine.Purchasing.JSONStore::Purchase(UnityEngine.Purchasing.ProductDefinition,System.String)
extern void JSONStore_Purchase_m7D156301950C35DFBC4F599DE09873B2EB9826BB ();
// 0x000000D3 System.Void UnityEngine.Purchasing.JSONStore::FinishTransaction(UnityEngine.Purchasing.ProductDefinition,System.String)
extern void JSONStore_FinishTransaction_mA233230DD1A80ACA1D3C1C130C5E44E06E95D2D5 ();
// 0x000000D4 System.Void UnityEngine.Purchasing.JSONStore::OnSetupFailed(System.String)
extern void JSONStore_OnSetupFailed_mB90D5D7B294947B141AEC41F035BB3E4C9DF9757 ();
// 0x000000D5 System.Void UnityEngine.Purchasing.JSONStore::OnProductsRetrieved(System.String)
extern void JSONStore_OnProductsRetrieved_mEC0780AD5E5EB570014B3B01ED23ECE2CCE9E9DB ();
// 0x000000D6 System.Void UnityEngine.Purchasing.JSONStore::OnPurchaseSucceeded(System.String,System.String,System.String)
extern void JSONStore_OnPurchaseSucceeded_m3FDCC6BCEA47445FF2A4277C3B9AA94C62590426 ();
// 0x000000D7 System.Void UnityEngine.Purchasing.JSONStore::OnPurchaseFailed(System.String)
extern void JSONStore_OnPurchaseFailed_m3E388358865346F3A9970D95B290649F69BA4960 ();
// 0x000000D8 System.Void UnityEngine.Purchasing.JSONStore::OnPurchaseFailed(UnityEngine.Purchasing.Extension.PurchaseFailureDescription,System.String)
extern void JSONStore_OnPurchaseFailed_mBF39584A62E58C74E02F78BABECAD84BEA96BBBC ();
// 0x000000D9 UnityEngine.Purchasing.Extension.PurchaseFailureDescription UnityEngine.Purchasing.JSONStore::GetLastPurchaseFailureDescription()
extern void JSONStore_GetLastPurchaseFailureDescription_m330B23E8D2114DE133B721C23630B5AD928B57A2 ();
// 0x000000DA UnityEngine.Purchasing.StoreSpecificPurchaseErrorCode UnityEngine.Purchasing.JSONStore::GetLastStoreSpecificPurchaseErrorCode()
extern void JSONStore_GetLastStoreSpecificPurchaseErrorCode_m95432D81D1F7D72FCEBABFD11E3011668B9BF5B5 ();
// 0x000000DB UnityEngine.Purchasing.StoreSpecificPurchaseErrorCode UnityEngine.Purchasing.JSONStore::ParseStoreSpecificPurchaseErrorCode(System.String)
extern void JSONStore_ParseStoreSpecificPurchaseErrorCode_mCFF280967DF9BBDF5CD6C9DD119E547EAEDBD9EE ();
// 0x000000DC UnityEngine.Purchasing.INativeStore UnityEngine.Purchasing.NativeStoreProvider::GetAndroidStore(UnityEngine.Purchasing.IUnityCallback,UnityEngine.Purchasing.AppStore,UnityEngine.Purchasing.Extension.IPurchasingBinder,Uniject.IUtil)
extern void NativeStoreProvider_GetAndroidStore_m1196800523C8E16499417841B8151B4A00700F54 ();
// 0x000000DD UnityEngine.Purchasing.INativeStore UnityEngine.Purchasing.NativeStoreProvider::GetAndroidStoreHelper(UnityEngine.Purchasing.IUnityCallback,UnityEngine.Purchasing.AppStore,UnityEngine.Purchasing.Extension.IPurchasingBinder,Uniject.IUtil)
extern void NativeStoreProvider_GetAndroidStoreHelper_mCFEDE06D2D8E302CA2470AE537954BBBE333F3AD ();
// 0x000000DE UnityEngine.Purchasing.INativeAppleStore UnityEngine.Purchasing.NativeStoreProvider::GetStorekit(UnityEngine.Purchasing.IUnityCallback)
extern void NativeStoreProvider_GetStorekit_m39ABB566FCE2C7E5BD26A09ECA93CD48C82BBBDB ();
// 0x000000DF UnityEngine.Purchasing.INativeTizenStore UnityEngine.Purchasing.NativeStoreProvider::GetTizenStore(UnityEngine.Purchasing.IUnityCallback,UnityEngine.Purchasing.Extension.IPurchasingBinder)
extern void NativeStoreProvider_GetTizenStore_m03DEE7DC0328CC8BCAA16EEA4E9F7435AA626397 ();
// 0x000000E0 UnityEngine.Purchasing.INativeFacebookStore UnityEngine.Purchasing.NativeStoreProvider::GetFacebookStore()
extern void NativeStoreProvider_GetFacebookStore_m980371E3FFB3C2537CFBE4A97177C8C75241359F ();
// 0x000000E1 System.Void UnityEngine.Purchasing.NativeStoreProvider::.ctor()
extern void NativeStoreProvider__ctor_m0B40B71A9CA0D70CCF43332D39AC8478F54EC0D8 ();
// 0x000000E2 UnityEngine.Purchasing.CloudCatalogImpl UnityEngine.Purchasing.CloudCatalogImpl::CreateInstance(System.String)
extern void CloudCatalogImpl_CreateInstance_m1AA64F576A28042FC7E851B3DFE874BDB469A0E8 ();
// 0x000000E3 System.Void UnityEngine.Purchasing.CloudCatalogImpl::.ctor(UnityEngine.Purchasing.IAsyncWebUtil,System.String,UnityEngine.ILogger,System.String,System.String)
extern void CloudCatalogImpl__ctor_mC098CCE60F7F47D7D363C96B570A25E0F162A0FB ();
// 0x000000E4 System.Void UnityEngine.Purchasing.CloudCatalogImpl::FetchProducts(System.Action`1<System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>>)
extern void CloudCatalogImpl_FetchProducts_mB566664220972396130C4DF65EB94AAE6C2F7ECD ();
// 0x000000E5 System.Void UnityEngine.Purchasing.CloudCatalogImpl::FetchProducts(System.Action`1<System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>>,System.Int32)
extern void CloudCatalogImpl_FetchProducts_mB4A18588B0F8977F26E8E1D9F6D3078F9944F9DC ();
// 0x000000E6 System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition> UnityEngine.Purchasing.CloudCatalogImpl::ParseProductsFromJSON(System.String,System.String)
extern void CloudCatalogImpl_ParseProductsFromJSON_mE4E0F0C7404FE2AD72D00A13746E54669375BE3B ();
// 0x000000E7 System.String UnityEngine.Purchasing.CloudCatalogImpl::CamelCaseToSnakeCase(System.String)
extern void CloudCatalogImpl_CamelCaseToSnakeCase_m73A1CB1CDF08A67B940F4EDE0478D45DF01851B2 ();
// 0x000000E8 System.Void UnityEngine.Purchasing.CloudCatalogImpl::TryPersistCatalog(System.String)
extern void CloudCatalogImpl_TryPersistCatalog_mD19B1241ED9DB8D1F3111C2FE1CA6AD4E5BB7957 ();
// 0x000000E9 System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition> UnityEngine.Purchasing.CloudCatalogImpl::TryLoadCachedCatalog()
extern void CloudCatalogImpl_TryLoadCachedCatalog_m2B909643837094DAB44C9226AED38DC6A1A7BAB9 ();
// 0x000000EA System.Void UnityEngine.Purchasing.CloudCatalogImpl_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m1CB7873CC19257089AF8D0961EAE5D84125CB97C ();
// 0x000000EB System.Void UnityEngine.Purchasing.CloudCatalogImpl_<>c__DisplayClass10_0::<FetchProducts>b__0(System.String)
extern void U3CU3Ec__DisplayClass10_0_U3CFetchProductsU3Eb__0_mF967FD4EBADB30A08B0876AB0AAFB870B0029286 ();
// 0x000000EC System.Void UnityEngine.Purchasing.CloudCatalogImpl_<>c__DisplayClass10_0::<FetchProducts>b__1(System.String)
extern void U3CU3Ec__DisplayClass10_0_U3CFetchProductsU3Eb__1_m2F08B283C9CFA2D8CA240C1E1296E29530B2C90C ();
// 0x000000ED System.Void UnityEngine.Purchasing.CloudCatalogImpl_<>c__DisplayClass10_0::<FetchProducts>b__2()
extern void U3CU3Ec__DisplayClass10_0_U3CFetchProductsU3Eb__2_m8ABF498B541EA6ABCC609134694B24551227C058 ();
// 0x000000EE System.Void UnityEngine.Purchasing.CloudCatalogImpl_<>c::.cctor()
extern void U3CU3Ec__cctor_m30C3BC52B28B791A13095D086D79182FE936B811 ();
// 0x000000EF System.Void UnityEngine.Purchasing.CloudCatalogImpl_<>c::.ctor()
extern void U3CU3Ec__ctor_mE5289AEE490022C4B29A79AA8381C86762FCC538 ();
// 0x000000F0 System.String UnityEngine.Purchasing.CloudCatalogImpl_<>c::<CamelCaseToSnakeCase>b__12_0(System.Char,System.Int32)
extern void U3CU3Ec_U3CCamelCaseToSnakeCaseU3Eb__12_0_m95EFACC4FAC7A902D618D28D7DF6362FEFF1CC48 ();
// 0x000000F1 System.String UnityEngine.Purchasing.CloudCatalogImpl_<>c::<CamelCaseToSnakeCase>b__12_1(System.String,System.String)
extern void U3CU3Ec_U3CCamelCaseToSnakeCaseU3Eb__12_1_mD002FB9D81947E50DBBE863B519FCF5F390F4156 ();
// 0x000000F2 System.Void UnityEngine.Purchasing.FakeManagedStoreConfig::.ctor()
extern void FakeManagedStoreConfig__ctor_m00A8F240B1F66BA3B7E0A582131CF7670A25A8B1 ();
// 0x000000F3 System.Void UnityEngine.Purchasing.FakeManagedStoreExtensions::.ctor()
extern void FakeManagedStoreExtensions__ctor_m382473F8F857791F8B680663AB814E8ABC61E1D0 ();
// 0x000000F4 UnityEngine.Purchasing.StoreCatalogImpl UnityEngine.Purchasing.StoreCatalogImpl::CreateInstance(System.String,System.String,UnityEngine.Purchasing.IAsyncWebUtil,UnityEngine.ILogger,Uniject.IUtil)
extern void StoreCatalogImpl_CreateInstance_m4419704ED56FCCF3118D5A702DF5B53C5B3E091A ();
// 0x000000F5 System.Void UnityEngine.Purchasing.StoreCatalogImpl::.ctor(UnityEngine.Purchasing.IAsyncWebUtil,UnityEngine.ILogger,System.String,System.String,UnityEngine.Purchasing.FileReference)
extern void StoreCatalogImpl__ctor_m66D05EEF7E176F6371317B973755694C455C89E7 ();
// 0x000000F6 System.Boolean UnityEngine.Purchasing.AdsIPC::InitAdsIPC(Uniject.IUtil)
extern void AdsIPC_InitAdsIPC_m4139F1FC1EF78DBD3F3A8C3BC099B4D69C6A5354 ();
// 0x000000F7 System.Boolean UnityEngine.Purchasing.AdsIPC::VerifyMethodExists()
extern void AdsIPC_VerifyMethodExists_m360B1626186241503F630453C0B95328D30B8218 ();
// 0x000000F8 System.Void UnityEngine.Purchasing.AdsIPC::.cctor()
extern void AdsIPC__cctor_mAEEEC6172E0E37868B7EC24F5B1DF1E3ED81D651 ();
// 0x000000F9 System.Void UnityEngine.Purchasing.EventQueue::.ctor(Uniject.IUtil,UnityEngine.Purchasing.IAsyncWebUtil)
extern void EventQueue__ctor_m1FFBCE42BC0A318E425D12E6FF353C88F36C12A6 ();
// 0x000000FA UnityEngine.Purchasing.EventQueue UnityEngine.Purchasing.EventQueue::Instance(Uniject.IUtil,UnityEngine.Purchasing.IAsyncWebUtil)
extern void EventQueue_Instance_m8AA0A9B8B2ABFDC92CEBB1975247BCFDF651BC86 ();
// 0x000000FB System.Void UnityEngine.Purchasing.EventQueue::SetAdsUrl(System.String)
extern void EventQueue_SetAdsUrl_m99D68C6E140201522C8FFD9D8549B9137F53AD4D ();
// 0x000000FC System.Void UnityEngine.Purchasing.EventQueue::SetIapUrl(System.String)
extern void EventQueue_SetIapUrl_m08748593860529F3CFD9647376733105D5D2CB69 ();
// 0x000000FD System.Void UnityEngine.Purchasing.AsyncWebUtil::Get(System.String,System.Action`1<System.String>,System.Action`1<System.String>,System.Int32)
extern void AsyncWebUtil_Get_m6D017D90320746E43A67EC2862A1AE6BD711C19B ();
// 0x000000FE System.Void UnityEngine.Purchasing.AsyncWebUtil::Post(System.String,System.String,System.Action`1<System.String>,System.Action`1<System.String>,System.Int32)
extern void AsyncWebUtil_Post_m193708E47AE2C30F356588A0C3FEC8291DC8A3A8 ();
// 0x000000FF System.Void UnityEngine.Purchasing.AsyncWebUtil::Schedule(System.Action,System.Int32)
extern void AsyncWebUtil_Schedule_mF0CD5967A6E9AF68FF80A9731ECABB5D8770915E ();
// 0x00000100 System.Collections.IEnumerator UnityEngine.Purchasing.AsyncWebUtil::DoInvoke(System.Action,System.Int32)
extern void AsyncWebUtil_DoInvoke_mBAD2A93E08A6BA10DB4E04A899131055D8D32E24 ();
// 0x00000101 System.Collections.IEnumerator UnityEngine.Purchasing.AsyncWebUtil::Process(UnityEngine.WWW,System.Action`1<System.String>,System.Action`1<System.String>,System.Int32)
extern void AsyncWebUtil_Process_mE07B0BB03E8724BA3E817C71248AC9F5E49EDBAA ();
// 0x00000102 System.Void UnityEngine.Purchasing.AsyncWebUtil::.ctor()
extern void AsyncWebUtil__ctor_mCCDFF67AE910DF60AB52A3ABD07D200BE3D4F774 ();
// 0x00000103 System.Void UnityEngine.Purchasing.AsyncWebUtil_<DoInvoke>d__3::.ctor(System.Int32)
extern void U3CDoInvokeU3Ed__3__ctor_m40185A1EE5CFFB147DE4BD16B44DF0D9770FFD63 ();
// 0x00000104 System.Void UnityEngine.Purchasing.AsyncWebUtil_<DoInvoke>d__3::System.IDisposable.Dispose()
extern void U3CDoInvokeU3Ed__3_System_IDisposable_Dispose_m6CB889BB4BFF12E8944AD3D5019606EF251AE4B8 ();
// 0x00000105 System.Boolean UnityEngine.Purchasing.AsyncWebUtil_<DoInvoke>d__3::MoveNext()
extern void U3CDoInvokeU3Ed__3_MoveNext_mA510EC21BCCC499E18D2F787EDA61BDA154B3BF8 ();
// 0x00000106 System.Object UnityEngine.Purchasing.AsyncWebUtil_<DoInvoke>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDoInvokeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA8DE06116F6430932B6CA60363A7D9B7ACCFB08F ();
// 0x00000107 System.Void UnityEngine.Purchasing.AsyncWebUtil_<DoInvoke>d__3::System.Collections.IEnumerator.Reset()
extern void U3CDoInvokeU3Ed__3_System_Collections_IEnumerator_Reset_m17644E4C6C15ECE6F79DFDD74E844F712949D4FE ();
// 0x00000108 System.Object UnityEngine.Purchasing.AsyncWebUtil_<DoInvoke>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CDoInvokeU3Ed__3_System_Collections_IEnumerator_get_Current_mE727FBB6EA1076ADE927A0E87DB79D7FDB3F9295 ();
// 0x00000109 System.Void UnityEngine.Purchasing.AsyncWebUtil_<Process>d__4::.ctor(System.Int32)
extern void U3CProcessU3Ed__4__ctor_m754F9594DDCBD85B6587666A027FE76D78E564EA ();
// 0x0000010A System.Void UnityEngine.Purchasing.AsyncWebUtil_<Process>d__4::System.IDisposable.Dispose()
extern void U3CProcessU3Ed__4_System_IDisposable_Dispose_m3BE20CB46FE0E9DDECC186752705AAD6A1E12DA6 ();
// 0x0000010B System.Boolean UnityEngine.Purchasing.AsyncWebUtil_<Process>d__4::MoveNext()
extern void U3CProcessU3Ed__4_MoveNext_m3619F13E3F9FD0807130F20B835AFC03525FA05A ();
// 0x0000010C System.Object UnityEngine.Purchasing.AsyncWebUtil_<Process>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF590CAD268580BBB0ECF94F487AE5DDE40796E9 ();
// 0x0000010D System.Void UnityEngine.Purchasing.AsyncWebUtil_<Process>d__4::System.Collections.IEnumerator.Reset()
extern void U3CProcessU3Ed__4_System_Collections_IEnumerator_Reset_m863BB0046EC53CD6A1066730293CD841EEEEA9AA ();
// 0x0000010E System.Object UnityEngine.Purchasing.AsyncWebUtil_<Process>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CProcessU3Ed__4_System_Collections_IEnumerator_get_Current_mD354922ABDF987AA80DA9B1E7B0C782C12F74C01 ();
// 0x0000010F System.Void UnityEngine.Purchasing.IAsyncWebUtil::Get(System.String,System.Action`1<System.String>,System.Action`1<System.String>,System.Int32)
// 0x00000110 System.Void UnityEngine.Purchasing.IAsyncWebUtil::Schedule(System.Action,System.Int32)
// 0x00000111 System.String UnityEngine.Purchasing.QueryHelper::ToQueryString(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void QueryHelper_ToQueryString_m4589D7D3119E03772920457BD048125C0DADE53D ();
// 0x00000112 System.Void UnityEngine.Purchasing.Price::OnBeforeSerialize()
extern void Price_OnBeforeSerialize_mEBDB52261FAD4BB758A3393D0785A6137934B57D ();
// 0x00000113 System.Void UnityEngine.Purchasing.Price::OnAfterDeserialize()
extern void Price_OnAfterDeserialize_m385CE4D04E076761BA0C605B68C91B29F4B99F20 ();
// 0x00000114 System.Void UnityEngine.Purchasing.Price::.ctor()
extern void Price__ctor_mB1535A2F6959285D025541ADE8A637ADE4710585 ();
// 0x00000115 System.String UnityEngine.Purchasing.LocalizedProductDescription::get_Title()
extern void LocalizedProductDescription_get_Title_m0432DC6615CF588EE3277A082B5F76D307B99503 ();
// 0x00000116 System.String UnityEngine.Purchasing.LocalizedProductDescription::get_Description()
extern void LocalizedProductDescription_get_Description_m6602E8CCC5D834D31B7F849B5772589EC48BBD9B ();
// 0x00000117 System.String UnityEngine.Purchasing.LocalizedProductDescription::DecodeNonLatinCharacters(System.String)
extern void LocalizedProductDescription_DecodeNonLatinCharacters_m408241D24FC656BC5DCF12E715336CDDA86A3FEE ();
// 0x00000118 System.Void UnityEngine.Purchasing.LocalizedProductDescription::.ctor()
extern void LocalizedProductDescription__ctor_mD0C6C96B08AD90504F712542A753B7B30CBC95D3 ();
// 0x00000119 System.Void UnityEngine.Purchasing.LocalizedProductDescription_<>c::.cctor()
extern void U3CU3Ec__cctor_mBB46B53847924EE6AA11498CBDE3A4BC33890B0B ();
// 0x0000011A System.Void UnityEngine.Purchasing.LocalizedProductDescription_<>c::.ctor()
extern void U3CU3Ec__ctor_mD8A8762E5FB1B50A84E12F29A65D5BFE0733FE84 ();
// 0x0000011B System.String UnityEngine.Purchasing.LocalizedProductDescription_<>c::<DecodeNonLatinCharacters>b__11_0(System.Text.RegularExpressions.Match)
extern void U3CU3Ec_U3CDecodeNonLatinCharactersU3Eb__11_0_mDE1E5E9761E76D94295AE1173EBCEA4F0F87CD1A ();
// 0x0000011C System.String UnityEngine.Purchasing.ProductCatalogPayout::get_typeString()
extern void ProductCatalogPayout_get_typeString_mE2D76BF97B5F5A10DB9A63A6D67E552CED6A4BC1 ();
// 0x0000011D System.String UnityEngine.Purchasing.ProductCatalogPayout::get_subtype()
extern void ProductCatalogPayout_get_subtype_m3E06D487DA9DFBADB3981720D978C7CCCACDC28F ();
// 0x0000011E System.Double UnityEngine.Purchasing.ProductCatalogPayout::get_quantity()
extern void ProductCatalogPayout_get_quantity_m92D5468328FD0B29609E9A50006FD62DDE732288 ();
// 0x0000011F System.String UnityEngine.Purchasing.ProductCatalogPayout::get_data()
extern void ProductCatalogPayout_get_data_m7955A4BDA357760B6993A748F57BC91A2FBDC8EC ();
// 0x00000120 System.Void UnityEngine.Purchasing.ProductCatalogPayout::.ctor()
extern void ProductCatalogPayout__ctor_m53F18B37C03F42785BEEDCD8C8337107A8905494 ();
// 0x00000121 System.Collections.Generic.IList`1<UnityEngine.Purchasing.ProductCatalogPayout> UnityEngine.Purchasing.ProductCatalogItem::get_Payouts()
extern void ProductCatalogItem_get_Payouts_m2846D0CC396C637AB69A58AF03A8B0C380F7081A ();
// 0x00000122 System.Collections.Generic.ICollection`1<UnityEngine.Purchasing.StoreID> UnityEngine.Purchasing.ProductCatalogItem::get_allStoreIDs()
extern void ProductCatalogItem_get_allStoreIDs_mE94829DBC77372C8EA978DF7706176A7AC175EE3 ();
// 0x00000123 System.Void UnityEngine.Purchasing.ProductCatalogItem::.ctor()
extern void ProductCatalogItem__ctor_m9A763F961DE12BE58E8B60B57750E93049A2B7C5 ();
// 0x00000124 System.Collections.Generic.ICollection`1<UnityEngine.Purchasing.ProductCatalogItem> UnityEngine.Purchasing.ProductCatalog::get_allProducts()
extern void ProductCatalog_get_allProducts_m3AA6CEC7A45E606CDA43B8A67797B50AA9575EFB ();
// 0x00000125 System.Collections.Generic.ICollection`1<UnityEngine.Purchasing.ProductCatalogItem> UnityEngine.Purchasing.ProductCatalog::get_allValidProducts()
extern void ProductCatalog_get_allValidProducts_mF4280873CCE80F06264BACC1DB67AC7FEBCBE53A ();
// 0x00000126 System.Void UnityEngine.Purchasing.ProductCatalog::Initialize()
extern void ProductCatalog_Initialize_mFFDE0D11B1B77B1BB6AD4C2137EABC2B6AB46AB8 ();
// 0x00000127 System.Void UnityEngine.Purchasing.ProductCatalog::Initialize(UnityEngine.Purchasing.IProductCatalogImpl)
extern void ProductCatalog_Initialize_mD6F8F97A0759177186E0FB992D0908646184B977 ();
// 0x00000128 System.Boolean UnityEngine.Purchasing.ProductCatalog::IsEmpty()
extern void ProductCatalog_IsEmpty_mB44278AB7415B9B3B5FD96A1A2E5B94D130F2FAA ();
// 0x00000129 UnityEngine.Purchasing.ProductCatalog UnityEngine.Purchasing.ProductCatalog::Deserialize(System.String)
extern void ProductCatalog_Deserialize_m0362D3F5370B960E6C0347E92DF1803A37CB9E02 ();
// 0x0000012A UnityEngine.Purchasing.ProductCatalog UnityEngine.Purchasing.ProductCatalog::FromTextAsset(UnityEngine.TextAsset)
extern void ProductCatalog_FromTextAsset_m80DAF012F02AA671A5E31CAFD1CF89EE2F97F550 ();
// 0x0000012B UnityEngine.Purchasing.ProductCatalog UnityEngine.Purchasing.ProductCatalog::LoadDefaultCatalog()
extern void ProductCatalog_LoadDefaultCatalog_mE33487264ED550B3816900791AC7920BB53EEA39 ();
// 0x0000012C System.Void UnityEngine.Purchasing.ProductCatalog::.ctor()
extern void ProductCatalog__ctor_m8951B1D5BCBE61C5BFA49CA2A7E3EBF1457A9450 ();
// 0x0000012D System.Void UnityEngine.Purchasing.ProductCatalog_<>c::.cctor()
extern void U3CU3Ec__cctor_m7A8D524D5764215776F18D67F7066639E855C9E6 ();
// 0x0000012E System.Void UnityEngine.Purchasing.ProductCatalog_<>c::.ctor()
extern void U3CU3Ec__ctor_m1B02EC40E2A7A30038A238EB5DD969F93A8F79C4 ();
// 0x0000012F System.Boolean UnityEngine.Purchasing.ProductCatalog_<>c::<get_allValidProducts>b__8_0(UnityEngine.Purchasing.ProductCatalogItem)
extern void U3CU3Ec_U3Cget_allValidProductsU3Eb__8_0_mFC1BC7CC66D35B647A3810A4876CEFA3467FCB45 ();
// 0x00000130 UnityEngine.Purchasing.ProductCatalog UnityEngine.Purchasing.IProductCatalogImpl::LoadDefaultCatalog()
// 0x00000131 UnityEngine.Purchasing.ProductCatalog UnityEngine.Purchasing.ProductCatalogImpl::LoadDefaultCatalog()
extern void ProductCatalogImpl_LoadDefaultCatalog_m35DA0FFDA85D7A2BC41CD615176386E3735759E1 ();
// 0x00000132 System.Void UnityEngine.Purchasing.ProductCatalogImpl::.ctor()
extern void ProductCatalogImpl__ctor_mF5BED5B749F90CD02A001265FC954E2FEC0EEDAE ();
// 0x00000133 System.String UnityEngine.Purchasing.ProfileData::get_AppId()
extern void ProfileData_get_AppId_m051152B180EE7BB8B4601AE5E8A7AFD5277A739B ();
// 0x00000134 System.Void UnityEngine.Purchasing.ProfileData::set_AppId(System.String)
extern void ProfileData_set_AppId_m54F28063B0708D703147406F84120542885B90C3 ();
// 0x00000135 System.String UnityEngine.Purchasing.ProfileData::get_UserId()
extern void ProfileData_get_UserId_mD6B78F3D39D4D6ED8E53271563AE52DB13A30A82 ();
// 0x00000136 System.Void UnityEngine.Purchasing.ProfileData::set_UserId(System.String)
extern void ProfileData_set_UserId_m4BB99D1DA889F57E9B202A5BE5982B05F987A406 ();
// 0x00000137 System.UInt64 UnityEngine.Purchasing.ProfileData::get_SessionId()
extern void ProfileData_get_SessionId_m8D0E16CD08475FFE033F83BA9E8307E6CB85E62D ();
// 0x00000138 System.Void UnityEngine.Purchasing.ProfileData::set_SessionId(System.UInt64)
extern void ProfileData_set_SessionId_m22FE026F0161D65052BC8751523B616A13B7A042 ();
// 0x00000139 System.String UnityEngine.Purchasing.ProfileData::get_Platform()
extern void ProfileData_get_Platform_m5E3D6BE3D7540457449BDC89565DDDC5B90CA53C ();
// 0x0000013A System.Void UnityEngine.Purchasing.ProfileData::set_Platform(System.String)
extern void ProfileData_set_Platform_m27CF2728A8A67D5DA6F12E9C7D79AEC9407E55C2 ();
// 0x0000013B System.Int32 UnityEngine.Purchasing.ProfileData::get_PlatformId()
extern void ProfileData_get_PlatformId_m656E35580FA5398A7A35BF4FF2AD65C0B80D7B8C ();
// 0x0000013C System.Void UnityEngine.Purchasing.ProfileData::set_PlatformId(System.Int32)
extern void ProfileData_set_PlatformId_m333B2ED653CAC85373B95416CD55830737106387 ();
// 0x0000013D System.String UnityEngine.Purchasing.ProfileData::get_SdkVer()
extern void ProfileData_get_SdkVer_m772F22F37E1DEFAAEC03B59BC97787B72603D6D4 ();
// 0x0000013E System.Void UnityEngine.Purchasing.ProfileData::set_SdkVer(System.String)
extern void ProfileData_set_SdkVer_mD5679B73DF7FBD0C62E5A57BCADCFD7D60B8BD4A ();
// 0x0000013F System.String UnityEngine.Purchasing.ProfileData::get_OsVer()
extern void ProfileData_get_OsVer_mEF496EEB5CEE1101679BDECFA35DFDA57FBB1CFB ();
// 0x00000140 System.Void UnityEngine.Purchasing.ProfileData::set_OsVer(System.String)
extern void ProfileData_set_OsVer_m51D839FCE0BD61AC3AC1FE8D2E6DC1502ECE447D ();
// 0x00000141 System.Int32 UnityEngine.Purchasing.ProfileData::get_ScreenWidth()
extern void ProfileData_get_ScreenWidth_m46F3301BBDBC7ADE8DBE654EF68268FA44BC08AB ();
// 0x00000142 System.Void UnityEngine.Purchasing.ProfileData::set_ScreenWidth(System.Int32)
extern void ProfileData_set_ScreenWidth_mD9F6EA075F7B5E90820D8B99DBD8FB589C950C58 ();
// 0x00000143 System.Int32 UnityEngine.Purchasing.ProfileData::get_ScreenHeight()
extern void ProfileData_get_ScreenHeight_m7F221500E74A8BEBF0F102DD92A9F8570954A6F3 ();
// 0x00000144 System.Void UnityEngine.Purchasing.ProfileData::set_ScreenHeight(System.Int32)
extern void ProfileData_set_ScreenHeight_mBDB5C1F16B95B313FAD0DFE9956C894CA70FED87 ();
// 0x00000145 System.Single UnityEngine.Purchasing.ProfileData::get_ScreenDpi()
extern void ProfileData_get_ScreenDpi_mC0959719DA6315973CFDC198B96AB793ED5D628D ();
// 0x00000146 System.Void UnityEngine.Purchasing.ProfileData::set_ScreenDpi(System.Single)
extern void ProfileData_set_ScreenDpi_m9E07798CA981BE00C9A27D083D7EC0E5A8679509 ();
// 0x00000147 System.String UnityEngine.Purchasing.ProfileData::get_ScreenOrientation()
extern void ProfileData_get_ScreenOrientation_mF6A6D883116A13E3ABA1FDB2903BF43583A8238F ();
// 0x00000148 System.Void UnityEngine.Purchasing.ProfileData::set_ScreenOrientation(System.String)
extern void ProfileData_set_ScreenOrientation_mD12A925E7E9828C5FD80E5EDD99A811A5F1340E4 ();
// 0x00000149 System.String UnityEngine.Purchasing.ProfileData::get_DeviceId()
extern void ProfileData_get_DeviceId_m2070E8CEE0AA6F301D3B7DCCA7B45ABDF0CE1C2A ();
// 0x0000014A System.Void UnityEngine.Purchasing.ProfileData::set_DeviceId(System.String)
extern void ProfileData_set_DeviceId_m7A48A43A4DA06687161CB6F40E88ABBF52B1E317 ();
// 0x0000014B System.String UnityEngine.Purchasing.ProfileData::get_BuildGUID()
extern void ProfileData_get_BuildGUID_m09968264495349E727A2478AF46514BC94535933 ();
// 0x0000014C System.String UnityEngine.Purchasing.ProfileData::get_IapVer()
extern void ProfileData_get_IapVer_m4239D22BED8ED5076999D94A26A066D2A9D1D63E ();
// 0x0000014D System.Void UnityEngine.Purchasing.ProfileData::set_IapVer(System.String)
extern void ProfileData_set_IapVer_m6D735E231337EE015BF73661C171DCC107C48B30 ();
// 0x0000014E System.String UnityEngine.Purchasing.ProfileData::get_AdsGamerToken()
extern void ProfileData_get_AdsGamerToken_m914ADBF8E719ED7D9DD7EBC69D483298945DA8CD ();
// 0x0000014F System.Void UnityEngine.Purchasing.ProfileData::set_AdsGamerToken(System.String)
extern void ProfileData_set_AdsGamerToken_m8FEF7905D11C42DF9060E7158D967223FA96CB9C ();
// 0x00000150 System.Nullable`1<System.Boolean> UnityEngine.Purchasing.ProfileData::get_TrackingOptOut()
extern void ProfileData_get_TrackingOptOut_m33D8775360B2753171555B774FF4757D9A4F64C1 ();
// 0x00000151 System.Void UnityEngine.Purchasing.ProfileData::set_TrackingOptOut(System.Nullable`1<System.Boolean>)
extern void ProfileData_set_TrackingOptOut_mC36D6C38D9442D7DC208E128A268D6FEFAF9B09F ();
// 0x00000152 System.Nullable`1<System.Int32> UnityEngine.Purchasing.ProfileData::get_AdsABGroup()
extern void ProfileData_get_AdsABGroup_m86FCDBE0F90929B71E8DCE3157BAEAADEAA2E400 ();
// 0x00000153 System.Void UnityEngine.Purchasing.ProfileData::set_AdsABGroup(System.Nullable`1<System.Int32>)
extern void ProfileData_set_AdsABGroup_mEA61578B7A3503C3401BEE8BB66A5297E42875E6 ();
// 0x00000154 System.String UnityEngine.Purchasing.ProfileData::get_AdsGameId()
extern void ProfileData_get_AdsGameId_mAE67083D1783919CBCCE604AEF6560BE026A4531 ();
// 0x00000155 System.Void UnityEngine.Purchasing.ProfileData::set_AdsGameId(System.String)
extern void ProfileData_set_AdsGameId_mC1858176F398B42D08D6D291333FF4F4094CB9C1 ();
// 0x00000156 System.Nullable`1<System.Int32> UnityEngine.Purchasing.ProfileData::get_StoreABGroup()
extern void ProfileData_get_StoreABGroup_m1E2CE62F1C241C997F8736B7156E580D424639B1 ();
// 0x00000157 System.String UnityEngine.Purchasing.ProfileData::get_CatalogId()
extern void ProfileData_get_CatalogId_m002CA8EBC0C0A0AEF0ADCE3FA7E2C91C1C865DAB ();
// 0x00000158 System.String UnityEngine.Purchasing.ProfileData::get_StoreName()
extern void ProfileData_get_StoreName_mA960D0B301E38AF33D98EA42A79BA699C308E239 ();
// 0x00000159 System.Void UnityEngine.Purchasing.ProfileData::set_StoreName(System.String)
extern void ProfileData_set_StoreName_m0E999AEE8E61DD2149655170546D77FA3E61C359 ();
// 0x0000015A System.String UnityEngine.Purchasing.ProfileData::get_GameVersion()
extern void ProfileData_get_GameVersion_m6C035D149F1AB488154DBE558925519AD93D4091 ();
// 0x0000015B System.Void UnityEngine.Purchasing.ProfileData::set_GameVersion(System.String)
extern void ProfileData_set_GameVersion_mE1BDCB13724FE2B788F90278575FBE515548433E ();
// 0x0000015C System.Nullable`1<System.Boolean> UnityEngine.Purchasing.ProfileData::get_StoreTestEnabled()
extern void ProfileData_get_StoreTestEnabled_m4BCBF69C392B7118AC80C10BE36EF64A93E6BF2D ();
// 0x0000015D System.Void UnityEngine.Purchasing.ProfileData::.ctor(Uniject.IUtil)
extern void ProfileData__ctor_mAD26AA7C1050AF9AAAAC986125C9A556A575C6AB ();
// 0x0000015E System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.ProfileData::GetProfileDict()
extern void ProfileData_GetProfileDict_m535280FFC251076B40718B0456CF0B0E538E8A13 ();
// 0x0000015F System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.ProfileData::GetProfileIds()
extern void ProfileData_GetProfileIds_m70CAC9710F648E26ECF0E9C819AD7DDDAE196DB0 ();
// 0x00000160 UnityEngine.Purchasing.ProfileData UnityEngine.Purchasing.ProfileData::Instance(Uniject.IUtil)
extern void ProfileData_Instance_m600D8B54938798052BBF5CB8F203723BA246EFB1 ();
// 0x00000161 System.Void UnityEngine.Purchasing.ProfileData::SetGamerToken(System.String)
extern void ProfileData_SetGamerToken_m896B1B829B5EEAD316B68C124AAE4340E1FB7EBC ();
// 0x00000162 System.Void UnityEngine.Purchasing.ProfileData::SetTrackingOptOut(System.Nullable`1<System.Boolean>)
extern void ProfileData_SetTrackingOptOut_mD6DA658A85E6CE89DA8A6A803C95CD83D7311C6E ();
// 0x00000163 System.Void UnityEngine.Purchasing.ProfileData::SetGameId(System.String)
extern void ProfileData_SetGameId_m52FEF84EDEAD039B1AF90A838ACEE3ACE40C71BA ();
// 0x00000164 System.Void UnityEngine.Purchasing.ProfileData::SetABGroup(System.Nullable`1<System.Int32>)
extern void ProfileData_SetABGroup_mBCA92DF96997E44A9864BB854C4E9E97F203BD5F ();
// 0x00000165 System.Void UnityEngine.Purchasing.ProfileData::SetStoreName(System.String)
extern void ProfileData_SetStoreName_mB5008E641E006D3534C59BE4279600F0558CD807 ();
// 0x00000166 System.Boolean UnityEngine.Purchasing.Promo::IsReady()
extern void Promo_IsReady_m25D5863028A51F9CF82CFA1006F8067500CB8C40 ();
// 0x00000167 System.String UnityEngine.Purchasing.Promo::Version()
extern void Promo_Version_m46BFDEA8CBA6117E779098FE26F515252207936B ();
// 0x00000168 System.Void UnityEngine.Purchasing.Promo::.ctor()
extern void Promo__ctor_m9C72894ABF0F420F05A3B69739BA04F8A9F815CB ();
// 0x00000169 System.Void UnityEngine.Purchasing.Promo::InitPromo(UnityEngine.RuntimePlatform,UnityEngine.ILogger,Uniject.IUtil,UnityEngine.Purchasing.IAsyncWebUtil)
extern void Promo_InitPromo_m96E5FDBEB73DDC913BF0678714599E63F8FD352F ();
// 0x0000016A System.Void UnityEngine.Purchasing.Promo::InitPromo(UnityEngine.RuntimePlatform,UnityEngine.ILogger,System.String,Uniject.IUtil,UnityEngine.Purchasing.IAsyncWebUtil)
extern void Promo_InitPromo_m1B239129CA1A6B5011808E6CBD891308689D223B ();
// 0x0000016B System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Product> UnityEngine.Purchasing.Promo::UpdatePromoProductList()
extern void Promo_UpdatePromoProductList_m2150D9A233644EFB63ED39A324F831DDC8484008 ();
// 0x0000016C System.Void UnityEngine.Purchasing.Promo::ProvideProductsToAds(UnityEngine.Purchasing.JSONStore,UnityEngine.Purchasing.Extension.IStoreCallback)
extern void Promo_ProvideProductsToAds_m4741C100A4BE2432085B683E7A2B457DA94C7ED2 ();
// 0x0000016D System.Void UnityEngine.Purchasing.Promo::ProvideProductsToAds(System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Product>)
extern void Promo_ProvideProductsToAds_mAB52653720EBD4608256A4E7BE9FEC53B2A040F7 ();
// 0x0000016E System.String UnityEngine.Purchasing.Promo::QueryPromoProducts()
extern void Promo_QueryPromoProducts_m6999978249A4806AF9257DA3DC932035892B3CDD ();
// 0x0000016F System.Boolean UnityEngine.Purchasing.Promo::InitiatePromoPurchase(System.String)
extern void Promo_InitiatePromoPurchase_m9FB134FF8C1CB64DC7233824C83C8E39F0DC6528 ();
// 0x00000170 System.Boolean UnityEngine.Purchasing.Promo::InitiatePurchasingCommand(System.String)
extern void Promo_InitiatePurchasingCommand_m0BEF8B6816E36005773E02999AB9055A6A32110B ();
// 0x00000171 System.Boolean UnityEngine.Purchasing.Promo::ExecPromoPurchase(System.String)
extern void Promo_ExecPromoPurchase_mC4227D3C5818922D42408CF418D0DCD03A29C90F ();
// 0x00000172 System.Void UnityEngine.Purchasing.Promo::.cctor()
extern void Promo__cctor_m5DBB17281C93C02A3F7F79A3FA8539FAA578406D ();
// 0x00000173 Uniject.IUtil UnityEngine.Purchasing.StandardPurchasingModule::get_util()
extern void StandardPurchasingModule_get_util_mE75325C5B8A59631C070F56C1F5A38F87E6D9150 ();
// 0x00000174 System.Void UnityEngine.Purchasing.StandardPurchasingModule::set_util(Uniject.IUtil)
extern void StandardPurchasingModule_set_util_m14F6FCC94C302A5E54DB7E31D5F1F7CA1D42E3EB ();
// 0x00000175 UnityEngine.ILogger UnityEngine.Purchasing.StandardPurchasingModule::get_logger()
extern void StandardPurchasingModule_get_logger_mE9C380CF9CD10EB576C38647EC1F9607B49A8E37 ();
// 0x00000176 System.Void UnityEngine.Purchasing.StandardPurchasingModule::set_logger(UnityEngine.ILogger)
extern void StandardPurchasingModule_set_logger_m5FA80C86716952D6A651A784341B3F11FF28DC8D ();
// 0x00000177 UnityEngine.Purchasing.IAsyncWebUtil UnityEngine.Purchasing.StandardPurchasingModule::get_webUtil()
extern void StandardPurchasingModule_get_webUtil_m8593EC4979C69FB8C42A46E0496EAD11CD1ABDF0 ();
// 0x00000178 System.Void UnityEngine.Purchasing.StandardPurchasingModule::set_webUtil(UnityEngine.Purchasing.IAsyncWebUtil)
extern void StandardPurchasingModule_set_webUtil_m0F7C2381A6EF95E78510ABE42040E38D80FD152A ();
// 0x00000179 UnityEngine.Purchasing.StandardPurchasingModule_StoreInstance UnityEngine.Purchasing.StandardPurchasingModule::get_storeInstance()
extern void StandardPurchasingModule_get_storeInstance_m085751B9A278D48A205411AD4138B055040D7D95 ();
// 0x0000017A System.Void UnityEngine.Purchasing.StandardPurchasingModule::set_storeInstance(UnityEngine.Purchasing.StandardPurchasingModule_StoreInstance)
extern void StandardPurchasingModule_set_storeInstance_m84F714FF3AEE32D1887B0C3AF697156B1A0B9627 ();
// 0x0000017B System.Void UnityEngine.Purchasing.StandardPurchasingModule::.ctor(Uniject.IUtil,UnityEngine.Purchasing.IAsyncWebUtil,UnityEngine.ILogger,UnityEngine.Purchasing.INativeStoreProvider,UnityEngine.RuntimePlatform,UnityEngine.Purchasing.AppStore,System.Boolean)
extern void StandardPurchasingModule__ctor_m8B9F53AD29A88B7B41FA8C20290D3C039FAC68BD ();
// 0x0000017C UnityEngine.Purchasing.AppStore UnityEngine.Purchasing.StandardPurchasingModule::get_appStore()
extern void StandardPurchasingModule_get_appStore_mD9A11B01D3325C7BA4400287E4F5A6619AB0466B ();
// 0x0000017D UnityEngine.Purchasing.FakeStoreUIMode UnityEngine.Purchasing.StandardPurchasingModule::get_useFakeStoreUIMode()
extern void StandardPurchasingModule_get_useFakeStoreUIMode_mD4755F84F60521F709EF841683A33E48E83ECD48 ();
// 0x0000017E System.Void UnityEngine.Purchasing.StandardPurchasingModule::set_useFakeStoreUIMode(UnityEngine.Purchasing.FakeStoreUIMode)
extern void StandardPurchasingModule_set_useFakeStoreUIMode_mDAE68A7B59C636A2B8E30A2C9ACB5D1B10F2F209 ();
// 0x0000017F System.Boolean UnityEngine.Purchasing.StandardPurchasingModule::get_useFakeStoreAlways()
extern void StandardPurchasingModule_get_useFakeStoreAlways_mCBB566421D3385BF6961F73BC869F46480FEE7C1 ();
// 0x00000180 System.Void UnityEngine.Purchasing.StandardPurchasingModule::set_useFakeStoreAlways(System.Boolean)
extern void StandardPurchasingModule_set_useFakeStoreAlways_m687F69DB0036A7FDD79ECF34B299C0080BC1F566 ();
// 0x00000181 UnityEngine.Purchasing.StandardPurchasingModule UnityEngine.Purchasing.StandardPurchasingModule::Instance()
extern void StandardPurchasingModule_Instance_mB18020DD1E1F75B5841D60DC58F9BB3D96F79CE5 ();
// 0x00000182 UnityEngine.Purchasing.StandardPurchasingModule UnityEngine.Purchasing.StandardPurchasingModule::Instance(UnityEngine.Purchasing.AppStore)
extern void StandardPurchasingModule_Instance_m1465CEAA3CFAAFBA554084194B6F74AB41E2A100 ();
// 0x00000183 System.Void UnityEngine.Purchasing.StandardPurchasingModule::Configure()
extern void StandardPurchasingModule_Configure_m3ED778C0F579E4E09C19D66D7D2532BE079FC7E7 ();
// 0x00000184 UnityEngine.Purchasing.StandardPurchasingModule_StoreInstance UnityEngine.Purchasing.StandardPurchasingModule::InstantiateStore()
extern void StandardPurchasingModule_InstantiateStore_m9168E84C4A64E3EBE40924701E6BA56EFD28A100 ();
// 0x00000185 UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.StandardPurchasingModule::InstantiateAndroid()
extern void StandardPurchasingModule_InstantiateAndroid_m1D0D24031DC9B95AFBB1A497261705C5A0ACEE01 ();
// 0x00000186 UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.StandardPurchasingModule::InstantiateUDP()
extern void StandardPurchasingModule_InstantiateUDP_m672E4EE1C23635B9BE7A8A7BE5FF52633803A170 ();
// 0x00000187 UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.StandardPurchasingModule::InstantiateAndroidHelper(UnityEngine.Purchasing.JSONStore)
extern void StandardPurchasingModule_InstantiateAndroidHelper_m9EF83A117BFEC870EE3CCAF2C0451E8FD4728325 ();
// 0x00000188 UnityEngine.Purchasing.INativeStore UnityEngine.Purchasing.StandardPurchasingModule::GetAndroidNativeStore(UnityEngine.Purchasing.JSONStore)
extern void StandardPurchasingModule_GetAndroidNativeStore_mCBB46F0175B9EAA03EF95B1A9A4F6662E8A11C27 ();
// 0x00000189 UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.StandardPurchasingModule::InstantiateCloudMoolah()
extern void StandardPurchasingModule_InstantiateCloudMoolah_mEC850A5A3091C92FDE2C4A1A56A8C5A57D10B0F9 ();
// 0x0000018A UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.StandardPurchasingModule::InstantiateApple()
extern void StandardPurchasingModule_InstantiateApple_m1706151B3D81F1F2CF707DE68FBC61733BF50937 ();
// 0x0000018B System.Void UnityEngine.Purchasing.StandardPurchasingModule::UseMockWindowsStore(System.Boolean)
extern void StandardPurchasingModule_UseMockWindowsStore_mB6BDE4B509E98FF703DB2AE2654B735EBDEFBE34 ();
// 0x0000018C UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.StandardPurchasingModule::instantiateWindowsStore()
extern void StandardPurchasingModule_instantiateWindowsStore_m47C8038AEAE6A35ADEDF7CDE1CB598F95473E4B2 ();
// 0x0000018D UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.StandardPurchasingModule::InstantiateTizen()
extern void StandardPurchasingModule_InstantiateTizen_m5A716B8CF121F1EF574C636F3B7DA3E33434CD98 ();
// 0x0000018E UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.StandardPurchasingModule::InstantiateFacebook()
extern void StandardPurchasingModule_InstantiateFacebook_m2DD89E8805482C84E11E667EF63E55A3EB30B7D3 ();
// 0x0000018F UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.StandardPurchasingModule::InstantiateFakeStore()
extern void StandardPurchasingModule_InstantiateFakeStore_mFD722C9962E77030C2AA876ECB17149A75482FFE ();
// 0x00000190 System.Void UnityEngine.Purchasing.StandardPurchasingModule::.cctor()
extern void StandardPurchasingModule__cctor_m978C32DFDFD68218ADB5C0D49A739B5FB30F5B95 ();
// 0x00000191 System.Void UnityEngine.Purchasing.StandardPurchasingModule::<Configure>b__45_0(System.Action`1<System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>>)
extern void StandardPurchasingModule_U3CConfigureU3Eb__45_0_m2BD9749522EB55B03BCDBD099918CDB602FD86CC ();
// 0x00000192 System.String UnityEngine.Purchasing.StandardPurchasingModule_StoreInstance::get_storeName()
extern void StoreInstance_get_storeName_m555469DA5D580347F934DB83B933E245335E39EA ();
// 0x00000193 UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.StandardPurchasingModule_StoreInstance::get_instance()
extern void StoreInstance_get_instance_mE68D22058589D0C7E57E1A74B16F72F316F7B531 ();
// 0x00000194 System.Void UnityEngine.Purchasing.StandardPurchasingModule_StoreInstance::.ctor(System.String,UnityEngine.Purchasing.Extension.IStore)
extern void StoreInstance__ctor_m008A8A29F01A347DA3CCFA075F7EF6FBFEF317A7 ();
// 0x00000195 System.Void UnityEngine.Purchasing.StandardPurchasingModule_MicrosoftConfiguration::.ctor(UnityEngine.Purchasing.StandardPurchasingModule)
extern void MicrosoftConfiguration__ctor_m21D9133564D5129B3E938C5BE4AFE91B6384D3D4 ();
// 0x00000196 System.Void UnityEngine.Purchasing.StandardPurchasingModule_MicrosoftConfiguration::set_useMockBillingSystem(System.Boolean)
extern void MicrosoftConfiguration_set_useMockBillingSystem_mD842F2ADC3B2AA183CA2F6F78D6D4BB7824C077D ();
// 0x00000197 UnityEngine.Purchasing.AppStore UnityEngine.Purchasing.StoreConfiguration::get_androidStore()
extern void StoreConfiguration_get_androidStore_m286667F96886750948104475E594EA799741D4D8 ();
// 0x00000198 System.Void UnityEngine.Purchasing.StoreConfiguration::set_androidStore(UnityEngine.Purchasing.AppStore)
extern void StoreConfiguration_set_androidStore_mBF7C394232399AB18E7174C68C083EF895AC2122 ();
// 0x00000199 System.Void UnityEngine.Purchasing.StoreConfiguration::.ctor(UnityEngine.Purchasing.AppStore)
extern void StoreConfiguration__ctor_m8C35F000B47DEF0E579B67B023278ED8B84DF8D2 ();
// 0x0000019A UnityEngine.Purchasing.StoreConfiguration UnityEngine.Purchasing.StoreConfiguration::Deserialize(System.String)
extern void StoreConfiguration_Deserialize_m33FC11C99256675CCDB0800DFD17346B7F0A1281 ();
// 0x0000019B System.Void UnityEngine.Purchasing.SubscriptionInfo::.ctor(UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt,System.String)
extern void SubscriptionInfo__ctor_m689803D4BC0676C6381CB3C0A3939B5C51133424 ();
// 0x0000019C UnityEngine.Purchasing.Result UnityEngine.Purchasing.SubscriptionInfo::isExpired()
extern void SubscriptionInfo_isExpired_m84520B381EB667539C8FD6B4507791CB36ED0C5E ();
// 0x0000019D System.Void UnityEngine.Purchasing.ReceiptParserException::.ctor()
extern void ReceiptParserException__ctor_mEEFA64A4D08934C669FF19CFEC2B6C0B4DC92E2F ();
// 0x0000019E System.Void UnityEngine.Purchasing.InvalidProductTypeException::.ctor()
extern void InvalidProductTypeException__ctor_m035887B0F41C4D815497308B1B248179F893D1A4 ();
// 0x0000019F UnityEngine.Purchasing.Extension.PurchaseFailureDescription UnityEngine.Purchasing.FakeTransactionHistoryExtensions::GetLastPurchaseFailureDescription()
extern void FakeTransactionHistoryExtensions_GetLastPurchaseFailureDescription_m86E109095A02EED174963D1FCFB1796F01AB2CB9 ();
// 0x000001A0 UnityEngine.Purchasing.StoreSpecificPurchaseErrorCode UnityEngine.Purchasing.FakeTransactionHistoryExtensions::GetLastStoreSpecificPurchaseErrorCode()
extern void FakeTransactionHistoryExtensions_GetLastStoreSpecificPurchaseErrorCode_m7B8A398A0DDCE5FCC7B19FB5079257507995E595 ();
// 0x000001A1 System.Void UnityEngine.Purchasing.FakeTransactionHistoryExtensions::.ctor()
extern void FakeTransactionHistoryExtensions__ctor_mBE23B3DA369DA56B2B78C114F38F5F6DFAC4C26A ();
// 0x000001A2 UnityEngine.Purchasing.Extension.PurchaseFailureDescription UnityEngine.Purchasing.ITransactionHistoryExtensions::GetLastPurchaseFailureDescription()
// 0x000001A3 UnityEngine.Purchasing.StoreSpecificPurchaseErrorCode UnityEngine.Purchasing.ITransactionHistoryExtensions::GetLastStoreSpecificPurchaseErrorCode()
// 0x000001A4 System.Void UnityEngine.Purchasing.FakeMicrosoftExtensions::RestoreTransactions()
extern void FakeMicrosoftExtensions_RestoreTransactions_mF2207CB74C8BEB1D23900966DB66EDFEAEC8D250 ();
// 0x000001A5 System.Void UnityEngine.Purchasing.FakeMicrosoftExtensions::.ctor()
extern void FakeMicrosoftExtensions__ctor_m85AB39C55C1D427F786EEDC2FCA70330274A19BA ();
// 0x000001A6 System.Void UnityEngine.Purchasing.IMicrosoftConfiguration::set_useMockBillingSystem(System.Boolean)
// 0x000001A7 System.Void UnityEngine.Purchasing.IMicrosoftExtensions::RestoreTransactions()
// 0x000001A8 System.Void UnityEngine.Purchasing.WinRTStore::.ctor(UnityEngine.Purchasing.Default.IWindowsIAP,Uniject.IUtil,UnityEngine.ILogger)
extern void WinRTStore__ctor_mF5D9BC13F7515FDBC897925C5F60882A7D95CFA0 ();
// 0x000001A9 System.Void UnityEngine.Purchasing.WinRTStore::SetWindowsIAP(UnityEngine.Purchasing.Default.IWindowsIAP)
extern void WinRTStore_SetWindowsIAP_m56CDA707AFE86FDF27795F620AEA61766648E5EB ();
// 0x000001AA System.Void UnityEngine.Purchasing.WinRTStore::Initialize(UnityEngine.Purchasing.Extension.IStoreCallback)
extern void WinRTStore_Initialize_mFDF2D86BD20A74E9D31556D37135D6014CFD9AD1 ();
// 0x000001AB System.Void UnityEngine.Purchasing.WinRTStore::RetrieveProducts(System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>)
extern void WinRTStore_RetrieveProducts_mAD36E588A1E2EAF7947214DE9379B069194733CE ();
// 0x000001AC System.Void UnityEngine.Purchasing.WinRTStore::FinishTransaction(UnityEngine.Purchasing.ProductDefinition,System.String)
extern void WinRTStore_FinishTransaction_m24A6D1268F44975BEFF2D11DF5309FF1382EA9DC ();
// 0x000001AD System.Void UnityEngine.Purchasing.WinRTStore::init(System.Int32)
extern void WinRTStore_init_m98EED74815AF77767124D3C655E6B1A3FD5D1CF6 ();
// 0x000001AE System.Void UnityEngine.Purchasing.WinRTStore::Purchase(UnityEngine.Purchasing.ProductDefinition,System.String)
extern void WinRTStore_Purchase_mBD850F6652FC86FA8E9FD27199BBE6921BF68689 ();
// 0x000001AF System.Void UnityEngine.Purchasing.WinRTStore::restoreTransactions(System.Boolean)
extern void WinRTStore_restoreTransactions_mB2037319EF3C63E6713BFD87BD705E54C78D7C7A ();
// 0x000001B0 System.Void UnityEngine.Purchasing.WinRTStore::RestoreTransactions()
extern void WinRTStore_RestoreTransactions_m2E1E758A1B26EF48A252E88B473A7BC049BFBA9D ();
// 0x000001B1 System.Void UnityEngine.Purchasing.WinRTStore_<>c::.cctor()
extern void U3CU3Ec__cctor_m015552D82ECC5C8F37F4E706949DA95EEF365363 ();
// 0x000001B2 System.Void UnityEngine.Purchasing.WinRTStore_<>c::.ctor()
extern void U3CU3Ec__ctor_mCCDA65ADA02A3E65AD16BAD2C79334F6F6159E61 ();
// 0x000001B3 System.Boolean UnityEngine.Purchasing.WinRTStore_<>c::<RetrieveProducts>b__8_0(UnityEngine.Purchasing.ProductDefinition)
extern void U3CU3Ec_U3CRetrieveProductsU3Eb__8_0_mD2601B3398853E534D1FE1F68623E5E6A5239603 ();
// 0x000001B4 UnityEngine.Purchasing.Default.WinProductDescription UnityEngine.Purchasing.WinRTStore_<>c::<RetrieveProducts>b__8_1(UnityEngine.Purchasing.ProductDefinition)
extern void U3CU3Ec_U3CRetrieveProductsU3Eb__8_1_mBC5C11EF6780F7C1DBD157972B2888C32364CCEC ();
// 0x000001B5 System.Void UnityEngine.Purchasing.ITizenStoreConfiguration::SetGroupId(System.String)
// 0x000001B6 System.Void UnityEngine.Purchasing.TizenStoreImpl::.ctor(Uniject.IUtil)
extern void TizenStoreImpl__ctor_m9DE3C722858981D6E00E91B3929FB929304E13A0 ();
// 0x000001B7 System.Void UnityEngine.Purchasing.TizenStoreImpl::SetNativeStore(UnityEngine.Purchasing.INativeTizenStore)
extern void TizenStoreImpl_SetNativeStore_mA3EA0BA1582896DC93C1F169432F05320333512E ();
// 0x000001B8 System.Void UnityEngine.Purchasing.TizenStoreImpl::SetGroupId(System.String)
extern void TizenStoreImpl_SetGroupId_mE5A2DA72D1468F08B6BD828A39504E7053311D2C ();
// 0x000001B9 System.Void UnityEngine.Purchasing.TizenStoreImpl::MessageCallback(System.String,System.String,System.String,System.String)
extern void TizenStoreImpl_MessageCallback_m468657DE860B56D8404F5502CB68ABA6C7B13748 ();
// 0x000001BA System.Void UnityEngine.Purchasing.TizenStoreImpl::ProcessMessage(System.String,System.String,System.String,System.String)
extern void TizenStoreImpl_ProcessMessage_m556B1FD5D0C7E17667638943E594999F0E0A971D ();
// 0x000001BB System.Void UnityEngine.Purchasing.FakeTizenStoreConfiguration::SetGroupId(System.String)
extern void FakeTizenStoreConfiguration_SetGroupId_mFEEA23EA981ACF2B92FA4DDAFE11F6493EB199C6 ();
// 0x000001BC System.Void UnityEngine.Purchasing.FakeTizenStoreConfiguration::.ctor()
extern void FakeTizenStoreConfiguration__ctor_m55BCB83DDAF81A885EADAC0A83FC24E711869FF8 ();
// 0x000001BD System.Void UnityEngine.Purchasing.FacebookStoreImpl::.ctor(Uniject.IUtil)
extern void FacebookStoreImpl__ctor_m5932D72754F6270061663FFE12F2DB7FDA18EB36 ();
// 0x000001BE System.Void UnityEngine.Purchasing.FacebookStoreImpl::SetNativeStore(UnityEngine.Purchasing.INativeFacebookStore)
extern void FacebookStoreImpl_SetNativeStore_mC8C28200154A7485A0195DFE8534F8224F4C26F7 ();
// 0x000001BF System.Void UnityEngine.Purchasing.FacebookStoreImpl::MessageCallback(System.String,System.String,System.String,System.String)
extern void FacebookStoreImpl_MessageCallback_m95B738D784D0C0AC054DE6F7DBC86B4E12517105 ();
// 0x000001C0 System.Void UnityEngine.Purchasing.FacebookStoreImpl::ProcessMessage(System.String,System.String,System.String,System.String)
extern void FacebookStoreImpl_ProcessMessage_m9D16C56756EF24357B54D6C0805DA4427AAE9C3A ();
// 0x000001C1 System.Void UnityEngine.Purchasing.FacebookStoreImpl_<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m3E341248F6101B5583977953730D3FBCB363BBF6 ();
// 0x000001C2 System.Void UnityEngine.Purchasing.FacebookStoreImpl_<>c__DisplayClass6_0::<MessageCallback>b__0()
extern void U3CU3Ec__DisplayClass6_0_U3CMessageCallbackU3Eb__0_m6BC2ABDEA18EB32E3D065FBDEB07BDA05C040BEA ();
// 0x000001C3 System.String UnityEngine.Purchasing.FakeStore::get_unavailableProductId()
extern void FakeStore_get_unavailableProductId_m1DC90721D64BAAD88F7584CFDD152322CE9D3BC7 ();
// 0x000001C4 System.Void UnityEngine.Purchasing.FakeStore::Initialize(UnityEngine.Purchasing.Extension.IStoreCallback)
extern void FakeStore_Initialize_m606BCDDA58D05C1861CBDCA6EFF7CEBA0020B1CF ();
// 0x000001C5 System.Void UnityEngine.Purchasing.FakeStore::RetrieveProducts(System.String)
extern void FakeStore_RetrieveProducts_m5A6065E4AC2661446E20DBA746102CD38E5F1C2F ();
// 0x000001C6 System.Void UnityEngine.Purchasing.FakeStore::StoreRetrieveProducts(System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>)
extern void FakeStore_StoreRetrieveProducts_mC973D58766D305297F06F31E0CC9B9B99AA45601 ();
// 0x000001C7 System.Void UnityEngine.Purchasing.FakeStore::Purchase(System.String,System.String)
extern void FakeStore_Purchase_mADD9588BDB4C14062725B736A5C76D97297C4379 ();
// 0x000001C8 System.Void UnityEngine.Purchasing.FakeStore::FakePurchase(UnityEngine.Purchasing.ProductDefinition,System.String)
extern void FakeStore_FakePurchase_mCA031E6C818E3303F7D2185BF690D0C54F10772A ();
// 0x000001C9 System.Void UnityEngine.Purchasing.FakeStore::FinishTransaction(System.String,System.String)
extern void FakeStore_FinishTransaction_m3F32EFBC388B143E445E5E162293E37A8EC0C142 ();
// 0x000001CA System.Void UnityEngine.Purchasing.FakeStore::FinishTransaction(UnityEngine.Purchasing.ProductDefinition,System.String)
extern void FakeStore_FinishTransaction_m33131F29D4DE7DDF90AB6E63B717040398D8FB67 ();
// 0x000001CB System.Boolean UnityEngine.Purchasing.FakeStore::StartUI(System.Object,UnityEngine.Purchasing.FakeStore_DialogType,System.Action`2<System.Boolean,T>)
// 0x000001CC System.Void UnityEngine.Purchasing.FakeStore::.ctor()
extern void FakeStore__ctor_m7B1E63C130FD7A59E1AF3C408C9DF97F079D8E5B ();
// 0x000001CD System.Void UnityEngine.Purchasing.FakeStore::<>n__0(System.String,System.String,System.String)
extern void FakeStore_U3CU3En__0_m48AE9C20E4AD0D61108991946F26F7D5C8E00339 ();
// 0x000001CE System.Void UnityEngine.Purchasing.FakeStore_<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_m3E08B70CD129A9D7407CD8B3813637462BEFDD39 ();
// 0x000001CF System.Void UnityEngine.Purchasing.FakeStore_<>c__DisplayClass13_0::<StoreRetrieveProducts>b__0(System.Boolean,UnityEngine.Purchasing.InitializationFailureReason)
extern void U3CU3Ec__DisplayClass13_0_U3CStoreRetrieveProductsU3Eb__0_m275E670C367D6668F587E084D9C270213DE7E616 ();
// 0x000001D0 System.Void UnityEngine.Purchasing.FakeStore_<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_mF811F04792946A30F919890178D9044E08752F7C ();
// 0x000001D1 System.Void UnityEngine.Purchasing.FakeStore_<>c__DisplayClass15_0::<FakePurchase>b__0(System.Boolean,UnityEngine.Purchasing.PurchaseFailureReason)
extern void U3CU3Ec__DisplayClass15_0_U3CFakePurchaseU3Eb__0_mE8AB5F0A69EEEF8B09D75B751E88C49419B15FD6 ();
// 0x000001D2 System.Void UnityEngine.Purchasing.UIFakeStore::.ctor()
extern void UIFakeStore__ctor_mA5EDC9D7CDF177F2E687C80BDF3F68C99131DB86 ();
// 0x000001D3 System.Boolean UnityEngine.Purchasing.UIFakeStore::StartUI(System.Object,UnityEngine.Purchasing.FakeStore_DialogType,System.Action`2<System.Boolean,T>)
// 0x000001D4 System.Boolean UnityEngine.Purchasing.UIFakeStore::StartUI(System.String,System.String,System.String,System.Collections.Generic.List`1<System.String>,System.Action`2<System.Boolean,System.Int32>)
extern void UIFakeStore_StartUI_mC8C893B1DDD59C7866318F31D13242E18F4BBF5A ();
// 0x000001D5 System.Void UnityEngine.Purchasing.UIFakeStore::InstantiateDialog()
extern void UIFakeStore_InstantiateDialog_m1440FB95866C330FCF7BEC848F1BDBD186CA0042 ();
// 0x000001D6 System.String UnityEngine.Purchasing.UIFakeStore::CreatePurchaseQuestion(UnityEngine.Purchasing.ProductDefinition)
extern void UIFakeStore_CreatePurchaseQuestion_m4A190057735CF8AE00B60FAF38AB205D36716AC3 ();
// 0x000001D7 System.String UnityEngine.Purchasing.UIFakeStore::CreateRetrieveProductsQuestion(System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>)
extern void UIFakeStore_CreateRetrieveProductsQuestion_mF8F0D9D35007FA7C36C42424EB3776FC46299051 ();
// 0x000001D8 UnityEngine.UI.Button UnityEngine.Purchasing.UIFakeStore::GetOkayButton()
extern void UIFakeStore_GetOkayButton_m6BBD69BC5434E3554F702AB903F3481D980263A5 ();
// 0x000001D9 UnityEngine.UI.Button UnityEngine.Purchasing.UIFakeStore::GetCancelButton()
extern void UIFakeStore_GetCancelButton_m059338D8E926C282508D499BE48CBC90C1C1812A ();
// 0x000001DA UnityEngine.GameObject UnityEngine.Purchasing.UIFakeStore::GetCancelButtonGameObject()
extern void UIFakeStore_GetCancelButtonGameObject_mF2855FF5C2A6E3C98546E285A63D3AAE7D32A18B ();
// 0x000001DB UnityEngine.UI.Text UnityEngine.Purchasing.UIFakeStore::GetOkayButtonText()
extern void UIFakeStore_GetOkayButtonText_mC917C603011D7E5EEA9AFDD28994CAC03DECB3A6 ();
// 0x000001DC UnityEngine.UI.Text UnityEngine.Purchasing.UIFakeStore::GetCancelButtonText()
extern void UIFakeStore_GetCancelButtonText_m689D8770314538090CA990230ADDCFAA63E75CD6 ();
// 0x000001DD UnityEngine.UI.Dropdown UnityEngine.Purchasing.UIFakeStore::GetDropdown()
extern void UIFakeStore_GetDropdown_mBE537DDD2414C098992BE08450D46D235A124FF0 ();
// 0x000001DE UnityEngine.GameObject UnityEngine.Purchasing.UIFakeStore::GetDropdownContainerGameObject()
extern void UIFakeStore_GetDropdownContainerGameObject_m4F79275493F404849C4E0EF75AC153443F41DBC0 ();
// 0x000001DF System.Void UnityEngine.Purchasing.UIFakeStore::OkayButtonClicked()
extern void UIFakeStore_OkayButtonClicked_mACFC7FF728DAE7966EC1ECA16BA31B239AE349D7 ();
// 0x000001E0 System.Void UnityEngine.Purchasing.UIFakeStore::CancelButtonClicked()
extern void UIFakeStore_CancelButtonClicked_m6B11DCFDFB8C3CB2EF134679B074E4707191A978 ();
// 0x000001E1 System.Void UnityEngine.Purchasing.UIFakeStore::DropdownValueChanged(System.Int32)
extern void UIFakeStore_DropdownValueChanged_mB0F03CAEAAADED776026525DAD048577F796AF99 ();
// 0x000001E2 System.Void UnityEngine.Purchasing.UIFakeStore::CloseDialog()
extern void UIFakeStore_CloseDialog_m25610A3B67BBFFE744F82F64A80BD92794638679 ();
// 0x000001E3 System.Boolean UnityEngine.Purchasing.UIFakeStore::IsShowingDialog()
extern void UIFakeStore_IsShowingDialog_m1969FBEA0C317F9D0F52C0AAD050A9B50CF8C599 ();
// 0x000001E4 System.Void UnityEngine.Purchasing.UIFakeStore::<InstantiateDialog>b__16_0()
extern void UIFakeStore_U3CInstantiateDialogU3Eb__16_0_mA954BE2B9E71D2E8CB7B5DB10632B6F041EB5AFF ();
// 0x000001E5 System.Void UnityEngine.Purchasing.UIFakeStore::<InstantiateDialog>b__16_1()
extern void UIFakeStore_U3CInstantiateDialogU3Eb__16_1_mAF5070FE37567B9BD839901F58FAC2D8EEF5C952 ();
// 0x000001E6 System.Void UnityEngine.Purchasing.UIFakeStore::<InstantiateDialog>b__16_2()
extern void UIFakeStore_U3CInstantiateDialogU3Eb__16_2_mB3DAD1862371AEB36A4B5657924837D49D2E07EA ();
// 0x000001E7 System.Void UnityEngine.Purchasing.UIFakeStore::<InstantiateDialog>b__16_3(System.Int32)
extern void UIFakeStore_U3CInstantiateDialogU3Eb__16_3_m5C54E1E95E43A956552E8B31CC234EF44B2693F6 ();
// 0x000001E8 System.Void UnityEngine.Purchasing.UIFakeStore_DialogRequest::.ctor()
extern void DialogRequest__ctor_mCAC8FB55B62AB198BD2877981C256AD896131CC3 ();
// 0x000001E9 System.Void UnityEngine.Purchasing.UIFakeStore_LifecycleNotifier::OnDestroy()
extern void LifecycleNotifier_OnDestroy_m92C3BCAAD0308E3EC1AEF7B2FA915179D93721D5 ();
// 0x000001EA System.Void UnityEngine.Purchasing.UIFakeStore_LifecycleNotifier::.ctor()
extern void LifecycleNotifier__ctor_m3B5E7F45E27216E49CE8B79272CFE0A49C8E914E ();
// 0x000001EB System.Void UnityEngine.Purchasing.UIFakeStore_<>c__DisplayClass14_0`1::.ctor()
// 0x000001EC System.Void UnityEngine.Purchasing.UIFakeStore_<>c__DisplayClass14_0`1::<StartUI>b__0(System.Boolean,System.Int32)
// 0x000001ED System.Void UnityEngine.Purchasing.UIFakeStore_<>c::.cctor()
extern void U3CU3Ec__cctor_m0E231BBBEE13B41986E938FC4F3950478A50649A ();
// 0x000001EE System.Void UnityEngine.Purchasing.UIFakeStore_<>c::.ctor()
extern void U3CU3Ec__ctor_m97428511F03B44A6BE0720340DA707ED07E6C4E0 ();
// 0x000001EF System.String UnityEngine.Purchasing.UIFakeStore_<>c::<CreateRetrieveProductsQuestion>b__18_0(UnityEngine.Purchasing.ProductDefinition)
extern void U3CU3Ec_U3CCreateRetrieveProductsQuestionU3Eb__18_0_mB4672BE448F0AD0318FCE596150D2B1F491427C2 ();
// 0x000001F0 UnityEngine.Purchasing.FileReference UnityEngine.Purchasing.FileReference::CreateInstance(System.String,UnityEngine.ILogger,Uniject.IUtil)
extern void FileReference_CreateInstance_m7DA4D6D768A4A1D2F8E98AC8FCB5025E2047A20D ();
// 0x000001F1 System.Void UnityEngine.Purchasing.FileReference::.ctor(System.String,UnityEngine.ILogger)
extern void FileReference__ctor_m06C32EC1721553D79528602981599D1C4E489EDE ();
// 0x000001F2 System.Collections.Generic.List`1<UnityEngine.Purchasing.ProductDefinition> UnityEngine.Purchasing.ProductDefinitionExtensions::DecodeJSON(System.Collections.Generic.List`1<System.Object>,System.String)
extern void ProductDefinitionExtensions_DecodeJSON_mBF72BDBA8057333883B4B2EDEBC8C2832FF05EF0 ();
// 0x000001F3 T[] UnityEngine.Purchasing.Extension.UnityUtil::GetAnyComponentsOfType()
// 0x000001F4 System.DateTime UnityEngine.Purchasing.Extension.UnityUtil::get_currentTime()
extern void UnityUtil_get_currentTime_m9E9CCAE5B5BA4B1C1DC542CF7D7CF8A8EAFD5342 ();
// 0x000001F5 System.String UnityEngine.Purchasing.Extension.UnityUtil::get_persistentDataPath()
extern void UnityUtil_get_persistentDataPath_mC3CD6F450B7DBDB8BE0C74403BD1BAA824BDB834 ();
// 0x000001F6 System.String UnityEngine.Purchasing.Extension.UnityUtil::get_deviceUniqueIdentifier()
extern void UnityUtil_get_deviceUniqueIdentifier_m25DE2982391E613C958996F707B8CCF38DA33EB5 ();
// 0x000001F7 System.String UnityEngine.Purchasing.Extension.UnityUtil::get_unityVersion()
extern void UnityUtil_get_unityVersion_m96A7AAAB19FFE96E2D3B21D00EE4C73F1F5858AB ();
// 0x000001F8 System.String UnityEngine.Purchasing.Extension.UnityUtil::get_cloudProjectId()
extern void UnityUtil_get_cloudProjectId_mED63EBEF43586F8ADADD115BEB5D216621FE5980 ();
// 0x000001F9 System.String UnityEngine.Purchasing.Extension.UnityUtil::get_userId()
extern void UnityUtil_get_userId_mE5F64C7833BA808141871DE1F5ECE69DDB11656A ();
// 0x000001FA System.String UnityEngine.Purchasing.Extension.UnityUtil::get_gameVersion()
extern void UnityUtil_get_gameVersion_mB0EC04C9CAA2E32F4CF972B9C69996240D2D2337 ();
// 0x000001FB System.UInt64 UnityEngine.Purchasing.Extension.UnityUtil::get_sessionId()
extern void UnityUtil_get_sessionId_mA7F6E612BD1D514246A7702B9B338D7743A7416A ();
// 0x000001FC UnityEngine.RuntimePlatform UnityEngine.Purchasing.Extension.UnityUtil::get_platform()
extern void UnityUtil_get_platform_mC1762AB21F8AD8E4E40DE8D5A360C5B136217F70 ();
// 0x000001FD System.Boolean UnityEngine.Purchasing.Extension.UnityUtil::get_isEditor()
extern void UnityUtil_get_isEditor_mF3113CA4F0AFEC87898B22792AA0A39AE993F1B5 ();
// 0x000001FE System.String UnityEngine.Purchasing.Extension.UnityUtil::get_deviceModel()
extern void UnityUtil_get_deviceModel_m5186FCB4B89BC17405D28E8A6BC3ACEAC0853EE5 ();
// 0x000001FF System.String UnityEngine.Purchasing.Extension.UnityUtil::get_deviceName()
extern void UnityUtil_get_deviceName_mDB723D68DE5C26107495316051A4583A197A78CA ();
// 0x00000200 UnityEngine.DeviceType UnityEngine.Purchasing.Extension.UnityUtil::get_deviceType()
extern void UnityUtil_get_deviceType_m5C8827248BA9E569547F132000EE9B96168625B6 ();
// 0x00000201 System.String UnityEngine.Purchasing.Extension.UnityUtil::get_operatingSystem()
extern void UnityUtil_get_operatingSystem_mD819B891D0AFEB70A4D4C073ADA39EEFB323F904 ();
// 0x00000202 System.Int32 UnityEngine.Purchasing.Extension.UnityUtil::get_screenWidth()
extern void UnityUtil_get_screenWidth_mA18588ECA827E6346F8ABE59E43C20052C4E6ECA ();
// 0x00000203 System.Int32 UnityEngine.Purchasing.Extension.UnityUtil::get_screenHeight()
extern void UnityUtil_get_screenHeight_mCAFF10CE892FB1C1170132B884790BD00E8A96E3 ();
// 0x00000204 System.Single UnityEngine.Purchasing.Extension.UnityUtil::get_screenDpi()
extern void UnityUtil_get_screenDpi_m8C77AA174CE1D4C44A660C1E67046FEA1795B153 ();
// 0x00000205 System.String UnityEngine.Purchasing.Extension.UnityUtil::get_screenOrientation()
extern void UnityUtil_get_screenOrientation_m5460F8E1928C6B8AE54377F0ADF2F5CEA0A200F8 ();
// 0x00000206 System.Object UnityEngine.Purchasing.Extension.UnityUtil::Uniject.IUtil.InitiateCoroutine(System.Collections.IEnumerator)
extern void UnityUtil_Uniject_IUtil_InitiateCoroutine_m824A968A924F7429BE28F1F130F0C8A86302A83A ();
// 0x00000207 System.Void UnityEngine.Purchasing.Extension.UnityUtil::Uniject.IUtil.InitiateCoroutine(System.Collections.IEnumerator,System.Int32)
extern void UnityUtil_Uniject_IUtil_InitiateCoroutine_m15C5FE56F62E224A56686C9133962C6F865FCD42 ();
// 0x00000208 System.Void UnityEngine.Purchasing.Extension.UnityUtil::RunOnMainThread(System.Action)
extern void UnityUtil_RunOnMainThread_m30F5D144B4DE3D1D9CD3F5388DE80D25E08E2155 ();
// 0x00000209 System.Object UnityEngine.Purchasing.Extension.UnityUtil::GetWaitForSeconds(System.Int32)
extern void UnityUtil_GetWaitForSeconds_mBC1B31A2B058FE9190DCEDC42B2865B13FE6B472 ();
// 0x0000020A System.Void UnityEngine.Purchasing.Extension.UnityUtil::Start()
extern void UnityUtil_Start_m55390FB80357F6F114F2C66BD385501711107A14 ();
// 0x0000020B T UnityEngine.Purchasing.Extension.UnityUtil::FindInstanceOfType()
// 0x0000020C T UnityEngine.Purchasing.Extension.UnityUtil::LoadResourceInstanceOfType()
// 0x0000020D System.Boolean UnityEngine.Purchasing.Extension.UnityUtil::PcPlatform()
extern void UnityUtil_PcPlatform_m3110D0C0274A092FAA8F85CAB49EAE98EE114E56 ();
// 0x0000020E System.Void UnityEngine.Purchasing.Extension.UnityUtil::DebugLog(System.String,System.Object[])
extern void UnityUtil_DebugLog_mF830A653808B1BE884A35A7E1C74CFC64DE332A3 ();
// 0x0000020F System.Collections.IEnumerator UnityEngine.Purchasing.Extension.UnityUtil::DelayedCoroutine(System.Collections.IEnumerator,System.Int32)
extern void UnityUtil_DelayedCoroutine_m8F0E876C0EBDF155FAFD0A124671D2A3B9695193 ();
// 0x00000210 System.Void UnityEngine.Purchasing.Extension.UnityUtil::Update()
extern void UnityUtil_Update_m1C0F7BC84B3485BAC0A1A56C6C2BEDF8571660A4 ();
// 0x00000211 System.Void UnityEngine.Purchasing.Extension.UnityUtil::AddPauseListener(System.Action`1<System.Boolean>)
extern void UnityUtil_AddPauseListener_mA492399FD82E70BB20847FCD4A71C44B8D4E4440 ();
// 0x00000212 System.Void UnityEngine.Purchasing.Extension.UnityUtil::OnApplicationPause(System.Boolean)
extern void UnityUtil_OnApplicationPause_mD84C23ACA7DBEBC9742500CE2AE40FB0E698A702 ();
// 0x00000213 System.Boolean UnityEngine.Purchasing.Extension.UnityUtil::IsClassOrSubclass(System.Type,System.Type)
extern void UnityUtil_IsClassOrSubclass_m42261E4D64A12506E2121D19DE17A30D20E17650 ();
// 0x00000214 System.Void UnityEngine.Purchasing.Extension.UnityUtil::.ctor()
extern void UnityUtil__ctor_mFB0926E85D1F4868536F4B9CFA5DEB189D08B752 ();
// 0x00000215 System.Void UnityEngine.Purchasing.Extension.UnityUtil::.cctor()
extern void UnityUtil__cctor_mA1F3566FA5E2CB13C8447E4BD9D11E2E8145B0B2 ();
// 0x00000216 System.Void UnityEngine.Purchasing.Extension.UnityUtil_<DelayedCoroutine>d__49::.ctor(System.Int32)
extern void U3CDelayedCoroutineU3Ed__49__ctor_m2B782505BEAA032BA9DAFB455C76973B34376363 ();
// 0x00000217 System.Void UnityEngine.Purchasing.Extension.UnityUtil_<DelayedCoroutine>d__49::System.IDisposable.Dispose()
extern void U3CDelayedCoroutineU3Ed__49_System_IDisposable_Dispose_m5DA8817D3026DAA484039141A24A3DB6BB29A9EB ();
// 0x00000218 System.Boolean UnityEngine.Purchasing.Extension.UnityUtil_<DelayedCoroutine>d__49::MoveNext()
extern void U3CDelayedCoroutineU3Ed__49_MoveNext_mE922A668A107FBF9E0877202FDF91DA159340DC0 ();
// 0x00000219 System.Object UnityEngine.Purchasing.Extension.UnityUtil_<DelayedCoroutine>d__49::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayedCoroutineU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m47A874A151D1D0ED0205CA0A11E30169B6692C8E ();
// 0x0000021A System.Void UnityEngine.Purchasing.Extension.UnityUtil_<DelayedCoroutine>d__49::System.Collections.IEnumerator.Reset()
extern void U3CDelayedCoroutineU3Ed__49_System_Collections_IEnumerator_Reset_m0E00247861EE916399C90C164F63E1D4830E306C ();
// 0x0000021B System.Object UnityEngine.Purchasing.Extension.UnityUtil_<DelayedCoroutine>d__49::System.Collections.IEnumerator.get_Current()
extern void U3CDelayedCoroutineU3Ed__49_System_Collections_IEnumerator_get_Current_mA09F472C1F0F2E9CC7FB249F6E67CD7D1E1B20AD ();
// 0x0000021C System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
extern void U3CPrivateImplementationDetailsU3E_ComputeStringHash_m7894934FA0FEB21E4CC31612C6C15F5275D74BF0 ();
static Il2CppMethodPointer s_methodPointers[540] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AndroidJavaStore_GetStore_mA304200D2D3C146F0AD3A80323B62AB953AB05A8,
	AndroidJavaStore__ctor_m01449C4CE7FC18DA7C3EB3BA04B5B04D10D4EFE2,
	AndroidJavaStore_RetrieveProducts_m8B8B7BC7B4E4BFB66A005A5D7049DFFCA86E40B3,
	AndroidJavaStore_Purchase_mC6AE78FB27F3914D88610A77DFE2842918E8805F,
	AndroidJavaStore_FinishTransaction_mC6D40C96823DADBD1D99426E70D9CB4EC8D8CC00,
	JavaBridge__ctor_m8B5EB23E084860EEDBF4BF2E4C6E3DA38A2B0C0C,
	SerializationExtensions_TryGetString_m8666BE2887720A908F5078AE72AB4ABD5E701297,
	JSONSerializer_SerializeProductDef_m77844CA2AE14EF74419CB47CA7A4B846D7B7EE61,
	JSONSerializer_SerializeProductDefs_m935D15BBAE29C788FBD6F5B8848AD9A0EBE1146F,
	JSONSerializer_DeserializeProductDescriptions_mDBCFC86BCF56FC829E2866E5D276FD3A20FB3BE2,
	JSONSerializer_DeserializeFailureReason_m3210F29425C97F28602B5D0F85E0532727969787,
	JSONSerializer_DeserializeMetadata_m1C99F239DED10235E61B29B19D05BC77BD0F7962,
	JSONSerializer_EncodeProductDef_m04E0C3CC8268B25EBDE5971EC04B5ABC31D21F49,
	ScriptingUnityCallback__ctor_m48619600DFDBC3CECBCD8D2F16D837ED08137520,
	AmazonAppStoreStoreExtensions__ctor_mB8D766EF75B8A54AD23B084A7DE79D450E4A17C0,
	FakeAmazonExtensions__ctor_m64E336084FA7DBFB957ACD40E1800208AEB5BE17,
	FakeMoolahConfiguration_set_appKey_m31E4DE57A1B03FB60CBBB599B7327AADD2570B8C,
	FakeMoolahConfiguration_set_hashKey_m2DA764F4DE68CDFA0FC5D0190C5B8DBA162A28CD,
	FakeMoolahConfiguration_SetMode_m9B36461A21CA242CC3EA775AB1ACBA66CE5A9015,
	FakeMoolahConfiguration__ctor_mA5AF3EDC0DF43F8476C64D6BE55057E8C815334A,
	FakeMoolahExtensions_RestoreTransactionID_m3615E412049BC50246AA573177BDF27677BA9B97,
	FakeMoolahExtensions__ctor_mE2B8A5320FF78AD5859FFA3682976AB911F1D4C4,
	NULL,
	NULL,
	NULL,
	NULL,
	MoolahStoreImpl_Initialize_mB3657B44EEF0337D234C5E9E758FB9521DCAC62A,
	MoolahStoreImpl_RetrieveProducts_mD752C59D83FB377D46B027E49A3BC58142042E62,
	MoolahStoreImpl_GetProductTypeIndex_m7A0D8AA38F30627E090D6FC4136DDFFD5315EBBF,
	MoolahStoreImpl_VaildateProductProcess_m55BC13006F68920045766C9CEBAE64459B68C4D3,
	MoolahStoreImpl_GetCurrentString_m757FE58555DABE1A2ECE45CA5ECBB8751FE8D1D3,
	MoolahStoreImpl_VaildateProduct_m7D5EF5CC862A8A9B325A7603CA6074DAF65406CB,
	MoolahStoreImpl_RetrieveProductsSucceeded_mAB8A7E7F4C609B60EA062277D31ED9455555C8B0,
	MoolahStoreImpl_RetrieveProductsFailed_m633FBD30A53905575209DD41A4259BE2D32F2964,
	MoolahStoreImpl_ClosePayWebView_mE6D8F087ED6EC82785F65C899A64A369DAE607C2,
	MoolahStoreImpl_PurchaseRusult_m39F0431BFD7C8C4CCD76F94FC2552B0DFA0B1C3E,
	MoolahStoreImpl_Purchase_mFC4820701C6AC6E8120A21EC4E4F7C3F8042F700,
	MoolahStoreImpl_DeviceUniqueIdentifier_m8ECD985898FFE720E8660548B5C34D911EB3AF99,
	MoolahStoreImpl_RequestAuthCode_m8772B6C0E0B1F528318B460C0F915A13846D4BEA,
	MoolahStoreImpl_RequestAuthCode_mFF90DD999B570306498950A93B99429EBE62F58F,
	MoolahStoreImpl_StartPurchasePolling_m74D1B15C798D1D319C0DA5EE254D274B218513A4,
	MoolahStoreImpl_PurchaseSucceed_m98845A1DBF5D7BA1DF22F49E6BE2A3D88F84F3A8,
	MoolahStoreImpl_PurchaseFailed_mD7C3E88D4962B6163D3DDB2DA21E010F7DC0F710,
	MoolahStoreImpl_FinishTransaction_m28D8C1743E97E7DCF2DF7BD01B1238664FA74110,
	MoolahStoreImpl_GetStringMD5_m12CD817C2B9062B7F2F62116337ECCDEFB00B2D0,
	MoolahStoreImpl_get_appKey_m0A0B85066F0388406D22AF439FC84831AEDBC423,
	MoolahStoreImpl_set_appKey_m10D03FF35EAB509842AACAE62575AFEE18449778,
	MoolahStoreImpl_get_hashKey_m32F417BB0A5FE8EC8649DF5164645CB10A55FE81,
	MoolahStoreImpl_set_hashKey_mF9CA09D3FDB1373131B3C2E6977F0133D07AB2E3,
	MoolahStoreImpl_get_notificationURL_mFEF77B53CB8EE3C9AB6A5498C10921C95F0D89D5,
	MoolahStoreImpl_set_notificationURL_m5586331F66E173CB3CE4531FE8A6EEB0F42A2101,
	MoolahStoreImpl_SetMode_m2F1A8F5A4EC30985BB0BBBB39EC0C6C5FC4C977B,
	MoolahStoreImpl_GetMode_m64ED3042B62C57EEC2C854A97D95FE33BB301994,
	MoolahStoreImpl_RestoreTransactionID_m912F75EB80A1274AEF5039785958F658F2BDD2A8,
	MoolahStoreImpl_RestoreTransactionIDProcess_m7F5CBA3F7A482DEEF40C3920DBD62432FBC5A5F9,
	MoolahStoreImpl_ValidateReceipt_m894D5C05C6E0F9ABAB35CAAB0659558BCD07E4E2,
	MoolahStoreImpl_ValidateReceiptProcess_m9BFD9A51B23A7C08A1B9379F18018278B1DFF652,
	MoolahStoreImpl__ctor_m45B9BF757BB724568D36823E1043C50B84AE0975,
	MoolahStoreImpl__cctor_m302299AD2F4E724E1539A480D4229B5AE695F912,
	MoolahStoreImpl_U3CRetrieveProductsU3Eb__9_0_mC0C23CDD67DB82B29E76A285049E70FA5CDFE6B1,
	U3CVaildateProductU3Ed__13__ctor_mCD67B17C14B37F361EB318504D89451ED1BB11D3,
	U3CVaildateProductU3Ed__13_System_IDisposable_Dispose_mDAD4D5966A79608342076309E6C214ACD1A77EDF,
	U3CVaildateProductU3Ed__13_MoveNext_m2C00B98CF827E68126B160B9E099932CC1677CCA,
	U3CVaildateProductU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC051E7E5F7240EE282DB4114EC9F70FB39F52214,
	U3CVaildateProductU3Ed__13_System_Collections_IEnumerator_Reset_mB0C69F193EB534C54C40222D449DB177EB9D610D,
	U3CVaildateProductU3Ed__13_System_Collections_IEnumerator_get_Current_m9466624D6F2DD779AC41B9900B8AB3F478D56215,
	U3CU3Ec__DisplayClass18_0__ctor_m023DB3766C8DD0B737650705E935AE59875E18EF,
	U3CU3Ec__DisplayClass18_0_U3CPurchaseU3Eb__0_m3EEF558B55F6449BB2915E92E5A2E6C460C8ABC0,
	U3CU3Ec__DisplayClass18_0_U3CPurchaseU3Eb__1_mB7EE8E5DFAEEDA1BF25975316F71C1FF91345B26,
	U3CU3Ec__DisplayClass18_0_U3CPurchaseU3Eb__2_m16A1A6C667F2E5C60C691924BBC7A31F62BE3E60,
	U3CU3Ec__DisplayClass18_0_U3CPurchaseU3Eb__3_m1CD71BCFD3805ADCD05CBA0AF0B4736C170F8087,
	U3CRequestAuthCodeU3Ed__22__ctor_mF17FC3E7F5BE183BA45278454C960414AFEC7F50,
	U3CRequestAuthCodeU3Ed__22_System_IDisposable_Dispose_mE812CAD04F33D94A151D8A3D4B35EB1182B0EA64,
	U3CRequestAuthCodeU3Ed__22_MoveNext_m997E4D8DF6C6A994ED009A028B95E78FF65B9531,
	U3CRequestAuthCodeU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDD8A8B0FFA0DA2DEB718608581008AA6EEC910D7,
	U3CRequestAuthCodeU3Ed__22_System_Collections_IEnumerator_Reset_mC4B5C97B30538D89F2AAC1781288D0DF9B84AD76,
	U3CRequestAuthCodeU3Ed__22_System_Collections_IEnumerator_get_Current_m967BEDFD426C15484A6254361D84FEAED019BA03,
	U3CStartPurchasePollingU3Ed__23__ctor_mC77E33A83A4BE9BE20E5939B0F48FC6DA5AF898C,
	U3CStartPurchasePollingU3Ed__23_System_IDisposable_Dispose_m39D82E1608C7EB76CA760A4AAF5093D9A03A1313,
	U3CStartPurchasePollingU3Ed__23_MoveNext_m66E26C8CB39A1BA33A573F5F87C8A632AD3EAC02,
	U3CStartPurchasePollingU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m679EF7F98417AFEE4F09BD5C29F643C91CA3528C,
	U3CStartPurchasePollingU3Ed__23_System_Collections_IEnumerator_Reset_mA313BB35752264939B088A1607FA33A32541F2E6,
	U3CStartPurchasePollingU3Ed__23_System_Collections_IEnumerator_get_Current_mE8C680CC04C71B1AE055F3DC2AB9DF18EE86922C,
	U3CRestoreTransactionIDProcessU3Ed__45__ctor_m99E1DAEC51C83B09140F3B39711CECEB7CDFF344,
	U3CRestoreTransactionIDProcessU3Ed__45_System_IDisposable_Dispose_mE71F3427A082632FACC7D1EDB6C8A628AC30AF30,
	U3CRestoreTransactionIDProcessU3Ed__45_MoveNext_m83DC83F9924CA05EF8FC9D3139C972C3C1FD88C1,
	U3CRestoreTransactionIDProcessU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC1FF1A67A12475C96368350787E66EDA4638BAF2,
	U3CRestoreTransactionIDProcessU3Ed__45_System_Collections_IEnumerator_Reset_m5C05EC468494E743F8323F918609947B417FEFF2,
	U3CRestoreTransactionIDProcessU3Ed__45_System_Collections_IEnumerator_get_Current_m71DE8DFF43C88E9F760ADF8DAC4295663BEFD235,
	U3CValidateReceiptProcessU3Ed__47__ctor_m1FDA2398BEB6F5716543617290D6BBF6528593B4,
	U3CValidateReceiptProcessU3Ed__47_System_IDisposable_Dispose_m7629CC7EFA06692F88CE4D990CFCF359C335BCAC,
	U3CValidateReceiptProcessU3Ed__47_MoveNext_m1BBD29387E31B4A1DDD374A8096AC2CEDD77F758,
	U3CValidateReceiptProcessU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEC2629C05909AF60C40B1F4F532256B190E2A38A,
	U3CValidateReceiptProcessU3Ed__47_System_Collections_IEnumerator_Reset_mAC5AA05F1DAF6EB74E5EB819CC1A05314E4FF1A3,
	U3CValidateReceiptProcessU3Ed__47_System_Collections_IEnumerator_get_Current_mDD606F8B415C305DCFE5D6421572CC2938BE0397,
	PayMethod_showPayWebView_mE7EAB314D203A45626FF2B3B9290D2B651B5038F,
	PayMethod_getDeviceID_m80B0E609E743D2B84EE9246BFA8F4DBBEC0F2E4E,
	FakeGooglePlayStoreExtensions_RestoreTransactions_m64163A0B7745A598DEF82ABE87A9AA149D80896C,
	FakeGooglePlayStoreExtensions_SetLogLevel_m2E9A4AA9C6B73AA2AC98BECC9B09CEF068D405A7,
	FakeGooglePlayStoreExtensions__ctor_mD85768C4E64FAE9E24423FB1C85C002732069334,
	GooglePlayAndroidJavaStore__ctor_mDF006F20A4C1F42F81AAB14C091216C9F16923B0,
	GooglePlayAndroidJavaStore_Purchase_m4966D459FDA000D08BBE44373B94BA84E9E5DC4E,
	FakeGooglePlayConfiguration__ctor_mDD816501908B8979EC958F0F535784C1994F2DD4,
	GooglePlayStoreCallback__ctor_m5788B5A80293BE28D30EF5B6AEC2B88C3CA3A69A,
	GooglePlayStoreExtensions__ctor_m8AABC42BC8DB59F0C13DEAD523B4A637C5D1A790,
	GooglePlayStoreExtensions_SetAndroidJavaObject_m8CD7F00EA62C94EDFB47165AD1B25B42E69BEDDA,
	GooglePlayStoreExtensions_RestoreTransactions_m3307BC78807B8C8AF0D3DE214740AC5205541D13,
	GooglePlayStoreExtensions_SetLogLevel_m7E8DF982F055391DF5C714761D316986C33A6F84,
	NULL,
	NULL,
	FakeSamsungAppsExtensions_SetMode_mE54A81C756C55FAB27C679E8AB529F91D3A8C2B3,
	FakeSamsungAppsExtensions_RestoreTransactions_m013DA5B1A804925A5C3FA7D0BBBE669D4621F04A,
	FakeSamsungAppsExtensions__ctor_mAA23B8D906AD755F40DCDE05789FE8FEB6B55469,
	NULL,
	NULL,
	SamsungAppsStoreExtensions__ctor_mF4879E66662B5AEB866D6CBD24E8CAC881C5EB82,
	SamsungAppsStoreExtensions_SetAndroidJavaObject_mE18B1644468157D40A94595975CFAA229523B235,
	SamsungAppsStoreExtensions_SetMode_mE61F4639399357C86C99B9FCFAC57557330FDC08,
	SamsungAppsStoreExtensions_RestoreTransactions_mFBB6B371266FD700DF722AF82648F2922B1E31D5,
	FakeUnityChannelConfiguration__ctor_mC7F0C4338B88A73167B2ACB881458FE70F8A5421,
	FakeUnityChannelExtensions__ctor_m90374E16EC5817B712997005119E062C4AFBE22C,
	FakeUDPExtension__ctor_mB867DF605829D19AFCE77C2398502A4285839B56,
	NULL,
	NULL,
	NULL,
	NULL,
	UDP_get_Name_m0710343BC35C86CEEF3D91B960E05C28DCC91044,
	UDPBindings_Initialize_mB60F0A25A84D2567599C1001C56A8A31E7E60984,
	UDPBindings_Purchase_m2E4C6A35CBD414BD7222CD74F29579B51846902C,
	UDPBindings_RetrieveProducts_m613FBAAC1B3C013FCA3E21A22AB1B1A64BDAC343,
	UDPBindings_FinishTransaction_mE36DE513656EB297079859B929AAE59808DD1D7B,
	UDPBindings_FindPurchaseInfo_mEE367AC13A7C28A3686217B4C11F3EB4ED674531,
	UDPBindings_OnInitialized_m4147796694FB5D707C5A9F60CC331EFB334BAA11,
	UDPBindings_RetrieveProducts_m8168EA5CF3372DC4F6B6A0BCEED07E774B0DB729,
	UDPBindings_Purchase_mA7F5841DA552BAC83C622AEB50300C9315D91757,
	UDPBindings_FinishTransaction_m00C71A282870BA753348E854919F2F7D747D98CC,
	UDPBindings_StringPropertyToDictionary_mA7A8CAAA37DD25F7A843EC31075E24E646A590B1,
	UDPBindings__ctor_m2EF551717D945B4C0EA1AF95153CC9F3EBEA6761,
	UDPImpl_SetNativeStore_m874352E8BC12DE27B4BDD1985848726D87CB36CD,
	UDPImpl_Initialize_m75E1116B65CF6D09D3C59D2FF0964628AEAB58CD,
	UDPImpl_RetrieveProducts_m43FECD5E35F51FFDAB48FF49FF8A3CDC2ECF3E8C,
	UDPImpl_Purchase_mDE36070774F3E79DAC26004D461475260EAF413C,
	UDPImpl_FinishTransaction_m20B3718BD437F18D4DC9F9519DDEEF8511508D81,
	UDPImpl_DictionaryToStringProperty_mE68DF0CFA71560EE63A3E05926B6CE1922956E8F,
	UDPImpl__ctor_m6FC64FF86C754128D7CC1C7CEDCFE5F5D340DEE4,
	U3CU3Ec__DisplayClass7_0__ctor_m7A1D41C3297EAE6EF09B7405D3FDF788CFE5CB0E,
	U3CU3Ec__DisplayClass7_0_U3CRetrieveProductsU3Eb__0_mA42C74DC47D02E5F876AF74B994DA283C8AAF5A2,
	U3CU3Ec__DisplayClass7_0_U3CRetrieveProductsU3Eb__1_mAD3B48FD8B66AD992F5B85169AC7DD69CBB36039,
	U3CU3Ec__DisplayClass8_0__ctor_m2FC07C2D4615C32BF3547CEA84409F924EA5BBA7,
	U3CU3Ec__DisplayClass8_0_U3CPurchaseU3Eb__0_mC0D3F6E272E13661A3C896AACC1EDE52A6610D8F,
	AppleStoreImpl__ctor_mAEC6782374073F02B686B1B9D4C65A90F413B0E7,
	AppleStoreImpl_SetNativeStore_m047D2A24FCC7F538C285C13BAA38A899A337790A,
	AppleStoreImpl_OnProductsRetrieved_m3C3B057EFE60FF6DFD4C7668D728103534F59BB1,
	AppleStoreImpl_RestoreTransactions_m7E1AA7E2D67A69AC55E12BC9C672F49C699DBF40,
	AppleStoreImpl_RegisterPurchaseDeferredListener_m4E9DB86DC8E9298BC1F0EB01671D8864E6AD487B,
	AppleStoreImpl_OnPurchaseDeferred_m21B0C908109F269236AD8DF339A3394F38E93685,
	AppleStoreImpl_OnPromotionalPurchaseAttempted_m8CA1AF8F64C7FA5AD53DD5A5E54640199D76853D,
	AppleStoreImpl_OnTransactionsRestoredSuccess_mBF3B092A79453DA3C88079AD8AA9B5078FD8A750,
	AppleStoreImpl_OnTransactionsRestoredFail_m04D83F71F4C285611530C74D31FA04F9B49532F5,
	AppleStoreImpl_OnAppReceiptRetrieved_m125538DA8CE1289976AA7A4C8D373147169A1B6D,
	AppleStoreImpl_OnAppReceiptRefreshedFailed_m2929E90AA825CCE6AD61130AAC8DE0A55D95E70D,
	AppleStoreImpl_MessageCallback_m1AFEBE0209301D9DE3F08BF561DC43984AEC3251,
	AppleStoreImpl_ProcessMessage_m97E280642E0C95EB7D9AE6C57BD06409CE1D4BFB,
	AppleStoreImpl_OnPurchaseSucceeded_m3857A395DAC7B2B05E1D6EDAE8BF24073D47C8B1,
	AppleStoreImpl_getAppleReceiptFromBase64String_m69D715DB6897A3F7817765FAE04AB263A79022C3,
	AppleStoreImpl_isValidPurchaseState_m801182B6892956E69E290B2559FA8C1B1865D117,
	U3CU3Ec__DisplayClass23_0__ctor_mEF76E555DEC2D18A456AB98F14FA14E084053EC6,
	U3CU3Ec__DisplayClass23_0_U3COnProductsRetrievedU3Eb__0_m8C494BC9AB278C6B70007DD7A1966E5A2E9ECF45,
	U3CU3Ec__cctor_m436F76A4CC0BC7119D77378DD18E7D0EDB313236,
	U3CU3Ec__ctor_m2A70E8334CFF3794283B6BB4691C29C22F374D33,
	U3CU3Ec_U3COnProductsRetrievedU3Eb__23_1_mA73EFEBE1374DA7A9D01A4EC96CBD1EB8DA18314,
	U3CU3Ec_U3CisValidPurchaseStateU3Eb__40_1_mE24747560E0DB04029406086B073A49EDEE6596C,
	U3CU3Ec__DisplayClass36_0__ctor_m303A2CB65F66B65E2F4B472D7C1C487457FB69F6,
	U3CU3Ec__DisplayClass36_0_U3CMessageCallbackU3Eb__0_m94A4FD7E1B7F9A3EF1835B4B1185955FB380C9F4,
	U3CU3Ec__DisplayClass40_0__ctor_m25071E24F4758ABF066B8803D41CF36CD70B5D22,
	U3CU3Ec__DisplayClass40_0_U3CisValidPurchaseStateU3Eb__0_m042A5EBCC9EFA6D00EC5E82A297A76F44F233F76,
	FakeAppleConfiguation__ctor_m98AA1830AC7F3C5934818B2C4F315534F327DCE6,
	FakeAppleExtensions_RestoreTransactions_m0042645492D2E541A3A7A45BEBFC52507897A518,
	FakeAppleExtensions_RegisterPurchaseDeferredListener_m71DB6504006F5E41FA3B3FE48210D1ADBFE895C9,
	FakeAppleExtensions__ctor_m0CAA9CE6D322BAFEEF692394C29C1F960E57B0E4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JSONStore__ctor_mC690C31EF9D9B8966C720B2BC2190FF79E60A57A,
	JSONStore_SetNativeStore_m6B53D93734F455C72FCA8AA0A04188229D691473,
	JSONStore_UnityEngine_Purchasing_IStoreInternal_SetModule_m53A4CEBC95BC512088AD2F2E47597EB174B7646D,
	JSONStore_Initialize_m5658965AF3AC26BD073D286C6351CB2D5ECA84D4,
	JSONStore_RetrieveProducts_mA76F64EE917B1DF7FC23909921174DE9733CA441,
	JSONStore_Purchase_m7D156301950C35DFBC4F599DE09873B2EB9826BB,
	JSONStore_FinishTransaction_mA233230DD1A80ACA1D3C1C130C5E44E06E95D2D5,
	JSONStore_OnSetupFailed_mB90D5D7B294947B141AEC41F035BB3E4C9DF9757,
	JSONStore_OnProductsRetrieved_mEC0780AD5E5EB570014B3B01ED23ECE2CCE9E9DB,
	JSONStore_OnPurchaseSucceeded_m3FDCC6BCEA47445FF2A4277C3B9AA94C62590426,
	JSONStore_OnPurchaseFailed_m3E388358865346F3A9970D95B290649F69BA4960,
	JSONStore_OnPurchaseFailed_mBF39584A62E58C74E02F78BABECAD84BEA96BBBC,
	JSONStore_GetLastPurchaseFailureDescription_m330B23E8D2114DE133B721C23630B5AD928B57A2,
	JSONStore_GetLastStoreSpecificPurchaseErrorCode_m95432D81D1F7D72FCEBABFD11E3011668B9BF5B5,
	JSONStore_ParseStoreSpecificPurchaseErrorCode_mCFF280967DF9BBDF5CD6C9DD119E547EAEDBD9EE,
	NativeStoreProvider_GetAndroidStore_m1196800523C8E16499417841B8151B4A00700F54,
	NativeStoreProvider_GetAndroidStoreHelper_mCFEDE06D2D8E302CA2470AE537954BBBE333F3AD,
	NativeStoreProvider_GetStorekit_m39ABB566FCE2C7E5BD26A09ECA93CD48C82BBBDB,
	NativeStoreProvider_GetTizenStore_m03DEE7DC0328CC8BCAA16EEA4E9F7435AA626397,
	NativeStoreProvider_GetFacebookStore_m980371E3FFB3C2537CFBE4A97177C8C75241359F,
	NativeStoreProvider__ctor_m0B40B71A9CA0D70CCF43332D39AC8478F54EC0D8,
	CloudCatalogImpl_CreateInstance_m1AA64F576A28042FC7E851B3DFE874BDB469A0E8,
	CloudCatalogImpl__ctor_mC098CCE60F7F47D7D363C96B570A25E0F162A0FB,
	CloudCatalogImpl_FetchProducts_mB566664220972396130C4DF65EB94AAE6C2F7ECD,
	CloudCatalogImpl_FetchProducts_mB4A18588B0F8977F26E8E1D9F6D3078F9944F9DC,
	CloudCatalogImpl_ParseProductsFromJSON_mE4E0F0C7404FE2AD72D00A13746E54669375BE3B,
	CloudCatalogImpl_CamelCaseToSnakeCase_m73A1CB1CDF08A67B940F4EDE0478D45DF01851B2,
	CloudCatalogImpl_TryPersistCatalog_mD19B1241ED9DB8D1F3111C2FE1CA6AD4E5BB7957,
	CloudCatalogImpl_TryLoadCachedCatalog_m2B909643837094DAB44C9226AED38DC6A1A7BAB9,
	U3CU3Ec__DisplayClass10_0__ctor_m1CB7873CC19257089AF8D0961EAE5D84125CB97C,
	U3CU3Ec__DisplayClass10_0_U3CFetchProductsU3Eb__0_mF967FD4EBADB30A08B0876AB0AAFB870B0029286,
	U3CU3Ec__DisplayClass10_0_U3CFetchProductsU3Eb__1_m2F08B283C9CFA2D8CA240C1E1296E29530B2C90C,
	U3CU3Ec__DisplayClass10_0_U3CFetchProductsU3Eb__2_m8ABF498B541EA6ABCC609134694B24551227C058,
	U3CU3Ec__cctor_m30C3BC52B28B791A13095D086D79182FE936B811,
	U3CU3Ec__ctor_mE5289AEE490022C4B29A79AA8381C86762FCC538,
	U3CU3Ec_U3CCamelCaseToSnakeCaseU3Eb__12_0_m95EFACC4FAC7A902D618D28D7DF6362FEFF1CC48,
	U3CU3Ec_U3CCamelCaseToSnakeCaseU3Eb__12_1_mD002FB9D81947E50DBBE863B519FCF5F390F4156,
	FakeManagedStoreConfig__ctor_m00A8F240B1F66BA3B7E0A582131CF7670A25A8B1,
	FakeManagedStoreExtensions__ctor_m382473F8F857791F8B680663AB814E8ABC61E1D0,
	StoreCatalogImpl_CreateInstance_m4419704ED56FCCF3118D5A702DF5B53C5B3E091A,
	StoreCatalogImpl__ctor_m66D05EEF7E176F6371317B973755694C455C89E7,
	AdsIPC_InitAdsIPC_m4139F1FC1EF78DBD3F3A8C3BC099B4D69C6A5354,
	AdsIPC_VerifyMethodExists_m360B1626186241503F630453C0B95328D30B8218,
	AdsIPC__cctor_mAEEEC6172E0E37868B7EC24F5B1DF1E3ED81D651,
	EventQueue__ctor_m1FFBCE42BC0A318E425D12E6FF353C88F36C12A6,
	EventQueue_Instance_m8AA0A9B8B2ABFDC92CEBB1975247BCFDF651BC86,
	EventQueue_SetAdsUrl_m99D68C6E140201522C8FFD9D8549B9137F53AD4D,
	EventQueue_SetIapUrl_m08748593860529F3CFD9647376733105D5D2CB69,
	AsyncWebUtil_Get_m6D017D90320746E43A67EC2862A1AE6BD711C19B,
	AsyncWebUtil_Post_m193708E47AE2C30F356588A0C3FEC8291DC8A3A8,
	AsyncWebUtil_Schedule_mF0CD5967A6E9AF68FF80A9731ECABB5D8770915E,
	AsyncWebUtil_DoInvoke_mBAD2A93E08A6BA10DB4E04A899131055D8D32E24,
	AsyncWebUtil_Process_mE07B0BB03E8724BA3E817C71248AC9F5E49EDBAA,
	AsyncWebUtil__ctor_mCCDFF67AE910DF60AB52A3ABD07D200BE3D4F774,
	U3CDoInvokeU3Ed__3__ctor_m40185A1EE5CFFB147DE4BD16B44DF0D9770FFD63,
	U3CDoInvokeU3Ed__3_System_IDisposable_Dispose_m6CB889BB4BFF12E8944AD3D5019606EF251AE4B8,
	U3CDoInvokeU3Ed__3_MoveNext_mA510EC21BCCC499E18D2F787EDA61BDA154B3BF8,
	U3CDoInvokeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA8DE06116F6430932B6CA60363A7D9B7ACCFB08F,
	U3CDoInvokeU3Ed__3_System_Collections_IEnumerator_Reset_m17644E4C6C15ECE6F79DFDD74E844F712949D4FE,
	U3CDoInvokeU3Ed__3_System_Collections_IEnumerator_get_Current_mE727FBB6EA1076ADE927A0E87DB79D7FDB3F9295,
	U3CProcessU3Ed__4__ctor_m754F9594DDCBD85B6587666A027FE76D78E564EA,
	U3CProcessU3Ed__4_System_IDisposable_Dispose_m3BE20CB46FE0E9DDECC186752705AAD6A1E12DA6,
	U3CProcessU3Ed__4_MoveNext_m3619F13E3F9FD0807130F20B835AFC03525FA05A,
	U3CProcessU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF590CAD268580BBB0ECF94F487AE5DDE40796E9,
	U3CProcessU3Ed__4_System_Collections_IEnumerator_Reset_m863BB0046EC53CD6A1066730293CD841EEEEA9AA,
	U3CProcessU3Ed__4_System_Collections_IEnumerator_get_Current_mD354922ABDF987AA80DA9B1E7B0C782C12F74C01,
	NULL,
	NULL,
	QueryHelper_ToQueryString_m4589D7D3119E03772920457BD048125C0DADE53D,
	Price_OnBeforeSerialize_mEBDB52261FAD4BB758A3393D0785A6137934B57D,
	Price_OnAfterDeserialize_m385CE4D04E076761BA0C605B68C91B29F4B99F20,
	Price__ctor_mB1535A2F6959285D025541ADE8A637ADE4710585,
	LocalizedProductDescription_get_Title_m0432DC6615CF588EE3277A082B5F76D307B99503,
	LocalizedProductDescription_get_Description_m6602E8CCC5D834D31B7F849B5772589EC48BBD9B,
	LocalizedProductDescription_DecodeNonLatinCharacters_m408241D24FC656BC5DCF12E715336CDDA86A3FEE,
	LocalizedProductDescription__ctor_mD0C6C96B08AD90504F712542A753B7B30CBC95D3,
	U3CU3Ec__cctor_mBB46B53847924EE6AA11498CBDE3A4BC33890B0B,
	U3CU3Ec__ctor_mD8A8762E5FB1B50A84E12F29A65D5BFE0733FE84,
	U3CU3Ec_U3CDecodeNonLatinCharactersU3Eb__11_0_mDE1E5E9761E76D94295AE1173EBCEA4F0F87CD1A,
	ProductCatalogPayout_get_typeString_mE2D76BF97B5F5A10DB9A63A6D67E552CED6A4BC1,
	ProductCatalogPayout_get_subtype_m3E06D487DA9DFBADB3981720D978C7CCCACDC28F,
	ProductCatalogPayout_get_quantity_m92D5468328FD0B29609E9A50006FD62DDE732288,
	ProductCatalogPayout_get_data_m7955A4BDA357760B6993A748F57BC91A2FBDC8EC,
	ProductCatalogPayout__ctor_m53F18B37C03F42785BEEDCD8C8337107A8905494,
	ProductCatalogItem_get_Payouts_m2846D0CC396C637AB69A58AF03A8B0C380F7081A,
	ProductCatalogItem_get_allStoreIDs_mE94829DBC77372C8EA978DF7706176A7AC175EE3,
	ProductCatalogItem__ctor_m9A763F961DE12BE58E8B60B57750E93049A2B7C5,
	ProductCatalog_get_allProducts_m3AA6CEC7A45E606CDA43B8A67797B50AA9575EFB,
	ProductCatalog_get_allValidProducts_mF4280873CCE80F06264BACC1DB67AC7FEBCBE53A,
	ProductCatalog_Initialize_mFFDE0D11B1B77B1BB6AD4C2137EABC2B6AB46AB8,
	ProductCatalog_Initialize_mD6F8F97A0759177186E0FB992D0908646184B977,
	ProductCatalog_IsEmpty_mB44278AB7415B9B3B5FD96A1A2E5B94D130F2FAA,
	ProductCatalog_Deserialize_m0362D3F5370B960E6C0347E92DF1803A37CB9E02,
	ProductCatalog_FromTextAsset_m80DAF012F02AA671A5E31CAFD1CF89EE2F97F550,
	ProductCatalog_LoadDefaultCatalog_mE33487264ED550B3816900791AC7920BB53EEA39,
	ProductCatalog__ctor_m8951B1D5BCBE61C5BFA49CA2A7E3EBF1457A9450,
	U3CU3Ec__cctor_m7A8D524D5764215776F18D67F7066639E855C9E6,
	U3CU3Ec__ctor_m1B02EC40E2A7A30038A238EB5DD969F93A8F79C4,
	U3CU3Ec_U3Cget_allValidProductsU3Eb__8_0_mFC1BC7CC66D35B647A3810A4876CEFA3467FCB45,
	NULL,
	ProductCatalogImpl_LoadDefaultCatalog_m35DA0FFDA85D7A2BC41CD615176386E3735759E1,
	ProductCatalogImpl__ctor_mF5BED5B749F90CD02A001265FC954E2FEC0EEDAE,
	ProfileData_get_AppId_m051152B180EE7BB8B4601AE5E8A7AFD5277A739B,
	ProfileData_set_AppId_m54F28063B0708D703147406F84120542885B90C3,
	ProfileData_get_UserId_mD6B78F3D39D4D6ED8E53271563AE52DB13A30A82,
	ProfileData_set_UserId_m4BB99D1DA889F57E9B202A5BE5982B05F987A406,
	ProfileData_get_SessionId_m8D0E16CD08475FFE033F83BA9E8307E6CB85E62D,
	ProfileData_set_SessionId_m22FE026F0161D65052BC8751523B616A13B7A042,
	ProfileData_get_Platform_m5E3D6BE3D7540457449BDC89565DDDC5B90CA53C,
	ProfileData_set_Platform_m27CF2728A8A67D5DA6F12E9C7D79AEC9407E55C2,
	ProfileData_get_PlatformId_m656E35580FA5398A7A35BF4FF2AD65C0B80D7B8C,
	ProfileData_set_PlatformId_m333B2ED653CAC85373B95416CD55830737106387,
	ProfileData_get_SdkVer_m772F22F37E1DEFAAEC03B59BC97787B72603D6D4,
	ProfileData_set_SdkVer_mD5679B73DF7FBD0C62E5A57BCADCFD7D60B8BD4A,
	ProfileData_get_OsVer_mEF496EEB5CEE1101679BDECFA35DFDA57FBB1CFB,
	ProfileData_set_OsVer_m51D839FCE0BD61AC3AC1FE8D2E6DC1502ECE447D,
	ProfileData_get_ScreenWidth_m46F3301BBDBC7ADE8DBE654EF68268FA44BC08AB,
	ProfileData_set_ScreenWidth_mD9F6EA075F7B5E90820D8B99DBD8FB589C950C58,
	ProfileData_get_ScreenHeight_m7F221500E74A8BEBF0F102DD92A9F8570954A6F3,
	ProfileData_set_ScreenHeight_mBDB5C1F16B95B313FAD0DFE9956C894CA70FED87,
	ProfileData_get_ScreenDpi_mC0959719DA6315973CFDC198B96AB793ED5D628D,
	ProfileData_set_ScreenDpi_m9E07798CA981BE00C9A27D083D7EC0E5A8679509,
	ProfileData_get_ScreenOrientation_mF6A6D883116A13E3ABA1FDB2903BF43583A8238F,
	ProfileData_set_ScreenOrientation_mD12A925E7E9828C5FD80E5EDD99A811A5F1340E4,
	ProfileData_get_DeviceId_m2070E8CEE0AA6F301D3B7DCCA7B45ABDF0CE1C2A,
	ProfileData_set_DeviceId_m7A48A43A4DA06687161CB6F40E88ABBF52B1E317,
	ProfileData_get_BuildGUID_m09968264495349E727A2478AF46514BC94535933,
	ProfileData_get_IapVer_m4239D22BED8ED5076999D94A26A066D2A9D1D63E,
	ProfileData_set_IapVer_m6D735E231337EE015BF73661C171DCC107C48B30,
	ProfileData_get_AdsGamerToken_m914ADBF8E719ED7D9DD7EBC69D483298945DA8CD,
	ProfileData_set_AdsGamerToken_m8FEF7905D11C42DF9060E7158D967223FA96CB9C,
	ProfileData_get_TrackingOptOut_m33D8775360B2753171555B774FF4757D9A4F64C1,
	ProfileData_set_TrackingOptOut_mC36D6C38D9442D7DC208E128A268D6FEFAF9B09F,
	ProfileData_get_AdsABGroup_m86FCDBE0F90929B71E8DCE3157BAEAADEAA2E400,
	ProfileData_set_AdsABGroup_mEA61578B7A3503C3401BEE8BB66A5297E42875E6,
	ProfileData_get_AdsGameId_mAE67083D1783919CBCCE604AEF6560BE026A4531,
	ProfileData_set_AdsGameId_mC1858176F398B42D08D6D291333FF4F4094CB9C1,
	ProfileData_get_StoreABGroup_m1E2CE62F1C241C997F8736B7156E580D424639B1,
	ProfileData_get_CatalogId_m002CA8EBC0C0A0AEF0ADCE3FA7E2C91C1C865DAB,
	ProfileData_get_StoreName_mA960D0B301E38AF33D98EA42A79BA699C308E239,
	ProfileData_set_StoreName_m0E999AEE8E61DD2149655170546D77FA3E61C359,
	ProfileData_get_GameVersion_m6C035D149F1AB488154DBE558925519AD93D4091,
	ProfileData_set_GameVersion_mE1BDCB13724FE2B788F90278575FBE515548433E,
	ProfileData_get_StoreTestEnabled_m4BCBF69C392B7118AC80C10BE36EF64A93E6BF2D,
	ProfileData__ctor_mAD26AA7C1050AF9AAAAC986125C9A556A575C6AB,
	ProfileData_GetProfileDict_m535280FFC251076B40718B0456CF0B0E538E8A13,
	ProfileData_GetProfileIds_m70CAC9710F648E26ECF0E9C819AD7DDDAE196DB0,
	ProfileData_Instance_m600D8B54938798052BBF5CB8F203723BA246EFB1,
	ProfileData_SetGamerToken_m896B1B829B5EEAD316B68C124AAE4340E1FB7EBC,
	ProfileData_SetTrackingOptOut_mD6DA658A85E6CE89DA8A6A803C95CD83D7311C6E,
	ProfileData_SetGameId_m52FEF84EDEAD039B1AF90A838ACEE3ACE40C71BA,
	ProfileData_SetABGroup_mBCA92DF96997E44A9864BB854C4E9E97F203BD5F,
	ProfileData_SetStoreName_mB5008E641E006D3534C59BE4279600F0558CD807,
	Promo_IsReady_m25D5863028A51F9CF82CFA1006F8067500CB8C40,
	Promo_Version_m46BFDEA8CBA6117E779098FE26F515252207936B,
	Promo__ctor_m9C72894ABF0F420F05A3B69739BA04F8A9F815CB,
	Promo_InitPromo_m96E5FDBEB73DDC913BF0678714599E63F8FD352F,
	Promo_InitPromo_m1B239129CA1A6B5011808E6CBD891308689D223B,
	Promo_UpdatePromoProductList_m2150D9A233644EFB63ED39A324F831DDC8484008,
	Promo_ProvideProductsToAds_m4741C100A4BE2432085B683E7A2B457DA94C7ED2,
	Promo_ProvideProductsToAds_mAB52653720EBD4608256A4E7BE9FEC53B2A040F7,
	Promo_QueryPromoProducts_m6999978249A4806AF9257DA3DC932035892B3CDD,
	Promo_InitiatePromoPurchase_m9FB134FF8C1CB64DC7233824C83C8E39F0DC6528,
	Promo_InitiatePurchasingCommand_m0BEF8B6816E36005773E02999AB9055A6A32110B,
	Promo_ExecPromoPurchase_mC4227D3C5818922D42408CF418D0DCD03A29C90F,
	Promo__cctor_m5DBB17281C93C02A3F7F79A3FA8539FAA578406D,
	StandardPurchasingModule_get_util_mE75325C5B8A59631C070F56C1F5A38F87E6D9150,
	StandardPurchasingModule_set_util_m14F6FCC94C302A5E54DB7E31D5F1F7CA1D42E3EB,
	StandardPurchasingModule_get_logger_mE9C380CF9CD10EB576C38647EC1F9607B49A8E37,
	StandardPurchasingModule_set_logger_m5FA80C86716952D6A651A784341B3F11FF28DC8D,
	StandardPurchasingModule_get_webUtil_m8593EC4979C69FB8C42A46E0496EAD11CD1ABDF0,
	StandardPurchasingModule_set_webUtil_m0F7C2381A6EF95E78510ABE42040E38D80FD152A,
	StandardPurchasingModule_get_storeInstance_m085751B9A278D48A205411AD4138B055040D7D95,
	StandardPurchasingModule_set_storeInstance_m84F714FF3AEE32D1887B0C3AF697156B1A0B9627,
	StandardPurchasingModule__ctor_m8B9F53AD29A88B7B41FA8C20290D3C039FAC68BD,
	StandardPurchasingModule_get_appStore_mD9A11B01D3325C7BA4400287E4F5A6619AB0466B,
	StandardPurchasingModule_get_useFakeStoreUIMode_mD4755F84F60521F709EF841683A33E48E83ECD48,
	StandardPurchasingModule_set_useFakeStoreUIMode_mDAE68A7B59C636A2B8E30A2C9ACB5D1B10F2F209,
	StandardPurchasingModule_get_useFakeStoreAlways_mCBB566421D3385BF6961F73BC869F46480FEE7C1,
	StandardPurchasingModule_set_useFakeStoreAlways_m687F69DB0036A7FDD79ECF34B299C0080BC1F566,
	StandardPurchasingModule_Instance_mB18020DD1E1F75B5841D60DC58F9BB3D96F79CE5,
	StandardPurchasingModule_Instance_m1465CEAA3CFAAFBA554084194B6F74AB41E2A100,
	StandardPurchasingModule_Configure_m3ED778C0F579E4E09C19D66D7D2532BE079FC7E7,
	StandardPurchasingModule_InstantiateStore_m9168E84C4A64E3EBE40924701E6BA56EFD28A100,
	StandardPurchasingModule_InstantiateAndroid_m1D0D24031DC9B95AFBB1A497261705C5A0ACEE01,
	StandardPurchasingModule_InstantiateUDP_m672E4EE1C23635B9BE7A8A7BE5FF52633803A170,
	StandardPurchasingModule_InstantiateAndroidHelper_m9EF83A117BFEC870EE3CCAF2C0451E8FD4728325,
	StandardPurchasingModule_GetAndroidNativeStore_mCBB46F0175B9EAA03EF95B1A9A4F6662E8A11C27,
	StandardPurchasingModule_InstantiateCloudMoolah_mEC850A5A3091C92FDE2C4A1A56A8C5A57D10B0F9,
	StandardPurchasingModule_InstantiateApple_m1706151B3D81F1F2CF707DE68FBC61733BF50937,
	StandardPurchasingModule_UseMockWindowsStore_mB6BDE4B509E98FF703DB2AE2654B735EBDEFBE34,
	StandardPurchasingModule_instantiateWindowsStore_m47C8038AEAE6A35ADEDF7CDE1CB598F95473E4B2,
	StandardPurchasingModule_InstantiateTizen_m5A716B8CF121F1EF574C636F3B7DA3E33434CD98,
	StandardPurchasingModule_InstantiateFacebook_m2DD89E8805482C84E11E667EF63E55A3EB30B7D3,
	StandardPurchasingModule_InstantiateFakeStore_mFD722C9962E77030C2AA876ECB17149A75482FFE,
	StandardPurchasingModule__cctor_m978C32DFDFD68218ADB5C0D49A739B5FB30F5B95,
	StandardPurchasingModule_U3CConfigureU3Eb__45_0_m2BD9749522EB55B03BCDBD099918CDB602FD86CC,
	StoreInstance_get_storeName_m555469DA5D580347F934DB83B933E245335E39EA,
	StoreInstance_get_instance_mE68D22058589D0C7E57E1A74B16F72F316F7B531,
	StoreInstance__ctor_m008A8A29F01A347DA3CCFA075F7EF6FBFEF317A7,
	MicrosoftConfiguration__ctor_m21D9133564D5129B3E938C5BE4AFE91B6384D3D4,
	MicrosoftConfiguration_set_useMockBillingSystem_mD842F2ADC3B2AA183CA2F6F78D6D4BB7824C077D,
	StoreConfiguration_get_androidStore_m286667F96886750948104475E594EA799741D4D8,
	StoreConfiguration_set_androidStore_mBF7C394232399AB18E7174C68C083EF895AC2122,
	StoreConfiguration__ctor_m8C35F000B47DEF0E579B67B023278ED8B84DF8D2,
	StoreConfiguration_Deserialize_m33FC11C99256675CCDB0800DFD17346B7F0A1281,
	SubscriptionInfo__ctor_m689803D4BC0676C6381CB3C0A3939B5C51133424,
	SubscriptionInfo_isExpired_m84520B381EB667539C8FD6B4507791CB36ED0C5E,
	ReceiptParserException__ctor_mEEFA64A4D08934C669FF19CFEC2B6C0B4DC92E2F,
	InvalidProductTypeException__ctor_m035887B0F41C4D815497308B1B248179F893D1A4,
	FakeTransactionHistoryExtensions_GetLastPurchaseFailureDescription_m86E109095A02EED174963D1FCFB1796F01AB2CB9,
	FakeTransactionHistoryExtensions_GetLastStoreSpecificPurchaseErrorCode_m7B8A398A0DDCE5FCC7B19FB5079257507995E595,
	FakeTransactionHistoryExtensions__ctor_mBE23B3DA369DA56B2B78C114F38F5F6DFAC4C26A,
	NULL,
	NULL,
	FakeMicrosoftExtensions_RestoreTransactions_mF2207CB74C8BEB1D23900966DB66EDFEAEC8D250,
	FakeMicrosoftExtensions__ctor_m85AB39C55C1D427F786EEDC2FCA70330274A19BA,
	NULL,
	NULL,
	WinRTStore__ctor_mF5D9BC13F7515FDBC897925C5F60882A7D95CFA0,
	WinRTStore_SetWindowsIAP_m56CDA707AFE86FDF27795F620AEA61766648E5EB,
	WinRTStore_Initialize_mFDF2D86BD20A74E9D31556D37135D6014CFD9AD1,
	WinRTStore_RetrieveProducts_mAD36E588A1E2EAF7947214DE9379B069194733CE,
	WinRTStore_FinishTransaction_m24A6D1268F44975BEFF2D11DF5309FF1382EA9DC,
	WinRTStore_init_m98EED74815AF77767124D3C655E6B1A3FD5D1CF6,
	WinRTStore_Purchase_mBD850F6652FC86FA8E9FD27199BBE6921BF68689,
	WinRTStore_restoreTransactions_mB2037319EF3C63E6713BFD87BD705E54C78D7C7A,
	WinRTStore_RestoreTransactions_m2E1E758A1B26EF48A252E88B473A7BC049BFBA9D,
	U3CU3Ec__cctor_m015552D82ECC5C8F37F4E706949DA95EEF365363,
	U3CU3Ec__ctor_mCCDA65ADA02A3E65AD16BAD2C79334F6F6159E61,
	U3CU3Ec_U3CRetrieveProductsU3Eb__8_0_mD2601B3398853E534D1FE1F68623E5E6A5239603,
	U3CU3Ec_U3CRetrieveProductsU3Eb__8_1_mBC5C11EF6780F7C1DBD157972B2888C32364CCEC,
	NULL,
	TizenStoreImpl__ctor_m9DE3C722858981D6E00E91B3929FB929304E13A0,
	TizenStoreImpl_SetNativeStore_mA3EA0BA1582896DC93C1F169432F05320333512E,
	TizenStoreImpl_SetGroupId_mE5A2DA72D1468F08B6BD828A39504E7053311D2C,
	TizenStoreImpl_MessageCallback_m468657DE860B56D8404F5502CB68ABA6C7B13748,
	TizenStoreImpl_ProcessMessage_m556B1FD5D0C7E17667638943E594999F0E0A971D,
	FakeTizenStoreConfiguration_SetGroupId_mFEEA23EA981ACF2B92FA4DDAFE11F6493EB199C6,
	FakeTizenStoreConfiguration__ctor_m55BCB83DDAF81A885EADAC0A83FC24E711869FF8,
	FacebookStoreImpl__ctor_m5932D72754F6270061663FFE12F2DB7FDA18EB36,
	FacebookStoreImpl_SetNativeStore_mC8C28200154A7485A0195DFE8534F8224F4C26F7,
	FacebookStoreImpl_MessageCallback_m95B738D784D0C0AC054DE6F7DBC86B4E12517105,
	FacebookStoreImpl_ProcessMessage_m9D16C56756EF24357B54D6C0805DA4427AAE9C3A,
	U3CU3Ec__DisplayClass6_0__ctor_m3E341248F6101B5583977953730D3FBCB363BBF6,
	U3CU3Ec__DisplayClass6_0_U3CMessageCallbackU3Eb__0_m6BC2ABDEA18EB32E3D065FBDEB07BDA05C040BEA,
	FakeStore_get_unavailableProductId_m1DC90721D64BAAD88F7584CFDD152322CE9D3BC7,
	FakeStore_Initialize_m606BCDDA58D05C1861CBDCA6EFF7CEBA0020B1CF,
	FakeStore_RetrieveProducts_m5A6065E4AC2661446E20DBA746102CD38E5F1C2F,
	FakeStore_StoreRetrieveProducts_mC973D58766D305297F06F31E0CC9B9B99AA45601,
	FakeStore_Purchase_mADD9588BDB4C14062725B736A5C76D97297C4379,
	FakeStore_FakePurchase_mCA031E6C818E3303F7D2185BF690D0C54F10772A,
	FakeStore_FinishTransaction_m3F32EFBC388B143E445E5E162293E37A8EC0C142,
	FakeStore_FinishTransaction_m33131F29D4DE7DDF90AB6E63B717040398D8FB67,
	NULL,
	FakeStore__ctor_m7B1E63C130FD7A59E1AF3C408C9DF97F079D8E5B,
	FakeStore_U3CU3En__0_m48AE9C20E4AD0D61108991946F26F7D5C8E00339,
	U3CU3Ec__DisplayClass13_0__ctor_m3E08B70CD129A9D7407CD8B3813637462BEFDD39,
	U3CU3Ec__DisplayClass13_0_U3CStoreRetrieveProductsU3Eb__0_m275E670C367D6668F587E084D9C270213DE7E616,
	U3CU3Ec__DisplayClass15_0__ctor_mF811F04792946A30F919890178D9044E08752F7C,
	U3CU3Ec__DisplayClass15_0_U3CFakePurchaseU3Eb__0_mE8AB5F0A69EEEF8B09D75B751E88C49419B15FD6,
	UIFakeStore__ctor_mA5EDC9D7CDF177F2E687C80BDF3F68C99131DB86,
	NULL,
	UIFakeStore_StartUI_mC8C893B1DDD59C7866318F31D13242E18F4BBF5A,
	UIFakeStore_InstantiateDialog_m1440FB95866C330FCF7BEC848F1BDBD186CA0042,
	UIFakeStore_CreatePurchaseQuestion_m4A190057735CF8AE00B60FAF38AB205D36716AC3,
	UIFakeStore_CreateRetrieveProductsQuestion_mF8F0D9D35007FA7C36C42424EB3776FC46299051,
	UIFakeStore_GetOkayButton_m6BBD69BC5434E3554F702AB903F3481D980263A5,
	UIFakeStore_GetCancelButton_m059338D8E926C282508D499BE48CBC90C1C1812A,
	UIFakeStore_GetCancelButtonGameObject_mF2855FF5C2A6E3C98546E285A63D3AAE7D32A18B,
	UIFakeStore_GetOkayButtonText_mC917C603011D7E5EEA9AFDD28994CAC03DECB3A6,
	UIFakeStore_GetCancelButtonText_m689D8770314538090CA990230ADDCFAA63E75CD6,
	UIFakeStore_GetDropdown_mBE537DDD2414C098992BE08450D46D235A124FF0,
	UIFakeStore_GetDropdownContainerGameObject_m4F79275493F404849C4E0EF75AC153443F41DBC0,
	UIFakeStore_OkayButtonClicked_mACFC7FF728DAE7966EC1ECA16BA31B239AE349D7,
	UIFakeStore_CancelButtonClicked_m6B11DCFDFB8C3CB2EF134679B074E4707191A978,
	UIFakeStore_DropdownValueChanged_mB0F03CAEAAADED776026525DAD048577F796AF99,
	UIFakeStore_CloseDialog_m25610A3B67BBFFE744F82F64A80BD92794638679,
	UIFakeStore_IsShowingDialog_m1969FBEA0C317F9D0F52C0AAD050A9B50CF8C599,
	UIFakeStore_U3CInstantiateDialogU3Eb__16_0_mA954BE2B9E71D2E8CB7B5DB10632B6F041EB5AFF,
	UIFakeStore_U3CInstantiateDialogU3Eb__16_1_mAF5070FE37567B9BD839901F58FAC2D8EEF5C952,
	UIFakeStore_U3CInstantiateDialogU3Eb__16_2_mB3DAD1862371AEB36A4B5657924837D49D2E07EA,
	UIFakeStore_U3CInstantiateDialogU3Eb__16_3_m5C54E1E95E43A956552E8B31CC234EF44B2693F6,
	DialogRequest__ctor_mCAC8FB55B62AB198BD2877981C256AD896131CC3,
	LifecycleNotifier_OnDestroy_m92C3BCAAD0308E3EC1AEF7B2FA915179D93721D5,
	LifecycleNotifier__ctor_m3B5E7F45E27216E49CE8B79272CFE0A49C8E914E,
	NULL,
	NULL,
	U3CU3Ec__cctor_m0E231BBBEE13B41986E938FC4F3950478A50649A,
	U3CU3Ec__ctor_m97428511F03B44A6BE0720340DA707ED07E6C4E0,
	U3CU3Ec_U3CCreateRetrieveProductsQuestionU3Eb__18_0_mB4672BE448F0AD0318FCE596150D2B1F491427C2,
	FileReference_CreateInstance_m7DA4D6D768A4A1D2F8E98AC8FCB5025E2047A20D,
	FileReference__ctor_m06C32EC1721553D79528602981599D1C4E489EDE,
	ProductDefinitionExtensions_DecodeJSON_mBF72BDBA8057333883B4B2EDEBC8C2832FF05EF0,
	NULL,
	UnityUtil_get_currentTime_m9E9CCAE5B5BA4B1C1DC542CF7D7CF8A8EAFD5342,
	UnityUtil_get_persistentDataPath_mC3CD6F450B7DBDB8BE0C74403BD1BAA824BDB834,
	UnityUtil_get_deviceUniqueIdentifier_m25DE2982391E613C958996F707B8CCF38DA33EB5,
	UnityUtil_get_unityVersion_m96A7AAAB19FFE96E2D3B21D00EE4C73F1F5858AB,
	UnityUtil_get_cloudProjectId_mED63EBEF43586F8ADADD115BEB5D216621FE5980,
	UnityUtil_get_userId_mE5F64C7833BA808141871DE1F5ECE69DDB11656A,
	UnityUtil_get_gameVersion_mB0EC04C9CAA2E32F4CF972B9C69996240D2D2337,
	UnityUtil_get_sessionId_mA7F6E612BD1D514246A7702B9B338D7743A7416A,
	UnityUtil_get_platform_mC1762AB21F8AD8E4E40DE8D5A360C5B136217F70,
	UnityUtil_get_isEditor_mF3113CA4F0AFEC87898B22792AA0A39AE993F1B5,
	UnityUtil_get_deviceModel_m5186FCB4B89BC17405D28E8A6BC3ACEAC0853EE5,
	UnityUtil_get_deviceName_mDB723D68DE5C26107495316051A4583A197A78CA,
	UnityUtil_get_deviceType_m5C8827248BA9E569547F132000EE9B96168625B6,
	UnityUtil_get_operatingSystem_mD819B891D0AFEB70A4D4C073ADA39EEFB323F904,
	UnityUtil_get_screenWidth_mA18588ECA827E6346F8ABE59E43C20052C4E6ECA,
	UnityUtil_get_screenHeight_mCAFF10CE892FB1C1170132B884790BD00E8A96E3,
	UnityUtil_get_screenDpi_m8C77AA174CE1D4C44A660C1E67046FEA1795B153,
	UnityUtil_get_screenOrientation_m5460F8E1928C6B8AE54377F0ADF2F5CEA0A200F8,
	UnityUtil_Uniject_IUtil_InitiateCoroutine_m824A968A924F7429BE28F1F130F0C8A86302A83A,
	UnityUtil_Uniject_IUtil_InitiateCoroutine_m15C5FE56F62E224A56686C9133962C6F865FCD42,
	UnityUtil_RunOnMainThread_m30F5D144B4DE3D1D9CD3F5388DE80D25E08E2155,
	UnityUtil_GetWaitForSeconds_mBC1B31A2B058FE9190DCEDC42B2865B13FE6B472,
	UnityUtil_Start_m55390FB80357F6F114F2C66BD385501711107A14,
	NULL,
	NULL,
	UnityUtil_PcPlatform_m3110D0C0274A092FAA8F85CAB49EAE98EE114E56,
	UnityUtil_DebugLog_mF830A653808B1BE884A35A7E1C74CFC64DE332A3,
	UnityUtil_DelayedCoroutine_m8F0E876C0EBDF155FAFD0A124671D2A3B9695193,
	UnityUtil_Update_m1C0F7BC84B3485BAC0A1A56C6C2BEDF8571660A4,
	UnityUtil_AddPauseListener_mA492399FD82E70BB20847FCD4A71C44B8D4E4440,
	UnityUtil_OnApplicationPause_mD84C23ACA7DBEBC9742500CE2AE40FB0E698A702,
	UnityUtil_IsClassOrSubclass_m42261E4D64A12506E2121D19DE17A30D20E17650,
	UnityUtil__ctor_mFB0926E85D1F4868536F4B9CFA5DEB189D08B752,
	UnityUtil__cctor_mA1F3566FA5E2CB13C8447E4BD9D11E2E8145B0B2,
	U3CDelayedCoroutineU3Ed__49__ctor_m2B782505BEAA032BA9DAFB455C76973B34376363,
	U3CDelayedCoroutineU3Ed__49_System_IDisposable_Dispose_m5DA8817D3026DAA484039141A24A3DB6BB29A9EB,
	U3CDelayedCoroutineU3Ed__49_MoveNext_mE922A668A107FBF9E0877202FDF91DA159340DC0,
	U3CDelayedCoroutineU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m47A874A151D1D0ED0205CA0A11E30169B6692C8E,
	U3CDelayedCoroutineU3Ed__49_System_Collections_IEnumerator_Reset_m0E00247861EE916399C90C164F63E1D4830E306C,
	U3CDelayedCoroutineU3Ed__49_System_Collections_IEnumerator_get_Current_mA09F472C1F0F2E9CC7FB249F6E67CD7D1E1B20AD,
	U3CPrivateImplementationDetailsU3E_ComputeStringHash_m7894934FA0FEB21E4CC31612C6C15F5275D74BF0,
};
static const int32_t s_InvokerIndices[540] = 
{
	10,
	14,
	14,
	14,
	14,
	14,
	190,
	14,
	10,
	10,
	713,
	14,
	28,
	144,
	26,
	26,
	90,
	14,
	26,
	26,
	27,
	27,
	26,
	1,
	0,
	0,
	0,
	0,
	0,
	0,
	27,
	26,
	23,
	26,
	26,
	32,
	23,
	26,
	23,
	26,
	26,
	32,
	26,
	26,
	26,
	37,
	88,
	28,
	222,
	26,
	32,
	26,
	26,
	27,
	14,
	436,
	1623,
	138,
	215,
	109,
	27,
	28,
	14,
	26,
	14,
	26,
	14,
	26,
	32,
	10,
	26,
	28,
	215,
	222,
	23,
	3,
	88,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	215,
	109,
	215,
	27,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	1624,
	4,
	26,
	32,
	23,
	27,
	27,
	23,
	26,
	23,
	26,
	26,
	32,
	26,
	32,
	32,
	26,
	23,
	32,
	26,
	23,
	26,
	32,
	26,
	23,
	23,
	23,
	26,
	215,
	27,
	27,
	4,
	26,
	215,
	27,
	27,
	28,
	26,
	26,
	27,
	27,
	0,
	23,
	26,
	26,
	26,
	27,
	27,
	149,
	23,
	23,
	88,
	88,
	23,
	88,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	26,
	26,
	23,
	1625,
	436,
	215,
	28,
	90,
	23,
	9,
	3,
	23,
	41,
	41,
	23,
	23,
	23,
	9,
	23,
	26,
	26,
	23,
	26,
	26,
	154,
	28,
	107,
	14,
	26,
	23,
	26,
	26,
	26,
	26,
	27,
	27,
	26,
	26,
	215,
	26,
	27,
	14,
	10,
	115,
	154,
	154,
	28,
	107,
	14,
	23,
	0,
	801,
	26,
	144,
	1,
	0,
	26,
	14,
	23,
	26,
	26,
	23,
	3,
	23,
	506,
	107,
	23,
	23,
	501,
	801,
	121,
	49,
	3,
	27,
	1,
	26,
	26,
	122,
	1527,
	144,
	58,
	165,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	122,
	144,
	0,
	23,
	23,
	23,
	14,
	14,
	0,
	23,
	3,
	23,
	28,
	14,
	14,
	452,
	14,
	23,
	14,
	14,
	23,
	14,
	14,
	3,
	174,
	89,
	0,
	0,
	4,
	23,
	3,
	23,
	9,
	14,
	14,
	23,
	14,
	26,
	14,
	26,
	190,
	217,
	14,
	26,
	10,
	32,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	32,
	713,
	337,
	14,
	26,
	14,
	26,
	14,
	14,
	26,
	14,
	26,
	1626,
	1627,
	761,
	1628,
	14,
	26,
	761,
	14,
	14,
	26,
	14,
	26,
	1626,
	26,
	14,
	14,
	0,
	26,
	1627,
	26,
	1628,
	26,
	49,
	4,
	23,
	1629,
	1630,
	4,
	149,
	174,
	4,
	121,
	121,
	121,
	3,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	1631,
	10,
	10,
	32,
	89,
	31,
	4,
	43,
	23,
	14,
	14,
	14,
	28,
	28,
	14,
	14,
	31,
	14,
	14,
	14,
	14,
	3,
	26,
	14,
	14,
	27,
	26,
	31,
	10,
	32,
	32,
	0,
	27,
	10,
	23,
	23,
	14,
	10,
	23,
	14,
	10,
	23,
	23,
	31,
	23,
	215,
	26,
	26,
	26,
	27,
	32,
	27,
	31,
	23,
	3,
	23,
	9,
	28,
	26,
	26,
	26,
	26,
	1625,
	436,
	26,
	23,
	26,
	26,
	1625,
	436,
	23,
	23,
	14,
	26,
	26,
	26,
	27,
	27,
	27,
	27,
	-1,
	23,
	215,
	23,
	802,
	23,
	802,
	23,
	-1,
	1632,
	23,
	28,
	28,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	23,
	23,
	32,
	23,
	89,
	23,
	23,
	23,
	32,
	23,
	23,
	23,
	-1,
	-1,
	3,
	23,
	28,
	2,
	27,
	1,
	-1,
	112,
	14,
	14,
	14,
	14,
	14,
	14,
	190,
	10,
	89,
	14,
	14,
	10,
	14,
	10,
	10,
	713,
	14,
	28,
	144,
	26,
	34,
	23,
	-1,
	-1,
	49,
	149,
	58,
	23,
	26,
	31,
	90,
	23,
	3,
	32,
	23,
	89,
	14,
	23,
	14,
	94,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[3] = 
{
	{ 0x060000B3, 2,  (void**)&AppleStoreImpl_MessageCallback_m1AFEBE0209301D9DE3F08BF561DC43984AEC3251_RuntimeMethod_var, 0 },
	{ 0x060001B9, 4,  (void**)&TizenStoreImpl_MessageCallback_m468657DE860B56D8404F5502CB68ABA6C7B13748_RuntimeMethod_var, 0 },
	{ 0x060001BF, 3,  (void**)&FacebookStoreImpl_MessageCallback_m95B738D784D0C0AC054DE6F7DBC86B4E12517105_RuntimeMethod_var, 0 },
};
static const Il2CppTokenRangePair s_rgctxIndices[5] = 
{
	{ 0x02000080, { 5, 2 } },
	{ 0x060001D3, { 0, 5 } },
	{ 0x060001F3, { 7, 5 } },
	{ 0x0600020B, { 12, 2 } },
	{ 0x0600020C, { 14, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[16] = 
{
	{ (Il2CppRGCTXDataType)2, 16553 },
	{ (Il2CppRGCTXDataType)3, 11028 },
	{ (Il2CppRGCTXDataType)1, 16072 },
	{ (Il2CppRGCTXDataType)2, 16072 },
	{ (Il2CppRGCTXDataType)3, 11029 },
	{ (Il2CppRGCTXDataType)2, 16082 },
	{ (Il2CppRGCTXDataType)3, 11030 },
	{ (Il2CppRGCTXDataType)2, 16554 },
	{ (Il2CppRGCTXDataType)3, 11031 },
	{ (Il2CppRGCTXDataType)2, 16092 },
	{ (Il2CppRGCTXDataType)3, 11032 },
	{ (Il2CppRGCTXDataType)3, 11033 },
	{ (Il2CppRGCTXDataType)1, 16093 },
	{ (Il2CppRGCTXDataType)2, 16093 },
	{ (Il2CppRGCTXDataType)1, 16094 },
	{ (Il2CppRGCTXDataType)3, 11034 },
};
extern const Il2CppCodeGenModule g_StoresCodeGenModule;
const Il2CppCodeGenModule g_StoresCodeGenModule = 
{
	"Stores.dll",
	540,
	s_methodPointers,
	s_InvokerIndices,
	3,
	s_reversePInvokeIndices,
	5,
	s_rgctxIndices,
	16,
	s_rgctxValues,
	NULL,
};
