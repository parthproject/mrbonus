﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"



extern const RuntimeMethod* AdLoaderClient_AdLoaderDidFailToReceiveAdWithErrorCallback_mC2A70F1FD62DF7B697FF79C19CDB49D89A3ED296_RuntimeMethod_var;
extern const RuntimeMethod* AdLoaderClient_AdLoaderDidReceiveNativeCustomTemplateAdCallback_m96CB8031ECC1E20CDB3AA013D0E1CA5C82FBA9F0_RuntimeMethod_var;
extern const RuntimeMethod* BannerClient_AdViewDidDismissScreenCallback_m15101864FE77339582C8DF5FC5129E33CFC9674B_RuntimeMethod_var;
extern const RuntimeMethod* BannerClient_AdViewDidFailToReceiveAdWithErrorCallback_m863303F5E9FF5A436CE67C416A3E708E9E2F6206_RuntimeMethod_var;
extern const RuntimeMethod* BannerClient_AdViewDidReceiveAdCallback_m5F0611A8BD59EBB7E6A4CFD4AF1C20132C7D5345_RuntimeMethod_var;
extern const RuntimeMethod* BannerClient_AdViewWillLeaveApplicationCallback_mD2C41507939D77A4986009D02E1962A5A200AFE8_RuntimeMethod_var;
extern const RuntimeMethod* BannerClient_AdViewWillPresentScreenCallback_mCCCDADC61157A1E8B91B12B866A326AE24024816_RuntimeMethod_var;
extern const RuntimeMethod* CustomNativeTemplateClient_NativeCustomTemplateDidReceiveClickCallback_m93BA0F7BF0763D9ECEECB912C673A1BAED9D989F_RuntimeMethod_var;
extern const RuntimeMethod* InterstitialClient_InterstitialDidDismissScreenCallback_mAD21EEBDD6D829719AF1FB8F438CC04C63EF431C_RuntimeMethod_var;
extern const RuntimeMethod* InterstitialClient_InterstitialDidFailToReceiveAdWithErrorCallback_m9BD349E80A9B92676F0BF98C52C36A690DF107CA_RuntimeMethod_var;
extern const RuntimeMethod* InterstitialClient_InterstitialDidReceiveAdCallback_mF485EB5E41F80ED5C5B6FF0376CD4B8BB96EED2B_RuntimeMethod_var;
extern const RuntimeMethod* InterstitialClient_InterstitialWillLeaveApplicationCallback_m41A6D1FEF7A23CB20F37D767455B69D71FE83D42_RuntimeMethod_var;
extern const RuntimeMethod* InterstitialClient_InterstitialWillPresentScreenCallback_mB5835C4991F852B2E0768EAE7F9E9478EB2FD2C5_RuntimeMethod_var;
extern const RuntimeMethod* NativeExpressAdClient_NativeExpressAdViewDidDismissScreenCallback_mB912EF80149AE81DA955BB54023A8919BEE81671_RuntimeMethod_var;
extern const RuntimeMethod* NativeExpressAdClient_NativeExpressAdViewDidFailToReceiveAdWithErrorCallback_m02B2DBEB39F61BC6B4188F167105425E5139DF1B_RuntimeMethod_var;
extern const RuntimeMethod* NativeExpressAdClient_NativeExpressAdViewDidReceiveAdCallback_m7E5BCA95DAE8C27B53B4DF27571E1D47DC1BC924_RuntimeMethod_var;
extern const RuntimeMethod* NativeExpressAdClient_NativeExpressAdViewWillLeaveApplicationCallback_m39E270E3EBE1071BA14C4D71464294BAA4C70D28_RuntimeMethod_var;
extern const RuntimeMethod* NativeExpressAdClient_NativeExpressAdViewWillPresentScreenCallback_m374AF054824A2809817D4D34D05128633FD80BDB_RuntimeMethod_var;
extern const RuntimeMethod* RewardBasedVideoAdClient_RewardBasedVideoAdDidCloseCallback_m734CE280D2BBC57576B3A57E32C521E83443B202_RuntimeMethod_var;
extern const RuntimeMethod* RewardBasedVideoAdClient_RewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_m92F8D48CFD9859C3DF734B3B531377978AF137BD_RuntimeMethod_var;
extern const RuntimeMethod* RewardBasedVideoAdClient_RewardBasedVideoAdDidOpenCallback_m139F9D3FE045E6AD73E3C653FD5BC0D1B4CF1901_RuntimeMethod_var;
extern const RuntimeMethod* RewardBasedVideoAdClient_RewardBasedVideoAdDidReceiveAdCallback_mC3AC0F651C19E19CC6247CB1AF7E694C3A8968E2_RuntimeMethod_var;
extern const RuntimeMethod* RewardBasedVideoAdClient_RewardBasedVideoAdDidRewardUserCallback_mC1CA5205857E2C48D6CB3F2BE820505DF05D7070_RuntimeMethod_var;
extern const RuntimeMethod* RewardBasedVideoAdClient_RewardBasedVideoAdDidStartCallback_mF45B63FD0D47CFC8566ED5B3137D933247CBAE87_RuntimeMethod_var;
extern const RuntimeMethod* RewardBasedVideoAdClient_RewardBasedVideoAdWillLeaveApplicationCallback_mFF7E45939A22B52E3C318C1115C13E42827B349C_RuntimeMethod_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern void MonoPInvokeCallbackAttribute__ctor_mB9D2688DB30B918CCD5961A8DB873F32F2205D7E ();
// 0x00000002 System.Void Card::OnEnable()
extern void Card_OnEnable_mA98BC80216BA24DE0FB1809658F5F4CE23D5C6D8 ();
// 0x00000003 System.Void Card::Start()
extern void Card_Start_m88E71E8C5EB701A2B11A2AD5CE4E8DF0C0CF0275 ();
// 0x00000004 System.Void Card::Update()
extern void Card_Update_m37036CE3D85389BF581EFE1357684D99FD238186 ();
// 0x00000005 System.Void Card::OpenCard()
extern void Card_OpenCard_m8E1C0BD9FD2BC0B5BF6E3C93BE9A4DAF466E9CB6 ();
// 0x00000006 System.Void Card::.ctor()
extern void Card__ctor_m706585CE3E010E315E14D9825379E2F7A44FB090 ();
// 0x00000007 System.Void Constants::.cctor()
extern void Constants__cctor_m98FD3FF3D8F21CD21887CCFAA3265E285CB01DF1 ();
// 0x00000008 System.Void Game::Start()
extern void Game_Start_m87108F3ECAE225CE62F13FB4F438BBB97BB71033 ();
// 0x00000009 System.Void Game::RequestInterstitial()
extern void Game_RequestInterstitial_mAD45F76512F7459FCC2FF292993CD3E276E172F0 ();
// 0x0000000A System.Void Game::Update()
extern void Game_Update_m5C0A097D8278D1AE765DA8A0BDC865A756C9E216 ();
// 0x0000000B System.Collections.IEnumerator Game::RestartGame()
extern void Game_RestartGame_m403D17AD7C508C9913474C2B6D7ECFEADC847CBA ();
// 0x0000000C System.Void Game::OpenPanels(System.Int32)
extern void Game_OpenPanels_mFF6E48301CC63ACA7768AA4993B851A6287B2603 ();
// 0x0000000D System.Void Game::RefreshScores()
extern void Game_RefreshScores_m4442AA5313CFCBE839D17D2EB0108A38C8A9D71C ();
// 0x0000000E System.Void Game::BackBtn()
extern void Game_BackBtn_m6C485830B002982AD0378B9E883F93EED478B042 ();
// 0x0000000F System.Void Game::ChipBtn(System.Int32)
extern void Game_ChipBtn_mE50F27435531C605880033069AAEAB5255BF1CF0 ();
// 0x00000010 System.Void Game::ClearBetBtn()
extern void Game_ClearBetBtn_m6DD3774C005A6DE3400C3F1B613425D299D46C4C ();
// 0x00000011 System.Void Game::LastBetBtn()
extern void Game_LastBetBtn_m417F8BD919EFCA1F5217B456003F3310C74DA2F7 ();
// 0x00000012 System.Void Game::DealBtn()
extern void Game_DealBtn_m08FDD9F5442E6D82E5206F00429660A2B0912EFD ();
// 0x00000013 System.Collections.IEnumerator Game::StartGame()
extern void Game_StartGame_m5D3F6A97655E0F54215E306B1CB4BD8F6F6D6A38 ();
// 0x00000014 System.Void Game::DoubleBtn()
extern void Game_DoubleBtn_m42FC86FA585DC094AED7E154ED6D6BA923F5F15B ();
// 0x00000015 System.Void Game::StayBtn()
extern void Game_StayBtn_mE9B2058DAEBE9F047711E2CDEC6D06D94845077D ();
// 0x00000016 System.Void Game::HitBtn()
extern void Game_HitBtn_mC0EE6A101B15E599667B198AB7452266848018D3 ();
// 0x00000017 System.Collections.IEnumerator Game::GetNewCard(System.Int32)
extern void Game_GetNewCard_mC4D9F0E7B519EEF8D32E8861F2964E63218CDD97 ();
// 0x00000018 System.Void Game::CardOpened()
extern void Game_CardOpened_m6D54C5E0B5CC7B8DA1D869C0DE54E28CE4307E65 ();
// 0x00000019 System.Void Game::CheckScores()
extern void Game_CheckScores_mF56E59971977CC8F8F63F0CB9AB65A079FE7C9B4 ();
// 0x0000001A System.Void Game::Win()
extern void Game_Win_mA19ADE2E7908AA6A58044A41474198E4341C30D7 ();
// 0x0000001B System.Void Game::Lose()
extern void Game_Lose_mE490FF52E14901AE6170516465C69BE062C72324 ();
// 0x0000001C System.Void Game::Blackjack(System.Int32)
extern void Game_Blackjack_m76BBACE4495A421F4C751746AEA884BD2C6C8CBB ();
// 0x0000001D System.Void Game::.ctor()
extern void Game__ctor_mC065A45FD96CE26F2F9543F0C6BC65F2935A8C12 ();
// 0x0000001E System.Void Menu::Start()
extern void Menu_Start_m26D2BE4EA9DEE5AF2FE548CF8EDDAFA0FE7379E9 ();
// 0x0000001F System.Void Menu::Update()
extern void Menu_Update_m3870FCE8E278FA9F95178BDD18B258C3F4D49BAA ();
// 0x00000020 System.Void Menu::RequestRewardVideo()
extern void Menu_RequestRewardVideo_mC6FC3DBAC40902860533D8BF5C78FB5BBD476944 ();
// 0x00000021 System.Void Menu::HandleRewardBasedVideoRewarded(System.Object,GoogleMobileAds.Api.Reward)
extern void Menu_HandleRewardBasedVideoRewarded_m0B9E46D6532F5EEAAD4E0F3737CE65436DA63572 ();
// 0x00000022 System.Void Menu::OpenScreen(System.Int32)
extern void Menu_OpenScreen_mCF89ADA1C1BA0D861053344895E94772A530EFE6 ();
// 0x00000023 System.Void Menu::PlayBtn()
extern void Menu_PlayBtn_m6E1DEECCF0637E2764399EDAB455C1CC65D2438E ();
// 0x00000024 System.Void Menu::RateBtn()
extern void Menu_RateBtn_m88827BCC56691B4728908B482D4D7FAB1F59AB54 ();
// 0x00000025 System.Void Menu::ShopBtn()
extern void Menu_ShopBtn_m85B3EA4AD485BF9FF002BD93FE770CBB5E74D217 ();
// 0x00000026 System.Void Menu::MainMenuBtn()
extern void Menu_MainMenuBtn_mC83A16665D27D193705E500150FE617D73C58019 ();
// 0x00000027 System.Void Menu::ShowVideoAd()
extern void Menu_ShowVideoAd_m3E4B2520FA144D70A553743FFB0318C83CE5B7B1 ();
// 0x00000028 System.Void Menu::ExitBtn()
extern void Menu_ExitBtn_mFCDBE922EFCF6352970EFED1E6455B25ADDB61CE ();
// 0x00000029 System.Void Menu::.ctor()
extern void Menu__ctor_mD372D109F6554E1F9A25291964C852C9F6BFC463 ();
// 0x0000002A System.Void Purchaser::Start()
extern void Purchaser_Start_mCD122FAB158727577ED229C323774D10737D1835 ();
// 0x0000002B System.Void Purchaser::InitializePurchasing()
extern void Purchaser_InitializePurchasing_mA433D882053BB54E75A01A2919EA1989C9470F9F ();
// 0x0000002C System.Boolean Purchaser::IsInitialized()
extern void Purchaser_IsInitialized_m272B7D0B6DB8CDEB87223E1224F013BB3533C890 ();
// 0x0000002D System.Void Purchaser::BuyConsumable(System.Int32)
extern void Purchaser_BuyConsumable_mFD6E311F8C75EF0A2589C9282FC91463BE256FDD ();
// 0x0000002E System.Void Purchaser::BuyProductID(System.String)
extern void Purchaser_BuyProductID_mBCDAC5023D5830F87D3E0116AE9DC5447065385E ();
// 0x0000002F System.Void Purchaser::RestorePurchases()
extern void Purchaser_RestorePurchases_m24A1E9C7782A272240B01EB09EBC8828ED550846 ();
// 0x00000030 System.Void Purchaser::OnInitialized(UnityEngine.Purchasing.IStoreController,UnityEngine.Purchasing.IExtensionProvider)
extern void Purchaser_OnInitialized_m2C5D997E02074CA968ACF89D2EA8849616E2AE8A ();
// 0x00000031 System.Void Purchaser::OnInitializeFailed(UnityEngine.Purchasing.InitializationFailureReason)
extern void Purchaser_OnInitializeFailed_mD26D07D1E337C7DFCA40D1D65DA7F4F85BF449DE ();
// 0x00000032 UnityEngine.Purchasing.PurchaseProcessingResult Purchaser::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern void Purchaser_ProcessPurchase_m9A0B2A4762E2AF7D006F42F231BB676CDDCC6238 ();
// 0x00000033 System.Void Purchaser::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern void Purchaser_OnPurchaseFailed_m4A93D014B730F86486878D3A2EEE69C949EB07F9 ();
// 0x00000034 System.Void Purchaser::.ctor()
extern void Purchaser__ctor_m526162775A909925C95DBABA1943472F883DFDDA ();
// 0x00000035 System.Void Purchaser::.cctor()
extern void Purchaser__cctor_mADAAF4DC1AEEC46CD5D7A737ED6896BF8BC7C6E8 ();
// 0x00000036 GoogleMobileAds.Common.IBannerClient GoogleMobileAds.GoogleMobileAdsClientFactory::BuildBannerClient()
extern void GoogleMobileAdsClientFactory_BuildBannerClient_mDF34A2569CD7EC1C65DBF778094794BE76C4285D ();
// 0x00000037 GoogleMobileAds.Common.IInterstitialClient GoogleMobileAds.GoogleMobileAdsClientFactory::BuildInterstitialClient()
extern void GoogleMobileAdsClientFactory_BuildInterstitialClient_m2C47FEE4563F9FBFA90EE3DC8026C07D0979410C ();
// 0x00000038 GoogleMobileAds.Common.IRewardBasedVideoAdClient GoogleMobileAds.GoogleMobileAdsClientFactory::BuildRewardBasedVideoAdClient()
extern void GoogleMobileAdsClientFactory_BuildRewardBasedVideoAdClient_m3DF8A4521BE504081121100F03BD8AFC83E3F798 ();
// 0x00000039 GoogleMobileAds.Common.IAdLoaderClient GoogleMobileAds.GoogleMobileAdsClientFactory::BuildAdLoaderClient(GoogleMobileAds.Api.AdLoader)
extern void GoogleMobileAdsClientFactory_BuildAdLoaderClient_mBA3B6397E3B56942810431CEC2710D699856DC24 ();
// 0x0000003A GoogleMobileAds.Common.INativeExpressAdClient GoogleMobileAds.GoogleMobileAdsClientFactory::BuildNativeExpressAdClient()
extern void GoogleMobileAdsClientFactory_BuildNativeExpressAdClient_m51966CE9566E9F3FCC0F5407E2E51377120A92ED ();
// 0x0000003B GoogleMobileAds.Common.IMobileAdsClient GoogleMobileAds.GoogleMobileAdsClientFactory::MobileAdsInstance()
extern void GoogleMobileAdsClientFactory_MobileAdsInstance_m5BA8107B3B6E51CA4BAB44F365AAE4DF5C66A671 ();
// 0x0000003C System.Void GoogleMobileAds.GoogleMobileAdsClientFactory::.ctor()
extern void GoogleMobileAdsClientFactory__ctor_mCCE39F56BB450D301B0F962D5AE78538C659A70A ();
// 0x0000003D System.Void GoogleMobileAds.iOS.AdLoaderClient::.ctor(GoogleMobileAds.Api.AdLoader)
extern void AdLoaderClient__ctor_m776C37E00913BBA78A0FFE5DB3A44F3499D78EA9 ();
// 0x0000003E System.Void GoogleMobileAds.iOS.AdLoaderClient::add_OnCustomNativeTemplateAdLoaded(System.EventHandler`1<GoogleMobileAds.Api.CustomNativeEventArgs>)
extern void AdLoaderClient_add_OnCustomNativeTemplateAdLoaded_m1EFC5C6C6E5A045EA65673ECF90650BC6B328C66 ();
// 0x0000003F System.Void GoogleMobileAds.iOS.AdLoaderClient::remove_OnCustomNativeTemplateAdLoaded(System.EventHandler`1<GoogleMobileAds.Api.CustomNativeEventArgs>)
extern void AdLoaderClient_remove_OnCustomNativeTemplateAdLoaded_mED9514C6FFD42DB29FB365119B969515A1A0E81E ();
// 0x00000040 System.Void GoogleMobileAds.iOS.AdLoaderClient::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern void AdLoaderClient_add_OnAdFailedToLoad_m378D89D2B93348EFADDFA501F8046155371D3B98 ();
// 0x00000041 System.Void GoogleMobileAds.iOS.AdLoaderClient::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern void AdLoaderClient_remove_OnAdFailedToLoad_m5335EC5167F9DAD3737B86B3CB8401824DD6BBAC ();
// 0x00000042 System.IntPtr GoogleMobileAds.iOS.AdLoaderClient::get_AdLoaderPtr()
extern void AdLoaderClient_get_AdLoaderPtr_m3AFA8F4324EAB0318D8726DCBBA52E5B2AC3B764 ();
// 0x00000043 System.Void GoogleMobileAds.iOS.AdLoaderClient::set_AdLoaderPtr(System.IntPtr)
extern void AdLoaderClient_set_AdLoaderPtr_m29A1E30DF114597A3625C8B0BEA0ABBE33BFDFEE ();
// 0x00000044 System.Void GoogleMobileAds.iOS.AdLoaderClient::LoadAd(GoogleMobileAds.Api.AdRequest)
extern void AdLoaderClient_LoadAd_mB74F09FB021A2FB3A5123E0A9175E16A989A4C88 ();
// 0x00000045 System.Void GoogleMobileAds.iOS.AdLoaderClient::DestroyAdLoader()
extern void AdLoaderClient_DestroyAdLoader_mC7994769E3ED1790F2EB9798FB700E21992E7AAE ();
// 0x00000046 System.Void GoogleMobileAds.iOS.AdLoaderClient::Dispose()
extern void AdLoaderClient_Dispose_mB46AD18199CCA93F20525BD0FBBD0E9DAE8B7EBC ();
// 0x00000047 System.Void GoogleMobileAds.iOS.AdLoaderClient::Finalize()
extern void AdLoaderClient_Finalize_mBBFF53308265B50E108D583F15BD8D785D0FBFC6 ();
// 0x00000048 System.Void GoogleMobileAds.iOS.AdLoaderClient::AdLoaderDidReceiveNativeCustomTemplateAdCallback(System.IntPtr,System.IntPtr,System.String)
extern void AdLoaderClient_AdLoaderDidReceiveNativeCustomTemplateAdCallback_m96CB8031ECC1E20CDB3AA013D0E1CA5C82FBA9F0 ();
// 0x00000049 System.Void GoogleMobileAds.iOS.AdLoaderClient::AdLoaderDidFailToReceiveAdWithErrorCallback(System.IntPtr,System.String)
extern void AdLoaderClient_AdLoaderDidFailToReceiveAdWithErrorCallback_mC2A70F1FD62DF7B697FF79C19CDB49D89A3ED296 ();
// 0x0000004A GoogleMobileAds.iOS.AdLoaderClient GoogleMobileAds.iOS.AdLoaderClient::IntPtrToAdLoaderClient(System.IntPtr)
extern void AdLoaderClient_IntPtrToAdLoaderClient_m3ED983610AD1859E5A69FC8FB96E50B77E0B3A3F ();
// 0x0000004B System.Void GoogleMobileAds.iOS.BannerClient::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void BannerClient_add_OnAdLoaded_m85C0A8F18C65AE4251E93125BA55EE42CCB53F0B ();
// 0x0000004C System.Void GoogleMobileAds.iOS.BannerClient::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void BannerClient_remove_OnAdLoaded_m89977460F2A599A67DC8CD8C3A8F50A197F11620 ();
// 0x0000004D System.Void GoogleMobileAds.iOS.BannerClient::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern void BannerClient_add_OnAdFailedToLoad_m5CD1230B0CF55AB9BA690B1D26BB03AC625362DE ();
// 0x0000004E System.Void GoogleMobileAds.iOS.BannerClient::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern void BannerClient_remove_OnAdFailedToLoad_m9E733D3F999D3ED8CCD1C4C5452778420B7F99E8 ();
// 0x0000004F System.Void GoogleMobileAds.iOS.BannerClient::add_OnAdOpening(System.EventHandler`1<System.EventArgs>)
extern void BannerClient_add_OnAdOpening_m1167105F6C87BB67BE6829FB15F3CA505192D27C ();
// 0x00000050 System.Void GoogleMobileAds.iOS.BannerClient::remove_OnAdOpening(System.EventHandler`1<System.EventArgs>)
extern void BannerClient_remove_OnAdOpening_m00DB05B39AE9F2A5ACE748910B5990F5B990C0D1 ();
// 0x00000051 System.Void GoogleMobileAds.iOS.BannerClient::add_OnAdClosed(System.EventHandler`1<System.EventArgs>)
extern void BannerClient_add_OnAdClosed_m9A5684725ED9AF6E48DB4E538DB176A662606AA0 ();
// 0x00000052 System.Void GoogleMobileAds.iOS.BannerClient::remove_OnAdClosed(System.EventHandler`1<System.EventArgs>)
extern void BannerClient_remove_OnAdClosed_m9C2B397BCEED79A202899C719CDB21AC52A66CD1 ();
// 0x00000053 System.Void GoogleMobileAds.iOS.BannerClient::add_OnAdLeavingApplication(System.EventHandler`1<System.EventArgs>)
extern void BannerClient_add_OnAdLeavingApplication_m59C732D86073AC57C4D34B491EEFBC9784D33053 ();
// 0x00000054 System.Void GoogleMobileAds.iOS.BannerClient::remove_OnAdLeavingApplication(System.EventHandler`1<System.EventArgs>)
extern void BannerClient_remove_OnAdLeavingApplication_m88E76B1E6239A086AF96F67390B146ACBFF1D7CA ();
// 0x00000055 System.IntPtr GoogleMobileAds.iOS.BannerClient::get_BannerViewPtr()
extern void BannerClient_get_BannerViewPtr_mCF1D51A11C17101E403C9B5968EA0A7518020C0C ();
// 0x00000056 System.Void GoogleMobileAds.iOS.BannerClient::set_BannerViewPtr(System.IntPtr)
extern void BannerClient_set_BannerViewPtr_m353E0194631D4929742E9BC6BCD8DA820877C834 ();
// 0x00000057 System.Void GoogleMobileAds.iOS.BannerClient::CreateBannerView(System.String,GoogleMobileAds.Api.AdSize,GoogleMobileAds.Api.AdPosition)
extern void BannerClient_CreateBannerView_mA9666C1CFB7BC00B379E28DC4BD57FE3D0BDE8D7 ();
// 0x00000058 System.Void GoogleMobileAds.iOS.BannerClient::CreateBannerView(System.String,GoogleMobileAds.Api.AdSize,System.Int32,System.Int32)
extern void BannerClient_CreateBannerView_mBD3B7E483609F175DF3ABC7BB9CF55F09FF966F6 ();
// 0x00000059 System.Void GoogleMobileAds.iOS.BannerClient::LoadAd(GoogleMobileAds.Api.AdRequest)
extern void BannerClient_LoadAd_mAF7404CEC6D070E8039EAE644AD57DA01F713E81 ();
// 0x0000005A System.Void GoogleMobileAds.iOS.BannerClient::ShowBannerView()
extern void BannerClient_ShowBannerView_m152A0DAD2496770376421189061540E0A9D9AB17 ();
// 0x0000005B System.Void GoogleMobileAds.iOS.BannerClient::HideBannerView()
extern void BannerClient_HideBannerView_m356318F8F8119E95BA4917D0BE95DD19C94E119B ();
// 0x0000005C System.Void GoogleMobileAds.iOS.BannerClient::DestroyBannerView()
extern void BannerClient_DestroyBannerView_m90363A637D524DD709F395C7E5AD12F5F783D563 ();
// 0x0000005D System.Void GoogleMobileAds.iOS.BannerClient::Dispose()
extern void BannerClient_Dispose_m322A94C006EAF78B295AE7F95517FA15C27BF6C9 ();
// 0x0000005E System.Void GoogleMobileAds.iOS.BannerClient::Finalize()
extern void BannerClient_Finalize_mF559A3AD4DA94DB4CF13033E6EBFD0066423CDE5 ();
// 0x0000005F System.Void GoogleMobileAds.iOS.BannerClient::AdViewDidReceiveAdCallback(System.IntPtr)
extern void BannerClient_AdViewDidReceiveAdCallback_m5F0611A8BD59EBB7E6A4CFD4AF1C20132C7D5345 ();
// 0x00000060 System.Void GoogleMobileAds.iOS.BannerClient::AdViewDidFailToReceiveAdWithErrorCallback(System.IntPtr,System.String)
extern void BannerClient_AdViewDidFailToReceiveAdWithErrorCallback_m863303F5E9FF5A436CE67C416A3E708E9E2F6206 ();
// 0x00000061 System.Void GoogleMobileAds.iOS.BannerClient::AdViewWillPresentScreenCallback(System.IntPtr)
extern void BannerClient_AdViewWillPresentScreenCallback_mCCCDADC61157A1E8B91B12B866A326AE24024816 ();
// 0x00000062 System.Void GoogleMobileAds.iOS.BannerClient::AdViewDidDismissScreenCallback(System.IntPtr)
extern void BannerClient_AdViewDidDismissScreenCallback_m15101864FE77339582C8DF5FC5129E33CFC9674B ();
// 0x00000063 System.Void GoogleMobileAds.iOS.BannerClient::AdViewWillLeaveApplicationCallback(System.IntPtr)
extern void BannerClient_AdViewWillLeaveApplicationCallback_mD2C41507939D77A4986009D02E1962A5A200AFE8 ();
// 0x00000064 GoogleMobileAds.iOS.BannerClient GoogleMobileAds.iOS.BannerClient::IntPtrToBannerClient(System.IntPtr)
extern void BannerClient_IntPtrToBannerClient_mC03443688427FD43FEDEB78D90FA808950C063C9 ();
// 0x00000065 System.Void GoogleMobileAds.iOS.BannerClient::.ctor()
extern void BannerClient__ctor_m0E636B97280FE26520BF647017A7AF7731652849 ();
// 0x00000066 System.IntPtr GoogleMobileAds.iOS.CustomNativeTemplateClient::get_CustomNativeAdPtr()
extern void CustomNativeTemplateClient_get_CustomNativeAdPtr_mF3B152C666CA5290B76A8FCF3CE0E3A6D10975F3 ();
// 0x00000067 System.Void GoogleMobileAds.iOS.CustomNativeTemplateClient::set_CustomNativeAdPtr(System.IntPtr)
extern void CustomNativeTemplateClient_set_CustomNativeAdPtr_mDB9F4A38A4EC892204971567F133026B0E1631FE ();
// 0x00000068 System.Void GoogleMobileAds.iOS.CustomNativeTemplateClient::.ctor(System.IntPtr,System.Action`2<GoogleMobileAds.Api.CustomNativeTemplateAd,System.String>)
extern void CustomNativeTemplateClient__ctor_mF23404A53988358DFD8051F45DF63F41451075DD ();
// 0x00000069 System.Collections.Generic.List`1<System.String> GoogleMobileAds.iOS.CustomNativeTemplateClient::GetAvailableAssetNames()
extern void CustomNativeTemplateClient_GetAvailableAssetNames_m9B45BF3FC124C0976BBEC7B6EDAA25065F92684C ();
// 0x0000006A System.String GoogleMobileAds.iOS.CustomNativeTemplateClient::GetTemplateId()
extern void CustomNativeTemplateClient_GetTemplateId_m03B096D3A7D6E223827C3DB8CC650B7983512BA6 ();
// 0x0000006B System.Byte[] GoogleMobileAds.iOS.CustomNativeTemplateClient::GetImageByteArray(System.String)
extern void CustomNativeTemplateClient_GetImageByteArray_m38B3756A149443C0EA1CC1E8A5994B1A8E4AFF36 ();
// 0x0000006C System.String GoogleMobileAds.iOS.CustomNativeTemplateClient::GetText(System.String)
extern void CustomNativeTemplateClient_GetText_m9C60ADACF912900D0AFA4B7EA7F97B71D86CD8E7 ();
// 0x0000006D System.Void GoogleMobileAds.iOS.CustomNativeTemplateClient::PerformClick(System.String)
extern void CustomNativeTemplateClient_PerformClick_m7CA0220F031CDDF0F19CD8E95D8C8D8A5D472D59 ();
// 0x0000006E System.Void GoogleMobileAds.iOS.CustomNativeTemplateClient::RecordImpression()
extern void CustomNativeTemplateClient_RecordImpression_m8FA4C608F4A89F3BDAA31B238BE936F698B7CB02 ();
// 0x0000006F System.Void GoogleMobileAds.iOS.CustomNativeTemplateClient::DestroyCustomNativeTemplateAd()
extern void CustomNativeTemplateClient_DestroyCustomNativeTemplateAd_m9FCFE963D61BC09DED5E01FCFDAB6BD47B12A18B ();
// 0x00000070 System.Void GoogleMobileAds.iOS.CustomNativeTemplateClient::Dispose()
extern void CustomNativeTemplateClient_Dispose_mD3CBD994BB8C2A137DCC376FBD94E27CB41BB97A ();
// 0x00000071 System.Void GoogleMobileAds.iOS.CustomNativeTemplateClient::Finalize()
extern void CustomNativeTemplateClient_Finalize_m997DED96E4DE609C78A71F6B8A0F38B33DB8AB13 ();
// 0x00000072 System.Void GoogleMobileAds.iOS.CustomNativeTemplateClient::NativeCustomTemplateDidReceiveClickCallback(System.IntPtr,System.String)
extern void CustomNativeTemplateClient_NativeCustomTemplateDidReceiveClickCallback_m93BA0F7BF0763D9ECEECB912C673A1BAED9D989F ();
// 0x00000073 GoogleMobileAds.iOS.CustomNativeTemplateClient GoogleMobileAds.iOS.CustomNativeTemplateClient::IntPtrToAdLoaderClient(System.IntPtr)
extern void CustomNativeTemplateClient_IntPtrToAdLoaderClient_m27F6CD4D1AA20E628AAB75D5DC37CB7BA3CBD91B ();
// 0x00000074 System.Void GoogleMobileAds.iOS.Externs::GADUInitialize(System.String)
extern void Externs_GADUInitialize_m2DC5EBB6A6B6A6E7A97D528CA76B3016051C0E80 ();
// 0x00000075 System.IntPtr GoogleMobileAds.iOS.Externs::GADUCreateRequest()
extern void Externs_GADUCreateRequest_m73B645271C266986E9711F38EF4E0CA6FC511D9C ();
// 0x00000076 System.IntPtr GoogleMobileAds.iOS.Externs::GADUCreateMutableDictionary()
extern void Externs_GADUCreateMutableDictionary_m71EE6107C3600A67D5A7C4D73C018CBAE659C23C ();
// 0x00000077 System.Void GoogleMobileAds.iOS.Externs::GADUMutableDictionarySetValue(System.IntPtr,System.String,System.String)
extern void Externs_GADUMutableDictionarySetValue_m1806F4834F6CCA36CD93928D50F81B097D90F60D ();
// 0x00000078 System.Void GoogleMobileAds.iOS.Externs::GADUSetMediationExtras(System.IntPtr,System.IntPtr,System.String)
extern void Externs_GADUSetMediationExtras_m89618AB332B3A9FE4FB423511B821186B35F1D2E ();
// 0x00000079 System.Void GoogleMobileAds.iOS.Externs::GADUAddTestDevice(System.IntPtr,System.String)
extern void Externs_GADUAddTestDevice_m76BAC633F8636E5D4F8BF2A1D6A9383B7BCC3866 ();
// 0x0000007A System.Void GoogleMobileAds.iOS.Externs::GADUAddKeyword(System.IntPtr,System.String)
extern void Externs_GADUAddKeyword_mCF88D72AA181DCB3C3DA3FB9BD096E64818FB492 ();
// 0x0000007B System.Void GoogleMobileAds.iOS.Externs::GADUSetBirthday(System.IntPtr,System.Int32,System.Int32,System.Int32)
extern void Externs_GADUSetBirthday_mF7965515078BEAD86EBCC07FFA7004755A1F4D4F ();
// 0x0000007C System.Void GoogleMobileAds.iOS.Externs::GADUSetGender(System.IntPtr,System.Int32)
extern void Externs_GADUSetGender_m6DA6BDFDB359466F6E2FBBC7FCF77693DA9241C2 ();
// 0x0000007D System.Void GoogleMobileAds.iOS.Externs::GADUTagForChildDirectedTreatment(System.IntPtr,System.Boolean)
extern void Externs_GADUTagForChildDirectedTreatment_m79D9D3D2E5D37AD810F239728B87D2264F994AF3 ();
// 0x0000007E System.Void GoogleMobileAds.iOS.Externs::GADUSetExtra(System.IntPtr,System.String,System.String)
extern void Externs_GADUSetExtra_m220E34583749470A8335664E94766D25674D1726 ();
// 0x0000007F System.Void GoogleMobileAds.iOS.Externs::GADUSetRequestAgent(System.IntPtr,System.String)
extern void Externs_GADUSetRequestAgent_m27CF00F87EA0EBA6A9C3C8709E5FAD8810198647 ();
// 0x00000080 System.Void GoogleMobileAds.iOS.Externs::GADURelease(System.IntPtr)
extern void Externs_GADURelease_mA1A6033D91229BF50E5357B8D12FE4B97424B08F ();
// 0x00000081 System.IntPtr GoogleMobileAds.iOS.Externs::GADUCreateBannerView(System.IntPtr,System.String,System.Int32,System.Int32,System.Int32)
extern void Externs_GADUCreateBannerView_m330BB8938EFD797A10859C7B34088781979DFEAF ();
// 0x00000082 System.IntPtr GoogleMobileAds.iOS.Externs::GADUCreateBannerViewWithCustomPosition(System.IntPtr,System.String,System.Int32,System.Int32,System.Int32,System.Int32)
extern void Externs_GADUCreateBannerViewWithCustomPosition_m09B328E45301936407727CFBA9569AF7960EE599 ();
// 0x00000083 System.IntPtr GoogleMobileAds.iOS.Externs::GADUCreateSmartBannerView(System.IntPtr,System.String,System.Int32)
extern void Externs_GADUCreateSmartBannerView_mCB9BA3A611AA1D22C3C1AB0ABFE5027FFD670557 ();
// 0x00000084 System.IntPtr GoogleMobileAds.iOS.Externs::GADUCreateSmartBannerViewWithCustomPosition(System.IntPtr,System.String,System.Int32,System.Int32)
extern void Externs_GADUCreateSmartBannerViewWithCustomPosition_mB7E8C01D1505C4FF0587E32157AB911C69E93E8C ();
// 0x00000085 System.Void GoogleMobileAds.iOS.Externs::GADUSetBannerCallbacks(System.IntPtr,GoogleMobileAds.iOS.BannerClient_GADUAdViewDidReceiveAdCallback,GoogleMobileAds.iOS.BannerClient_GADUAdViewDidFailToReceiveAdWithErrorCallback,GoogleMobileAds.iOS.BannerClient_GADUAdViewWillPresentScreenCallback,GoogleMobileAds.iOS.BannerClient_GADUAdViewDidDismissScreenCallback,GoogleMobileAds.iOS.BannerClient_GADUAdViewWillLeaveApplicationCallback)
extern void Externs_GADUSetBannerCallbacks_m55B99E0FDF40B0CA06351FDED7D6FE82D71C238C ();
// 0x00000086 System.Void GoogleMobileAds.iOS.Externs::GADUHideBannerView(System.IntPtr)
extern void Externs_GADUHideBannerView_mB52E09C130A2CB24A423B0857D8342D100EB28F2 ();
// 0x00000087 System.Void GoogleMobileAds.iOS.Externs::GADUShowBannerView(System.IntPtr)
extern void Externs_GADUShowBannerView_m472CD3FA35C432E8ECDD7BA266316F5DDDEC224F ();
// 0x00000088 System.Void GoogleMobileAds.iOS.Externs::GADURemoveBannerView(System.IntPtr)
extern void Externs_GADURemoveBannerView_m58C4B13ACDD405DB163F13B37D870086EC140091 ();
// 0x00000089 System.Void GoogleMobileAds.iOS.Externs::GADURequestBannerAd(System.IntPtr,System.IntPtr)
extern void Externs_GADURequestBannerAd_m4D4212C3D10F54900739111245FF7D08CB832D67 ();
// 0x0000008A System.IntPtr GoogleMobileAds.iOS.Externs::GADUCreateInterstitial(System.IntPtr,System.String)
extern void Externs_GADUCreateInterstitial_mBB5011FE3F7A220A2A1C18491EB2FA1CC8BF228F ();
// 0x0000008B System.Void GoogleMobileAds.iOS.Externs::GADUSetInterstitialCallbacks(System.IntPtr,GoogleMobileAds.iOS.InterstitialClient_GADUInterstitialDidReceiveAdCallback,GoogleMobileAds.iOS.InterstitialClient_GADUInterstitialDidFailToReceiveAdWithErrorCallback,GoogleMobileAds.iOS.InterstitialClient_GADUInterstitialWillPresentScreenCallback,GoogleMobileAds.iOS.InterstitialClient_GADUInterstitialDidDismissScreenCallback,GoogleMobileAds.iOS.InterstitialClient_GADUInterstitialWillLeaveApplicationCallback)
extern void Externs_GADUSetInterstitialCallbacks_m6C17569859C333E78DB7B262DF49FD49474F3B1E ();
// 0x0000008C System.Boolean GoogleMobileAds.iOS.Externs::GADUInterstitialReady(System.IntPtr)
extern void Externs_GADUInterstitialReady_m3B59FBBF66CFFED00743CA573A1ABCEBA2D721AA ();
// 0x0000008D System.Void GoogleMobileAds.iOS.Externs::GADUShowInterstitial(System.IntPtr)
extern void Externs_GADUShowInterstitial_mFAE7157C540B0F2A4A2287E1F4987862232D314A ();
// 0x0000008E System.Void GoogleMobileAds.iOS.Externs::GADURequestInterstitial(System.IntPtr,System.IntPtr)
extern void Externs_GADURequestInterstitial_m7C292EA914D2701FF7E8F34827B6C327E9CEA2ED ();
// 0x0000008F System.IntPtr GoogleMobileAds.iOS.Externs::GADUCreateRewardBasedVideoAd(System.IntPtr)
extern void Externs_GADUCreateRewardBasedVideoAd_m9DC3169D059E38DC294D25299004797599C5315D ();
// 0x00000090 System.Boolean GoogleMobileAds.iOS.Externs::GADURewardBasedVideoAdReady(System.IntPtr)
extern void Externs_GADURewardBasedVideoAdReady_mD3AF839910B89E6C292D2D07842834803B6F4E57 ();
// 0x00000091 System.Void GoogleMobileAds.iOS.Externs::GADUShowRewardBasedVideoAd(System.IntPtr)
extern void Externs_GADUShowRewardBasedVideoAd_m79B32218E887318B0CC061E0E7DC56D2E018D82E ();
// 0x00000092 System.Void GoogleMobileAds.iOS.Externs::GADURequestRewardBasedVideoAd(System.IntPtr,System.IntPtr,System.String)
extern void Externs_GADURequestRewardBasedVideoAd_m03CAA6C4D6CB4D2D1A949AEBE3F70072826A42DE ();
// 0x00000093 System.Void GoogleMobileAds.iOS.Externs::GADUSetRewardBasedVideoAdCallbacks(System.IntPtr,GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidReceiveAdCallback,GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback,GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidOpenCallback,GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidStartCallback,GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidCloseCallback,GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidRewardCallback,GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdWillLeaveApplicationCallback)
extern void Externs_GADUSetRewardBasedVideoAdCallbacks_m84A73BE6D0604C0CEAB5F25DC04B113F502A58AC ();
// 0x00000094 System.IntPtr GoogleMobileAds.iOS.Externs::GADUCreateAdLoader(System.IntPtr,System.String,System.String[],System.Int32,GoogleMobileAds.iOS.NativeAdTypes&)
extern void Externs_GADUCreateAdLoader_m52B92F08A6DC29B35188A34463524EA5F84466E7 ();
// 0x00000095 System.Void GoogleMobileAds.iOS.Externs::GADURequestNativeAd(System.IntPtr,System.IntPtr)
extern void Externs_GADURequestNativeAd_m771F421E091A4BA77364824E1059AA86E8DFEA0C ();
// 0x00000096 System.Void GoogleMobileAds.iOS.Externs::GADUSetAdLoaderCallbacks(System.IntPtr,GoogleMobileAds.iOS.AdLoaderClient_GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback,GoogleMobileAds.iOS.AdLoaderClient_GADUAdLoaderDidFailToReceiveAdWithErrorCallback)
extern void Externs_GADUSetAdLoaderCallbacks_m8F706419533752CD86C86039F02E81E4FE45255C ();
// 0x00000097 System.String GoogleMobileAds.iOS.Externs::GADUNativeCustomTemplateAdTemplateID(System.IntPtr)
extern void Externs_GADUNativeCustomTemplateAdTemplateID_mF258BA627C6B3F615798C35EA50742E6572B0A3B ();
// 0x00000098 System.String GoogleMobileAds.iOS.Externs::GADUNativeCustomTemplateAdImageAsBytesForKey(System.IntPtr,System.String)
extern void Externs_GADUNativeCustomTemplateAdImageAsBytesForKey_m177D2CFB72DEB8199E879D01D9615E31B4590583 ();
// 0x00000099 System.String GoogleMobileAds.iOS.Externs::GADUNativeCustomTemplateAdStringForKey(System.IntPtr,System.String)
extern void Externs_GADUNativeCustomTemplateAdStringForKey_mAEE62F9026067FF4B82ABD1C32CD308B2FE48BE0 ();
// 0x0000009A System.Void GoogleMobileAds.iOS.Externs::GADUNativeCustomTemplateAdRecordImpression(System.IntPtr)
extern void Externs_GADUNativeCustomTemplateAdRecordImpression_m86F37A2179013E352F967E169A7DE932DB031D8D ();
// 0x0000009B System.Void GoogleMobileAds.iOS.Externs::GADUNativeCustomTemplateAdPerformClickOnAssetWithKey(System.IntPtr,System.String,System.Boolean)
extern void Externs_GADUNativeCustomTemplateAdPerformClickOnAssetWithKey_m901C391C058E7C249B091DBBAB301A98F8BE83BA ();
// 0x0000009C System.IntPtr GoogleMobileAds.iOS.Externs::GADUNativeCustomTemplateAdAvailableAssetKeys(System.IntPtr)
extern void Externs_GADUNativeCustomTemplateAdAvailableAssetKeys_m6A2827935AEACD365679F1F2A3A3843C768C05A0 ();
// 0x0000009D System.Int32 GoogleMobileAds.iOS.Externs::GADUNativeCustomTemplateAdNumberOfAvailableAssetKeys(System.IntPtr)
extern void Externs_GADUNativeCustomTemplateAdNumberOfAvailableAssetKeys_mD55D743161D0F80D2E3A85A722697B6C2C854EEC ();
// 0x0000009E System.Void GoogleMobileAds.iOS.Externs::GADUSetNativeCustomTemplateAdUnityClient(System.IntPtr,System.IntPtr)
extern void Externs_GADUSetNativeCustomTemplateAdUnityClient_mE7A1817DDFDE2130C8B6649A1FE837ED88340491 ();
// 0x0000009F System.Void GoogleMobileAds.iOS.Externs::GADUSetNativeCustomTemplateAdCallbacks(System.IntPtr,GoogleMobileAds.iOS.CustomNativeTemplateClient_GADUNativeCustomTemplateDidReceiveClick)
extern void Externs_GADUSetNativeCustomTemplateAdCallbacks_m38D48258FB3008B874889D374D275B29EF7FC13F ();
// 0x000000A0 System.IntPtr GoogleMobileAds.iOS.Externs::GADUCreateNativeExpressAdView(System.IntPtr,System.String,System.Int32,System.Int32,System.Int32)
extern void Externs_GADUCreateNativeExpressAdView_m3A9843414E578E7F0C5BEE5B37B8FFE7C7396E0C ();
// 0x000000A1 System.IntPtr GoogleMobileAds.iOS.Externs::GADUCreateNativeExpressAdViewWithCustomPosition(System.IntPtr,System.String,System.Int32,System.Int32,System.Int32,System.Int32)
extern void Externs_GADUCreateNativeExpressAdViewWithCustomPosition_mAF8E09A8A9BA503C47ED472848E76BBD2250EB02 ();
// 0x000000A2 System.Void GoogleMobileAds.iOS.Externs::GADUSetNativeExpressAdCallbacks(System.IntPtr,GoogleMobileAds.iOS.NativeExpressAdClient_GADUNativeExpressAdViewDidReceiveAdCallback,GoogleMobileAds.iOS.NativeExpressAdClient_GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback,GoogleMobileAds.iOS.NativeExpressAdClient_GADUNativeExpressAdViewWillPresentScreenCallback,GoogleMobileAds.iOS.NativeExpressAdClient_GADUNativeExpressAdViewDidDismissScreenCallback,GoogleMobileAds.iOS.NativeExpressAdClient_GADUNativeExpressAdViewWillLeaveApplicationCallback)
extern void Externs_GADUSetNativeExpressAdCallbacks_mBB0A35806F4092661B77D0B0843978A3E704BF96 ();
// 0x000000A3 System.Void GoogleMobileAds.iOS.Externs::GADUHideNativeExpressAdView(System.IntPtr)
extern void Externs_GADUHideNativeExpressAdView_mC2C950CA25B51FC0BA4649E64ECD561C5CE9E9A0 ();
// 0x000000A4 System.Void GoogleMobileAds.iOS.Externs::GADUShowNativeExpressAdView(System.IntPtr)
extern void Externs_GADUShowNativeExpressAdView_m8EFEBC6156D1ABE4E74C09B81704C54970645941 ();
// 0x000000A5 System.Void GoogleMobileAds.iOS.Externs::GADURemoveNativeExpressAdView(System.IntPtr)
extern void Externs_GADURemoveNativeExpressAdView_m849B78D65F7208C51A6DDFB235ACDF5A8C8125BD ();
// 0x000000A6 System.Void GoogleMobileAds.iOS.Externs::GADURequestNativeExpressAd(System.IntPtr,System.IntPtr)
extern void Externs_GADURequestNativeExpressAd_mC9BCB2D886B637A3764F80AEF1ABE148214941B6 ();
// 0x000000A7 System.Void GoogleMobileAds.iOS.Externs::.ctor()
extern void Externs__ctor_m1B8F9795C92865B5C37DF1F73C0DADDCFAF1D27E ();
// 0x000000A8 System.Void GoogleMobileAds.iOS.InterstitialClient::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void InterstitialClient_add_OnAdLoaded_m7FF81DFE1D529E663594F56F1F08E758C53A7978 ();
// 0x000000A9 System.Void GoogleMobileAds.iOS.InterstitialClient::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void InterstitialClient_remove_OnAdLoaded_m18393564E716B23C6FDC0DE4593847CFFB224F71 ();
// 0x000000AA System.Void GoogleMobileAds.iOS.InterstitialClient::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern void InterstitialClient_add_OnAdFailedToLoad_m20194C2A5AB143538AB1B86027F067A8DE89BDC0 ();
// 0x000000AB System.Void GoogleMobileAds.iOS.InterstitialClient::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern void InterstitialClient_remove_OnAdFailedToLoad_m8DCD9420EE00D7C67E9A2DFB5725DB930388D37E ();
// 0x000000AC System.Void GoogleMobileAds.iOS.InterstitialClient::add_OnAdOpening(System.EventHandler`1<System.EventArgs>)
extern void InterstitialClient_add_OnAdOpening_mDDEBB7FE0D0D739A4AE516FF7F4E11A1804FC848 ();
// 0x000000AD System.Void GoogleMobileAds.iOS.InterstitialClient::remove_OnAdOpening(System.EventHandler`1<System.EventArgs>)
extern void InterstitialClient_remove_OnAdOpening_mF021A585CD392E9EC4200EC6C7ACDAE666C3D088 ();
// 0x000000AE System.Void GoogleMobileAds.iOS.InterstitialClient::add_OnAdClosed(System.EventHandler`1<System.EventArgs>)
extern void InterstitialClient_add_OnAdClosed_m6FB7CBF39A25A8605A845417E241CDD2D6C332A2 ();
// 0x000000AF System.Void GoogleMobileAds.iOS.InterstitialClient::remove_OnAdClosed(System.EventHandler`1<System.EventArgs>)
extern void InterstitialClient_remove_OnAdClosed_mCA6AD28BEB8B346EB283375AD38B4CA876DCFB3C ();
// 0x000000B0 System.Void GoogleMobileAds.iOS.InterstitialClient::add_OnAdLeavingApplication(System.EventHandler`1<System.EventArgs>)
extern void InterstitialClient_add_OnAdLeavingApplication_m8466A9CF5F9EC993D6F544022B7A412776C58FCF ();
// 0x000000B1 System.Void GoogleMobileAds.iOS.InterstitialClient::remove_OnAdLeavingApplication(System.EventHandler`1<System.EventArgs>)
extern void InterstitialClient_remove_OnAdLeavingApplication_m2759CB8145124D7CE0C895ACAA3F7CCE9F2E73DC ();
// 0x000000B2 System.IntPtr GoogleMobileAds.iOS.InterstitialClient::get_InterstitialPtr()
extern void InterstitialClient_get_InterstitialPtr_mF19309BA5A75A3592B55C9034703C536CD798D22 ();
// 0x000000B3 System.Void GoogleMobileAds.iOS.InterstitialClient::set_InterstitialPtr(System.IntPtr)
extern void InterstitialClient_set_InterstitialPtr_m46CD1113827F95A2556B498FF8A605C98C7C1F15 ();
// 0x000000B4 System.Void GoogleMobileAds.iOS.InterstitialClient::CreateInterstitialAd(System.String)
extern void InterstitialClient_CreateInterstitialAd_m7E082B54344097D75CAD764A3F87FF07C38640DA ();
// 0x000000B5 System.Void GoogleMobileAds.iOS.InterstitialClient::LoadAd(GoogleMobileAds.Api.AdRequest)
extern void InterstitialClient_LoadAd_m274EC58618BCC9934F96D414E3B77DD06EAC08F7 ();
// 0x000000B6 System.Boolean GoogleMobileAds.iOS.InterstitialClient::IsLoaded()
extern void InterstitialClient_IsLoaded_m3E8FCA13281A0743EBC6558A432F5AE9EF7F1045 ();
// 0x000000B7 System.Void GoogleMobileAds.iOS.InterstitialClient::ShowInterstitial()
extern void InterstitialClient_ShowInterstitial_m619602539A7123C311D8EBF1D0FC4F956789A2A0 ();
// 0x000000B8 System.Void GoogleMobileAds.iOS.InterstitialClient::DestroyInterstitial()
extern void InterstitialClient_DestroyInterstitial_mB91E830A772ADBE03B4CF2077D338C28FD3693EC ();
// 0x000000B9 System.Void GoogleMobileAds.iOS.InterstitialClient::Dispose()
extern void InterstitialClient_Dispose_mA736ECCD646683B1AA60A8DA130D7D550ECBE039 ();
// 0x000000BA System.Void GoogleMobileAds.iOS.InterstitialClient::Finalize()
extern void InterstitialClient_Finalize_mE3E095E94743AC0781E1058A4F7DCF261FF4D383 ();
// 0x000000BB System.Void GoogleMobileAds.iOS.InterstitialClient::InterstitialDidReceiveAdCallback(System.IntPtr)
extern void InterstitialClient_InterstitialDidReceiveAdCallback_mF485EB5E41F80ED5C5B6FF0376CD4B8BB96EED2B ();
// 0x000000BC System.Void GoogleMobileAds.iOS.InterstitialClient::InterstitialDidFailToReceiveAdWithErrorCallback(System.IntPtr,System.String)
extern void InterstitialClient_InterstitialDidFailToReceiveAdWithErrorCallback_m9BD349E80A9B92676F0BF98C52C36A690DF107CA ();
// 0x000000BD System.Void GoogleMobileAds.iOS.InterstitialClient::InterstitialWillPresentScreenCallback(System.IntPtr)
extern void InterstitialClient_InterstitialWillPresentScreenCallback_mB5835C4991F852B2E0768EAE7F9E9478EB2FD2C5 ();
// 0x000000BE System.Void GoogleMobileAds.iOS.InterstitialClient::InterstitialDidDismissScreenCallback(System.IntPtr)
extern void InterstitialClient_InterstitialDidDismissScreenCallback_mAD21EEBDD6D829719AF1FB8F438CC04C63EF431C ();
// 0x000000BF System.Void GoogleMobileAds.iOS.InterstitialClient::InterstitialWillLeaveApplicationCallback(System.IntPtr)
extern void InterstitialClient_InterstitialWillLeaveApplicationCallback_m41A6D1FEF7A23CB20F37D767455B69D71FE83D42 ();
// 0x000000C0 GoogleMobileAds.iOS.InterstitialClient GoogleMobileAds.iOS.InterstitialClient::IntPtrToInterstitialClient(System.IntPtr)
extern void InterstitialClient_IntPtrToInterstitialClient_mD7E495556451E3A916AED6AD9097FCAA4A0ED9DE ();
// 0x000000C1 System.Void GoogleMobileAds.iOS.InterstitialClient::.ctor()
extern void InterstitialClient__ctor_mA038B4FD51D6ECEC361A8E32BB6E2E89009BE522 ();
// 0x000000C2 System.Void GoogleMobileAds.iOS.MobileAdsClient::.ctor()
extern void MobileAdsClient__ctor_mC8DAE3EAC3FD1D338504A7CE0B5CD1D2074E0B24 ();
// 0x000000C3 GoogleMobileAds.iOS.MobileAdsClient GoogleMobileAds.iOS.MobileAdsClient::get_Instance()
extern void MobileAdsClient_get_Instance_m1D2605F82CAB8B9522BDFF9EB65DBDA7AD53D11F ();
// 0x000000C4 System.Void GoogleMobileAds.iOS.MobileAdsClient::Initialize(System.String)
extern void MobileAdsClient_Initialize_mD8E3D0BB200F36413F9D15FE914DED546EA98EFC ();
// 0x000000C5 System.Void GoogleMobileAds.iOS.MobileAdsClient::.cctor()
extern void MobileAdsClient__cctor_m5C56A069E99872AC4D84284C79EEDA47CD71BE90 ();
// 0x000000C6 System.Void GoogleMobileAds.iOS.NativeExpressAdClient::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void NativeExpressAdClient_add_OnAdLoaded_m61B75205166DB21299020E7F2927A03666AA8C5C ();
// 0x000000C7 System.Void GoogleMobileAds.iOS.NativeExpressAdClient::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void NativeExpressAdClient_remove_OnAdLoaded_m304395F0D657E6A788D3DD7612CE4F32FBF1697E ();
// 0x000000C8 System.Void GoogleMobileAds.iOS.NativeExpressAdClient::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern void NativeExpressAdClient_add_OnAdFailedToLoad_mE938E4F0AFC94FDA0D753EC69E10B78AC3E4C504 ();
// 0x000000C9 System.Void GoogleMobileAds.iOS.NativeExpressAdClient::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern void NativeExpressAdClient_remove_OnAdFailedToLoad_m5731DC10D2A0D455F9A883C7518F2F685A4839AE ();
// 0x000000CA System.Void GoogleMobileAds.iOS.NativeExpressAdClient::add_OnAdOpening(System.EventHandler`1<System.EventArgs>)
extern void NativeExpressAdClient_add_OnAdOpening_m2429A951065B6822828164BEBB278FA81C225B11 ();
// 0x000000CB System.Void GoogleMobileAds.iOS.NativeExpressAdClient::remove_OnAdOpening(System.EventHandler`1<System.EventArgs>)
extern void NativeExpressAdClient_remove_OnAdOpening_mD4B56212A51E8E93D15B55749991EEB3CD68349B ();
// 0x000000CC System.Void GoogleMobileAds.iOS.NativeExpressAdClient::add_OnAdClosed(System.EventHandler`1<System.EventArgs>)
extern void NativeExpressAdClient_add_OnAdClosed_m76C7A38AE602DF481FA3FDA746BA443556454D68 ();
// 0x000000CD System.Void GoogleMobileAds.iOS.NativeExpressAdClient::remove_OnAdClosed(System.EventHandler`1<System.EventArgs>)
extern void NativeExpressAdClient_remove_OnAdClosed_mDF5CC6CC487B96415EDAC2C8A08A7792D974CE32 ();
// 0x000000CE System.Void GoogleMobileAds.iOS.NativeExpressAdClient::add_OnAdLeavingApplication(System.EventHandler`1<System.EventArgs>)
extern void NativeExpressAdClient_add_OnAdLeavingApplication_m22264B2690FB5DA4412EA65723D86AFA90977194 ();
// 0x000000CF System.Void GoogleMobileAds.iOS.NativeExpressAdClient::remove_OnAdLeavingApplication(System.EventHandler`1<System.EventArgs>)
extern void NativeExpressAdClient_remove_OnAdLeavingApplication_m92BD728ABD5C29100C0453DE44763D1EBCD77339 ();
// 0x000000D0 System.IntPtr GoogleMobileAds.iOS.NativeExpressAdClient::get_NativeExpressAdViewPtr()
extern void NativeExpressAdClient_get_NativeExpressAdViewPtr_m60BDC950D04AD2343B12FC792C26C5B3F3AE85A6 ();
// 0x000000D1 System.Void GoogleMobileAds.iOS.NativeExpressAdClient::set_NativeExpressAdViewPtr(System.IntPtr)
extern void NativeExpressAdClient_set_NativeExpressAdViewPtr_m826D00251F3B7DF9BE6A1F2D23F2C81CC2768CE1 ();
// 0x000000D2 System.Void GoogleMobileAds.iOS.NativeExpressAdClient::CreateNativeExpressAdView(System.String,GoogleMobileAds.Api.AdSize,GoogleMobileAds.Api.AdPosition)
extern void NativeExpressAdClient_CreateNativeExpressAdView_m4BD420FADED6190A7DFA895D15A6092BB11BD45B ();
// 0x000000D3 System.Void GoogleMobileAds.iOS.NativeExpressAdClient::CreateNativeExpressAdView(System.String,GoogleMobileAds.Api.AdSize,System.Int32,System.Int32)
extern void NativeExpressAdClient_CreateNativeExpressAdView_m635C29C84C42BFCC1B67ABECFCAC8CAB881F61B2 ();
// 0x000000D4 System.Void GoogleMobileAds.iOS.NativeExpressAdClient::LoadAd(GoogleMobileAds.Api.AdRequest)
extern void NativeExpressAdClient_LoadAd_mF62C1ABCEE4593D2522C2364A932505718341672 ();
// 0x000000D5 System.Void GoogleMobileAds.iOS.NativeExpressAdClient::ShowNativeExpressAdView()
extern void NativeExpressAdClient_ShowNativeExpressAdView_mBFD045AF0E6C94B4C37F09A01D1938F98CBE7CE2 ();
// 0x000000D6 System.Void GoogleMobileAds.iOS.NativeExpressAdClient::HideNativeExpressAdView()
extern void NativeExpressAdClient_HideNativeExpressAdView_m41576E80275B343E9E11D7F0D9D5A88810825CE1 ();
// 0x000000D7 System.Void GoogleMobileAds.iOS.NativeExpressAdClient::DestroyNativeExpressAdView()
extern void NativeExpressAdClient_DestroyNativeExpressAdView_m5A6634ACAD43D15C765AD610615C263E3A98838B ();
// 0x000000D8 System.Void GoogleMobileAds.iOS.NativeExpressAdClient::Dispose()
extern void NativeExpressAdClient_Dispose_mEEDD761DA08FEC9C03B298D728A87DBAC1D70885 ();
// 0x000000D9 System.Void GoogleMobileAds.iOS.NativeExpressAdClient::Finalize()
extern void NativeExpressAdClient_Finalize_m3DEA3D88CEECB33693084B72B2AFA7F808F4B084 ();
// 0x000000DA System.Void GoogleMobileAds.iOS.NativeExpressAdClient::NativeExpressAdViewDidReceiveAdCallback(System.IntPtr)
extern void NativeExpressAdClient_NativeExpressAdViewDidReceiveAdCallback_m7E5BCA95DAE8C27B53B4DF27571E1D47DC1BC924 ();
// 0x000000DB System.Void GoogleMobileAds.iOS.NativeExpressAdClient::NativeExpressAdViewDidFailToReceiveAdWithErrorCallback(System.IntPtr,System.String)
extern void NativeExpressAdClient_NativeExpressAdViewDidFailToReceiveAdWithErrorCallback_m02B2DBEB39F61BC6B4188F167105425E5139DF1B ();
// 0x000000DC System.Void GoogleMobileAds.iOS.NativeExpressAdClient::NativeExpressAdViewWillPresentScreenCallback(System.IntPtr)
extern void NativeExpressAdClient_NativeExpressAdViewWillPresentScreenCallback_m374AF054824A2809817D4D34D05128633FD80BDB ();
// 0x000000DD System.Void GoogleMobileAds.iOS.NativeExpressAdClient::NativeExpressAdViewDidDismissScreenCallback(System.IntPtr)
extern void NativeExpressAdClient_NativeExpressAdViewDidDismissScreenCallback_mB912EF80149AE81DA955BB54023A8919BEE81671 ();
// 0x000000DE System.Void GoogleMobileAds.iOS.NativeExpressAdClient::NativeExpressAdViewWillLeaveApplicationCallback(System.IntPtr)
extern void NativeExpressAdClient_NativeExpressAdViewWillLeaveApplicationCallback_m39E270E3EBE1071BA14C4D71464294BAA4C70D28 ();
// 0x000000DF GoogleMobileAds.iOS.NativeExpressAdClient GoogleMobileAds.iOS.NativeExpressAdClient::IntPtrToNativeExpressAdClient(System.IntPtr)
extern void NativeExpressAdClient_IntPtrToNativeExpressAdClient_mC775C276A6914274D8C3EF4794F2305F7EE93179 ();
// 0x000000E0 System.Void GoogleMobileAds.iOS.NativeExpressAdClient::.ctor()
extern void NativeExpressAdClient__ctor_mD9356B704E083447EE18E5B58EDD1E036A4775FF ();
// 0x000000E1 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void RewardBasedVideoAdClient_add_OnAdLoaded_m288E7403FCEEB56AC329BC56EE21F26A2542C0EE ();
// 0x000000E2 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void RewardBasedVideoAdClient_remove_OnAdLoaded_mF89D4EA7868CD288E348E448E4700F1E41879F32 ();
// 0x000000E3 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern void RewardBasedVideoAdClient_add_OnAdFailedToLoad_mCCB067CA039D5122E45558FC43C079EA93AB37D8 ();
// 0x000000E4 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern void RewardBasedVideoAdClient_remove_OnAdFailedToLoad_mF64A6F2271989A44A2DDB626435F0937063006FB ();
// 0x000000E5 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::add_OnAdOpening(System.EventHandler`1<System.EventArgs>)
extern void RewardBasedVideoAdClient_add_OnAdOpening_mDFCB39F3DCADE57957CD0A3F520F4D8D6B78FACB ();
// 0x000000E6 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::remove_OnAdOpening(System.EventHandler`1<System.EventArgs>)
extern void RewardBasedVideoAdClient_remove_OnAdOpening_mC7D1ACF64C9F157E0DB01C11AC40FC88267A2CA7 ();
// 0x000000E7 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::add_OnAdStarted(System.EventHandler`1<System.EventArgs>)
extern void RewardBasedVideoAdClient_add_OnAdStarted_mBB127E18532FA7929D514A2017601A91FAFB0669 ();
// 0x000000E8 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::remove_OnAdStarted(System.EventHandler`1<System.EventArgs>)
extern void RewardBasedVideoAdClient_remove_OnAdStarted_m41DF78DD621ED7B214ECB2F6523FD7FBB6D46885 ();
// 0x000000E9 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::add_OnAdClosed(System.EventHandler`1<System.EventArgs>)
extern void RewardBasedVideoAdClient_add_OnAdClosed_m2BE6E86ABA9B6DC4740FE149F6E1A15D274F0371 ();
// 0x000000EA System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::remove_OnAdClosed(System.EventHandler`1<System.EventArgs>)
extern void RewardBasedVideoAdClient_remove_OnAdClosed_m7200B30E383D1B110645104197258F422CC8B991 ();
// 0x000000EB System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::add_OnAdRewarded(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
extern void RewardBasedVideoAdClient_add_OnAdRewarded_mBD07CC223AA48F3DB977E8EF08781C3093EFD101 ();
// 0x000000EC System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::remove_OnAdRewarded(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
extern void RewardBasedVideoAdClient_remove_OnAdRewarded_mE7D586DDA5CCB3AB5650417C1C773C5DAB6EC06C ();
// 0x000000ED System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::add_OnAdLeavingApplication(System.EventHandler`1<System.EventArgs>)
extern void RewardBasedVideoAdClient_add_OnAdLeavingApplication_m57561C2BE3DB4D2EEDB9F0334190E76D2886A367 ();
// 0x000000EE System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::remove_OnAdLeavingApplication(System.EventHandler`1<System.EventArgs>)
extern void RewardBasedVideoAdClient_remove_OnAdLeavingApplication_m53755FBA928AAB13C28DF51590FC099213300F64 ();
// 0x000000EF System.IntPtr GoogleMobileAds.iOS.RewardBasedVideoAdClient::get_RewardBasedVideoAdPtr()
extern void RewardBasedVideoAdClient_get_RewardBasedVideoAdPtr_m1F3DE898E6CFB9607D5567EE5D5E73EF2644863E ();
// 0x000000F0 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::set_RewardBasedVideoAdPtr(System.IntPtr)
extern void RewardBasedVideoAdClient_set_RewardBasedVideoAdPtr_mD144E86D1BE08F42F0F01B813408430BE4DDE370 ();
// 0x000000F1 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::CreateRewardBasedVideoAd()
extern void RewardBasedVideoAdClient_CreateRewardBasedVideoAd_mCED64E1E751A92726F4CF42B848FB2E457DE4A98 ();
// 0x000000F2 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::LoadAd(GoogleMobileAds.Api.AdRequest,System.String)
extern void RewardBasedVideoAdClient_LoadAd_m2473195E7EF1C6A67CEFCA8E944FF437B61823CD ();
// 0x000000F3 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::ShowRewardBasedVideoAd()
extern void RewardBasedVideoAdClient_ShowRewardBasedVideoAd_m890F18B07EA6EF8C51E96F99DD087F7A897D07C7 ();
// 0x000000F4 System.Boolean GoogleMobileAds.iOS.RewardBasedVideoAdClient::IsLoaded()
extern void RewardBasedVideoAdClient_IsLoaded_m376917C457D49B46EF76E0AE492CDDEAE5901D12 ();
// 0x000000F5 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::DestroyRewardedVideoAd()
extern void RewardBasedVideoAdClient_DestroyRewardedVideoAd_m835649B9B66B4BB350D3DD11D962029617A2EEB2 ();
// 0x000000F6 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::Dispose()
extern void RewardBasedVideoAdClient_Dispose_mE33CCE97427FA4CBABC6539BAFDCA10DC96F91B5 ();
// 0x000000F7 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::Finalize()
extern void RewardBasedVideoAdClient_Finalize_m98F5C3570DAE9CE50E027B352EE82A03CC7FE63B ();
// 0x000000F8 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::RewardBasedVideoAdDidReceiveAdCallback(System.IntPtr)
extern void RewardBasedVideoAdClient_RewardBasedVideoAdDidReceiveAdCallback_mC3AC0F651C19E19CC6247CB1AF7E694C3A8968E2 ();
// 0x000000F9 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::RewardBasedVideoAdDidFailToReceiveAdWithErrorCallback(System.IntPtr,System.String)
extern void RewardBasedVideoAdClient_RewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_m92F8D48CFD9859C3DF734B3B531377978AF137BD ();
// 0x000000FA System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::RewardBasedVideoAdDidOpenCallback(System.IntPtr)
extern void RewardBasedVideoAdClient_RewardBasedVideoAdDidOpenCallback_m139F9D3FE045E6AD73E3C653FD5BC0D1B4CF1901 ();
// 0x000000FB System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::RewardBasedVideoAdDidStartCallback(System.IntPtr)
extern void RewardBasedVideoAdClient_RewardBasedVideoAdDidStartCallback_mF45B63FD0D47CFC8566ED5B3137D933247CBAE87 ();
// 0x000000FC System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::RewardBasedVideoAdDidCloseCallback(System.IntPtr)
extern void RewardBasedVideoAdClient_RewardBasedVideoAdDidCloseCallback_m734CE280D2BBC57576B3A57E32C521E83443B202 ();
// 0x000000FD System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::RewardBasedVideoAdDidRewardUserCallback(System.IntPtr,System.String,System.Double)
extern void RewardBasedVideoAdClient_RewardBasedVideoAdDidRewardUserCallback_mC1CA5205857E2C48D6CB3F2BE820505DF05D7070 ();
// 0x000000FE System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::RewardBasedVideoAdWillLeaveApplicationCallback(System.IntPtr)
extern void RewardBasedVideoAdClient_RewardBasedVideoAdWillLeaveApplicationCallback_mFF7E45939A22B52E3C318C1115C13E42827B349C ();
// 0x000000FF GoogleMobileAds.iOS.RewardBasedVideoAdClient GoogleMobileAds.iOS.RewardBasedVideoAdClient::IntPtrToRewardBasedVideoClient(System.IntPtr)
extern void RewardBasedVideoAdClient_IntPtrToRewardBasedVideoClient_mAB84423F44303BF2C18015BF7BDFFDAB40C8253A ();
// 0x00000100 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient::.ctor()
extern void RewardBasedVideoAdClient__ctor_m7E434825A64254490DE421DF6FCBD47DE4C363D9 ();
// 0x00000101 System.IntPtr GoogleMobileAds.iOS.Utils::BuildAdRequest(GoogleMobileAds.Api.AdRequest)
extern void Utils_BuildAdRequest_m79F99FD27AEBB11A401DF217D6C5F537B8CC9FA2 ();
// 0x00000102 System.Void GoogleMobileAds.iOS.Utils::.ctor()
extern void Utils__ctor_mFAEDAC0248C372B9CD40E50183B3662EB5B4A7AC ();
// 0x00000103 System.Void GoogleMobileAds.Common.DummyClient::.ctor()
extern void DummyClient__ctor_m415BC83649885989D5632B2CFA42D1AAB8DE2390 ();
// 0x00000104 System.Void GoogleMobileAds.Common.DummyClient::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void DummyClient_add_OnAdLoaded_m5DF52E7736ECF965A548E5F68A070A4C4D88C3DD ();
// 0x00000105 System.Void GoogleMobileAds.Common.DummyClient::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void DummyClient_remove_OnAdLoaded_mE4FB7E15A79B727B7DD7CA55BA7EA50B104804B5 ();
// 0x00000106 System.Void GoogleMobileAds.Common.DummyClient::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern void DummyClient_add_OnAdFailedToLoad_m924E35A4925572FDE96E634DBF557410306EC706 ();
// 0x00000107 System.Void GoogleMobileAds.Common.DummyClient::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern void DummyClient_remove_OnAdFailedToLoad_m68725E31184CEA38ACA99641C7F5C8554A4D89F0 ();
// 0x00000108 System.Void GoogleMobileAds.Common.DummyClient::add_OnAdOpening(System.EventHandler`1<System.EventArgs>)
extern void DummyClient_add_OnAdOpening_m8E1196E3D8C44FAA11170DC62CF75F1DE7472247 ();
// 0x00000109 System.Void GoogleMobileAds.Common.DummyClient::remove_OnAdOpening(System.EventHandler`1<System.EventArgs>)
extern void DummyClient_remove_OnAdOpening_mF55CA617BB7C09DBDEDD5FD8A11A8DDC99411956 ();
// 0x0000010A System.Void GoogleMobileAds.Common.DummyClient::add_OnAdStarted(System.EventHandler`1<System.EventArgs>)
extern void DummyClient_add_OnAdStarted_mA25EB97B356325F1F640BCDB64229F4DC879B099 ();
// 0x0000010B System.Void GoogleMobileAds.Common.DummyClient::remove_OnAdStarted(System.EventHandler`1<System.EventArgs>)
extern void DummyClient_remove_OnAdStarted_mBBDD969259E6EBF1EA2D014F4D88E836FAD54ADF ();
// 0x0000010C System.Void GoogleMobileAds.Common.DummyClient::add_OnAdClosed(System.EventHandler`1<System.EventArgs>)
extern void DummyClient_add_OnAdClosed_m58DE279A243EB1ABB9C6F5E49BD07E17956EF6AA ();
// 0x0000010D System.Void GoogleMobileAds.Common.DummyClient::remove_OnAdClosed(System.EventHandler`1<System.EventArgs>)
extern void DummyClient_remove_OnAdClosed_m033065AC7E04DFA63FF92490E5E5525176266063 ();
// 0x0000010E System.Void GoogleMobileAds.Common.DummyClient::add_OnAdRewarded(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
extern void DummyClient_add_OnAdRewarded_m0422FCBCE46103B4563B2AF5A6005B1A8E734B32 ();
// 0x0000010F System.Void GoogleMobileAds.Common.DummyClient::remove_OnAdRewarded(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
extern void DummyClient_remove_OnAdRewarded_m4E66EBD33DAA35F69226AEFD34177C791CECA24C ();
// 0x00000110 System.Void GoogleMobileAds.Common.DummyClient::add_OnAdLeavingApplication(System.EventHandler`1<System.EventArgs>)
extern void DummyClient_add_OnAdLeavingApplication_m6ED1B118F6FBBB78B8AEF66707D90E077182E027 ();
// 0x00000111 System.Void GoogleMobileAds.Common.DummyClient::remove_OnAdLeavingApplication(System.EventHandler`1<System.EventArgs>)
extern void DummyClient_remove_OnAdLeavingApplication_m0FD38C022E4D71C364F9A3357851EF76AE117603 ();
// 0x00000112 System.Void GoogleMobileAds.Common.DummyClient::add_OnCustomNativeTemplateAdLoaded(System.EventHandler`1<GoogleMobileAds.Api.CustomNativeEventArgs>)
extern void DummyClient_add_OnCustomNativeTemplateAdLoaded_m5C8D42A0B833DF463D5B82485145BBA8E2D15E39 ();
// 0x00000113 System.Void GoogleMobileAds.Common.DummyClient::remove_OnCustomNativeTemplateAdLoaded(System.EventHandler`1<GoogleMobileAds.Api.CustomNativeEventArgs>)
extern void DummyClient_remove_OnCustomNativeTemplateAdLoaded_m498A9C68CFEE1A9F5867C75D9A0BBED1991C5A94 ();
// 0x00000114 System.String GoogleMobileAds.Common.DummyClient::get_UserId()
extern void DummyClient_get_UserId_mF3BDE1B3C01B7C50AB76099802CB24161BF2DFF0 ();
// 0x00000115 System.Void GoogleMobileAds.Common.DummyClient::set_UserId(System.String)
extern void DummyClient_set_UserId_m99C2A0AB8E5542E3B7F543884D02B931BBAD4E79 ();
// 0x00000116 System.Void GoogleMobileAds.Common.DummyClient::Initialize(System.String)
extern void DummyClient_Initialize_m9528C626842EA8090CECEEA6A504EBD51C1B96B7 ();
// 0x00000117 System.Void GoogleMobileAds.Common.DummyClient::CreateBannerView(System.String,GoogleMobileAds.Api.AdSize,GoogleMobileAds.Api.AdPosition)
extern void DummyClient_CreateBannerView_m1A13C26FF3DB3735060172E22836F874D207DED2 ();
// 0x00000118 System.Void GoogleMobileAds.Common.DummyClient::CreateBannerView(System.String,GoogleMobileAds.Api.AdSize,System.Int32,System.Int32)
extern void DummyClient_CreateBannerView_mB8B3A4424688E2215F951CC372C2167A6E47D617 ();
// 0x00000119 System.Void GoogleMobileAds.Common.DummyClient::LoadAd(GoogleMobileAds.Api.AdRequest)
extern void DummyClient_LoadAd_m73E9982188004A34600627D04F7C3F5CCA408C3B ();
// 0x0000011A System.Void GoogleMobileAds.Common.DummyClient::ShowBannerView()
extern void DummyClient_ShowBannerView_m31EB944535650EB7814A7AE0A45936AB2A2FE189 ();
// 0x0000011B System.Void GoogleMobileAds.Common.DummyClient::HideBannerView()
extern void DummyClient_HideBannerView_mBFA9F00484225F0546A6D2A3214E8D50AA700CAC ();
// 0x0000011C System.Void GoogleMobileAds.Common.DummyClient::DestroyBannerView()
extern void DummyClient_DestroyBannerView_mA529DF86AC96568706FC0F90A64968410E38982B ();
// 0x0000011D System.Void GoogleMobileAds.Common.DummyClient::CreateInterstitialAd(System.String)
extern void DummyClient_CreateInterstitialAd_m37434429DA45C2E50D2A26CABDCDC6AE8D7EE10C ();
// 0x0000011E System.Boolean GoogleMobileAds.Common.DummyClient::IsLoaded()
extern void DummyClient_IsLoaded_mDD188150144BD2251BF13C64FE7EEB55BB2DB467 ();
// 0x0000011F System.Void GoogleMobileAds.Common.DummyClient::ShowInterstitial()
extern void DummyClient_ShowInterstitial_m72E81B1F857B66DE35B9C4560671CF14F43E43DF ();
// 0x00000120 System.Void GoogleMobileAds.Common.DummyClient::DestroyInterstitial()
extern void DummyClient_DestroyInterstitial_m0EFB09868D915C4363A3F4F1E27E99EA735B9845 ();
// 0x00000121 System.Void GoogleMobileAds.Common.DummyClient::CreateRewardBasedVideoAd()
extern void DummyClient_CreateRewardBasedVideoAd_m15F851D1A3B5F1E2F78DBF1B67CAAF3F411B30B2 ();
// 0x00000122 System.Void GoogleMobileAds.Common.DummyClient::SetUserId(System.String)
extern void DummyClient_SetUserId_m351642C0069EAD0C5189D5C2D991CDE9B4154AD2 ();
// 0x00000123 System.Void GoogleMobileAds.Common.DummyClient::LoadAd(GoogleMobileAds.Api.AdRequest,System.String)
extern void DummyClient_LoadAd_mF5A5795104C0B908A8BF7B07FAAD0AD3B85A3865 ();
// 0x00000124 System.Void GoogleMobileAds.Common.DummyClient::DestroyRewardBasedVideoAd()
extern void DummyClient_DestroyRewardBasedVideoAd_m08D54178B18DAF95D8992ED12BFA3C533A0CF1A4 ();
// 0x00000125 System.Void GoogleMobileAds.Common.DummyClient::ShowRewardBasedVideoAd()
extern void DummyClient_ShowRewardBasedVideoAd_m8629C3A92ABE31E0A36E3D9E19EE7280D339C109 ();
// 0x00000126 System.Void GoogleMobileAds.Common.DummyClient::CreateAdLoader(GoogleMobileAds.Api.AdLoader_Builder)
extern void DummyClient_CreateAdLoader_mCF7A6337C06D69EF14797F6B5000E5FCF142F676 ();
// 0x00000127 System.Void GoogleMobileAds.Common.DummyClient::Load(GoogleMobileAds.Api.AdRequest)
extern void DummyClient_Load_m8D0BD1832281496A0E660ED6136E68F08327DAC3 ();
// 0x00000128 System.Void GoogleMobileAds.Common.DummyClient::CreateNativeExpressAdView(System.String,GoogleMobileAds.Api.AdSize,GoogleMobileAds.Api.AdPosition)
extern void DummyClient_CreateNativeExpressAdView_mA47540D6A688871096018776A5BB1A6115F0E160 ();
// 0x00000129 System.Void GoogleMobileAds.Common.DummyClient::CreateNativeExpressAdView(System.String,GoogleMobileAds.Api.AdSize,System.Int32,System.Int32)
extern void DummyClient_CreateNativeExpressAdView_mAF46B7E5D58CA3308803C3DEBCDEDA67906D2C48 ();
// 0x0000012A System.Void GoogleMobileAds.Common.DummyClient::SetAdSize(GoogleMobileAds.Api.AdSize)
extern void DummyClient_SetAdSize_mA3AB88C00C6B37065B4A3D69A3847655E053F81E ();
// 0x0000012B System.Void GoogleMobileAds.Common.DummyClient::ShowNativeExpressAdView()
extern void DummyClient_ShowNativeExpressAdView_mB508839B88FD24421F0C2CF21BE03845A8712CDF ();
// 0x0000012C System.Void GoogleMobileAds.Common.DummyClient::HideNativeExpressAdView()
extern void DummyClient_HideNativeExpressAdView_m46C5987BC38587F607D566692BF22410098C1E8C ();
// 0x0000012D System.Void GoogleMobileAds.Common.DummyClient::DestroyNativeExpressAdView()
extern void DummyClient_DestroyNativeExpressAdView_m9C24AE627D67B434997B3DF76A9804E973181DEA ();
// 0x0000012E System.Void GoogleMobileAds.Common.IAdLoaderClient::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
// 0x0000012F System.Void GoogleMobileAds.Common.IAdLoaderClient::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
// 0x00000130 System.Void GoogleMobileAds.Common.IAdLoaderClient::add_OnCustomNativeTemplateAdLoaded(System.EventHandler`1<GoogleMobileAds.Api.CustomNativeEventArgs>)
// 0x00000131 System.Void GoogleMobileAds.Common.IAdLoaderClient::remove_OnCustomNativeTemplateAdLoaded(System.EventHandler`1<GoogleMobileAds.Api.CustomNativeEventArgs>)
// 0x00000132 System.Void GoogleMobileAds.Common.IAdLoaderClient::LoadAd(GoogleMobileAds.Api.AdRequest)
// 0x00000133 System.Void GoogleMobileAds.Common.IBannerClient::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
// 0x00000134 System.Void GoogleMobileAds.Common.IBannerClient::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
// 0x00000135 System.Void GoogleMobileAds.Common.IBannerClient::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
// 0x00000136 System.Void GoogleMobileAds.Common.IBannerClient::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
// 0x00000137 System.Void GoogleMobileAds.Common.IBannerClient::add_OnAdOpening(System.EventHandler`1<System.EventArgs>)
// 0x00000138 System.Void GoogleMobileAds.Common.IBannerClient::remove_OnAdOpening(System.EventHandler`1<System.EventArgs>)
// 0x00000139 System.Void GoogleMobileAds.Common.IBannerClient::add_OnAdClosed(System.EventHandler`1<System.EventArgs>)
// 0x0000013A System.Void GoogleMobileAds.Common.IBannerClient::remove_OnAdClosed(System.EventHandler`1<System.EventArgs>)
// 0x0000013B System.Void GoogleMobileAds.Common.IBannerClient::add_OnAdLeavingApplication(System.EventHandler`1<System.EventArgs>)
// 0x0000013C System.Void GoogleMobileAds.Common.IBannerClient::remove_OnAdLeavingApplication(System.EventHandler`1<System.EventArgs>)
// 0x0000013D System.Void GoogleMobileAds.Common.IBannerClient::CreateBannerView(System.String,GoogleMobileAds.Api.AdSize,GoogleMobileAds.Api.AdPosition)
// 0x0000013E System.Void GoogleMobileAds.Common.IBannerClient::CreateBannerView(System.String,GoogleMobileAds.Api.AdSize,System.Int32,System.Int32)
// 0x0000013F System.Void GoogleMobileAds.Common.IBannerClient::LoadAd(GoogleMobileAds.Api.AdRequest)
// 0x00000140 System.Void GoogleMobileAds.Common.IBannerClient::ShowBannerView()
// 0x00000141 System.Void GoogleMobileAds.Common.IBannerClient::HideBannerView()
// 0x00000142 System.Void GoogleMobileAds.Common.IBannerClient::DestroyBannerView()
// 0x00000143 System.String GoogleMobileAds.Common.ICustomNativeTemplateClient::GetTemplateId()
// 0x00000144 System.Byte[] GoogleMobileAds.Common.ICustomNativeTemplateClient::GetImageByteArray(System.String)
// 0x00000145 System.Collections.Generic.List`1<System.String> GoogleMobileAds.Common.ICustomNativeTemplateClient::GetAvailableAssetNames()
// 0x00000146 System.String GoogleMobileAds.Common.ICustomNativeTemplateClient::GetText(System.String)
// 0x00000147 System.Void GoogleMobileAds.Common.ICustomNativeTemplateClient::PerformClick(System.String)
// 0x00000148 System.Void GoogleMobileAds.Common.ICustomNativeTemplateClient::RecordImpression()
// 0x00000149 System.Void GoogleMobileAds.Common.IInterstitialClient::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
// 0x0000014A System.Void GoogleMobileAds.Common.IInterstitialClient::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
// 0x0000014B System.Void GoogleMobileAds.Common.IInterstitialClient::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
// 0x0000014C System.Void GoogleMobileAds.Common.IInterstitialClient::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
// 0x0000014D System.Void GoogleMobileAds.Common.IInterstitialClient::add_OnAdOpening(System.EventHandler`1<System.EventArgs>)
// 0x0000014E System.Void GoogleMobileAds.Common.IInterstitialClient::remove_OnAdOpening(System.EventHandler`1<System.EventArgs>)
// 0x0000014F System.Void GoogleMobileAds.Common.IInterstitialClient::add_OnAdClosed(System.EventHandler`1<System.EventArgs>)
// 0x00000150 System.Void GoogleMobileAds.Common.IInterstitialClient::remove_OnAdClosed(System.EventHandler`1<System.EventArgs>)
// 0x00000151 System.Void GoogleMobileAds.Common.IInterstitialClient::add_OnAdLeavingApplication(System.EventHandler`1<System.EventArgs>)
// 0x00000152 System.Void GoogleMobileAds.Common.IInterstitialClient::remove_OnAdLeavingApplication(System.EventHandler`1<System.EventArgs>)
// 0x00000153 System.Void GoogleMobileAds.Common.IInterstitialClient::CreateInterstitialAd(System.String)
// 0x00000154 System.Void GoogleMobileAds.Common.IInterstitialClient::LoadAd(GoogleMobileAds.Api.AdRequest)
// 0x00000155 System.Boolean GoogleMobileAds.Common.IInterstitialClient::IsLoaded()
// 0x00000156 System.Void GoogleMobileAds.Common.IInterstitialClient::ShowInterstitial()
// 0x00000157 System.Void GoogleMobileAds.Common.IInterstitialClient::DestroyInterstitial()
// 0x00000158 System.Void GoogleMobileAds.Common.IMobileAdsClient::Initialize(System.String)
// 0x00000159 System.Void GoogleMobileAds.Common.INativeExpressAdClient::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
// 0x0000015A System.Void GoogleMobileAds.Common.INativeExpressAdClient::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
// 0x0000015B System.Void GoogleMobileAds.Common.INativeExpressAdClient::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
// 0x0000015C System.Void GoogleMobileAds.Common.INativeExpressAdClient::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
// 0x0000015D System.Void GoogleMobileAds.Common.INativeExpressAdClient::add_OnAdOpening(System.EventHandler`1<System.EventArgs>)
// 0x0000015E System.Void GoogleMobileAds.Common.INativeExpressAdClient::remove_OnAdOpening(System.EventHandler`1<System.EventArgs>)
// 0x0000015F System.Void GoogleMobileAds.Common.INativeExpressAdClient::add_OnAdClosed(System.EventHandler`1<System.EventArgs>)
// 0x00000160 System.Void GoogleMobileAds.Common.INativeExpressAdClient::remove_OnAdClosed(System.EventHandler`1<System.EventArgs>)
// 0x00000161 System.Void GoogleMobileAds.Common.INativeExpressAdClient::add_OnAdLeavingApplication(System.EventHandler`1<System.EventArgs>)
// 0x00000162 System.Void GoogleMobileAds.Common.INativeExpressAdClient::remove_OnAdLeavingApplication(System.EventHandler`1<System.EventArgs>)
// 0x00000163 System.Void GoogleMobileAds.Common.INativeExpressAdClient::CreateNativeExpressAdView(System.String,GoogleMobileAds.Api.AdSize,GoogleMobileAds.Api.AdPosition)
// 0x00000164 System.Void GoogleMobileAds.Common.INativeExpressAdClient::CreateNativeExpressAdView(System.String,GoogleMobileAds.Api.AdSize,System.Int32,System.Int32)
// 0x00000165 System.Void GoogleMobileAds.Common.INativeExpressAdClient::LoadAd(GoogleMobileAds.Api.AdRequest)
// 0x00000166 System.Void GoogleMobileAds.Common.INativeExpressAdClient::ShowNativeExpressAdView()
// 0x00000167 System.Void GoogleMobileAds.Common.INativeExpressAdClient::HideNativeExpressAdView()
// 0x00000168 System.Void GoogleMobileAds.Common.INativeExpressAdClient::DestroyNativeExpressAdView()
// 0x00000169 System.Void GoogleMobileAds.Common.IRewardBasedVideoAdClient::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
// 0x0000016A System.Void GoogleMobileAds.Common.IRewardBasedVideoAdClient::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
// 0x0000016B System.Void GoogleMobileAds.Common.IRewardBasedVideoAdClient::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
// 0x0000016C System.Void GoogleMobileAds.Common.IRewardBasedVideoAdClient::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
// 0x0000016D System.Void GoogleMobileAds.Common.IRewardBasedVideoAdClient::add_OnAdOpening(System.EventHandler`1<System.EventArgs>)
// 0x0000016E System.Void GoogleMobileAds.Common.IRewardBasedVideoAdClient::remove_OnAdOpening(System.EventHandler`1<System.EventArgs>)
// 0x0000016F System.Void GoogleMobileAds.Common.IRewardBasedVideoAdClient::add_OnAdStarted(System.EventHandler`1<System.EventArgs>)
// 0x00000170 System.Void GoogleMobileAds.Common.IRewardBasedVideoAdClient::remove_OnAdStarted(System.EventHandler`1<System.EventArgs>)
// 0x00000171 System.Void GoogleMobileAds.Common.IRewardBasedVideoAdClient::add_OnAdRewarded(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
// 0x00000172 System.Void GoogleMobileAds.Common.IRewardBasedVideoAdClient::remove_OnAdRewarded(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
// 0x00000173 System.Void GoogleMobileAds.Common.IRewardBasedVideoAdClient::add_OnAdClosed(System.EventHandler`1<System.EventArgs>)
// 0x00000174 System.Void GoogleMobileAds.Common.IRewardBasedVideoAdClient::remove_OnAdClosed(System.EventHandler`1<System.EventArgs>)
// 0x00000175 System.Void GoogleMobileAds.Common.IRewardBasedVideoAdClient::add_OnAdLeavingApplication(System.EventHandler`1<System.EventArgs>)
// 0x00000176 System.Void GoogleMobileAds.Common.IRewardBasedVideoAdClient::remove_OnAdLeavingApplication(System.EventHandler`1<System.EventArgs>)
// 0x00000177 System.Void GoogleMobileAds.Common.IRewardBasedVideoAdClient::CreateRewardBasedVideoAd()
// 0x00000178 System.Void GoogleMobileAds.Common.IRewardBasedVideoAdClient::LoadAd(GoogleMobileAds.Api.AdRequest,System.String)
// 0x00000179 System.Boolean GoogleMobileAds.Common.IRewardBasedVideoAdClient::IsLoaded()
// 0x0000017A System.Void GoogleMobileAds.Common.IRewardBasedVideoAdClient::ShowRewardBasedVideoAd()
// 0x0000017B UnityEngine.Texture2D GoogleMobileAds.Common.Utils::GetTexture2DFromByteArray(System.Byte[])
extern void Utils_GetTexture2DFromByteArray_m544761C10D3FFEF3E5458CB2C161BEC868CBB4B5 ();
// 0x0000017C System.Void GoogleMobileAds.Common.Utils::.ctor()
extern void Utils__ctor_m5851D6B26E085BB36515206CEE3B80D6008EF5A9 ();
// 0x0000017D System.String GoogleMobileAds.Api.AdFailedToLoadEventArgs::get_Message()
extern void AdFailedToLoadEventArgs_get_Message_mAFCBACF7888F3D1708B63F48AB40BE9669938730 ();
// 0x0000017E System.Void GoogleMobileAds.Api.AdFailedToLoadEventArgs::set_Message(System.String)
extern void AdFailedToLoadEventArgs_set_Message_m6A87076DC03E1DF0795054ED41640B0C25A65F12 ();
// 0x0000017F System.Void GoogleMobileAds.Api.AdFailedToLoadEventArgs::.ctor()
extern void AdFailedToLoadEventArgs__ctor_m49BF00B309FD740DFBE7AC4D54DEDF4A27B8ED30 ();
// 0x00000180 System.Void GoogleMobileAds.Api.AdLoader::.ctor(GoogleMobileAds.Api.AdLoader_Builder)
extern void AdLoader__ctor_mA392711408CE360F65895976D093A9F40436D6DE ();
// 0x00000181 System.Void GoogleMobileAds.Api.AdLoader::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern void AdLoader_add_OnAdFailedToLoad_m8EEFA8C0A16867E80D2B9DF795AA5222DA4ABFD4 ();
// 0x00000182 System.Void GoogleMobileAds.Api.AdLoader::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern void AdLoader_remove_OnAdFailedToLoad_m196F2E244AA1ED7D380B8D38EFDFD0FA6542E57B ();
// 0x00000183 System.Void GoogleMobileAds.Api.AdLoader::add_OnCustomNativeTemplateAdLoaded(System.EventHandler`1<GoogleMobileAds.Api.CustomNativeEventArgs>)
extern void AdLoader_add_OnCustomNativeTemplateAdLoaded_mDE6794CFA0776B5B360872D59DC1F28C15FFEF42 ();
// 0x00000184 System.Void GoogleMobileAds.Api.AdLoader::remove_OnCustomNativeTemplateAdLoaded(System.EventHandler`1<GoogleMobileAds.Api.CustomNativeEventArgs>)
extern void AdLoader_remove_OnCustomNativeTemplateAdLoaded_mA2E0DF28A39671263E21726F78B854951686F47B ();
// 0x00000185 System.Collections.Generic.Dictionary`2<System.String,System.Action`2<GoogleMobileAds.Api.CustomNativeTemplateAd,System.String>> GoogleMobileAds.Api.AdLoader::get_CustomNativeTemplateClickHandlers()
extern void AdLoader_get_CustomNativeTemplateClickHandlers_m255D9763F690C84AECE7BBAF3500AD6D938CB2AA ();
// 0x00000186 System.Void GoogleMobileAds.Api.AdLoader::set_CustomNativeTemplateClickHandlers(System.Collections.Generic.Dictionary`2<System.String,System.Action`2<GoogleMobileAds.Api.CustomNativeTemplateAd,System.String>>)
extern void AdLoader_set_CustomNativeTemplateClickHandlers_mEBFFF3B81D6F604EA3515C877B69AEDDAB72244D ();
// 0x00000187 System.String GoogleMobileAds.Api.AdLoader::get_AdUnitId()
extern void AdLoader_get_AdUnitId_m7886AD0BE8DEABE9CB4D5CD69E6F844B7D7D59C5 ();
// 0x00000188 System.Void GoogleMobileAds.Api.AdLoader::set_AdUnitId(System.String)
extern void AdLoader_set_AdUnitId_m5D7C35C83EDC7E8479B94A7E9860176957D84C69 ();
// 0x00000189 System.Collections.Generic.HashSet`1<GoogleMobileAds.Api.NativeAdType> GoogleMobileAds.Api.AdLoader::get_AdTypes()
extern void AdLoader_get_AdTypes_mC5B9F1C39B4778BEA6DC344E364694231EED88E9 ();
// 0x0000018A System.Void GoogleMobileAds.Api.AdLoader::set_AdTypes(System.Collections.Generic.HashSet`1<GoogleMobileAds.Api.NativeAdType>)
extern void AdLoader_set_AdTypes_mA6CA57A563B2C06ABAD62422F4E6B203110D14B8 ();
// 0x0000018B System.Collections.Generic.HashSet`1<System.String> GoogleMobileAds.Api.AdLoader::get_TemplateIds()
extern void AdLoader_get_TemplateIds_m459A5618E18C36FB388E3F8869E96CADC4A4742B ();
// 0x0000018C System.Void GoogleMobileAds.Api.AdLoader::set_TemplateIds(System.Collections.Generic.HashSet`1<System.String>)
extern void AdLoader_set_TemplateIds_m4AD70321838E87E465424966380BEB4B28E4BB31 ();
// 0x0000018D System.Void GoogleMobileAds.Api.AdLoader::LoadAd(GoogleMobileAds.Api.AdRequest)
extern void AdLoader_LoadAd_m0C2FAD27AC919A54E4D099FB2585E8D3054F076D ();
// 0x0000018E System.Void GoogleMobileAds.Api.AdLoader::<.ctor>b__1_0(System.Object,GoogleMobileAds.Api.CustomNativeEventArgs)
extern void AdLoader_U3C_ctorU3Eb__1_0_m5CEEF987CAFB19418DE1A337DCC1E119AA26EE10 ();
// 0x0000018F System.Void GoogleMobileAds.Api.AdLoader::<.ctor>b__1_1(System.Object,GoogleMobileAds.Api.AdFailedToLoadEventArgs)
extern void AdLoader_U3C_ctorU3Eb__1_1_mE1A34AA0C411010497A5FE593437A4D1FDC11CA0 ();
// 0x00000190 System.Void GoogleMobileAds.Api.AdRequest::.ctor(GoogleMobileAds.Api.AdRequest_Builder)
extern void AdRequest__ctor_m788BAC27DAD4C0BFB9146E3E805EA8B412FD368F ();
// 0x00000191 System.Collections.Generic.List`1<System.String> GoogleMobileAds.Api.AdRequest::get_TestDevices()
extern void AdRequest_get_TestDevices_m02EC537E8CEEC7E695A6055B8CB1E1889F164617 ();
// 0x00000192 System.Void GoogleMobileAds.Api.AdRequest::set_TestDevices(System.Collections.Generic.List`1<System.String>)
extern void AdRequest_set_TestDevices_mD3019595DA7952487AE75EBDB9FA5770FFED7B10 ();
// 0x00000193 System.Collections.Generic.HashSet`1<System.String> GoogleMobileAds.Api.AdRequest::get_Keywords()
extern void AdRequest_get_Keywords_mA1D66061D1F93CF256DDD5AAE938C629104F11AA ();
// 0x00000194 System.Void GoogleMobileAds.Api.AdRequest::set_Keywords(System.Collections.Generic.HashSet`1<System.String>)
extern void AdRequest_set_Keywords_m73D12849D19999742A9F1C976EEA2C78F83FDE6B ();
// 0x00000195 System.Nullable`1<System.DateTime> GoogleMobileAds.Api.AdRequest::get_Birthday()
extern void AdRequest_get_Birthday_mF6551ECBB0E8EB04D73536381A5685D0FF55715A ();
// 0x00000196 System.Void GoogleMobileAds.Api.AdRequest::set_Birthday(System.Nullable`1<System.DateTime>)
extern void AdRequest_set_Birthday_mCA669659854C4AD03734238B6BD38379AE46D5EC ();
// 0x00000197 System.Nullable`1<GoogleMobileAds.Api.Gender> GoogleMobileAds.Api.AdRequest::get_Gender()
extern void AdRequest_get_Gender_m40E1D2BE6A214044F95918E202D11E5551118889 ();
// 0x00000198 System.Void GoogleMobileAds.Api.AdRequest::set_Gender(System.Nullable`1<GoogleMobileAds.Api.Gender>)
extern void AdRequest_set_Gender_m69CEBFA698EE2C203B4E70F7E27F9F7B4D1452BD ();
// 0x00000199 System.Nullable`1<System.Boolean> GoogleMobileAds.Api.AdRequest::get_TagForChildDirectedTreatment()
extern void AdRequest_get_TagForChildDirectedTreatment_mAF138AD39FC86FA60D310B685A6F3F1CDE91ABAA ();
// 0x0000019A System.Void GoogleMobileAds.Api.AdRequest::set_TagForChildDirectedTreatment(System.Nullable`1<System.Boolean>)
extern void AdRequest_set_TagForChildDirectedTreatment_mDD924F9F9593352918DA4A9459AC6A3471D71059 ();
// 0x0000019B System.Collections.Generic.Dictionary`2<System.String,System.String> GoogleMobileAds.Api.AdRequest::get_Extras()
extern void AdRequest_get_Extras_m814DAAAC04DFD17FC1DD6446718BEF964934B83B ();
// 0x0000019C System.Void GoogleMobileAds.Api.AdRequest::set_Extras(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void AdRequest_set_Extras_m81633870EA00D9B67B551212A65B9132FB18B076 ();
// 0x0000019D System.Collections.Generic.List`1<GoogleMobileAds.Api.Mediation.MediationExtras> GoogleMobileAds.Api.AdRequest::get_MediationExtras()
extern void AdRequest_get_MediationExtras_mEEC772BDDD5CA17C993C45BD09F342E0CCFB1842 ();
// 0x0000019E System.Void GoogleMobileAds.Api.AdRequest::set_MediationExtras(System.Collections.Generic.List`1<GoogleMobileAds.Api.Mediation.MediationExtras>)
extern void AdRequest_set_MediationExtras_m6EC989AE0BAE1AC4B4A514ABEECD4010D6137513 ();
// 0x0000019F System.Void GoogleMobileAds.Api.AdSize::.ctor(System.Int32,System.Int32)
extern void AdSize__ctor_m0C50105C319FEDF8747333CD7D91D0B30FE42DB9 ();
// 0x000001A0 System.Void GoogleMobileAds.Api.AdSize::.ctor(System.Boolean)
extern void AdSize__ctor_m49B4BC95FE5B63C3A42B4600DB353B5F8DACCD4E ();
// 0x000001A1 System.Int32 GoogleMobileAds.Api.AdSize::get_Width()
extern void AdSize_get_Width_m601B66752396AC49504944139A4C2095DA56ECD4 ();
// 0x000001A2 System.Int32 GoogleMobileAds.Api.AdSize::get_Height()
extern void AdSize_get_Height_m5F2594D77109414FF4E8F43B6DB95ED482879556 ();
// 0x000001A3 System.Boolean GoogleMobileAds.Api.AdSize::get_IsSmartBanner()
extern void AdSize_get_IsSmartBanner_m7FEB13C8ECEFEDEE425011F3C288571827B58283 ();
// 0x000001A4 System.Void GoogleMobileAds.Api.AdSize::.cctor()
extern void AdSize__cctor_m4EE0706B1CFBCABD3372213B90F9459B356FDB46 ();
// 0x000001A5 System.Void GoogleMobileAds.Api.BannerView::.ctor(System.String,GoogleMobileAds.Api.AdSize,GoogleMobileAds.Api.AdPosition)
extern void BannerView__ctor_m6022987337C51378CFE91044D6C129DD58963B70 ();
// 0x000001A6 System.Void GoogleMobileAds.Api.BannerView::.ctor(System.String,GoogleMobileAds.Api.AdSize,System.Int32,System.Int32)
extern void BannerView__ctor_m6035654B85852D7058762F6AB7D401F7DDD7E066 ();
// 0x000001A7 System.Void GoogleMobileAds.Api.BannerView::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void BannerView_add_OnAdLoaded_m4CA4BAF7D14C88BC1125C8718914C10A8832ACA9 ();
// 0x000001A8 System.Void GoogleMobileAds.Api.BannerView::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void BannerView_remove_OnAdLoaded_m4AC2656F5F698E022CB672C13294BD186BEC9AB6 ();
// 0x000001A9 System.Void GoogleMobileAds.Api.BannerView::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern void BannerView_add_OnAdFailedToLoad_m4417B7CAE9503180DA1CF7C9C8FD69E2207843E2 ();
// 0x000001AA System.Void GoogleMobileAds.Api.BannerView::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern void BannerView_remove_OnAdFailedToLoad_m955DE3177E172610BC3DF0E7BF1E504CC4072419 ();
// 0x000001AB System.Void GoogleMobileAds.Api.BannerView::add_OnAdOpening(System.EventHandler`1<System.EventArgs>)
extern void BannerView_add_OnAdOpening_m8530663A5BE3B20DE2E21DE11774CBD2B56C95D5 ();
// 0x000001AC System.Void GoogleMobileAds.Api.BannerView::remove_OnAdOpening(System.EventHandler`1<System.EventArgs>)
extern void BannerView_remove_OnAdOpening_m63DD5D98EE6D79D34691832318B89CFA0ED5479C ();
// 0x000001AD System.Void GoogleMobileAds.Api.BannerView::add_OnAdClosed(System.EventHandler`1<System.EventArgs>)
extern void BannerView_add_OnAdClosed_m92D31DE8FFC3CA53A6E39338A024F905BEC4B873 ();
// 0x000001AE System.Void GoogleMobileAds.Api.BannerView::remove_OnAdClosed(System.EventHandler`1<System.EventArgs>)
extern void BannerView_remove_OnAdClosed_mE76548DB9A6FD3CA888706F3B7D8D222F0C581AB ();
// 0x000001AF System.Void GoogleMobileAds.Api.BannerView::add_OnAdLeavingApplication(System.EventHandler`1<System.EventArgs>)
extern void BannerView_add_OnAdLeavingApplication_mC64D39B3B87BD28427224BDA357FB2B7557689BD ();
// 0x000001B0 System.Void GoogleMobileAds.Api.BannerView::remove_OnAdLeavingApplication(System.EventHandler`1<System.EventArgs>)
extern void BannerView_remove_OnAdLeavingApplication_m79FB7B5D67AA516FE980CFEF2561EA69E2ABF977 ();
// 0x000001B1 System.Void GoogleMobileAds.Api.BannerView::LoadAd(GoogleMobileAds.Api.AdRequest)
extern void BannerView_LoadAd_mA22207B43FCBEC7A88469DAB0DB912611B568305 ();
// 0x000001B2 System.Void GoogleMobileAds.Api.BannerView::Hide()
extern void BannerView_Hide_m21981618F5392099BC82C735AA937E257718BF2A ();
// 0x000001B3 System.Void GoogleMobileAds.Api.BannerView::Show()
extern void BannerView_Show_m7CF03A1B9FCF671F1C0E9BC51744FA800FD138AE ();
// 0x000001B4 System.Void GoogleMobileAds.Api.BannerView::Destroy()
extern void BannerView_Destroy_m6AC3329E2403E577164C5F0895B416620279F2B8 ();
// 0x000001B5 System.Void GoogleMobileAds.Api.BannerView::ConfigureBannerEvents()
extern void BannerView_ConfigureBannerEvents_m934DAD8D1E0309FDE3759CF3EA48BB7BDAB0541F ();
// 0x000001B6 System.Void GoogleMobileAds.Api.BannerView::<ConfigureBannerEvents>b__22_0(System.Object,System.EventArgs)
extern void BannerView_U3CConfigureBannerEventsU3Eb__22_0_m581166F1337E8F127274F5961141716A6CB3E1AA ();
// 0x000001B7 System.Void GoogleMobileAds.Api.BannerView::<ConfigureBannerEvents>b__22_1(System.Object,GoogleMobileAds.Api.AdFailedToLoadEventArgs)
extern void BannerView_U3CConfigureBannerEventsU3Eb__22_1_m68D646088326FA102AB048B3E30553323A85F841 ();
// 0x000001B8 System.Void GoogleMobileAds.Api.BannerView::<ConfigureBannerEvents>b__22_2(System.Object,System.EventArgs)
extern void BannerView_U3CConfigureBannerEventsU3Eb__22_2_m90176468CC818A8371537C81A33C0396A7609EC3 ();
// 0x000001B9 System.Void GoogleMobileAds.Api.BannerView::<ConfigureBannerEvents>b__22_3(System.Object,System.EventArgs)
extern void BannerView_U3CConfigureBannerEventsU3Eb__22_3_mDF6BB5287A9A4EA548E86F5AD7E71098773F61B3 ();
// 0x000001BA System.Void GoogleMobileAds.Api.BannerView::<ConfigureBannerEvents>b__22_4(System.Object,System.EventArgs)
extern void BannerView_U3CConfigureBannerEventsU3Eb__22_4_mC7901B8841C9A115851057B52B164061A89BA2AB ();
// 0x000001BB GoogleMobileAds.Api.CustomNativeTemplateAd GoogleMobileAds.Api.CustomNativeEventArgs::get_nativeAd()
extern void CustomNativeEventArgs_get_nativeAd_m8CF64F30A9268D10E4D2A1A45697911F478FC073 ();
// 0x000001BC System.Void GoogleMobileAds.Api.CustomNativeEventArgs::set_nativeAd(GoogleMobileAds.Api.CustomNativeTemplateAd)
extern void CustomNativeEventArgs_set_nativeAd_m5864A2B45419FC4C8307D44B3497C0A1D7E2F72A ();
// 0x000001BD System.Void GoogleMobileAds.Api.CustomNativeEventArgs::.ctor()
extern void CustomNativeEventArgs__ctor_m8083C677AB9FB88407C2EA5459957681CA9871B1 ();
// 0x000001BE System.Void GoogleMobileAds.Api.CustomNativeTemplateAd::.ctor(GoogleMobileAds.Common.ICustomNativeTemplateClient)
extern void CustomNativeTemplateAd__ctor_m76DD1242D76D7AFAD82A20C4FA26C2F450BDF34D ();
// 0x000001BF System.Collections.Generic.List`1<System.String> GoogleMobileAds.Api.CustomNativeTemplateAd::GetAvailableAssetNames()
extern void CustomNativeTemplateAd_GetAvailableAssetNames_m5FB1A05B896817D3E17798A644F5BC4F2941D70B ();
// 0x000001C0 System.String GoogleMobileAds.Api.CustomNativeTemplateAd::GetCustomTemplateId()
extern void CustomNativeTemplateAd_GetCustomTemplateId_mAEEAAA84324A61D34A3ABE9654F86A0F72F51144 ();
// 0x000001C1 UnityEngine.Texture2D GoogleMobileAds.Api.CustomNativeTemplateAd::GetTexture2D(System.String)
extern void CustomNativeTemplateAd_GetTexture2D_m62A91B20DF41EC8AEF45E5037F894A3F45934E8D ();
// 0x000001C2 System.String GoogleMobileAds.Api.CustomNativeTemplateAd::GetText(System.String)
extern void CustomNativeTemplateAd_GetText_m7F7F8B124077FB6F238F6ECAAEA73B3ADA3ED54D ();
// 0x000001C3 System.Void GoogleMobileAds.Api.CustomNativeTemplateAd::PerformClick(System.String)
extern void CustomNativeTemplateAd_PerformClick_mAC10A57DF78C5613215A3A06F56B80422EC2CF50 ();
// 0x000001C4 System.Void GoogleMobileAds.Api.CustomNativeTemplateAd::RecordImpression()
extern void CustomNativeTemplateAd_RecordImpression_mC88C8406464F196A22921864E032D4F4FF637CDA ();
// 0x000001C5 System.Void GoogleMobileAds.Api.InterstitialAd::.ctor(System.String)
extern void InterstitialAd__ctor_mBC0DF92DB3A94D9FA1B86E52001EA456187FB8A3 ();
// 0x000001C6 System.Void GoogleMobileAds.Api.InterstitialAd::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void InterstitialAd_add_OnAdLoaded_mCD788235B83ACE0DCE2082128ADD6BAA7ACED301 ();
// 0x000001C7 System.Void GoogleMobileAds.Api.InterstitialAd::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void InterstitialAd_remove_OnAdLoaded_m96A5603033494B528D13A60194236A21CFD87272 ();
// 0x000001C8 System.Void GoogleMobileAds.Api.InterstitialAd::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern void InterstitialAd_add_OnAdFailedToLoad_mE7C99EDC105FB7C15B0CF941275DB526BFBB4069 ();
// 0x000001C9 System.Void GoogleMobileAds.Api.InterstitialAd::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern void InterstitialAd_remove_OnAdFailedToLoad_m5B024E743F730EE8BDCE26EF0C4262F670F27B34 ();
// 0x000001CA System.Void GoogleMobileAds.Api.InterstitialAd::add_OnAdOpening(System.EventHandler`1<System.EventArgs>)
extern void InterstitialAd_add_OnAdOpening_m2E4509B320D841BC472E92921E9FE2B97CC8364C ();
// 0x000001CB System.Void GoogleMobileAds.Api.InterstitialAd::remove_OnAdOpening(System.EventHandler`1<System.EventArgs>)
extern void InterstitialAd_remove_OnAdOpening_m5DB2079F937EA2A95A6BDCB5D18CC571AE10CDFB ();
// 0x000001CC System.Void GoogleMobileAds.Api.InterstitialAd::add_OnAdClosed(System.EventHandler`1<System.EventArgs>)
extern void InterstitialAd_add_OnAdClosed_m938576E7CA20056E5FD79107B5DE20F82DBA6E37 ();
// 0x000001CD System.Void GoogleMobileAds.Api.InterstitialAd::remove_OnAdClosed(System.EventHandler`1<System.EventArgs>)
extern void InterstitialAd_remove_OnAdClosed_m693F37A33B9B454732A577E5215D1478DDC037F8 ();
// 0x000001CE System.Void GoogleMobileAds.Api.InterstitialAd::add_OnAdLeavingApplication(System.EventHandler`1<System.EventArgs>)
extern void InterstitialAd_add_OnAdLeavingApplication_m35F6AF01336D70EE791806515E3982A898DCE907 ();
// 0x000001CF System.Void GoogleMobileAds.Api.InterstitialAd::remove_OnAdLeavingApplication(System.EventHandler`1<System.EventArgs>)
extern void InterstitialAd_remove_OnAdLeavingApplication_mC0779CD437B95EB6E47AEAF134599196C5EF8522 ();
// 0x000001D0 System.Void GoogleMobileAds.Api.InterstitialAd::LoadAd(GoogleMobileAds.Api.AdRequest)
extern void InterstitialAd_LoadAd_m90AD135183B5E93F63711FAAFF9A854FB54DEE41 ();
// 0x000001D1 System.Boolean GoogleMobileAds.Api.InterstitialAd::IsLoaded()
extern void InterstitialAd_IsLoaded_m68E4FE04DE8DBEBF0FC1F0D7C988AF69B4856013 ();
// 0x000001D2 System.Void GoogleMobileAds.Api.InterstitialAd::Show()
extern void InterstitialAd_Show_m926F39FE9EA37D113E5B2F3F8E04878AD3A490AB ();
// 0x000001D3 System.Void GoogleMobileAds.Api.InterstitialAd::Destroy()
extern void InterstitialAd_Destroy_mD9CF29B7289CC3D032A112D2973192E8A0E86607 ();
// 0x000001D4 System.Void GoogleMobileAds.Api.InterstitialAd::<.ctor>b__1_0(System.Object,System.EventArgs)
extern void InterstitialAd_U3C_ctorU3Eb__1_0_m11B142CD464FB526F742219198D4BC931540FD87 ();
// 0x000001D5 System.Void GoogleMobileAds.Api.InterstitialAd::<.ctor>b__1_1(System.Object,GoogleMobileAds.Api.AdFailedToLoadEventArgs)
extern void InterstitialAd_U3C_ctorU3Eb__1_1_m7B0D7D5A4125069DC1063F5E6E1191775321ABB5 ();
// 0x000001D6 System.Void GoogleMobileAds.Api.InterstitialAd::<.ctor>b__1_2(System.Object,System.EventArgs)
extern void InterstitialAd_U3C_ctorU3Eb__1_2_m2F3A2EA02AC054CE6AAF8DE78DB60F4831EA3B5C ();
// 0x000001D7 System.Void GoogleMobileAds.Api.InterstitialAd::<.ctor>b__1_3(System.Object,System.EventArgs)
extern void InterstitialAd_U3C_ctorU3Eb__1_3_m5EDCB395DC0C78742CF2BAAE027CF311B843A87D ();
// 0x000001D8 System.Void GoogleMobileAds.Api.InterstitialAd::<.ctor>b__1_4(System.Object,System.EventArgs)
extern void InterstitialAd_U3C_ctorU3Eb__1_4_m9B1C273D9A4FC9D47C901FE335DF6278600D0D14 ();
// 0x000001D9 System.Void GoogleMobileAds.Api.MobileAds::Initialize(System.String)
extern void MobileAds_Initialize_mBB7539DECEF49FAFBAFBE33C3FEB92B1D0FE9FE1 ();
// 0x000001DA GoogleMobileAds.Common.IMobileAdsClient GoogleMobileAds.Api.MobileAds::GetMobileAdsClient()
extern void MobileAds_GetMobileAdsClient_mB118D2C9A5A05E5FF249EE0C8BDCA5FA6BF42BEE ();
// 0x000001DB System.Void GoogleMobileAds.Api.MobileAds::.ctor()
extern void MobileAds__ctor_mCD2EA0B29AB1801A42F7C8282550A4D31EE8E6BD ();
// 0x000001DC System.Void GoogleMobileAds.Api.MobileAds::.cctor()
extern void MobileAds__cctor_mC87B40DF3EE70F421CBD6ED61FBC9E84B40AFD21 ();
// 0x000001DD System.Void GoogleMobileAds.Api.NativeExpressAdView::.ctor(System.String,GoogleMobileAds.Api.AdSize,GoogleMobileAds.Api.AdPosition)
extern void NativeExpressAdView__ctor_m94025E1FB7EAA65028004F82B6C40F430AB059C8 ();
// 0x000001DE System.Void GoogleMobileAds.Api.NativeExpressAdView::.ctor(System.String,GoogleMobileAds.Api.AdSize,System.Int32,System.Int32)
extern void NativeExpressAdView__ctor_m0D4A186508126CC6E75BC9977FF5013C58D7A992 ();
// 0x000001DF System.Void GoogleMobileAds.Api.NativeExpressAdView::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void NativeExpressAdView_add_OnAdLoaded_mF387677BB7039EF0D8C20B0AB1390E041884DE85 ();
// 0x000001E0 System.Void GoogleMobileAds.Api.NativeExpressAdView::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void NativeExpressAdView_remove_OnAdLoaded_mAAB8E951902ACB124CD4A9FDD8C7C675CA2D70AA ();
// 0x000001E1 System.Void GoogleMobileAds.Api.NativeExpressAdView::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern void NativeExpressAdView_add_OnAdFailedToLoad_m6F945474BF6F47F4E29344A3C1B2CDB1D0C50131 ();
// 0x000001E2 System.Void GoogleMobileAds.Api.NativeExpressAdView::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern void NativeExpressAdView_remove_OnAdFailedToLoad_m3C50A8C9C1681714DD9B72BA00183F0A997F92D9 ();
// 0x000001E3 System.Void GoogleMobileAds.Api.NativeExpressAdView::add_OnAdOpening(System.EventHandler`1<System.EventArgs>)
extern void NativeExpressAdView_add_OnAdOpening_mFC8BD2D75653E79526A22148CBED5152BFA02C0A ();
// 0x000001E4 System.Void GoogleMobileAds.Api.NativeExpressAdView::remove_OnAdOpening(System.EventHandler`1<System.EventArgs>)
extern void NativeExpressAdView_remove_OnAdOpening_mD4256F305CAD79ADECCC7A75834208C471187AAA ();
// 0x000001E5 System.Void GoogleMobileAds.Api.NativeExpressAdView::add_OnAdClosed(System.EventHandler`1<System.EventArgs>)
extern void NativeExpressAdView_add_OnAdClosed_m4858A136ACD99DDB8BB36F47D22EE12FA325F2DA ();
// 0x000001E6 System.Void GoogleMobileAds.Api.NativeExpressAdView::remove_OnAdClosed(System.EventHandler`1<System.EventArgs>)
extern void NativeExpressAdView_remove_OnAdClosed_m4722354EBC273C8A58127D7B0BECFBB3A49DCBDF ();
// 0x000001E7 System.Void GoogleMobileAds.Api.NativeExpressAdView::add_OnAdLeavingApplication(System.EventHandler`1<System.EventArgs>)
extern void NativeExpressAdView_add_OnAdLeavingApplication_m1C2EF3C4FCB814B5465133E3F39B2B3E76C88C85 ();
// 0x000001E8 System.Void GoogleMobileAds.Api.NativeExpressAdView::remove_OnAdLeavingApplication(System.EventHandler`1<System.EventArgs>)
extern void NativeExpressAdView_remove_OnAdLeavingApplication_m697117E6F0581B0A83DD7D1A6B05C4B425569F1D ();
// 0x000001E9 System.Void GoogleMobileAds.Api.NativeExpressAdView::LoadAd(GoogleMobileAds.Api.AdRequest)
extern void NativeExpressAdView_LoadAd_m2CDC846A00F0314A0F2B229BA87E6AEA6205F894 ();
// 0x000001EA System.Void GoogleMobileAds.Api.NativeExpressAdView::Hide()
extern void NativeExpressAdView_Hide_mE1C8F343A10CB63897D911F22C6EA5C945F9C743 ();
// 0x000001EB System.Void GoogleMobileAds.Api.NativeExpressAdView::Show()
extern void NativeExpressAdView_Show_m523AA7DC6B76E0B00141C0FBEEE2867A59407767 ();
// 0x000001EC System.Void GoogleMobileAds.Api.NativeExpressAdView::Destroy()
extern void NativeExpressAdView_Destroy_mBB65A037F95A658242368432CC1645A502194A90 ();
// 0x000001ED System.Void GoogleMobileAds.Api.NativeExpressAdView::ConfigureNativeExpressAdEvents()
extern void NativeExpressAdView_ConfigureNativeExpressAdEvents_mB31C3D4EAA639919869E79BBD6DB97A5D4E32350 ();
// 0x000001EE System.Void GoogleMobileAds.Api.NativeExpressAdView::<ConfigureNativeExpressAdEvents>b__22_0(System.Object,System.EventArgs)
extern void NativeExpressAdView_U3CConfigureNativeExpressAdEventsU3Eb__22_0_mC8B593FC672F0A3C5F94E09F51968774B5BBB18A ();
// 0x000001EF System.Void GoogleMobileAds.Api.NativeExpressAdView::<ConfigureNativeExpressAdEvents>b__22_1(System.Object,GoogleMobileAds.Api.AdFailedToLoadEventArgs)
extern void NativeExpressAdView_U3CConfigureNativeExpressAdEventsU3Eb__22_1_mA2359610C07C930A23845F711BAF6B996EB4C615 ();
// 0x000001F0 System.Void GoogleMobileAds.Api.NativeExpressAdView::<ConfigureNativeExpressAdEvents>b__22_2(System.Object,System.EventArgs)
extern void NativeExpressAdView_U3CConfigureNativeExpressAdEventsU3Eb__22_2_mE3D9AA2931356AFBF2C483DD44245AABA8FB15F8 ();
// 0x000001F1 System.Void GoogleMobileAds.Api.NativeExpressAdView::<ConfigureNativeExpressAdEvents>b__22_3(System.Object,System.EventArgs)
extern void NativeExpressAdView_U3CConfigureNativeExpressAdEventsU3Eb__22_3_mA79D9B0BDCB409288B09489D441963EECA22E934 ();
// 0x000001F2 System.Void GoogleMobileAds.Api.NativeExpressAdView::<ConfigureNativeExpressAdEvents>b__22_4(System.Object,System.EventArgs)
extern void NativeExpressAdView_U3CConfigureNativeExpressAdEventsU3Eb__22_4_m2B8B099EE3820D3EF77FAFD1C917DD5260DED5F0 ();
// 0x000001F3 System.String GoogleMobileAds.Api.Reward::get_Type()
extern void Reward_get_Type_m88484E4A637E2E67559B23D8D94CDECE6FC643FF ();
// 0x000001F4 System.Void GoogleMobileAds.Api.Reward::set_Type(System.String)
extern void Reward_set_Type_mDAD98536D1135210648EBC83AD6B0453F68F2C29 ();
// 0x000001F5 System.Double GoogleMobileAds.Api.Reward::get_Amount()
extern void Reward_get_Amount_mA620F592EC72DEC2FE7D4C62552A3C129D69C16E ();
// 0x000001F6 System.Void GoogleMobileAds.Api.Reward::set_Amount(System.Double)
extern void Reward_set_Amount_m19E1FF38C371472AA17BE0DC7FD9559DA2E3DF53 ();
// 0x000001F7 System.Void GoogleMobileAds.Api.Reward::.ctor()
extern void Reward__ctor_mB22E882CC0C8549E065DD745FD5DA2B065F248D4 ();
// 0x000001F8 GoogleMobileAds.Api.RewardBasedVideoAd GoogleMobileAds.Api.RewardBasedVideoAd::get_Instance()
extern void RewardBasedVideoAd_get_Instance_mD22F18358E8AAE1EF7D0EF41D89938E27F110413 ();
// 0x000001F9 System.Void GoogleMobileAds.Api.RewardBasedVideoAd::.ctor()
extern void RewardBasedVideoAd__ctor_mCCDA78B541C02A50B80EFA09004FDDF8EAB88C97 ();
// 0x000001FA System.Void GoogleMobileAds.Api.RewardBasedVideoAd::add_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void RewardBasedVideoAd_add_OnAdLoaded_m3B6259AAA17196BF6269153F9FA89168C68B14C6 ();
// 0x000001FB System.Void GoogleMobileAds.Api.RewardBasedVideoAd::remove_OnAdLoaded(System.EventHandler`1<System.EventArgs>)
extern void RewardBasedVideoAd_remove_OnAdLoaded_m581D661BC4261782EB3EAB17428FB806A55D3946 ();
// 0x000001FC System.Void GoogleMobileAds.Api.RewardBasedVideoAd::add_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern void RewardBasedVideoAd_add_OnAdFailedToLoad_mA6CBF40C4C4AB1B09ADD6B44BD01ED36192D4B63 ();
// 0x000001FD System.Void GoogleMobileAds.Api.RewardBasedVideoAd::remove_OnAdFailedToLoad(System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>)
extern void RewardBasedVideoAd_remove_OnAdFailedToLoad_m915FDD23DBE6E532F6E34348D50600BAF8C6748D ();
// 0x000001FE System.Void GoogleMobileAds.Api.RewardBasedVideoAd::add_OnAdOpening(System.EventHandler`1<System.EventArgs>)
extern void RewardBasedVideoAd_add_OnAdOpening_mC002CFD99C45593B31D9A34A9745E478C8116CF8 ();
// 0x000001FF System.Void GoogleMobileAds.Api.RewardBasedVideoAd::remove_OnAdOpening(System.EventHandler`1<System.EventArgs>)
extern void RewardBasedVideoAd_remove_OnAdOpening_m2E298551D5F9F94277504D485856605D757328A2 ();
// 0x00000200 System.Void GoogleMobileAds.Api.RewardBasedVideoAd::add_OnAdStarted(System.EventHandler`1<System.EventArgs>)
extern void RewardBasedVideoAd_add_OnAdStarted_mA41531B9FCFDEC881174D5BE3809D7E4CE82D2A3 ();
// 0x00000201 System.Void GoogleMobileAds.Api.RewardBasedVideoAd::remove_OnAdStarted(System.EventHandler`1<System.EventArgs>)
extern void RewardBasedVideoAd_remove_OnAdStarted_m5ED905EBA857DDDAA7BA00AE2CB57AF0740DC1BC ();
// 0x00000202 System.Void GoogleMobileAds.Api.RewardBasedVideoAd::add_OnAdClosed(System.EventHandler`1<System.EventArgs>)
extern void RewardBasedVideoAd_add_OnAdClosed_m1A6B6CB3EC80C77E52C54E55C7F57E72BDED15DA ();
// 0x00000203 System.Void GoogleMobileAds.Api.RewardBasedVideoAd::remove_OnAdClosed(System.EventHandler`1<System.EventArgs>)
extern void RewardBasedVideoAd_remove_OnAdClosed_mB994C35F507AC163E45460D6DFBB1ACA8B10949A ();
// 0x00000204 System.Void GoogleMobileAds.Api.RewardBasedVideoAd::add_OnAdRewarded(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
extern void RewardBasedVideoAd_add_OnAdRewarded_mA6F7313150EB1E9237EF673700535A0E5DB4E200 ();
// 0x00000205 System.Void GoogleMobileAds.Api.RewardBasedVideoAd::remove_OnAdRewarded(System.EventHandler`1<GoogleMobileAds.Api.Reward>)
extern void RewardBasedVideoAd_remove_OnAdRewarded_m838857B1B270FD00C7B0D629266479073A865569 ();
// 0x00000206 System.Void GoogleMobileAds.Api.RewardBasedVideoAd::add_OnAdLeavingApplication(System.EventHandler`1<System.EventArgs>)
extern void RewardBasedVideoAd_add_OnAdLeavingApplication_m5D0E04912268B472848453E2AF6A783E3A8CB5A3 ();
// 0x00000207 System.Void GoogleMobileAds.Api.RewardBasedVideoAd::remove_OnAdLeavingApplication(System.EventHandler`1<System.EventArgs>)
extern void RewardBasedVideoAd_remove_OnAdLeavingApplication_m6FA977A93908F7EF19BD0EFC375D1AF1116BA472 ();
// 0x00000208 System.Void GoogleMobileAds.Api.RewardBasedVideoAd::LoadAd(GoogleMobileAds.Api.AdRequest,System.String)
extern void RewardBasedVideoAd_LoadAd_mDC5DA9B2E11911512038CBE1455D1066834CB1CE ();
// 0x00000209 System.Boolean GoogleMobileAds.Api.RewardBasedVideoAd::IsLoaded()
extern void RewardBasedVideoAd_IsLoaded_m52AE856E045477F4340E5A04F3497E51C71E3744 ();
// 0x0000020A System.Void GoogleMobileAds.Api.RewardBasedVideoAd::Show()
extern void RewardBasedVideoAd_Show_m6690A11AF00C03DFD435BD1D95D66F9FB67178F1 ();
// 0x0000020B System.Void GoogleMobileAds.Api.RewardBasedVideoAd::.cctor()
extern void RewardBasedVideoAd__cctor_m6DB083621C071D6873F56EA6F85C28063EEA4966 ();
// 0x0000020C System.Void GoogleMobileAds.Api.RewardBasedVideoAd::<.ctor>b__4_0(System.Object,System.EventArgs)
extern void RewardBasedVideoAd_U3C_ctorU3Eb__4_0_m7163F7EF3B4A1D85A2C7ECF5602379476E4DB332 ();
// 0x0000020D System.Void GoogleMobileAds.Api.RewardBasedVideoAd::<.ctor>b__4_1(System.Object,GoogleMobileAds.Api.AdFailedToLoadEventArgs)
extern void RewardBasedVideoAd_U3C_ctorU3Eb__4_1_m51EBF4658B499F46C786E4E3BBCF4FF2F6A157FC ();
// 0x0000020E System.Void GoogleMobileAds.Api.RewardBasedVideoAd::<.ctor>b__4_2(System.Object,System.EventArgs)
extern void RewardBasedVideoAd_U3C_ctorU3Eb__4_2_mAD88537833F1FAA82377BFC44EF204B71D18AF36 ();
// 0x0000020F System.Void GoogleMobileAds.Api.RewardBasedVideoAd::<.ctor>b__4_3(System.Object,System.EventArgs)
extern void RewardBasedVideoAd_U3C_ctorU3Eb__4_3_m4EEC8D756041C0F52671DB502E343809B8684C58 ();
// 0x00000210 System.Void GoogleMobileAds.Api.RewardBasedVideoAd::<.ctor>b__4_4(System.Object,System.EventArgs)
extern void RewardBasedVideoAd_U3C_ctorU3Eb__4_4_m94FDC2E4A0EED9410AE8A0199419305A4886B18D ();
// 0x00000211 System.Void GoogleMobileAds.Api.RewardBasedVideoAd::<.ctor>b__4_5(System.Object,System.EventArgs)
extern void RewardBasedVideoAd_U3C_ctorU3Eb__4_5_m0A49EDB0A41556D5C450C7D8147C99C72A1B702E ();
// 0x00000212 System.Void GoogleMobileAds.Api.RewardBasedVideoAd::<.ctor>b__4_6(System.Object,GoogleMobileAds.Api.Reward)
extern void RewardBasedVideoAd_U3C_ctorU3Eb__4_6_m5B7D593155E690622FF351BEA30C615A1772CC38 ();
// 0x00000213 System.Collections.Generic.Dictionary`2<System.String,System.String> GoogleMobileAds.Api.Mediation.MediationExtras::get_Extras()
extern void MediationExtras_get_Extras_m845B48080AE808551366F9B6E363E0429EAE7869 ();
// 0x00000214 System.Void GoogleMobileAds.Api.Mediation.MediationExtras::set_Extras(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void MediationExtras_set_Extras_m8B938E04FD53E53543C27B52AE94A87B6764026A ();
// 0x00000215 System.String GoogleMobileAds.Api.Mediation.MediationExtras::get_AndroidMediationExtraBuilderClassName()
// 0x00000216 System.String GoogleMobileAds.Api.Mediation.MediationExtras::get_IOSMediationExtraBuilderClassName()
// 0x00000217 System.Void GoogleMobileAds.Api.Mediation.MediationExtras::.ctor()
extern void MediationExtras__ctor_m539737F193B83E77F64430AB34EA566B0D1DA538 ();
// 0x00000218 System.Void Game_<RestartGame>d__30::.ctor(System.Int32)
extern void U3CRestartGameU3Ed__30__ctor_m47F17CFE6FA5D24A23C3A8CA909EC52B03043E23 ();
// 0x00000219 System.Void Game_<RestartGame>d__30::System.IDisposable.Dispose()
extern void U3CRestartGameU3Ed__30_System_IDisposable_Dispose_m5F182F7F6510C94711F56E3140B0B93C2F31DAA7 ();
// 0x0000021A System.Boolean Game_<RestartGame>d__30::MoveNext()
extern void U3CRestartGameU3Ed__30_MoveNext_m05BCDAA5783A4216891DD7D3E7C5DCDCBB71EE86 ();
// 0x0000021B System.Object Game_<RestartGame>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRestartGameU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCA499D5F0D23E9227931A436B48A7F49D78EFA10 ();
// 0x0000021C System.Void Game_<RestartGame>d__30::System.Collections.IEnumerator.Reset()
extern void U3CRestartGameU3Ed__30_System_Collections_IEnumerator_Reset_m1830BDF1305DE8B31CDD81077D42FB4128EEE15B ();
// 0x0000021D System.Object Game_<RestartGame>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CRestartGameU3Ed__30_System_Collections_IEnumerator_get_Current_m1CA3493029F2A59FB70653BEB766338F6FC5BE2D ();
// 0x0000021E System.Void Game_<StartGame>d__38::.ctor(System.Int32)
extern void U3CStartGameU3Ed__38__ctor_m0D8BDE1483E9E0675FD749744BA99B755B12C40C ();
// 0x0000021F System.Void Game_<StartGame>d__38::System.IDisposable.Dispose()
extern void U3CStartGameU3Ed__38_System_IDisposable_Dispose_mA374F7607FF38FACB7223A2E5740F3D5FA99D1B5 ();
// 0x00000220 System.Boolean Game_<StartGame>d__38::MoveNext()
extern void U3CStartGameU3Ed__38_MoveNext_mBAD287F838C59DB2AA5535AAA5B0C983A2F77D98 ();
// 0x00000221 System.Object Game_<StartGame>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartGameU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEAED3530594DCE0BA782A7875138725EEDBBCE64 ();
// 0x00000222 System.Void Game_<StartGame>d__38::System.Collections.IEnumerator.Reset()
extern void U3CStartGameU3Ed__38_System_Collections_IEnumerator_Reset_mDFEA9255F458B12A9542288E515ADE3596725DD9 ();
// 0x00000223 System.Object Game_<StartGame>d__38::System.Collections.IEnumerator.get_Current()
extern void U3CStartGameU3Ed__38_System_Collections_IEnumerator_get_Current_m0DDBF8C4562A3721C64E01A6541262BDBD51F8CF ();
// 0x00000224 System.Void Game_<GetNewCard>d__42::.ctor(System.Int32)
extern void U3CGetNewCardU3Ed__42__ctor_m9B0F18FA2AE2448B06F3460DB78A642C6D78CBF9 ();
// 0x00000225 System.Void Game_<GetNewCard>d__42::System.IDisposable.Dispose()
extern void U3CGetNewCardU3Ed__42_System_IDisposable_Dispose_mB9A4DED753303C5B3870C9016016AF3722AFB8D6 ();
// 0x00000226 System.Boolean Game_<GetNewCard>d__42::MoveNext()
extern void U3CGetNewCardU3Ed__42_MoveNext_mC7D9081126D66CB79C75784BAC2C434661586D04 ();
// 0x00000227 System.Object Game_<GetNewCard>d__42::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetNewCardU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9E3D3044A41AFB19C919C7506BD876778469C332 ();
// 0x00000228 System.Void Game_<GetNewCard>d__42::System.Collections.IEnumerator.Reset()
extern void U3CGetNewCardU3Ed__42_System_Collections_IEnumerator_Reset_m66B41C6E90E9193A18ECE520B84CA56E381B8ED8 ();
// 0x00000229 System.Object Game_<GetNewCard>d__42::System.Collections.IEnumerator.get_Current()
extern void U3CGetNewCardU3Ed__42_System_Collections_IEnumerator_get_Current_m6C0BEDFC680DAFA53CB41A98BB6AAC8C5B174F39 ();
// 0x0000022A System.Void Purchaser_<>c::.cctor()
extern void U3CU3Ec__cctor_mBACD32049FC5669777FA1E6521D96C92CD685940 ();
// 0x0000022B System.Void Purchaser_<>c::.ctor()
extern void U3CU3Ec__ctor_mFFFF6E872083F954C08D0CB94E2B71E42C0B465D ();
// 0x0000022C System.Void Purchaser_<>c::<RestorePurchases>b__11_0(System.Boolean)
extern void U3CU3Ec_U3CRestorePurchasesU3Eb__11_0_m997253913D06FEB1A817C89E993EC7970359E20E ();
// 0x0000022D System.Void GoogleMobileAds.iOS.AdLoaderClient_GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback::.ctor(System.Object,System.IntPtr)
extern void GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback__ctor_m01865DCDE5629D6C88575A6C4C56164643A1D931 ();
// 0x0000022E System.Void GoogleMobileAds.iOS.AdLoaderClient_GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback::Invoke(System.IntPtr,System.IntPtr,System.String)
extern void GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_Invoke_mA0655E0A4C0FCC33E3E5E5217982B78B72F6D181 ();
// 0x0000022F System.IAsyncResult GoogleMobileAds.iOS.AdLoaderClient_GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.String,System.AsyncCallback,System.Object)
extern void GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_BeginInvoke_m9A6566A6AA46081016F9E2B3C09C3AFAA8AEAE49 ();
// 0x00000230 System.Void GoogleMobileAds.iOS.AdLoaderClient_GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback::EndInvoke(System.IAsyncResult)
extern void GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_EndInvoke_mB6E927089B02B8B068414DC276F25F826CAA7B8F ();
// 0x00000231 System.Void GoogleMobileAds.iOS.AdLoaderClient_GADUAdLoaderDidFailToReceiveAdWithErrorCallback::.ctor(System.Object,System.IntPtr)
extern void GADUAdLoaderDidFailToReceiveAdWithErrorCallback__ctor_m86FF5C27E612003B7B0027C47368283384626806 ();
// 0x00000232 System.Void GoogleMobileAds.iOS.AdLoaderClient_GADUAdLoaderDidFailToReceiveAdWithErrorCallback::Invoke(System.IntPtr,System.String)
extern void GADUAdLoaderDidFailToReceiveAdWithErrorCallback_Invoke_m7FB34DDFCEEAC8BAB2CE876C4EFD97B3F6CBDCDA ();
// 0x00000233 System.IAsyncResult GoogleMobileAds.iOS.AdLoaderClient_GADUAdLoaderDidFailToReceiveAdWithErrorCallback::BeginInvoke(System.IntPtr,System.String,System.AsyncCallback,System.Object)
extern void GADUAdLoaderDidFailToReceiveAdWithErrorCallback_BeginInvoke_mB31CE310EB3B11BFCBF0D365380B91173FC12C5E ();
// 0x00000234 System.Void GoogleMobileAds.iOS.AdLoaderClient_GADUAdLoaderDidFailToReceiveAdWithErrorCallback::EndInvoke(System.IAsyncResult)
extern void GADUAdLoaderDidFailToReceiveAdWithErrorCallback_EndInvoke_m8CFDE3B6D89A4875F729C7029B8FA6304267C1C9 ();
// 0x00000235 System.Void GoogleMobileAds.iOS.BannerClient_GADUAdViewDidReceiveAdCallback::.ctor(System.Object,System.IntPtr)
extern void GADUAdViewDidReceiveAdCallback__ctor_m0EE7ABAEA4086D3A9E370D5BEB84447C69D28840 ();
// 0x00000236 System.Void GoogleMobileAds.iOS.BannerClient_GADUAdViewDidReceiveAdCallback::Invoke(System.IntPtr)
extern void GADUAdViewDidReceiveAdCallback_Invoke_m212CBA85682D81108ABD290D15248A3FF19F8463 ();
// 0x00000237 System.IAsyncResult GoogleMobileAds.iOS.BannerClient_GADUAdViewDidReceiveAdCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void GADUAdViewDidReceiveAdCallback_BeginInvoke_m380642644B6A5129F85CCB611F527D8EF35347BB ();
// 0x00000238 System.Void GoogleMobileAds.iOS.BannerClient_GADUAdViewDidReceiveAdCallback::EndInvoke(System.IAsyncResult)
extern void GADUAdViewDidReceiveAdCallback_EndInvoke_m505F1F8382A0BD0955E7941687343163F249EC78 ();
// 0x00000239 System.Void GoogleMobileAds.iOS.BannerClient_GADUAdViewDidFailToReceiveAdWithErrorCallback::.ctor(System.Object,System.IntPtr)
extern void GADUAdViewDidFailToReceiveAdWithErrorCallback__ctor_mED774BF3A3D359F5CA462CC757A00841DA612BD2 ();
// 0x0000023A System.Void GoogleMobileAds.iOS.BannerClient_GADUAdViewDidFailToReceiveAdWithErrorCallback::Invoke(System.IntPtr,System.String)
extern void GADUAdViewDidFailToReceiveAdWithErrorCallback_Invoke_m8339C39A87BA824870FF102B4D3EC376A7BB9561 ();
// 0x0000023B System.IAsyncResult GoogleMobileAds.iOS.BannerClient_GADUAdViewDidFailToReceiveAdWithErrorCallback::BeginInvoke(System.IntPtr,System.String,System.AsyncCallback,System.Object)
extern void GADUAdViewDidFailToReceiveAdWithErrorCallback_BeginInvoke_m47F76A8B29F144ADF50E7A1269BF0B3A7A0C0008 ();
// 0x0000023C System.Void GoogleMobileAds.iOS.BannerClient_GADUAdViewDidFailToReceiveAdWithErrorCallback::EndInvoke(System.IAsyncResult)
extern void GADUAdViewDidFailToReceiveAdWithErrorCallback_EndInvoke_m019B4B32CFDCA278EB93BA6CA3E0F7E878E8733A ();
// 0x0000023D System.Void GoogleMobileAds.iOS.BannerClient_GADUAdViewWillPresentScreenCallback::.ctor(System.Object,System.IntPtr)
extern void GADUAdViewWillPresentScreenCallback__ctor_mB315B52B033E48AF41FCD4A19D234A5CF8F75EC2 ();
// 0x0000023E System.Void GoogleMobileAds.iOS.BannerClient_GADUAdViewWillPresentScreenCallback::Invoke(System.IntPtr)
extern void GADUAdViewWillPresentScreenCallback_Invoke_mE11C18A6EBD243187C26BE348D39F78357E3060C ();
// 0x0000023F System.IAsyncResult GoogleMobileAds.iOS.BannerClient_GADUAdViewWillPresentScreenCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void GADUAdViewWillPresentScreenCallback_BeginInvoke_m8BE2D393DC4460E8D9B9ABEF9BC7A57685639627 ();
// 0x00000240 System.Void GoogleMobileAds.iOS.BannerClient_GADUAdViewWillPresentScreenCallback::EndInvoke(System.IAsyncResult)
extern void GADUAdViewWillPresentScreenCallback_EndInvoke_m26C583C70345059439501E3E78F2F98C937C562A ();
// 0x00000241 System.Void GoogleMobileAds.iOS.BannerClient_GADUAdViewDidDismissScreenCallback::.ctor(System.Object,System.IntPtr)
extern void GADUAdViewDidDismissScreenCallback__ctor_m10C62AA35086747BDB71F5056C7A5BE67EFEED14 ();
// 0x00000242 System.Void GoogleMobileAds.iOS.BannerClient_GADUAdViewDidDismissScreenCallback::Invoke(System.IntPtr)
extern void GADUAdViewDidDismissScreenCallback_Invoke_mCB1FE63B69549B6467AB3CC3F452FA39DCEB4D67 ();
// 0x00000243 System.IAsyncResult GoogleMobileAds.iOS.BannerClient_GADUAdViewDidDismissScreenCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void GADUAdViewDidDismissScreenCallback_BeginInvoke_m5FB7A6168E9CA0BD08265A2A01043B008F85FF71 ();
// 0x00000244 System.Void GoogleMobileAds.iOS.BannerClient_GADUAdViewDidDismissScreenCallback::EndInvoke(System.IAsyncResult)
extern void GADUAdViewDidDismissScreenCallback_EndInvoke_m30C69C7276DD3040F46FCD43360F67D96B3778E3 ();
// 0x00000245 System.Void GoogleMobileAds.iOS.BannerClient_GADUAdViewWillLeaveApplicationCallback::.ctor(System.Object,System.IntPtr)
extern void GADUAdViewWillLeaveApplicationCallback__ctor_mDFEE54087FF2859342A5693E587CB7482D14FB40 ();
// 0x00000246 System.Void GoogleMobileAds.iOS.BannerClient_GADUAdViewWillLeaveApplicationCallback::Invoke(System.IntPtr)
extern void GADUAdViewWillLeaveApplicationCallback_Invoke_mF13E9AE50D0EC69E91DE5646F8FD3E9C01D26195 ();
// 0x00000247 System.IAsyncResult GoogleMobileAds.iOS.BannerClient_GADUAdViewWillLeaveApplicationCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void GADUAdViewWillLeaveApplicationCallback_BeginInvoke_mC1310358A4FA5F77E38DC0FB5DDBC470AF4C291E ();
// 0x00000248 System.Void GoogleMobileAds.iOS.BannerClient_GADUAdViewWillLeaveApplicationCallback::EndInvoke(System.IAsyncResult)
extern void GADUAdViewWillLeaveApplicationCallback_EndInvoke_m290FA391D668433D0AF69240C28927C7C320A4B9 ();
// 0x00000249 System.Void GoogleMobileAds.iOS.CustomNativeTemplateClient_GADUNativeCustomTemplateDidReceiveClick::.ctor(System.Object,System.IntPtr)
extern void GADUNativeCustomTemplateDidReceiveClick__ctor_m56EBC8773DABBE9EE718230BA35A3649280F35C0 ();
// 0x0000024A System.Void GoogleMobileAds.iOS.CustomNativeTemplateClient_GADUNativeCustomTemplateDidReceiveClick::Invoke(System.IntPtr,System.String)
extern void GADUNativeCustomTemplateDidReceiveClick_Invoke_m1BEFD4AE0D3B53D80EEF5D89B4647F9601974C05 ();
// 0x0000024B System.IAsyncResult GoogleMobileAds.iOS.CustomNativeTemplateClient_GADUNativeCustomTemplateDidReceiveClick::BeginInvoke(System.IntPtr,System.String,System.AsyncCallback,System.Object)
extern void GADUNativeCustomTemplateDidReceiveClick_BeginInvoke_m8BF32143D30147F22F9F070DCD7E20FDA5A1FB9A ();
// 0x0000024C System.Void GoogleMobileAds.iOS.CustomNativeTemplateClient_GADUNativeCustomTemplateDidReceiveClick::EndInvoke(System.IAsyncResult)
extern void GADUNativeCustomTemplateDidReceiveClick_EndInvoke_m7BC1E02EECCF69236579B8483A01BE45AB0B195C ();
// 0x0000024D System.Void GoogleMobileAds.iOS.InterstitialClient_GADUInterstitialDidReceiveAdCallback::.ctor(System.Object,System.IntPtr)
extern void GADUInterstitialDidReceiveAdCallback__ctor_m46748BE7477AC633AB8BB3CB7B8F7C1DAEB19663 ();
// 0x0000024E System.Void GoogleMobileAds.iOS.InterstitialClient_GADUInterstitialDidReceiveAdCallback::Invoke(System.IntPtr)
extern void GADUInterstitialDidReceiveAdCallback_Invoke_mCB1C1DD422AD2F3FE64DDA7275D58CDD6EB78093 ();
// 0x0000024F System.IAsyncResult GoogleMobileAds.iOS.InterstitialClient_GADUInterstitialDidReceiveAdCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void GADUInterstitialDidReceiveAdCallback_BeginInvoke_m2ABFF1841B4711C187AF499F6BCB1FDF803BAA03 ();
// 0x00000250 System.Void GoogleMobileAds.iOS.InterstitialClient_GADUInterstitialDidReceiveAdCallback::EndInvoke(System.IAsyncResult)
extern void GADUInterstitialDidReceiveAdCallback_EndInvoke_m59E7E9C62CB96CD36D5290EFF73541A3B4868E33 ();
// 0x00000251 System.Void GoogleMobileAds.iOS.InterstitialClient_GADUInterstitialDidFailToReceiveAdWithErrorCallback::.ctor(System.Object,System.IntPtr)
extern void GADUInterstitialDidFailToReceiveAdWithErrorCallback__ctor_m04C3C19130FC79B58231B1CC33AAE9DAAF6BE106 ();
// 0x00000252 System.Void GoogleMobileAds.iOS.InterstitialClient_GADUInterstitialDidFailToReceiveAdWithErrorCallback::Invoke(System.IntPtr,System.String)
extern void GADUInterstitialDidFailToReceiveAdWithErrorCallback_Invoke_m543D790AAFF4C3D3312B6605E7D28D807E536BBF ();
// 0x00000253 System.IAsyncResult GoogleMobileAds.iOS.InterstitialClient_GADUInterstitialDidFailToReceiveAdWithErrorCallback::BeginInvoke(System.IntPtr,System.String,System.AsyncCallback,System.Object)
extern void GADUInterstitialDidFailToReceiveAdWithErrorCallback_BeginInvoke_mD42FC423DA96E58C26B28A81BE121651EEBB1A72 ();
// 0x00000254 System.Void GoogleMobileAds.iOS.InterstitialClient_GADUInterstitialDidFailToReceiveAdWithErrorCallback::EndInvoke(System.IAsyncResult)
extern void GADUInterstitialDidFailToReceiveAdWithErrorCallback_EndInvoke_m09CFEAD53907A2D5902CA4BA212E8B38387C1E0E ();
// 0x00000255 System.Void GoogleMobileAds.iOS.InterstitialClient_GADUInterstitialWillPresentScreenCallback::.ctor(System.Object,System.IntPtr)
extern void GADUInterstitialWillPresentScreenCallback__ctor_mE0D16D80A71B8A0816B5BDFAB00BBF70450A7EC0 ();
// 0x00000256 System.Void GoogleMobileAds.iOS.InterstitialClient_GADUInterstitialWillPresentScreenCallback::Invoke(System.IntPtr)
extern void GADUInterstitialWillPresentScreenCallback_Invoke_mB845E3851869376F978E91A10737701005BCF3E2 ();
// 0x00000257 System.IAsyncResult GoogleMobileAds.iOS.InterstitialClient_GADUInterstitialWillPresentScreenCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void GADUInterstitialWillPresentScreenCallback_BeginInvoke_mF129D26DD9BF99785F634453E15E2563A7514D38 ();
// 0x00000258 System.Void GoogleMobileAds.iOS.InterstitialClient_GADUInterstitialWillPresentScreenCallback::EndInvoke(System.IAsyncResult)
extern void GADUInterstitialWillPresentScreenCallback_EndInvoke_m9E51AE156A7AFBCF3FFEEBDD92DD842E657C7F5C ();
// 0x00000259 System.Void GoogleMobileAds.iOS.InterstitialClient_GADUInterstitialDidDismissScreenCallback::.ctor(System.Object,System.IntPtr)
extern void GADUInterstitialDidDismissScreenCallback__ctor_mC8BB9E760E0970E3169E805F88B2BDA8A6C22DDF ();
// 0x0000025A System.Void GoogleMobileAds.iOS.InterstitialClient_GADUInterstitialDidDismissScreenCallback::Invoke(System.IntPtr)
extern void GADUInterstitialDidDismissScreenCallback_Invoke_m4A18067CB2FF2130FC7A4CF483CB9B2C0C1FB48A ();
// 0x0000025B System.IAsyncResult GoogleMobileAds.iOS.InterstitialClient_GADUInterstitialDidDismissScreenCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void GADUInterstitialDidDismissScreenCallback_BeginInvoke_m1B8C9194FA4A00B46E7DB735573B00B5AC742D7D ();
// 0x0000025C System.Void GoogleMobileAds.iOS.InterstitialClient_GADUInterstitialDidDismissScreenCallback::EndInvoke(System.IAsyncResult)
extern void GADUInterstitialDidDismissScreenCallback_EndInvoke_m2C0BA479E1297482F3D8A912B6BC6DD55BAEEA41 ();
// 0x0000025D System.Void GoogleMobileAds.iOS.InterstitialClient_GADUInterstitialWillLeaveApplicationCallback::.ctor(System.Object,System.IntPtr)
extern void GADUInterstitialWillLeaveApplicationCallback__ctor_mAF92C05FCA7D4AA439064C7F088B2A0A050DDF8B ();
// 0x0000025E System.Void GoogleMobileAds.iOS.InterstitialClient_GADUInterstitialWillLeaveApplicationCallback::Invoke(System.IntPtr)
extern void GADUInterstitialWillLeaveApplicationCallback_Invoke_mFD61B207AECCEF60F6B212B370274122667206D9 ();
// 0x0000025F System.IAsyncResult GoogleMobileAds.iOS.InterstitialClient_GADUInterstitialWillLeaveApplicationCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void GADUInterstitialWillLeaveApplicationCallback_BeginInvoke_m7FA8F610B77A46A667B1A3B54267843699D299DC ();
// 0x00000260 System.Void GoogleMobileAds.iOS.InterstitialClient_GADUInterstitialWillLeaveApplicationCallback::EndInvoke(System.IAsyncResult)
extern void GADUInterstitialWillLeaveApplicationCallback_EndInvoke_m0EB02A834D96098756FD881E685A08FC46213ED6 ();
// 0x00000261 System.Void GoogleMobileAds.iOS.NativeExpressAdClient_GADUNativeExpressAdViewDidReceiveAdCallback::.ctor(System.Object,System.IntPtr)
extern void GADUNativeExpressAdViewDidReceiveAdCallback__ctor_m3F0BF39499CE1CC16A49A4170DFDB7B8E752EB53 ();
// 0x00000262 System.Void GoogleMobileAds.iOS.NativeExpressAdClient_GADUNativeExpressAdViewDidReceiveAdCallback::Invoke(System.IntPtr)
extern void GADUNativeExpressAdViewDidReceiveAdCallback_Invoke_mD212D551CDFFE824AC9526289390078F8CAF5826 ();
// 0x00000263 System.IAsyncResult GoogleMobileAds.iOS.NativeExpressAdClient_GADUNativeExpressAdViewDidReceiveAdCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void GADUNativeExpressAdViewDidReceiveAdCallback_BeginInvoke_m407584C3E16C4D5D99FBD47E5312EEB1AED0D4AA ();
// 0x00000264 System.Void GoogleMobileAds.iOS.NativeExpressAdClient_GADUNativeExpressAdViewDidReceiveAdCallback::EndInvoke(System.IAsyncResult)
extern void GADUNativeExpressAdViewDidReceiveAdCallback_EndInvoke_m9230BCEE95802639DBF7A36C6362FF2EA2416484 ();
// 0x00000265 System.Void GoogleMobileAds.iOS.NativeExpressAdClient_GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback::.ctor(System.Object,System.IntPtr)
extern void GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback__ctor_m60B2E21BA729E06B6312738B3D1B078F0930E98E ();
// 0x00000266 System.Void GoogleMobileAds.iOS.NativeExpressAdClient_GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback::Invoke(System.IntPtr,System.String)
extern void GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback_Invoke_mCECA7919ED88DABD5418F57582700B30F50D9077 ();
// 0x00000267 System.IAsyncResult GoogleMobileAds.iOS.NativeExpressAdClient_GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback::BeginInvoke(System.IntPtr,System.String,System.AsyncCallback,System.Object)
extern void GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback_BeginInvoke_mB8CA4C992E7D784FB359C8FC46E923EC4AC7B243 ();
// 0x00000268 System.Void GoogleMobileAds.iOS.NativeExpressAdClient_GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback::EndInvoke(System.IAsyncResult)
extern void GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback_EndInvoke_m5DECD9E83F9543200B6F882BFD70DC4ED729C25D ();
// 0x00000269 System.Void GoogleMobileAds.iOS.NativeExpressAdClient_GADUNativeExpressAdViewWillPresentScreenCallback::.ctor(System.Object,System.IntPtr)
extern void GADUNativeExpressAdViewWillPresentScreenCallback__ctor_mFFC950549B9D671950DE48F9752EB3FE7A3F9143 ();
// 0x0000026A System.Void GoogleMobileAds.iOS.NativeExpressAdClient_GADUNativeExpressAdViewWillPresentScreenCallback::Invoke(System.IntPtr)
extern void GADUNativeExpressAdViewWillPresentScreenCallback_Invoke_m2B79E3424B20F59922B91D38C06935F9DF3FF5AD ();
// 0x0000026B System.IAsyncResult GoogleMobileAds.iOS.NativeExpressAdClient_GADUNativeExpressAdViewWillPresentScreenCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void GADUNativeExpressAdViewWillPresentScreenCallback_BeginInvoke_mFED86644462FB13B70CDAF54ED5595DD32E410C2 ();
// 0x0000026C System.Void GoogleMobileAds.iOS.NativeExpressAdClient_GADUNativeExpressAdViewWillPresentScreenCallback::EndInvoke(System.IAsyncResult)
extern void GADUNativeExpressAdViewWillPresentScreenCallback_EndInvoke_mC503DA89AF7D63F6995151862F8EAB2A66E30FD3 ();
// 0x0000026D System.Void GoogleMobileAds.iOS.NativeExpressAdClient_GADUNativeExpressAdViewDidDismissScreenCallback::.ctor(System.Object,System.IntPtr)
extern void GADUNativeExpressAdViewDidDismissScreenCallback__ctor_m017F2517AEC939DBDD723E364E1CB8C51CE2909A ();
// 0x0000026E System.Void GoogleMobileAds.iOS.NativeExpressAdClient_GADUNativeExpressAdViewDidDismissScreenCallback::Invoke(System.IntPtr)
extern void GADUNativeExpressAdViewDidDismissScreenCallback_Invoke_m540C7864A978F11A29E544B4D6B415B9E02471DE ();
// 0x0000026F System.IAsyncResult GoogleMobileAds.iOS.NativeExpressAdClient_GADUNativeExpressAdViewDidDismissScreenCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void GADUNativeExpressAdViewDidDismissScreenCallback_BeginInvoke_m5DAC5BAA324287DDCF4CDB7658F1AFED345833FC ();
// 0x00000270 System.Void GoogleMobileAds.iOS.NativeExpressAdClient_GADUNativeExpressAdViewDidDismissScreenCallback::EndInvoke(System.IAsyncResult)
extern void GADUNativeExpressAdViewDidDismissScreenCallback_EndInvoke_m2FCB617DB87882C57FEDC90137CEAF6AC2CAB9A1 ();
// 0x00000271 System.Void GoogleMobileAds.iOS.NativeExpressAdClient_GADUNativeExpressAdViewWillLeaveApplicationCallback::.ctor(System.Object,System.IntPtr)
extern void GADUNativeExpressAdViewWillLeaveApplicationCallback__ctor_m65DD5BF6C7F475164ED1CF2D3E5E1C8F066461ED ();
// 0x00000272 System.Void GoogleMobileAds.iOS.NativeExpressAdClient_GADUNativeExpressAdViewWillLeaveApplicationCallback::Invoke(System.IntPtr)
extern void GADUNativeExpressAdViewWillLeaveApplicationCallback_Invoke_mACA28AC736D7F383C119F242EF4B25DF934707CA ();
// 0x00000273 System.IAsyncResult GoogleMobileAds.iOS.NativeExpressAdClient_GADUNativeExpressAdViewWillLeaveApplicationCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void GADUNativeExpressAdViewWillLeaveApplicationCallback_BeginInvoke_m5AE8DC3BEB87F02CA5DDCB4EF276B5913EBA14E8 ();
// 0x00000274 System.Void GoogleMobileAds.iOS.NativeExpressAdClient_GADUNativeExpressAdViewWillLeaveApplicationCallback::EndInvoke(System.IAsyncResult)
extern void GADUNativeExpressAdViewWillLeaveApplicationCallback_EndInvoke_m494F8C744B2E5D189CF94A8CAD77C9FE42EB0037 ();
// 0x00000275 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidReceiveAdCallback::.ctor(System.Object,System.IntPtr)
extern void GADURewardBasedVideoAdDidReceiveAdCallback__ctor_m6BFA47F70744CEAFCF3723C1844138908979D754 ();
// 0x00000276 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidReceiveAdCallback::Invoke(System.IntPtr)
extern void GADURewardBasedVideoAdDidReceiveAdCallback_Invoke_m130AB06CC46B715A709BB98EAE031F0D200E924A ();
// 0x00000277 System.IAsyncResult GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidReceiveAdCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void GADURewardBasedVideoAdDidReceiveAdCallback_BeginInvoke_m6FD473EDD7855DCDD2A5BE6A3DB55A82026355C8 ();
// 0x00000278 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidReceiveAdCallback::EndInvoke(System.IAsyncResult)
extern void GADURewardBasedVideoAdDidReceiveAdCallback_EndInvoke_mE55D1CBBA8BEC8FC555305EF17F19C579733E168 ();
// 0x00000279 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback::.ctor(System.Object,System.IntPtr)
extern void GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback__ctor_m99DB2B38896C37DAC1AF5D8373B44400B717EBF5 ();
// 0x0000027A System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback::Invoke(System.IntPtr,System.String)
extern void GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_Invoke_mC418796EFB99655C21941C19867A04E7A73E0A69 ();
// 0x0000027B System.IAsyncResult GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback::BeginInvoke(System.IntPtr,System.String,System.AsyncCallback,System.Object)
extern void GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_BeginInvoke_mC6F75C40CF2453D5769559E02626C43CA4956F79 ();
// 0x0000027C System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback::EndInvoke(System.IAsyncResult)
extern void GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_EndInvoke_mBD01AB0139E82724D959317CCC017F45C2404C45 ();
// 0x0000027D System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidOpenCallback::.ctor(System.Object,System.IntPtr)
extern void GADURewardBasedVideoAdDidOpenCallback__ctor_m70452EE1AF2DD79704BC6EADB5A3C50D902D1B31 ();
// 0x0000027E System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidOpenCallback::Invoke(System.IntPtr)
extern void GADURewardBasedVideoAdDidOpenCallback_Invoke_mE6863B03981866D09D5E5D090A1399F1F1413BF2 ();
// 0x0000027F System.IAsyncResult GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidOpenCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void GADURewardBasedVideoAdDidOpenCallback_BeginInvoke_mD728B72F171C4846733C3D2E0490675A10265E10 ();
// 0x00000280 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidOpenCallback::EndInvoke(System.IAsyncResult)
extern void GADURewardBasedVideoAdDidOpenCallback_EndInvoke_mF614F861815F5C9DFB2E1204241C8C4EBFD3729D ();
// 0x00000281 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidStartCallback::.ctor(System.Object,System.IntPtr)
extern void GADURewardBasedVideoAdDidStartCallback__ctor_mFC4FF294BCD84D28985FC5DBCE5FA1F3A16F0518 ();
// 0x00000282 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidStartCallback::Invoke(System.IntPtr)
extern void GADURewardBasedVideoAdDidStartCallback_Invoke_m720D6286CD962856EE355C3D8147A0AFB57D8041 ();
// 0x00000283 System.IAsyncResult GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidStartCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void GADURewardBasedVideoAdDidStartCallback_BeginInvoke_m8FB416F8EC8EBF4FB4580CAE67A2D93845587F30 ();
// 0x00000284 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidStartCallback::EndInvoke(System.IAsyncResult)
extern void GADURewardBasedVideoAdDidStartCallback_EndInvoke_m2940E0F4A0F252E0A8D2BD073CFAD19510B9C4D7 ();
// 0x00000285 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidCloseCallback::.ctor(System.Object,System.IntPtr)
extern void GADURewardBasedVideoAdDidCloseCallback__ctor_mC38F7673ADC9F2131DCC5836932E2089DCAC65F2 ();
// 0x00000286 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidCloseCallback::Invoke(System.IntPtr)
extern void GADURewardBasedVideoAdDidCloseCallback_Invoke_m9C17098C750904FFCEB4D0177A7ADB01A998B1B9 ();
// 0x00000287 System.IAsyncResult GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidCloseCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void GADURewardBasedVideoAdDidCloseCallback_BeginInvoke_m72E64232FA64777E23ED798F84D1D3E49D7B3DD5 ();
// 0x00000288 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidCloseCallback::EndInvoke(System.IAsyncResult)
extern void GADURewardBasedVideoAdDidCloseCallback_EndInvoke_m3462576E3E6DD2264AE10F2C8297D0829D7C72D6 ();
// 0x00000289 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidRewardCallback::.ctor(System.Object,System.IntPtr)
extern void GADURewardBasedVideoAdDidRewardCallback__ctor_mC1CD3B54C468E63C7DEAE8B2E773BEACBF8A8B50 ();
// 0x0000028A System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidRewardCallback::Invoke(System.IntPtr,System.String,System.Double)
extern void GADURewardBasedVideoAdDidRewardCallback_Invoke_m63D57299C3435D8639407C85943D1E3CC8F7BAC6 ();
// 0x0000028B System.IAsyncResult GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidRewardCallback::BeginInvoke(System.IntPtr,System.String,System.Double,System.AsyncCallback,System.Object)
extern void GADURewardBasedVideoAdDidRewardCallback_BeginInvoke_m846C276D1F7E4A27EA129BB3C0BAF9A0D6A5AD6D ();
// 0x0000028C System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdDidRewardCallback::EndInvoke(System.IAsyncResult)
extern void GADURewardBasedVideoAdDidRewardCallback_EndInvoke_m212F8DF54757231A230F22B585BE24B22B39DC7B ();
// 0x0000028D System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdWillLeaveApplicationCallback::.ctor(System.Object,System.IntPtr)
extern void GADURewardBasedVideoAdWillLeaveApplicationCallback__ctor_m8BD895E3953FF6819459E215D6C87C6CEF0642F1 ();
// 0x0000028E System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdWillLeaveApplicationCallback::Invoke(System.IntPtr)
extern void GADURewardBasedVideoAdWillLeaveApplicationCallback_Invoke_mEEB31E100B363252E5946E753A1526DBE27D3115 ();
// 0x0000028F System.IAsyncResult GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdWillLeaveApplicationCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void GADURewardBasedVideoAdWillLeaveApplicationCallback_BeginInvoke_mED9BCF0C91B338AD4B9A6E39629994AFEAAE6F32 ();
// 0x00000290 System.Void GoogleMobileAds.iOS.RewardBasedVideoAdClient_GADURewardBasedVideoAdWillLeaveApplicationCallback::EndInvoke(System.IAsyncResult)
extern void GADURewardBasedVideoAdWillLeaveApplicationCallback_EndInvoke_m3022871D902B75E579C671E2161BDD27035154B5 ();
// 0x00000291 System.Void GoogleMobileAds.Api.AdLoader_Builder::.ctor(System.String)
extern void Builder__ctor_m383E9C8CAA60EC9F75835BBF4CA3F57D7630A019 ();
// 0x00000292 System.String GoogleMobileAds.Api.AdLoader_Builder::get_AdUnitId()
extern void Builder_get_AdUnitId_mDE6780A96F897E02302302823C3B8DCF5253580D ();
// 0x00000293 System.Void GoogleMobileAds.Api.AdLoader_Builder::set_AdUnitId(System.String)
extern void Builder_set_AdUnitId_mB2E68F81D6350AA1407A38DEE85E1CAECF7822C7 ();
// 0x00000294 System.Collections.Generic.HashSet`1<GoogleMobileAds.Api.NativeAdType> GoogleMobileAds.Api.AdLoader_Builder::get_AdTypes()
extern void Builder_get_AdTypes_mACA59D470BEA53A8211EE0337235F8FA2B37AD71 ();
// 0x00000295 System.Void GoogleMobileAds.Api.AdLoader_Builder::set_AdTypes(System.Collections.Generic.HashSet`1<GoogleMobileAds.Api.NativeAdType>)
extern void Builder_set_AdTypes_mC57AD00A722A31B417D8179427898A618C4CBEBE ();
// 0x00000296 System.Collections.Generic.HashSet`1<System.String> GoogleMobileAds.Api.AdLoader_Builder::get_TemplateIds()
extern void Builder_get_TemplateIds_mA06AF8062791298F2F3EB6BB3438D0E6C6DA6985 ();
// 0x00000297 System.Void GoogleMobileAds.Api.AdLoader_Builder::set_TemplateIds(System.Collections.Generic.HashSet`1<System.String>)
extern void Builder_set_TemplateIds_m49D18BAC46DB1908B32BC81A8DB08BC5354D606E ();
// 0x00000298 System.Collections.Generic.Dictionary`2<System.String,System.Action`2<GoogleMobileAds.Api.CustomNativeTemplateAd,System.String>> GoogleMobileAds.Api.AdLoader_Builder::get_CustomNativeTemplateClickHandlers()
extern void Builder_get_CustomNativeTemplateClickHandlers_mAC253BD630C3800E6DAD06BBE8C455011619CCFD ();
// 0x00000299 System.Void GoogleMobileAds.Api.AdLoader_Builder::set_CustomNativeTemplateClickHandlers(System.Collections.Generic.Dictionary`2<System.String,System.Action`2<GoogleMobileAds.Api.CustomNativeTemplateAd,System.String>>)
extern void Builder_set_CustomNativeTemplateClickHandlers_m95A81DF276EC0CF1B89021970868E447A670C22C ();
// 0x0000029A GoogleMobileAds.Api.AdLoader_Builder GoogleMobileAds.Api.AdLoader_Builder::ForCustomNativeAd(System.String)
extern void Builder_ForCustomNativeAd_m3CF5FBF44E85C1A4910FA6EC989EC11EA1CBCFB8 ();
// 0x0000029B GoogleMobileAds.Api.AdLoader_Builder GoogleMobileAds.Api.AdLoader_Builder::ForCustomNativeAd(System.String,System.Action`2<GoogleMobileAds.Api.CustomNativeTemplateAd,System.String>)
extern void Builder_ForCustomNativeAd_m81FE15EBFB6CDB2EF57C131C070AFCE83049D448 ();
// 0x0000029C GoogleMobileAds.Api.AdLoader GoogleMobileAds.Api.AdLoader_Builder::Build()
extern void Builder_Build_m53BF7F1231B6D473EB0C8FEF9FDDC98257B9B50C ();
// 0x0000029D System.Void GoogleMobileAds.Api.AdRequest_Builder::.ctor()
extern void Builder__ctor_m0E264AB53F2163A32997830032FAFE5B49BD0897 ();
// 0x0000029E System.Collections.Generic.List`1<System.String> GoogleMobileAds.Api.AdRequest_Builder::get_TestDevices()
extern void Builder_get_TestDevices_m798C6F87230C7CED80DE52F8A43E133CC781CB6F ();
// 0x0000029F System.Void GoogleMobileAds.Api.AdRequest_Builder::set_TestDevices(System.Collections.Generic.List`1<System.String>)
extern void Builder_set_TestDevices_m3A7C5F1BB1C4BFC6EABE2BD684288995F20F32ED ();
// 0x000002A0 System.Collections.Generic.HashSet`1<System.String> GoogleMobileAds.Api.AdRequest_Builder::get_Keywords()
extern void Builder_get_Keywords_m078AC1113E0A1F738AF609E1A601AD0937C2E7DA ();
// 0x000002A1 System.Void GoogleMobileAds.Api.AdRequest_Builder::set_Keywords(System.Collections.Generic.HashSet`1<System.String>)
extern void Builder_set_Keywords_mFF9014498E526DEB566B1A433CE05606567C2F0D ();
// 0x000002A2 System.Nullable`1<System.DateTime> GoogleMobileAds.Api.AdRequest_Builder::get_Birthday()
extern void Builder_get_Birthday_mBAF2FCD92DC7A0FD4059B8D93CDB595C26FB965F ();
// 0x000002A3 System.Void GoogleMobileAds.Api.AdRequest_Builder::set_Birthday(System.Nullable`1<System.DateTime>)
extern void Builder_set_Birthday_m18F4907DCB53B43A4AB84D9A4FEC331EF3BDD6BA ();
// 0x000002A4 System.Nullable`1<GoogleMobileAds.Api.Gender> GoogleMobileAds.Api.AdRequest_Builder::get_Gender()
extern void Builder_get_Gender_mB83590E6D5AF36CA159E7F61C299D3772F35673B ();
// 0x000002A5 System.Void GoogleMobileAds.Api.AdRequest_Builder::set_Gender(System.Nullable`1<GoogleMobileAds.Api.Gender>)
extern void Builder_set_Gender_m8F62ACDE8B79A67F0699938219CA8C1EF498150F ();
// 0x000002A6 System.Nullable`1<System.Boolean> GoogleMobileAds.Api.AdRequest_Builder::get_ChildDirectedTreatmentTag()
extern void Builder_get_ChildDirectedTreatmentTag_mC242E9C902CD6129ECE54B6B05F7DD3F678A1B65 ();
// 0x000002A7 System.Void GoogleMobileAds.Api.AdRequest_Builder::set_ChildDirectedTreatmentTag(System.Nullable`1<System.Boolean>)
extern void Builder_set_ChildDirectedTreatmentTag_m2D7E167EA7BF368A801A8BD1C896A2E5192E359F ();
// 0x000002A8 System.Collections.Generic.Dictionary`2<System.String,System.String> GoogleMobileAds.Api.AdRequest_Builder::get_Extras()
extern void Builder_get_Extras_m947369FDFFE90AE086C2E16A10AC3B0CE7E5E1C7 ();
// 0x000002A9 System.Void GoogleMobileAds.Api.AdRequest_Builder::set_Extras(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void Builder_set_Extras_mC1939AD69520DC9E0B7FAAD0F2AF17629A15A0ED ();
// 0x000002AA System.Collections.Generic.List`1<GoogleMobileAds.Api.Mediation.MediationExtras> GoogleMobileAds.Api.AdRequest_Builder::get_MediationExtras()
extern void Builder_get_MediationExtras_m19DFB6A4399E985BB64F5D7106CF0B3A6F8A1DFD ();
// 0x000002AB System.Void GoogleMobileAds.Api.AdRequest_Builder::set_MediationExtras(System.Collections.Generic.List`1<GoogleMobileAds.Api.Mediation.MediationExtras>)
extern void Builder_set_MediationExtras_m5C41AB155F4913CB7FD7F430CEEE3AF40CB13109 ();
// 0x000002AC GoogleMobileAds.Api.AdRequest_Builder GoogleMobileAds.Api.AdRequest_Builder::AddKeyword(System.String)
extern void Builder_AddKeyword_m9FB2118C9FF37FF5537CDBF091A1DC4E9F87B039 ();
// 0x000002AD GoogleMobileAds.Api.AdRequest_Builder GoogleMobileAds.Api.AdRequest_Builder::AddTestDevice(System.String)
extern void Builder_AddTestDevice_m30A22C328FDE7DF6EA8173624814D8EB06E0B56F ();
// 0x000002AE GoogleMobileAds.Api.AdRequest GoogleMobileAds.Api.AdRequest_Builder::Build()
extern void Builder_Build_mD244B61E5DBF9D8D8E882A33977EAC5A6187BFC7 ();
// 0x000002AF GoogleMobileAds.Api.AdRequest_Builder GoogleMobileAds.Api.AdRequest_Builder::SetBirthday(System.DateTime)
extern void Builder_SetBirthday_mCDCF1E0197F741F1AAA2EDE4174BDE219596A619 ();
// 0x000002B0 GoogleMobileAds.Api.AdRequest_Builder GoogleMobileAds.Api.AdRequest_Builder::SetGender(GoogleMobileAds.Api.Gender)
extern void Builder_SetGender_m041362E0E96A00796EF5A1C2A118D0DAD02E34E4 ();
// 0x000002B1 GoogleMobileAds.Api.AdRequest_Builder GoogleMobileAds.Api.AdRequest_Builder::AddMediationExtras(GoogleMobileAds.Api.Mediation.MediationExtras)
extern void Builder_AddMediationExtras_m1AD3E9978AA58EFE3932CBA4F1D8CB9B0941C699 ();
// 0x000002B2 GoogleMobileAds.Api.AdRequest_Builder GoogleMobileAds.Api.AdRequest_Builder::TagForChildDirectedTreatment(System.Boolean)
extern void Builder_TagForChildDirectedTreatment_m673ED0659E607F8E82672E7C106DA36CD064F732 ();
// 0x000002B3 GoogleMobileAds.Api.AdRequest_Builder GoogleMobileAds.Api.AdRequest_Builder::AddExtra(System.String,System.String)
extern void Builder_AddExtra_m49788B6DFBD13194BFFA5146E1FCBC8B8B4C8AD4 ();
static Il2CppMethodPointer s_methodPointers[691] = 
{
	MonoPInvokeCallbackAttribute__ctor_mB9D2688DB30B918CCD5961A8DB873F32F2205D7E,
	Card_OnEnable_mA98BC80216BA24DE0FB1809658F5F4CE23D5C6D8,
	Card_Start_m88E71E8C5EB701A2B11A2AD5CE4E8DF0C0CF0275,
	Card_Update_m37036CE3D85389BF581EFE1357684D99FD238186,
	Card_OpenCard_m8E1C0BD9FD2BC0B5BF6E3C93BE9A4DAF466E9CB6,
	Card__ctor_m706585CE3E010E315E14D9825379E2F7A44FB090,
	Constants__cctor_m98FD3FF3D8F21CD21887CCFAA3265E285CB01DF1,
	Game_Start_m87108F3ECAE225CE62F13FB4F438BBB97BB71033,
	Game_RequestInterstitial_mAD45F76512F7459FCC2FF292993CD3E276E172F0,
	Game_Update_m5C0A097D8278D1AE765DA8A0BDC865A756C9E216,
	Game_RestartGame_m403D17AD7C508C9913474C2B6D7ECFEADC847CBA,
	Game_OpenPanels_mFF6E48301CC63ACA7768AA4993B851A6287B2603,
	Game_RefreshScores_m4442AA5313CFCBE839D17D2EB0108A38C8A9D71C,
	Game_BackBtn_m6C485830B002982AD0378B9E883F93EED478B042,
	Game_ChipBtn_mE50F27435531C605880033069AAEAB5255BF1CF0,
	Game_ClearBetBtn_m6DD3774C005A6DE3400C3F1B613425D299D46C4C,
	Game_LastBetBtn_m417F8BD919EFCA1F5217B456003F3310C74DA2F7,
	Game_DealBtn_m08FDD9F5442E6D82E5206F00429660A2B0912EFD,
	Game_StartGame_m5D3F6A97655E0F54215E306B1CB4BD8F6F6D6A38,
	Game_DoubleBtn_m42FC86FA585DC094AED7E154ED6D6BA923F5F15B,
	Game_StayBtn_mE9B2058DAEBE9F047711E2CDEC6D06D94845077D,
	Game_HitBtn_mC0EE6A101B15E599667B198AB7452266848018D3,
	Game_GetNewCard_mC4D9F0E7B519EEF8D32E8861F2964E63218CDD97,
	Game_CardOpened_m6D54C5E0B5CC7B8DA1D869C0DE54E28CE4307E65,
	Game_CheckScores_mF56E59971977CC8F8F63F0CB9AB65A079FE7C9B4,
	Game_Win_mA19ADE2E7908AA6A58044A41474198E4341C30D7,
	Game_Lose_mE490FF52E14901AE6170516465C69BE062C72324,
	Game_Blackjack_m76BBACE4495A421F4C751746AEA884BD2C6C8CBB,
	Game__ctor_mC065A45FD96CE26F2F9543F0C6BC65F2935A8C12,
	Menu_Start_m26D2BE4EA9DEE5AF2FE548CF8EDDAFA0FE7379E9,
	Menu_Update_m3870FCE8E278FA9F95178BDD18B258C3F4D49BAA,
	Menu_RequestRewardVideo_mC6FC3DBAC40902860533D8BF5C78FB5BBD476944,
	Menu_HandleRewardBasedVideoRewarded_m0B9E46D6532F5EEAAD4E0F3737CE65436DA63572,
	Menu_OpenScreen_mCF89ADA1C1BA0D861053344895E94772A530EFE6,
	Menu_PlayBtn_m6E1DEECCF0637E2764399EDAB455C1CC65D2438E,
	Menu_RateBtn_m88827BCC56691B4728908B482D4D7FAB1F59AB54,
	Menu_ShopBtn_m85B3EA4AD485BF9FF002BD93FE770CBB5E74D217,
	Menu_MainMenuBtn_mC83A16665D27D193705E500150FE617D73C58019,
	Menu_ShowVideoAd_m3E4B2520FA144D70A553743FFB0318C83CE5B7B1,
	Menu_ExitBtn_mFCDBE922EFCF6352970EFED1E6455B25ADDB61CE,
	Menu__ctor_mD372D109F6554E1F9A25291964C852C9F6BFC463,
	Purchaser_Start_mCD122FAB158727577ED229C323774D10737D1835,
	Purchaser_InitializePurchasing_mA433D882053BB54E75A01A2919EA1989C9470F9F,
	Purchaser_IsInitialized_m272B7D0B6DB8CDEB87223E1224F013BB3533C890,
	Purchaser_BuyConsumable_mFD6E311F8C75EF0A2589C9282FC91463BE256FDD,
	Purchaser_BuyProductID_mBCDAC5023D5830F87D3E0116AE9DC5447065385E,
	Purchaser_RestorePurchases_m24A1E9C7782A272240B01EB09EBC8828ED550846,
	Purchaser_OnInitialized_m2C5D997E02074CA968ACF89D2EA8849616E2AE8A,
	Purchaser_OnInitializeFailed_mD26D07D1E337C7DFCA40D1D65DA7F4F85BF449DE,
	Purchaser_ProcessPurchase_m9A0B2A4762E2AF7D006F42F231BB676CDDCC6238,
	Purchaser_OnPurchaseFailed_m4A93D014B730F86486878D3A2EEE69C949EB07F9,
	Purchaser__ctor_m526162775A909925C95DBABA1943472F883DFDDA,
	Purchaser__cctor_mADAAF4DC1AEEC46CD5D7A737ED6896BF8BC7C6E8,
	GoogleMobileAdsClientFactory_BuildBannerClient_mDF34A2569CD7EC1C65DBF778094794BE76C4285D,
	GoogleMobileAdsClientFactory_BuildInterstitialClient_m2C47FEE4563F9FBFA90EE3DC8026C07D0979410C,
	GoogleMobileAdsClientFactory_BuildRewardBasedVideoAdClient_m3DF8A4521BE504081121100F03BD8AFC83E3F798,
	GoogleMobileAdsClientFactory_BuildAdLoaderClient_mBA3B6397E3B56942810431CEC2710D699856DC24,
	GoogleMobileAdsClientFactory_BuildNativeExpressAdClient_m51966CE9566E9F3FCC0F5407E2E51377120A92ED,
	GoogleMobileAdsClientFactory_MobileAdsInstance_m5BA8107B3B6E51CA4BAB44F365AAE4DF5C66A671,
	GoogleMobileAdsClientFactory__ctor_mCCE39F56BB450D301B0F962D5AE78538C659A70A,
	AdLoaderClient__ctor_m776C37E00913BBA78A0FFE5DB3A44F3499D78EA9,
	AdLoaderClient_add_OnCustomNativeTemplateAdLoaded_m1EFC5C6C6E5A045EA65673ECF90650BC6B328C66,
	AdLoaderClient_remove_OnCustomNativeTemplateAdLoaded_mED9514C6FFD42DB29FB365119B969515A1A0E81E,
	AdLoaderClient_add_OnAdFailedToLoad_m378D89D2B93348EFADDFA501F8046155371D3B98,
	AdLoaderClient_remove_OnAdFailedToLoad_m5335EC5167F9DAD3737B86B3CB8401824DD6BBAC,
	AdLoaderClient_get_AdLoaderPtr_m3AFA8F4324EAB0318D8726DCBBA52E5B2AC3B764,
	AdLoaderClient_set_AdLoaderPtr_m29A1E30DF114597A3625C8B0BEA0ABBE33BFDFEE,
	AdLoaderClient_LoadAd_mB74F09FB021A2FB3A5123E0A9175E16A989A4C88,
	AdLoaderClient_DestroyAdLoader_mC7994769E3ED1790F2EB9798FB700E21992E7AAE,
	AdLoaderClient_Dispose_mB46AD18199CCA93F20525BD0FBBD0E9DAE8B7EBC,
	AdLoaderClient_Finalize_mBBFF53308265B50E108D583F15BD8D785D0FBFC6,
	AdLoaderClient_AdLoaderDidReceiveNativeCustomTemplateAdCallback_m96CB8031ECC1E20CDB3AA013D0E1CA5C82FBA9F0,
	AdLoaderClient_AdLoaderDidFailToReceiveAdWithErrorCallback_mC2A70F1FD62DF7B697FF79C19CDB49D89A3ED296,
	AdLoaderClient_IntPtrToAdLoaderClient_m3ED983610AD1859E5A69FC8FB96E50B77E0B3A3F,
	BannerClient_add_OnAdLoaded_m85C0A8F18C65AE4251E93125BA55EE42CCB53F0B,
	BannerClient_remove_OnAdLoaded_m89977460F2A599A67DC8CD8C3A8F50A197F11620,
	BannerClient_add_OnAdFailedToLoad_m5CD1230B0CF55AB9BA690B1D26BB03AC625362DE,
	BannerClient_remove_OnAdFailedToLoad_m9E733D3F999D3ED8CCD1C4C5452778420B7F99E8,
	BannerClient_add_OnAdOpening_m1167105F6C87BB67BE6829FB15F3CA505192D27C,
	BannerClient_remove_OnAdOpening_m00DB05B39AE9F2A5ACE748910B5990F5B990C0D1,
	BannerClient_add_OnAdClosed_m9A5684725ED9AF6E48DB4E538DB176A662606AA0,
	BannerClient_remove_OnAdClosed_m9C2B397BCEED79A202899C719CDB21AC52A66CD1,
	BannerClient_add_OnAdLeavingApplication_m59C732D86073AC57C4D34B491EEFBC9784D33053,
	BannerClient_remove_OnAdLeavingApplication_m88E76B1E6239A086AF96F67390B146ACBFF1D7CA,
	BannerClient_get_BannerViewPtr_mCF1D51A11C17101E403C9B5968EA0A7518020C0C,
	BannerClient_set_BannerViewPtr_m353E0194631D4929742E9BC6BCD8DA820877C834,
	BannerClient_CreateBannerView_mA9666C1CFB7BC00B379E28DC4BD57FE3D0BDE8D7,
	BannerClient_CreateBannerView_mBD3B7E483609F175DF3ABC7BB9CF55F09FF966F6,
	BannerClient_LoadAd_mAF7404CEC6D070E8039EAE644AD57DA01F713E81,
	BannerClient_ShowBannerView_m152A0DAD2496770376421189061540E0A9D9AB17,
	BannerClient_HideBannerView_m356318F8F8119E95BA4917D0BE95DD19C94E119B,
	BannerClient_DestroyBannerView_m90363A637D524DD709F395C7E5AD12F5F783D563,
	BannerClient_Dispose_m322A94C006EAF78B295AE7F95517FA15C27BF6C9,
	BannerClient_Finalize_mF559A3AD4DA94DB4CF13033E6EBFD0066423CDE5,
	BannerClient_AdViewDidReceiveAdCallback_m5F0611A8BD59EBB7E6A4CFD4AF1C20132C7D5345,
	BannerClient_AdViewDidFailToReceiveAdWithErrorCallback_m863303F5E9FF5A436CE67C416A3E708E9E2F6206,
	BannerClient_AdViewWillPresentScreenCallback_mCCCDADC61157A1E8B91B12B866A326AE24024816,
	BannerClient_AdViewDidDismissScreenCallback_m15101864FE77339582C8DF5FC5129E33CFC9674B,
	BannerClient_AdViewWillLeaveApplicationCallback_mD2C41507939D77A4986009D02E1962A5A200AFE8,
	BannerClient_IntPtrToBannerClient_mC03443688427FD43FEDEB78D90FA808950C063C9,
	BannerClient__ctor_m0E636B97280FE26520BF647017A7AF7731652849,
	CustomNativeTemplateClient_get_CustomNativeAdPtr_mF3B152C666CA5290B76A8FCF3CE0E3A6D10975F3,
	CustomNativeTemplateClient_set_CustomNativeAdPtr_mDB9F4A38A4EC892204971567F133026B0E1631FE,
	CustomNativeTemplateClient__ctor_mF23404A53988358DFD8051F45DF63F41451075DD,
	CustomNativeTemplateClient_GetAvailableAssetNames_m9B45BF3FC124C0976BBEC7B6EDAA25065F92684C,
	CustomNativeTemplateClient_GetTemplateId_m03B096D3A7D6E223827C3DB8CC650B7983512BA6,
	CustomNativeTemplateClient_GetImageByteArray_m38B3756A149443C0EA1CC1E8A5994B1A8E4AFF36,
	CustomNativeTemplateClient_GetText_m9C60ADACF912900D0AFA4B7EA7F97B71D86CD8E7,
	CustomNativeTemplateClient_PerformClick_m7CA0220F031CDDF0F19CD8E95D8C8D8A5D472D59,
	CustomNativeTemplateClient_RecordImpression_m8FA4C608F4A89F3BDAA31B238BE936F698B7CB02,
	CustomNativeTemplateClient_DestroyCustomNativeTemplateAd_m9FCFE963D61BC09DED5E01FCFDAB6BD47B12A18B,
	CustomNativeTemplateClient_Dispose_mD3CBD994BB8C2A137DCC376FBD94E27CB41BB97A,
	CustomNativeTemplateClient_Finalize_m997DED96E4DE609C78A71F6B8A0F38B33DB8AB13,
	CustomNativeTemplateClient_NativeCustomTemplateDidReceiveClickCallback_m93BA0F7BF0763D9ECEECB912C673A1BAED9D989F,
	CustomNativeTemplateClient_IntPtrToAdLoaderClient_m27F6CD4D1AA20E628AAB75D5DC37CB7BA3CBD91B,
	Externs_GADUInitialize_m2DC5EBB6A6B6A6E7A97D528CA76B3016051C0E80,
	Externs_GADUCreateRequest_m73B645271C266986E9711F38EF4E0CA6FC511D9C,
	Externs_GADUCreateMutableDictionary_m71EE6107C3600A67D5A7C4D73C018CBAE659C23C,
	Externs_GADUMutableDictionarySetValue_m1806F4834F6CCA36CD93928D50F81B097D90F60D,
	Externs_GADUSetMediationExtras_m89618AB332B3A9FE4FB423511B821186B35F1D2E,
	Externs_GADUAddTestDevice_m76BAC633F8636E5D4F8BF2A1D6A9383B7BCC3866,
	Externs_GADUAddKeyword_mCF88D72AA181DCB3C3DA3FB9BD096E64818FB492,
	Externs_GADUSetBirthday_mF7965515078BEAD86EBCC07FFA7004755A1F4D4F,
	Externs_GADUSetGender_m6DA6BDFDB359466F6E2FBBC7FCF77693DA9241C2,
	Externs_GADUTagForChildDirectedTreatment_m79D9D3D2E5D37AD810F239728B87D2264F994AF3,
	Externs_GADUSetExtra_m220E34583749470A8335664E94766D25674D1726,
	Externs_GADUSetRequestAgent_m27CF00F87EA0EBA6A9C3C8709E5FAD8810198647,
	Externs_GADURelease_mA1A6033D91229BF50E5357B8D12FE4B97424B08F,
	Externs_GADUCreateBannerView_m330BB8938EFD797A10859C7B34088781979DFEAF,
	Externs_GADUCreateBannerViewWithCustomPosition_m09B328E45301936407727CFBA9569AF7960EE599,
	Externs_GADUCreateSmartBannerView_mCB9BA3A611AA1D22C3C1AB0ABFE5027FFD670557,
	Externs_GADUCreateSmartBannerViewWithCustomPosition_mB7E8C01D1505C4FF0587E32157AB911C69E93E8C,
	Externs_GADUSetBannerCallbacks_m55B99E0FDF40B0CA06351FDED7D6FE82D71C238C,
	Externs_GADUHideBannerView_mB52E09C130A2CB24A423B0857D8342D100EB28F2,
	Externs_GADUShowBannerView_m472CD3FA35C432E8ECDD7BA266316F5DDDEC224F,
	Externs_GADURemoveBannerView_m58C4B13ACDD405DB163F13B37D870086EC140091,
	Externs_GADURequestBannerAd_m4D4212C3D10F54900739111245FF7D08CB832D67,
	Externs_GADUCreateInterstitial_mBB5011FE3F7A220A2A1C18491EB2FA1CC8BF228F,
	Externs_GADUSetInterstitialCallbacks_m6C17569859C333E78DB7B262DF49FD49474F3B1E,
	Externs_GADUInterstitialReady_m3B59FBBF66CFFED00743CA573A1ABCEBA2D721AA,
	Externs_GADUShowInterstitial_mFAE7157C540B0F2A4A2287E1F4987862232D314A,
	Externs_GADURequestInterstitial_m7C292EA914D2701FF7E8F34827B6C327E9CEA2ED,
	Externs_GADUCreateRewardBasedVideoAd_m9DC3169D059E38DC294D25299004797599C5315D,
	Externs_GADURewardBasedVideoAdReady_mD3AF839910B89E6C292D2D07842834803B6F4E57,
	Externs_GADUShowRewardBasedVideoAd_m79B32218E887318B0CC061E0E7DC56D2E018D82E,
	Externs_GADURequestRewardBasedVideoAd_m03CAA6C4D6CB4D2D1A949AEBE3F70072826A42DE,
	Externs_GADUSetRewardBasedVideoAdCallbacks_m84A73BE6D0604C0CEAB5F25DC04B113F502A58AC,
	Externs_GADUCreateAdLoader_m52B92F08A6DC29B35188A34463524EA5F84466E7,
	Externs_GADURequestNativeAd_m771F421E091A4BA77364824E1059AA86E8DFEA0C,
	Externs_GADUSetAdLoaderCallbacks_m8F706419533752CD86C86039F02E81E4FE45255C,
	Externs_GADUNativeCustomTemplateAdTemplateID_mF258BA627C6B3F615798C35EA50742E6572B0A3B,
	Externs_GADUNativeCustomTemplateAdImageAsBytesForKey_m177D2CFB72DEB8199E879D01D9615E31B4590583,
	Externs_GADUNativeCustomTemplateAdStringForKey_mAEE62F9026067FF4B82ABD1C32CD308B2FE48BE0,
	Externs_GADUNativeCustomTemplateAdRecordImpression_m86F37A2179013E352F967E169A7DE932DB031D8D,
	Externs_GADUNativeCustomTemplateAdPerformClickOnAssetWithKey_m901C391C058E7C249B091DBBAB301A98F8BE83BA,
	Externs_GADUNativeCustomTemplateAdAvailableAssetKeys_m6A2827935AEACD365679F1F2A3A3843C768C05A0,
	Externs_GADUNativeCustomTemplateAdNumberOfAvailableAssetKeys_mD55D743161D0F80D2E3A85A722697B6C2C854EEC,
	Externs_GADUSetNativeCustomTemplateAdUnityClient_mE7A1817DDFDE2130C8B6649A1FE837ED88340491,
	Externs_GADUSetNativeCustomTemplateAdCallbacks_m38D48258FB3008B874889D374D275B29EF7FC13F,
	Externs_GADUCreateNativeExpressAdView_m3A9843414E578E7F0C5BEE5B37B8FFE7C7396E0C,
	Externs_GADUCreateNativeExpressAdViewWithCustomPosition_mAF8E09A8A9BA503C47ED472848E76BBD2250EB02,
	Externs_GADUSetNativeExpressAdCallbacks_mBB0A35806F4092661B77D0B0843978A3E704BF96,
	Externs_GADUHideNativeExpressAdView_mC2C950CA25B51FC0BA4649E64ECD561C5CE9E9A0,
	Externs_GADUShowNativeExpressAdView_m8EFEBC6156D1ABE4E74C09B81704C54970645941,
	Externs_GADURemoveNativeExpressAdView_m849B78D65F7208C51A6DDFB235ACDF5A8C8125BD,
	Externs_GADURequestNativeExpressAd_mC9BCB2D886B637A3764F80AEF1ABE148214941B6,
	Externs__ctor_m1B8F9795C92865B5C37DF1F73C0DADDCFAF1D27E,
	InterstitialClient_add_OnAdLoaded_m7FF81DFE1D529E663594F56F1F08E758C53A7978,
	InterstitialClient_remove_OnAdLoaded_m18393564E716B23C6FDC0DE4593847CFFB224F71,
	InterstitialClient_add_OnAdFailedToLoad_m20194C2A5AB143538AB1B86027F067A8DE89BDC0,
	InterstitialClient_remove_OnAdFailedToLoad_m8DCD9420EE00D7C67E9A2DFB5725DB930388D37E,
	InterstitialClient_add_OnAdOpening_mDDEBB7FE0D0D739A4AE516FF7F4E11A1804FC848,
	InterstitialClient_remove_OnAdOpening_mF021A585CD392E9EC4200EC6C7ACDAE666C3D088,
	InterstitialClient_add_OnAdClosed_m6FB7CBF39A25A8605A845417E241CDD2D6C332A2,
	InterstitialClient_remove_OnAdClosed_mCA6AD28BEB8B346EB283375AD38B4CA876DCFB3C,
	InterstitialClient_add_OnAdLeavingApplication_m8466A9CF5F9EC993D6F544022B7A412776C58FCF,
	InterstitialClient_remove_OnAdLeavingApplication_m2759CB8145124D7CE0C895ACAA3F7CCE9F2E73DC,
	InterstitialClient_get_InterstitialPtr_mF19309BA5A75A3592B55C9034703C536CD798D22,
	InterstitialClient_set_InterstitialPtr_m46CD1113827F95A2556B498FF8A605C98C7C1F15,
	InterstitialClient_CreateInterstitialAd_m7E082B54344097D75CAD764A3F87FF07C38640DA,
	InterstitialClient_LoadAd_m274EC58618BCC9934F96D414E3B77DD06EAC08F7,
	InterstitialClient_IsLoaded_m3E8FCA13281A0743EBC6558A432F5AE9EF7F1045,
	InterstitialClient_ShowInterstitial_m619602539A7123C311D8EBF1D0FC4F956789A2A0,
	InterstitialClient_DestroyInterstitial_mB91E830A772ADBE03B4CF2077D338C28FD3693EC,
	InterstitialClient_Dispose_mA736ECCD646683B1AA60A8DA130D7D550ECBE039,
	InterstitialClient_Finalize_mE3E095E94743AC0781E1058A4F7DCF261FF4D383,
	InterstitialClient_InterstitialDidReceiveAdCallback_mF485EB5E41F80ED5C5B6FF0376CD4B8BB96EED2B,
	InterstitialClient_InterstitialDidFailToReceiveAdWithErrorCallback_m9BD349E80A9B92676F0BF98C52C36A690DF107CA,
	InterstitialClient_InterstitialWillPresentScreenCallback_mB5835C4991F852B2E0768EAE7F9E9478EB2FD2C5,
	InterstitialClient_InterstitialDidDismissScreenCallback_mAD21EEBDD6D829719AF1FB8F438CC04C63EF431C,
	InterstitialClient_InterstitialWillLeaveApplicationCallback_m41A6D1FEF7A23CB20F37D767455B69D71FE83D42,
	InterstitialClient_IntPtrToInterstitialClient_mD7E495556451E3A916AED6AD9097FCAA4A0ED9DE,
	InterstitialClient__ctor_mA038B4FD51D6ECEC361A8E32BB6E2E89009BE522,
	MobileAdsClient__ctor_mC8DAE3EAC3FD1D338504A7CE0B5CD1D2074E0B24,
	MobileAdsClient_get_Instance_m1D2605F82CAB8B9522BDFF9EB65DBDA7AD53D11F,
	MobileAdsClient_Initialize_mD8E3D0BB200F36413F9D15FE914DED546EA98EFC,
	MobileAdsClient__cctor_m5C56A069E99872AC4D84284C79EEDA47CD71BE90,
	NativeExpressAdClient_add_OnAdLoaded_m61B75205166DB21299020E7F2927A03666AA8C5C,
	NativeExpressAdClient_remove_OnAdLoaded_m304395F0D657E6A788D3DD7612CE4F32FBF1697E,
	NativeExpressAdClient_add_OnAdFailedToLoad_mE938E4F0AFC94FDA0D753EC69E10B78AC3E4C504,
	NativeExpressAdClient_remove_OnAdFailedToLoad_m5731DC10D2A0D455F9A883C7518F2F685A4839AE,
	NativeExpressAdClient_add_OnAdOpening_m2429A951065B6822828164BEBB278FA81C225B11,
	NativeExpressAdClient_remove_OnAdOpening_mD4B56212A51E8E93D15B55749991EEB3CD68349B,
	NativeExpressAdClient_add_OnAdClosed_m76C7A38AE602DF481FA3FDA746BA443556454D68,
	NativeExpressAdClient_remove_OnAdClosed_mDF5CC6CC487B96415EDAC2C8A08A7792D974CE32,
	NativeExpressAdClient_add_OnAdLeavingApplication_m22264B2690FB5DA4412EA65723D86AFA90977194,
	NativeExpressAdClient_remove_OnAdLeavingApplication_m92BD728ABD5C29100C0453DE44763D1EBCD77339,
	NativeExpressAdClient_get_NativeExpressAdViewPtr_m60BDC950D04AD2343B12FC792C26C5B3F3AE85A6,
	NativeExpressAdClient_set_NativeExpressAdViewPtr_m826D00251F3B7DF9BE6A1F2D23F2C81CC2768CE1,
	NativeExpressAdClient_CreateNativeExpressAdView_m4BD420FADED6190A7DFA895D15A6092BB11BD45B,
	NativeExpressAdClient_CreateNativeExpressAdView_m635C29C84C42BFCC1B67ABECFCAC8CAB881F61B2,
	NativeExpressAdClient_LoadAd_mF62C1ABCEE4593D2522C2364A932505718341672,
	NativeExpressAdClient_ShowNativeExpressAdView_mBFD045AF0E6C94B4C37F09A01D1938F98CBE7CE2,
	NativeExpressAdClient_HideNativeExpressAdView_m41576E80275B343E9E11D7F0D9D5A88810825CE1,
	NativeExpressAdClient_DestroyNativeExpressAdView_m5A6634ACAD43D15C765AD610615C263E3A98838B,
	NativeExpressAdClient_Dispose_mEEDD761DA08FEC9C03B298D728A87DBAC1D70885,
	NativeExpressAdClient_Finalize_m3DEA3D88CEECB33693084B72B2AFA7F808F4B084,
	NativeExpressAdClient_NativeExpressAdViewDidReceiveAdCallback_m7E5BCA95DAE8C27B53B4DF27571E1D47DC1BC924,
	NativeExpressAdClient_NativeExpressAdViewDidFailToReceiveAdWithErrorCallback_m02B2DBEB39F61BC6B4188F167105425E5139DF1B,
	NativeExpressAdClient_NativeExpressAdViewWillPresentScreenCallback_m374AF054824A2809817D4D34D05128633FD80BDB,
	NativeExpressAdClient_NativeExpressAdViewDidDismissScreenCallback_mB912EF80149AE81DA955BB54023A8919BEE81671,
	NativeExpressAdClient_NativeExpressAdViewWillLeaveApplicationCallback_m39E270E3EBE1071BA14C4D71464294BAA4C70D28,
	NativeExpressAdClient_IntPtrToNativeExpressAdClient_mC775C276A6914274D8C3EF4794F2305F7EE93179,
	NativeExpressAdClient__ctor_mD9356B704E083447EE18E5B58EDD1E036A4775FF,
	RewardBasedVideoAdClient_add_OnAdLoaded_m288E7403FCEEB56AC329BC56EE21F26A2542C0EE,
	RewardBasedVideoAdClient_remove_OnAdLoaded_mF89D4EA7868CD288E348E448E4700F1E41879F32,
	RewardBasedVideoAdClient_add_OnAdFailedToLoad_mCCB067CA039D5122E45558FC43C079EA93AB37D8,
	RewardBasedVideoAdClient_remove_OnAdFailedToLoad_mF64A6F2271989A44A2DDB626435F0937063006FB,
	RewardBasedVideoAdClient_add_OnAdOpening_mDFCB39F3DCADE57957CD0A3F520F4D8D6B78FACB,
	RewardBasedVideoAdClient_remove_OnAdOpening_mC7D1ACF64C9F157E0DB01C11AC40FC88267A2CA7,
	RewardBasedVideoAdClient_add_OnAdStarted_mBB127E18532FA7929D514A2017601A91FAFB0669,
	RewardBasedVideoAdClient_remove_OnAdStarted_m41DF78DD621ED7B214ECB2F6523FD7FBB6D46885,
	RewardBasedVideoAdClient_add_OnAdClosed_m2BE6E86ABA9B6DC4740FE149F6E1A15D274F0371,
	RewardBasedVideoAdClient_remove_OnAdClosed_m7200B30E383D1B110645104197258F422CC8B991,
	RewardBasedVideoAdClient_add_OnAdRewarded_mBD07CC223AA48F3DB977E8EF08781C3093EFD101,
	RewardBasedVideoAdClient_remove_OnAdRewarded_mE7D586DDA5CCB3AB5650417C1C773C5DAB6EC06C,
	RewardBasedVideoAdClient_add_OnAdLeavingApplication_m57561C2BE3DB4D2EEDB9F0334190E76D2886A367,
	RewardBasedVideoAdClient_remove_OnAdLeavingApplication_m53755FBA928AAB13C28DF51590FC099213300F64,
	RewardBasedVideoAdClient_get_RewardBasedVideoAdPtr_m1F3DE898E6CFB9607D5567EE5D5E73EF2644863E,
	RewardBasedVideoAdClient_set_RewardBasedVideoAdPtr_mD144E86D1BE08F42F0F01B813408430BE4DDE370,
	RewardBasedVideoAdClient_CreateRewardBasedVideoAd_mCED64E1E751A92726F4CF42B848FB2E457DE4A98,
	RewardBasedVideoAdClient_LoadAd_m2473195E7EF1C6A67CEFCA8E944FF437B61823CD,
	RewardBasedVideoAdClient_ShowRewardBasedVideoAd_m890F18B07EA6EF8C51E96F99DD087F7A897D07C7,
	RewardBasedVideoAdClient_IsLoaded_m376917C457D49B46EF76E0AE492CDDEAE5901D12,
	RewardBasedVideoAdClient_DestroyRewardedVideoAd_m835649B9B66B4BB350D3DD11D962029617A2EEB2,
	RewardBasedVideoAdClient_Dispose_mE33CCE97427FA4CBABC6539BAFDCA10DC96F91B5,
	RewardBasedVideoAdClient_Finalize_m98F5C3570DAE9CE50E027B352EE82A03CC7FE63B,
	RewardBasedVideoAdClient_RewardBasedVideoAdDidReceiveAdCallback_mC3AC0F651C19E19CC6247CB1AF7E694C3A8968E2,
	RewardBasedVideoAdClient_RewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_m92F8D48CFD9859C3DF734B3B531377978AF137BD,
	RewardBasedVideoAdClient_RewardBasedVideoAdDidOpenCallback_m139F9D3FE045E6AD73E3C653FD5BC0D1B4CF1901,
	RewardBasedVideoAdClient_RewardBasedVideoAdDidStartCallback_mF45B63FD0D47CFC8566ED5B3137D933247CBAE87,
	RewardBasedVideoAdClient_RewardBasedVideoAdDidCloseCallback_m734CE280D2BBC57576B3A57E32C521E83443B202,
	RewardBasedVideoAdClient_RewardBasedVideoAdDidRewardUserCallback_mC1CA5205857E2C48D6CB3F2BE820505DF05D7070,
	RewardBasedVideoAdClient_RewardBasedVideoAdWillLeaveApplicationCallback_mFF7E45939A22B52E3C318C1115C13E42827B349C,
	RewardBasedVideoAdClient_IntPtrToRewardBasedVideoClient_mAB84423F44303BF2C18015BF7BDFFDAB40C8253A,
	RewardBasedVideoAdClient__ctor_m7E434825A64254490DE421DF6FCBD47DE4C363D9,
	Utils_BuildAdRequest_m79F99FD27AEBB11A401DF217D6C5F537B8CC9FA2,
	Utils__ctor_mFAEDAC0248C372B9CD40E50183B3662EB5B4A7AC,
	DummyClient__ctor_m415BC83649885989D5632B2CFA42D1AAB8DE2390,
	DummyClient_add_OnAdLoaded_m5DF52E7736ECF965A548E5F68A070A4C4D88C3DD,
	DummyClient_remove_OnAdLoaded_mE4FB7E15A79B727B7DD7CA55BA7EA50B104804B5,
	DummyClient_add_OnAdFailedToLoad_m924E35A4925572FDE96E634DBF557410306EC706,
	DummyClient_remove_OnAdFailedToLoad_m68725E31184CEA38ACA99641C7F5C8554A4D89F0,
	DummyClient_add_OnAdOpening_m8E1196E3D8C44FAA11170DC62CF75F1DE7472247,
	DummyClient_remove_OnAdOpening_mF55CA617BB7C09DBDEDD5FD8A11A8DDC99411956,
	DummyClient_add_OnAdStarted_mA25EB97B356325F1F640BCDB64229F4DC879B099,
	DummyClient_remove_OnAdStarted_mBBDD969259E6EBF1EA2D014F4D88E836FAD54ADF,
	DummyClient_add_OnAdClosed_m58DE279A243EB1ABB9C6F5E49BD07E17956EF6AA,
	DummyClient_remove_OnAdClosed_m033065AC7E04DFA63FF92490E5E5525176266063,
	DummyClient_add_OnAdRewarded_m0422FCBCE46103B4563B2AF5A6005B1A8E734B32,
	DummyClient_remove_OnAdRewarded_m4E66EBD33DAA35F69226AEFD34177C791CECA24C,
	DummyClient_add_OnAdLeavingApplication_m6ED1B118F6FBBB78B8AEF66707D90E077182E027,
	DummyClient_remove_OnAdLeavingApplication_m0FD38C022E4D71C364F9A3357851EF76AE117603,
	DummyClient_add_OnCustomNativeTemplateAdLoaded_m5C8D42A0B833DF463D5B82485145BBA8E2D15E39,
	DummyClient_remove_OnCustomNativeTemplateAdLoaded_m498A9C68CFEE1A9F5867C75D9A0BBED1991C5A94,
	DummyClient_get_UserId_mF3BDE1B3C01B7C50AB76099802CB24161BF2DFF0,
	DummyClient_set_UserId_m99C2A0AB8E5542E3B7F543884D02B931BBAD4E79,
	DummyClient_Initialize_m9528C626842EA8090CECEEA6A504EBD51C1B96B7,
	DummyClient_CreateBannerView_m1A13C26FF3DB3735060172E22836F874D207DED2,
	DummyClient_CreateBannerView_mB8B3A4424688E2215F951CC372C2167A6E47D617,
	DummyClient_LoadAd_m73E9982188004A34600627D04F7C3F5CCA408C3B,
	DummyClient_ShowBannerView_m31EB944535650EB7814A7AE0A45936AB2A2FE189,
	DummyClient_HideBannerView_mBFA9F00484225F0546A6D2A3214E8D50AA700CAC,
	DummyClient_DestroyBannerView_mA529DF86AC96568706FC0F90A64968410E38982B,
	DummyClient_CreateInterstitialAd_m37434429DA45C2E50D2A26CABDCDC6AE8D7EE10C,
	DummyClient_IsLoaded_mDD188150144BD2251BF13C64FE7EEB55BB2DB467,
	DummyClient_ShowInterstitial_m72E81B1F857B66DE35B9C4560671CF14F43E43DF,
	DummyClient_DestroyInterstitial_m0EFB09868D915C4363A3F4F1E27E99EA735B9845,
	DummyClient_CreateRewardBasedVideoAd_m15F851D1A3B5F1E2F78DBF1B67CAAF3F411B30B2,
	DummyClient_SetUserId_m351642C0069EAD0C5189D5C2D991CDE9B4154AD2,
	DummyClient_LoadAd_mF5A5795104C0B908A8BF7B07FAAD0AD3B85A3865,
	DummyClient_DestroyRewardBasedVideoAd_m08D54178B18DAF95D8992ED12BFA3C533A0CF1A4,
	DummyClient_ShowRewardBasedVideoAd_m8629C3A92ABE31E0A36E3D9E19EE7280D339C109,
	DummyClient_CreateAdLoader_mCF7A6337C06D69EF14797F6B5000E5FCF142F676,
	DummyClient_Load_m8D0BD1832281496A0E660ED6136E68F08327DAC3,
	DummyClient_CreateNativeExpressAdView_mA47540D6A688871096018776A5BB1A6115F0E160,
	DummyClient_CreateNativeExpressAdView_mAF46B7E5D58CA3308803C3DEBCDEDA67906D2C48,
	DummyClient_SetAdSize_mA3AB88C00C6B37065B4A3D69A3847655E053F81E,
	DummyClient_ShowNativeExpressAdView_mB508839B88FD24421F0C2CF21BE03845A8712CDF,
	DummyClient_HideNativeExpressAdView_m46C5987BC38587F607D566692BF22410098C1E8C,
	DummyClient_DestroyNativeExpressAdView_m9C24AE627D67B434997B3DF76A9804E973181DEA,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Utils_GetTexture2DFromByteArray_m544761C10D3FFEF3E5458CB2C161BEC868CBB4B5,
	Utils__ctor_m5851D6B26E085BB36515206CEE3B80D6008EF5A9,
	AdFailedToLoadEventArgs_get_Message_mAFCBACF7888F3D1708B63F48AB40BE9669938730,
	AdFailedToLoadEventArgs_set_Message_m6A87076DC03E1DF0795054ED41640B0C25A65F12,
	AdFailedToLoadEventArgs__ctor_m49BF00B309FD740DFBE7AC4D54DEDF4A27B8ED30,
	AdLoader__ctor_mA392711408CE360F65895976D093A9F40436D6DE,
	AdLoader_add_OnAdFailedToLoad_m8EEFA8C0A16867E80D2B9DF795AA5222DA4ABFD4,
	AdLoader_remove_OnAdFailedToLoad_m196F2E244AA1ED7D380B8D38EFDFD0FA6542E57B,
	AdLoader_add_OnCustomNativeTemplateAdLoaded_mDE6794CFA0776B5B360872D59DC1F28C15FFEF42,
	AdLoader_remove_OnCustomNativeTemplateAdLoaded_mA2E0DF28A39671263E21726F78B854951686F47B,
	AdLoader_get_CustomNativeTemplateClickHandlers_m255D9763F690C84AECE7BBAF3500AD6D938CB2AA,
	AdLoader_set_CustomNativeTemplateClickHandlers_mEBFFF3B81D6F604EA3515C877B69AEDDAB72244D,
	AdLoader_get_AdUnitId_m7886AD0BE8DEABE9CB4D5CD69E6F844B7D7D59C5,
	AdLoader_set_AdUnitId_m5D7C35C83EDC7E8479B94A7E9860176957D84C69,
	AdLoader_get_AdTypes_mC5B9F1C39B4778BEA6DC344E364694231EED88E9,
	AdLoader_set_AdTypes_mA6CA57A563B2C06ABAD62422F4E6B203110D14B8,
	AdLoader_get_TemplateIds_m459A5618E18C36FB388E3F8869E96CADC4A4742B,
	AdLoader_set_TemplateIds_m4AD70321838E87E465424966380BEB4B28E4BB31,
	AdLoader_LoadAd_m0C2FAD27AC919A54E4D099FB2585E8D3054F076D,
	AdLoader_U3C_ctorU3Eb__1_0_m5CEEF987CAFB19418DE1A337DCC1E119AA26EE10,
	AdLoader_U3C_ctorU3Eb__1_1_mE1A34AA0C411010497A5FE593437A4D1FDC11CA0,
	AdRequest__ctor_m788BAC27DAD4C0BFB9146E3E805EA8B412FD368F,
	AdRequest_get_TestDevices_m02EC537E8CEEC7E695A6055B8CB1E1889F164617,
	AdRequest_set_TestDevices_mD3019595DA7952487AE75EBDB9FA5770FFED7B10,
	AdRequest_get_Keywords_mA1D66061D1F93CF256DDD5AAE938C629104F11AA,
	AdRequest_set_Keywords_m73D12849D19999742A9F1C976EEA2C78F83FDE6B,
	AdRequest_get_Birthday_mF6551ECBB0E8EB04D73536381A5685D0FF55715A,
	AdRequest_set_Birthday_mCA669659854C4AD03734238B6BD38379AE46D5EC,
	AdRequest_get_Gender_m40E1D2BE6A214044F95918E202D11E5551118889,
	AdRequest_set_Gender_m69CEBFA698EE2C203B4E70F7E27F9F7B4D1452BD,
	AdRequest_get_TagForChildDirectedTreatment_mAF138AD39FC86FA60D310B685A6F3F1CDE91ABAA,
	AdRequest_set_TagForChildDirectedTreatment_mDD924F9F9593352918DA4A9459AC6A3471D71059,
	AdRequest_get_Extras_m814DAAAC04DFD17FC1DD6446718BEF964934B83B,
	AdRequest_set_Extras_m81633870EA00D9B67B551212A65B9132FB18B076,
	AdRequest_get_MediationExtras_mEEC772BDDD5CA17C993C45BD09F342E0CCFB1842,
	AdRequest_set_MediationExtras_m6EC989AE0BAE1AC4B4A514ABEECD4010D6137513,
	AdSize__ctor_m0C50105C319FEDF8747333CD7D91D0B30FE42DB9,
	AdSize__ctor_m49B4BC95FE5B63C3A42B4600DB353B5F8DACCD4E,
	AdSize_get_Width_m601B66752396AC49504944139A4C2095DA56ECD4,
	AdSize_get_Height_m5F2594D77109414FF4E8F43B6DB95ED482879556,
	AdSize_get_IsSmartBanner_m7FEB13C8ECEFEDEE425011F3C288571827B58283,
	AdSize__cctor_m4EE0706B1CFBCABD3372213B90F9459B356FDB46,
	BannerView__ctor_m6022987337C51378CFE91044D6C129DD58963B70,
	BannerView__ctor_m6035654B85852D7058762F6AB7D401F7DDD7E066,
	BannerView_add_OnAdLoaded_m4CA4BAF7D14C88BC1125C8718914C10A8832ACA9,
	BannerView_remove_OnAdLoaded_m4AC2656F5F698E022CB672C13294BD186BEC9AB6,
	BannerView_add_OnAdFailedToLoad_m4417B7CAE9503180DA1CF7C9C8FD69E2207843E2,
	BannerView_remove_OnAdFailedToLoad_m955DE3177E172610BC3DF0E7BF1E504CC4072419,
	BannerView_add_OnAdOpening_m8530663A5BE3B20DE2E21DE11774CBD2B56C95D5,
	BannerView_remove_OnAdOpening_m63DD5D98EE6D79D34691832318B89CFA0ED5479C,
	BannerView_add_OnAdClosed_m92D31DE8FFC3CA53A6E39338A024F905BEC4B873,
	BannerView_remove_OnAdClosed_mE76548DB9A6FD3CA888706F3B7D8D222F0C581AB,
	BannerView_add_OnAdLeavingApplication_mC64D39B3B87BD28427224BDA357FB2B7557689BD,
	BannerView_remove_OnAdLeavingApplication_m79FB7B5D67AA516FE980CFEF2561EA69E2ABF977,
	BannerView_LoadAd_mA22207B43FCBEC7A88469DAB0DB912611B568305,
	BannerView_Hide_m21981618F5392099BC82C735AA937E257718BF2A,
	BannerView_Show_m7CF03A1B9FCF671F1C0E9BC51744FA800FD138AE,
	BannerView_Destroy_m6AC3329E2403E577164C5F0895B416620279F2B8,
	BannerView_ConfigureBannerEvents_m934DAD8D1E0309FDE3759CF3EA48BB7BDAB0541F,
	BannerView_U3CConfigureBannerEventsU3Eb__22_0_m581166F1337E8F127274F5961141716A6CB3E1AA,
	BannerView_U3CConfigureBannerEventsU3Eb__22_1_m68D646088326FA102AB048B3E30553323A85F841,
	BannerView_U3CConfigureBannerEventsU3Eb__22_2_m90176468CC818A8371537C81A33C0396A7609EC3,
	BannerView_U3CConfigureBannerEventsU3Eb__22_3_mDF6BB5287A9A4EA548E86F5AD7E71098773F61B3,
	BannerView_U3CConfigureBannerEventsU3Eb__22_4_mC7901B8841C9A115851057B52B164061A89BA2AB,
	CustomNativeEventArgs_get_nativeAd_m8CF64F30A9268D10E4D2A1A45697911F478FC073,
	CustomNativeEventArgs_set_nativeAd_m5864A2B45419FC4C8307D44B3497C0A1D7E2F72A,
	CustomNativeEventArgs__ctor_m8083C677AB9FB88407C2EA5459957681CA9871B1,
	CustomNativeTemplateAd__ctor_m76DD1242D76D7AFAD82A20C4FA26C2F450BDF34D,
	CustomNativeTemplateAd_GetAvailableAssetNames_m5FB1A05B896817D3E17798A644F5BC4F2941D70B,
	CustomNativeTemplateAd_GetCustomTemplateId_mAEEAAA84324A61D34A3ABE9654F86A0F72F51144,
	CustomNativeTemplateAd_GetTexture2D_m62A91B20DF41EC8AEF45E5037F894A3F45934E8D,
	CustomNativeTemplateAd_GetText_m7F7F8B124077FB6F238F6ECAAEA73B3ADA3ED54D,
	CustomNativeTemplateAd_PerformClick_mAC10A57DF78C5613215A3A06F56B80422EC2CF50,
	CustomNativeTemplateAd_RecordImpression_mC88C8406464F196A22921864E032D4F4FF637CDA,
	InterstitialAd__ctor_mBC0DF92DB3A94D9FA1B86E52001EA456187FB8A3,
	InterstitialAd_add_OnAdLoaded_mCD788235B83ACE0DCE2082128ADD6BAA7ACED301,
	InterstitialAd_remove_OnAdLoaded_m96A5603033494B528D13A60194236A21CFD87272,
	InterstitialAd_add_OnAdFailedToLoad_mE7C99EDC105FB7C15B0CF941275DB526BFBB4069,
	InterstitialAd_remove_OnAdFailedToLoad_m5B024E743F730EE8BDCE26EF0C4262F670F27B34,
	InterstitialAd_add_OnAdOpening_m2E4509B320D841BC472E92921E9FE2B97CC8364C,
	InterstitialAd_remove_OnAdOpening_m5DB2079F937EA2A95A6BDCB5D18CC571AE10CDFB,
	InterstitialAd_add_OnAdClosed_m938576E7CA20056E5FD79107B5DE20F82DBA6E37,
	InterstitialAd_remove_OnAdClosed_m693F37A33B9B454732A577E5215D1478DDC037F8,
	InterstitialAd_add_OnAdLeavingApplication_m35F6AF01336D70EE791806515E3982A898DCE907,
	InterstitialAd_remove_OnAdLeavingApplication_mC0779CD437B95EB6E47AEAF134599196C5EF8522,
	InterstitialAd_LoadAd_m90AD135183B5E93F63711FAAFF9A854FB54DEE41,
	InterstitialAd_IsLoaded_m68E4FE04DE8DBEBF0FC1F0D7C988AF69B4856013,
	InterstitialAd_Show_m926F39FE9EA37D113E5B2F3F8E04878AD3A490AB,
	InterstitialAd_Destroy_mD9CF29B7289CC3D032A112D2973192E8A0E86607,
	InterstitialAd_U3C_ctorU3Eb__1_0_m11B142CD464FB526F742219198D4BC931540FD87,
	InterstitialAd_U3C_ctorU3Eb__1_1_m7B0D7D5A4125069DC1063F5E6E1191775321ABB5,
	InterstitialAd_U3C_ctorU3Eb__1_2_m2F3A2EA02AC054CE6AAF8DE78DB60F4831EA3B5C,
	InterstitialAd_U3C_ctorU3Eb__1_3_m5EDCB395DC0C78742CF2BAAE027CF311B843A87D,
	InterstitialAd_U3C_ctorU3Eb__1_4_m9B1C273D9A4FC9D47C901FE335DF6278600D0D14,
	MobileAds_Initialize_mBB7539DECEF49FAFBAFBE33C3FEB92B1D0FE9FE1,
	MobileAds_GetMobileAdsClient_mB118D2C9A5A05E5FF249EE0C8BDCA5FA6BF42BEE,
	MobileAds__ctor_mCD2EA0B29AB1801A42F7C8282550A4D31EE8E6BD,
	MobileAds__cctor_mC87B40DF3EE70F421CBD6ED61FBC9E84B40AFD21,
	NativeExpressAdView__ctor_m94025E1FB7EAA65028004F82B6C40F430AB059C8,
	NativeExpressAdView__ctor_m0D4A186508126CC6E75BC9977FF5013C58D7A992,
	NativeExpressAdView_add_OnAdLoaded_mF387677BB7039EF0D8C20B0AB1390E041884DE85,
	NativeExpressAdView_remove_OnAdLoaded_mAAB8E951902ACB124CD4A9FDD8C7C675CA2D70AA,
	NativeExpressAdView_add_OnAdFailedToLoad_m6F945474BF6F47F4E29344A3C1B2CDB1D0C50131,
	NativeExpressAdView_remove_OnAdFailedToLoad_m3C50A8C9C1681714DD9B72BA00183F0A997F92D9,
	NativeExpressAdView_add_OnAdOpening_mFC8BD2D75653E79526A22148CBED5152BFA02C0A,
	NativeExpressAdView_remove_OnAdOpening_mD4256F305CAD79ADECCC7A75834208C471187AAA,
	NativeExpressAdView_add_OnAdClosed_m4858A136ACD99DDB8BB36F47D22EE12FA325F2DA,
	NativeExpressAdView_remove_OnAdClosed_m4722354EBC273C8A58127D7B0BECFBB3A49DCBDF,
	NativeExpressAdView_add_OnAdLeavingApplication_m1C2EF3C4FCB814B5465133E3F39B2B3E76C88C85,
	NativeExpressAdView_remove_OnAdLeavingApplication_m697117E6F0581B0A83DD7D1A6B05C4B425569F1D,
	NativeExpressAdView_LoadAd_m2CDC846A00F0314A0F2B229BA87E6AEA6205F894,
	NativeExpressAdView_Hide_mE1C8F343A10CB63897D911F22C6EA5C945F9C743,
	NativeExpressAdView_Show_m523AA7DC6B76E0B00141C0FBEEE2867A59407767,
	NativeExpressAdView_Destroy_mBB65A037F95A658242368432CC1645A502194A90,
	NativeExpressAdView_ConfigureNativeExpressAdEvents_mB31C3D4EAA639919869E79BBD6DB97A5D4E32350,
	NativeExpressAdView_U3CConfigureNativeExpressAdEventsU3Eb__22_0_mC8B593FC672F0A3C5F94E09F51968774B5BBB18A,
	NativeExpressAdView_U3CConfigureNativeExpressAdEventsU3Eb__22_1_mA2359610C07C930A23845F711BAF6B996EB4C615,
	NativeExpressAdView_U3CConfigureNativeExpressAdEventsU3Eb__22_2_mE3D9AA2931356AFBF2C483DD44245AABA8FB15F8,
	NativeExpressAdView_U3CConfigureNativeExpressAdEventsU3Eb__22_3_mA79D9B0BDCB409288B09489D441963EECA22E934,
	NativeExpressAdView_U3CConfigureNativeExpressAdEventsU3Eb__22_4_m2B8B099EE3820D3EF77FAFD1C917DD5260DED5F0,
	Reward_get_Type_m88484E4A637E2E67559B23D8D94CDECE6FC643FF,
	Reward_set_Type_mDAD98536D1135210648EBC83AD6B0453F68F2C29,
	Reward_get_Amount_mA620F592EC72DEC2FE7D4C62552A3C129D69C16E,
	Reward_set_Amount_m19E1FF38C371472AA17BE0DC7FD9559DA2E3DF53,
	Reward__ctor_mB22E882CC0C8549E065DD745FD5DA2B065F248D4,
	RewardBasedVideoAd_get_Instance_mD22F18358E8AAE1EF7D0EF41D89938E27F110413,
	RewardBasedVideoAd__ctor_mCCDA78B541C02A50B80EFA09004FDDF8EAB88C97,
	RewardBasedVideoAd_add_OnAdLoaded_m3B6259AAA17196BF6269153F9FA89168C68B14C6,
	RewardBasedVideoAd_remove_OnAdLoaded_m581D661BC4261782EB3EAB17428FB806A55D3946,
	RewardBasedVideoAd_add_OnAdFailedToLoad_mA6CBF40C4C4AB1B09ADD6B44BD01ED36192D4B63,
	RewardBasedVideoAd_remove_OnAdFailedToLoad_m915FDD23DBE6E532F6E34348D50600BAF8C6748D,
	RewardBasedVideoAd_add_OnAdOpening_mC002CFD99C45593B31D9A34A9745E478C8116CF8,
	RewardBasedVideoAd_remove_OnAdOpening_m2E298551D5F9F94277504D485856605D757328A2,
	RewardBasedVideoAd_add_OnAdStarted_mA41531B9FCFDEC881174D5BE3809D7E4CE82D2A3,
	RewardBasedVideoAd_remove_OnAdStarted_m5ED905EBA857DDDAA7BA00AE2CB57AF0740DC1BC,
	RewardBasedVideoAd_add_OnAdClosed_m1A6B6CB3EC80C77E52C54E55C7F57E72BDED15DA,
	RewardBasedVideoAd_remove_OnAdClosed_mB994C35F507AC163E45460D6DFBB1ACA8B10949A,
	RewardBasedVideoAd_add_OnAdRewarded_mA6F7313150EB1E9237EF673700535A0E5DB4E200,
	RewardBasedVideoAd_remove_OnAdRewarded_m838857B1B270FD00C7B0D629266479073A865569,
	RewardBasedVideoAd_add_OnAdLeavingApplication_m5D0E04912268B472848453E2AF6A783E3A8CB5A3,
	RewardBasedVideoAd_remove_OnAdLeavingApplication_m6FA977A93908F7EF19BD0EFC375D1AF1116BA472,
	RewardBasedVideoAd_LoadAd_mDC5DA9B2E11911512038CBE1455D1066834CB1CE,
	RewardBasedVideoAd_IsLoaded_m52AE856E045477F4340E5A04F3497E51C71E3744,
	RewardBasedVideoAd_Show_m6690A11AF00C03DFD435BD1D95D66F9FB67178F1,
	RewardBasedVideoAd__cctor_m6DB083621C071D6873F56EA6F85C28063EEA4966,
	RewardBasedVideoAd_U3C_ctorU3Eb__4_0_m7163F7EF3B4A1D85A2C7ECF5602379476E4DB332,
	RewardBasedVideoAd_U3C_ctorU3Eb__4_1_m51EBF4658B499F46C786E4E3BBCF4FF2F6A157FC,
	RewardBasedVideoAd_U3C_ctorU3Eb__4_2_mAD88537833F1FAA82377BFC44EF204B71D18AF36,
	RewardBasedVideoAd_U3C_ctorU3Eb__4_3_m4EEC8D756041C0F52671DB502E343809B8684C58,
	RewardBasedVideoAd_U3C_ctorU3Eb__4_4_m94FDC2E4A0EED9410AE8A0199419305A4886B18D,
	RewardBasedVideoAd_U3C_ctorU3Eb__4_5_m0A49EDB0A41556D5C450C7D8147C99C72A1B702E,
	RewardBasedVideoAd_U3C_ctorU3Eb__4_6_m5B7D593155E690622FF351BEA30C615A1772CC38,
	MediationExtras_get_Extras_m845B48080AE808551366F9B6E363E0429EAE7869,
	MediationExtras_set_Extras_m8B938E04FD53E53543C27B52AE94A87B6764026A,
	NULL,
	NULL,
	MediationExtras__ctor_m539737F193B83E77F64430AB34EA566B0D1DA538,
	U3CRestartGameU3Ed__30__ctor_m47F17CFE6FA5D24A23C3A8CA909EC52B03043E23,
	U3CRestartGameU3Ed__30_System_IDisposable_Dispose_m5F182F7F6510C94711F56E3140B0B93C2F31DAA7,
	U3CRestartGameU3Ed__30_MoveNext_m05BCDAA5783A4216891DD7D3E7C5DCDCBB71EE86,
	U3CRestartGameU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCA499D5F0D23E9227931A436B48A7F49D78EFA10,
	U3CRestartGameU3Ed__30_System_Collections_IEnumerator_Reset_m1830BDF1305DE8B31CDD81077D42FB4128EEE15B,
	U3CRestartGameU3Ed__30_System_Collections_IEnumerator_get_Current_m1CA3493029F2A59FB70653BEB766338F6FC5BE2D,
	U3CStartGameU3Ed__38__ctor_m0D8BDE1483E9E0675FD749744BA99B755B12C40C,
	U3CStartGameU3Ed__38_System_IDisposable_Dispose_mA374F7607FF38FACB7223A2E5740F3D5FA99D1B5,
	U3CStartGameU3Ed__38_MoveNext_mBAD287F838C59DB2AA5535AAA5B0C983A2F77D98,
	U3CStartGameU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEAED3530594DCE0BA782A7875138725EEDBBCE64,
	U3CStartGameU3Ed__38_System_Collections_IEnumerator_Reset_mDFEA9255F458B12A9542288E515ADE3596725DD9,
	U3CStartGameU3Ed__38_System_Collections_IEnumerator_get_Current_m0DDBF8C4562A3721C64E01A6541262BDBD51F8CF,
	U3CGetNewCardU3Ed__42__ctor_m9B0F18FA2AE2448B06F3460DB78A642C6D78CBF9,
	U3CGetNewCardU3Ed__42_System_IDisposable_Dispose_mB9A4DED753303C5B3870C9016016AF3722AFB8D6,
	U3CGetNewCardU3Ed__42_MoveNext_mC7D9081126D66CB79C75784BAC2C434661586D04,
	U3CGetNewCardU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9E3D3044A41AFB19C919C7506BD876778469C332,
	U3CGetNewCardU3Ed__42_System_Collections_IEnumerator_Reset_m66B41C6E90E9193A18ECE520B84CA56E381B8ED8,
	U3CGetNewCardU3Ed__42_System_Collections_IEnumerator_get_Current_m6C0BEDFC680DAFA53CB41A98BB6AAC8C5B174F39,
	U3CU3Ec__cctor_mBACD32049FC5669777FA1E6521D96C92CD685940,
	U3CU3Ec__ctor_mFFFF6E872083F954C08D0CB94E2B71E42C0B465D,
	U3CU3Ec_U3CRestorePurchasesU3Eb__11_0_m997253913D06FEB1A817C89E993EC7970359E20E,
	GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback__ctor_m01865DCDE5629D6C88575A6C4C56164643A1D931,
	GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_Invoke_mA0655E0A4C0FCC33E3E5E5217982B78B72F6D181,
	GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_BeginInvoke_m9A6566A6AA46081016F9E2B3C09C3AFAA8AEAE49,
	GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_EndInvoke_mB6E927089B02B8B068414DC276F25F826CAA7B8F,
	GADUAdLoaderDidFailToReceiveAdWithErrorCallback__ctor_m86FF5C27E612003B7B0027C47368283384626806,
	GADUAdLoaderDidFailToReceiveAdWithErrorCallback_Invoke_m7FB34DDFCEEAC8BAB2CE876C4EFD97B3F6CBDCDA,
	GADUAdLoaderDidFailToReceiveAdWithErrorCallback_BeginInvoke_mB31CE310EB3B11BFCBF0D365380B91173FC12C5E,
	GADUAdLoaderDidFailToReceiveAdWithErrorCallback_EndInvoke_m8CFDE3B6D89A4875F729C7029B8FA6304267C1C9,
	GADUAdViewDidReceiveAdCallback__ctor_m0EE7ABAEA4086D3A9E370D5BEB84447C69D28840,
	GADUAdViewDidReceiveAdCallback_Invoke_m212CBA85682D81108ABD290D15248A3FF19F8463,
	GADUAdViewDidReceiveAdCallback_BeginInvoke_m380642644B6A5129F85CCB611F527D8EF35347BB,
	GADUAdViewDidReceiveAdCallback_EndInvoke_m505F1F8382A0BD0955E7941687343163F249EC78,
	GADUAdViewDidFailToReceiveAdWithErrorCallback__ctor_mED774BF3A3D359F5CA462CC757A00841DA612BD2,
	GADUAdViewDidFailToReceiveAdWithErrorCallback_Invoke_m8339C39A87BA824870FF102B4D3EC376A7BB9561,
	GADUAdViewDidFailToReceiveAdWithErrorCallback_BeginInvoke_m47F76A8B29F144ADF50E7A1269BF0B3A7A0C0008,
	GADUAdViewDidFailToReceiveAdWithErrorCallback_EndInvoke_m019B4B32CFDCA278EB93BA6CA3E0F7E878E8733A,
	GADUAdViewWillPresentScreenCallback__ctor_mB315B52B033E48AF41FCD4A19D234A5CF8F75EC2,
	GADUAdViewWillPresentScreenCallback_Invoke_mE11C18A6EBD243187C26BE348D39F78357E3060C,
	GADUAdViewWillPresentScreenCallback_BeginInvoke_m8BE2D393DC4460E8D9B9ABEF9BC7A57685639627,
	GADUAdViewWillPresentScreenCallback_EndInvoke_m26C583C70345059439501E3E78F2F98C937C562A,
	GADUAdViewDidDismissScreenCallback__ctor_m10C62AA35086747BDB71F5056C7A5BE67EFEED14,
	GADUAdViewDidDismissScreenCallback_Invoke_mCB1FE63B69549B6467AB3CC3F452FA39DCEB4D67,
	GADUAdViewDidDismissScreenCallback_BeginInvoke_m5FB7A6168E9CA0BD08265A2A01043B008F85FF71,
	GADUAdViewDidDismissScreenCallback_EndInvoke_m30C69C7276DD3040F46FCD43360F67D96B3778E3,
	GADUAdViewWillLeaveApplicationCallback__ctor_mDFEE54087FF2859342A5693E587CB7482D14FB40,
	GADUAdViewWillLeaveApplicationCallback_Invoke_mF13E9AE50D0EC69E91DE5646F8FD3E9C01D26195,
	GADUAdViewWillLeaveApplicationCallback_BeginInvoke_mC1310358A4FA5F77E38DC0FB5DDBC470AF4C291E,
	GADUAdViewWillLeaveApplicationCallback_EndInvoke_m290FA391D668433D0AF69240C28927C7C320A4B9,
	GADUNativeCustomTemplateDidReceiveClick__ctor_m56EBC8773DABBE9EE718230BA35A3649280F35C0,
	GADUNativeCustomTemplateDidReceiveClick_Invoke_m1BEFD4AE0D3B53D80EEF5D89B4647F9601974C05,
	GADUNativeCustomTemplateDidReceiveClick_BeginInvoke_m8BF32143D30147F22F9F070DCD7E20FDA5A1FB9A,
	GADUNativeCustomTemplateDidReceiveClick_EndInvoke_m7BC1E02EECCF69236579B8483A01BE45AB0B195C,
	GADUInterstitialDidReceiveAdCallback__ctor_m46748BE7477AC633AB8BB3CB7B8F7C1DAEB19663,
	GADUInterstitialDidReceiveAdCallback_Invoke_mCB1C1DD422AD2F3FE64DDA7275D58CDD6EB78093,
	GADUInterstitialDidReceiveAdCallback_BeginInvoke_m2ABFF1841B4711C187AF499F6BCB1FDF803BAA03,
	GADUInterstitialDidReceiveAdCallback_EndInvoke_m59E7E9C62CB96CD36D5290EFF73541A3B4868E33,
	GADUInterstitialDidFailToReceiveAdWithErrorCallback__ctor_m04C3C19130FC79B58231B1CC33AAE9DAAF6BE106,
	GADUInterstitialDidFailToReceiveAdWithErrorCallback_Invoke_m543D790AAFF4C3D3312B6605E7D28D807E536BBF,
	GADUInterstitialDidFailToReceiveAdWithErrorCallback_BeginInvoke_mD42FC423DA96E58C26B28A81BE121651EEBB1A72,
	GADUInterstitialDidFailToReceiveAdWithErrorCallback_EndInvoke_m09CFEAD53907A2D5902CA4BA212E8B38387C1E0E,
	GADUInterstitialWillPresentScreenCallback__ctor_mE0D16D80A71B8A0816B5BDFAB00BBF70450A7EC0,
	GADUInterstitialWillPresentScreenCallback_Invoke_mB845E3851869376F978E91A10737701005BCF3E2,
	GADUInterstitialWillPresentScreenCallback_BeginInvoke_mF129D26DD9BF99785F634453E15E2563A7514D38,
	GADUInterstitialWillPresentScreenCallback_EndInvoke_m9E51AE156A7AFBCF3FFEEBDD92DD842E657C7F5C,
	GADUInterstitialDidDismissScreenCallback__ctor_mC8BB9E760E0970E3169E805F88B2BDA8A6C22DDF,
	GADUInterstitialDidDismissScreenCallback_Invoke_m4A18067CB2FF2130FC7A4CF483CB9B2C0C1FB48A,
	GADUInterstitialDidDismissScreenCallback_BeginInvoke_m1B8C9194FA4A00B46E7DB735573B00B5AC742D7D,
	GADUInterstitialDidDismissScreenCallback_EndInvoke_m2C0BA479E1297482F3D8A912B6BC6DD55BAEEA41,
	GADUInterstitialWillLeaveApplicationCallback__ctor_mAF92C05FCA7D4AA439064C7F088B2A0A050DDF8B,
	GADUInterstitialWillLeaveApplicationCallback_Invoke_mFD61B207AECCEF60F6B212B370274122667206D9,
	GADUInterstitialWillLeaveApplicationCallback_BeginInvoke_m7FA8F610B77A46A667B1A3B54267843699D299DC,
	GADUInterstitialWillLeaveApplicationCallback_EndInvoke_m0EB02A834D96098756FD881E685A08FC46213ED6,
	GADUNativeExpressAdViewDidReceiveAdCallback__ctor_m3F0BF39499CE1CC16A49A4170DFDB7B8E752EB53,
	GADUNativeExpressAdViewDidReceiveAdCallback_Invoke_mD212D551CDFFE824AC9526289390078F8CAF5826,
	GADUNativeExpressAdViewDidReceiveAdCallback_BeginInvoke_m407584C3E16C4D5D99FBD47E5312EEB1AED0D4AA,
	GADUNativeExpressAdViewDidReceiveAdCallback_EndInvoke_m9230BCEE95802639DBF7A36C6362FF2EA2416484,
	GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback__ctor_m60B2E21BA729E06B6312738B3D1B078F0930E98E,
	GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback_Invoke_mCECA7919ED88DABD5418F57582700B30F50D9077,
	GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback_BeginInvoke_mB8CA4C992E7D784FB359C8FC46E923EC4AC7B243,
	GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback_EndInvoke_m5DECD9E83F9543200B6F882BFD70DC4ED729C25D,
	GADUNativeExpressAdViewWillPresentScreenCallback__ctor_mFFC950549B9D671950DE48F9752EB3FE7A3F9143,
	GADUNativeExpressAdViewWillPresentScreenCallback_Invoke_m2B79E3424B20F59922B91D38C06935F9DF3FF5AD,
	GADUNativeExpressAdViewWillPresentScreenCallback_BeginInvoke_mFED86644462FB13B70CDAF54ED5595DD32E410C2,
	GADUNativeExpressAdViewWillPresentScreenCallback_EndInvoke_mC503DA89AF7D63F6995151862F8EAB2A66E30FD3,
	GADUNativeExpressAdViewDidDismissScreenCallback__ctor_m017F2517AEC939DBDD723E364E1CB8C51CE2909A,
	GADUNativeExpressAdViewDidDismissScreenCallback_Invoke_m540C7864A978F11A29E544B4D6B415B9E02471DE,
	GADUNativeExpressAdViewDidDismissScreenCallback_BeginInvoke_m5DAC5BAA324287DDCF4CDB7658F1AFED345833FC,
	GADUNativeExpressAdViewDidDismissScreenCallback_EndInvoke_m2FCB617DB87882C57FEDC90137CEAF6AC2CAB9A1,
	GADUNativeExpressAdViewWillLeaveApplicationCallback__ctor_m65DD5BF6C7F475164ED1CF2D3E5E1C8F066461ED,
	GADUNativeExpressAdViewWillLeaveApplicationCallback_Invoke_mACA28AC736D7F383C119F242EF4B25DF934707CA,
	GADUNativeExpressAdViewWillLeaveApplicationCallback_BeginInvoke_m5AE8DC3BEB87F02CA5DDCB4EF276B5913EBA14E8,
	GADUNativeExpressAdViewWillLeaveApplicationCallback_EndInvoke_m494F8C744B2E5D189CF94A8CAD77C9FE42EB0037,
	GADURewardBasedVideoAdDidReceiveAdCallback__ctor_m6BFA47F70744CEAFCF3723C1844138908979D754,
	GADURewardBasedVideoAdDidReceiveAdCallback_Invoke_m130AB06CC46B715A709BB98EAE031F0D200E924A,
	GADURewardBasedVideoAdDidReceiveAdCallback_BeginInvoke_m6FD473EDD7855DCDD2A5BE6A3DB55A82026355C8,
	GADURewardBasedVideoAdDidReceiveAdCallback_EndInvoke_mE55D1CBBA8BEC8FC555305EF17F19C579733E168,
	GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback__ctor_m99DB2B38896C37DAC1AF5D8373B44400B717EBF5,
	GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_Invoke_mC418796EFB99655C21941C19867A04E7A73E0A69,
	GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_BeginInvoke_mC6F75C40CF2453D5769559E02626C43CA4956F79,
	GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_EndInvoke_mBD01AB0139E82724D959317CCC017F45C2404C45,
	GADURewardBasedVideoAdDidOpenCallback__ctor_m70452EE1AF2DD79704BC6EADB5A3C50D902D1B31,
	GADURewardBasedVideoAdDidOpenCallback_Invoke_mE6863B03981866D09D5E5D090A1399F1F1413BF2,
	GADURewardBasedVideoAdDidOpenCallback_BeginInvoke_mD728B72F171C4846733C3D2E0490675A10265E10,
	GADURewardBasedVideoAdDidOpenCallback_EndInvoke_mF614F861815F5C9DFB2E1204241C8C4EBFD3729D,
	GADURewardBasedVideoAdDidStartCallback__ctor_mFC4FF294BCD84D28985FC5DBCE5FA1F3A16F0518,
	GADURewardBasedVideoAdDidStartCallback_Invoke_m720D6286CD962856EE355C3D8147A0AFB57D8041,
	GADURewardBasedVideoAdDidStartCallback_BeginInvoke_m8FB416F8EC8EBF4FB4580CAE67A2D93845587F30,
	GADURewardBasedVideoAdDidStartCallback_EndInvoke_m2940E0F4A0F252E0A8D2BD073CFAD19510B9C4D7,
	GADURewardBasedVideoAdDidCloseCallback__ctor_mC38F7673ADC9F2131DCC5836932E2089DCAC65F2,
	GADURewardBasedVideoAdDidCloseCallback_Invoke_m9C17098C750904FFCEB4D0177A7ADB01A998B1B9,
	GADURewardBasedVideoAdDidCloseCallback_BeginInvoke_m72E64232FA64777E23ED798F84D1D3E49D7B3DD5,
	GADURewardBasedVideoAdDidCloseCallback_EndInvoke_m3462576E3E6DD2264AE10F2C8297D0829D7C72D6,
	GADURewardBasedVideoAdDidRewardCallback__ctor_mC1CD3B54C468E63C7DEAE8B2E773BEACBF8A8B50,
	GADURewardBasedVideoAdDidRewardCallback_Invoke_m63D57299C3435D8639407C85943D1E3CC8F7BAC6,
	GADURewardBasedVideoAdDidRewardCallback_BeginInvoke_m846C276D1F7E4A27EA129BB3C0BAF9A0D6A5AD6D,
	GADURewardBasedVideoAdDidRewardCallback_EndInvoke_m212F8DF54757231A230F22B585BE24B22B39DC7B,
	GADURewardBasedVideoAdWillLeaveApplicationCallback__ctor_m8BD895E3953FF6819459E215D6C87C6CEF0642F1,
	GADURewardBasedVideoAdWillLeaveApplicationCallback_Invoke_mEEB31E100B363252E5946E753A1526DBE27D3115,
	GADURewardBasedVideoAdWillLeaveApplicationCallback_BeginInvoke_mED9BCF0C91B338AD4B9A6E39629994AFEAAE6F32,
	GADURewardBasedVideoAdWillLeaveApplicationCallback_EndInvoke_m3022871D902B75E579C671E2161BDD27035154B5,
	Builder__ctor_m383E9C8CAA60EC9F75835BBF4CA3F57D7630A019,
	Builder_get_AdUnitId_mDE6780A96F897E02302302823C3B8DCF5253580D,
	Builder_set_AdUnitId_mB2E68F81D6350AA1407A38DEE85E1CAECF7822C7,
	Builder_get_AdTypes_mACA59D470BEA53A8211EE0337235F8FA2B37AD71,
	Builder_set_AdTypes_mC57AD00A722A31B417D8179427898A618C4CBEBE,
	Builder_get_TemplateIds_mA06AF8062791298F2F3EB6BB3438D0E6C6DA6985,
	Builder_set_TemplateIds_m49D18BAC46DB1908B32BC81A8DB08BC5354D606E,
	Builder_get_CustomNativeTemplateClickHandlers_mAC253BD630C3800E6DAD06BBE8C455011619CCFD,
	Builder_set_CustomNativeTemplateClickHandlers_m95A81DF276EC0CF1B89021970868E447A670C22C,
	Builder_ForCustomNativeAd_m3CF5FBF44E85C1A4910FA6EC989EC11EA1CBCFB8,
	Builder_ForCustomNativeAd_m81FE15EBFB6CDB2EF57C131C070AFCE83049D448,
	Builder_Build_m53BF7F1231B6D473EB0C8FEF9FDDC98257B9B50C,
	Builder__ctor_m0E264AB53F2163A32997830032FAFE5B49BD0897,
	Builder_get_TestDevices_m798C6F87230C7CED80DE52F8A43E133CC781CB6F,
	Builder_set_TestDevices_m3A7C5F1BB1C4BFC6EABE2BD684288995F20F32ED,
	Builder_get_Keywords_m078AC1113E0A1F738AF609E1A601AD0937C2E7DA,
	Builder_set_Keywords_mFF9014498E526DEB566B1A433CE05606567C2F0D,
	Builder_get_Birthday_mBAF2FCD92DC7A0FD4059B8D93CDB595C26FB965F,
	Builder_set_Birthday_m18F4907DCB53B43A4AB84D9A4FEC331EF3BDD6BA,
	Builder_get_Gender_mB83590E6D5AF36CA159E7F61C299D3772F35673B,
	Builder_set_Gender_m8F62ACDE8B79A67F0699938219CA8C1EF498150F,
	Builder_get_ChildDirectedTreatmentTag_mC242E9C902CD6129ECE54B6B05F7DD3F678A1B65,
	Builder_set_ChildDirectedTreatmentTag_m2D7E167EA7BF368A801A8BD1C896A2E5192E359F,
	Builder_get_Extras_m947369FDFFE90AE086C2E16A10AC3B0CE7E5E1C7,
	Builder_set_Extras_mC1939AD69520DC9E0B7FAAD0F2AF17629A15A0ED,
	Builder_get_MediationExtras_m19DFB6A4399E985BB64F5D7106CF0B3A6F8A1DFD,
	Builder_set_MediationExtras_m5C41AB155F4913CB7FD7F430CEEE3AF40CB13109,
	Builder_AddKeyword_m9FB2118C9FF37FF5537CDBF091A1DC4E9F87B039,
	Builder_AddTestDevice_m30A22C328FDE7DF6EA8173624814D8EB06E0B56F,
	Builder_Build_mD244B61E5DBF9D8D8E882A33977EAC5A6187BFC7,
	Builder_SetBirthday_mCDCF1E0197F741F1AAA2EDE4174BDE219596A619,
	Builder_SetGender_m041362E0E96A00796EF5A1C2A118D0DAD02E34E4,
	Builder_AddMediationExtras_m1AD3E9978AA58EFE3932CBA4F1D8CB9B0941C699,
	Builder_TagForChildDirectedTreatment_m673ED0659E607F8E82672E7C106DA36CD064F732,
	Builder_AddExtra_m49788B6DFBD13194BFFA5146E1FCBC8B8B4C8AD4,
};
static const int32_t s_InvokerIndices[691] = 
{
	26,
	23,
	23,
	23,
	23,
	23,
	3,
	23,
	23,
	23,
	14,
	32,
	23,
	23,
	32,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	34,
	23,
	23,
	23,
	23,
	32,
	23,
	23,
	23,
	23,
	27,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	89,
	32,
	26,
	23,
	27,
	32,
	115,
	144,
	23,
	3,
	4,
	4,
	4,
	0,
	4,
	4,
	23,
	26,
	26,
	26,
	26,
	26,
	15,
	7,
	26,
	23,
	23,
	23,
	1428,
	1387,
	18,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	15,
	7,
	125,
	36,
	26,
	23,
	23,
	23,
	23,
	23,
	25,
	1387,
	25,
	25,
	25,
	18,
	23,
	15,
	7,
	1634,
	14,
	14,
	28,
	28,
	26,
	23,
	23,
	23,
	23,
	1387,
	18,
	174,
	751,
	751,
	1636,
	1428,
	1387,
	1387,
	1637,
	1638,
	1639,
	1636,
	1387,
	25,
	1640,
	1641,
	1642,
	1643,
	1644,
	25,
	25,
	25,
	1645,
	914,
	1644,
	579,
	25,
	1645,
	1418,
	579,
	25,
	1428,
	1646,
	1647,
	1645,
	1636,
	18,
	707,
	707,
	25,
	1447,
	1418,
	175,
	1645,
	1387,
	1640,
	1641,
	1644,
	25,
	25,
	25,
	1645,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	15,
	7,
	26,
	26,
	89,
	23,
	23,
	23,
	23,
	25,
	1387,
	25,
	25,
	25,
	18,
	23,
	23,
	4,
	26,
	3,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	15,
	7,
	125,
	36,
	26,
	23,
	23,
	23,
	23,
	23,
	25,
	1387,
	25,
	25,
	25,
	18,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	15,
	7,
	23,
	27,
	23,
	89,
	23,
	23,
	23,
	25,
	1387,
	25,
	25,
	25,
	1648,
	25,
	18,
	23,
	24,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	14,
	26,
	26,
	125,
	36,
	26,
	23,
	23,
	23,
	26,
	89,
	23,
	23,
	23,
	26,
	27,
	23,
	23,
	26,
	26,
	125,
	36,
	26,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	125,
	36,
	26,
	23,
	23,
	23,
	14,
	28,
	14,
	28,
	26,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	89,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	125,
	36,
	26,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	27,
	89,
	23,
	0,
	23,
	14,
	26,
	23,
	26,
	26,
	26,
	26,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	26,
	27,
	27,
	26,
	14,
	26,
	14,
	26,
	1651,
	1652,
	1653,
	1654,
	1626,
	1627,
	14,
	26,
	14,
	26,
	143,
	31,
	10,
	10,
	89,
	3,
	125,
	36,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	27,
	27,
	27,
	27,
	27,
	14,
	26,
	23,
	26,
	14,
	14,
	28,
	28,
	26,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	89,
	23,
	23,
	27,
	27,
	27,
	27,
	27,
	174,
	4,
	23,
	3,
	125,
	36,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	27,
	27,
	27,
	27,
	27,
	14,
	26,
	452,
	338,
	23,
	4,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	27,
	89,
	23,
	3,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	14,
	26,
	14,
	14,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	3,
	23,
	31,
	137,
	978,
	1633,
	26,
	137,
	1634,
	1635,
	26,
	137,
	7,
	818,
	26,
	137,
	1634,
	1635,
	26,
	137,
	7,
	818,
	26,
	137,
	7,
	818,
	26,
	137,
	7,
	818,
	26,
	137,
	1634,
	1635,
	26,
	137,
	7,
	818,
	26,
	137,
	1634,
	1635,
	26,
	137,
	7,
	818,
	26,
	137,
	7,
	818,
	26,
	137,
	7,
	818,
	26,
	137,
	7,
	818,
	26,
	137,
	1634,
	1635,
	26,
	137,
	7,
	818,
	26,
	137,
	7,
	818,
	26,
	137,
	7,
	818,
	26,
	137,
	7,
	818,
	26,
	137,
	1634,
	1635,
	26,
	137,
	7,
	818,
	26,
	137,
	7,
	818,
	26,
	137,
	7,
	818,
	26,
	137,
	1649,
	1650,
	26,
	137,
	7,
	818,
	26,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	28,
	107,
	14,
	23,
	14,
	26,
	14,
	26,
	1651,
	1652,
	1653,
	1654,
	1626,
	1627,
	14,
	26,
	14,
	26,
	28,
	28,
	14,
	539,
	34,
	28,
	136,
	107,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[25] = 
{
	{ 0x06000048, 5,  (void**)&AdLoaderClient_AdLoaderDidReceiveNativeCustomTemplateAdCallback_m96CB8031ECC1E20CDB3AA013D0E1CA5C82FBA9F0_RuntimeMethod_var, 0 },
	{ 0x06000049, 6,  (void**)&AdLoaderClient_AdLoaderDidFailToReceiveAdWithErrorCallback_mC2A70F1FD62DF7B697FF79C19CDB49D89A3ED296_RuntimeMethod_var, 0 },
	{ 0x0600005F, 7,  (void**)&BannerClient_AdViewDidReceiveAdCallback_m5F0611A8BD59EBB7E6A4CFD4AF1C20132C7D5345_RuntimeMethod_var, 0 },
	{ 0x06000060, 8,  (void**)&BannerClient_AdViewDidFailToReceiveAdWithErrorCallback_m863303F5E9FF5A436CE67C416A3E708E9E2F6206_RuntimeMethod_var, 0 },
	{ 0x06000061, 9,  (void**)&BannerClient_AdViewWillPresentScreenCallback_mCCCDADC61157A1E8B91B12B866A326AE24024816_RuntimeMethod_var, 0 },
	{ 0x06000062, 10,  (void**)&BannerClient_AdViewDidDismissScreenCallback_m15101864FE77339582C8DF5FC5129E33CFC9674B_RuntimeMethod_var, 0 },
	{ 0x06000063, 11,  (void**)&BannerClient_AdViewWillLeaveApplicationCallback_mD2C41507939D77A4986009D02E1962A5A200AFE8_RuntimeMethod_var, 0 },
	{ 0x06000072, 12,  (void**)&CustomNativeTemplateClient_NativeCustomTemplateDidReceiveClickCallback_m93BA0F7BF0763D9ECEECB912C673A1BAED9D989F_RuntimeMethod_var, 0 },
	{ 0x060000BB, 13,  (void**)&InterstitialClient_InterstitialDidReceiveAdCallback_mF485EB5E41F80ED5C5B6FF0376CD4B8BB96EED2B_RuntimeMethod_var, 0 },
	{ 0x060000BC, 14,  (void**)&InterstitialClient_InterstitialDidFailToReceiveAdWithErrorCallback_m9BD349E80A9B92676F0BF98C52C36A690DF107CA_RuntimeMethod_var, 0 },
	{ 0x060000BD, 15,  (void**)&InterstitialClient_InterstitialWillPresentScreenCallback_mB5835C4991F852B2E0768EAE7F9E9478EB2FD2C5_RuntimeMethod_var, 0 },
	{ 0x060000BE, 16,  (void**)&InterstitialClient_InterstitialDidDismissScreenCallback_mAD21EEBDD6D829719AF1FB8F438CC04C63EF431C_RuntimeMethod_var, 0 },
	{ 0x060000BF, 17,  (void**)&InterstitialClient_InterstitialWillLeaveApplicationCallback_m41A6D1FEF7A23CB20F37D767455B69D71FE83D42_RuntimeMethod_var, 0 },
	{ 0x060000DA, 18,  (void**)&NativeExpressAdClient_NativeExpressAdViewDidReceiveAdCallback_m7E5BCA95DAE8C27B53B4DF27571E1D47DC1BC924_RuntimeMethod_var, 0 },
	{ 0x060000DB, 19,  (void**)&NativeExpressAdClient_NativeExpressAdViewDidFailToReceiveAdWithErrorCallback_m02B2DBEB39F61BC6B4188F167105425E5139DF1B_RuntimeMethod_var, 0 },
	{ 0x060000DC, 20,  (void**)&NativeExpressAdClient_NativeExpressAdViewWillPresentScreenCallback_m374AF054824A2809817D4D34D05128633FD80BDB_RuntimeMethod_var, 0 },
	{ 0x060000DD, 21,  (void**)&NativeExpressAdClient_NativeExpressAdViewDidDismissScreenCallback_mB912EF80149AE81DA955BB54023A8919BEE81671_RuntimeMethod_var, 0 },
	{ 0x060000DE, 22,  (void**)&NativeExpressAdClient_NativeExpressAdViewWillLeaveApplicationCallback_m39E270E3EBE1071BA14C4D71464294BAA4C70D28_RuntimeMethod_var, 0 },
	{ 0x060000F8, 23,  (void**)&RewardBasedVideoAdClient_RewardBasedVideoAdDidReceiveAdCallback_mC3AC0F651C19E19CC6247CB1AF7E694C3A8968E2_RuntimeMethod_var, 0 },
	{ 0x060000F9, 24,  (void**)&RewardBasedVideoAdClient_RewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_m92F8D48CFD9859C3DF734B3B531377978AF137BD_RuntimeMethod_var, 0 },
	{ 0x060000FA, 25,  (void**)&RewardBasedVideoAdClient_RewardBasedVideoAdDidOpenCallback_m139F9D3FE045E6AD73E3C653FD5BC0D1B4CF1901_RuntimeMethod_var, 0 },
	{ 0x060000FB, 26,  (void**)&RewardBasedVideoAdClient_RewardBasedVideoAdDidStartCallback_mF45B63FD0D47CFC8566ED5B3137D933247CBAE87_RuntimeMethod_var, 0 },
	{ 0x060000FC, 27,  (void**)&RewardBasedVideoAdClient_RewardBasedVideoAdDidCloseCallback_m734CE280D2BBC57576B3A57E32C521E83443B202_RuntimeMethod_var, 0 },
	{ 0x060000FD, 28,  (void**)&RewardBasedVideoAdClient_RewardBasedVideoAdDidRewardUserCallback_mC1CA5205857E2C48D6CB3F2BE820505DF05D7070_RuntimeMethod_var, 0 },
	{ 0x060000FE, 29,  (void**)&RewardBasedVideoAdClient_RewardBasedVideoAdWillLeaveApplicationCallback_mFF7E45939A22B52E3C318C1115C13E42827B349C_RuntimeMethod_var, 0 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	691,
	s_methodPointers,
	s_InvokerIndices,
	25,
	s_reversePInvokeIndices,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
