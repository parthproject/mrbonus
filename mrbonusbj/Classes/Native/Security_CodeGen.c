﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String UnityEngine.Purchasing.Security.DistinguishedName::get_Country()
extern void DistinguishedName_get_Country_m27D4E8B49804318270890ED9F41E00B291DD84E1 ();
// 0x00000002 System.Void UnityEngine.Purchasing.Security.DistinguishedName::set_Country(System.String)
extern void DistinguishedName_set_Country_m00E25B4C828A93251C476025B832B3B56F930F45 ();
// 0x00000003 System.String UnityEngine.Purchasing.Security.DistinguishedName::get_Organization()
extern void DistinguishedName_get_Organization_mCC686BE583CA12E32235260314D808AE44E2C181 ();
// 0x00000004 System.Void UnityEngine.Purchasing.Security.DistinguishedName::set_Organization(System.String)
extern void DistinguishedName_set_Organization_mA53B8FC985363E83D16DF38A50089EB9B70DB62D ();
// 0x00000005 System.String UnityEngine.Purchasing.Security.DistinguishedName::get_OrganizationalUnit()
extern void DistinguishedName_get_OrganizationalUnit_mE19318B17691E9DAC9F3AACCE55F5F73DF65EBAB ();
// 0x00000006 System.Void UnityEngine.Purchasing.Security.DistinguishedName::set_OrganizationalUnit(System.String)
extern void DistinguishedName_set_OrganizationalUnit_m0184277BD62539F23B4BEC531C190287FFD44496 ();
// 0x00000007 System.String UnityEngine.Purchasing.Security.DistinguishedName::get_Dnq()
extern void DistinguishedName_get_Dnq_mEF98F1605FBEF5927B340BE6074FC37AE37FBF1D ();
// 0x00000008 System.Void UnityEngine.Purchasing.Security.DistinguishedName::set_Dnq(System.String)
extern void DistinguishedName_set_Dnq_m79DA2975D9D89F4543324D86EF9276CC0C46C843 ();
// 0x00000009 System.String UnityEngine.Purchasing.Security.DistinguishedName::get_State()
extern void DistinguishedName_get_State_mFBABFC0FEB6F2DDF07A413901D98A87C758EB2C7 ();
// 0x0000000A System.Void UnityEngine.Purchasing.Security.DistinguishedName::set_State(System.String)
extern void DistinguishedName_set_State_m6CD7373BB7A0B66EE4528D2B604E3207EC7B06CD ();
// 0x0000000B System.String UnityEngine.Purchasing.Security.DistinguishedName::get_CommonName()
extern void DistinguishedName_get_CommonName_m575DCDB5FAF4883AD24AAE02236D3146EE55A5D0 ();
// 0x0000000C System.Void UnityEngine.Purchasing.Security.DistinguishedName::set_CommonName(System.String)
extern void DistinguishedName_set_CommonName_mA608754548337CF47EFBBD1D7AC5473C3BE5835D ();
// 0x0000000D System.Void UnityEngine.Purchasing.Security.DistinguishedName::set_SerialNumber(System.String)
extern void DistinguishedName_set_SerialNumber_m744A423172218C47707DEB407B3FE78A60891FD0 ();
// 0x0000000E System.Void UnityEngine.Purchasing.Security.DistinguishedName::.ctor(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern void DistinguishedName__ctor_mABA27F8AB7256D47002AC39C5A62470640A2C990 ();
// 0x0000000F System.Boolean UnityEngine.Purchasing.Security.DistinguishedName::Equals(UnityEngine.Purchasing.Security.DistinguishedName)
extern void DistinguishedName_Equals_m2273D715E5128AA9DB551EAB93A405A7F67D2277 ();
// 0x00000010 System.String UnityEngine.Purchasing.Security.DistinguishedName::ToString()
extern void DistinguishedName_ToString_m7A7D6A003A71713545FCDD8FD509042EF7E469DE ();
// 0x00000011 System.Void UnityEngine.Purchasing.Security.X509Cert::set_SerialNumber(System.String)
extern void X509Cert_set_SerialNumber_m05F3E78124172BCD03494374A7793293CA76A325 ();
// 0x00000012 System.Void UnityEngine.Purchasing.Security.X509Cert::set_ValidAfter(System.DateTime)
extern void X509Cert_set_ValidAfter_m4C305A27AD5FFD155A5B20F865C30E59617437D8 ();
// 0x00000013 System.Void UnityEngine.Purchasing.Security.X509Cert::set_ValidBefore(System.DateTime)
extern void X509Cert_set_ValidBefore_m794D7FF4707A13B5B05834258F81ED5E7A36EF11 ();
// 0x00000014 System.Void UnityEngine.Purchasing.Security.X509Cert::set_PubKey(UnityEngine.Purchasing.Security.RSAKey)
extern void X509Cert_set_PubKey_m4EE1C3669043F9D6963658B63FFB7A80BA4F58B2 ();
// 0x00000015 System.Void UnityEngine.Purchasing.Security.X509Cert::set_SelfSigned(System.Boolean)
extern void X509Cert_set_SelfSigned_mB92EBE4FDA4F1A710FC9D0D31D7B03CF0B57BF3D ();
// 0x00000016 UnityEngine.Purchasing.Security.DistinguishedName UnityEngine.Purchasing.Security.X509Cert::get_Subject()
extern void X509Cert_get_Subject_mFC1E1273E1EA2F70FF9B0D7421B88F776E7A2B22 ();
// 0x00000017 System.Void UnityEngine.Purchasing.Security.X509Cert::set_Subject(UnityEngine.Purchasing.Security.DistinguishedName)
extern void X509Cert_set_Subject_m4FE30A7F52C371B3642025803DBF6A864CC0B949 ();
// 0x00000018 UnityEngine.Purchasing.Security.DistinguishedName UnityEngine.Purchasing.Security.X509Cert::get_Issuer()
extern void X509Cert_get_Issuer_m63A2EC83463E64DCD270B66CE55C94ABD2A2D8FD ();
// 0x00000019 System.Void UnityEngine.Purchasing.Security.X509Cert::set_Issuer(UnityEngine.Purchasing.Security.DistinguishedName)
extern void X509Cert_set_Issuer_m5168289F36D844D4556428EEAAFB009E218F0993 ();
// 0x0000001A System.Void UnityEngine.Purchasing.Security.X509Cert::set_Signature(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern void X509Cert_set_Signature_mEEE076F624A5A9AE0FBF3297FBC56A15CA808040 ();
// 0x0000001B System.Void UnityEngine.Purchasing.Security.X509Cert::.ctor(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern void X509Cert__ctor_m16B268E0660C17D4DE2DBB49C5F43995DBAAEECE ();
// 0x0000001C System.Void UnityEngine.Purchasing.Security.X509Cert::ParseNode(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern void X509Cert_ParseNode_m6706478B6D79871A89D70C2463C2A85100CFC9BC ();
// 0x0000001D System.DateTime UnityEngine.Purchasing.Security.X509Cert::ParseTime(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern void X509Cert_ParseTime_m0E21379D326B869E1C4B11D530B1B5D3FB1E22DB ();
// 0x0000001E System.Void UnityEngine.Purchasing.Security.InvalidX509Data::.ctor()
extern void InvalidX509Data__ctor_m0406193C9CC7403B517D574E7E6AE03E70055E3B ();
// 0x0000001F LipingShare.LCLib.Asn1Processor.Asn1Node UnityEngine.Purchasing.Security.PKCS7::get_data()
extern void PKCS7_get_data_m948BF7C72D9C7F2DD5A1D7A7FB8B1342744712A9 ();
// 0x00000020 System.Void UnityEngine.Purchasing.Security.PKCS7::set_data(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern void PKCS7_set_data_m4A0AB958BFF7D70A7EAE5C54AE14655F50242360 ();
// 0x00000021 System.Collections.Generic.List`1<UnityEngine.Purchasing.Security.SignerInfo> UnityEngine.Purchasing.Security.PKCS7::get_sinfos()
extern void PKCS7_get_sinfos_mC3AAEB227E1CDD2A405AE1B3C3659B4BBF9490A5 ();
// 0x00000022 System.Void UnityEngine.Purchasing.Security.PKCS7::set_sinfos(System.Collections.Generic.List`1<UnityEngine.Purchasing.Security.SignerInfo>)
extern void PKCS7_set_sinfos_m5701F26693B6BBF54EF63CE2609028ACA1B297A2 ();
// 0x00000023 System.Collections.Generic.List`1<UnityEngine.Purchasing.Security.X509Cert> UnityEngine.Purchasing.Security.PKCS7::get_certChain()
extern void PKCS7_get_certChain_m647A8CBACC57B80F10CF41148111F938DD33A5A0 ();
// 0x00000024 System.Void UnityEngine.Purchasing.Security.PKCS7::set_certChain(System.Collections.Generic.List`1<UnityEngine.Purchasing.Security.X509Cert>)
extern void PKCS7_set_certChain_mC4C999DD19B8F78BD20FDDD4774C962612FFBAA9 ();
// 0x00000025 System.Void UnityEngine.Purchasing.Security.PKCS7::.ctor(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern void PKCS7__ctor_mDB535D63E656612CCD3141AF99855B044B3D3C85 ();
// 0x00000026 System.Void UnityEngine.Purchasing.Security.PKCS7::CheckStructure()
extern void PKCS7_CheckStructure_m4B9D8F80CAC7D3057112491D39E2C0038AB39666 ();
// 0x00000027 System.Int32 UnityEngine.Purchasing.Security.SignerInfo::get_Version()
extern void SignerInfo_get_Version_m6DD6B9B2476DD5A4CB388EFD4A5F55E3780F021A ();
// 0x00000028 System.Void UnityEngine.Purchasing.Security.SignerInfo::set_Version(System.Int32)
extern void SignerInfo_set_Version_mAF63A9C18D4843EBF167FCD34A38B9835B81BB15 ();
// 0x00000029 System.Void UnityEngine.Purchasing.Security.SignerInfo::set_IssuerSerialNumber(System.String)
extern void SignerInfo_set_IssuerSerialNumber_mEAF569A7BDBA2D85B939321CFB23E8F2DBE6C641 ();
// 0x0000002A System.Void UnityEngine.Purchasing.Security.SignerInfo::set_EncryptedDigest(System.Byte[])
extern void SignerInfo_set_EncryptedDigest_m800BAF437A0C61DB3023CFE6D35F211BC53B93AB ();
// 0x0000002B System.Void UnityEngine.Purchasing.Security.SignerInfo::.ctor(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern void SignerInfo__ctor_mE2FC3E812F4FF3AB71A7D8B63E220D6B25237E05 ();
// 0x0000002C System.Void UnityEngine.Purchasing.Security.IAPSecurityException::.ctor()
extern void IAPSecurityException__ctor_mEE5C6B06562B82D2FA0FFD86680B082D981F37A0 ();
// 0x0000002D System.Void UnityEngine.Purchasing.Security.InvalidPKCS7Data::.ctor()
extern void InvalidPKCS7Data__ctor_mF10128BBF8FCC6299DED02BE839AC452508FE6D8 ();
// 0x0000002E System.Void UnityEngine.Purchasing.Security.InvalidTimeFormat::.ctor()
extern void InvalidTimeFormat__ctor_m80EB2344062B3CF224E7C50E32633D6BE6761E06 ();
// 0x0000002F System.Void UnityEngine.Purchasing.Security.UnsupportedSignerInfoVersion::.ctor()
extern void UnsupportedSignerInfoVersion__ctor_mBC5A95399C04432DE2BF41AC39F052F0C74FE443 ();
// 0x00000030 System.Void UnityEngine.Purchasing.Security.RSAKey::set_rsa(System.Security.Cryptography.RSACryptoServiceProvider)
extern void RSAKey_set_rsa_mA985188A7C5477FE2B3CFFC4CCDAD3D9262DDC19 ();
// 0x00000031 System.Void UnityEngine.Purchasing.Security.RSAKey::.ctor(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern void RSAKey__ctor_m9CE603E33908A3C3BE529BF2500D9882579B542A ();
// 0x00000032 System.Security.Cryptography.RSACryptoServiceProvider UnityEngine.Purchasing.Security.RSAKey::ParseNode(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern void RSAKey_ParseNode_m374B0EEA8BD98F4C3F1C81BF1278048C0E3E2D75 ();
// 0x00000033 System.String UnityEngine.Purchasing.Security.RSAKey::ToXML(System.String,System.String)
extern void RSAKey_ToXML_m2ED8CADF067B42841DC8DF2EE9C41A6B4A6C7CA6 ();
// 0x00000034 System.Void UnityEngine.Purchasing.Security.InvalidRSAData::.ctor()
extern void InvalidRSAData__ctor_mBD6DD4963CBCBA3D90A0840D18949E0729F6AC49 ();
// 0x00000035 UnityEngine.Purchasing.Security.AppleReceipt UnityEngine.Purchasing.Security.AppleReceiptParser::Parse(System.Byte[])
extern void AppleReceiptParser_Parse_m86F70F4BFA6395DC071B46774B8AFC99DF2C2F3F ();
// 0x00000036 UnityEngine.Purchasing.Security.AppleReceipt UnityEngine.Purchasing.Security.AppleReceiptParser::Parse(System.Byte[],UnityEngine.Purchasing.Security.PKCS7&)
extern void AppleReceiptParser_Parse_mA1C64085E979FC8E3978A29034A83B54BC3F5568 ();
// 0x00000037 UnityEngine.Purchasing.Security.AppleReceipt UnityEngine.Purchasing.Security.AppleReceiptParser::ParseReceipt(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern void AppleReceiptParser_ParseReceipt_m9D98471FAA50CB1B470DF99CC91DFE5D77939AE9 ();
// 0x00000038 UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt UnityEngine.Purchasing.Security.AppleReceiptParser::ParseInAppReceipt(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern void AppleReceiptParser_ParseInAppReceipt_mBB1E882408D783432073B787195A92B29876C7E4 ();
// 0x00000039 System.DateTime UnityEngine.Purchasing.Security.AppleReceiptParser::TryParseDateTimeNode(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern void AppleReceiptParser_TryParseDateTimeNode_m76518AB3D22F3017E1A87F37758E7873E932C036 ();
// 0x0000003A System.Boolean UnityEngine.Purchasing.Security.AppleReceiptParser::ArrayEquals(T[],T[])
// 0x0000003B System.Void UnityEngine.Purchasing.Security.AppleReceiptParser::.ctor()
extern void AppleReceiptParser__ctor_m7FBB33B4A632A071E45E0B96BB19F1187ABC8D3A ();
// 0x0000003C System.Void UnityEngine.Purchasing.Security.AppleReceiptParser::.cctor()
extern void AppleReceiptParser__cctor_mE8DB2B9B3C1311CB5F5A25F44B991AEC4B8AB9D2 ();
// 0x0000003D System.Void UnityEngine.Purchasing.Security.AppleReceipt::set_bundleID(System.String)
extern void AppleReceipt_set_bundleID_m18D4017369F0942480F860C9C5E931B34CBCC55D ();
// 0x0000003E System.Void UnityEngine.Purchasing.Security.AppleReceipt::set_appVersion(System.String)
extern void AppleReceipt_set_appVersion_mD46F969C4725B3066A2C5DDC1A312175C4EE42C9 ();
// 0x0000003F System.Void UnityEngine.Purchasing.Security.AppleReceipt::set_opaque(System.Byte[])
extern void AppleReceipt_set_opaque_m1C3A799EE744DAD19647F1D493CC4015960BE694 ();
// 0x00000040 System.Void UnityEngine.Purchasing.Security.AppleReceipt::set_hash(System.Byte[])
extern void AppleReceipt_set_hash_m09D73BF998A3D414D9AA59DAF0EA50DD9C8FC0D7 ();
// 0x00000041 System.Void UnityEngine.Purchasing.Security.AppleReceipt::set_originalApplicationVersion(System.String)
extern void AppleReceipt_set_originalApplicationVersion_mA56F71E0A60F9CB135B615600091F569C2F173BC ();
// 0x00000042 System.Void UnityEngine.Purchasing.Security.AppleReceipt::set_receiptCreationDate(System.DateTime)
extern void AppleReceipt_set_receiptCreationDate_m8AFC15FFBB179D2A1AF332A0111DB29542739B02 ();
// 0x00000043 System.Void UnityEngine.Purchasing.Security.AppleReceipt::.ctor()
extern void AppleReceipt__ctor_m77284D7A1748BF2BF1F6D7BC669E37C6FB32CA66 ();
// 0x00000044 System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::set_quantity(System.Int32)
extern void AppleInAppPurchaseReceipt_set_quantity_m1B3527AEE43E35923E9B1AA79D459C6DCBF7BE36 ();
// 0x00000045 System.String UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::get_productID()
extern void AppleInAppPurchaseReceipt_get_productID_m342C7BAFE33CF778FA9DD1FAD4F416E5632CFFF2 ();
// 0x00000046 System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::set_productID(System.String)
extern void AppleInAppPurchaseReceipt_set_productID_m8856F5E46B4640A86D72B90E57CEAD939933F25D ();
// 0x00000047 System.String UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::get_transactionID()
extern void AppleInAppPurchaseReceipt_get_transactionID_m0ACEE55358C533A5139436DF7FBF8EA8E77BB13D ();
// 0x00000048 System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::set_transactionID(System.String)
extern void AppleInAppPurchaseReceipt_set_transactionID_m23F321C1776EC15DB0F2A4DD2D6375FBB9C33982 ();
// 0x00000049 System.String UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::get_originalTransactionIdentifier()
extern void AppleInAppPurchaseReceipt_get_originalTransactionIdentifier_mEA064F5D2A874A0F7D989BE7611E1F6C0F494D4F ();
// 0x0000004A System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::set_originalTransactionIdentifier(System.String)
extern void AppleInAppPurchaseReceipt_set_originalTransactionIdentifier_mA3EBC88CF53124921FD60F058305B03F2D7955D9 ();
// 0x0000004B System.DateTime UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::get_purchaseDate()
extern void AppleInAppPurchaseReceipt_get_purchaseDate_m8368DC8A4A27526E2F93EC19569842B55FAFB014 ();
// 0x0000004C System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::set_purchaseDate(System.DateTime)
extern void AppleInAppPurchaseReceipt_set_purchaseDate_m3F9BEC803E49E5C7B391C56C79352610010E1F4A ();
// 0x0000004D System.DateTime UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::get_originalPurchaseDate()
extern void AppleInAppPurchaseReceipt_get_originalPurchaseDate_m88CC67CD7B8652438DDFD69B34DDEB220D2838D9 ();
// 0x0000004E System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::set_originalPurchaseDate(System.DateTime)
extern void AppleInAppPurchaseReceipt_set_originalPurchaseDate_m83EA1646E1236C7DB29AAC21E8865B4077A52FF5 ();
// 0x0000004F System.DateTime UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::get_subscriptionExpirationDate()
extern void AppleInAppPurchaseReceipt_get_subscriptionExpirationDate_m3B6E451DFD2C77717B46DBEDF778FC35909B8A42 ();
// 0x00000050 System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::set_subscriptionExpirationDate(System.DateTime)
extern void AppleInAppPurchaseReceipt_set_subscriptionExpirationDate_m597F0DF94C97BE72B8E6D618AA05B5D68847A326 ();
// 0x00000051 System.DateTime UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::get_cancellationDate()
extern void AppleInAppPurchaseReceipt_get_cancellationDate_mF6D2BC01A0CD91D2F261EA69781AF5C4A559B0CB ();
// 0x00000052 System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::set_cancellationDate(System.DateTime)
extern void AppleInAppPurchaseReceipt_set_cancellationDate_mE269A74125D29E540818C07DB8B3BF471E188709 ();
// 0x00000053 System.Int32 UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::get_isFreeTrial()
extern void AppleInAppPurchaseReceipt_get_isFreeTrial_m94163306933ECAE902A5B1F0303A690ED3193AC2 ();
// 0x00000054 System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::set_isFreeTrial(System.Int32)
extern void AppleInAppPurchaseReceipt_set_isFreeTrial_m13CA8654B0D8813A15AE87191B60F3AF71A1543D ();
// 0x00000055 System.Int32 UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::get_productType()
extern void AppleInAppPurchaseReceipt_get_productType_mE57DFC228999EF79D830FADD6C2C6834A8F6A4EC ();
// 0x00000056 System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::set_productType(System.Int32)
extern void AppleInAppPurchaseReceipt_set_productType_mF8100BA9FCCC5548A9D00F004C45560B815461F7 ();
// 0x00000057 System.Int32 UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::get_isIntroductoryPricePeriod()
extern void AppleInAppPurchaseReceipt_get_isIntroductoryPricePeriod_mE40C7F712283A6952A9733FB9BEE6C4003964B59 ();
// 0x00000058 System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::set_isIntroductoryPricePeriod(System.Int32)
extern void AppleInAppPurchaseReceipt_set_isIntroductoryPricePeriod_m3DBC4A3228EEBAA815E62F0A7DA5B781BB4E76DE ();
// 0x00000059 System.Void UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::.ctor()
extern void AppleInAppPurchaseReceipt__ctor_m0A15EFC439370FB1201F8920434A999BEF164A09 ();
// 0x0000005A System.Void LipingShare.LCLib.Asn1Processor.Asn1Node::.ctor(LipingShare.LCLib.Asn1Processor.Asn1Node,System.Int64)
extern void Asn1Node__ctor_m052670A18A609C13A30FF3BF44F42292F90B5835 ();
// 0x0000005B System.Void LipingShare.LCLib.Asn1Processor.Asn1Node::Init()
extern void Asn1Node_Init_mDBFC888A267475E4627E72BE4CD12295A133D240 ();
// 0x0000005C System.String LipingShare.LCLib.Asn1Processor.Asn1Node::GetHexPrintingStr(LipingShare.LCLib.Asn1Processor.Asn1Node,System.String,System.String,System.Int32)
extern void Asn1Node_GetHexPrintingStr_m5799642E9D8E03D27AD437EF46CB21AD1B8B0916 ();
// 0x0000005D System.String LipingShare.LCLib.Asn1Processor.Asn1Node::FormatLineString(System.String,System.Int32,System.Int32,System.String)
extern void Asn1Node_FormatLineString_m5E573DC5B8749918EE821B39FF9A1F8056748F32 ();
// 0x0000005E System.String LipingShare.LCLib.Asn1Processor.Asn1Node::FormatLineHexString(System.String,System.Int32,System.Int32,System.String)
extern void Asn1Node_FormatLineHexString_mF947EC13BE2B1476CC1295B758DC90C28CA90D0D ();
// 0x0000005F System.Void LipingShare.LCLib.Asn1Processor.Asn1Node::.ctor()
extern void Asn1Node__ctor_mB895C7967A41AAA537B9D261A66C686952D38945 ();
// 0x00000060 System.Byte LipingShare.LCLib.Asn1Processor.Asn1Node::get_Tag()
extern void Asn1Node_get_Tag_mB20DF1AAA2B8D03D5C905B21F1877077580A599E ();
// 0x00000061 System.Byte LipingShare.LCLib.Asn1Processor.Asn1Node::get_MaskedTag()
extern void Asn1Node_get_MaskedTag_m96C6D2796A132D3040C2AA91D53526C1480CAE57 ();
// 0x00000062 System.Boolean LipingShare.LCLib.Asn1Processor.Asn1Node::LoadData(System.IO.Stream)
extern void Asn1Node_LoadData_m020D328BF401DD10BE43BF7616EE7DB22FD55B9F ();
// 0x00000063 System.Boolean LipingShare.LCLib.Asn1Processor.Asn1Node::SaveData(System.IO.Stream)
extern void Asn1Node_SaveData_m225C667D486622DB65AC083360206C0B0087DA5E ();
// 0x00000064 System.Void LipingShare.LCLib.Asn1Processor.Asn1Node::ClearAll()
extern void Asn1Node_ClearAll_m3BC6216C01ACAC711F558EC315F37AAE00A444C0 ();
// 0x00000065 System.Void LipingShare.LCLib.Asn1Processor.Asn1Node::AddChild(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern void Asn1Node_AddChild_mBAD5E7CD7DD3031021FBD7440C088D5C47FA3B00 ();
// 0x00000066 System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Node::get_ChildNodeCount()
extern void Asn1Node_get_ChildNodeCount_mFF3B0FE66CC780D78DC16BA35A3AD17AC25FE867 ();
// 0x00000067 LipingShare.LCLib.Asn1Processor.Asn1Node LipingShare.LCLib.Asn1Processor.Asn1Node::GetChildNode(System.Int32)
extern void Asn1Node_GetChildNode_m253F399D7632FC95B92F3490DB6985B9C07FC238 ();
// 0x00000068 System.String LipingShare.LCLib.Asn1Processor.Asn1Node::get_TagName()
extern void Asn1Node_get_TagName_m77D19A81729B25E427656F161477E21A06FDF67A ();
// 0x00000069 LipingShare.LCLib.Asn1Processor.Asn1Node LipingShare.LCLib.Asn1Processor.Asn1Node::get_ParentNode()
extern void Asn1Node_get_ParentNode_m778967836987D1CB6B26EDC56AF8011F17297DF3 ();
// 0x0000006A System.String LipingShare.LCLib.Asn1Processor.Asn1Node::GetText(LipingShare.LCLib.Asn1Processor.Asn1Node,System.Int32)
extern void Asn1Node_GetText_m2196C0DEACE777BC160131956EE6F968C961FE20 ();
// 0x0000006B System.String LipingShare.LCLib.Asn1Processor.Asn1Node::GetDataStr(System.Boolean)
extern void Asn1Node_GetDataStr_mE38E8F3EB43A40F8E0773F1B4387DBC11AEF5672 ();
// 0x0000006C System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Node::get_DataLength()
extern void Asn1Node_get_DataLength_m5E17D3369F2B0EE71BF6E4B57B86D8DC8AD61CAD ();
// 0x0000006D System.Byte[] LipingShare.LCLib.Asn1Processor.Asn1Node::get_Data()
extern void Asn1Node_get_Data_m0E1E0617C6E417EAFF68AD5E773C8ECEFDF8E4A2 ();
// 0x0000006E System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Node::get_Deepness()
extern void Asn1Node_get_Deepness_m2FB44E1BAD03C278A0622FB7D898DF4F2B6B3C83 ();
// 0x0000006F System.Void LipingShare.LCLib.Asn1Processor.Asn1Node::set_RequireRecalculatePar(System.Boolean)
extern void Asn1Node_set_RequireRecalculatePar_m76B8EB84833193FB0BFBC459A7A516FB2812C357 ();
// 0x00000070 System.Void LipingShare.LCLib.Asn1Processor.Asn1Node::RecalculateTreePar()
extern void Asn1Node_RecalculateTreePar_m7C7A35DC21B77C08D70AE60CA5C477BB43A5F736 ();
// 0x00000071 System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Node::ResetBranchDataLength(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern void Asn1Node_ResetBranchDataLength_m2A130CA2632CED5A992861B1E7D93BFC8C3966F5 ();
// 0x00000072 System.Void LipingShare.LCLib.Asn1Processor.Asn1Node::ResetDataLengthFieldWidth(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern void Asn1Node_ResetDataLengthFieldWidth_m58644868AC08BE51CF710A6704B7854A0BE5E5CF ();
// 0x00000073 System.Void LipingShare.LCLib.Asn1Processor.Asn1Node::ResetChildNodePar(LipingShare.LCLib.Asn1Processor.Asn1Node,System.Int64)
extern void Asn1Node_ResetChildNodePar_m9EB8E7AB23152E4C8FB105D8FF21231C4EAB4778 ();
// 0x00000074 System.String LipingShare.LCLib.Asn1Processor.Asn1Node::GetListStr(LipingShare.LCLib.Asn1Processor.Asn1Node,System.Int32)
extern void Asn1Node_GetListStr_m53278A63C27B25B99FF12B8B0FA8003D964DC436 ();
// 0x00000075 System.String LipingShare.LCLib.Asn1Processor.Asn1Node::GetIndentStr(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern void Asn1Node_GetIndentStr_m40BFE323432887D2A0665792BC01CAE49D0C88B4 ();
// 0x00000076 System.Boolean LipingShare.LCLib.Asn1Processor.Asn1Node::GeneralDecode(System.IO.Stream)
extern void Asn1Node_GeneralDecode_m9B5C8C226CCC66EB64E7E59AB1618D30D59264B9 ();
// 0x00000077 System.Boolean LipingShare.LCLib.Asn1Processor.Asn1Node::ListDecode(System.IO.Stream)
extern void Asn1Node_ListDecode_m926C504D93EB07D45B9D3BD041DA259AFC89A5BE ();
// 0x00000078 System.Boolean LipingShare.LCLib.Asn1Processor.Asn1Node::InternalLoadData(System.IO.Stream)
extern void Asn1Node_InternalLoadData_m7266383EBC8A8504BC7DBD82056C2DD869E9E3F2 ();
// 0x00000079 System.Void LipingShare.LCLib.Asn1Processor.Asn1Parser::.ctor()
extern void Asn1Parser__ctor_mAEDC614D4605C27F4F610E481133BF1A9084A9F9 ();
// 0x0000007A System.Void LipingShare.LCLib.Asn1Processor.Asn1Parser::LoadData(System.IO.Stream)
extern void Asn1Parser_LoadData_mA88FD3A737019D65B9FF992AA6726AE9C6082C5E ();
// 0x0000007B LipingShare.LCLib.Asn1Processor.Asn1Node LipingShare.LCLib.Asn1Processor.Asn1Parser::get_RootNode()
extern void Asn1Parser_get_RootNode_m1BD2FEBA02CE99C54DA109AA78FFC1356E177590 ();
// 0x0000007C System.String LipingShare.LCLib.Asn1Processor.Asn1Parser::GetNodeTextHeader(System.Int32)
extern void Asn1Parser_GetNodeTextHeader_m0EA714BDDF54331302E7D2CCFC4ECA80531CC8DC ();
// 0x0000007D System.String LipingShare.LCLib.Asn1Processor.Asn1Parser::ToString()
extern void Asn1Parser_ToString_m75C377D84312EECCB5FF1C94DE2E32C5134A138E ();
// 0x0000007E System.String LipingShare.LCLib.Asn1Processor.Asn1Parser::GetNodeText(LipingShare.LCLib.Asn1Processor.Asn1Node,System.Int32)
extern void Asn1Parser_GetNodeText_mDE3FE82635CF43616E1CE6A75B8BEF75FED75982 ();
// 0x0000007F System.String LipingShare.LCLib.Asn1Processor.Asn1Util::FormatString(System.String,System.Int32,System.Int32)
extern void Asn1Util_FormatString_m1B438132EAE23A021F1EBB2716D611CB2C9B1300 ();
// 0x00000080 System.String LipingShare.LCLib.Asn1Processor.Asn1Util::GenStr(System.Int32,System.Char)
extern void Asn1Util_GenStr_m0C632140B3E2EC84F8E77ED8AA9B1C34747E4F65 ();
// 0x00000081 System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Util::BytesToLong(System.Byte[])
extern void Asn1Util_BytesToLong_m6532433DF91A9D791CE5E1187A1DDA12AE4A2A26 ();
// 0x00000082 System.String LipingShare.LCLib.Asn1Processor.Asn1Util::BytesToString(System.Byte[])
extern void Asn1Util_BytesToString_m979F5871B81B2C7BFA9701ED469A00F1D06E70B9 ();
// 0x00000083 System.String LipingShare.LCLib.Asn1Processor.Asn1Util::ToHexString(System.Byte[])
extern void Asn1Util_ToHexString_mACDA01C634BA0551D6A85A172CFCA94CDD4DE275 ();
// 0x00000084 System.Int32 LipingShare.LCLib.Asn1Processor.Asn1Util::BytePrecision(System.UInt64)
extern void Asn1Util_BytePrecision_mD36B2453F0C4AB155D692F0E7E08B58E1AFFF8F8 ();
// 0x00000085 System.Int32 LipingShare.LCLib.Asn1Processor.Asn1Util::DERLengthEncode(System.IO.Stream,System.UInt64)
extern void Asn1Util_DERLengthEncode_m5128680A717894A0E213D92963329B56CDC60D8F ();
// 0x00000086 System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Util::DerLengthDecode(System.IO.Stream,System.Boolean&)
extern void Asn1Util_DerLengthDecode_mACD73D1686B04B0D72BFFF77786842F1C7886E1E ();
// 0x00000087 System.String LipingShare.LCLib.Asn1Processor.Asn1Util::GetTagName(System.Byte)
extern void Asn1Util_GetTagName_mF94246451CEEC1DE50B9F046E6FCFD89444E2758 ();
// 0x00000088 System.Void LipingShare.LCLib.Asn1Processor.Asn1Util::.cctor()
extern void Asn1Util__cctor_m0718E758063DAC4F6D32E788D035E2AEDE2C8B8C ();
// 0x00000089 System.String LipingShare.LCLib.Asn1Processor.Oid::GetOidName(System.String)
extern void Oid_GetOidName_m5BB43CFC4ACF8863E8DD1FEBDB0141766084E271 ();
// 0x0000008A System.String LipingShare.LCLib.Asn1Processor.Oid::Decode(System.Byte[])
extern void Oid_Decode_m4ABDE4E939B9E3D6A2BD07B9C2B1299B2D7B2858 ();
// 0x0000008B System.String LipingShare.LCLib.Asn1Processor.Oid::Decode(System.IO.Stream)
extern void Oid_Decode_m50188F353E6B4A9F0ABD1403F9206E727B4451DD ();
// 0x0000008C System.Void LipingShare.LCLib.Asn1Processor.Oid::.ctor()
extern void Oid__ctor_m084F419D48646CD6DE030B62F5E3799C76FA9349 ();
// 0x0000008D System.Int32 LipingShare.LCLib.Asn1Processor.Oid::DecodeValue(System.IO.Stream,System.UInt64&)
extern void Oid_DecodeValue_m97062AC62333DF748CA16A09DEFB2B6C04A659F3 ();
// 0x0000008E System.Void LipingShare.LCLib.Asn1Processor.Oid::.cctor()
extern void Oid__cctor_m3C1002982D29B0A1CA68F4B478A705BADA8E342F ();
// 0x0000008F System.Void LipingShare.LCLib.Asn1Processor.RelativeOid::.ctor()
extern void RelativeOid__ctor_mECF2F0ED0127EB910EB5235F4E6B03B77D69ED69 ();
// 0x00000090 System.String LipingShare.LCLib.Asn1Processor.RelativeOid::Decode(System.IO.Stream)
extern void RelativeOid_Decode_mBD4C9DE80433B92BE4FBCA46DE84F7BBF3726085 ();
// 0x00000091 System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
extern void U3CPrivateImplementationDetailsU3E_ComputeStringHash_mDE5366C4E6A4BA98360E4EBDEEB620CBC8328424 ();
static Il2CppMethodPointer s_methodPointers[145] = 
{
	DistinguishedName_get_Country_m27D4E8B49804318270890ED9F41E00B291DD84E1,
	DistinguishedName_set_Country_m00E25B4C828A93251C476025B832B3B56F930F45,
	DistinguishedName_get_Organization_mCC686BE583CA12E32235260314D808AE44E2C181,
	DistinguishedName_set_Organization_mA53B8FC985363E83D16DF38A50089EB9B70DB62D,
	DistinguishedName_get_OrganizationalUnit_mE19318B17691E9DAC9F3AACCE55F5F73DF65EBAB,
	DistinguishedName_set_OrganizationalUnit_m0184277BD62539F23B4BEC531C190287FFD44496,
	DistinguishedName_get_Dnq_mEF98F1605FBEF5927B340BE6074FC37AE37FBF1D,
	DistinguishedName_set_Dnq_m79DA2975D9D89F4543324D86EF9276CC0C46C843,
	DistinguishedName_get_State_mFBABFC0FEB6F2DDF07A413901D98A87C758EB2C7,
	DistinguishedName_set_State_m6CD7373BB7A0B66EE4528D2B604E3207EC7B06CD,
	DistinguishedName_get_CommonName_m575DCDB5FAF4883AD24AAE02236D3146EE55A5D0,
	DistinguishedName_set_CommonName_mA608754548337CF47EFBBD1D7AC5473C3BE5835D,
	DistinguishedName_set_SerialNumber_m744A423172218C47707DEB407B3FE78A60891FD0,
	DistinguishedName__ctor_mABA27F8AB7256D47002AC39C5A62470640A2C990,
	DistinguishedName_Equals_m2273D715E5128AA9DB551EAB93A405A7F67D2277,
	DistinguishedName_ToString_m7A7D6A003A71713545FCDD8FD509042EF7E469DE,
	X509Cert_set_SerialNumber_m05F3E78124172BCD03494374A7793293CA76A325,
	X509Cert_set_ValidAfter_m4C305A27AD5FFD155A5B20F865C30E59617437D8,
	X509Cert_set_ValidBefore_m794D7FF4707A13B5B05834258F81ED5E7A36EF11,
	X509Cert_set_PubKey_m4EE1C3669043F9D6963658B63FFB7A80BA4F58B2,
	X509Cert_set_SelfSigned_mB92EBE4FDA4F1A710FC9D0D31D7B03CF0B57BF3D,
	X509Cert_get_Subject_mFC1E1273E1EA2F70FF9B0D7421B88F776E7A2B22,
	X509Cert_set_Subject_m4FE30A7F52C371B3642025803DBF6A864CC0B949,
	X509Cert_get_Issuer_m63A2EC83463E64DCD270B66CE55C94ABD2A2D8FD,
	X509Cert_set_Issuer_m5168289F36D844D4556428EEAAFB009E218F0993,
	X509Cert_set_Signature_mEEE076F624A5A9AE0FBF3297FBC56A15CA808040,
	X509Cert__ctor_m16B268E0660C17D4DE2DBB49C5F43995DBAAEECE,
	X509Cert_ParseNode_m6706478B6D79871A89D70C2463C2A85100CFC9BC,
	X509Cert_ParseTime_m0E21379D326B869E1C4B11D530B1B5D3FB1E22DB,
	InvalidX509Data__ctor_m0406193C9CC7403B517D574E7E6AE03E70055E3B,
	PKCS7_get_data_m948BF7C72D9C7F2DD5A1D7A7FB8B1342744712A9,
	PKCS7_set_data_m4A0AB958BFF7D70A7EAE5C54AE14655F50242360,
	PKCS7_get_sinfos_mC3AAEB227E1CDD2A405AE1B3C3659B4BBF9490A5,
	PKCS7_set_sinfos_m5701F26693B6BBF54EF63CE2609028ACA1B297A2,
	PKCS7_get_certChain_m647A8CBACC57B80F10CF41148111F938DD33A5A0,
	PKCS7_set_certChain_mC4C999DD19B8F78BD20FDDD4774C962612FFBAA9,
	PKCS7__ctor_mDB535D63E656612CCD3141AF99855B044B3D3C85,
	PKCS7_CheckStructure_m4B9D8F80CAC7D3057112491D39E2C0038AB39666,
	SignerInfo_get_Version_m6DD6B9B2476DD5A4CB388EFD4A5F55E3780F021A,
	SignerInfo_set_Version_mAF63A9C18D4843EBF167FCD34A38B9835B81BB15,
	SignerInfo_set_IssuerSerialNumber_mEAF569A7BDBA2D85B939321CFB23E8F2DBE6C641,
	SignerInfo_set_EncryptedDigest_m800BAF437A0C61DB3023CFE6D35F211BC53B93AB,
	SignerInfo__ctor_mE2FC3E812F4FF3AB71A7D8B63E220D6B25237E05,
	IAPSecurityException__ctor_mEE5C6B06562B82D2FA0FFD86680B082D981F37A0,
	InvalidPKCS7Data__ctor_mF10128BBF8FCC6299DED02BE839AC452508FE6D8,
	InvalidTimeFormat__ctor_m80EB2344062B3CF224E7C50E32633D6BE6761E06,
	UnsupportedSignerInfoVersion__ctor_mBC5A95399C04432DE2BF41AC39F052F0C74FE443,
	RSAKey_set_rsa_mA985188A7C5477FE2B3CFFC4CCDAD3D9262DDC19,
	RSAKey__ctor_m9CE603E33908A3C3BE529BF2500D9882579B542A,
	RSAKey_ParseNode_m374B0EEA8BD98F4C3F1C81BF1278048C0E3E2D75,
	RSAKey_ToXML_m2ED8CADF067B42841DC8DF2EE9C41A6B4A6C7CA6,
	InvalidRSAData__ctor_mBD6DD4963CBCBA3D90A0840D18949E0729F6AC49,
	AppleReceiptParser_Parse_m86F70F4BFA6395DC071B46774B8AFC99DF2C2F3F,
	AppleReceiptParser_Parse_mA1C64085E979FC8E3978A29034A83B54BC3F5568,
	AppleReceiptParser_ParseReceipt_m9D98471FAA50CB1B470DF99CC91DFE5D77939AE9,
	AppleReceiptParser_ParseInAppReceipt_mBB1E882408D783432073B787195A92B29876C7E4,
	AppleReceiptParser_TryParseDateTimeNode_m76518AB3D22F3017E1A87F37758E7873E932C036,
	NULL,
	AppleReceiptParser__ctor_m7FBB33B4A632A071E45E0B96BB19F1187ABC8D3A,
	AppleReceiptParser__cctor_mE8DB2B9B3C1311CB5F5A25F44B991AEC4B8AB9D2,
	AppleReceipt_set_bundleID_m18D4017369F0942480F860C9C5E931B34CBCC55D,
	AppleReceipt_set_appVersion_mD46F969C4725B3066A2C5DDC1A312175C4EE42C9,
	AppleReceipt_set_opaque_m1C3A799EE744DAD19647F1D493CC4015960BE694,
	AppleReceipt_set_hash_m09D73BF998A3D414D9AA59DAF0EA50DD9C8FC0D7,
	AppleReceipt_set_originalApplicationVersion_mA56F71E0A60F9CB135B615600091F569C2F173BC,
	AppleReceipt_set_receiptCreationDate_m8AFC15FFBB179D2A1AF332A0111DB29542739B02,
	AppleReceipt__ctor_m77284D7A1748BF2BF1F6D7BC669E37C6FB32CA66,
	AppleInAppPurchaseReceipt_set_quantity_m1B3527AEE43E35923E9B1AA79D459C6DCBF7BE36,
	AppleInAppPurchaseReceipt_get_productID_m342C7BAFE33CF778FA9DD1FAD4F416E5632CFFF2,
	AppleInAppPurchaseReceipt_set_productID_m8856F5E46B4640A86D72B90E57CEAD939933F25D,
	AppleInAppPurchaseReceipt_get_transactionID_m0ACEE55358C533A5139436DF7FBF8EA8E77BB13D,
	AppleInAppPurchaseReceipt_set_transactionID_m23F321C1776EC15DB0F2A4DD2D6375FBB9C33982,
	AppleInAppPurchaseReceipt_get_originalTransactionIdentifier_mEA064F5D2A874A0F7D989BE7611E1F6C0F494D4F,
	AppleInAppPurchaseReceipt_set_originalTransactionIdentifier_mA3EBC88CF53124921FD60F058305B03F2D7955D9,
	AppleInAppPurchaseReceipt_get_purchaseDate_m8368DC8A4A27526E2F93EC19569842B55FAFB014,
	AppleInAppPurchaseReceipt_set_purchaseDate_m3F9BEC803E49E5C7B391C56C79352610010E1F4A,
	AppleInAppPurchaseReceipt_get_originalPurchaseDate_m88CC67CD7B8652438DDFD69B34DDEB220D2838D9,
	AppleInAppPurchaseReceipt_set_originalPurchaseDate_m83EA1646E1236C7DB29AAC21E8865B4077A52FF5,
	AppleInAppPurchaseReceipt_get_subscriptionExpirationDate_m3B6E451DFD2C77717B46DBEDF778FC35909B8A42,
	AppleInAppPurchaseReceipt_set_subscriptionExpirationDate_m597F0DF94C97BE72B8E6D618AA05B5D68847A326,
	AppleInAppPurchaseReceipt_get_cancellationDate_mF6D2BC01A0CD91D2F261EA69781AF5C4A559B0CB,
	AppleInAppPurchaseReceipt_set_cancellationDate_mE269A74125D29E540818C07DB8B3BF471E188709,
	AppleInAppPurchaseReceipt_get_isFreeTrial_m94163306933ECAE902A5B1F0303A690ED3193AC2,
	AppleInAppPurchaseReceipt_set_isFreeTrial_m13CA8654B0D8813A15AE87191B60F3AF71A1543D,
	AppleInAppPurchaseReceipt_get_productType_mE57DFC228999EF79D830FADD6C2C6834A8F6A4EC,
	AppleInAppPurchaseReceipt_set_productType_mF8100BA9FCCC5548A9D00F004C45560B815461F7,
	AppleInAppPurchaseReceipt_get_isIntroductoryPricePeriod_mE40C7F712283A6952A9733FB9BEE6C4003964B59,
	AppleInAppPurchaseReceipt_set_isIntroductoryPricePeriod_m3DBC4A3228EEBAA815E62F0A7DA5B781BB4E76DE,
	AppleInAppPurchaseReceipt__ctor_m0A15EFC439370FB1201F8920434A999BEF164A09,
	Asn1Node__ctor_m052670A18A609C13A30FF3BF44F42292F90B5835,
	Asn1Node_Init_mDBFC888A267475E4627E72BE4CD12295A133D240,
	Asn1Node_GetHexPrintingStr_m5799642E9D8E03D27AD437EF46CB21AD1B8B0916,
	Asn1Node_FormatLineString_m5E573DC5B8749918EE821B39FF9A1F8056748F32,
	Asn1Node_FormatLineHexString_mF947EC13BE2B1476CC1295B758DC90C28CA90D0D,
	Asn1Node__ctor_mB895C7967A41AAA537B9D261A66C686952D38945,
	Asn1Node_get_Tag_mB20DF1AAA2B8D03D5C905B21F1877077580A599E,
	Asn1Node_get_MaskedTag_m96C6D2796A132D3040C2AA91D53526C1480CAE57,
	Asn1Node_LoadData_m020D328BF401DD10BE43BF7616EE7DB22FD55B9F,
	Asn1Node_SaveData_m225C667D486622DB65AC083360206C0B0087DA5E,
	Asn1Node_ClearAll_m3BC6216C01ACAC711F558EC315F37AAE00A444C0,
	Asn1Node_AddChild_mBAD5E7CD7DD3031021FBD7440C088D5C47FA3B00,
	Asn1Node_get_ChildNodeCount_mFF3B0FE66CC780D78DC16BA35A3AD17AC25FE867,
	Asn1Node_GetChildNode_m253F399D7632FC95B92F3490DB6985B9C07FC238,
	Asn1Node_get_TagName_m77D19A81729B25E427656F161477E21A06FDF67A,
	Asn1Node_get_ParentNode_m778967836987D1CB6B26EDC56AF8011F17297DF3,
	Asn1Node_GetText_m2196C0DEACE777BC160131956EE6F968C961FE20,
	Asn1Node_GetDataStr_mE38E8F3EB43A40F8E0773F1B4387DBC11AEF5672,
	Asn1Node_get_DataLength_m5E17D3369F2B0EE71BF6E4B57B86D8DC8AD61CAD,
	Asn1Node_get_Data_m0E1E0617C6E417EAFF68AD5E773C8ECEFDF8E4A2,
	Asn1Node_get_Deepness_m2FB44E1BAD03C278A0622FB7D898DF4F2B6B3C83,
	Asn1Node_set_RequireRecalculatePar_m76B8EB84833193FB0BFBC459A7A516FB2812C357,
	Asn1Node_RecalculateTreePar_m7C7A35DC21B77C08D70AE60CA5C477BB43A5F736,
	Asn1Node_ResetBranchDataLength_m2A130CA2632CED5A992861B1E7D93BFC8C3966F5,
	Asn1Node_ResetDataLengthFieldWidth_m58644868AC08BE51CF710A6704B7854A0BE5E5CF,
	Asn1Node_ResetChildNodePar_m9EB8E7AB23152E4C8FB105D8FF21231C4EAB4778,
	Asn1Node_GetListStr_m53278A63C27B25B99FF12B8B0FA8003D964DC436,
	Asn1Node_GetIndentStr_m40BFE323432887D2A0665792BC01CAE49D0C88B4,
	Asn1Node_GeneralDecode_m9B5C8C226CCC66EB64E7E59AB1618D30D59264B9,
	Asn1Node_ListDecode_m926C504D93EB07D45B9D3BD041DA259AFC89A5BE,
	Asn1Node_InternalLoadData_m7266383EBC8A8504BC7DBD82056C2DD869E9E3F2,
	Asn1Parser__ctor_mAEDC614D4605C27F4F610E481133BF1A9084A9F9,
	Asn1Parser_LoadData_mA88FD3A737019D65B9FF992AA6726AE9C6082C5E,
	Asn1Parser_get_RootNode_m1BD2FEBA02CE99C54DA109AA78FFC1356E177590,
	Asn1Parser_GetNodeTextHeader_m0EA714BDDF54331302E7D2CCFC4ECA80531CC8DC,
	Asn1Parser_ToString_m75C377D84312EECCB5FF1C94DE2E32C5134A138E,
	Asn1Parser_GetNodeText_mDE3FE82635CF43616E1CE6A75B8BEF75FED75982,
	Asn1Util_FormatString_m1B438132EAE23A021F1EBB2716D611CB2C9B1300,
	Asn1Util_GenStr_m0C632140B3E2EC84F8E77ED8AA9B1C34747E4F65,
	Asn1Util_BytesToLong_m6532433DF91A9D791CE5E1187A1DDA12AE4A2A26,
	Asn1Util_BytesToString_m979F5871B81B2C7BFA9701ED469A00F1D06E70B9,
	Asn1Util_ToHexString_mACDA01C634BA0551D6A85A172CFCA94CDD4DE275,
	Asn1Util_BytePrecision_mD36B2453F0C4AB155D692F0E7E08B58E1AFFF8F8,
	Asn1Util_DERLengthEncode_m5128680A717894A0E213D92963329B56CDC60D8F,
	Asn1Util_DerLengthDecode_mACD73D1686B04B0D72BFFF77786842F1C7886E1E,
	Asn1Util_GetTagName_mF94246451CEEC1DE50B9F046E6FCFD89444E2758,
	Asn1Util__cctor_m0718E758063DAC4F6D32E788D035E2AEDE2C8B8C,
	Oid_GetOidName_m5BB43CFC4ACF8863E8DD1FEBDB0141766084E271,
	Oid_Decode_m4ABDE4E939B9E3D6A2BD07B9C2B1299B2D7B2858,
	Oid_Decode_m50188F353E6B4A9F0ABD1403F9206E727B4451DD,
	Oid__ctor_m084F419D48646CD6DE030B62F5E3799C76FA9349,
	Oid_DecodeValue_m97062AC62333DF748CA16A09DEFB2B6C04A659F3,
	Oid__cctor_m3C1002982D29B0A1CA68F4B478A705BADA8E342F,
	RelativeOid__ctor_mECF2F0ED0127EB910EB5235F4E6B03B77D69ED69,
	RelativeOid_Decode_mBD4C9DE80433B92BE4FBCA46DE84F7BBF3726085,
	U3CPrivateImplementationDetailsU3E_ComputeStringHash_mDE5366C4E6A4BA98360E4EBDEEB620CBC8328424,
};
static const int32_t s_InvokerIndices[145] = 
{
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	26,
	26,
	9,
	14,
	26,
	949,
	949,
	26,
	31,
	14,
	26,
	14,
	26,
	26,
	26,
	26,
	239,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	26,
	23,
	10,
	32,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	26,
	26,
	28,
	107,
	23,
	28,
	472,
	28,
	28,
	95,
	-1,
	23,
	3,
	26,
	26,
	26,
	26,
	26,
	949,
	23,
	32,
	14,
	26,
	14,
	26,
	14,
	26,
	112,
	949,
	112,
	949,
	112,
	949,
	112,
	949,
	10,
	32,
	10,
	32,
	10,
	32,
	23,
	189,
	23,
	165,
	55,
	55,
	23,
	89,
	89,
	9,
	9,
	23,
	26,
	190,
	34,
	14,
	14,
	58,
	136,
	190,
	14,
	190,
	31,
	23,
	172,
	174,
	189,
	58,
	28,
	9,
	9,
	9,
	23,
	26,
	14,
	43,
	14,
	120,
	211,
	1516,
	172,
	0,
	0,
	156,
	1517,
	748,
	797,
	3,
	28,
	28,
	28,
	23,
	411,
	3,
	23,
	28,
	94,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x0600003A, { 0, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[3] = 
{
	{ (Il2CppRGCTXDataType)2, 14842 },
	{ (Il2CppRGCTXDataType)2, 14843 },
	{ (Il2CppRGCTXDataType)3, 10975 },
};
extern const Il2CppCodeGenModule g_SecurityCodeGenModule;
const Il2CppCodeGenModule g_SecurityCodeGenModule = 
{
	"Security.dll",
	145,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	3,
	s_rgctxValues,
	NULL,
};
